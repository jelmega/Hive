%VK_SDK_PATH%/Bin/glslangValidator.exe -V -S vert -o Shader_vert.spv Shader.vert
%VK_SDK_PATH%/Bin/glslangValidator.exe -V -S frag -o Shader_frag.spv Shader.frag
%VK_SDK_PATH%/Bin/glslangValidator.exe -V -S vert -o Forward_vert.spv forwardPass.vert
%VK_SDK_PATH%/Bin/glslangValidator.exe -V -S frag -o Forward_frag.spv forwardPass.frag
pause