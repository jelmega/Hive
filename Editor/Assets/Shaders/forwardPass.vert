#version 450
#extension GL_ARB_separate_shader_objects : enable

out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec4 inColor;
layout(location = 3) in vec2 inTexCoord;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec2 fragTexCoord;

layout(set=0, binding=0) uniform ViewData
{
    mat4 proj;
    mat4 view;
} view;

layout(set=2, binding=0) uniform ObjectData
{
    mat4 model;
} obj;

void main()
{
    gl_Position = view.proj * view.view * obj.model * vec4(inPosition, 1.0);
    fragColor = inColor;
    fragTexCoord = inTexCoord;
}