# Hive

Hive is game engine, written in c++17, completely from scratch

Note that this engine is in a pre-alpha state and everything is still able to change in the future

# Build instruction
When you are using visual studio, you can use the premade .sln file (these are being used during the develpment of the engine)

To create the required project files, Premake is used. Use the following command line.
```premake5``` should be resplaced with the correct path to premake5

Visual studio (in case you don't want to use the manually made project):

```premake5 vs20xx```

note that creating the visual studio project using Premake, will not have the projects filter and correctly working 'show all files' than the existing visual studio project

Codelite project:

```premake5 codelite```

GMake:

```premake5 gmake2```