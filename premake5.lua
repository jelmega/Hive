-- TODO:
    -- Other compiler support than vs (targetdir, ...)
    -- Does pchheader work with non vs?
    -- Non vs windows?

-- Globals
Src = "Source/"
EngineSrc = Src .. "Engine/"
EditorSrc = Src .. "Editor/"
ToolsSrc = Src .. "Tools/"
PluginsSrc = Src .. "Plugins/"

Header = "/**.h"
Inline = "/**.inl"
Cpp = "/**.cpp"
CSharp = "/**.cs"
PCHeader = "PCH.h"
PCHSource = "PCH.cpp"

Intermadiate = "Intermediate"
Binaries = "Binaries"

SrcDir = ""
filter "action:vs*"
    SrcDir = "$(SolutionDir)../Source/"

EngineIncludeDir = SrcDir .. "Engine/"
EditorIncludeDir = SrcDir .. "Editor/"
ToolsSrcDir = SrcDir .. "Tools/"
PluginsSrcDir = SrcDir .. "Plugins/"
IncludeDir = "/Include"  


CoreIncludeDir = EngineIncludeDir .. "Core" .. IncludeDir
SystemIncludeDir = EngineIncludeDir .. "System" .. IncludeDir
RendererCoreIncludeDir = EngineIncludeDir .. "RendererCore" .. IncludeDir
RendererIncludeDir = EngineIncludeDir .. "Renderer" .. IncludeDir
CompressionIncludeDir = EngineIncludeDir .. "Compression" .. IncludeDir
AssetSystemIncludeDir = EngineIncludeDir .. "AssetSystem" .. IncludeDir
PluginSystemIncludeDir = EngineIncludeDir .. "PluginSystem" .. IncludeDir
ECSIncludeDir = EngineIncludeDir .. "ECS" .. IncludeDir
EngineIncludeDir = EngineIncludeDir .. "Engine" .. IncludeDir

VulkanRHIIncludeDir = EngineIncludeDir .. "VulkanRHI" .. IncludeDir

EditorIncludeDir = EditorIncludeDir .. "Editor" .. IncludeDir

FileHandlersIncludeDir = PluginsSrcDir .. "FileHandlers" .. IncludeDir

-- Utility

-- Premake
workspace "Hive"
    configurations { "Debug", "Development", "Shipping", "Test" }
    platforms { "x86", "x64" }
    location "Build"
    targetprefix "Hive"
    startproject "Launch"

    -- Configurations
    filter "configurations:Debug"
        defines { "HV_DEBUG=1" }
        cppdialect "C++17"
        warnings "Extra"

    filter "configurations:Development"
        defines { "HV_DEVELOPMENT=1" }
        cppdialect "C++17"
        optimize "Debug"
        warnings "Extra"

    filter "configurations:Shipping"
        defines { "HV_SHIPPING=1" }
        cppdialect "C++17"
        optimize "Full"
        warnings "Extra"

    filter "configurations:Test"
        defines { "HV_TEST=1" }
        optimize "On"
        warnings "Extra"

    -- Platforms
    filter "platforms:x86"
        architecture "x86"

    filter "platforms:x64"
        architecture "x86_64"

    -- Environment specific
    filter "action:vs*"
        targetdir "$(SolutionDir)..\\Binaries\\$(PlatformTarget)\\$(Configuration)"
        objdir "$(SolutionDir)..\\Intermediate\\"
        cppdialect "C++17"
        toolset "v141"
        systemversion "10.0.17134.0"

    -- Engine Projects
    project "Core"
        kind "SharedLib"
        location "Build/Engine"
        language "C++"
        defines { "HV_EXPORT=1" }
        files {
            (EngineSrc .. "Core" .. Header),
            (EngineSrc .. "Core" .. Inline),
            (EngineSrc .. "Core" .. Cpp),
        }
        includedirs
        {
            CoreIncludeDir,
        }
        dependson {
            "RTTIGen",
        }

        filter "action:vs*"
            prebuildcommands {
                "dotnet $(OutDir)Tools\\RTTIGen\\netcoreapp2.0\\RTTIGen.dll $(ProjectDir)"
            }

    project "System"
        kind "SharedLib"
        location "Build/Engine"
        language "C++"
        defines { "HV_EXPORT=1" }
        files {
            (EngineSrc .. "System" .. Header),
            (EngineSrc .. "System" .. Inline),
            (EngineSrc .. "System" .. Cpp),
        }
        includedirs
        {
            SystemIncludeDir,
            CoreIncludeDir,
        }
        links {
            "Core",
        }
        dependson {
            "RTTIGen",
            "Core",
        }
        pchheader ("System" .. PCHeader)
        pchsource (EngineSrc .. "System/Source/System" .. PCHSource)

        filter "action:vs*"
            prebuildcommands {
                "dotnet $(OutDir)Tools\\RTTIGen\\netcoreapp2.0\\RTTIGen.dll $(ProjectDir)"
            }

    project "Compression"
        kind "SharedLib"
        location "Build/Engine"
        language "C++"
        defines { "HV_EXPORT=1" }
        files {
            (EngineSrc .. "Compression" .. Header),
            (EngineSrc .. "Compression" .. Inline),
            (EngineSrc .. "Compression" .. Cpp),
        }
        includedirs
        {
            CompressionIncludeDir,
            CoreIncludeDir,
            SystemIncludeDir,
        }
        links {
            "Core",
        }
        dependson {
            "RTTIGen",
            "Core",
        }
        pchheader ("Compression" .. PCHeader)
        pchsource (EngineSrc .. "Compression/Source/Compression" .. PCHSource)

        filter "action:vs*"
            prebuildcommands {
                "dotnet $(OutDir)Tools\\RTTIGen\\netcoreapp2.0\\RTTIGen.dll $(ProjectDir)"
            }

    project "AssetSystem"
        kind "SharedLib"
        location "Build/Engine"
        language "C++"
        defines { "HV_EXPORT=1" }
        files {
            (EngineSrc .. "AssetSystem" .. Header),
            (EngineSrc .. "AssetSystem" .. Inline),
            (EngineSrc .. "AssetSystem" .. Cpp),
        }
        includedirs
        {
            AssetSystemIncludeDir,
            CoreIncludeDir,
            SystemIncludeDir,
            RendererCoreIncludeDir,
        }
        links {
            "Core",
            "System",
            "RendererCore",
        }
        dependson {
            "RTTIGen",
            "Core",
            "System",
            "RendererCore",
        }
        pchheader ("AssetSystem" .. PCHeader)
        pchsource (EngineSrc .. "AssetSystem/Source/AssetSystem" .. PCHSource)

        filter "action:vs*"
            prebuildcommands {
                "dotnet $(OutDir)Tools\\RTTIGen\\netcoreapp2.0\\RTTIGen.dll $(ProjectDir)"
            }

    project "RendererCore"
        kind "SharedLib"
        location "Build/Engine"
        language "C++"
        defines { "HV_EXPORT=1" }
        files {
            (EngineSrc .. "RendererCore" .. Header),
            (EngineSrc .. "RendererCore" .. Inline),
            (EngineSrc .. "RendererCore" .. Cpp),
        }
        includedirs
        {
            RendererCoreIncludeDir,
            CoreIncludeDir,
            SystemIncludeDir,
        }
        links {
            "Core",
            "System",
        }
        dependson {
            "RTTIGen",
            "Core",
            "System",
        }
        pchheader ("RendererCore" .. PCHeader)
        pchsource (EngineSrc .. "RendererCore/Source/RendererCore" .. PCHSource)

        filter "action:vs*"
            prebuildcommands {
                "dotnet $(OutDir)Tools\\RTTIGen\\netcoreapp2.0\\RTTIGen.dll $(ProjectDir)"
            }


    project "Renderer"
        kind "SharedLib"
        location "Build/Engine"
        language "C++"
        defines { "HV_EXPORT=1" }
        files {
            (EngineSrc .. "Renderer" .. Header),
            (EngineSrc .. "Renderer" .. Inline),
            (EngineSrc .. "Renderer" .. Cpp),
        }
        includedirs
        {
            RendererIncludeDir,
            CoreIncludeDir,
            SystemIncludeDir,
            RendererCoreIncludeDir,
            AssetSystemIncludeDir,
        }
        links {
            "Core",
            "System",
            "RendererCore",
            "AssetSystem",
        }
        dependson {
            "RTTIGen",
            "Core",
            "System",
            "RendererCore",
            "AssetSystem",
        }
        pchheader ("Renderer" .. PCHeader)
        pchsource (EngineSrc .. "Renderer/Source/Renderer" .. PCHSource)

        filter "action:vs*"
            prebuildcommands {
                "dotnet $(OutDir)Tools\\RTTIGen\\netcoreapp2.0\\RTTIGen.dll $(ProjectDir)"
            }


    project "PluginSystem"
        kind "SharedLib"
        location "Build/Engine"
        language "C++"
        defines { "HV_EXPORT=1" }
        files {
            (EngineSrc .. "PluginSystem" .. Header),
            (EngineSrc .. "PluginSystem" .. Inline),
            (EngineSrc .. "PluginSystem" .. Cpp),
        }
        includedirs
        {
            AssetSystemIncludeDir,
            CoreIncludeDir,
        }
        links {
            "Core",
        }
        dependson {
            "RTTIGen",
            "Core",
        }
        pchheader ("PluginSystem" .. PCHeader)
        pchsource (EngineSrc .. "PluginSystem/Source/PluginSystem" .. PCHSource)

        filter "action:vs*"
            prebuildcommands {
                "dotnet $(OutDir)Tools\\RTTIGen\\netcoreapp2.0\\RTTIGen.dll $(ProjectDir)"
            }

    project "ECS"
        kind "SharedLib"
        location "Build/Engine"
        language "C++"
        defines { "HV_EXPORT=1" }
        files {
            (EngineSrc .. "ECS" .. Header),
            (EngineSrc .. "ECS" .. Inline),
            (EngineSrc .. "ECS" .. Cpp),
        }
        includedirs
        {
            ECSIncludeDir,
            CoreIncludeDir,
            SystemIncludeDir,
        }
        links {
            "Core",
            "System",
        }
        dependson {
            "RTTIGen",
            "Core",
            "System",
        }
        pchheader ("ECS" .. PCHeader)
        pchsource (EngineSrc .. "ECS/Source/ECS" .. PCHSource)

        filter "action:vs*"
            prebuildcommands {
                "dotnet $(OutDir)Tools\\RTTIGen\\netcoreapp2.0\\RTTIGen.dll $(ProjectDir)"
            }

    project "Engine"
        kind "SharedLib"
        location "Build/Engine"
        language "C++"
        defines { "HV_EXPORT=1" }
        files {
            (EngineSrc .. "Engine" .. Header),
            (EngineSrc .. "Engine" .. Inline),
            (EngineSrc .. "Engine" .. Cpp),
        }
        includedirs
        {
            EngineIncludeDir,
            CoreIncludeDir,
            SystemIncludeDir,
            RendererCoreIncludeDir,
            RendererIncludeDir,
            CompressionIncludeDir,
            AssetSystemIncludeDir,
            ECSIncludeDir,
            AssetSystemIncludeDir,
        }
        links {
            "Core",
            "System",
            "RendererCore",
            "Renderer",
            "Compression",
            "AssetSystem",
            "ECS",
            "PluginSystem",
        }
        dependson {
            "RTTIGen",
            "Core",
            "System",
            "RendererCore",
            "Renderer",
            "Compression",
            "AssetSystem",
            "ECS",
            "PluginSystem",
        }
        pchheader ("Engine" .. PCHeader)
        pchsource (EngineSrc .. "Engine/Source/Engine" .. PCHSource)

        filter "action:vs*"
            prebuildcommands {
                "dotnet $(OutDir)Tools\\RTTIGen\\netcoreapp2.0\\RTTIGen.dll $(ProjectDir)"
            }

    -- RHI projects
    project "VulkanRHI"
        kind "SharedLib"
        location "Build/Engine"
        language "C++"
        defines { "HV_EXPORT=1" }
        files {
            (EngineSrc .. "VulkanRHI" .. Header),
            (EngineSrc .. "VulkanRHI" .. Inline),
            (EngineSrc .. "VulkanRHI" .. Cpp),
        }
        includedirs
        {
            VulkanRHIIncludeDir,
            CoreIncludeDir,
            SystemIncludeDir,
            RendererCoreIncludeDir,
            RendererIncludeDir,
            AssetSystemIncludeDir,
            ECSIncludeDir,
            AssetSystemIncludeDir,
            "$(VULKAN_SDK)/include",
        }
        links {
            "Core",
            "System",
            "RendererCore",
        }
        dependson {
            "RTTIGen",
            "Core",
            "System",
            "RendererCore",
        }
        pchheader ("VulkanRHI" .. PCHeader)
        pchsource (EngineSrc .. "VulkanRHI/Source/VulkanRHI" .. PCHSource)

        filter "action:vs*"
            prebuildcommands {
                "dotnet $(OutDir)Tools\\RTTIGen\\netcoreapp2.0\\RTTIGen.dll $(ProjectDir)"
            }

    -- Editor projects
    project "Editor"
        kind "SharedLib"
        location "Build/Editor"
        language "C++"
        defines { "HV_EXPORT=1" }
        files {
            (EditorSrc .. "Editor" .. Header),
            (EditorSrc .. "Editor" .. Inline),
            (EditorSrc .. "Editor" .. Cpp),
        }
        includedirs
        {
            EditorIncludeDir,
            CoreIncludeDir,
            SystemIncludeDir,
            RendererCoreIncludeDir,
            RendererIncludeDir,
            CompressionIncludeDir,
            AssetSystemIncludeDir,
            ECSIncludeDir,
            AssetSystemIncludeDir,
            EngineIncludeDir,
        }
        links {
            "Core",
            "System",
            "RendererCore",
            "Renderer",
            "Compression",
            "AssetSystem",
            "ECS",
            "PluginSystem",
            "Engine",
        }
        dependson {
            "RTTIGen",
            "Core",
            "System",
            "RendererCore",
            "Renderer",
            "Compression",
            "AssetSystem",
            "ECS",
            "PluginSystem",
            "Engine",
        }
        pchheader ("Editor" .. PCHeader)
        pchsource (EditorSrc .. "Editor/Source/Editor" .. PCHSource)

        filter "action:vs*"
            prebuildcommands {
                "dotnet $(OutDir)Tools\\RTTIGen\\netcoreapp2.0\\RTTIGen.dll $(ProjectDir)"
            }

    -- Plugin projects
    project "FileHandlers"
        kind "SharedLib"
        location "Build/Plugins"
        language "C++"
        defines { "HV_EXPORT=1" }
        files {
            (PluginsSrc .. "FileHandlers" .. Header),
            (PluginsSrc .. "FileHandlers" .. Inline),
            (PluginsSrc .. "FileHandlers" .. Cpp),
        }
        includedirs
        {
            FileHandlersIncludeDir,
            CoreIncludeDir,
            SystemIncludeDir,
            RendererCoreIncludeDir,
            RendererIncludeDir,
            CompressionIncludeDir,
            AssetSystemIncludeDir,
            ECSIncludeDir,
            AssetSystemIncludeDir,
            EngineIncludeDir,
        }
        links {
            "Core",
            "System",
            "RendererCore",
            "Renderer",
            "Compression",
            "AssetSystem",
            "ECS",
            "PluginSystem",
            "Engine",
        }
        dependson {
            "RTTIGen",
            "Core",
            "System",
            "RendererCore",
            "Renderer",
            "Compression",
            "AssetSystem",
            "ECS",
            "PluginSystem",
            "Engine",
        }
        pchheader ("FileHandlers" .. PCHeader)
        pchsource (PluginsSrc .. "FileHandlers/Source/FileHandlers" .. PCHSource)

        filter "action:vs*"
            prebuildcommands {
                "dotnet $(OutDir)Tools\\RTTIGen\\netcoreapp2.0\\RTTIGen.dll $(ProjectDir)"
            }
            postbuildcommands {
                "if not exist \"$(SolutionDir)Editor\\Plugins\\FileHandlers\" mkdir \"$(SolutionDir)Editor\\Plugins\\FileHandlers\"" ..
                "copy \"$(ProjectDir)$(TargetName).hvplugin\" \"$(SolutionDir)Editor\\Plugins\\FileHandlers\\$(TargetName).hvplugin\"" ..
                "copy \"$(OutDir)$(TargetFileName)\" \"$(SolutionDir)Editor\\Plugins\\FileHandlers\\$(TargetFileName)\""
            }

    -- Tools projects
    externalproject "RTTIGen"
        kind "ConsoleApp"
        uuid "AC0CBB5E-CDEF-4BC5-B49A-838BD24AE0C1"
        location "Source/Tools/RTTIGen"
        language "C#"

    -- Launch projects  
    project "Launch"
        kind "ConsoleApp"
        location "Build"
        language "C++"
        files {
            (Src .. "Launch" .. Header),
            (Src .. "Launch" .. Inline),
            (Src .. "Launch" .. Cpp),
        }
        includedirs
        {
            CoreIncludeDir,
            SystemIncludeDir,
            RendererCoreIncludeDir,
            RendererIncludeDir,
            CompressionIncludeDir,
            AssetSystemIncludeDir,
            ECSIncludeDir,
            AssetSystemIncludeDir,
            EngineIncludeDir,
        }
        links {
            "Core",
            "System",
            "RendererCore",
            "Renderer",
            "Compression",
            "AssetSystem",
            "ECS",
            "PluginSystem",
            "Engine",
        }
        dependson {
            "RTTIGen",
            "Core",
            "System",
            "RendererCore",
            "Renderer",
            "Compression",
            "AssetSystem",
            "ECS",
            "PluginSystem",
            "Engine",
        }

        filter "action:vs*"
            prebuildcommands {
                "dotnet $(OutDir)Tools\\RTTIGen\\netcoreapp2.0\\RTTIGen.dll $(ProjectDir)"
            }
