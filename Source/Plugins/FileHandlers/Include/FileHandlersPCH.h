// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// FileHandlersPCH.h: Image file handlers Precompiled header
#pragma once
#include <Core.h>
#include <Compression.h>
#include <AssetSystem.h>
#include <PluginSystem.h>

HV_DECLARE_LOG_CATEGORY_EXTERN(PgImageFH)