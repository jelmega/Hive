// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ObjImporter.h: OBJ importer
// spec: http://paulbourke.net/dataformats/obj/
#pragma once
#include "FileHandlersPCH.h"

namespace Hv::FH {

	class HIVE_API ObjImporter : public AssetSystem::IAssetImporter
	{
	public:
		ObjImporter();
		~ObjImporter();

		/**
		* Import an asset
		* @param[in] filePath			Path to file
		* @param[in] pImportSettings	Import settings
		* @return						Pointer to loaded asset, nullptr if loading failed
		*/
		AssetSystem::IAsset* Import(const String& filePath, AssetSystem::ImportSettings* pImportSettings = nullptr) override final;

	private:

		struct IndexPair
		{
			u32 vnIdx;
			u32 vtIdx;
			u32 idx;
		};
	};
}