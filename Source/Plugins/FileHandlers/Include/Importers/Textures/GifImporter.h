// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// GifImporter.h: GIF importer
// Spec: https://www.w3.org/Graphics/GIF/spec-gif89a.txt
#pragma once
#include "FileHandlersPCH.h"
#include "FileCommon/Textures/GifCommon.h"

// TODO: how t handle animated gif data?

namespace Hv::FH {

	class HIVE_API GifImporter : public AssetSystem::IAssetImporter
	{
	public:
		GifImporter();
		~GifImporter();

		/**
		* Import an asset
		* @param[in] filePath			Path to file
		* @param[in] pImportSettings	Import settings
		* @return						Pointer to loaded asset, nullptr if loading failed
		*/
		AssetSystem::IAsset* Import(const String& filePath, AssetSystem::ImportSettings* pImportSettings = nullptr) override final;

	private:

		/**
		 * Load the data from the gif
		 * @param[in] file	File
		 * @param[in] data	Gif data
		 */
		void LoadData(HvFS::File& file, Gif::GifData& data);

		/**
		 * Decode an image
		 * @param[in] data		Gif data
		 * @param[in] idx		Image index
		 * @param[out] rawData	Raw data
		 */
		void DecodeImage(Gif::GifData& data, u8 idx, DynArray<u8>& rawData);

		/**
		 * Convert indices to RGBA
		 * @param[in] indices		Indices
		 * @param[in] colorTable	Color table
		 * @param[out] pixels		Pixel data
		 */
		void ToRGBA(DynArray<u8>& indices, Gif::ColorTable& colorTable, DynArray<u8>& pixels);

		/**
		 * Deinterlace an image
		 * @param[in] desc			Image descriptor
		 * @param[in] interlaced	Interlaces data
		 * @param[out] data			Data
		 */
		void Deinterlace(Gif::ImageDescriptor& desc, DynArray<u8>& interlaced, DynArray<u8>& data);
	};

}
