// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PnmImporter.h: PNM importer
#pragma once
#include "FileHandlersPCH.h"
#include "FileCommon/Textures/PnmCommon.h"

namespace Hv::FH {
	
	class HIVE_API PnmImporter : public AssetSystem::IAssetImporter
	{
	public:
		PnmImporter();
		~PnmImporter();

		/**
		 * Import an asset
		 * @param[in] filePath			Path to file
		 * @param[in] pImportSettings	Import settings
		 * @return						Pointer to loaded asset, nullptr if loading failed
		 */
		AssetSystem::IAsset* Import(const String& filePath, AssetSystem::ImportSettings* pImportSettings = nullptr) override final;

	private:

		/**
		 * Load the PNM header
		 * @param[in] file		File
		 * @param[out] header	PNM header
		 * @param[inout] line		Current line
		 * @return				True if the header was loaded succesfully, false otherwise
		 */
		b8 LoadHeader(HvFS::File& file, Pnm::PnmHeader& header, AnsiString& line);
		/**
		 * Interpret a PNM header line
		 * @param[inout] header	PNM header
		 * @param[in] line		Current line
		 */
		void InterpHeaderLine(Pnm::PnmHeader& header, AnsiString& line);

		/**
		 * Check if the file is encoded as ascii
		 * @param[in] type	PNM type
		 * @return			True if the type is ascii
		 */
		b8 IsAscii(Pnm::PnmType type);

		/**
		 * Load the ascii data
		 * @param[in] file		File
		 * @param[in] line		Current line in file
		 * @param[in] header	PNM header
		 * @param[out] rawData	Raw image data
		 */
		void LoadAscii(HvFS::File& file, AnsiString& line, Pnm::PnmHeader& header, DynArray<u8>& rawData);
		/**
		 * Load the raw data
		 * @param[in] file		File
		 * @param[in] header	PNM header
		 * @param[out] rawData	Raw image data
		 */
		void LoadRaw(HvFS::File& file, Pnm::PnmHeader& header, DynArray<u8>& rawData);
	};

}
