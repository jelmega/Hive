// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// BmpImporter.h: BMP importer
#pragma once
#include "FileHandlersPCH.h"
#include "FileCommon/Textures/BmpCommon.h"

namespace Hv::FH {

	class HIVE_API BmpImporter : public AssetSystem::IAssetImporter
	{
	public:
		BmpImporter();
		~BmpImporter();

		/**
		* Import an asset
		* @param[in] filePath			Path to file
		* @param[in] pImportSettings	Import settings
		* @return						Pointer to loaded asset, nullptr if loading failed
		*/
		AssetSystem::IAsset* Import(const String& filePath, AssetSystem::ImportSettings* pImportSettings = nullptr) override final;

	private:

		/**
		 * Load the DIB header
		 * @param[in] file		File
		 * @param[out] header	DIB header
		 * @return				True if the DIB header was loaded successfully, false otherwise
		 */
		b8 LoadDibHeader(HvFS::File& file, Gif::DIBHeader& header);

		/**
		 * Load the color table
		 * @param[in] file				File
		 * @param[in] colorsInTable		Amount of colors in color table
		 * @param[out] colorTable		Color table
		 */
		void LoadColorTable(HvFS::File& file, u32 colorsInTable, u8 colorTable[256 * 4]);

		/**
		 * Load the raw RGB BMP data
		 * @param[in] file		File
		 * @param[in] header	DIB header
		 * @param[out] rawData	Raw data
		 */
		void LoadRgb(HvFS::File& file, Gif::DIBHeader& header, DynArray<u8>& rawData);

		/**
		* Load the RGB BMP data with bit fields
		* @param[in] file		File
		* @param[in] header	DIB header
		* @param[out] rawData	Raw data
		*/
		void LoadBitField(HvFS::File& file, Gif::DIBHeader& header, DynArray<u8>& rawData);

		/**
		 * Get the bit field offset in bits
		 * @param[in] bitField	Bit field
		 * @param[out] offset	Offset of bit field in bits
		 * @param[out] maxVal	max value in the bitfield
		 */
		void GetBitFieldOffsetAndMaxVal(u32 bitField, u8& offset, u32& maxVal);

		/**
		 * Create raw data from indices
		 * @param[in] indices		Indices in color table
		 * @param[in] colorTable	Color table
		 * @param[in] converted		Converted data
		 */
		void GenerateFromIndices(const DynArray<u8>& indices, u8 colorTable[256 * 4], DynArray<u8>& converted);
	};

}
