// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PngImporter.h: PNG importer
#pragma once
#include "FileHandlersPCH.h"
#include "FileCommon/Textures/PngCommon.h"

// PNG specs: https://www.w3.org/TR/PNG
// TODO: Complete full implementation
namespace Hv::FH {
	
	class HIVE_API PngImporter : public AssetSystem::IAssetImporter
	{
	public:
		PngImporter();
		~PngImporter();

		/**
		* Import an asset
		* @param[in] filePath			Path to file
		* @param[in] pImportSettings	Import settings
		* @return						Pointer to loaded asset, nullptr if loading failed
		*/
		AssetSystem::IAsset* Import(const String& filePath, AssetSystem::ImportSettings* pImportSettings = nullptr) override final;

	private:

		/**
		 * Extract the PNG chunks
		 * @param[in] file		File
		 * @param[in] chunks	Chunks
		 */
		void ExtractChunks(HvFS::File& file, Png::Chunks& chunks);

		/**
		 * Remove the filter from the image
		 * @param[in] chunks		Chunks
		 * @param[in] unfiltered	Unfiltered data
		 * @param[out] data			Data
		 */
		void RemoveFilter(Png::Chunks& chunks, DynArray<u8>& unfiltered, DynArray<u8>& data);
		/**
		 * Peath predictor
		 * @param[in] a		Pixel to the left of the pixel
		 * @param[in] b		Pixel to the top of the pixel
		 * @param[in] c		Pixel to the top left of the pixel
		 * @return		Peath result
		 */
		u8 PeathPredictor(u8 a, u8 b, u8 c);

		/**
		 * Remove the filter and deinterlace an image using Adam7
		 * @param[in] chunks		Chunks
		 * @param[in] interlaced	Interlaced data
		 * @param[out] data			Data
		 */
		void ProcessAdam7(Png::Chunks& chunks, DynArray<u8>& interlaced, DynArray<u8>& data);

		/**
		* Convert the image data to RGBA
		* @param[in] chunks			Chunks
		* @param[in] data			Unfiltered ata
		* @param[out] converted		Converted data
		*/
		void ToRGBA(Png::Chunks& chunks, DynArray<u8>& data, DynArray<u8>& converted);
	};

}
