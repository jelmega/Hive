// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Plugin.h: Image file handlers plugin
#pragma once
#include "FileHandlersPCH.h"

namespace Hv::FH {

	// Texture importers
	class PngImporter;
	class GifImporter;
	class BmpImporter;
	class PnmImporter;

	// Model importers
	class ObjImporter;


	class HIVE_API Plugin : public PluginSystem::IPlugin
	{
	public:
		Plugin();
		~Plugin();

		/**
		* This function is called when the plugin is loaded, use this for pre initialization actions
		* @note Not all systems are created, do not use this function to register anything to the engine
		*/
		b8 OnLoad() override final;
		/**
		* This function is called when the plugin is registered, use this to initialize and/or register systems
		*/
		b8 OnRegister() override final;
		/**
		* This function is called when the plugin is enabled during runtime
		*/
		b8 OnEnable() override final;
		/**
		* This function is called when the plugin is disabled during runtime
		*/
		b8 OnDisable() override final;
		/**
		* This function is called when the plugin is unregistered, use this to shutdown and/or unregister systems
		*/
		b8 OnUnregister() override final;
		/**
		* This function is called when the plugin is unloaded, use this for post shutdown actions
		* @note Not all systems are created, do not use this function to register anything to the engine
		*/
		b8 OnUnload() override final;

	private:
		// Texture importers
		PnmImporter* m_pPnmImporter;	/**< PNM importer */
		BmpImporter* m_pBmpImporter;	/**< BMP importer */
		PngImporter* m_pPngImporter;	/**< PNG importer */
		GifImporter* m_pGifImporter;	/**< GIF importer */

		// Model importers
		ObjImporter* m_pObjImporter;	/**< OBJ importer */
	};

	extern "C"
	{
		/**
		 * Generate the plugin for the engine to use
		 * @return	Generated plugin
		 */
		HIVE_API PluginSystem::IPlugin* LoadPlugin();
	}
}
