// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// BmpCommon.cpp: BMP common data
#pragma once
#include "FileHandlersPCH.h"

namespace Hv::FH::Gif {

#pragma pack(push, 2)

	struct FileHeader
	{
		AnsiChar signature[2];
		u32 size;
		u16 reserved1;
		u16 reserved2;
		u32 pixelOffset;
	};

#pragma pack(pop)

	struct DIBHeader
	{
		u32 size;
		u32 width;
		u32 height;
		u16 planes;
		u16 bitsPerPixel;
		u32 compression;
		u32 imageSize;
		u32 xPixelsPerMeter;
		u32 yPixelsPerMeter;
		u32 colorsInColorTable;
		u32 importantColorCount;
		u32 redColorMask;
		u32 greenColorMask;
		u32 blueColorMask;
		u32 alphaColorMask;
		u32 colorSpaceType;
		u32 colorSpaceEndpoint[9];
		u32 redGamma;
		u32 greenGamma;
		u32 blueGamma;
		u32 intent;
		u32 iccProfileData;
		u32 iccProfileSize;
		u32 reserved;
	};

}
