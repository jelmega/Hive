// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PNMCommon.cpp: PNM common data
#pragma once
#include "FileHandlersPCH.h"

namespace Hv::FH::Pnm {

	enum class PnmType : u8
	{
		PbmAscii,
		PgmAscii,
		PpmAscii,
		PbmRaw,
		PgmRaw,
		PpmRaw,
		Count
	};

	struct PnmHeader
	{
		PnmType type = PnmType::Count;
		u32 width = 0xFFFF'FFFF;
		u32 height = 0xFFFF'FFFF;
		u32 maxValue = 0xFFFF'FFFF;
	};

}
