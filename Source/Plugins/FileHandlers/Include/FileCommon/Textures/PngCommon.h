// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PngCommon.cpp: PNG common data
#pragma once
#include "FileHandlersPCH.h"

namespace Hv::FH::Png {

	struct ChunkInfo
	{
		u32 length;
		u8 iden[4];
		u32 crc;
	};

	struct IHDR
	{
		u32 width;
		u32 height;
		u8 depth;
		u8 colorType;
		u8 compression;
		u8 filter;
		u8 interlace;
		ChunkInfo chInfo;
	};

	struct PLTE
	{
		u8 palette[256 * 3];
		ChunkInfo chInfo;
	};

	struct IDAT
	{
		DynArray<u8> data;
		ChunkInfo chInfo;
	};

	struct IEND
	{
		ChunkInfo chInfo;
	};

	struct PHYS
	{
		u32 xPix;
		u32 yPix;
		u8 unit;
		ChunkInfo chInfo;
	};

	struct TIME
	{
		u16 year;
		u8 month;
		u8 day;
		u8 hour;
		u8 minute;
		u8 second;
		ChunkInfo chInfo;
	};

	struct GAMA
	{
		u32 gamma;			/**< Actual gama times 100'000 */
		ChunkInfo chInfo;
	};

	struct BKGD
	{
		union
		{
			u16 grey;
			struct
			{
				u16 red;
				u16 green;
				u16 blue;
			};
			u8 index;
		};
		ChunkInfo chInfo;
	};

	/**
	* Plain text
	*/
	struct TEXT
	{
		String keyWord;
		String text;
		ChunkInfo chInfo;
	};

	/**
	* Compressed text
	*/
	struct ZTXT
	{
		String keyWord;
		String text;
		ChunkInfo chInfo;
	};

	/**
	* Compressed text
	*/
	struct ITXT
	{
		String keyWord;
		String language;
		String translatedKeyWord;
		b8 compressed;
		String text;
		ChunkInfo chInfo;
	};

	struct Chunks
	{
		IHDR ihdr;
		PLTE plte;
		DynArray<IDAT> idat;
		IEND iend;

		PHYS phys;
		TIME time;
		GAMA gama;
		BKGD bkgd;
		DynArray<TEXT> text;
		DynArray<ZTXT> ztxt;
		DynArray<ITXT> itxt;

	};

}
