// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// GifCommon.cpp: GIF common data
#pragma once
#include "FileHandlersPCH.h"

namespace Hv::FH::Gif {

	enum class Version : u8
	{
		GIF87a,
		GIF89a
	};

	struct Header
	{
		u8 signature[6];
		Version ver;
	};

#pragma pack(push, 1)
	struct LogicalScreenDesc
	{
		u16 width;
		u16 height;
		union
		{
			u8 packed;
#if HV_LITTLE_ENDIAN
			struct
			{
				u8 tableSize : 3;
				b8 sort : 1;
				u8 colorRes : 3;
				b8 globalTable : 1;
			};
#else
			struct
			{
				b8 globalTable : 1;
				u8 colorRes : 3;
				b8 sort : 1;
				u8 tableSize : 3;
		};
#endif
		};
		u8 backGround;
		u8 aspect;
	};
#pragma pack(pop)

	struct ColorTable
	{
		u8 colors[256 * 3];
	};

#pragma pack(push, 1)
	struct ImageDescriptor
	{
		u16 left;
		u16 top;
		u16 width;
		u16 height;
		union
		{
			u8 packed;
#if HV_LITTLE_ENDIAN
			struct
			{
				u8 tableSize : 3;
				u8 reserved : 2;
				b8 sort : 1;
				b8 interlace : 1;
				b8 localTable : 1;
			};
#else
			struct
			{
				b8 localTable : 1;
				b8 interlace : 1;
				b8 sort : 1;
				u8 reserved : 2;
				u8 tableSize : 3;
			};
#endif
		};
	};
#pragma pack(pop)

	struct ImageData
	{
		u8 minCodeSize;
		DynArray<u8> data;
	};

	struct GraphicsControlExt
	{
		u8 blockSize;
		union
		{
			u8 packed;
#if HV_LITTLE_ENDIAN
			struct
			{
				b8 transparent : 1;
				b8 userInput : 1;
				u8 desposal : 3;
				u8 reserved : 3;
			};
#else
			struct
			{
				u8 reserved : 3;
				u8 desposal : 3;
				b8 userInput : 1;
				b8 transparent : 1;
			};
#endif
		};
		u16 delayTime;
		u8 transparentColor;
	};

#pragma pack(push, 1)
	struct PlainTextExt
	{
		u8 blockSize;
		u16 left;
		u16 top;
		u16 width;
		u16 heigt;
		u8 cellWidth;
		u8 cellHeight;
		u8 foreColor;
		u8 backColor;
		String text;
	};

	struct AppExt
	{
		u8 blockSize;
		u64 iden;
		u32 authentication : 24;
		DynArray<u8> data;
	};
#pragma pack(pop)

	struct ImageInfo
	{
		ImageDescriptor desc;
		ImageData data;
		ColorTable table;
	};

	struct GifData
	{
		Header header;
		LogicalScreenDesc screenDesc;
		ColorTable colorTable;
		GraphicsControlExt control;
		AppExt appInfo;
		DynArray<ImageInfo> images;
		DynArray<String> comments;
		DynArray<PlainTextExt> plainText;
	};

}
