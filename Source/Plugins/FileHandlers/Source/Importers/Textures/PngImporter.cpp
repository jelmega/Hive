// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PngImporter.h: PNG importer
#include "FileHandlersPCH.h"
#include "Importers/Textures/PngImporter.h"

#define SIG(a, b, c, d, e, f, g, h) (u64(a) << 56 | u64(b) << 48 | u64(c) << 40 | u64(d) << 32 | u64(e) << 24 | u64(f) << 16 | u64(g) << 8 | u64(h))
#define IDEN(a, b, c, d) (u32(a) << 24 | u32(b) << 16 | u32(c) << 8 | u32(d))

namespace Hv::FH {

	PngImporter::PngImporter()
		: IAssetImporter(AssetSystem::AssetType::Image)
	{
	}

	PngImporter::~PngImporter()
	{
	}

	AssetSystem::IAsset* PngImporter::Import(const String& filePath, AssetSystem::ImportSettings* pImportSettings)
	{
		// TODO: use import settings

		HvFS::Path path(filePath);
		HvFS::File file = HvFS::CreateFile(path, HvFS::FileMode::Open, FileSystem::AccessMode::Read, FileSystem::ShareMode::Read);
		if (!file.IsValid())
		{
			g_Logger.LogFormat(LogPgImageFH(), LogLevel::Error, "Failed to load file (filepath: %s)", filePath);
			return nullptr;
		}

		u8 sig[8];
		file.ReadBytes(sig, 8);

		if (SIG(sig[0], sig[1], sig[2], sig[3], sig[4], sig[5], sig[6], sig[7]) != SIG('\x89', 'P', 'N', 'G', '\r', '\n', '\x1a', '\n'))
		{
			g_Logger.LogError(LogPgImageFH(), "Invalid PNG signature");
			return nullptr;
		}

		Png::Chunks chunks;
		ExtractChunks(file, chunks);
		file.Close();

		DynArray<u8> zlibStream;
		for (Png::IDAT& idat : chunks.idat)
		{
			zlibStream.Append(idat.data);
		}

		DynArray<u8> zlibRes;
		Compression::DecompressZlib(zlibStream, zlibRes);

		DynArray<u8> rawData;
		if (chunks.ihdr.interlace == 1)
		{
			ProcessAdam7(chunks, zlibRes, rawData);
		}
		else
		{
			RemoveFilter(chunks, zlibRes, rawData);
		}

		DynArray<u8> pixels;
		u64 neededSpace = chunks.ihdr.width * chunks.ihdr.height * 4; // RGBA
		pixels.Reserve(neededSpace);
		ToRGBA(chunks, rawData, pixels);


		ImageDesc desc = {};
		desc.width = chunks.ihdr.width;
		desc.height = chunks.ihdr.height;
		desc.format = PixelFormat(PixelFormatComponents::R8G8B8A8, PixelFormatTransform::UNORM);
		Image* pImage = HvNew Image();
		pImage->Create(desc, pixels);

		return pImage;
		//return nullptr;
	}

	void PngImporter::ExtractChunks(HvFS::File& file, Png::Chunks& chunks)
	{
		DynArray<u8> rawChunk;
		while (!file.EoF())
		{
			Png::ChunkInfo chInfo;

			file.ReadBytes((u8*)&chInfo, 8);
#if HV_LITTLE_ENDIAN
			chInfo.length = SwitchEndianess(chInfo.length);
#endif

			rawChunk.Push(chInfo.iden[0]);
			rawChunk.Push(chInfo.iden[1]);
			rawChunk.Push(chInfo.iden[2]);
			rawChunk.Push(chInfo.iden[3]);
			rawChunk.Resize(chInfo.length + 4);
			file.ReadBytes(rawChunk.Data() + 4, rawChunk.Size() - 4);

			file.Read(chInfo.crc);
#if HV_LITTLE_ENDIAN
			chInfo.crc = SwitchEndianess(chInfo.crc);
#endif

			switch (IDEN(chInfo.iden[0], chInfo.iden[1], chInfo.iden[2], chInfo.iden[3]))
			{
				//case IDEN('', '', '', ''):
			case IDEN('I', 'H', 'D', 'R'):
			{
				chunks.ihdr.chInfo = chInfo;
				Memory::Copy(&chunks.ihdr, rawChunk.Data() + 4, chInfo.length);
#if HV_LITTLE_ENDIAN
				chunks.ihdr.width = SwitchEndianess(chunks.ihdr.width);
				chunks.ihdr.height = SwitchEndianess(chunks.ihdr.height);
#endif
				break;
			}
			case IDEN('P', 'L', 'T', 'E'):
			{
				chunks.plte.chInfo = chInfo;
				//u16 numColors = chunks.iplt.chInfo.length / 3;
				Memory::Copy(chunks.plte.palette, rawChunk.Data() + 4, chInfo.length);
				break;
			}
			case IDEN('I', 'D', 'A', 'T'):
			{
				Png::IDAT idat;
				idat.chInfo = chInfo;
				idat.data.Assign(rawChunk.Data() + 4, rawChunk.Size() - 4);
				chunks.idat.Push(idat);
				break;
			}
			case IDEN('I', 'E', 'N', 'D'):
			{
				chunks.iend.chInfo = chInfo;
				u32 crc = Checksum::CRC32(rawChunk);
				if (crc != chInfo.crc)
				{
					g_Logger.LogFormat(LogPgImageFH(), LogLevel::Error, "Invalid PNG Chunk, iden: '%c%c%c%c'", chInfo.iden[0], chInfo.iden[1], chInfo.iden[2], chInfo.iden[3]);
				}
				return;
			}
			case IDEN('p', 'H', 'Y', 's'):
			{
				chunks.phys.chInfo = chInfo;
				Memory::Copy(&chunks.phys, rawChunk.Data() + 4, chInfo.length);
#if HV_LITTLE_ENDIAN
				chunks.phys.xPix = SwitchEndianess(chunks.phys.xPix);
				chunks.phys.yPix = SwitchEndianess(chunks.phys.yPix);
#endif
				break;
			}
			case IDEN('t', 'I', 'M', 'E'):
			{
				chunks.time.chInfo = chInfo;
				Memory::Copy(&chunks.time, rawChunk.Data() + 4, chInfo.length);
#if HV_LITTLE_ENDIAN
				chunks.time.year = SwitchEndianess(chunks.time.year);
#endif
				break;
			}
			case IDEN('g', 'A', 'M', 'A'):
			{
				chunks.gama.chInfo = chInfo;
				Memory::Copy(&chunks.gama, rawChunk.Data() + 4, chInfo.length);
#if HV_LITTLE_ENDIAN
				chunks.gama.gamma = SwitchEndianess(chunks.gama.gamma);
#endif
				break;
			}
			case IDEN('b', 'K', 'G', 'D'):
			{
				chunks.bkgd.chInfo = chInfo;
				Memory::Copy(&chunks.bkgd, rawChunk.Data() + 4, chInfo.length);
#if HV_LITTLE_ENDIAN
				if (chunks.ihdr.colorType & 0x02)
				{
					chunks.bkgd.red = SwitchEndianess(chunks.bkgd.red);
					chunks.bkgd.green = SwitchEndianess(chunks.bkgd.green);
					chunks.bkgd.blue = SwitchEndianess(chunks.bkgd.blue);
				}
				else
				{
					chunks.bkgd.grey = SwitchEndianess(chunks.bkgd.grey);
				}
#endif
				break;
			}
			case IDEN('t', 'E', 'X', 't'):
			{
				Png::TEXT text;
				text.chInfo = chInfo;
				text.keyWord = (AnsiChar*)rawChunk.Data() + 4;

				sizeT offset = text.keyWord.Length() + 1;
				text.text = String((AnsiChar*)rawChunk.Data() + offset, rawChunk.Size() - offset);
				chunks.text.Push(text);
				break;
			}
			case IDEN('z', 'T', 'X', 't'):
			{
				Png::ZTXT ztxt;
				ztxt.chInfo = chInfo;
				ztxt.keyWord = (AnsiChar*)rawChunk.Data() + 4;

				sizeT offset = ztxt.keyWord.Length() + 5;
				u8 compression = rawChunk[offset++];
				HV_ASSERT(compression == 0);

				DynArray<u8> zlibStream(&rawChunk[offset], chInfo.length - offset + 4);
				DynArray<u8> decompressed;
				Compression::DecompressZlib(zlibStream, decompressed);
				ztxt.text = String((AnsiChar*)decompressed.Data(), decompressed.Size());

				chunks.ztxt.Push(ztxt);
				break;
			}
			case IDEN('i', 'T', 'X', 't'):
			{
				Png::ITXT itxt;
				itxt.chInfo = chInfo;
				itxt.keyWord = (AnsiChar*)rawChunk.Data() + 4;

				sizeT offset = itxt.keyWord.Length() + 5;
				u8 flag = rawChunk[offset++];
				u8 compression = rawChunk[offset++];
				HV_ASSERT(compression == 0);

				itxt.language = (AnsiChar*)rawChunk.Data() + offset;
				offset += itxt.language.Length() + 1;
				itxt.translatedKeyWord = (AnsiChar*)rawChunk.Data() + offset;
				offset += itxt.translatedKeyWord.Length() + 1;

				if (flag)
				{
					itxt.text = String((AnsiChar*)rawChunk.Data() + offset, rawChunk.Size() - offset);
				}
				else
				{
					DynArray<u8> zlibStream(&rawChunk[offset], chInfo.length - offset + 4);
					DynArray<u8> decompressed;
					Compression::DecompressZlib(zlibStream, decompressed);
					itxt.text = String((AnsiChar*)decompressed.Data(), decompressed.Size());
				}
				chunks.itxt.Push(itxt);


				chunks.itxt.Push(itxt);
				break;
			}
			default:
				g_Logger.LogFormat(LogPgImageFH(), LogLevel::Error, "Unsupported PNG Chunk, iden: '%c%c%c%c'", chInfo.iden[0], chInfo.iden[1], chInfo.iden[2], chInfo.iden[3]);
				break;
			}

			// Checksum
			u32 crc = Checksum::CRC32(rawChunk);

			if (crc != chInfo.crc)
			{
				g_Logger.LogFormat(LogPgImageFH(), LogLevel::Error, "Invalid PNG Chunk, iden: '%c%c%c%c'", chInfo.iden[0], chInfo.iden[1], chInfo.iden[2], chInfo.iden[3]);
			}

			rawChunk.Clear();
		}
	}

	void PngImporter::RemoveFilter(Png::Chunks& chunks, DynArray<u8>& unfiltered, DynArray<u8>& data)
	{
		u8 numComponents;
		switch (chunks.ihdr.colorType)
		{
		default:
		case 0:
		case 3:
			numComponents = 1;
			break;
		case 4:
			numComponents = 2;
			break;
		case 2:
			numComponents = 3;
			break;
		case 6:
			numComponents = 4;
			break;
		}

		u8 bytesPerPix = (chunks.ihdr.depth + 7) / 8 * numComponents;
		u32 dstLineSize;
		u32 width;
		if (chunks.ihdr.depth < 8)
		{
			width = chunks.ihdr.width * chunks.ihdr.depth / 8;
			dstLineSize = chunks.ihdr.width * chunks.ihdr.depth / 8;
		}
		else
		{
			width = chunks.ihdr.width;
			dstLineSize = chunks.ihdr.width * bytesPerPix;
		}
		u32 srcLineSize = dstLineSize + 1;

		data.Reserve(dstLineSize * chunks.ihdr.height);

		for (u32 y = 0; y < chunks.ihdr.height; ++y)
		{
			u32 srcOffet = y * srcLineSize;

			u8 filter = unfiltered[srcOffet++];
			for (u32 x = 0; x < width; ++x)
			{
				for (u8 byte = 0; byte < bytesPerPix; ++byte)
				{
					u32 srcIdx = y * srcLineSize + x * bytesPerPix + byte + 1;
					u32 idx = y * dstLineSize + x * bytesPerPix + byte;
					switch (filter)
					{
					case 1: // sub
					{
						u8 a = x > 0 ? data[idx - bytesPerPix] : 0;
						data.Push(unfiltered[srcIdx] + a);
						break;
					}
					case 2: // up
					{
						u8 a = y > 0 ? data[idx - dstLineSize] : 0;
						data.Push(unfiltered[srcIdx] + a);
						break;
					}
					case 3: // average
					{
						u8 a = x > 0 ? data[idx - bytesPerPix] : 0;
						u8 b = y > 0 ? data[idx - dstLineSize] : 0;
						data.Push(unfiltered[srcIdx] + u8((a + b) / 2));
						break;
					}
					case 4: // peath
					{
						u8 a = x > 0 ? data[idx - bytesPerPix] : 0;
						u8 b = y > 0 ? data[idx - dstLineSize] : 0;
						u8 c = x > 0 && y > 0 ? data[idx - dstLineSize - bytesPerPix] : 0;
						data.Push(unfiltered[srcIdx] + PeathPredictor(a, b, c));
						break;
					}
					default:
					{
						data.Push(unfiltered[srcIdx]);
						break;
					}
					}
				}
			}
		}
	}

	u8 PngImporter::PeathPredictor(u8 a, u8 b, u8 c)
	{
		i16 p = i16(a + b) - c;
		u16 pa = u16(Hm::Abs(p - a));
		u16 pb = u16(Hm::Abs(p - b));
		u16 pc = u16(Hm::Abs(p - c));
		if (pa <= pb && pa <= pc)
			return a;
		if (pb <= pc)
			return b;
		return c;
	}

	void PngImporter::ProcessAdam7(Png::Chunks& chunks, DynArray<u8>& interlaced, DynArray<u8>& data)
	{
		data.Resize(interlaced.Size());

		u8 numComponents;
		switch (chunks.ihdr.colorType)
		{
		default:
		case 0:
		case 3:
			numComponents = 1;
			break;
		case 4:
			numComponents = 2;
			break;
		case 2:
			numComponents = 3;
			break;
		case 6:
			numComponents = 4;
			break;
		}

		u32 xBlocks = (chunks.ihdr.width + 7) / 8;
		u32 yBlocks = (chunks.ihdr.height + 7) / 8;

		u32 xOffset = 0;
		u32 xDelta = 8;
		u32 yOffset = 0;
		u32 yDelta = 8;
		//u32 subWidth = chunks.ihdr.width * numComponents;

		u32 lineWidth = chunks.ihdr.width * numComponents;
		u64 srcIdx = 0;

		for (u8 i = 0; i < 7; ++i)
		{
			u32 subWidth = yDelta * lineWidth;
			u32 pixOffset = xDelta * numComponents;;
			for (u32 y = 0, subY = 0; y < chunks.ihdr.height; y += yDelta, ++subY)
			{
				u8 filter = interlaced[srcIdx++];
				for (u32 x = 0, subX = 0; x < chunks.ihdr.width; x += xDelta, ++subX)
				{
					u64 idx = (y + yOffset) * lineWidth + (x + xOffset) * numComponents;
					for (u8 c = 0; c < numComponents; ++c, ++srcIdx)
					{
						switch (filter)
						{
						case 1: // sub
						{
							u8 a = subX > 0 ? data[idx + c - pixOffset] : 0;
							data[idx + c] = interlaced[srcIdx] + a;
							break;
						}
						case 2: // up
						{
							u8 b = subY > 0 ? data[idx + c - subWidth] : 0;
							data[idx + c] = interlaced[srcIdx] + b;
							break;
						}
						case 3: // average
						{
							u8 a = subX > 0 ? data[idx + c - pixOffset] : 0;
							u8 b = subY > 0 ? data[idx + c - subWidth] : 0;
							data[idx + c] = interlaced[srcIdx] + u8((a + b) / 2);
							break;
						}
						case 4: // peath
						{
							u8 a = subX > 0 ? data[idx + c - pixOffset] : 0;
							u8 b = subY > 0 ? data[idx + c - subWidth] : 0;
							u8 d = subX > 0 && subY > 0 ? data[idx + c - subWidth - pixOffset] : 0;
							data[idx + c] = interlaced[srcIdx] + PeathPredictor(a, b, d);
							break;
						}
						default:
						{
							data[idx + c] = interlaced[srcIdx];
							break;
						}
						}

					}
				}
			}

			// Update deltas and offsets for next step
			switch (i)
			{
			default:
			case 0: // 2
				xOffset = 4;
				xDelta = 8;
				yOffset = 0;
				yDelta = 8;
				break;
			case 1: // 3
				xOffset = 0;
				xDelta = 4;
				yOffset = 4;
				yDelta = 8;
				break;
			case 2: // 4
				xOffset = 2;
				xDelta = 4;
				yOffset = 0;
				yDelta = 4;
				break;
			case 3: // 5
				xOffset = 0;
				xDelta = 2;
				yOffset = 2;
				yDelta = 4;
				break;
			case 4: // 6
				xOffset = 1;
				xDelta = 2;
				yOffset = 0;
				yDelta = 2;
				break;
			case 5: // 7
				xOffset = 0;
				xDelta = 1;
				yOffset = 1;
				yDelta = 2;
				break;
			}
		}
	}

	void PngImporter::ToRGBA(Png::Chunks& chunks, DynArray<u8>& data, DynArray<u8>& converted)
	{
		switch (chunks.ihdr.colorType)
		{
		case 0:
		{
			if (chunks.ihdr.depth == 8)
			{
				for (sizeT i = 0; i < data.Size(); ++i)
				{
					// just skip lower value, order MSB LSB
					u8 grey = data[i];
					converted.Push(grey);
					converted.Push(grey);
					converted.Push(grey);
					converted.Push(0xFF);
				}
			}
			else // depth == 16
			{
				converted.Reserve(data.Size() / 2);
				for (sizeT i = 0; i < data.Size(); i += 2)
				{
					// just skip lower value, order MSB LSB
					u8 grey = data[i];
					converted.Push(grey);
					converted.Push(grey);
					converted.Push(grey);
					converted.Push(0xFF);
				}
			}
			break;
		}
		case 2:
		{
			if (chunks.ihdr.depth == 8)
			{
				for (sizeT i = 0; i < data.Size(); i += 3)
				{
					// just skip lower value, order MSB LSB
					converted.Push(data[i]);
					converted.Push(data[i + 1]);
					converted.Push(data[i + 2]);
					converted.Push(0xFF);
				}
			}
			else // depth == 16
			{
				converted.Reserve(data.Size() / 2);
				for (sizeT i = 0; i < data.Size(); i += 6)
				{
					// just skip lower value, order MSB LSB
					converted.Push(data[i]);
					converted.Push(data[i + 2]);
					converted.Push(data[i + 4]);
					converted.Push(0xFF);
				}
			}
			break;
		}
		case 3:
		{
			switch (chunks.ihdr.depth)
			{
			default:
			case 8:
			{
				for (sizeT i = 0; i < data.Size(); ++i)
				{
					u16 idx = data[i] * 3;
					// just skip lower value, order MSB LSB
					converted.Push(chunks.plte.palette[idx]);
					converted.Push(chunks.plte.palette[idx + 1]);
					converted.Push(chunks.plte.palette[idx + 2]);
					converted.Push(0xFF);
				}
				break;
			}
			case 4:
			{
				for (sizeT i = 0; i < data.Size(); ++i)
				{
					u16 idx0 = (data[i] >> 4) * 3;
					u16 idx1 = (data[i] & 0x0F) * 3;
					// just skip lower value, order MSB LSB
					converted.Push(chunks.plte.palette[idx0]);
					converted.Push(chunks.plte.palette[idx0 + 1]);
					converted.Push(chunks.plte.palette[idx0 + 2]);
					converted.Push(0xFF);
					converted.Push(chunks.plte.palette[idx1]);
					converted.Push(chunks.plte.palette[idx1 + 1]);
					converted.Push(chunks.plte.palette[idx1 + 2]);
					converted.Push(0xFF);
				}
				break;
			}
			case 2:
			{
				for (sizeT i = 0; i < data.Size(); ++i)
				{
					u16 idx0 = ((data[i] >> 6) & 0x03) * 3;
					u16 idx1 = ((data[i] >> 4) & 0x03) * 3;
					u16 idx2 = ((data[i] >> 2) & 0x03) * 3;
					u16 idx3 = (data[i] & 0x03) * 3;
					// just skip lower value, order MSB LSB
					converted.Push(chunks.plte.palette[idx0]);
					converted.Push(chunks.plte.palette[idx0 + 1]);
					converted.Push(chunks.plte.palette[idx0 + 2]);
					converted.Push(0xFF);
					converted.Push(chunks.plte.palette[idx1]);
					converted.Push(chunks.plte.palette[idx1 + 1]);
					converted.Push(chunks.plte.palette[idx1 + 2]);
					converted.Push(0xFF);
					converted.Push(chunks.plte.palette[idx2]);
					converted.Push(chunks.plte.palette[idx2 + 1]);
					converted.Push(chunks.plte.palette[idx2 + 2]);
					converted.Push(0xFF);
					converted.Push(chunks.plte.palette[idx3]);
					converted.Push(chunks.plte.palette[idx3 + 1]);
					converted.Push(chunks.plte.palette[idx3 + 2]);
					converted.Push(0xFF);
				}
				break;
			}
			case 1:
			{
				for (sizeT i = 0; i < data.Size(); ++i)
				{
					u16 idx0 = (data[i] & 0x80) ? 3 : 0;
					u16 idx1 = (data[i] & 0x40) ? 3 : 0;
					u16 idx2 = (data[i] & 0x20) ? 3 : 0;
					u16 idx3 = (data[i] & 0x10) ? 3 : 0;
					u16 idx4 = (data[i] & 0x08) ? 3 : 0;
					u16 idx5 = (data[i] & 0x04) ? 3 : 0;
					u16 idx6 = (data[i] & 0x02) ? 3 : 0;
					u16 idx7 = (data[i] & 0x01) ? 3 : 0;
					// just skip lower value, order MSB LSB
					converted.Push(chunks.plte.palette[idx0]);
					converted.Push(chunks.plte.palette[idx0 + 1]);
					converted.Push(chunks.plte.palette[idx0 + 2]);
					converted.Push(0xFF);
					converted.Push(chunks.plte.palette[idx1]);
					converted.Push(chunks.plte.palette[idx1 + 1]);
					converted.Push(chunks.plte.palette[idx1 + 2]);
					converted.Push(0xFF);
					converted.Push(chunks.plte.palette[idx2]);
					converted.Push(chunks.plte.palette[idx2 + 1]);
					converted.Push(chunks.plte.palette[idx2 + 2]);
					converted.Push(0xFF);
					converted.Push(chunks.plte.palette[idx3]);
					converted.Push(chunks.plte.palette[idx3 + 1]);
					converted.Push(chunks.plte.palette[idx3 + 2]);
					converted.Push(0xFF);
					converted.Push(chunks.plte.palette[idx4]);
					converted.Push(chunks.plte.palette[idx4 + 1]);
					converted.Push(chunks.plte.palette[idx4 + 2]);
					converted.Push(0xFF);
					converted.Push(chunks.plte.palette[idx5]);
					converted.Push(chunks.plte.palette[idx5 + 1]);
					converted.Push(chunks.plte.palette[idx5 + 2]);
					converted.Push(0xFF);
					converted.Push(chunks.plte.palette[idx6]);
					converted.Push(chunks.plte.palette[idx6 + 1]);
					converted.Push(chunks.plte.palette[idx6 + 2]);
					converted.Push(0xFF);
					converted.Push(chunks.plte.palette[idx7]);
					converted.Push(chunks.plte.palette[idx7 + 1]);
					converted.Push(chunks.plte.palette[idx7 + 2]);
					converted.Push(0xFF);
				}
				break;
			}
			}
			break;
		}
		case 4:
		{
			if (chunks.ihdr.depth == 8)
			{
				for (sizeT i = 0; i < data.Size(); i += 2)
				{
					// just skip lower value, order MSB LSB
					u8 grey = data[i];
					converted.Push(grey);
					converted.Push(grey);
					converted.Push(grey);
					converted.Push(data[i + 1]);
				}
			}
			else // depth == 16
			{
				converted.Reserve(data.Size() / 2);
				for (sizeT i = 0; i < data.Size(); i += 4)
				{
					// just skip lower value, order MSB LSB
					u8 grey = data[i];
					converted.Push(grey);
					converted.Push(grey);
					converted.Push(grey);
					converted.Push(data[i + 2]);
				}
			}
			break;
		}
		case 5:
		{
			if (chunks.ihdr.depth == 8)
			{
				converted.Append(data);
			}
			else // depth == 16
			{
				converted.Reserve(data.Size() / 2);
				for (sizeT i = 0; i < data.Size(); i += 2)
				{
					// just skip lower value, order MSB LSB
					converted.Push(data[i]);
				}
			}
			break;
		}
		default:
			break;
		}
	}
}
