// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// BmpImporter.cpp: BMP importer
#include "FileHandlersPCH.h"
#include "Importers/Textures/BmpImporter.h"

namespace Hv::FH {

	BmpImporter::BmpImporter()
		: IAssetImporter(AssetSystem::AssetType::Image)
	{
	}

	BmpImporter::~BmpImporter()
	{
	}

	AssetSystem::IAsset* BmpImporter::Import(const String& filePath, AssetSystem::ImportSettings* pImportSettings)
	{
		// TODO: use import settings

		HvFS::Path path(filePath);
		HvFS::File file = HvFS::CreateFile(path, HvFS::FileMode::Open, FileSystem::AccessMode::Read, FileSystem::ShareMode::Read);
		if (!file.IsValid())
		{
			g_Logger.LogFormat(LogPgImageFH(), LogLevel::Error, "Failed to load file (filepath: %s)", filePath);
			return nullptr;
		}

		// Read file header
		Gif::FileHeader fileHeader = {};
		file.Read(fileHeader);

		if (fileHeader.signature[0] != 'B' || fileHeader.signature[1] != 'M')
		{
			g_Logger.LogFormat(LogPgImageFH(), LogLevel::Error, "Invalid BMP signature (signature: '%c%c', expected 'BM')", filePath);
			return nullptr;
		}

#if HV_BIG_ENDIAN
		fileHeader.size = SwitchSystemLittleEndian(fileHeader.size);
		fileHeader.pixelOffset = SwitchSystemLittleEndian(fileHeader.pixelOffset);
#endif

		Gif::DIBHeader dibHeader = {};
		LoadDibHeader(file, dibHeader);

		u8 colorTable[256 * 4];
		if (dibHeader.colorsInColorTable > 0)
		{
			LoadColorTable(file, dibHeader.colorsInColorTable, colorTable);
		}
		else if (dibHeader.bitsPerPixel <= 8)
		{
			LoadColorTable(file, 1 << dibHeader.bitsPerPixel, colorTable);
		}

		// Jump to image data
		sizeT curPos = file.GetPosition();
		if (curPos < fileHeader.pixelOffset)
			file.MovePosition(fileHeader.pixelOffset - curPos);

		DynArray<u8> rawData;
		u64 neededSpace = dibHeader.width * dibHeader.height * 4; // RGBA
		rawData.Reserve(neededSpace);

		switch (dibHeader.compression)
		{
		case 0: // RGB
			LoadRgb(file, dibHeader, rawData);
			break;
		case 1: // RLE8
		{
			DynArray<u8> compressed;
			DynArray<u8> indices;
			compressed.Resize(dibHeader.imageSize);
			file.ReadBytes(compressed.Data(), compressed.Size());
			Compression::DecompressRLE8Bmp(compressed, dibHeader.width, indices);
			GenerateFromIndices(indices, colorTable, rawData);
			break;
		}
		case 2: // RLE4
		{
			DynArray<u8> compressed;
			DynArray<u8> indices;
			compressed.Resize(dibHeader.imageSize);
			file.ReadBytes(compressed.Data(), compressed.Size());
			Compression::DecompressRLE4Bmp(compressed, dibHeader.width, indices);
			GenerateFromIndices(indices, colorTable, rawData);
			break;
		}
		case 3: // BITFIELDS
			LoadBitField(file, dibHeader, rawData);
			break;
		case 4:	// JPEG
			// TODO
			break;
		case 5:	// PNG
			// TODO
			break;
		default:
			break;
		}

		file.Close();

		ImageDesc imageDesc = {};
		imageDesc.width = dibHeader.width;
		imageDesc.height = dibHeader.height;
		imageDesc.format = PixelFormat(PixelFormatComponents::R8G8B8A8, PixelFormatTransform::UNORM);

		Image* pImage = HvNew Image();
		pImage->Create(imageDesc, rawData);
		pImage->Flip2D(ImageFlipDirection::Vertical);

		return pImage;
	}

	b8 BmpImporter::LoadDibHeader(HvFS::File& file, Gif::DIBHeader& header)
	{
		u32 headerSize;
		file.Read(headerSize);
#if HV_BIG_ENDIAN
		headerSize = SwitchEndianess(headerSize);
#endif
		header.size = headerSize;

		u32 leftToRead = headerSize - sizeof(u32);
		u32 readSize = leftToRead;
		if (leftToRead > 36)
		{
			readSize = 36;
			leftToRead -= 36;
		}

		file.ReadBytes((u8*)&header.width, readSize);

#if HV_BIG_ENDIAN
		header.width = SwitchEndianess(header.width);
		header.height = SwitchEndianess(header.height);
		header.planes = SwitchEndianess(header.planes);
		header.bitsPerPixel = SwitchEndianess(header.bitsPerPixel);
		header.compression = SwitchEndianess(header.compression);
		header.imageSize = SwitchEndianess(header.imageSize);
		header.xPixelsPerMeter = SwitchEndianess(header.xPixelsPerMeter);
		header.yPixelsPerMeter = SwitchEndianess(header.yPixelsPerMeter);
		header.colorsInColorTable = SwitchEndianess(header.colorsInColorTable);
		header.importantColorCount = SwitchEndianess(header.importantColorCount);
#endif

		if (leftToRead > 0)
		{
			// If compression is masks
			if (header.compression == 3 || header.size == 128)
			{
				file.ReadBytes((u8*)&header.redColorMask, leftToRead);
#if HV_BIG_ENDIAN
				header.redColorMask = SwitchEndianess(header.redColorMask);
				header.blueColorMask = SwitchEndianess(header.blueColorMask);
				header.greenColorMask = SwitchEndianess(header.greenColorMask);
				header.alphaColorMask = SwitchEndianess(header.alphaColorMask);
#endif
			}
			else
			{
				file.ReadBytes((u8*)&header.colorSpaceType, leftToRead);
			}

#if HV_BIG_ENDIAN
			header.colorSpaceType = SwitchEndianess(header.colorSpaceType);
			header.colorSpaceEndpoint[0] = SwitchEndianess(header.colorSpaceEndpoint[0]);
			header.colorSpaceEndpoint[1] = SwitchEndianess(header.colorSpaceEndpoint[1]);
			header.colorSpaceEndpoint[2] = SwitchEndianess(header.colorSpaceEndpoint[2]);
			header.colorSpaceEndpoint[3] = SwitchEndianess(header.colorSpaceEndpoint[3]);
			header.colorSpaceEndpoint[4] = SwitchEndianess(header.colorSpaceEndpoint[4]);
			header.colorSpaceEndpoint[5] = SwitchEndianess(header.colorSpaceEndpoint[5]);
			header.colorSpaceEndpoint[6] = SwitchEndianess(header.colorSpaceEndpoint[6]);
			header.colorSpaceEndpoint[7] = SwitchEndianess(header.colorSpaceEndpoint[7]);
			header.colorSpaceEndpoint[8] = SwitchEndianess(header.colorSpaceEndpoint[8]);
			header.redGamma = SwitchEndianess(header.redGamma);
			header.greenGamma = SwitchEndianess(header.greenGamma);
			header.blueGamma = SwitchEndianess(header.blueGamma);
			header.intent = SwitchEndianess(header.intent);
			header.iccProfileData = SwitchEndianess(header.iccProfileData);
			header.iccProfileSize = SwitchEndianess(header.iccProfileSize);
			header.reserved = SwitchEndianess(header.reserved);
#endif
		}

		return true;
	}

	void BmpImporter::LoadColorTable(HvFS::File& file, u32 colorsInTable, u8 colorTable[256 * 4])
	{
		file.ReadBytes(colorTable, colorsInTable * 4);

		// Change format from BGRX to RGBA
		u8* it = colorTable;
		for (u32 i = 0; i < colorsInTable; ++i)
		{
			Swap(it[0], it[2]);
			it[3] = 0xFF;
			it += 4;
		}
	}

	void BmpImporter::LoadRgb(HvFS::File& file, Gif::DIBHeader& header, DynArray<u8>& rawData)
	{
		if (header.bitsPerPixel == 24)
		{
			for (u64 i = 0, max = header.width * header.height; i < max; ++i)
			{
				u8 bgr[3];
				file.ReadBytes(bgr, 3);
				rawData.Push(bgr[2]);
				rawData.Push(bgr[1]);
				rawData.Push(bgr[0]);
				rawData.Push(0xFF);
			}
		}
		else // bpp == 32
		{
			file.ReadBytes(rawData.Data(), header.width * header.height * 4);
		}
	}

	void BmpImporter::LoadBitField(HvFS::File& file, Gif::DIBHeader& header, DynArray<u8>& rawData)
	{
		u8 redOffset;
		u32 redMaxVal;
		GetBitFieldOffsetAndMaxVal(header.redColorMask, redOffset, redMaxVal);
		u8 greenOffset;
		u32 greenMaxVal;
		GetBitFieldOffsetAndMaxVal(header.greenColorMask, greenOffset, greenMaxVal);
		u8 blueOffset;
		u32 blueMaxVal;
		GetBitFieldOffsetAndMaxVal(header.blueColorMask, blueOffset, blueMaxVal);
		u8 alphaOffset;
		u32 alphaMaxVal;
		GetBitFieldOffsetAndMaxVal(header.alphaColorMask, alphaOffset, alphaMaxVal);

		for (u64 i = 0, max = header.width * header.height; i < max; ++i)
		{
			u32 data = 0;
			file.ReadBytes((u8*)&data, header.bitsPerPixel / 8);
			// R
			if (header.redColorMask)
			{
				u32 val = (data & header.redColorMask) >> redOffset;
				if (redMaxVal != 0xFF)
				{
					val *= 0xFF;
					val /= redMaxVal;
				}
				rawData.Push(val);
			}
			else
			{
				rawData.Push(0);
			}
			// G
			if (header.greenColorMask)
			{
				u32 val = (data & header.greenColorMask) >> greenOffset;
				if (greenMaxVal != 0xFF)
				{
					val *= 0xFF;
					val /= greenMaxVal;
				}
				rawData.Push(val);
			}
			else
			{
				rawData.Push(0);
			}
			// B
			if (header.blueColorMask)
			{
				u32 val = (data & header.blueColorMask) >> blueOffset;
				if (blueMaxVal != 0xFF)
				{
					val *= 0xFF;
					val /= blueMaxVal;
				}
				rawData.Push(val);
			}
			else
			{
				rawData.Push(0);
			}
			// A
			if (header.alphaColorMask)
			{
				u32 val = (data & header.alphaColorMask) >> alphaOffset;
				if (alphaMaxVal != 0xFF)
				{
					val *= 0xFF;
					val /= alphaMaxVal;
				}
				rawData.Push(val);
			}
			else
			{
				rawData.Push(0xFF);
			}
		}
	}

	void BmpImporter::GetBitFieldOffsetAndMaxVal(u32 bitField, u8& offset, u32& maxVal)
	{
		if (bitField == 0)
		{
			offset = 0;
			maxVal = 0;
			return;
		}

		offset = 0;
		maxVal = bitField;
		while (!(maxVal & 1))
		{
			maxVal >>= 1;
			++offset;
		}
	}

	void BmpImporter::GenerateFromIndices(const DynArray<u8>& indices, u8 colorTable[1024], DynArray<u8>& converted)
	{
		converted.Reserve(indices.Size() * 4);

		for (u16 index : indices)
		{
			index *= 4;
			converted.Push(colorTable[index]);
			converted.Push(colorTable[index + 1]);
			converted.Push(colorTable[index + 2]);
			converted.Push(colorTable[index + 3]);
		}
	}
}
