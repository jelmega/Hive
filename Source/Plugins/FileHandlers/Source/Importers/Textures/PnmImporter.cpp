// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PnmImporter.h: PNM importer
#include "FileHandlersPCH.h"
#include "Importers/Textures/PnmImporter.h"

namespace Hv::FH {

	PnmImporter::PnmImporter()
		: IAssetImporter(AssetSystem::AssetType::Image)
	{
	}

	PnmImporter::~PnmImporter()
	{
	}

	AssetSystem::IAsset* PnmImporter::Import(const String& filePath, AssetSystem::ImportSettings* pImportSettings)
	{
		// TODO: use import settings

		HvFS::Path path(filePath);
		HvFS::File file = HvFS::CreateFile(path, HvFS::FileMode::Open, FileSystem::AccessMode::Read, FileSystem::ShareMode::Read);
		if (!file.IsValid())
		{
			g_Logger.LogFormat(LogPgImageFH(), LogLevel::Error, "Failed to load file (filepath: %s)", filePath);
			return nullptr;
		}

		Pnm::PnmHeader header = {};
		AnsiString line;

		b8 res = LoadHeader(file, header, line);
		if (!res)
		{
			g_Logger.LogFormat(LogPgImageFH(), LogLevel::Error, "Invalid pnm header (filepath: %s)", filePath);
			return nullptr;
		}

		DynArray<u8> rawData;
		u64 neededSpace = header.width * header.height * 4; // RGBA
		rawData.Reserve(neededSpace);

		if (IsAscii(header.type))
		{
			LoadAscii(file, line, header, rawData);
		}
		else
		{
			LoadRaw(file, header, rawData);
		}

		file.Close();

		ImageDesc desc = {};
		desc.width = header.width;
		desc.height = header.height;
		desc.format = PixelFormat(PixelFormatComponents::R8G8B8A8, PixelFormatTransform::UNORM);

		Image* pImage = HvNew Image();
		pImage->Create(desc, rawData);

		return pImage;
	}

	b8 PnmImporter::LoadHeader(HvFS::File& file, Pnm::PnmHeader& header, AnsiString& line)
	{
		line = file.GetLineA();
		while (line.StartsWith('#'))
		{
			line = file.GetLineA();
		}

		if (!line.StartsWith('P'))
		{
			return false;
		}

		u8 idx = line[1] - '1';
		if (idx >= u8(Pnm::PnmType::Count))
			return false;

		header.type = Pnm::PnmType(idx);

		if (header.type == Pnm::PnmType::PbmAscii || header.type == Pnm::PnmType::PbmRaw)
		{
			while (header.height == 0xFFFF'FFFF)
			{
				line = file.GetLineA();
				while (line.StartsWith('#'))
				{
					line = file.GetLineA();
				}
				InterpHeaderLine(header, line);
			}
		}
		else
		{
			while (header.maxValue == 0xFFFF'FFFF)
			{
				line = file.GetLineA();
				while (line.StartsWith('#'))
				{
					line = file.GetLineA();
				}
				InterpHeaderLine(header, line);
			}
		}

		// Raw expects that their isn't a comment right before the data, since '#' can be a valid value at the front of the data
		// Ascii can have comments before data
		if (IsAscii(header.type))
		{
			line = file.GetLineA();
			while (line.StartsWith('#'))
			{
				line = file.GetLineA();
			}
		}

		return true;
	}

	void PnmImporter::InterpHeaderLine(Pnm::PnmHeader& header, AnsiString& line)
	{
		DynArray<AnsiString> vals = line.SplitChar(' ');
		for (AnsiString& val : vals)
		{
			if (val.StartsWith('#'))
				break;

			if (header.width == 0xFFFF'FFFF)
			{
				header.width = ParseU32(val);
			}
			else if (header.height == 0xFFFF'FFFF)
			{
				header.height = ParseU32(val);
			}
			else if (header.maxValue == 0xFFFF'FFFF)
			{
				header.maxValue = ParseU32(val);
			}
		}
	}

	b8 PnmImporter::IsAscii(Pnm::PnmType type)
	{
		switch (type)
		{
		case Pnm::PnmType::PbmAscii:
		case Pnm::PnmType::PgmAscii:
		case Pnm::PnmType::PpmAscii:
			return true;
		default:
			return false;
		}
	}

	void PnmImporter::LoadAscii(HvFS::File& file, AnsiString& line, Pnm::PnmHeader& header, DynArray<u8>& rawData)
	{
		// no settings yet, so load as R8B8G8A8

		u8 component = 0;
		DynArray<AnsiString> vals;
		vals.Resize(1);
		while (!file.EoF())
		{
			b8 oneValue = line.Find(' ') == AnsiString::NPos;
			if (oneValue)
			{
				vals[0] = line;
			}
			else
			{
				vals = line.SplitChar(' ');
			}
			for (AnsiString& val : vals)
			{
				switch (header.type)
				{
				case Pnm::PnmType::PpmAscii:
				{
					u32 comp = ParseU32(val);
					if (header.maxValue != 255)
					{
						comp *= 0xFF;
						comp /= header.maxValue;
					}
					rawData.Push(u8(comp));

					++component;
					if (component == 3)
					{
						rawData.Push(0xFF);
						component = 0;
					}
					break;
				}
				case Pnm::PnmType::PgmAscii:
				{
					u32 comp = ParseU32(val);
					if (header.maxValue != 255)
					{
						comp *= 0xFF;
						comp /= header.maxValue;
					}
					rawData.Push(u8(comp));
					rawData.Push(u8(comp));
					rawData.Push(u8(comp));
					rawData.Push(0xFF);
					break;
				}
				case Pnm::PnmType::PbmAscii:
				{
					for (AnsiChar ch : val)
					{
						u8 comp = ~((ch - '0') * 0xFF);
						rawData.Push(comp);
						rawData.Push(comp);
						rawData.Push(comp);
						rawData.Push(0xFF);
					}
					break;
				}
				default: break;
				}
			}

			line = file.GetLineA();

			if (!oneValue)
			{
				vals.Resize(1);
			}
		}
	}

	void PnmImporter::LoadRaw(HvFS::File& file, Pnm::PnmHeader& header, DynArray<u8>& rawData)
	{
		// no settings yet, so load as R8B8G8A8

		// Load data 1 line at a time
		DynArray<u8> lineData;
		while (!file.EoF())
		{
			switch (header.type)
			{
			case Pnm::PnmType::PpmRaw:
			{
				u32 stride = header.maxValue <= 255 ? 3 : 6;
				u64 lineSize = header.width * stride;
				lineData.Resize(lineSize);

				file.ReadBytes(lineData.Data(), lineSize);
				for (u32 i = 0; i < lineSize; i += stride)
				{
					if (header.maxValue <= 255)
					{
						u8* pix = &lineData[i];
						if (header.maxValue != 255)
						{
							pix[0] *= 0xFF;
							pix[0] /= header.maxValue;
							pix[1] *= 0xFF;
							pix[1] /= header.maxValue;
							pix[2] *= 0xFF;
							pix[2] /= header.maxValue;
						}
						rawData.Push(pix[0]);
						rawData.Push(pix[1]);
						rawData.Push(pix[2]);
						rawData.Push(0xFF);
					}
					else
					{
						u8* pix = &lineData[i];
						pix[0] = SwitchSystemBigEndian(pix[0]);
						pix[1] = SwitchSystemBigEndian(pix[1]);
						pix[2] = SwitchSystemBigEndian(pix[2]);

						pix[0] *= 0xFF;
						pix[0] /= header.maxValue;
						pix[1] *= 0xFF;
						pix[1] /= header.maxValue;
						pix[2] *= 0xFF;
						pix[2] /= header.maxValue;

						rawData.Push(u8(pix[0]));
						rawData.Push(u8(pix[1]));
						rawData.Push(u8(pix[2]));
						rawData.Push(0xFF);
					}
				}
				break;
			}
			case Pnm::PnmType::PgmRaw:
			{
				u32 stride = header.maxValue <= 255 ? 1 : 2;
				u64 lineSize = header.width * stride;
				lineData.Resize(lineSize);

				file.ReadBytes(lineData.Data(), lineSize);
				for (u32 i = 0; i < lineSize; i += stride)
				{
					if (header.maxValue <= 255)
					{
						u8 pix = lineData[i];
						if (header.maxValue != 255)
						{
							pix *= 0xFF;
							pix /= header.maxValue;
						}
						rawData.Push(pix);
						rawData.Push(pix);
						rawData.Push(pix);
						rawData.Push(0xFF);
					}
					else
					{
						u16 pix = *((u16*)lineData[i]);
						file.ReadBytes((u8*)&pix, sizeof(u16));
						pix = SwitchSystemBigEndian(pix);

						pix *= 0xFF;
						pix /= header.maxValue;

						rawData.Push(u8(pix));
						rawData.Push(u8(pix));
						rawData.Push(u8(pix));
						rawData.Push(0xFF);
					}
				}
				break;
			}
			case Pnm::PnmType::PbmRaw:
			{
				u64 lineSize = header.width / 8;
				lineData.Resize(lineSize);

				file.ReadBytes(lineData.Data(), lineSize);
				for (u32 i = 0; i < lineSize; ++i)
				{
					u8 packed = lineData[i];
					for (u8 i = 0; i < 8; ++i)
					{
						u8 pix = (packed >> (7 - i)) & 0x01;
						pix *= 0xFF;
						pix = ~pix;
						rawData.Push(pix);
						rawData.Push(pix);
						rawData.Push(pix);
						rawData.Push(0xFF);
					}
				}
				break;
			}
			default: break;
			}
		}
	}
}
