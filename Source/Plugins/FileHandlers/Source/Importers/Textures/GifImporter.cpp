// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// GifImporter.cpp: GIF importer
#include "FileHandlersPCH.h"
#include "Importers/Textures/GifImporter.h"
#include "Compression.h"

namespace Hv::FH {

	GifImporter::GifImporter()
		: IAssetImporter(AssetSystem::AssetType::Image)
	{
	}

	GifImporter::~GifImporter()
	{
	}

	AssetSystem::IAsset* GifImporter::Import(const String& filePath, AssetSystem::ImportSettings* pImportSettings)
	{
		// TODO: use import settings

		HvFS::Path path(filePath);
		HvFS::File file = HvFS::CreateFile(path, HvFS::FileMode::Open, FileSystem::AccessMode::Read, FileSystem::ShareMode::Read);
		if (!file.IsValid())
		{
			g_Logger.LogFormat(LogPgImageFH(), LogLevel::Error, "Failed to load file (filepath: %s)", filePath);
			return nullptr;
		}

		Gif::GifData data;
		LoadData(file, data);

		Gif::ImageDescriptor& desc = data.images[0].desc;
		DynArray<u8> rawData;
		u64 neededSpace = desc.width * desc.height * 4; // RGBA
		rawData.Reserve(neededSpace);
		DecodeImage(data, 0, rawData);

		file.Close();

		ImageDesc imageDesc = {};
		imageDesc.width = desc.width;
		imageDesc.height = desc.height;
		imageDesc.format = PixelFormat(PixelFormatComponents::R8G8B8A8, PixelFormatTransform::UNORM);

		Image* pImage = HvNew Image();
		pImage->Create(imageDesc, rawData);

		return pImage;
	}

	void GifImporter::LoadData(HvFS::File& file, Gif::GifData& data)
	{
		// Header
		file.ReadBytes((u8*)&data.header, 6);
		if (data.header.signature[0] != 'G' || data.header.signature[1] != 'I' || data.header.signature[2] != 'F')
		{
			g_Logger.LogFormat(LogPgImageFH(), LogLevel::Error, "Invalid GIF signature: '%c%c%c'", data.header.signature[0], data.header.signature[1], data.header.signature[2]);
		}
		if (data.header.signature[3] != '8' || (data.header.signature[4] != '7' && data.header.signature[4] != '9') || data.header.signature[5] != 'a')
		{
			g_Logger.LogFormat(LogPgImageFH(), LogLevel::Error, "Invalid GIF version: '%c%c%c'", data.header.signature[3], data.header.signature[4], data.header.signature[5]);
		}

		if (data.header.signature[4] == '7')
			data.header.ver = Gif::Version::GIF87a;
		else
			data.header.ver = Gif::Version::GIF89a;

		// Logical screen descriptor
		file.Read(data.screenDesc);

#if HV_BIG_ENDIAN
		data.screenDesc.width = SwitchEndianess(data.screenDesc.width);
		data.screenDesc.height = SwitchEndianess(data.screenDesc.height);
#endif

		if (data.screenDesc.globalTable)
		{
			u32 size = 3 * (1 << (data.screenDesc.tableSize + 1));
			file.ReadBytes(data.colorTable.colors, size);
		}

		u8 iden = 0;
		while (!file.EoF())
		{
			file.Read(iden);

			switch (iden)
			{
			case 0x2C: // Image descriptor
			{
				Gif::ImageInfo info;
				file.Read(info.desc);
#if HV_BIG_ENDIAN
				info.desc.left = SwitchEndianess(info.desc.width);
				info.desc.top = SwitchEndianess(info.desc.width);
				info.desc.width = SwitchEndianess(info.desc.width);
				info.desc.height = SwitchEndianess(info.desc.height);
#endif

				if (info.desc.localTable)
				{
					u32 size = 3 * (1 << (info.desc.tableSize + 1));
					file.ReadBytes(info.table.colors, size);
				}

				// Image data
				file.Read(info.data.minCodeSize);

				u8 blockSize;
				file.Read(blockSize);
				while (blockSize)
				{
					DynArray<u8> rawData;
					rawData.Resize(blockSize);
					file.ReadBytes(rawData.Data(), rawData.Size());
					info.data.data.Append(rawData);

					file.Read(blockSize);
				}
				data.images.Push(info);

				break;
			}
			case 0x21: // extensions
			{
				file.Read(iden);
				switch (iden)
				{
				case 0xFF: // Application ext
				{
					if (data.header.ver != Gif::Version::GIF89a)
					{
						g_Logger.LogError(LogPgImageFH(), "A GIF87a may not contain a application block!");
						return;
					}

					file.ReadBytes((u8*)&data.appInfo, 12);
					HV_ASSERT(data.appInfo.blockSize == 11);

					u8 blockSize;
					file.Read(blockSize);
					while (blockSize)
					{
						DynArray<u8> rawData;
						rawData.Resize(blockSize);
						file.ReadBytes(rawData.Data(), rawData.Size());
						data.appInfo.data.Append(rawData);

						file.Read(blockSize);
					}
					
					break;
				}
				case 0xFE: // Comment ext
				{
					if (data.header.ver != Gif::Version::GIF89a)
					{
						g_Logger.LogError(LogPgImageFH(), "A GIF87a may not contain a comment block!");
						return;
					}

					u8 blockSize;
					file.Read(blockSize);
					String comment;
					while (blockSize)
					{
						DynArray<u8> rawData;
						rawData.Resize(blockSize);
						file.ReadBytes(rawData.Data(), rawData.Size());
						comment.Append((AnsiChar*)rawData.Data(), rawData.Size());

						file.Read(blockSize);
					}
					data.comments.Push(comment);

					break;
				}
				case 0xF9: // Graphic control ext
				{
					if (data.header.ver != Gif::Version::GIF89a)
					{
						g_Logger.LogError(LogPgImageFH(), "A GIF87a may not contain a graphics control block!");
						return;
					}

					file.Read(data.control);
					HV_ASSERT(data.control.blockSize == 4);

					u8 terminator;
					file.Read(terminator);
					HV_ASSERT(terminator == 0);
					break;
				}
				case 0x01: // Plain text ext
				{
					if (data.header.ver != Gif::Version::GIF89a)
					{
						g_Logger.LogError(LogPgImageFH(), "A GIF87a may not contain a plain text block!");
						return;
					}

					Gif::PlainTextExt plainText;
					file.ReadBytes((u8*)&plainText, 13);
					HV_ASSERT(plainText.blockSize == 12);

					u8 blockSize;
					file.Read(blockSize);
					while (blockSize)
					{
						DynArray<u8> rawData;
						rawData.Resize(blockSize);
						file.ReadBytes(rawData.Data(), rawData.Size());
						plainText.text.Append((AnsiChar*)rawData.Data(), rawData.Size());

						file.Read(blockSize);
					}
					data.plainText.Push(plainText);

					break;
				}
				default:
					break;
				}
				break;
			}
			case 0x3B: // Trailer (end of image)
			{
				return;
			}
			default:
				g_Logger.LogFormat(LogPgImageFH(), LogLevel::Error, "Invalid GIF label: %u", iden);
				break;
			}
		}
	}

	void GifImporter::DecodeImage(Gif::GifData& data, u8 idx, DynArray<u8>& rawData)
	{
		Gif::ImageInfo& info = data.images[idx];

		DynArray<u8> decompressed;
		decompressed.Reserve(info.desc.width * info.desc.height); // reserce space for width * height number of indices
		Compression::DecompressLZWGif(info.data.data, info.data.minCodeSize, decompressed);

		Gif::ColorTable& colorTable = info.desc.localTable ? info.table : data.colorTable;

		if (info.desc.interlace)
		{
			DynArray<u8> deinterlaced;
			Deinterlace(info.desc, decompressed, deinterlaced);
			ToRGBA(deinterlaced, colorTable, rawData);
		}
		else
		{
			ToRGBA(decompressed, colorTable, rawData);
		}
	}

	void GifImporter::ToRGBA(DynArray<u8>& indices, Gif::ColorTable& colorTable, DynArray<u8>& pixels)
	{
		for (u8 index : indices)
		{
			u16 idx = index * 3;
			pixels.Push(colorTable.colors[idx]);
			pixels.Push(colorTable.colors[idx + 1]);
			pixels.Push(colorTable.colors[idx + 2]);
			pixels.Push(0xFF);
		}
	}

	void GifImporter::Deinterlace(Gif::ImageDescriptor& desc, DynArray<u8>& interlaced, DynArray<u8>& data)
	{
		u8 delta = 8;
		u8 offset = 0;
		u32 srcIdx = 0;

		data.Resize(interlaced.Size());
		for (u8 i = 0; i < 4; ++i)
		{
			for (u32 y = offset; y < desc.height; y += delta, srcIdx += desc.width)
			{
				Memory::Copy(&data[y * desc.width], &interlaced[srcIdx], desc.width);
			}

			switch (i)
			{
			case 0:
				offset = 4;
				break;
			case 1:
				offset = 2;
				delta = 4;
				break;
			case 2:
				offset = 1;
				delta = 2;
				break;
			default:
				break;
			}
		}
	}
}
