// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ObjImporter.cpp: OBJ importer
#include "FileHandlersPCH.h"
#include "Importers/Models/ObjImporter.h"

namespace Hv::FH {


	ObjImporter::ObjImporter()
		: IAssetImporter(AssetSystem::AssetType::Mesh)
	{
	}

	ObjImporter::~ObjImporter()
	{
	}

	AssetSystem::IAsset* ObjImporter::Import(const String& filePath, AssetSystem::ImportSettings* pImportSettings)
	{
		// TODO: use import settings

		HvFS::Path path(filePath);
		HvFS::File file = HvFS::CreateFile(path, HvFS::FileMode::Open, FileSystem::AccessMode::Read, FileSystem::ShareMode::Read);
		if (!file.IsValid())
		{
			g_Logger.LogFormat(LogPgImageFH(), LogLevel::Error, "Failed to load file (filepath: %s)", filePath);
			return nullptr;
		}

		DynArray<f32v3> positions;
		DynArray<f32v3> normals;
		DynArray<f32v4> colors;
		DynArray<f32v2> texCoords;

		DynArray<Vertex> vertices;
		DynArray<u32> indices;
		DynArray<DynArray<IndexPair>> indexPairs;

		while (!file.EoF())
		{
			AnsiString line = file.GetLineA();
			AnsiChar iden = line[0];


			switch (iden)
			{
			case 'v': // vertices
			{
				iden = line[1];
				switch (iden)
				{
				case ' ': // position
				{
					sizeT begin = 2;
					sizeT end = line.Find(' ', begin);
					f32v3 pos;

					String val = line.SubString(begin, end - begin);
					pos.x = ParseF32(val);

					begin = end + 1;
					end = line.Find(' ', begin);
					val = line.SubString(begin, end - begin);
					pos.y = ParseF32(val);

					begin = end + 1;
					end = line.Find(' ', begin);
					val = line.SubString(begin, end - begin);
					// Flip z, since most blender files use right handed Y-up | TODO: Add this to obj import settings?
					pos.z = -ParseF32(val);

					positions.Push(pos);
					break;
				}
				case 'n': // Normals
				{
					sizeT begin = 3;
					sizeT end = line.Find(' ', begin);
					f32v3 norm;

					String val = line.SubString(begin, end - begin);
					norm.x = ParseF32(val);

					begin = end + 1;
					end = line.Find(' ', begin);
					val = line.SubString(begin, end - begin);
					norm.y = ParseF32(val);

					begin = end + 1;
					end = line.Find(' ', begin);
					val = line.SubString(begin, end - begin);
					norm.z = ParseF32(val);

					normals.Push(norm);
					break;
					break;
				}
				case 't': // Texcoord (Y-coord flipped in obj)
				{
					sizeT begin = 3;
					sizeT end = line.Find(' ', begin);
					f32v2 tc;

					String val = line.SubString(begin, end - begin);
					tc.x = ParseF32(val);

					begin = end + 1;
					end = line.Find(' ', begin);
					val = line.SubString(begin, end - begin);
					tc.y = 1.f - ParseF32(val);

					texCoords.Push(tc);
					break;
				}
				default:
					break;
				}
				break;
			}
			case 'f': // faces (indices, start at 1)
			{
				sizeT space = line.Find(' ', 2);
				String val = line.SubString(2, space - 2);

				// TODO 4: index faces
				for (u8 i = 0; i < 3; ++i)
				{
					sizeT slash = val.Find('/');
					if (slash == AnsiString::NPos)
					{
						u32 idx = ParseI32(val) - 1;
						indices.Push(idx);
					}
					else
					{
						String tmp = val.SubString(0, slash);
						u32 vIdx = ParseI32(tmp);

						sizeT slash2 = val.Find('/', slash + 1);
						u32 vnIdx = vIdx;
						u32 vtIdx = vIdx;
						if (slash2 != AnsiString::NPos)
						{
							if (slash + 1 < slash2)
							{
								tmp = val.SubString(slash + 1, slash2 - slash);
								vtIdx = ParseI32(tmp);
							}

							tmp = val.SubString(slash2 + 1);
							vnIdx = ParseI32(tmp);
						}
						else
						{
							tmp = val.SubString(slash + 1);
							vtIdx = ParseI32(tmp);
						}

						--vIdx;
						--vtIdx;
						--vnIdx;

#if 0
						Vertex vertex;
						vertex.position = positions[vIdx];
						if (vertices.Size() == 524287)
							int db = 0;
						vertices.Push(vertex);
						indices.Push(vIdx);
#else
						// Handle multi index verts
						if (positions.Size() > indexPairs.Size())
						{
							indexPairs.Resize(positions.Size());
						}

						DynArray<IndexPair>& pairs = indexPairs[vIdx];

						b8 found = false;
						if (pairs.Size() > 0)
						{
							for (IndexPair& pair : pairs)
							{
								if (vtIdx == pair.vtIdx && vnIdx == pair.vnIdx)
								{
									indices.Push(pair.idx);
									found = true;
									break;
								}
							}
						}

						if (!found)
						{
							if (pairs.Size() == 0)
							{
								// Assign new dynarray, so that allocator is set correctly
								pairs = DynArray<IndexPair>();
							}

							IndexPair pair;
							pair.vtIdx = vtIdx;
							pair.vnIdx = vnIdx;
							pair.idx = u32(vertices.Size());
							pairs.Push(pair);
							indices.Push(pair.idx);

							Vertex vertex;
							vertex.position = positions[vIdx];
							if (texCoords.Size() > 0)
								vertex.texCoords = texCoords[vtIdx];
							if (normals.Size() > 0)
								vertex.normals = normals[vnIdx];
							vertices.Push(vertex);
						}
#endif
					}

					sizeT begin = space + 1;
					space = line.Find(' ', begin);
					val = line.SubString(begin, space - begin);
				}
				break;
			}

			case '#': // Comment
			default:
				break;
			}
		}

		Mesh* pMesh = HvNew Mesh();

		pMesh->SetVertices(vertices);
		pMesh->SetIndices(indices);

		return pMesh;
	}
}
 