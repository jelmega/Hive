// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Plugin.cpp: Image file handlers plugin
#include "FileHandlersPCH.h"
#include "Plugin.h"

// Texture importers
#include "Importers/Textures/PnmImporter.h"
#include "Importers/Textures/BmpImporter.h"
#include "Importers/Textures/PngImporter.h"
#include "Importers/Textures/GifImporter.h"

// Model importers
#include "Importers/Models/ObjImporter.h"

HV_DECLARE_LOG_CATEGORY(PgImageFH, Hv::LogLevel::All);

namespace Hv::FH {

	Plugin::Plugin()
		: m_pPnmImporter(nullptr)
		, m_pBmpImporter(nullptr)
		, m_pPngImporter(nullptr)
		, m_pGifImporter(nullptr)

		, m_pObjImporter(nullptr)
	{
	}

	Plugin::~Plugin()
	{
	}

	b8 Plugin::OnLoad()
	{
		// Create texture importers
		m_pPnmImporter = HvNew PnmImporter();
		m_pBmpImporter = HvNew BmpImporter();
		m_pPngImporter = HvNew PngImporter();
		m_pGifImporter = HvNew GifImporter();

		// Create model importers
		m_pObjImporter = HvNew ObjImporter();

		g_Logger.LogInfo(LogPgImageFH(), "Loaded image file handlers plugin");
		return true;
	}

	b8 Plugin::OnRegister()
	{
		// Register texture importers
		g_AssetManager.RegisterImporter("pnm", m_pPnmImporter);
		g_AssetManager.RegisterImporter("ppm", m_pPnmImporter);
		g_AssetManager.RegisterImporter("pgm", m_pPnmImporter);
		g_AssetManager.RegisterImporter("pbm", m_pPnmImporter);
		g_AssetManager.RegisterImporter("bmp", m_pBmpImporter);
		g_AssetManager.RegisterImporter("png", m_pPngImporter);
		g_AssetManager.RegisterImporter("gif", m_pGifImporter);

		// Register model importers
		g_AssetManager.RegisterImporter("obj", m_pObjImporter);


		g_Logger.LogInfo(LogPgImageFH(), "Registered image file handlers plugin");
		return true;
	}

	b8 Plugin::OnEnable()
	{
		// Do nothing
		return true;
	}

	b8 Plugin::OnDisable()
	{
		// Do nothing
		return true;
	}

	b8 Plugin::OnUnregister()
	{
		// Unregister texture importers
		g_AssetManager.UnregisterImporter("pnm", m_pPnmImporter);
		g_AssetManager.UnregisterImporter("ppm", m_pPnmImporter);
		g_AssetManager.UnregisterImporter("pgm", m_pPnmImporter);
		g_AssetManager.UnregisterImporter("pbm", m_pPnmImporter);
		g_AssetManager.UnregisterImporter("bmp", m_pBmpImporter);
		g_AssetManager.UnregisterImporter("png", m_pPngImporter);
		g_AssetManager.UnregisterImporter("gif", m_pGifImporter);

		// Unregister models importers
		g_AssetManager.UnregisterImporter("obj", m_pObjImporter);

		g_Logger.LogInfo(LogPgImageFH(), "Unregistered image file handlers plugin");
		return true;
	}

	b8 Plugin::OnUnload()
	{
		// Destroy texture importers
		HvDelete m_pPnmImporter;
		HvDelete m_pBmpImporter;
		HvDelete m_pPngImporter;
		HvDelete m_pGifImporter;

		// Destroy model importers
		HvDelete m_pObjImporter;

		g_Logger.LogInfo(LogPgImageFH(), "Unloaded image file handlers plugin");
		return true;
	}

	PluginSystem::IPlugin* LoadPlugin()
	{
		return HvNew Plugin();
	}
}
