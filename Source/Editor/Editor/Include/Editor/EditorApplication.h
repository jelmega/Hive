// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// EditorApplication.h: Editor application
#pragma once
#include "EditorPCH.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv::Editor {
	
	/**
	 * Editor application
	 */
	class HIVE_API EditorApplication final : public IApplication
	{
	public:
		/**
		 * Create an editor application
		 */
		EditorApplication();
		~EditorApplication();

	private:

		/**
		* Create the settings for the application
		* @return	Application settings
		*/
		ApplicationSettings CreateSettings() override final;
		/**
		* Initialize the application
		* @return	True if the initialization was succesful, false otherwise
		*/
		b8 Init() override final;
		/**
		* Tick/update the application
		*/
		void Tick() override final;
		/**
		* Additional drawing for the application
		*/
		void Draw() override final;
		/**
		* Shutdown the application
		* @return	True if the shutdown was succesful, false otherwise
		*/
		b8 Shutdown() override final;

		// FPS
		u32 m_TotalFrameCount;
		u32 m_FrameCount;
		u32 m_Fps;
		f32 m_TimePassed;
		Timer m_Timer;
		String m_BaseTitle;

		// TEMP
		b8 InitSizeDependent();
		b8 DestroySizeDependent();
		void ResizeCallback(u32 width, u32 height);

		Renderer::Core::Shader* m_pVertexShader;
		Renderer::Core::Shader* m_pFragmentShader;
		Renderer::Core::RenderPass* m_pRenderPass;
		Renderer::Core::Pipeline* m_pPipeline;
		Renderer::Core::RenderTarget* m_pDepthStencils[3];
		Renderer::Core::Framebuffer* m_pFramebuffers[3];
		Renderer::Core::CommandList* m_pCommandLists[3];
		Renderer::Core::Buffer* m_pVertexBuffer;
		Renderer::Core::Buffer* m_pIndexBuffer;
		Renderer::Core::Buffer* m_pUniformBuffer;
		Renderer::Core::DescriptorSet* m_pUniformDescSet;
		Renderer::Core::Texture* m_pTexture;
		Renderer::Core::Sampler* m_pSampler;

		Image* m_pImage;
		MeshHandle m_Mesh;

		// New temp
		Renderer::MaterialHandle m_Material;
		Renderer::MaterialInstanceHandle m_MatInstance;
	};

	// DynLib Interface
#if __cplusplus
	extern "C"
	{
#endif
		/**
		* Generate the application for the engine to use
		* @return	Generated application
		*/
		HIVE_API IApplication* LoadApplication();

#if __cplusplus
	}
#endif

}

#pragma warning(pop)