// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// EditorApplication.h: Editor application
#include "EditorPCH.h"
#include "Editor/EditorApplication.h"
#include "ECS/ComponentId.h"
#include "Renderer/Material/Material.h" 

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

HV_DECLARE_LOG_CATEGORY(Editor, Hv::LogLevel::All);

namespace Hv::Editor {

	EditorApplication::EditorApplication()
		: IApplication()
		, m_TotalFrameCount(0)
		, m_FrameCount(0)
		, m_Fps(0)
		, m_TimePassed(0.f)
	{
		m_pImage = nullptr;
	}

	EditorApplication::~EditorApplication()
	{
	}

	ApplicationSettings EditorApplication::CreateSettings()
	{
		ApplicationSettings settings;

		WindowDesc& windowDesc = settings.mainWindowDesc;
		windowDesc.width = 1024;
		windowDesc.height = 1024;
		windowDesc.monitor = Move(Monitor(0));
		windowDesc.x = HV_WINDOW_CENTERED;
		windowDesc.y = HV_WINDOW_CENTERED;
		windowDesc.title = "Hive Editor v0.1.0 (alpha)";
		windowDesc.style = WindowStyle::Default;

		Renderer::RendererDesc& renderDesc = settings.renderDesc;
		renderDesc.rhiType = Renderer::RHIType::Vulkan;
		renderDesc.rhiValidationLevel = Renderer::RHIValidationLevel::Warning;
		renderDesc.defaultVSyncMode = Renderer::VSyncMode::Tripple;
		renderDesc.renderingMode = Renderer::RenderingMode::Forward;

		return settings;
	}

	// TEMP

	/*struct Vertex
	{
		f32v3 position;
		f32v3 color;
		f32v2 texcoord;
	};

	Array<Vertex, 8> vertices = {
		/*{ {  0.5f, -0.5f, 0.f },{ 1.0f, 0.0f, 0.0f },{ 1.f, 0.f } },
		{ { -0.5f, -0.5f, 0.f },{ 0.0f, 1.0f, 0.0f },{ 0.f, 0.f } },
		{ { -0.5f,  0.5f, 0.f },{ 0.0f, 0.0f, 1.0f },{ 0.f, 1.f } },
		{ {  0.5f,  0.5f, 0.f },{ 1.0f, 1.0f, 1.0f },{ 1.f, 1.f } }*//*

		{ { -0.5f, -0.5f,  0.0f },{ 1.0f, 0.0f, 0.0f },{ 0.0f, 1.0f } },
		{ {  0.5f, -0.5f,  0.0f },{ 0.0f, 1.0f, 0.0f },{ 1.0f, 1.0f } },
		{ {  0.5f,  0.5f,  0.0f },{ 0.0f, 0.0f, 1.0f },{ 1.0f, 0.0f } },
		{ { -0.5f,  0.5f,  0.0f },{ 1.0f, 1.0f, 1.0f },{ 0.0f, 0.0f } },

		{ { -0.5f, -0.5f, -0.5f },{ 1.0f, 0.0f, 0.0f },{ 0.0f, 1.0f } },
		{ {  0.5f, -0.5f, -0.5f },{ 0.0f, 1.0f, 0.0f },{ 1.0f, 1.0f } },
		{ {  0.5f,  0.5f, -0.5f },{ 0.0f, 0.0f, 1.0f },{ 1.0f, 0.0f } },
		{ { -0.5f,  0.5f, -0.5f },{ 1.0f, 1.0f, 1.0f },{ 0.0f, 0.0f } }
	};

	Array<u16, 12> indices = {
		/*0, 1, 2,
		2, 3, 0*//*

		0, 1, 2, 2, 3, 0,
		4, 5, 6, 6, 7, 4
	};*/

	struct UBO
	{
		f32m4 model;
		f32m4 view;
		f32m4 proj;
	} ubo;

	// END TEMP

	b8 EditorApplication::Init()
	{
		glm::mat4 mat = glm::lookAtLH(glm::vec3(-1, 1, -2), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

		HvFS::Mount("shaders", "$res/Shaders");
		HvFS::Mount("tex", "$res/Textures");
		HvFS::Mount("model", "$res/Models");

		// Start timer for fps counter
		m_Timer.Start();

		m_BaseTitle = g_WindowManager.GetMainWindow()->GetTitle();

		// Tempory Code Begin

		/*m_pMesh = (Mesh*)g_AssetManager.Load("$model/obj/chalet.obj");  

		// General
		Window* pWindow = g_WindowManager.GetMainWindow();
		Renderer::Core::RenderView* pRenderView = g_Renderer.GetRenderView(pWindow->GetId());

		// Shaders
		Renderer::Core::ShaderDesc vertexShader = { Renderer::ShaderType::Vertex, "$shaders/vert.spv", "main", Renderer::ShaderLanguage::Spirv };
		Renderer::Core::ShaderDesc fragmentShader = { Renderer::ShaderType::Fragment, "$shaders/frag.spv", "main", Renderer::ShaderLanguage::Spirv };

		m_pVertexShader = g_Renderer.CoreAddShader(vertexShader);
		m_pFragmentShader = g_Renderer.CoreAddShader(fragmentShader);

		{
			DynArray<Renderer::Core::DescriptorSetBinding> bindings;

			Renderer::Core::DescriptorSetBinding binding;
			binding.count = 1;
			binding.type = Renderer::DescriptorSetBindingType::Uniform;
			binding.shadertype = Renderer::ShaderType::Vertex;
			bindings.Push(binding);

			binding.type = Renderer::DescriptorSetBindingType::CombinedImageSampler;
			binding.shadertype = Renderer::ShaderType::Fragment;
			bindings.Push(binding);

			m_pUniformDescSet = g_Renderer.CoreAddDescriptorSet(bindings);
		}

		InitSizeDependent();

		// command list queue is the same as the present queue for now
		Renderer::Core::Queue* pQueue = pRenderView->GetPresentQueue();

		for (u32 i = 0; i < 3; ++i)
		{
			m_pCommandLists[i] = g_Renderer.CoreAddCommandList(pQueue);
		}

		//m_pVertexBuffer = g_Renderer.CoreAddVertexBuffer(u32(vertices.Size()), sizeof(Vertex), Renderer::BufferFlags::None);
		//m_pVertexBuffer->Write(0, vertices.Size() * sizeof(Vertex), vertices.Data());
		DynArray<Vertex>& vertices = m_pMesh->GetVertices();
		m_pVertexBuffer = g_Renderer.CoreAddVertexBuffer(u32(vertices.Size()), sizeof(Vertex), Renderer::BufferFlags::None);
		m_pVertexBuffer->Write(0, vertices.Size() * sizeof(Vertex), vertices.Data());

		//m_pIndexBuffer = g_Renderer.CoreAddIndexuffer(u32(indices.Size()), Renderer::IndexType::UShort, Renderer::BufferFlags::Static);
		//m_pIndexBuffer->Write(0, indices.Size() * sizeof(u16), indices.Data());
		DynArray<u32>& indices = m_pMesh->GetIndices();
		m_pIndexBuffer = g_Renderer.CoreAddIndexuffer(u32(indices.Size()), Renderer::IndexType::UInt, Renderer::BufferFlags::Static);
		m_pIndexBuffer->Write(0, indices.Size() * sizeof(u32), indices.Data());

		ubo.model = f32m4::Identity;
		ubo.view = f32m4::Identity;
		ubo.proj = f32m4::Identity;

		m_pUniformBuffer = g_Renderer.CoreAddBuffer(Renderer::BufferType::Uniform, sizeof(UBO), Renderer::BufferFlags::Dynamic);
		m_pUniformBuffer->Write(0, sizeof(UBO), &ubo);
		m_pUniformDescSet->Write(0, m_pUniformBuffer);

		// Image
		m_pImage = (Image*)g_AssetManager.Load("$tex/png/chalet.png");
		if (!m_pImage)
			return false;

		m_pTexture = g_Renderer.CoreAddTexture(m_pImage, Renderer::SampleCount::Sample1, Renderer::TextureFlags::Static, Renderer::TextureLayout::ShaderReadOnly, Renderer::QueueType::Graphics);


		Renderer::Core::SamplerDesc samplerDesc = {};
		samplerDesc.SetFilter(Renderer::FilterMode::Nearest);
		samplerDesc.SetAddressMode(Renderer::AddressMode::Repeat);
		samplerDesc.enableAnisotropy = true;
		samplerDesc.anisotropy = 16;
		samplerDesc.borderColor = f32v4(0, 0, 0, 1);
		m_pSampler = g_Renderer.CoreAddSampler(samplerDesc);

		m_pUniformDescSet->Write(1, m_pTexture, m_pSampler);


		pWindow->SetOnUserResizeEndedCallback(Delegate<void(u32, u32)>(this, &EditorApplication::ResizeCallback));
		pWindow->SetOnMaximizedCallback(Delegate<void(u32, u32)>(this, &EditorApplication::ResizeCallback));
		pWindow->SetOnMinimizedCallback(Delegate<void(u32, u32)>(this, &EditorApplication::ResizeCallback));
		pWindow->SetOnRestoredCallback(Delegate<void(u32, u32)>(this, &EditorApplication::ResizeCallback));*/

		// New temp stuff
		m_Mesh = g_AssetManager.LoadMesh("$model/obj/chalet.obj", "Chalet");
		g_Renderer.RegisterMesh(m_Mesh);

		// Shaders
		Renderer::Core::ShaderDesc vertexShader = { Renderer::ShaderType::Vertex, "$shaders/Forward_vert.spv", "main", Renderer::ShaderLanguage::Spirv };
		Renderer::Core::ShaderDesc fragmentShader = { Renderer::ShaderType::Fragment, "$shaders/Forward_frag.spv", "main", Renderer::ShaderLanguage::Spirv };

		m_pVertexShader = g_Renderer.CoreAddShader(vertexShader);
		m_pFragmentShader = g_Renderer.CoreAddShader(fragmentShader);

		Renderer::MaterialDesc matDesc = {};

		Renderer::MaterialParam param = {};
		param.name = "Texture";
		param.type = Renderer::MaterialParamType::CombinedImageSampler;
		param.shadertype = Renderer::ShaderType::Fragment;
		param.arrayElements = 1;
		matDesc.parameters.Push(param);

		matDesc.shaders.pVertexShader = m_pVertexShader;
		matDesc.shaders.pFragmentShader = m_pFragmentShader;

		matDesc.domain = Renderer::MaterialDomain::Surface;
		matDesc.blendMode = Renderer::MaterialBlendMode::Opaque;
		matDesc.shadingMode = Renderer::MaterialShadingMode::Unlit;

		Renderer::MaterialManager* pMaterialManager = g_Renderer.GetMaterialManager();
		m_Material = pMaterialManager->CreateMaterial("M_General", matDesc);
		m_MatInstance = pMaterialManager->CreateMaterialInstance("MI_General", m_Material);


		AssetHandle imageHandle = g_AssetManager.Load("$tex/pnm/chalet.ppm", "Chalet_tex");
		if (!imageHandle.IsValid())
			return false;
		m_pImage = (Image*)imageHandle.Get();

		m_pTexture = g_Renderer.CoreAddTexture(m_pImage, Renderer::SampleCount::Sample1, Renderer::TextureFlags::Static, Renderer::TextureLayout::ShaderReadOnly, Renderer::QueueType::Graphics);


		Renderer::Core::SamplerDesc samplerDesc = {};
		samplerDesc.SetFilter(Renderer::FilterMode::Nearest);
		samplerDesc.SetAddressMode(Renderer::AddressMode::Repeat);
		samplerDesc.enableAnisotropy = true;
		samplerDesc.anisotropy = 16;
		samplerDesc.borderColor = f32v4(0, 0, 0, 1);
		samplerDesc.maxLod = 1.f;
		m_pSampler = g_Renderer.CoreAddSampler(samplerDesc);

		m_MatInstance->SetParam("Texture", m_pTexture, m_pSampler);

		//g_Renderer.AddRenderable(m_pMesh, m_MatInstance);

		// Chalet entity
		ECS::Entity ent = g_ECS.CreateEntity();

		TransformComponent transform;
		//transform.position.y = -1;
		transform.rotation = f32q::CreateAxisAngle(f32v3::AxisX, Hm::ToRadians(-90.f)) * f32q::CreateAxisAngle(f32v3::AxisY, Hm::ToRadians(90.f));
		g_ECS.AddComponent(ent, transform);

		MeshComponent meshComp;
		meshComp.mesh = m_Mesh;
		meshComp.material = m_MatInstance;
		g_ECS.AddComponent(ent, meshComp);


		// Tempory Code End

		return true;
	}

	void EditorApplication::Tick()
	{
		// Update FPS
		++m_FrameCount;
		++m_TotalFrameCount;

		f32 dt = f32(m_Timer.Reset().AsSeconds());
		m_TimePassed += dt;
		if (m_TimePassed > .5f)
		{
			m_Fps = m_FrameCount * 2;
			m_FrameCount = 0;
			m_TimePassed = 0.f;

			String title; 
			title.FormatF(String("%s (FPS: %u, Total: %u)"), m_BaseTitle, m_Fps, m_TotalFrameCount);
			g_WindowManager.GetMainWindow()->SetTitle(title);
		}

		// Update ubo
		/*ubo.model = f32m4::CreateRotation(f32q::CreateAxisAngle(f32v3::Left, Math::ToRadians(90.f))) * (f32m4::CreateTranslation(0, 0, .5f) * f32m4::CreateRotation(f32q::CreateAxisAngle(f32v3::Up, Hm::ToRadians(f32(m_TotalFrameCount % 14400) * 0.025f))));

		ubo.view = f32m4::CreateLookat(f32v3(0, 1, 2), f32v3(0, 0, 0), f32v3(0, 1, 0));
		ubo.view *= f32m4(1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); // Vulkan correction

		ubo.proj = f32m4::CreatePerspective(Math::ToRadians(90.f), 1.f, 0.01f, 1000.f);

		m_pUniformBuffer->Write(0, sizeof(UBO), &ubo);

		Draw();*/
	}

	void EditorApplication::Draw()
	{
		Window* pWindow = g_WindowManager.GetMainWindow();
		if (!pWindow)
			return;
		if (pWindow->IsMinimized())
			return;
		Renderer::Core::SwapChain* pRenderView = g_Renderer.GetSwapChain(pWindow->GetId());
		if (!pRenderView)
			return;

		u32 index = pRenderView->GetCurrentIndex();

		// Record command buffer
		Renderer::Core::CommandList* pCommandList = m_pCommandLists[index];

		if (pCommandList->GetState() != Renderer::CommandListState::Finished)
		{
			pCommandList->Wait();
		}

		pCommandList->Begin();

		pCommandList->BeginRenderPass(m_pRenderPass, m_pFramebuffers[index]);
		pCommandList->BindPipeline(m_pPipeline);
		pCommandList->BindVertexBuffer(0, m_pVertexBuffer, 0);
		pCommandList->BindIndexBuffer(m_pIndexBuffer, 0);

		pCommandList->BindDescriptorSets(0, m_pUniformDescSet);
		pCommandList->DrawIndexed(m_pIndexBuffer->GetIndexCount());
		pCommandList->EndRenderPass();

		pCommandList->End();

		DynArray<Renderer::Core::Semaphore*> waitSemaphores = { pRenderView->GetSignalSemaphore() };
		DynArray<Renderer::PipelineStage> waitStages = { Renderer::PipelineStage::ColorAttachmentOutput };
		DynArray<Renderer::Core::Semaphore*> signalSemaphores = { pRenderView->GetWaitSemaphore() };
		pCommandList->Submit(waitSemaphores, waitStages, signalSemaphores);

		pRenderView->Present();
	}

	b8 EditorApplication::Shutdown()
	{
		// Tempory Code Begin
		/*DestroySizeDependent();
		g_Renderer.CoreRemoveShader(m_pVertexShader);
		g_Renderer.CoreRemoveShader(m_pFragmentShader);

		if (m_pImage)
			HvDelete m_pImage;*/

		// Tempory Code End

		return true;
	}

	b8 EditorApplication::InitSizeDependent()
	{
		Window* pWindow = g_WindowManager.GetMainWindow();
		if (pWindow->IsMinimized())
			return true;

		Renderer::Core::SwapChain* pRenderView = g_Renderer.GetSwapChain(pWindow->GetId());

		// Render pass
		DynArray<Renderer::Core::RenderPassAttachment> attachments;

		Renderer::Core::RenderPassAttachment attachment = {};
		attachment.format = pRenderView->GetCurrentRenderTarget()->GetTexture()->GetFormat();
		attachment.type = Renderer::RenderTargetType::Presentable;
		attachment.samples = Renderer::SampleCount::Sample1;
		attachment.storeOp = Renderer::StoreOp::Store;
		attachment.loadOp = Renderer::LoadOp::Clear;
		attachments.Push(attachment);

		Renderer::Core::RenderPassAttachmentRef attachmentRef;
		attachmentRef.index = 0;
		attachmentRef.type = Renderer::RenderTargetType::Presentable;

		Renderer::Core::SubRenderPass subpass;
		subpass.attachments.Push(attachmentRef);

		attachment.format = PixelFormat(PixelFormatComponents::D32, PixelFormatTransform::SFLOAT);
		attachment.type = Renderer::RenderTargetType::DepthStencil;
		attachment.samples = Renderer::SampleCount::Sample1;
		attachment.storeOp = Renderer::StoreOp::DontCare;
		attachment.loadOp = Renderer::LoadOp::Clear;
		attachments.Push(attachment);

		attachmentRef.index = 1;
		attachmentRef.type = Renderer::RenderTargetType::DepthStencil;
		subpass.attachments.Push(attachmentRef);

		DynArray<Renderer::Core::SubRenderPass> subpasses;
		subpasses.Push(subpass);

		m_pRenderPass = g_Renderer.CoreAddRenderPass(attachments, subpasses);

		Renderer::Core::GraphicsPipelineDesc pipelineDesc = {};
		pipelineDesc.viewport.x = 0.f;
		pipelineDesc.viewport.y = 0.f;
		pipelineDesc.viewport.width = f32(pWindow->GetWidth());
		pipelineDesc.viewport.height = f32(pWindow->GetHeight());
		pipelineDesc.viewport.minDepth = 0.f;
		pipelineDesc.viewport.maxDepth = 1.f;
		pipelineDesc.scissor.x = 0;
		pipelineDesc.scissor.y = 0;
		pipelineDesc.scissor.width = pWindow->GetWidth();
		pipelineDesc.scissor.height = pWindow->GetHeight();

		pipelineDesc.pVertexShader = m_pVertexShader;
		pipelineDesc.pFragmentShader = m_pFragmentShader;

		// Vertex Layout
		/*Renderer::InputDescriptor inputDesc;
		inputDesc.Append(Renderer::InputElementDesc(Renderer::InputSemantic::Position, 0, Renderer::InputElementType::Float3, 0, 0, 0));
		inputDesc.Append(Renderer::InputElementDesc(Renderer::InputSemantic::Color, 0, Renderer::InputElementType::Float3, 3 * sizeof(f32), 0, 0));
		inputDesc.Append(Renderer::InputElementDesc(Renderer::InputSemantic::TexCoord, 0, Renderer::InputElementType::Float2, 6 * sizeof(f32), 0, 0));*/
		pipelineDesc.inputDescriptor = m_Mesh->GetInputDescriptor();

		Renderer::BlendAttachment blendAttachment = {};
		blendAttachment.components = Renderer::ColorComponentMask::R | Renderer::ColorComponentMask::G | Renderer::ColorComponentMask::B | Renderer::ColorComponentMask::A;
		blendAttachment.enable = false;
		pipelineDesc.blendState.attachments.Push(blendAttachment);

		pipelineDesc.rasterizer.fillMode = Renderer::FillMode::Solid;
		pipelineDesc.rasterizer.cullMode = Renderer::CullMode::None;

		pipelineDesc.primitiveTopology = Renderer::PrimitiveTopology::Triangle;

		pipelineDesc.pRenderPass = m_pRenderPass;

		pipelineDesc.descriptorSetLayouts.Push(m_pUniformDescSet->GetLayout());

		Renderer::DepthStencilDesc& depthStencil = pipelineDesc.depthStencil;
		depthStencil.enableDepthWrite = true;
		depthStencil.enableDepthTest = true;
		depthStencil.depthCompareOp = Renderer::CompareOp::Less;

		m_pPipeline = g_Renderer.CoreAddPipeline(pipelineDesc);



		Renderer::Core::RenderTargetDesc depthStencilRTDesc = {};
		depthStencilRTDesc.width = pRenderView->GetCurrentRenderTarget()->GetWidth();
		depthStencilRTDesc.height = pRenderView->GetCurrentRenderTarget()->GetHeight();
		depthStencilRTDesc.format = PixelFormat(PixelFormatComponents::D32, PixelFormatTransform::SFLOAT);
		depthStencilRTDesc.type = Renderer::RenderTargetType::DepthStencil;

		for (u32 i = 0; i < 3; ++i)
		{
			m_pDepthStencils[i] = g_Renderer.CoreAddRenderTarget(depthStencilRTDesc);
			m_pDepthStencils[i]->SetClearColor(Renderer::Core::ClearValue(1.f, 0));

			Renderer::Core::RenderTarget* pRenderTarget = pRenderView->GetRenderTarget(i);

			DynArray<Renderer::Core::RenderTarget*> renderTargets = { pRenderTarget, m_pDepthStencils[i] };
			m_pFramebuffers[i] = g_Renderer.CoreAddFramebuffer(renderTargets, m_pRenderPass);
		}

		return true;
	}

	b8 EditorApplication::DestroySizeDependent()
	{
		// no render pass, so window was minimized
		if (!m_pRenderPass)
			return true;

		// Remove framebuffer, pipeline and render pass
		for (u32 i = 0; i < 3; ++i)
		{
			g_Renderer.CoreRemoveFramebuffer(m_pFramebuffers[i]);
			m_pFramebuffers[i] = nullptr;

			g_Renderer.CoreRemoveRenderTarget(m_pDepthStencils[i]); 
			m_pDepthStencils[i] = nullptr;
		}

		g_Renderer.CoreRemovePipeline(m_pPipeline);
		m_pPipeline = nullptr;

		g_Renderer.CoreRemoveRenderPass(m_pRenderPass);
		m_pRenderPass = nullptr;

		return true;
	}

	void EditorApplication::ResizeCallback(u32 width, u32 height)
	{
		DestroySizeDependent();
		InitSizeDependent();
	}

	IApplication* LoadApplication()
	{
		return HvNew EditorApplication();
	}
}

