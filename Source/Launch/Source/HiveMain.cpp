// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HiveMain.cpp: Hive main entry point
// TODO: Correct configurations other than Debug x64
#include "Core.h"
#include "Engine.h"
#include "Engine/EngineLoop.h"

i32 HiveMain(Hv::SystemHandle pSysHandle, const Hv::String& cmdLine)
{
	Hv::FileSystem::Mount("bin", HvFS::Path("D:/Projects/Hive/Binaries/x64/Debug"));
	HvFS::Path editorPath("$bin/HiveEditor");
	editorPath += Hv::DynLib::g_Extension;
	editorPath = HvFS::ResolvePath(editorPath);
	Hv::DynLib::DynLibHandle editorHandle = Hv::DynLib::Load(editorPath.ToString());
	if (editorHandle == Hv::DynLib::InvalidDynLibHandle)
	{
		g_Logger.LogFormat(Hv::LogLevel::Fatal, "Failed to load application (path: %s)", editorPath.ToString());
		return -1;
	}

	Hv::LoadApplicationFunc fGenApp = (Hv::LoadApplicationFunc)Hv::DynLib::GetProc(editorHandle, "LoadApplication");

	Hv::Engine::EngineLoop engineLoop;
	engineLoop.PreInit(fGenApp(), cmdLine, pSysHandle);
	i32 ret = engineLoop.Run();

	Hv::DynLib::Unload(editorHandle);

	return 0;
}
