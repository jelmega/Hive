// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// WindowsLaunch.cpp: Windows main functions

#include "Core/CoreHeaders.h"
#include "HAL/HalSystem.h"
#include "String/String.h"

#if HV_PLATFORM_WINDOWS

extern i32 HiveMain(Hv::SystemHandle, const Hv::String& cmdLine);

int main(int argc, char* argv[])
{
	Hv::AnsiString str;
	for (i32 i = 0; i < argc; ++i)
	{
		if (i > 0)
			str.Append(' ');
		str.Append(argv[i]);
	}

	return WinMain(GetModuleHandleW(nullptr), nullptr, str.CStr(), SW_SHOW);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	HV_UNREFERENCED_PARAM(hInstance);
	HV_UNREFERENCED_PARAM(hPrevInstance);
	HV_UNREFERENCED_PARAM(lpCmdLine);
	HV_UNREFERENCED_PARAM(nCmdShow);

	return HiveMain((Hv::SystemHandle)hInstance, Hv::String(lpCmdLine));
}


#endif