﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTIGen
{
	public interface IToken
	{
	}

	public class NamespaceToken : IToken
	{
		public string Namespace;
	}

	public class EndNamespaceToken : IToken
	{
	}

	public struct TemplateParam
	{
		public string Type;
		public string Name;
	}

	public class ClassToken : IToken
	{
		public string Name = "";
		public ClassFlags Flags = ClassFlags.None;
		public List<TemplateParam> TemplateMetaData = new List<TemplateParam>();
		public List<string> Bases = new List<string>();
	}

	public class EndClassToken : IToken
	{
	}

	public class StructToken : IToken
	{
		public string Name = "";
		public StructFlags Flags = StructFlags.None;
		public List<TemplateParam> TemplateMetaData = new List<TemplateParam>();
		public List<string> Bases = new List<string>();
	}

	public class EndStructToken : IToken
	{
	}

	public class PropertyToken : IToken
	{
		public PropertyFlags Flags;
		public string Type = "";
		public string Name = "";
	}
}
