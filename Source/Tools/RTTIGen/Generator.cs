﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace RTTIGen
{
    public static class Generator
    {
	    public static RTTIData GenerateRTTI(IToken[] tokens)
	    {
		    Stack<string> namespaces = new Stack<string>();
			RTTIData data = new RTTIData();

		    for (int i = 0; i < tokens.Length; ++i)
		    {
			    IToken token = tokens[i];
			    Type type = token.GetType();
			    switch (type.Name)
			    {
				    case "NamespaceToken":
				    {
						namespaces.Push(((NamespaceToken)token).Namespace);

					    break;
				    }
				    case "EndNamespaceToken":
				    {
					    namespaces.Pop();
					    break;
				    }
				    case "ClassToken":
				    {
					    RTTIClass rtti = ProcessClass(tokens, namespaces, ref i);
						data.Classes.Add(rtti);
					    break;
				    }
				    case "StructToken":
				    {
					    RTTIStruct rtti = ProcessStruct(tokens, namespaces, ref i);
					    data.Structs.Add(rtti);
					    break;
				    }
					default:
						break;
			    }
		    }
			
		    return data;
	    }

		// Generate identification hash
	    public static UInt64 GenerateHash(string s)
	    {
		    byte[] content = Encoding.Unicode.GetBytes(s);
			System.Security.Cryptography.SHA256 sha = new SHA256CryptoServiceProvider();
		    byte[] hashText = sha.ComputeHash(content);

		    UInt64 start = BitConverter.ToUInt64(hashText, 0);
		    UInt64 mid = BitConverter.ToUInt64(hashText, 8);
		    UInt64 end = BitConverter.ToUInt64(hashText, 24);

			return start ^ mid ^ end;
	    }

	    public static RTTIClass ProcessClass(IToken[] tokens, Stack<string> namespaces, ref int idx)
	    {
		    ClassToken mainToken = (ClassToken)tokens[idx];

			RTTIClass rtti = new RTTIClass();
		    foreach (string ns in namespaces)
		    {
			    if (rtti.Namespace.Length > 0)
				    rtti.Namespace += ns;
			    rtti.Namespace += ns;
		    }

		    rtti.Name = mainToken.Name;
		    rtti.FullName = rtti.Namespace + "::" + rtti.Name;
		    rtti.Hash = GenerateHash(rtti.FullName);

			// Process tokens before "EndClassToken
		    IToken token = mainToken;
		    while (token.GetType().Name != "EndClassToken")
		    {
			    token = tokens[++idx];
		    }

		    return rtti;
	    }

	    public static RTTIStruct ProcessStruct(IToken[] tokens, Stack<string> namespaces, ref int idx)
	    {
		    StructToken mainToken = (StructToken)tokens[idx];

		    RTTIStruct rtti = new RTTIStruct();
		    foreach (string ns in namespaces)
		    {
			    if (rtti.Namespace.Length > 0)
				    rtti.Namespace += ns;
			    rtti.Namespace += ns;
		    }

		    rtti.Name = mainToken.Name;
		    rtti.FullName = rtti.Namespace + "::" + rtti.Name;
		    rtti.Hash = GenerateHash(rtti.FullName);
		    rtti.Flags = mainToken.Flags;

			// template params
		    foreach (TemplateParam template in mainToken.TemplateMetaData)
		    {
			    RTTITemplateParam param = new RTTITemplateParam();
			    param.Type = template.Type;
			    param.Name = template.Name;
			    rtti.TemplateParams.Add(param);

			}

		    // Process tokens before "EndClassToken
		    IToken token = mainToken;
		    while (token.GetType().Name != "EndStructToken")
		    {
			    token = tokens[++idx];
		    }

		    return rtti;
	    }

		// Convert data to header file
		public static string GenerateHeader(RTTIData data, string fileName, string moduleName)
	    {
		    string header =  "// Copyright 2018 Jelte Meganck. All Rights Reserved.\n" +
		                     "//\n" +
		                    $"// Generated RTTI data for {fileName}\n" +
							 "// Do not manually modify this file\n" +
							 "#pragma once\n" +
							 "" +
							 "#pragma warning(push)\n" +
							 "#pragma warning(disable: 4307) // '...': integral constant overflow (for RTTIHashCombine)\n";

		    if (moduleName == "Core") // Core needs to have "" style include
		    {
			    header += "#include \"RTTI/RTTI.h\"\n" +
			                    "\n";
		    }
		    else
		    {
			    header += "#include <RTTI/RTTI.h>\n" +
			                    "\n";
		    }

			foreach (RTTIClass dc in data.Classes)
			{
				string classText = $"namespace {dc.Namespace}\n" +
				                    "{\n"+
				                   $"\tclass {dc.Name};\n" +
									"}\n" +
			                      	"namespace Hv\n" +
			                      	"{\n" +
			                      	"\ttemplate<>\n" +
									$"\tclass RTTI<::{dc.FullName}>\n" + 
									 "\t{\n" +
									 "\tpublic:\n" +
									$"\t\tstatic constexpr const AnsiChar* Name = \"{dc.Name}\";\n" +
									$"\t\tstatic constexpr const AnsiChar* FullName = \"{dc.FullName}\";\n" +
									$"\t\tstatic constexpr const AnsiChar* Namespace = \"{dc.Namespace}\";\n" +
									$"\t\tstatic constexpr u64 Hash = {dc.Hash};\n" +
									 "\t\tstatic constexpr b8 IsClass = true;\n" +
									 "" +
									 "\t};\n" +
									 "}\n";


			    header += classText;
		    }

		    foreach (RTTIStruct ds in data.Structs)
		    {
			    string rttiTemplate = "template<>";
			    string templateStr = "";
				string templateSpec = "";
			    string hash = $"{ds.Hash}";
			    if (ds.TemplateParams.Count > 0)
			    {
				    templateStr = "template<";
				    templateSpec = "<";


					bool comma = false;
				    foreach (RTTITemplateParam template in ds.TemplateParams)
				    {
					    if (comma)
					    {
						    templateStr += ", ";
						    templateSpec += ", ";

					    }

					    templateStr += $"{template.Type} {template.Name}";
					    templateSpec += template.Name;
						comma = true;

					    hash = $"RTTIHashCombine({hash}, RTTI<{template.Name}>::Hash)";
				    }

				    templateStr += ">";
				    templateSpec += ">";
				    rttiTemplate = templateStr;

				    templateStr = '\t' + templateStr + '\n';

			    }

				string structText = $"namespace {ds.Namespace}\n" +
				                    "{\n" +
								   $"{templateStr}" +
				                   $"\tstruct {ds.Name};\n" +
									"}\n" +
									"namespace Hv\n" +
			                        "{\n" +
			                       $"\t{rttiTemplate}\n" +
			                       $"\tclass RTTI<::{ds.FullName}{templateSpec}>\n" +
			                        "\t{\n" +
			                        "\tpublic:\n" +
			                       $"\t\tstatic constexpr const AnsiChar* Name = \"{ds.Name}\";\n" +
			                       $"\t\tstatic constexpr const AnsiChar* FullName = \"{ds.FullName}\";\n" +
			                       $"\t\tstatic constexpr const AnsiChar* Namespace = \"{ds.Namespace}\";\n" +
			                       $"\t\tstatic constexpr u64 Hash = {hash};\n" +
			                        "\t\tstatic constexpr b8 IsClass = false;\n" +
								   $"\t\tstatic constexpr RTTIStructFlags StructFlags = RTTIStructFlags({(uint)ds.Flags});\n" +
									"" +
			                        "\t};\n" +
			                        "}\n";


			    header += structText;
		    }

		    header += "#pragma warning(pop)";
			return header;

		}
    }
}
