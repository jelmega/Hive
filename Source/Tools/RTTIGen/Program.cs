﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RTTIGen
{
	// TODO: Extend functionality
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
                return;

	        string moduleDir = args[0];
	        if (!moduleDir.EndsWith('\\') && !moduleDir.EndsWith('/'))
		        moduleDir += '/';

	        string moduleName = "";
	        if (moduleDir.EndsWith('\\') || moduleDir.EndsWith('/'))
	        {
		        int pos = moduleDir.LastIndexOf('/', moduleDir.Length - 2);
		        if (pos == -1)
			        pos = moduleDir.LastIndexOf('\\', moduleDir.Length - 2);

		        if (pos == -1)
		        {
			        moduleName = moduleDir.Substring(0, moduleDir.Length - 1);
		        }
		        else
		        {
			        moduleName = moduleDir.Substring(pos + 1, moduleDir.Length - pos - 2);
		        }
	        }
	        else
	        {
		        int pos = moduleDir.LastIndexOf('/');
		        if (pos == -1)
			        pos = moduleDir.LastIndexOf('\\');

		        if (pos == -1)
		        {
			        moduleName = moduleDir;
		        }
		        else
		        {
			        moduleName = moduleDir.Substring(pos, moduleDir.Length - pos);
		        }
	        }

	        if (!Directory.Exists(moduleDir + "Generated/"))
		        Directory.CreateDirectory(moduleDir + "Generated/");

			List<string> generatedHeaders = new List<string>();

	        string[] files = Directory.EnumerateFiles(moduleDir + "Include/", "*.h", SearchOption.AllDirectories).ToArray();

			foreach (string filePath in files)
            {
	            string fileName = Path.GetFileName(filePath);
                string generatedPath = moduleDir + "Generated/" + fileName.Substring(0, fileName.Length - 1) + "generated.h";

	            IToken[] tokens;

				using (StreamReader reader = new StreamReader(filePath))
                {
	                tokens = Lexer.Tokenize(reader);
                }

	            if (tokens.Length == 0)
		            continue;

				// Check for any non namespace tokens
	            bool process = false;
	            foreach (IToken token in tokens)
	            {
		            Type type = token.GetType();
		            if (type.Name != "NamespaceToken" && type.Name != "EndNamespaceToken")
		            {
			            process = true;
						break;
		            }
	            }

	            if (!process)
		            continue;

				// Process tokens

	            RTTIData data = Generator.GenerateRTTI(tokens);
	            if (data == null)
		            continue;

	            string header = Generator.GenerateHeader(data, fileName, moduleName);

	            if (!string.IsNullOrWhiteSpace(header))
	            {
		            using (StreamWriter writer =
			            new StreamWriter(File.Open(generatedPath, FileMode.Create, FileAccess.Write)))
		            {
						writer.Write(header);
		            }
		            generatedHeaders.Add(Path.GetFileName(generatedPath));

				}
            }

			// Generate ''Module'.module.generated.h' for module

	        string moduleHeader = "// Copyright 2018 Jelte Meganck. All Rights Reserved.\n" +
	                              "//\n" +
	                              $"// Generate RTTI data for module: {moduleName}\n" +
	                              "// Do not manually modify this file\n" +
	                              "#pragma once\n";

	        if (moduleName == "Core") // Core needs to have "" style include
	        {
				moduleHeader += "#include \"RTTI/RTTI.h\"" +
				                "\n";
			}
	        else
	        {
		        moduleHeader += "#include <RTTI/RTTI.h>" +
		                        "\n";
			}
								  

		    foreach (string file in generatedHeaders)
		    {
			    moduleHeader += $"#include \"{file}\"\n";
		    }

		    string modulePath = moduleDir + "Generated/" + moduleName + ".module.generated.h";
			using (StreamWriter writer =
			     new StreamWriter(File.Open(modulePath, FileMode.Create, FileAccess.Write)))
		    {
			    writer.Write(moduleHeader);
		    }
			
        }
    } 
}
