﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTIGen
{
    public class RTTIData
    {
	    public List<RTTIClass> Classes = new List<RTTIClass>();
	    public List<RTTIStruct> Structs = new List<RTTIStruct>();
	}

	[Flags]
	public enum ClassFlags
	{
		None = 0x00000000,
	}

	public class RTTIClass
	{
		public string Name = "";
		public string FullName = "";
		public string Namespace = "";
		public UInt64 Hash = 0;
		public ClassFlags Flags;
	}

	[Flags]
	public enum StructFlags
	{
		None		= 0x00000000,
		Component	= 0x00000001,	// ECS component
		Event		= 0x00000002,	// ECS event
	}

	public class RTTIStruct
	{
		public string Name = "";
		public string FullName = "";
		public string Namespace = "";
		public UInt64 Hash = 0;
		public StructFlags Flags;

		public List<RTTITemplateParam> TemplateParams = new List<RTTITemplateParam>();
	}

	public class RTTITemplateParam
	{
		public string Type = "";
		public string Name = "";
	}

	[Flags]
	public enum PropertyFlags
	{
		None = 0x00000000,
	}
}
