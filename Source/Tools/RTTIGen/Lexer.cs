﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RTTIGen
{
	public class Lexer
	{
		private enum IndentType
		{
			None,
			Namespace,
			Class,
			Struct
		}

		// RTTI macros
		private struct Macros
		{
			public const string HvClass = "HV_CLASS";
			public const string HvStruct = "HV_STRUCT";
			public const string HvProperty = "HV_PROPERTY";
		}

		public static IToken[] Tokenize(StreamReader reader)
		{
			List<string> values = new List<string>();
			List<string> tmpList = new List<string>();
			Stack<IndentType> indents = new Stack<IndentType>();

			while (!reader.EndOfStream)
			{
				string line = reader.ReadLine();
				if (line == null)
					continue;
				line = line.Trim();

				bool define = line.StartsWith("#define");
				if (line.StartsWith("#")) // Skip # lines
					continue;

				string patern = "(?=[ \\t{}\\[\\]<>,();]|\\/\\*|\\*\\/)|(?<=[ \\t{}\\[\\]<>,();]|\\/\\*|\\*\\/)";
				string[] split = Regex.Split(line, patern);
				// trim spaces, tabs and single line comment + handle defines (since these can contain the struct or class keyword!)
				string prev = "";
				foreach (string s in split)
				{
					if (s == "//")
					{
						if (define && prev != "\\")
								define = false;
						break;
					}

					if (!string.IsNullOrWhiteSpace(s) && !define)
						tmpList.Add(s.Trim());

					prev = s;
				}

				if (define && prev != "\\")
					define = false;

				if (tmpList.Count > 0)
				{
					values.AddRange(tmpList.ToArray());
					tmpList.Clear();
				}
			}

			// Remove multi line comments
			for (int i = 0; i < values.Count; ++i)
			{
				string val = values[i];
				if (val == "/*")
				{
					val = values[++i];
					while (val != "*/")
					{
						val = values[++i];
					}
				}
				else
				{
					tmpList.Add(val);
				}
			}

			values = tmpList;

			List<IToken> tokens = new List<IToken>();
			// Process tokens
			for (int i = 0; i < values.Count; ++i)
			{
				string val = values[i];

				switch (val)
				{
					case "namespace": // namespace
					{
						if (i > 0 && values[i - 1] == "using")
							break;

						val = values[++i];

						NamespaceToken token = new NamespaceToken();
						bool validToken = true;
						while (val != "{")
						{
							if (val == "=")
							{
								while (val != "=")
								{
									val = values[++i];
									validToken = false;
								}

								break;
							}

							if (string.IsNullOrWhiteSpace(token.Namespace))
							{
								token.Namespace += val;
							}
							else
							{
								token.Namespace += "::" + val;
							}

							if (i < values.Count - 1)
								val = values[++i];
						}

						if (validToken)
						{
							tokens.Add(token);
							indents.Push(IndentType.Namespace);
						}

						break;
					}
					case Macros.HvClass: // class macro
					{
						ClassToken token = ParseClassToken(values, ref val, ref i);
						// Add tokens
						tokens.Add(token);
						indents.Push(IndentType.Class);
						break;
					}
					case Macros.HvStruct: // struct macro
					{
						StructToken token = ParseStructToken(values, ref val, ref i);
						// Add tokens
						tokens.Add(token);
						indents.Push(IndentType.Struct);
						break;
					}
					case Macros.HvProperty: // class macro
					{
						PropertyToken token = ParsePropertyToken(values, ref val, ref i);
						// Add tokens
						tokens.Add(token);
						break;
					}
					case "template": // skip template (skip to correct '>')
					{
						bool end = false;
						byte indentCount = 1;
						val = values[++i];

						// Find the first {
						while (val != "<")
						{
							val = values[++i];
						}

						while (!end)
						{
							val = values[++i];

							if (val == "<")
								++indentCount;
							else if (val == ">")
								--indentCount;

							if (indentCount == 0)
								end = true;
						}

						goto case "class";
					}
					case "struct": // skip struct
					case "class": // skip struct
					{
						bool end = false;
						byte indentCount = 1;
						val = values[++i];
						int di = i;

						// Find the first {
						while (val != "{" && val != ";")
						{
							val = values[++i];
						}

						if (val == ";")
							break;

						while (!end)
						{
							val = values[++i];

							if (val == "{")
								++indentCount;
							else if (val == "}")
								--indentCount;

							if (indentCount == 0)
								end = true;
						}

						break;
					}
					case "}":
					{
						switch (indents.Peek())
						{
							case IndentType.Namespace:
								tokens.Add(new EndNamespaceToken());
								break;
							case IndentType.Class:
								tokens.Add(new EndClassToken());
								break;
							case IndentType.Struct:
								tokens.Add(new EndStructToken());
								break;
							}

						indents.Pop();
						break;
					}
					case "{":
					{
						indents.Push(IndentType.None);
						break;
					}
				}
			}

			return tokens.ToArray();
		}

		public static ClassToken ParseClassToken(List<string> values, ref string val, ref int idx)
		{
			// Parse flags
			val = values[++idx];
			while (val != "(")
			{
				val = values[++idx]; // Should not happen
			}

			val = values[++idx];

			ClassToken token = new ClassToken();

			while (val != ")")
			{
				ClassFlags flag = ClassFlags.None;
				bool res = Enum.TryParse(val, out flag);
				if (res)
					token.Flags |= flag;

				val = values[++idx];
			}

			// Parse data in class
			if (val == "template") // Template meta data
			{
				bool end = false;
				byte indentCount = 1;
				val = values[++idx];

				// Find the first {
				while (val != "<")
				{
					val = values[++idx];
				}

				TemplateParam tmpParam = new TemplateParam();
				string typename = "";
				while (!end)
				{
					val = values[++idx];

					if (val == "<")
						++indentCount;
					else if (val == ">")
						--indentCount;

					if (val == "," && indentCount == 1)
					{
						token.TemplateMetaData.Add(tmpParam);
						tmpParam = new TemplateParam();
					}
					else if (string.IsNullOrWhiteSpace(typename) || indentCount > 1)
					{
						typename += val;
					}
					else
					{
						tmpParam.Type = typename;
						tmpParam.Name = val;
					}

					if (indentCount == 0)
						end = true;
				}
			}

			while (val != "{" && val != ":")
			{
				token.Name = val;
				val = values[++idx];
			}

			// Check for base class
			if (val == ":")
			{
				string typename = "";
				while (val != "{")
				{
					if (val == "public" || val == "protected" || val == "private" || val == ",")
					{
						token.Bases.Add(typename);
						typename = "";

						val = values[++idx];

						if (val != ",")
							continue;
						break;
					}

					typename += val;

					val = values[++idx];
				}
			}
			val = values[++idx];

			return token;
		}

		public static StructToken ParseStructToken(List<string> values, ref string val, ref int idx)
		{
			// Parse flags
			val = values[++idx];
			while (val != "(")
			{
				val = values[++idx]; // Should not happen
			}

			val = values[++idx];

			StructToken token = new StructToken();

			while (val != ")")
			{
				StructFlags flag;
				bool res = Enum.TryParse(val, out flag);
				if (res)
					token.Flags |= flag;

				val = values[++idx];
			}
			val = values[++idx];

			// Parse data in class
			if (val == "template") // Template meta data
			{
				bool end = false;
				byte indentCount = 1;
				val = values[++idx];

				// Find the first {
				while (val != "<")
				{
					val = values[++idx];
				}

				TemplateParam tmpParam = new TemplateParam();
				string typename = "";
				string name = "";
				while (!end)
				{
					val = values[++idx];

					if (val == "<")
						++indentCount;
					else if (val == ">")
						--indentCount;

					if (val == "," || indentCount == 0)
					{
						tmpParam.Type = typename;
						tmpParam.Name = name;
						token.TemplateMetaData.Add(tmpParam);
						tmpParam = new TemplateParam();
						typename = "";
						name = "";

						if (indentCount == 0)
							end = true;
					}

					typename += name;
					name = val;
				}
			}

			while (val != "{" && val != ":")
			{
				token.Name = val;
				val = values[++idx];
			}

			// Check for base class
			if (val == ":")
			{
				string typename = "";
				while (val != "{")
				{
					if (val == "public" || val == "protected" || val == "private" || val == ",")
					{
						token.Bases.Add(typename);
						typename = "";

						val = values[++idx];

						if (val != ",")
							continue;
						break;
					}

					typename += val;

					val = values[++idx];
				}
			}
			val = values[++idx];

			return token;
		}

		public static PropertyToken ParsePropertyToken(List<string> values, ref string val, ref int idx)
		{
			// Parse flags
			val = values[++idx];
			while (val != "(")
			{
				val = values[++idx]; // Should not happen
			}

			val = values[++idx];

			PropertyToken token = new PropertyToken();

			while (val != ")")
			{
				PropertyFlags flag;
				bool res = Enum.TryParse(val, out flag);
				if (res)
					token.Flags |= flag;

				val = values[++idx];
			}
			val = values[++idx];

			// Parse type and name
			while (val != ";")
			{
				token.Type += token.Name;
				token.Name = val;

				val = values[++idx];
			}

			return token;
		}
	}
}
