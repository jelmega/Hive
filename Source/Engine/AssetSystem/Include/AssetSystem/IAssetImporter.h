// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// IAssetImporter.h: Asset importer interface
#pragma once
#include "AssetSystemPCH.h"
#include "IAsset.h"

namespace Hv::AssetSystem {

	// TODO: how to allow plugins to add import settings (UI for params, map with data)?
	/**
	 * Import settings base structure
	 */
	struct ImportSettings
	{
	};

	// TODO: Add import settings UI if needed
	class HIVE_API IAssetImporter
	{
	public:
		/**
		 * Instantiate an asset importer
		 * @param[in] importedType	Type of asset the importer loads, Unknown if importer can load multiple asset types
		 */
		IAssetImporter(AssetType importedType) : m_ImportedType(importedType) {}
		virtual ~IAssetImporter() {}

		/**
		 * Import an asset
		 * @param[in] filePath			Path to file
		 * @param[in] pImportSettings	Import settings
		 * @return						Pointer to loaded asset, nullptr if loading failed
		 */
		virtual IAsset* Import(const String& filePath, ImportSettings* pImportSettings = nullptr) = 0;

		/**
		 * Get the type of asset that this importer imports
		 * @return	Imported asset type
		 */
		AssetType GetImportedType() { return m_ImportedType; }

	private:
		AssetType m_ImportedType;
	};

}

