// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ImageHelpers.h: Image helpers
#pragma once
#include "AssetSystemPCH.h"
#include "Image.h"

namespace Hv::ImageHelpers {
	
	/**
	 * Get the image size from its description
	 * @param[in] desc	Omage description
	 * @return
	 */
	HIVE_API u64 GetImageSize(const ImageDesc& desc);

	/**
	* Get the image size from its description
	* @param[in] desc	Image description
	* @return			Max mip level
	*/
	HIVE_API u8 GetMaxMipDepth(const ImageDesc& desc);

	/**
	 * Flip a 2D image horizontally
	 * @param[in] data		Image data
	 * @param[in] desc		Image description
	 * @param[in] layer		Array layer to flip
	 * @param[in] mipLevel	Mip level to flip
	 */
	HIVE_API void FlipVer2D(DynArray<u8>& data, const ImageDesc& desc, u32 layer, u8 mipLevel);
}
