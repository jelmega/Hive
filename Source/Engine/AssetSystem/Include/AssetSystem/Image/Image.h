// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Image.h: Image
#pragma once
#include "AssetSystemPCH.h"
#include "AssetSystem/IAsset.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv {

	enum class ImageFlipDirection : u8
	{
		Horizontal,
		Vertical,
	};
	HV_ENABLE_ENUM_FLAG_OPERATORS(ImageFlipDirection);

	struct ImageDesc
	{
		u32 width			= 1;		/**< Image with */
		u32 height			= 1;		/**< Image height */
		u32 depth			= 1;		/**< Image depth */
		u32 arrayLayers		= 1;		/**< Array layers */
		u8 mipLevels		= 1;		/**< Mip levels */
		PixelFormat format;				/**< Image format */
		b8 isCubemap		= false;	/**< If the image is a cubemap */
	};
	
	class HIVE_API Image : public AssetSystem::IAsset
	{
	public:
		Image();
		~Image();

		/**
		 * Create an image
		 * @param[in] desc		Image description
		 * @param[in] rawData	Raw image data
		 */
		void Create(const ImageDesc& desc, const DynArray<u8>& rawData);
		/**
		 * Create an image
		 * @param[in] desc		Image description
		 * @param[in] rawData	Raw image data
		 * @param[in] dataSize	Size of raw data in bytes
		 */
		void Create(const ImageDesc& desc, const u8* rawData, u64 dataSize);

		/**
		 * Flip an image around 1 or multiple axes
		 * @param[in] dir		Flip direction
		 * @param[in] layer		Layer to flip
		 * @param[in] mip		Mip level to flip
		 */
		void Flip2D(ImageFlipDirection dir, u32 layer = 0, u8 mip = 0);

		/**
		 * Get the image width
		 * @return	Image width
		 */
		u32 GetWidth() const { return m_Desc.width; }
		/**
		 * Get the image height
		 * @return	Image height
		 */
		u32 GetHeight() const { return m_Desc.height; }
		/**
		 * Get the image depth
		 * @return	Image depth
		 */
		u32 GetDepth() const { return m_Desc.depth; }
		/**
		 * Get the image array layers
		 * @return	Image array layers
		 */
		u32 GetLayerCount() const { return m_Desc.arrayLayers; }
		/**
		 * Get the image mip levels
		 * @return	Image mip levels
		 */
		u8 GetMipLevels() const { return m_Desc.mipLevels; }
		/**
		 * Get the image format
		 * @return	Image format
		 */
		PixelFormat GetFormat() const { return m_Desc.format; }
		/**
		* Check if the image is a cubemap
		* @return	True if the image is a cubemap, false otherwise
		*/
		b8 IsCubemap() const { return m_Desc.isCubemap; }
		/**
		 * Get the size of the raw data
		 * @return	Raw data size
		 */
		DynArray<u8>& GetData() { return m_RawData; }
		/**
		 * Get the size of the raw data
		 * @return	Raw data size
		 */
		const DynArray<u8>& GetData() const { return m_RawData; }

	private:
		ImageDesc m_Desc;
		DynArray<u8> m_RawData;	/**< Raw image data (wrapped by DynArray for implicit memory managment) */

	};

}

#pragma warning(pop)