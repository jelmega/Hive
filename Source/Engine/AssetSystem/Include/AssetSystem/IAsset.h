// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Asset.h: Asset
#pragma once
#include "AssetSystemPCH.h"

namespace Hv::AssetSystem {

	enum class AssetType : u16
	{
		Unkown	= 0,		/**< Unknown asset */
		Image	= 1,		/**< Image */
		Mesh	= 2,		/**< Mesh */

		Ext	= 0x1000,	/**< Start for plugin added extensions */
	};
	
	class HIVE_API IAsset
	{
	public:
		/**
		 * Create an unknown asset
		 */
		IAsset();
		/**
		 * Create an asset of a certain type
		 * @param[in] type	Asset type
		 */
		IAsset(AssetType type);
		~IAsset();

		/**
		 * Get the asset's type
		 * @return	Asset type
		 */
		AssetType GetAssetType() const { return m_Type; }

	protected:
		AssetType m_Type;
	};

}