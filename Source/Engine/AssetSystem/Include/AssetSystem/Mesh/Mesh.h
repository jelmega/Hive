// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Mesh.h: Mesh
#pragma once
#include "AssetSystemPCH.h"
#include "AssetSystem/IAsset.h"
#include "Renderer/Core/InputDescriptor.h"
#include "Renderer/Core/Buffer.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv {
	
	// Basic vertex
	struct Vertex
	{
		f32v3 position;
		f32v3 normals;
		f32v4 color;
		f32v2 texCoords;
	};

	// TODO: need a better way to handle the mesh and extend functionality (e.g. Input slot)
	// TODO: generate from shader files (how??)
	/**
	 * Simple mesh
	 */
	class HIVE_API Mesh : public AssetSystem::IAsset
	{
	public:
		Mesh();
		~Mesh();

		/**
		 * Set the mesh's positions
		 * @param[in] positions		Positions
		 */
		void SetPositions(const DynArray<f32v3>& positions);
		/**
		 * Set the mesh's normals
		 * @param[in] normals	Normals
		 */
		void SetNormals(const DynArray<f32v3>& normals);
		/**
		 * Set the mesh's colors
		 * @param[in] colors	Colors
		 */
		void SetColors(const DynArray<f32v4>& colors);
		/**
		 * Set the mesh's texCoords
		 * @param[in] texCoords		TexCoords
		 */
		void SetTexCoords(const DynArray<f32v2>& texCoords);

		/**
		* Set the mesh's vertices
		* @param[in] vertices	Vertices
		*/
		void SetVertices(const DynArray<Vertex>& vertices);

		/**
		* Set the meshes indices
		* @param[in] indices	Indices
		*/
		void SetIndices(const DynArray<u32>& indices);

		/**
		 * Get the meshes input descriptor
		 */
		static Renderer::InputDescriptor& GetInputDescriptor();

		/**
		 * Get the mesh's vertices
		 * @return	Vertices
		 */
		DynArray<Vertex>& GetVertices() { return m_Vertices; }
		/**
		* Get the mesh's indices
		* @return	Indices
		*/
		DynArray<u32>& GetIndices() { return m_Indices; }

	private:
		DynArray<Vertex> m_Vertices;
		DynArray<u32> m_Indices;
	};
}

#pragma warning(pop)