// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// AssetManager.h: Asset manager
#pragma once
#include "AssetSystemPCH.h"
#include "IAssetImporter.h"
#include "Mesh/Mesh.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv {
	using AssetHandle = Handle<AssetSystem::IAsset>;
	using MeshHandle = Handle<Mesh>;
}

namespace Hv::AssetSystem {
	class IAsset;
	class IAssetImporter;

	class HIVE_API AssetManager
	{
	private:
		struct IndexPair
		{
			AssetType type;
			u32 id;

			IndexPair() : type(AssetType::Unkown), id(0xFFFF'FFFF) {}
			IndexPair(AssetType type, u32 id) : type(type), id(id) {}
		};

		struct AssetPair
		{
			DynArray<IAsset*> assets;
			DynArray<u32> freeSlots;
		};

	public:
		AssetManager();
		~AssetManager();

		/**
		 * Initialize the asset manager
		 * @return	True if the asset manager has been initialize successfully, false otherwise
		 */
		b8 Init();
		/**
		 * Shutdown the asset manager
		 * @return	True if the asset manager has been shut down successfully, false otherwise
		 */
		b8 Shutdown();

		/**
		 * Load an asset
		 * @param[in] filePath			Path to file to load
		 * @param[in] assetName			Unique identifier for asset
		 * @param[in] pImportSettings	[OPTIONAL] Import settings
		 * @return						Pointer to an asset, nullptr if the loading failed
		 * @note						If the asset is already loaded, it will just be returned, otherwise is will be imported with the import settings
		 */
		AssetHandle Load(const String& filePath, const String& assetName, ImportSettings* pImportSettings = nullptr);
		/**
		* Load a mesh
		* @param[in] filePath			Path to file to load
		* @param[in] assetName			Unique identifier for asset
		* @param[in] pImportSettings	[OPTIONAL] Import settings
		* @return						Pointer to a mesh, nullptr if the loading failed
		* @note							If the mesh is already loaded, it will just be returned, otherwise is will be imported with the import settings
		* @note							If the the loaded asset is not a mesh, the asset will still be imported, but an invalid handle will be returned
		*/
		MeshHandle LoadMesh(const String& filePath, const String& assetName, ImportSettings* pImportSettings = nullptr);

		/**
		* Unload an asset
		* @param[in] assetName	Name of asset to unload
		* @return				True if the asset was unloaded, false otherwise
		*/
		b8 Unload(const String& assetName);

		/**
		 * Convert an asset handle to a mesh handle
		 * @param handle	Asset handle
		 * @return			Mesh handle, or an invalid handle if the handle is invalid or not a mesh
		 */
		MeshHandle GetMeshHandle(AssetHandle& handle);

		/**
		 * Get an asset
		 * @param[in] name	Asset name
		 * @return			Asset handle
		 */
		AssetHandle GetAsset(const String& name);

		/**
		 * Register an importer to the asset manager
		 * @param[in] extension		File extension associated with importer
		 * @param[in] pImporter		Importer to register
		 * @param[in] override		If their already exists an importer for the extension, should it be overriden
		 * @return					True if the importer is successfully registered
		 */
		b8 RegisterImporter(String extension, IAssetImporter* pImporter, b8 override = false);
		/**
		* Unregister an importer to the asset manager
		* @param[in] extension		File extension associated with importer
		* @param[in] pImporter		Importer to unregister (used to check if importer wasn't overriden with other importer)
		* @return					True if the importer is successfully registered
		*/
		b8 UnregisterImporter(String extension, IAssetImporter* pImporter);

	private:
		HV_MAKE_NON_COPYABLE(AssetManager);

		AssetPair& GetAssetPair(AssetType type);

		DynArray<AssetPair> m_Assets;								/**< Assets, sorted by asset type */
		DynArray<AssetPair> m_ExtAssets;							/**< Extension assets, sorted by asset type */

		DynArray<IndexPair> m_AssetMapping;							/**< Asset mapping (AssetHandle) */
		DynArray<u32> m_FreeAssetMappingSlots;						/**< Free asset mapping slots */
		HashMap<String, u32> m_NameMapping;							/**< Asset name mapping */

		HandleManager<IAsset> m_AssetHandleManager;					/**< Asset handle manager */
		HandleManager<Mesh> m_MeshHandleManager;					/**< Mesh handle manager */

		HashMap<String, IAssetImporter*> m_Importers;				/**< Importers */
	};

	HIVE_API AssetManager& GetAssetManager();

}

#define g_AssetManager Hv::AssetSystem::GetAssetManager()

#pragma warning(pop)
