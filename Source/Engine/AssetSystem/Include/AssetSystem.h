// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// AssetSystem.h: Asset system main header
#pragma once
#include "AssetSystemPCH.h"

#include "AssetSystem/IAsset.h"
#include "AssetSystem/AssetManager.h"

#include "AssetSystem/Image/Image.h"
#include "AssetSystem/Mesh/Mesh.h"