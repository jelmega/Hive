// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// AssetSystemPCH.h: Asset system Precompiled header
#pragma once
#include <Core.h>
#include HV_INCLUDE_RTTI_MODULE(AssetSystem)

HV_DECLARE_LOG_CATEGORY_EXTERN(AssetSys)