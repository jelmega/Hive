// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Mesh.cpp: Mesh
#pragma once
#include "AssetSystemPCH.h"
#include "AssetSystem/Mesh/Mesh.h"

namespace Hv {

	Mesh::Mesh()
		: IAsset(AssetSystem::AssetType::Mesh)
	{
	}

	Mesh::~Mesh()
	{
	}

	void Mesh::SetPositions(const DynArray<f32v3>& positions)
	{
		if (m_Vertices.Size() < positions.Size())
			m_Vertices.Resize(positions.Size());

		for (sizeT i = 0; i < positions.Size(); ++i)
		{
			m_Vertices[i].position = positions[i];
		}
	}

	void Mesh::SetNormals(const DynArray<f32v3>& normals)
	{
		if (m_Vertices.Size() < normals.Size())
			m_Vertices.Resize(normals.Size());

		for (sizeT i = 0; i < normals.Size(); ++i)
		{
			m_Vertices[i].normals = normals[i];
		}
	}

	void Mesh::SetColors(const DynArray<f32v4>& colors)
	{
		if (m_Vertices.Size() < colors.Size())
			m_Vertices.Resize(colors.Size());

		for (sizeT i = 0; i < colors.Size(); ++i)
		{
			m_Vertices[i].color = colors[i];
		}
	}

	void Mesh::SetTexCoords(const DynArray<f32v2>& texCoords)
	{
		if (m_Vertices.Size() < texCoords.Size())
			m_Vertices.Resize(texCoords.Size());

		for (sizeT i = 0; i < texCoords.Size(); ++i)
		{
			m_Vertices[i].texCoords = texCoords[i];
		}
	}

	void Mesh::SetVertices(const DynArray<Vertex>& vertices)
	{
		m_Vertices = vertices;
	}

	void Mesh::SetIndices(const DynArray<u32>& indices)
	{
		m_Indices = indices;
	}

	Renderer::InputDescriptor& Mesh::GetInputDescriptor()
	{
		static b8 isInputDescCreated = false;
		static Renderer::InputDescriptor inputDesc;

		if (!isInputDescCreated)
		{
			inputDesc.Append(Renderer::InputElementDesc(Renderer::InputSemantic::Position, 0, Renderer::InputElementType::Float3, 0, 0, 0));
			inputDesc.Append(Renderer::InputElementDesc(Renderer::InputSemantic::Normal, 0, Renderer::InputElementType::Float3, 3 * sizeof(f32), 0, 0));
			inputDesc.Append(Renderer::InputElementDesc(Renderer::InputSemantic::Color, 0, Renderer::InputElementType::Float4, 6 * sizeof(f32), 0, 0));
			inputDesc.Append(Renderer::InputElementDesc(Renderer::InputSemantic::TexCoord, 0, Renderer::InputElementType::Float2, 10 * sizeof(f32), 0, 0));
			isInputDescCreated = true;
		}
		return inputDesc;
	}
}
