// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Asset.h: Asset
#include "AssetSystemPCH.h"
#include "AssetSystem/IAsset.h"

namespace Hv::AssetSystem {

	IAsset::IAsset()
		: m_Type(AssetType::Unkown)
	{
	}

	IAsset::IAsset(AssetType type)
		: m_Type(type)
	{
	}

	IAsset::~IAsset()
	{
	}
}
