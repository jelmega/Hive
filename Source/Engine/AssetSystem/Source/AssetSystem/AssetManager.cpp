// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// AssetManager.cpp: Asset manager
#include "AssetSystemPCH.h"
#include "AssetSystem/AssetManager.h"
#include "AssetSystem/IAsset.h"

HV_DECLARE_LOG_CATEGORY(AssetSys, Hv::LogLevel::All);

namespace Hv::AssetSystem {

	AssetManager::AssetManager()
	{
	}

	AssetManager::~AssetManager()
	{
	}

	b8 AssetManager::Init()
	{
		return true;
	}

	b8 AssetManager::Shutdown()
	{
		for (AssetPair& pair : m_Assets)
		{
			for (IAsset* pAsset : pair.assets)
			{
				HvDelete pAsset;
			}
		}
		m_Assets.Clear();

		for (AssetPair& pair : m_ExtAssets)
		{
			for (IAsset* pAsset : pair.assets)
			{
				HvDelete pAsset;
			}
		}
		m_ExtAssets.Clear();

		m_AssetMapping.Clear();
		m_NameMapping.Clear();
		m_FreeAssetMappingSlots.Clear();

		for (auto& pair : m_Importers)
		{
			HvDelete pair.second;
		}

		return true;
	}

	AssetHandle AssetManager::Load(const String& filePath, const String& assetName, ImportSettings* pImportSettings)
	{
		// If an asset is already loaded, return it
		AssetHandle handle = GetAsset(assetName);
		if (handle.IsValid())
			return handle;

		// Retrieve importer
		String extension = HvFS::Path(filePath).Extension().ToString();
		auto importIt = m_Importers.Find(extension);
		if (importIt == m_Importers.Back())
		{
			g_Logger.LogFormat(LogAssetSys(), LogLevel::Error, "Failed to find importer for extension: ", extension);
			return AssetHandle();
		}
		IAssetImporter* pImporter = importIt->second;

		// Import asset
		IAsset* pAsset = pImporter->Import(filePath, pImportSettings);

		// Inset asset in correct pair
		AssetType type = pAsset->GetAssetType();
		AssetPair& pair = GetAssetPair(type);

		u32 id;
		if (pair.freeSlots.Size() > 0)
		{
			id = *pair.freeSlots.Last();
			pair.freeSlots.Pop();
			pair.assets[id] = pAsset;
		}
		else
		{
			id = u32(pair.assets.Size());
			pair.assets.Push(pAsset);
		}

		// Map asset to asset id
		u32 assetId;
		if (m_FreeAssetMappingSlots.Size() > 0)
		{
			assetId = *m_FreeAssetMappingSlots.Last();
			m_FreeAssetMappingSlots.Pop();
			m_AssetMapping[assetId] = IndexPair(type, id);
		}
		else
		{
			assetId = u32(m_AssetMapping.Size());
			m_AssetMapping.Push(IndexPair(type, id));
		}

		return m_AssetHandleManager.Create(assetId, pAsset);
	}

	MeshHandle AssetManager::LoadMesh(const String& filePath, const String& assetName, ImportSettings* pImportSettings)
	{
		AssetHandle handle = Load(filePath, assetName, pImportSettings);
		if (!handle.IsValid())
			return MeshHandle();
		if (handle->GetAssetType() != AssetType::Mesh)
			return MeshHandle();

		IndexPair index = m_AssetMapping[handle.GetId()];
		DynArray<IAsset*> assets = m_Assets[u16(index.type)].assets;
		IAsset* pAsset = assets[index.id];

		return m_MeshHandleManager.Create(index.id, (Mesh*)pAsset);
	}

	b8 AssetManager::Unload(const String& assetName)
	{
		auto it = m_NameMapping.Find(assetName);
		if (it == m_NameMapping.Back())
			return false;

		IndexPair index = m_AssetMapping[it->second];
		AssetPair& pair = m_Assets[u32(index.type)];

		u32 idx = index.id;
		IAsset* pAsset = pair.assets[idx];
		HvDelete pAsset;
		pair.assets[idx] = nullptr;
		pair.freeSlots.Push(idx);

		return true;
	}

	MeshHandle AssetManager::GetMeshHandle(AssetHandle& handle)
	{
		if (!handle.IsValid())
			return MeshHandle();
		if (handle->GetAssetType() != AssetType::Mesh)
			return MeshHandle();

		return m_MeshHandleManager.Create(handle.GetId(), (Mesh*)handle.Get());
	}

	AssetHandle AssetManager::GetAsset(const String& name)
	{
		auto assetIt = m_NameMapping.Find(name);
		if (assetIt == m_NameMapping.Back())
			return AssetHandle();

		u32 id = assetIt->second;
		IndexPair index = m_AssetMapping[id];
		IAsset* pAsset;
		if (u16(index.type) < u16(AssetType::Ext))
		{
			DynArray<IAsset*> assets = m_Assets[u16(index.type)].assets;
			pAsset = assets[index.id];
		}
		else
		{
			DynArray<IAsset*> assets = m_ExtAssets[u16(index.type) - u16(AssetType::Ext)].assets;
			pAsset = assets[index.id];
		}
		return m_AssetHandleManager.Create(id, pAsset);
	}

	b8 AssetManager::RegisterImporter(String extension, IAssetImporter* pImporter, b8 override)
	{
		auto it = m_Importers.Find(extension);
		if (it != m_Importers.Back())
		{
			if (override)
			{
				it->second = pImporter;
				g_Logger.LogFormat(LogAssetSys(), LogLevel::Info, "Overriding importer for extension: %s", extension);
				return true;
			}
			else
			{
				g_Logger.LogFormat(LogAssetSys(), LogLevel::Warning, "Trying to register importer for extension: %s, but an importer already exist for that filetype", extension);
				return false;
			}
		}
		m_Importers.Insert(extension, pImporter);

		return true;
	}

	b8 AssetManager::UnregisterImporter(String extension, IAssetImporter* pImporter)
	{
		auto it = m_Importers.Find(extension);
		if (it != m_Importers.Back())
		{
			if (it->second == pImporter)
			{
				m_Importers.Erase(extension);
			}
			else
			{
				g_Logger.LogFormat(LogAssetSys(), LogLevel::Info, "Trying to unregister importer that was overriden, nothing will happen, extension: %s", extension);
			}
			return true;
		}
		else
		{
			g_Logger.LogFormat(LogAssetSys(), LogLevel::Warning, "Trying to unregister importer that isn't registered, extension: %s", extension);
			return false;
		}
	}

	AssetManager::AssetPair& AssetManager::GetAssetPair(AssetType type)
	{
		if (u16(type) < u16(AssetType::Ext))
		{
			if (m_Assets.Size() <= u16(type))
				m_Assets.Resize(u16(type) + 1, AssetPair());

			return m_Assets[u16(type)];
		}
		else
		{
			u32 size = u16(type) - u16(AssetType::Ext);
			if (m_ExtAssets.Size() <= size)
				m_ExtAssets.Resize(size + 1, AssetPair());

			return m_ExtAssets[u16(type)];
		}
	}

	AssetManager& GetAssetManager()
	{
		static AssetManager manager;
		return manager;
	}
}
