// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Image.h: Image
#include "AssetSystemPCH.h"
#include "AssetSystem/Image/Image.h"
#include "AssetSystem/Image/ImageHelpers.h"

namespace Hv {

	Image::Image()
		: IAsset(AssetSystem::AssetType::Image)
		, m_Desc()
	{
	}

	Image::~Image()
	{
	}

	void Image::Create(const ImageDesc& desc, const DynArray<u8>& rawData)
	{
		m_Desc = desc;
		m_RawData = rawData;

		// Make sure that the mip level is possible to be reached
		u8 maxMip = ImageHelpers::GetMaxMipDepth(m_Desc);
		if (maxMip < m_Desc.mipLevels)
			m_Desc.mipLevels = maxMip;

		u64 expectedSize = ImageHelpers::GetImageSize(m_Desc);

		m_RawData.Resize(sizeT(expectedSize));
		m_RawData.ShrinkToFit();
	}

	void Image::Create(const ImageDesc& desc, const u8* rawData, u64 dataSize)
	{
		m_Desc = desc;
		m_RawData.Resize(sizeT(dataSize));
		Memory::Copy(m_RawData.Data(), rawData, sizeT(dataSize));

		// Make sure that the mip level is possible to be reached
		u8 maxMip = ImageHelpers::GetMaxMipDepth(m_Desc);
		if (maxMip < m_Desc.mipLevels)
			m_Desc.mipLevels = maxMip;

		u64 expectedSize = ImageHelpers::GetImageSize(m_Desc);
		m_RawData.Resize(expectedSize);
		m_RawData.ShrinkToFit();
	}

	void Image::Flip2D(ImageFlipDirection dir, u32 layer, u8 mip)
	{
		if (HV_IS_ENUM_FLAG_SET(dir, ImageFlipDirection::Vertical))
		{
			ImageHelpers::FlipVer2D(m_RawData, m_Desc, layer, mip);
		}

	}
}
