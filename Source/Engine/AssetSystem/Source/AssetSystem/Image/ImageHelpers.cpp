// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ImageHelpers.h: Image helpers
#include "AssetSystemPCH.h"
#include "AssetSystem/Image/ImageHelpers.h"

namespace Hv::ImageHelpers {

	u64 GetImageSize(const ImageDesc& desc)
	{
		u8 formatSize = desc.format.GetSize();

		if (formatSize == 0)
			return 0;

		u64 mip0Size;
		if (formatSize == 0xFF)
		{
			mip0Size = 0;
			switch (desc.format.components)
			{
			case PixelFormatComponents::BC1:
			case PixelFormatComponents::BC4:
				mip0Size = desc.width * desc.height / 2; // width/4 * height/4 * 8 (8 bytes per 4x4 block)
				break;
			case PixelFormatComponents::BC2: 
			case PixelFormatComponents::BC3:
			case PixelFormatComponents::BC5: 
			case PixelFormatComponents::BC6H: 
			case PixelFormatComponents::BC7: 
				mip0Size = desc.width * desc.height; // width/4 * height/4 * 16 (16 bytes per 4x4 block)
				break;
			case PixelFormatComponents::ASTC4X4:
				mip0Size = desc.width * desc.height / 8; // width/4 * height/4 * 128 (128 byte block)
				break;
			case PixelFormatComponents::ASTC6X6:
				mip0Size = desc.width * desc.height * 128 / 36; // width/6 * height/6 * 128 (128 byte block)
				break;
			case PixelFormatComponents::ASTC8X8:
				mip0Size = desc.width * desc.height / 2; // width/8 * height/8 * 128 (128 byte block)
				break;
			case PixelFormatComponents::ASTC10X10:
				mip0Size = desc.width * desc.height * 128 / 100; // width/10 * height/10 * 128 (128 byte block)
				break;
			case PixelFormatComponents::ASTC12X12:
				mip0Size = desc.width * desc.height * 128 / 144; // width/12 * height/12 * 128 (128 byte block)
				break;
			default: 
				HV_ASSERT(false);
			}
		}
		else
		{
			mip0Size = desc.width * desc.height * formatSize;
		}

		mip0Size *= desc.depth * desc.arrayLayers;
		u64 size = 0;
		for (u8 i = 0; i < desc.mipLevels; ++i)
		{
			size += mip0Size;
			mip0Size <<= 1;
		}
		return size;
	}

	u8 GetMaxMipDepth(const ImageDesc& desc)
	{
		u32 minDimension;
		if (desc.depth == 1)
		{
			if (desc.height == 1)
			{
				minDimension = desc.width;
			}
			else
			{
				minDimension = Hm::Min(desc.width, desc.height);
			}
		}
		else
		{
			minDimension = Hm::Min(desc.width, desc.height, desc.depth);
		}

		for (u8 i = 0; ; ++i)
		{
			if ((minDimension >> i) & 1)
				return i;
		}
	}

	void FlipVer2D(DynArray<u8>& data, const ImageDesc& desc, u32 layer, u8 mipLevel)
	{
		u32 formatSize = desc.format.GetSize();
		u64 mipOffset = mipLevel * desc.arrayLayers * desc.width * desc.height;
		u64 layerOffset = layer * desc.width * desc.height;
		u64 offset = mipOffset + layerOffset;

		u64 lineSize = desc.width * formatSize;

		DynArray<u8> line;
		line.Resize(sizeT(lineSize));
		for (u32 y = 0, half = desc.height / 2; y < half; ++y)
		{
			u64 topOffset = offset + y * lineSize;
			u64 bottomOffset = offset + (desc.height - y - 1) * lineSize;

			Memory::Copy(line.Data(), data.Data() + topOffset, sizeT(lineSize));
			Memory::Copy(data.Data() + topOffset, data.Data() + bottomOffset, sizeT(lineSize));
			Memory::Copy(data.Data() + bottomOffset, line.Data(), sizeT(lineSize));
		}
	}
}
