// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// WindowsInput.h: Windows input
#pragma once
#include "Input/InputCommon.h"

namespace Hv::HAL::Input {
	
	/**
	 * Convert a win32 virtual key into a virtual key
	 * @param[in] vk	Win32 virtual key
	 * @return			Virtual key
	 */
	VKey Win32KeyToVKey(u32 vk);

}
