// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// WindowsWindowSystem.h: Windows window system
#pragma once
#include "SystemPCH.h"
#include "HAL/HalWindowSystem.h"

#if HV_PLATFORM_WINDOWS

namespace Hv::HAL::WindowSystem {
	
	/**
	 * Windows monitor handle
	 */
	struct WindowsMonitorHandle
	{
		HMONITOR handle;
		WideChar devName[CCHDEVICENAME];
	};

	/**
	* Windows window handle
	*/
	struct WindowsWindowHandle
	{
		HINSTANCE instance = nullptr;	/**< Application instance */
		HWND handle = nullptr;			/**< Window handle */
	};

	/**
	 * Convert the window style to the windows window style
	 * @param[in] style	Window style
	 * @return			Windows Window style
	 */
	DWORD WindowStyleToWinStyle(u8 style);
	/**
	 * Convert the window style to the windows window extended style
	 * @param[in] style	Window style
	 * @return			Windows Window style
	 */
	DWORD WindowStyleToWinStyleEx(u8 style);

}

#endif