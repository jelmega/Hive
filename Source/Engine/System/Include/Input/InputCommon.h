#pragma once
#include "Core/CoreHeaders.h"

namespace Hv {
	
	/**
	 * Virtual keys
	 */
	enum class VKey : u8
	{
		None,
		A,
		B,
		C,
		D,
		E,
		F,
		G,
		H,
		I,
		J,
		K,
		L,
		M,
		N,
		O,
		P,
		Q,
		R,
		S,
		T,
		U,
		V,
		W,
		X,
		Y,
		Z,
		Num0,
		Num1,
		Num2,
		Num3,
		Num4,
		Num5,
		Num6,
		Num7,
		Num8,
		Num9,
		Escape,
		LShift,
		LCtrl,
		LAlt,
		LSystem,
		RShift,
		RCtrl,
		RAlt,
		RSystem,
		RBracket,
		LBracket,
		SemiColon,
		Comma,
		Period,
		Quote,
		Slash,
		BackSlash,
		Tilde,
		Equal,
		Dash,
		Space,
		Enter,
		BackSpace,
		Tab,
		PageUp,
		PageDown,
		End,
		Home,
		Insert,
		Delete,
		CapsLock,
		Left,
		Up,
		Right,
		Down,
		Numpad0,
		Numpad1,
		Numpad2,
		Numpad3,
		Numpad4,
		Numpad5,
		Numpad6,
		Numpad7,
		Numpad8,
		Numpad9,
		NumpadMultiply,
		NumpadPlus,
		NumpadMinus,
		NumpadDivide,
		NumpadPeriod,
		F1,
		F2,
		F3,
		F4,
		F5,
		F6,
		F7,
		F8,
		F9,
		F10,
		F11,
		F12, 
		F13,	/**< Shift + F1 */
		F14,	/**< Shift + F2 */
		F15,	/**< Shift + F3 */
		F16,	/**< Shift + F4 */
		F17,	/**< Shift + F5 */
		F18,	/**< Shift + F6 */
		F19,	/**< Shift + F7 */
		F20,	/**< Shift + F8 */
		F21,	/**< Shift + F9 */
		F22,	/**< Shift + F10 */
		F23,	/**< Shift + F11 */
		F24,	/**< Shift + F12 */
		NumKeys
	};

	/**
	 * Mouse button
	 */
	enum class MouseButton : u8
	{
		None	= 0x00,
		LButton	= 0x01,
		MButton = 0x02,
		RButton	= 0x04,
		Button4 = 0x08,
		Button5	= 0x10,
		NumButtons = 5
	};
	HV_ENABLE_ENUM_FLAG_OPERATORS(MouseButton)

	/**
	 * Input modifier
	 */
	enum class InputModifier : u8
	{
		None	= 0x00,
		LShift	= 0x01,
		LCtrl	= 0x02,
		LAlt	= 0x04,
		RShift	= 0x08,
		RCtrl	= 0x10,
		RAlt	= 0x20,
		Caps	= 0x40,
		Shift	= LShift | RShift,
		Ctrl	= LCtrl | RCtrl,
		Alt		= LAlt | RAlt
	};
	HV_ENABLE_ENUM_FLAG_OPERATORS(InputModifier)

	/**
	 * State of key/button
	 */
	enum class InputState : u8
	{
		Pressed,
		Down,
		Released
	};

	/**
	 * Mouse scroll direction
	 */
	enum class MouseScrollDirection : u8
	{
		Up,
		Down,
		Both
	};

	/**
	 * Input axes
	 */
	enum class InputAxis : u8
	{
		MouseX,
		MouseY
	};

}
