// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// InputManager.h: Input manager
#pragma once
#include "SystemPCH.h"
#include "InputCommon.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv {namespace System {
	struct InputEvent;
}

/**
	 * Mouse data
	 */
	struct MouseData
	{
		Flags<u8, MouseButton> buttons = MouseButton::None;	/**< Mouse buttons that are down */
		i32 x = 0;												/**< Mouse x position */
		i32 y = 0;												/**< Mouse y position */
		f32 scroll = 0.f;										/**< Mouse scroll delta */
	};
	
	/**
	 * Input manager
	 * @note Axis and move actions are only called when a change in value happens
	 */
	class HIVE_API InputManager
	{
	private:
		/**
		 * Key input action
		 */
		struct KeyInputAction
		{
			InputModifier mod;			/**< Key modifier */
			Delegate<void()> del;	/**< Delegate to action callback */
		};

		/**
		 * Mouse button input action
		 */
		struct MouseButtonInputAction
		{
			InputModifier mod;			/**< Key modifier */
			Delegate<void()> del;	/**< Delegate to action callback */
		};

		/**
		* Mouse scroll input action
		*/
		struct MouseScrollInputAction
		{
			MouseScrollDirection dir;		/**< scroll direction */
			Delegate<void(f32)> del;	/**< Delegate to action callback */
		};

		/**
		* Mouse move input action
		*/
		struct MouseMoveInputAction
		{
			Delegate<void(i32, i32)> del;	/**< Delegate to action callback */
		};

		/**
		* Axis input action
		*/
		struct AxisInputAction
		{
			Delegate<void(f32)> del;	/**< Delegate to action callback */
		};

	public:
		/**
		 * Create an input manager
		 */
		InputManager();
		~InputManager();

		/**
		 * Initialize the input manager
		 */
		b8 Init();
		/**
		 * Tick the input manager
		 */
		void Tick();
		/**
		 * Tick the input manager at the end of the current frame
		 */
		void PostTick();

		////////////////////////////////////////////////////////////////////////////////
		//	Input checks
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Check if a key is pressed in this frame
		 * @param[in] key	Key to check
		 * @return			True if the key is pressed during this frame, false otherwise
		 */
		HV_FORCE_INL b8 IsKeyPressed(VKey key) const { return m_KeysPressed[sizeT(key)]; }
		/**
		 * Check if a key is down in this frame
		 * @param[in] key	Key to check
		 * @return			True if the key is down during this frame, false otherwise
		 */
		HV_FORCE_INL b8 IsKeyDown(VKey key) const { return m_KeysDown[sizeT(key)]; }
		/**
		 * Check if a key is released in this frame
		 * @param[in] key	Key to check
		 * @return			True if the key is released during this frame, false otherwise
		 */
		HV_FORCE_INL b8 IsKeyReleased(VKey key) const { return m_KeysReleased[sizeT(key)]; }

		/**
		 * Check if a mouse button is pressed in this frame
		 * @param[in] button	Mouse button to check
		 * @return				True if the mouse button is pressed during this frame, false otherwise
		 */
		HV_FORCE_INL b8 IsMouseButtonPressed(MouseButton button) const { return m_MouseButtonPressed[sizeT(button)]; }
		/**
		 * Check if a mouse button is down in this frame
		 * @param[in] button	Mouse button to check
		 * @return				True if the mouse button is down during this frame, false otherwise
		 */
		HV_FORCE_INL b8 IsMouseButtonDown(MouseButton button) const { return m_MouseButtonDown[sizeT(button)]; }
		/**
		 * Check if a mouse button is released in this frame
		 * @param[in] button	Mouse button to check
		 * @return				True if the mouse button is released during this frame, false otherwise
		 */
		HV_FORCE_INL b8 IsMouseButtonReleased(MouseButton button) const { return m_MouseButtonReleased[sizeT(button)]; }

		/**
		 * Get the current mouse scroll delta
		 * @return	Scroll delta
		 */
		HV_FORCE_INL f32 GetScrollDelta() const { return m_Mouse.scroll; }
		/**
		 * Get the mouse's x position
		 * @return	Mouse x position
		 */
		HV_FORCE_INL i32 GetMouseXPosition() const { return m_Mouse.x; }
		/**
		 * Get the mouse's y position
		 * @return	Mouse y position
		 */
		HV_FORCE_INL i32 GetMouseYPosition() const { return m_Mouse.y; }
		/**
		 * Get the mouse's position
		 * @return	Mouse position
		 */
		HV_FORCE_INL i32v2 GetMousePosition() const { return i32v2(m_Mouse.x, m_Mouse.y); }

		////////////////////////////////////////////////////////////////////////////////
		//	Bind actions
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Bind an action to a key
		 * @param[in] key	Key
		 * @param[in] state	Key state
		 * @param[in] del	Delegate to action callback
		 */
		void BindKey(VKey key, InputState state, const Delegate<void()>& del);
		/**
		 * Bind an action to a key
		 * @param[in] mod	Key modifier
		 * @param[in] key	Key
		 * @param[in] state	Key state
		 * @param[in] del	Delegate to action callback
		 */
		void BindKey(InputModifier mod, VKey key, InputState state, const Delegate<void()>& del);

		/**
		 * Bind an action to a mouse button
		 * @param[in] button	Mouse button
		 * @param[in] state		Button state
		 * @param[in] del		Delegate to action callback
		 */
		void BindMouseButton(MouseButton button, InputState state, const Delegate<void()>& del);

		/**
		 * Bind an action to a mouse button
		 * @param[in] mod	Key modifier
		 * @param[in] button	Mouse button
		 * @param[in] state		Button state
		 * @param[in] del		Delegate to action callback
		 */
		void BindMouseButton(InputModifier mod, MouseButton button, InputState state, const Delegate<void()>& del);

		/**
		 * Bind an action to a mouse scroll
		 * @param[in] dir	Scroll direction
		 * @param[in] del	Delegate to action callback
		 */
		void BindMouseScroll(MouseScrollDirection dir, const Delegate<void(f32)>& del);
		/**
		 * Bind an action to a mouse move
		 * @param[in] del	Delegate to action callback
		 * @note			Function will be given the change in movement
		 */
		void BindMouseMove(const Delegate<void(i32, i32)>& del);
		/**
		 * Bind an action to a mouse move
		 * @param[in] axis	Axis
		 * @param[in] del	Delegate to action callback
		 * @note			Function will be given the change in movement
		 */
		void BindAxis(InputAxis axis, const Delegate<void(f32)>& del);

		/**
		 * [INTERNAL] Handle input events
		 * @param[in] event	Event to handle
		 * @return			True if the event was processed, false otherwise
		 */
		b8 HandleEvents(const System::InputEvent& event);

	private:

		HV_MAKE_NON_COPYABLE(InputManager);

		/**
		 * Callback for key pressed event
		 * @param[in] key	Key
		 */
		void OnKeyPressed(VKey key);
		/**
		 * Callback for key released event
		 * @param[in] key	Key
		 */
		void OnKeyReleased(VKey key);

		/**
		 * Callback for mouse button pressed event
		 * @param[in] button	Mouse button
		 */
		void OnMouseButtonPressed(MouseButton button);
		/**
		 * Callback for mouse button released event
		 * @param[in] button	Mouse button
		 */
		void OnMouseButtonReleased(MouseButton button);

		/**
		 * Callback for mouse scroll event
		 * @param[in] delta	Scroll delta
		 */
		void OnMouseScroll(f32 delta);
		/**
		 * Callback for mouse move event
		 * @param[in] x	Mouse x position
		 * @param[in] y	Mouse y position
		 */
		void OnMouseMove(i32 x, i32 y);

		/**
		 * Get the input modifier from a key
		 * @param[in] key	Key
		 * @return			Input modifier
		 */
		InputModifier GetKeyMod(VKey key) const;

		/**
		 * Get a mouse buttton from its index
		 * @param[in] index	Index
		 * @return			Mouse button
		 */
		MouseButton IndexToMouseButton(u32 index);

		/**
		* Get a the index of a mouse button
		* @param[in] button	Mouse button
		* @return			Index
		*/
		u32 MouseButtonToIndex(MouseButton button);


		Array<b8, sizeT(VKey::NumKeys)> m_KeysPressed;									/**< Array with keys that are pressed during the current frame */
		Array<b8, sizeT(VKey::NumKeys)> m_KeysDown;										/**< Array with keys that are down during the current frame */
		Array<b8, sizeT(VKey::NumKeys)> m_KeysReleased;									/**< Array with keys that are released during the current frame */
		Flags<u8, InputModifier> m_Modifiers;											/**< Key modifiers */

		Array<b8, sizeT(MouseButton::NumButtons)> m_MouseButtonPressed;					/**< Array with mouse buttons that are pressed during the current frame */
		Array<b8, sizeT(MouseButton::NumButtons)> m_MouseButtonDown;					/**< Array with mouse buttons that are down during the current frame */
		Array<b8, sizeT(MouseButton::NumButtons)> m_MouseButtonReleased;				/**< Array with mouse buttons that are released during the current frame */

		HashMultiMap<VKey, KeyInputAction> m_KeyPressedActions;							/**< Map with actions to process on key pressed */
		HashMultiMap<VKey, KeyInputAction> m_KeyDownActions;							/**< Map with actions to process on key down */
		HashMultiMap<VKey, KeyInputAction> m_KeyReleasedActions;						/**< Map with actions to process on key released */

		HashMultiMap<MouseButton, MouseButtonInputAction> m_MouseButtonPressedActions;	/**< Map with actions to process on mouse button pressed */
		HashMultiMap<MouseButton, MouseButtonInputAction> m_MouseButtonDownActions;		/**< Map with actions to process on mouse button down */
		HashMultiMap<MouseButton, MouseButtonInputAction> m_MouseButtonReleasedActions;	/**< Map with actions to process on mouse button released */

		DynArray<MouseScrollInputAction> m_MouseScrollActions;							/**< Map with actions to process on mouse scroll */
		DynArray<MouseMoveInputAction> m_MouseMoveActions;								/**< Map with actions to process on mouse moved */

		HashMultiMap<InputAxis, AxisInputAction> m_AxisActions;							/**< Map with action to process axis changed */

		MouseData m_Mouse;																	/**< Mouse data */
	};

	HIVE_API InputManager& GetInputManager();

}

#define g_InputManager Hv::GetInputManager()

#pragma warning(pop)