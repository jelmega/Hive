// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Input.h: Input include header
#pragma once

#include "Input/InputCommon.h"
#include "HAL/HalInput.h"
#include "Input/InputManager.h"

#include "WindowSystem/Monitor.h"
#include "WindowSystem/Window.h"
#include "WindowSystem/WindowManager.h"
#include "HAL/HalWindowSystem.h"

#include "System/SystemEvent.h"