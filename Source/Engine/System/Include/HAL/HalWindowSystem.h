// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HalWindowSystem.h: HAL window system
#pragma once
#include "SystemPCH.h"

namespace Hv::WindowSystem {

	/**
	* Window handle
	*/
	typedef struct WindowHandleT* WindowHandle;
	/**
	* Invalid window handle
	*/
	const WindowHandle InvalidWindowHandle = (WindowHandle)-1;

	/**
	* Monitor handle
	*/
	typedef struct MonitorHandleT* MonitorHandle;
	/**
	* Invalid monitor handle
	*/
	const MonitorHandle InvalidMonitorHandle = (MonitorHandle)-1;

}

namespace Hv::HAL::WindowSystem {
	
	using WindowHandle = Hv::WindowSystem::WindowHandle;
	using MonitorHandle = Hv::WindowSystem::MonitorHandle;

	////////////////////////////////////////////////////////////////////////////////
	//	Monitor
	////////////////////////////////////////////////////////////////////////////////
	/**
	 * Get the montor handle from an index
	 * @param[in] idx	Monitor index
	 * @return			Handle to the monitor
	 */
	MonitorHandle GetMonitorFromIndex(i8 idx);
	/**
	 * Get the dimensions of the monitor
	 * @param[in] handle	Monitor handle
	 * @param[out] rc		Monitor rect
	 * @return				True if the dimensions were recieved successfully, false otherwise
	 */
	b8 GetMonitorRect(MonitorHandle& handle, i32rect& rc);
	/**
	 * Get the refresh-rate of the monitor
	 * @param[in] handle		Monitor handle
	 * @param[out] refreshRate	Monitor refresh-rate
	 * @return					True if the refresh-rate was recieved successfully, false otherwise
	 */
	b8 GetMonitorRefreshRate(MonitorHandle& handle, u32& refreshRate);
	/**
	 * Destroy the monitor handle
	 * @param[in] handle	Monitor handle
	 */
	void DestroyMonitorHandle(MonitorHandle& handle);

	////////////////////////////////////////////////////////////////////////////////
	//	Window
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Create a window handle
	 * @param[in] width		Window width
	 * @param[in] height	Window height
	 * @param[in] title		Window title
	 * @param[in] style		Window style
	 * @param[in] id		Window id
	 * @param[in] sysHandle	System handle
	 * @param[in] pUserData	User data
	 * @return				Window handle
	 */
	WindowHandle CreateWindow(u32 width, u32 height, const String& title, u8 style, u16 id, SystemHandle& sysHandle, void* pUserData);

	/**
	 * Close a window
	 * @param[in] handle	Window handle
	 * @return				True if the window could be closed, false otherwise
	 */
	b8 CloseWindow(WindowHandle& handle);

	/**
	 * Update the window
	 * @param[in] handle	Window handle
	 * @return				True if the window could update, false otherwise
	 */
	b8 UpdateWindow(WindowHandle& handle);
	/**
	 * Resize the window
	 * @param[in] handle	Window handle
	 * @param[in] width		Window width
	 * @param[in] height	Window height
	 * @param[in] style		Window style
	 * @return				True if the window could be resized
	 */
	b8 ResizeWindow(WindowHandle& handle, u32 width, u32 height, u8 style);
	/**
	 * Move a window
	 * @param[in] handle	Window handle
	 * @param[in] x			X position of the top-left corner
	 * @param[in] y			Y position of the top-left corner
	 * @param[in] style		Window style
	 * @return				True if the window could be resized
	 */
	b8 MoveWindow(WindowHandle& handle, i32 x, i32 y, u8 style);
	/**
	 * Set a window's title
	 * @param[in] handle	Window handle
	 * @param[in] title		Window title
	 * @return				True if the window title was set, false otherwise
	 */
	b8 SetWindowTitle(WindowHandle& handle, String title);

	/**
	 * Get the current window position
	 * @param[in] handle	Window handle
	 * @param[in] x			Window x position
	 * @param[in] y			Window y position
	 * @return				True if the window position was set successfully
	 */
	b8 GetWindowPosition(const WindowHandle& handle, i32& x, i32& y);

	/**
	 * Minimize the window
	 * @param[in] handle	Window handle
	 * @returb				True if the window was minimized sucessfully, false otherwise
	 */
	b8 MinimizeWindow(WindowHandle& handle);
	/**
	 * Maximize the window
	 * @param[in] handle	Window handle
	 * @returb				True if the window was maximized sucessfully, false otherwise
	 */
	b8 MaximizeWindow(WindowHandle& handle);
	/**
	 * Restore the window
	 * @param[in] handle	Window handle
	 * @returb				True if the window was restored sucessfully, false otherwise
	 */
	b8 RestoreWindow(WindowHandle& handle);


	/**
	 * Get the window rect adjusted to the style
	 * @param[in] rc	Window rect
	 * @param[in] style	Window style
	 * @return			Adjusted window rect
	 */
	i32rect GetAdjustedWindowRect(i32rect rc, u8 style);

}

#if HV_PLATFORM_WINDOWS
#	include "Windows/WindowsWindowSystem.h"
#else
#endif
