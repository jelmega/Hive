// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HalInput.h: HAL input
#pragma once
#include "SystemPCH.h"

namespace Hv::HAL::Input {
	
	/**
	 * Get the mouse position from the window
	 * @param[out] x	Mouse x position
	 * @param[out] y	Mouse y position
	 * @return			True if the mouse position was returned successfully, false otherwise
	 */
	b8 GetMousePosition(i32& x, i32& y);

}

#if HV_PLATFORM_WINDOWS
#	include "Windows/WindowsInput.h"	
#else
#endif
