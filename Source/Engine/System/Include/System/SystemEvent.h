// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// SystemEvent.h: System event
#pragma once
#include "SystemPCH.h"
#include "Input/InputCommon.h"

#pragma warning(push)
#pragma warning(disable: 4201) // nonstandard extension used : nameless struct/union

namespace Hv::System {
	
	enum class SystemEventType : u8
	{
		Unknown,
		Window,
		Input
	};

	/**
	* Input event type
	*/
	enum class InputEventType : u8
	{
		KeyPressed,
		KeyReleased,
		MouseMove,
		MouseButtonPressed,
		MouseButtonReleased,
		MouseScroll
	};

	/**
	* Window event type
	*/
	enum class WindowEventType : u8
	{
		Close,			/**< Window is closed */
		Shown,			/**< WIndow is shown */
		Hidden,			/**< Window is hidden */
		Restored,		/**< Window is restored (not maximized or minimized) */
		Minimized,		/**< Window is minimized */
		Maximized,		/**< Window is maximized */
		Resized,		/**< Size has changed to data0 x data1 */
		UserResized,	/**< Resized by external event i.e. User resizes the window, to data0 x data1. Succeded by Resized */
		Moved,			/**< Window was moved to data0, data1 */
		FocusGained,	/**< Window focus was gained */
		FocusLost,		/**< Window focus was lost */
		StartGrab,		/**< The window is grabbed */
		EndGrab,		/**< The window is released */
	};

	/**
	* Unkown event info
	*/
	struct UnknownEvent
	{
		u32 id;
		u32 data0;
		u32 data1;
	};

	/**
	* Window event data
	*/
	struct WindowEvent
	{
		WindowEventType type;
		u8 windowId;
		i32 data0;
		i32 data1;
	};

	/**
	* Keyboard input event
	*/
	struct KeyBoardEvent
	{
		VKey key;
	};

	/**
	* Mouse input event
	*/
	struct MouseEvent
	{
		MouseButton button;
		i32 xPos;
		i32 yPos;
		f32 scroll;
	};

	/**
	* Input event data
	*/
	struct InputEvent
	{
		InputEventType type;
		union
		{
			KeyBoardEvent key;
			MouseEvent mouse;
		};
	};

	/**
	* System event data (main event)
	*/
	struct SystemEvent
	{
		SystemEventType type = SystemEventType::Unknown;	/**< System event type */
		union
		{
			UnknownEvent unknownEvent;	/**< Unknown event */
			WindowEvent windowEvent;	/**< Window event */
			InputEvent inputEvent;		/**< Input event */
		};
	};

}

#pragma warning(pop)
