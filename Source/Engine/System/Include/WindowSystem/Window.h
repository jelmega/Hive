// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Window.h: Window
#pragma once
#include "SystemPCH.h"
#include "HAL/HalWindowSystem.h"
#include "Monitor.h"
#include "System/SystemEvent.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv {
	
#define HV_WINDOW_CENTERED -1

	/**
	 * Window style
	 */
	enum class WindowStyle
	{
		None = 0x00,	/**< No style (no borders, caption, etc.) */
		Border = 0x01,	/**< Border, has no effect when the ThickBorder flag is set */
		UserResizable = 0x02,	/**< If the windown is resizable by the user */
		Caption = 0x04,	/**< Caption/titlebar (this will enable border) */
		CloseBox = 0x08, /**< Close box (this will enable border and caption) */
		MaximizeBox = 0x10,	/**< Maximize box (CloseBox needs to be set) */
		MinimizeBox = 0x20,	/**< Minimize box (CloseBox needs to be set) */
		OuterEdge = 0x40,	/**< Inner sunken edge */
		InnerEdge = 0x80,	/**< Outer sunken edge */
		Default = Border | UserResizable | Caption | CloseBox |	/**< Default style */
		MinimizeBox | MaximizeBox | InnerEdge | OuterEdge
	};
	HV_ENABLE_ENUM_FLAG_OPERATORS(WindowStyle);

	enum class WindowFlags : u16
	{
		None = 0x0000,	/**< No flags */
		Hidden = 0x0001,	/**< If the window is hidden */
		Minimized = 0x0002,	/**< If the window is minimized */
		Maximized = 0x0004,	/**< If the window is maximized */
		Resizable = 0x0008,	/**< If the window is resizable (false if in borderless fullscreen) */

		FullScreen = 0x0010,	/**< Is the window is fullscreen */
		Borderless = 0x0020,	/**< Is the window borderless */
		Grabbed = 0x0040,	/**< Is the window grabbed by the user (used to bypass win32 modal loop) */
		InFocus = 0x0080,	/**< Is the window in focus */

		WasResized = 0x0100,	/**< If the window was resized while being grabbed */
	};
	HV_ENABLE_ENUM_FLAG_OPERATORS(WindowFlags);

	/**
	 * UserData for HAL window
	 */
	struct WindowUserData
	{
		Hv::Delegate<b8(const System::SystemEvent&)> msgDelegate;	/**< Message delegate */
		u8 id;														/**< Window id */
	};

	/**
	 * Window description
	 */
	struct WindowDesc
	{
		u32 width = 640;							/**< Window width */
		u32 height = 360;							/**< Window height */
		i32 x = HV_WINDOW_CENTERED;					/**< Window x position (HV_WINDOW_CENTERED = monitor center, only if monitor is valid) */
		i32 y = HV_WINDOW_CENTERED;					/**< Window y position (HV_WINDOW_CENTERED = monitor center, only if monitor is valid) */
		String title = "Application";				/**< Window title */
		WindowStyle style = WindowStyle::Default;	/**< Window style */
		Monitor monitor;							/**< Monitor to put window on */
	};

	/**
	 * Window
	 * @note A window is render API agnostic, to be able to render to a window, the window needs to be registered with the renderer
	 */
	class HIVE_API Window
	{
	public:
		static constexpr u8 InvalidId = 0xFF;

	public:
		/**
		 * Create an uninitialized window
		 */
		Window();
		~Window();

		/**
		 * Update the window
		 */
		void Update();

		/**
		 * Close the window
		 * @return	True if the window closed successfully, false otherwise
		 * @note	Closing the main window will end the program
		 */
		b8 Close();
		/**
		 * Check if the window is open
		 * @return	True if the window is open, false otherwise
		 */
		HV_FORCE_INL b8 IsOpen() const { return m_Handle != WindowSystem::InvalidWindowHandle; }

		/**
		 * Get the id of the window
		 * @return	Window id
		 */
		HV_FORCE_INL u8 GetId() const { return m_Id; }

		/**
		 * Set the size of the window
		 * @param[in] width		Window width
		 * @param[in] height	Window height
		 */
		void SetSize(u32 width, u32 height);
		/**
		 * Get the window width
		 * @return	Window width
		 */
		HV_FORCE_INL u32 GetWidth() const { return m_Width; }
		/**
		 * Get the window height
		 * @return	Window height
		 */
		HV_FORCE_INL u32 GetHeight() const { return m_Height; }
		/**
		 * Get the window size
		 * @return	2D vector with width and height
		 */
		HV_FORCE_INL u32v2 GetSize() const { return u32v2(m_Width, m_Height); }

		/**
		 * Set the window position
		 * @param[in] x	X position
		 * @param[in] y	Y position
		 */
		void SetPosition(i32 x, i32 y);
		/**
		 * Get the window's x position
		 * @return	Window x position
		 */
		HV_FORCE_INL i32 GetXPos() const { return m_XPos; }
		/**
		 * Get the window's y position
		 * @return	Window y position
		 */
		HV_FORCE_INL i32 GetYPos() const { return m_YPos; }
		/**
		 * Get the window position
		 * @return	2D vector with width and height
		 */
		HV_FORCE_INL i32v2 GetPosition() const { return i32v2(m_XPos, m_YPos); }
		/**
		 * Get the window rect
		 * @return	Window rect
		 */
		HV_FORCE_INL i32rect GetWindowRect() const { return i32rect(m_XPos, m_YPos, m_XPos + m_Width, m_YPos + m_Height); }
		/**
		 * Get the window rect adjusted to the window style
		 * @return	Window rect adjusted to the window style
		 */
		i32rect GetAdjustedWindowRect() const;

		/**
		 * Set the window title
		 * @param[in] title	Window title
		 */
		void SetTitle(const Hv::String& title);
		/**
		 * Get the window title
		 * @return	Window title
		 */
		HV_FORCE_INL String GetTitle() const { return m_Title; }

		/**
		 * Minimizes the window
		 */
		void Minimize();
		/**
		 * Maximizes the window
		 */
		void Maximize();
		/**
		 * Restore the window (not minimized/maximized)
		 */
		void Restore();

		/**
		 * [INTERNAL] Get the handle to the platform window
		 * @return	Window handle
		 */
		HV_FORCE_INL WindowSystem::WindowHandle GetHandle() const { return m_Handle; }

		////////////////////////////////////////////////////////////////////////////////
		// Flag and style getters
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Get the window style
		 * @return	Window style
		 */
		HV_FORCE_INL Flags<u8, WindowStyle> GetWindowStyle() const { return m_Style; }
		/**
		 * Get the window flags
		 * @return	Window flags
		 */
		HV_FORCE_INL Flags<u16, WindowFlags> GetWindowFlags() const { return m_Flags; }

		/**
		 * Check if the window is visible
		 * @return	True if the window is visible, false otherwise
		 */
		HV_FORCE_INL b8 IsVisible() const { return m_Flags & WindowFlags::Hidden; }
		/**
		 * Check if the window is hidden
		 * @return	True if the window is hidden, false otherwise
		 */
		HV_FORCE_INL b8 IsHidden() const { return !(m_Flags & WindowFlags::Hidden); }
		/**
		 * Check if the window is maximized
		 * @return True if the window is maximized, false otherwise
		 */
		HV_FORCE_INL b8 IsMaximized() const { return m_Flags & WindowFlags::Maximized; }
		/**
		 * Check if the window is minimized
		 * @return True if the window is minimized, false otherwise
		 */
		HV_FORCE_INL b8 IsMinimized() const { return m_Flags & WindowFlags::Minimized; }
		/**
		 * Check if the window is fullscreen
		 * @return True if the window is fullscreen, false otherwise
		 */
		HV_FORCE_INL b8 IsFullscreen() const { return m_Flags & WindowFlags::FullScreen; }
		/**
		 * Check if the window is borderless
		 * @return True if the window is borderless, false otherwise
		 */
		HV_FORCE_INL b8 IsBorderless() const { return m_Flags & WindowFlags::Borderless; }
		/**
		 * Check if the window is grabbed
		 * @return True if the window is grabbed, false otherwise
		 */
		HV_FORCE_INL b8 IsGrabbed() const { return m_Flags & WindowFlags::Grabbed; }
		/**
		 * Check if the window is in focus
		 * @return True if the window is in focus, false otherwise
		 */
		HV_FORCE_INL b8 IsInFocus() const { return m_Flags & WindowFlags::InFocus; }

		////////////////////////////////////////////////////////////////////////////////
		// Calback setters
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Set the window close callback
		 * @param[in] callback	Callback
		 * @note				Calback returns true if the window is allowed to close, false if closing needs to be blocked
		 */
		HV_FORCE_INL void SetOnCloseCallback(const Hv::Delegate<b8()>& callback) { m_CloseCallback = callback; }
		/**
		 * Set the window shown callback
		 * @param[in] callback	Callback
		 */
		HV_FORCE_INL void SetOnShownCallback(const Hv::Delegate<void()>& callback) { m_ShownCallback = callback; }
		/**
		 * Set the window hidden callback
		 * @param[in] callback	Callback
		 */
		HV_FORCE_INL void SetOnHiddenCallback(const Hv::Delegate<void()>& callback) { m_HiddenCallback = callback; }
		/**
		 * Set the window resized callback
		 * @param[in] callback	Callback
		 */
		HV_FORCE_INL void SetOnResizedCallback(const Hv::Delegate<void(u32, u32)>& callback) { m_ResizedCallback = callback; }
		/**
		 * Set the window user resized callback
		 * @param[in] callback	Callback
		 */
		HV_FORCE_INL void SetOnUserResizingCallback(const Hv::Delegate<void(u32, u32)>& callback) { m_UserResizingCallback = callback; }
		/**
		 * Set the window user resize ended callback
		 * @param[in] callback	Callback
		 */
		HV_FORCE_INL void SetOnUserResizeEndedCallback(const Hv::Delegate<void(u32, u32)>& callback) { m_UserResizeEndedCallback = callback; }
		/**
		 * Set the window moved callback
		 * @param[in] callback	Callback
		 */
		HV_FORCE_INL void SetOnMovedCallback(const Hv::Delegate<void(i32, i32)>& callback) { m_MovedCallback = callback; }
		/**
		 * Set the window restored callback
		 * @param[in] callback	Callback
		 */
		HV_FORCE_INL void SetOnRestoredCallback(const Hv::Delegate<void(u32, u32)>& callback) { m_RestoredCallback = callback; }
		/**
		 * Set the window minimized callback
		 * @param[in] callback	Callback
		 */
		HV_FORCE_INL void SetOnMinimizedCallback(const Hv::Delegate<void(u32, u32)>& callback) { m_MinimizedCallback = callback; }
		/**
		 * Set the window maximized callback
		 * @param[in] callback	Callback
		 */
		HV_FORCE_INL void SetOnMaximizedCallback(const Hv::Delegate<void(u32, u32)>& callback) { m_MaximizedCallback = callback; }
		/**
		 * Set the window minimized callback
		 * @param[in] callback	Callback
		 */
		HV_FORCE_INL void SetOnFocusGainedCallback(const Hv::Delegate<void()>& callback) { m_FocuGainedCallback = callback; }
		/**
		 * Set the window maximized callback
		 * @param[in] callback	Callback
		 */
		HV_FORCE_INL void SetOnFocusLostCallback(const Hv::Delegate<void()>& callback) { m_FocusLostCallback = callback; }
		/**
		 * Set the window minimized callback
		 * @param[in] callback	Callback
		 */
		HV_FORCE_INL void SetOnStartGrabCallback(const Hv::Delegate<void()>& callback) { m_StartGrabCallback = callback; }
		/**
		 * Set the window maximized callback
		 * @param[in] callback	Callback
		 */
		HV_FORCE_INL void SetOnEndGrabCallback(const Hv::Delegate<void()>& callback) { m_EndGrabCallback = callback; }

		/**
		 * [INTERNAL] Set the renderer resize callback
		 * @param[in] callback	Callback
		 * @note				Do <b>NOT</b> manually set this callback, this can cause severe issues
		 */
		HV_FORCE_INL void SetOnRendererResizeCallback(const Hv::Delegate<void(u32, u32)>& callback) { m_RendererResizeCallback = callback; }
		/**
		 * [INTERNAL] Set the renderer unregister callback
		 * @param[in] callback	Callback
		 * @note				Do <b>NOT</b> manually set this callback, this can cause severe issues
		 */
		HV_FORCE_INL void SetOnRendererUnregisterCallback(const Hv::Delegate<b8(u16)>& callback) { m_RendererUnregisterCallback = callback; }

	private:
		friend class WindowManager;

		/**
		 * Create a window
		 * @param[in] desc		Window description
		 * @param[in] id		Window id
		 * @param[in] sysHandle	System handle
		 */
		void Create(const WindowDesc& desc, u8 id, SystemHandle& sysHandle);

		////////////////////////////////////////////////////////////////////////////////
		// Calbacks
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Callback for shown event
		 */
		void OnShown();
		/**
		 * Callback for hidden event
		 */
		void OnHidden();
		/**
		 * Close callback
		 * @return	True is the window is allowed to close, false otherwise
		 */
		b8 OnClose();
		/**
		 * Callback for size changed event
		 * @param[in] width		Window width
		 * @param[in] height	Window height
		 */
		void OnResized(u32 width, u32 height);
		/**
		 * Callback for resized event
		 * @param[in] width		Window width
		 * @param[in] height	Window height
		 */
		void OnUserResizing(u32 width, u32 height);
		/**
		 * Callback for user resized event
		 * @param[in] x	X position
		 * @param[in] y	Y position
		 */
		void OnMoved(i32 x, i32 y);
		/**
		 * Callback for retored event
		 */
		void OnRestored();
		/**
		 * Callback for minimized event
		 */
		void OnMinimized();
		/**
		 * Callback for maximized event
		 */
		void OnMaximized();
		/**
		 * Callback for focus gained event
		 */
		void OnFocusGained();
		/**
		 * Callback for focus lost event
		 */
		void OnFocusLost();
		/**
		 * Callback for start grab event
		 */
		void OnStartGrab();
		/**
		 * Callback for end grab event
		 */
		void OnEndGrab();

		u32 m_Width;											/**< Window width */
		u32 m_Height;											/**< Window height */
		i32 m_XPos;												/**< Window X position */
		i32 m_YPos;												/**< Window Y position */
		Flags<u8, WindowStyle> m_Style;							/**< Window style */
		Flags<u16, WindowFlags> m_Flags;						/**< Window flags */
		u8 m_Id;												/**< Window id */
		String m_Title;											/**< Window title */
		WindowSystem::WindowHandle m_Handle;					/**< Window handle */
		WindowUserData m_UserData;								/**< Window user data for platform window (set by window manager) */

																// user callbacks
		Delegate<void()> m_ShownCallback;						/**< Shown callback */
		Delegate<void()> m_HiddenCallback;						/**< Hidden callback */
		Delegate<b8()> m_CloseCallback;							/**< Close callback */
		Delegate<void(u32, u32)> m_ResizedCallback;				/**< Resized callback */
		Delegate<void(u32, u32)> m_UserResizingCallback;		/**< User resizing callback */
		Delegate<void(u32, u32)> m_UserResizeEndedCallback;		/**< User resize ended callback */
		Delegate<void(i32, i32)> m_MovedCallback;				/**< Moved callback */
		Delegate<void(u32, u32)> m_RestoredCallback;			/**< Restored callback */
		Delegate<void(u32, u32)> m_MinimizedCallback;			/**< Minimized callback */
		Delegate<void(u32, u32)> m_MaximizedCallback;			/**< Maximized callback */
		Delegate<void()> m_FocuGainedCallback;					/**< Focus gained callback */
		Delegate<void()> m_FocusLostCallback;					/**< Focus lost callback */
		Delegate<void()> m_StartGrabCallback;					/**< Start grab callback */
		Delegate<void()> m_EndGrabCallback;						/**< End grab callback */
		Delegate<void(u32, u32)> m_RendererResizeCallback;		/**< Renderer resize callback */
		Delegate<b8(u16)> m_RendererUnregisterCallback;			/**< Renderer unregister callback */
	};

}

#pragma warning(pop)