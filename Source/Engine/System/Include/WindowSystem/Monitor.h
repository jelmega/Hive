// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Monitor.h: Monitor (physical)
#pragma once
#include "SystemPCH.h"
#include "HAL/HalWindowSystem.h"

namespace Hv {
	
	/**
	 * Monitor
	 */
	class HIVE_API Monitor
	{
	public:
		/**
		 * Create a default monitor
		 */
		Monitor();
		/**
		* Create a default monitor from an index
		* @param[in] idx	Monitor index
		*/
		explicit Monitor(i8 idx);
		Monitor(Monitor&& monitor) noexcept;
		~Monitor();

		Monitor& operator=(Monitor&& monitor) noexcept;

		/**
		 * Get the width of the monitor
		 * @return	Monitor width
		 */
		HV_FORCE_INL u32 GetWidth() const { return m_Width; }
		/**
		 * Get the height of the monitor
		 * @return	Monitor height
		 */
		HV_FORCE_INL u32 GetHeight() const { return m_Height; }
		/**
		* Get the x position of the monitor
		* @return	Monitor x position
		*/
		HV_FORCE_INL u32 GetXPosition() const { return m_XPos; }
		/**
		* Get the y position of the monitor
		* @return	Monitor y position
		*/
		HV_FORCE_INL u32 GetYPosition() const { return m_YPos; }
		/**
		 * Get the refresh-rate of the monitor
		 * @return	Monitor refresh-rate
		 */
		HV_FORCE_INL u32 GetRefreshRate() const { return m_RefreshRate; }
		/**
		 * Check if the monitor is valid
		 * @return	True if te monitor is valid, false otherwise
		 */
		HV_FORCE_INL b8 IsValid() const { return m_Valid; }

	private:
		HV_MAKE_NON_COPYABLE(Monitor);

		u32 m_Width;							/**< Monitor width */
		u32 m_Height;							/**< Monitor height */
		i32 m_XPos;								/**< Monitor x position */
		i32 m_YPos;								/**< Monitor y position */
		u32 m_RefreshRate;						/**< Monitor refrech-rate */
		WindowSystem::MonitorHandle m_Handle;	/**< Handle to system monitor */
		b8 m_Valid;								/**< If the montor is valid */
	};

}