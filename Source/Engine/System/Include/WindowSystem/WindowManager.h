// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// WindowManager.h: Window manager
#pragma once
#include "SystemPCH.h"
#include "Window.h"
#include <System/SystemEvent.h>

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv {
	
	/**
	 * Window manager
	 */
	class HIVE_API WindowManager
	{
	public:
		WindowManager();
		~WindowManager();

		/**
		 * [INTERNAL] Initialize the window manager and creates the main window
		 * @param[in] desc			Main window Description
		 * @param[in] sysHandle		System handle
		 * @return					True if the window manager intialized successfully, false otherwise
		 * @note					This function is not meant to be called by the user
		 */
		b8 Init(const WindowDesc& desc, SystemHandle& sysHandle);

		/**
		 * Create a window
		 * @param[in] desc		Main window Description
		 * @return				Pointer to a window
		 */
		Window* CreateWindow(const WindowDesc& desc);
		/**
		 * Remove a window from the manager
		 * @param[in] pWindow	Pointer to the window to remove
		 * @return				True if the window could be removed
		 */
		b8 DestroyWindow(Window* pWindow);
		/**
		* Remove a window from the manager
		* @param[in] id	Id of window to remove
		* @return		True if the window could be removed
		*/
		b8 DestroyWindow(u8 id);

		/**
		 * Update all windows
		 */
		void Tick();

		/**
		 * Get the main window
		 */
		HV_FORCE_INL Window* GetMainWindow() const { return m_pMainWindow; }
		/**
		 * Get a window from its id
		 * @param[in] id	Window id
		 * @return			Pointer to the window
		 */
		Window* GetWindowFromId(u16 id) const;
		/**
		 * Get the window which is currently in focus
		 * @return	Window which is currently in focus
		 */
		Window* GetWindowInFocus() const;

		/**
		 * Check if any window in the manager is grabbed
		 * @return	True if any window is grabbed
		 */
		HV_FORCE_INL b8 IsAnyWindowGrabbed() const { return m_IsAnyWindowGrabbed; }
		/**
		 * Check if there is any window that isn't closed
		 * @return	True if any window is open
		 */
		b8 IsAnyWindowOpen();
		/**
		 * Check if any window is in focus
		 * @return	True if any window is in focus
		 */
		HV_FORCE_INL b8 IsAnyWindowInFocus() const { return m_InFocusId != Window::InvalidId; }

		/**
		 * [INTERNAL] Shutdown the window manager
		 * @return	True if the window manager shut down successfully, false otherwise
		 */
		b8 Shutdown();
		/**
		 * [INTERNAL] Set the message delegate
		 * @param[in] msgDelegate	Message delegate
		 */
		HV_FORCE_INL void SetMessageDelegate(Delegate<b8(const System::SystemEvent&)> msgDelegate) { m_msgDelegate = msgDelegate; }
		/**
		 * [INTERNAL] Handle window events
		 * @param[in] wndEvent	Window event
		 * @return				True if the event was processed, false otherwise
		 */
		b8 HandleEvents(const System::WindowEvent& wndEvent);

	private:

		Window* m_pMainWindow;									/**< Main window */
		HashMap<u16, Window*> m_pWindows;						/**< Map with existing windows */
		SystemHandle m_SysHandle;								/**< System handle */
		Delegate<b8(const System::SystemEvent&)> m_msgDelegate;	/**< Message delegate */
		b8 m_IsAnyWindowGrabbed;								/**< If a window is grabbed */
		u8 m_InFocusId;											/**< Id of the window in focus */

		u8 m_CurrentId;									/**< Current id (used as window if when created) */
		DynArray<u8> m_FreeIds;									/**< Free ids */
	};

	/**
	 * Get the global window manager
	 * @return	Global window manager
	 */
	HIVE_API WindowManager& GetWindowManager();

}

#define g_WindowManager Hv::GetWindowManager()

#pragma warning(pop)