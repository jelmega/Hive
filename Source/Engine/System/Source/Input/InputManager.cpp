// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// InputManager.cpp: Input manager
#include "SystemPCH.h"
#include "System/SystemEvent.h"
#include "Input/InputManager.h"
#include "HAL/HalInput.h"
#include "WindowSystem/WindowManager.h"

namespace Hv {
	
	InputManager::InputManager()
	{
	}

	InputManager::~InputManager()
	{
	}

	b8 InputManager::Init()
	{
		return HAL::Input::GetMousePosition(m_Mouse.x, m_Mouse.y);
	}

	void InputManager::Tick()
	{
		// Update keys that are down
		for (u32 i = 0; i < u32(VKey::NumKeys); ++i)
		{
			if (m_KeysDown[i])
			{
				//update actions
				VKey key = VKey(i);
				auto it = m_KeyDownActions.Find(key);
				if (it == m_KeyDownActions.Back())
					break;
				while (true)
				{
					if (m_Modifiers & it->second.mod || it->second.mod == InputModifier::None)
						it->second.del();

					++it;
					if (it == m_KeyDownActions.Back() || it->first != key)
						break;
				}
			}
		}

		// Update mouse buttons that are down
		for (u32 i = 0; i < u32(MouseButton::NumButtons); ++i)
		{
			if (m_MouseButtonDown[i])
			{
				//update actions
				MouseButton button = MouseButton(i);
				auto it = m_MouseButtonDownActions.Find(button);
				if (it == m_MouseButtonDownActions.Back())
					break;
				while (true)
				{
					if (m_Modifiers & it->second.mod || it->second.mod == InputModifier::None)
						it->second.del();

					++it;
					if (it == m_MouseButtonDownActions.Back() || it->first != button)
						break;
				}
			}
		}
	}

	void InputManager::PostTick()
	{
		if (g_WindowManager.IsAnyWindowInFocus()) // If a window is in focus, tick normally
		{
			// Update keys pressed during the frame and clear pressed and released
			for (u32 i = 0; i < u32(VKey::NumKeys); ++i)
			{
				m_KeysDown[i] |= m_KeysPressed[i];
				// m_KeysDown[i] &= !m_KeysReleased[i]; // Done in OnKeyReleased()
				m_KeysPressed[i] = false;
				m_KeysReleased[i] = false;
			}

			// Update mouse buttons pressed during the frame and clear pressed and released
			for (u32 i = 0; i < u32(MouseButton::NumButtons); ++i)
			{
				if (m_MouseButtonPressed[i])
				{
					m_MouseButtonDown[i] = true;
					m_Mouse.buttons |= IndexToMouseButton(i);
				}
				// m_KeysDown[i] &= !m_KeysReleased[i]; // Done in OnMouseButtonReleased()
				m_MouseButtonPressed[i] = false;
				m_MouseButtonReleased[i] = false;
			}
		}
		else // Otherwise reset keys and mouse buttons
		{
			// Reset keys
			for (u32 i = 0; i < u32(VKey::NumKeys); ++i)
			{
				m_KeysPressed[i] = false;
				m_KeysDown[i] = false;
				m_KeysReleased[i] = false;
			}

			// Reset mouse buttons
			for (u32 i = 0; i < u32(MouseButton::NumButtons); ++i)
			{
				m_MouseButtonPressed[i] = false;
				m_MouseButtonDown[i] = false;
				m_MouseButtonReleased[i] = false;
			}
			m_Mouse.buttons = MouseButton::None;
		}
		m_Mouse.scroll = 0;

	}

	void InputManager::BindKey(VKey key, InputState state, const Delegate<void()>& del)
	{
		KeyInputAction action;
		action.mod = InputModifier::None;
		action.del = del;
		switch (state)
		{
		case InputState::Pressed:
			m_KeyPressedActions.Insert(key, action);
			break;
		case InputState::Down:
			m_KeyDownActions.Insert(key, action);
			break;
		case InputState::Released:
			m_KeyReleasedActions.Insert(key, action);
			break;
		default:
			break;
		}
	}

	void InputManager::BindKey(InputModifier mod, VKey key, InputState state, const Delegate<void()>& del)
	{
		KeyInputAction action;
		action.mod = mod;
		action.del = del;
		switch (state)
		{
		case InputState::Pressed:
			m_KeyPressedActions.Insert(key, action);
			break;
		case InputState::Down:
			m_KeyDownActions.Insert(key, action);
			break;
		case InputState::Released:
			m_KeyReleasedActions.Insert(key, action);
			break;
		default:
			break;
		}
	}

	void InputManager::BindMouseButton(MouseButton button, InputState state, const Delegate<void()>& del)
	{
		MouseButtonInputAction action;
		action.mod = InputModifier::None;
		action.del = del;
		switch (state)
		{
		case InputState::Pressed:
			m_MouseButtonPressedActions.Insert(button, action);
			break;
		case InputState::Down:
			m_MouseButtonDownActions.Insert(button, action);
			break;
		case InputState::Released:
			m_MouseButtonReleasedActions.Insert(button, action);
			break;
		default:
			break;
		}
	}

	void InputManager::BindMouseButton(InputModifier mod, MouseButton button, InputState state,
		const Delegate<void()>& del)
	{
		MouseButtonInputAction action;
		action.mod = mod;
		action.del = del;
		switch (state)
		{
		case InputState::Pressed:
			m_MouseButtonPressedActions.Insert(button, action);
			break;
		case InputState::Down:
			m_MouseButtonDownActions.Insert(button, action);
			break;
		case InputState::Released:
			m_MouseButtonReleasedActions.Insert(button, action);
			break;
		default:
			break;
		}
	}

	void InputManager::BindMouseScroll(MouseScrollDirection dir, const Delegate<void(f32)>& del)
	{
		MouseScrollInputAction action;
		action.dir = dir;
		action.del = del;
		m_MouseScrollActions.Push(action);
	}

	void InputManager::BindMouseMove(const Delegate<void(i32, i32)>& del)
	{
		MouseMoveInputAction action;
		action.del = del;
		m_MouseMoveActions.Push(action);
	}

	void InputManager::BindAxis(InputAxis axis, const Delegate<void(f32)>& del)
	{
		AxisInputAction action;
		action.del = del;
		m_AxisActions.Insert(axis, action);
	}

	b8 InputManager::HandleEvents(const System::InputEvent& event)
	{
		switch (event.type)
		{
		case System::InputEventType::KeyPressed:
			OnKeyPressed(event.key.key);
			return true;
		case System::InputEventType::KeyReleased:
			OnKeyReleased(event.key.key);
			return true;
		case System::InputEventType::MouseMove:
			OnMouseMove(event.mouse.xPos, event.mouse.yPos);
			return true;
		case System::InputEventType::MouseButtonPressed:
			OnMouseButtonPressed(event.mouse.button);
			return true;
		case System::InputEventType::MouseButtonReleased:
			OnMouseButtonReleased(event.mouse.button);
			return true;
		case System::InputEventType::MouseScroll:
			OnMouseScroll(event.mouse.scroll);
			return true;
		default:
			return false;
		}
	}

	void InputManager::OnKeyPressed(VKey key)
	{
		// Only set pressed if key is not down
		if (m_KeysDown[sizeT(key)])
			return;
		m_KeysPressed[sizeT(key)] = true;
		InputModifier mod = GetKeyMod(key);
		if (mod != InputModifier::None)
			m_Modifiers |= mod;

		//update actions
		auto it = m_KeyPressedActions.Find(key);
		if (it == m_KeyPressedActions.Back())
			return;
		while (true)
		{
			if (m_Modifiers & it->second.mod || it->second.mod == InputModifier::None)
				it->second.del();

			++it;
			if (it == m_KeyPressedActions.Back() || it->first != key)
				break;
		}
	}

	void InputManager::OnKeyReleased(VKey key)
	{
		m_KeysDown[sizeT(key)] = false;
		m_KeysReleased[sizeT(key)] = true;
		InputModifier mod = GetKeyMod(key);
		if (mod != InputModifier::None)
			m_Modifiers &= ~mod;

		//update actions
		auto it = m_KeyReleasedActions.Find(key);
		if (it == m_KeyReleasedActions.Back())
			return;
		while (true)
		{
			if (m_Modifiers & it->second.mod || it->second.mod == InputModifier::None)
				it->second.del();

			++it;
			if (it == m_KeyReleasedActions.Back() || it->first != key)
				break;
		}
	}

	void InputManager::OnMouseButtonPressed(MouseButton button)
	{
		// Only set pressed if mouse button is not down
		u32 idx = MouseButtonToIndex(button);
		if (m_MouseButtonDown[sizeT(idx)])
			return;
		m_MouseButtonPressed[sizeT(idx)] = true;

		//update actions
		auto it = m_MouseButtonPressedActions.Find(button);
		if (it == m_MouseButtonPressedActions.Back())
			return;
		while (true)
		{
			if (m_Modifiers & it->second.mod || it->second.mod == InputModifier::None)
				it->second.del();

			++it;
			if (it == m_MouseButtonPressedActions.Back() || it->first != button)
				break;
		}
	}

	void InputManager::OnMouseButtonReleased(MouseButton button)
	{
		u32 idx = MouseButtonToIndex(button);
		m_MouseButtonDown[sizeT(idx)] = false;
		m_MouseButtonReleased[sizeT(idx)] = true;
		m_Mouse.buttons &= ~button;

		//update actions
		auto it = m_MouseButtonReleasedActions.Find(button);
		if (it == m_MouseButtonReleasedActions.Back())
			return;
		while (true)
		{
			if (m_Modifiers & it->second.mod || it->second.mod == InputModifier::None)
				it->second.del();

			++it;
			if (it == m_MouseButtonReleasedActions.Back() || it->first != button)
				break;
		}
	}

	void InputManager::OnMouseScroll(f32 delta)
	{
		m_Mouse.scroll = delta;
		for (auto& action : m_MouseScrollActions)
		{
			switch (action.dir)
			{
			case MouseScrollDirection::Up:
				if (delta > 0)
					action.del(delta);
				break;
			case MouseScrollDirection::Down:
				if (delta < 0)
					action.del(-delta);
				break;
			default:
			case MouseScrollDirection::Both:
				action.del(delta);
				break;
			}
		}
	}

	void InputManager::OnMouseMove(i32 x, i32 y)
	{
		i32 xDelta = x - m_Mouse.x;
		i32 yDelta = y - m_Mouse.y;
		m_Mouse.x = x;
		m_Mouse.y = y;
		if (xDelta != 0 || yDelta != 0)
		{
			for (auto action : m_MouseMoveActions)
			{
				action.del(xDelta, yDelta);
			}
		}
		if (xDelta != 0)
		{
			auto it = m_AxisActions.Find(InputAxis::MouseX);
			if (it != m_AxisActions.Back())
			{
				while (true)
				{
					it->second.del(f32(xDelta));
					++it;
					if (it == m_AxisActions.Back() || it->first != InputAxis::MouseX)
						break;
				}
			}
		}
		if (yDelta != 0)
		{
			auto it = m_AxisActions.Find(InputAxis::MouseY);
			if (it != m_AxisActions.Back())
			{
				while (true)
				{
					it->second.del(f32(yDelta));
					++it;
					if (it == m_AxisActions.Back() || it->first != InputAxis::MouseY)
						break;
				}
			}
		}
	}

	InputModifier InputManager::GetKeyMod(VKey key) const
	{
		switch (key)
		{
		case VKey::LShift:
			return InputModifier::LShift;
		case VKey::LCtrl:
			return InputModifier::LCtrl;
		case VKey::LAlt:
			return InputModifier::LAlt;
		case VKey::RShift:
			return InputModifier::RShift;
		case VKey::RCtrl:
			return InputModifier::RCtrl;
		case VKey::RAlt:
			return InputModifier::RAlt;
		case VKey::CapsLock:
			return InputModifier::Caps;
		default:
			return InputModifier::None;
		}
	}

	MouseButton InputManager::IndexToMouseButton(u32 index)
	{
		switch (index)
		{
		case 0:
			return MouseButton::LButton;
		case 1:
			return MouseButton::MButton;
		case 2:
			return MouseButton::RButton;
		case 3:
			return MouseButton::Button4;
		case 4:
			return MouseButton::Button5;
		default:
			return MouseButton::None;
		}
	}

	u32 InputManager::MouseButtonToIndex(MouseButton button)
	{
		switch (button)
		{
		case MouseButton::LButton:
			return 0;
		case MouseButton::MButton:
			return 1;
		case MouseButton::RButton:
			return 2;
		case MouseButton::Button4:
			return 3;
		case MouseButton::Button5:
			return 4;
		case MouseButton::NumButtons:
		case MouseButton::None:
		default:
			HV_ASSERT(false); // SHould not hit this!
			return 0;
		}
	}

	InputManager& GetInputManager()
	{
		static InputManager input;
		return input;
	}

}
