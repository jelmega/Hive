// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Monitor.h: Monitor (physical)
#include "SystemPCH.h"
#include "WindowSystem/Monitor.h"
#include "HAL/HalWindowSystem.h"

namespace Hv {
	
	Monitor::Monitor()
		: m_Width(0)
		, m_Height(0)
		, m_XPos(0)
		, m_YPos(0)
		, m_RefreshRate(0)
		, m_Handle(WindowSystem::InvalidMonitorHandle)
		, m_Valid(false)
	{
	}

	Monitor::Monitor(i8 idx)
	{
		m_Handle = HAL::WindowSystem::GetMonitorFromIndex(idx);
		if (m_Handle == WindowSystem::InvalidMonitorHandle)
		{
			g_Logger.LogFormat(LogLevel::Error, "Could not retrieve the monitor at index %u!", idx);
			m_Valid = false;
			return;
		}

		i32rect rc;
		b8 res = HAL::WindowSystem::GetMonitorRect(m_Handle, rc);
		if (!res)
		{
			g_Logger.LogError("Could not retrieve monitor size");
			m_Valid = false;
			return;
		}
		m_XPos = rc.left;
		m_YPos = rc.top;
		m_Width = rc.right - rc.left;
		m_Height = rc.bottom - rc.top;

		res = HAL::WindowSystem::GetMonitorRefreshRate(m_Handle, m_RefreshRate);
		if (!res)
		{
			g_Logger.LogWarning("Could not retrieve monitor refresh rate, using defualt value of 60Hz!");
			m_RefreshRate = 60;
		}

		m_Valid = true;
	}

	Monitor::Monitor(Monitor&& monitor) noexcept
		: m_Width(monitor.m_Width)
		, m_Height(monitor.m_Height)
		, m_XPos(monitor.m_XPos)
		, m_YPos(monitor.m_YPos)
		, m_RefreshRate(monitor.m_RefreshRate)
		, m_Handle(monitor.m_Handle)
		, m_Valid(monitor.m_Valid)
	{
		monitor.m_Handle = WindowSystem::InvalidMonitorHandle;
	}

	Monitor::~Monitor()
	{
		if (m_Handle != WindowSystem::InvalidMonitorHandle)
			HAL::WindowSystem::DestroyMonitorHandle(m_Handle);
	}

	Monitor& Monitor::operator=(Monitor&& monitor) noexcept
	{
		m_Width = monitor.m_Width;
		m_Height = monitor.m_Height;
		m_XPos = monitor.m_XPos;
		m_YPos = monitor.m_YPos;
		m_RefreshRate = monitor.m_RefreshRate;
		m_Handle = monitor.m_Handle;
		m_Valid = monitor.m_Valid;
		monitor.m_Handle = WindowSystem::InvalidMonitorHandle;
		return *this;
	}

}
