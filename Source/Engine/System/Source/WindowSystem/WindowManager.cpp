// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// WindowManager.cpp: Window manager
#include "SystemPCH.h"
#include "WindowSystem/WindowManager.h"

namespace Hv {

	WindowManager::WindowManager()
		: m_pMainWindow(nullptr)
		, m_IsAnyWindowGrabbed(false)
		, m_InFocusId(Window::InvalidId)
		, m_CurrentId(0)
		, m_SysHandle(InvalidSystemHandle)
	{
	}

	WindowManager::~WindowManager()
	{
	}

	b8 WindowManager::Init(const WindowDesc& desc, SystemHandle& sysHandle)
	{
		m_SysHandle = sysHandle;
		m_pMainWindow = CreateWindow(desc);

		return m_pMainWindow;
	}

	Window* WindowManager::CreateWindow(const WindowDesc& desc)
	{
		if (m_CurrentId == Window::InvalidId)
		{
			g_Logger.LogError("Reached window id limit, can't use more than 255 windows (including main window) concurrently!");
			return nullptr;
		}
		if (m_SysHandle == InvalidSystemHandle)
		{
			g_Logger.LogError("Invalid system handle!");
			return nullptr;
		}

		Window* pWindow = HvNew Window();
		// Set userdata for the platformWindow
		WindowUserData userData;
		userData.msgDelegate = m_msgDelegate;
		userData.id = m_CurrentId;
		pWindow->m_UserData = userData;

		u8 id;
		if (m_FreeIds.Size() > 0)
		{
			id = m_FreeIds[m_FreeIds.Size() - 1];
			m_FreeIds.Pop();
		}
		else
		{
			id = m_CurrentId;
			++m_CurrentId;
		}

		pWindow->Create(desc, id, m_SysHandle);
		m_pWindows.Insert(id, pWindow);

		return pWindow;
	}

	b8 WindowManager::DestroyWindow(Window* pWindow)
	{
		u8 id = pWindow->GetId();
		if (id == 0 && m_pWindows.Size() > 1)
		{
			g_Logger.LogError("Can't remove main window if other windows are still open!");
			return false;
		}

		b8 res = m_pWindows.Erase(id); // <- returns 1 (true) if window could be removed
		res |= pWindow->Close();
		HvDelete pWindow;
		m_FreeIds.Push(id);
		return res;
	}

	b8 WindowManager::DestroyWindow(u8 id)
	{
		if (id == 0 && m_pWindows.Size() > 1)
		{
			g_Logger.LogError("Can't remove main window if other windows are still open!");
			return false;
		}

		Window* pWindow = GetWindowFromId(id);
		b8 res = b8(m_pWindows.Erase(pWindow->m_Id)); // <- returns 1 (true) if window could be removed
		if (pWindow->IsOpen())
			res &= pWindow->Close();
		HvDelete pWindow;
		m_FreeIds.Push(id);

		return res;
	}

	void WindowManager::Tick()
	{
		for (auto pair : m_pWindows)
		{
			pair.second->Update();
		}
	}

	Window* WindowManager::GetWindowFromId(u16 id) const
	{
		if (id == Window::InvalidId)
			return nullptr;
		if (id == 0)
			return m_pMainWindow;

		auto it = m_pWindows.Find(id);
		if (it == m_pWindows.Back())
			return nullptr;
		return it->second;
	}

	Window* WindowManager::GetWindowInFocus() const
	{
		if (m_InFocusId == Window::InvalidId)
			return nullptr;
		if (m_InFocusId == 0)
			return m_pMainWindow;

		auto it = m_pWindows.Find(m_InFocusId);
		if (it == m_pWindows.Back())
			return nullptr;
		return it->second;
	}

	b8 WindowManager::IsAnyWindowOpen()
	{
		for (Pair<u16, Window*> pair : m_pWindows)
		{
			if (pair.second->IsOpen())
				return true;
		}
		return false;
	}

	b8 WindowManager::Shutdown()
	{
		b8 res = true;
		for (auto pair : m_pWindows)
		{
			if (pair.second->IsOpen())
			{
				res &= pair.second->Close();
				HvDelete pair.second;
			}
		}
		m_pWindows.Clear();

		return res;
	}

	b8 WindowManager::HandleEvents(const System::WindowEvent& wndEvent)
	{
		Window* pWindow = GetWindowFromId(wndEvent.windowId);
		if (!pWindow)
			return false;
		switch (wndEvent.type)
		{
		case System::WindowEventType::Close:
			if (pWindow->OnClose())
				pWindow->Close();
			if (wndEvent.windowId != 0)
				HvDelete pWindow;
			return true;
		case System::WindowEventType::Shown:
			pWindow->OnShown();
			return true;
		case System::WindowEventType::Hidden:
			pWindow->OnHidden();
			return true;
		case System::WindowEventType::Restored:
			pWindow->OnRestored();
			return true;
		case System::WindowEventType::Minimized:
			pWindow->OnMinimized();
			return true;
		case System::WindowEventType::Maximized:
			pWindow->OnMaximized();
			return true;
		case System::WindowEventType::Resized:
			pWindow->OnResized(wndEvent.data0, wndEvent.data1);
			return true;
		case System::WindowEventType::UserResized:
			pWindow->OnUserResizing(wndEvent.data0, wndEvent.data1);
			return true;
		case System::WindowEventType::Moved:
			pWindow->OnMoved(wndEvent.data0, wndEvent.data1);
			return true;
		case System::WindowEventType::FocusGained:
			pWindow->OnFocusGained();
			m_InFocusId = wndEvent.windowId;
			break;
		case System::WindowEventType::FocusLost:
			pWindow->OnFocusLost();
			m_InFocusId = Window::InvalidId;
			break;
			break;
		case System::WindowEventType::StartGrab:
			m_IsAnyWindowGrabbed = true;
			pWindow->OnStartGrab();
			return true;
		case System::WindowEventType::EndGrab:
			m_IsAnyWindowGrabbed = false;
			pWindow->OnEndGrab();
			return true;
		default:
			break;
		}
		return false;
	}

	WindowManager& GetWindowManager()
	{
		static WindowManager manager;
		return manager;
	}

}