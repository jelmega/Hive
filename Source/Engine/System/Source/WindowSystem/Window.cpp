// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Window.cpp: Window
#include "SystemPCH.h"
#include "WindowSystem/Window.h"
#include "WindowSystem/Monitor.h"
#include "WindowSystem/WindowManager.h"

namespace Hv {
	
	Window::Window()
		: m_Width(0)
		, m_Height(0)
		, m_XPos(0)
		, m_YPos(0)
		, m_Style(WindowStyle::Default)
		, m_Flags(WindowFlags::None)
		, m_Id(InvalidId)
		, m_Handle(WindowSystem::InvalidWindowHandle)
		, m_UserData()
	{
	}

	Window::~Window()
	{
	}

	void Window::Update()
	{
		if (m_Handle != WindowSystem::InvalidWindowHandle)
			HAL::WindowSystem::UpdateWindow(m_Handle);
		//SwapBuffers();
	}

	b8 Window::Close()
	{
		HV_LOG_INFO_ON_COND(m_Id == 0, "Closing main window!");
		if (m_Handle == WindowSystem::InvalidWindowHandle)
		{
			g_Logger.LogWarning("Trying to close closed window!");
			return false;
		}

		b8 res = HAL::WindowSystem::CloseWindow(m_Handle);

		if (res)
			m_Handle = WindowSystem::InvalidWindowHandle;

		return res;
	}

	void Window::SetSize(u32 width, u32 height)
	{
		if (!(m_Flags & WindowFlags::Resizable))
			return;
		if (m_Handle != WindowSystem::InvalidWindowHandle)
		{
			m_Width = width;
			m_Height = height;
			HAL::WindowSystem::ResizeWindow(m_Handle, width, height, m_Style);
		}
	}

	void Window::SetPosition(i32 x, i32 y)
	{
		if (m_Handle != WindowSystem::InvalidWindowHandle)
		{
			m_XPos = x;
			m_YPos = y;
			HAL::WindowSystem::MoveWindow(m_Handle, x, y, m_Style);
		}
	}

	i32rect Window::GetAdjustedWindowRect() const
	{
		return HAL::WindowSystem::GetAdjustedWindowRect(i32rect(m_XPos, m_YPos, m_XPos + m_Width, m_YPos + m_Height), m_Style);
	}

	void Window::SetTitle(const String& title)
	{
		if (m_Handle != WindowSystem::InvalidWindowHandle)
		{
			m_Title = title;
			HAL::WindowSystem::SetWindowTitle(m_Handle, title);
		}
	}

	void Window::Minimize()
	{
		if (m_Handle != WindowSystem::InvalidWindowHandle)
		{
			m_Flags |= WindowFlags::Minimized;
			m_Flags &= ~WindowFlags::Maximized;
			HAL::WindowSystem::MinimizeWindow(m_Handle);
		}
	}

	void Window::Maximize()
	{
		if (m_Handle != WindowSystem::InvalidWindowHandle)
		{
			m_Flags |= WindowFlags::Maximized;
			m_Flags &= ~WindowFlags::Minimized;
			HAL::WindowSystem::MaximizeWindow(m_Handle);
		}
	}

	void Window::Restore()
	{
		if (m_Handle != WindowSystem::InvalidWindowHandle)
		{
			m_Flags &= ~(WindowFlags::Minimized | WindowFlags::Maximized);
			HAL::WindowSystem::RestoreWindow(m_Handle);
		}
	}

	////////////////////////////////////////////////////////////////////////////////

	void Window::Create(const WindowDesc& desc, u8 id, SystemHandle& sysHandle)
	{
		m_Width = desc.width;
		m_Height = desc.height;
		m_Title = desc.title;
		m_Style = desc.style;
		m_Id = id;

		m_Handle = HAL::WindowSystem::CreateWindow(desc.width, desc.height, desc.title, (u8)desc.style, id, sysHandle, &m_UserData);

		b8 moveWnd = false;
		i32rect rc = GetAdjustedWindowRect();
		i32 x = rc.left;
		i32 y = rc.top;
		i32 width = rc.right - rc.left;
		i32 height = rc.bottom - rc.top;
		HAL::WindowSystem::GetWindowPosition(m_Handle, x, y);
		const Monitor& mon = desc.monitor;
		if (desc.x == HV_WINDOW_CENTERED && mon.IsValid())
		{
			x = mon.GetXPosition() + (i32(mon.GetWidth()) - width) / 2;
			moveWnd = true;
		}
		if (desc.y == HV_WINDOW_CENTERED && mon.IsValid())
		{
			y = mon.GetXPosition() + (i32(mon.GetHeight()) - height) / 2;
			moveWnd = true;
		}
		m_XPos = x;
		m_YPos = y;

		if (moveWnd)
			HAL::WindowSystem::MoveWindow(m_Handle, x, y, m_Style);

		if (desc.style == WindowStyle::None)
			m_Flags = WindowFlags::Borderless;
	}

	////////////////////////////////////////////////////////////////////////////////
	// Calbacks
	////////////////////////////////////////////////////////////////////////////////
	void Window::OnShown()
	{
		m_Flags &= ~WindowFlags::Hidden;
		if (m_ShownCallback.CanInvoke())
			m_ShownCallback();
	}

	void Window::OnHidden()
	{
		m_Flags |= WindowFlags::Hidden;
		if (m_HiddenCallback.CanInvoke())
			m_HiddenCallback();
	}

	b8 Window::OnClose()
	{
		b8 res = true;
		if (m_CloseCallback.CanInvoke())
			res &= m_CloseCallback();
		if (m_RendererUnregisterCallback.CanInvoke())
			res &= m_RendererUnregisterCallback(m_Id);
		return res;
	}

	void Window::OnResized(u32 width, u32 height)
	{
		m_Width = width;
		m_Height = height;
		if (m_ResizedCallback.CanInvoke())
			m_ResizedCallback(width, height);
	}

	void Window::OnUserResizing(u32 width, u32 height)
	{
		m_Width = width;
		m_Height = height;
		m_Flags |= WindowFlags::WasResized;
		if (m_UserResizingCallback.CanInvoke())
			m_UserResizingCallback(width, height);
	}

	void Window::OnMoved(i32 x, i32 y)
	{
		m_XPos = x;
		m_YPos = y;
		if (m_MovedCallback.CanInvoke())
			m_MovedCallback(x, y);
	}

	void Window::OnRestored()
	{
		m_Flags &= ~(WindowFlags::Minimized | WindowFlags::Maximized);
		if (m_RendererResizeCallback.CanInvoke())
			m_RendererResizeCallback(m_Width, m_Height);
		if (m_RestoredCallback.CanInvoke())
			m_RestoredCallback(m_Width, m_Height);
	}

	void Window::OnMinimized()
	{
		m_Flags |= WindowFlags::Minimized;
		m_Flags &= ~WindowFlags::Maximized;
		if (m_RendererResizeCallback.CanInvoke())
			m_RendererResizeCallback(m_Width, m_Height);
		if (m_MinimizedCallback.CanInvoke())
			m_MinimizedCallback(m_Width, m_Height);
	}

	void Window::OnMaximized()
	{
		m_Flags |= WindowFlags::Maximized;
		m_Flags &= ~WindowFlags::Minimized;
		if (m_RendererResizeCallback.CanInvoke())
			m_RendererResizeCallback(m_Width, m_Height);
		if (m_MaximizedCallback.CanInvoke())
			m_MaximizedCallback(m_Width, m_Height);
	}

	void Window::OnFocusGained()
	{
		m_Flags |= WindowFlags::InFocus;
		if (m_FocuGainedCallback.CanInvoke())
			m_FocuGainedCallback();
	}

	void Window::OnFocusLost()
	{
		m_Flags &= ~WindowFlags::InFocus;
		if (m_FocusLostCallback.CanInvoke())
			m_FocusLostCallback();
	}

	void Window::OnStartGrab()
	{
		m_Flags |= WindowFlags::Grabbed;
		if (m_StartGrabCallback.CanInvoke())
			m_StartGrabCallback();
	}

	void Window::OnEndGrab()
	{
		m_Flags &= ~WindowFlags::Grabbed;
		if (m_EndGrabCallback.CanInvoke())
			m_EndGrabCallback();
		if (m_Flags & WindowFlags::WasResized)
		{
			if (m_RendererResizeCallback.CanInvoke())
				m_RendererResizeCallback(m_Width, m_Height);
			if (m_UserResizeEndedCallback.CanInvoke())
				m_UserResizeEndedCallback(m_Width, m_Height);
			m_Flags &= ~WindowFlags::WasResized;
		}
	}

}
