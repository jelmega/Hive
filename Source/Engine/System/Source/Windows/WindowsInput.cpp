// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// WindowsInput.cpp: Windows input
#include "SystemPCH.h"
#include "HAL/HalInput.h"

#if HV_PLATFORM_WINDOWS

namespace Hv::HAL::Input {


	VKey Win32KeyToVKey(u32 vk)
	{
		// Keys are in the same order as VKey
		switch (vk)
		{
			// Letters
		case 0x41:
			return VKey::A;
		case 0x42:
			return VKey::B;
		case 0x43:
			return VKey::C;
		case 0x44:
			return VKey::D;
		case 0x45:
			return VKey::E;
		case 0x46:
			return VKey::F;
		case 0x47:
			return VKey::G;
		case 0x48:
			return VKey::H;
		case 0x49:
			return VKey::I;
		case 0x4a:
			return VKey::J;
		case 0x4b:
			return VKey::K;
		case 0x4c:
			return VKey::L;
		case 0x4d:
			return VKey::M;
		case 0x4e:
			return VKey::N;
		case 0x4f:
			return VKey::O;
		case 0x50:
			return VKey::P;
		case 0x51:
			return VKey::Q;
		case 0x52:
			return VKey::R;
		case 0x53:
			return VKey::S;
		case 0x54:
			return VKey::T;
		case 0x55:
			return VKey::U;
		case 0x56:
			return VKey::V;
		case 0x57:
			return VKey::W;
		case 0x58:
			return VKey::X;
		case 0x59:
			return VKey::Y;
		case 0x5a:
			return VKey::Z;

			// Numbers
		case 0x30:
			return VKey::Num0;
		case 0x31:
			return VKey::Num1;
		case 0x32:
			return VKey::Num2;
		case 0x33:
			return VKey::Num3;
		case 0x34:
			return VKey::Num4;
		case 0x35:
			return VKey::Num5;
		case 0x36:
			return VKey::Num6;
		case 0x37:
			return VKey::Num7;
		case 0x38:
			return VKey::Num8;
		case 0x39:
			return VKey::Num9;

			// Modifiers
		case VK_ESCAPE:
			return VKey::Escape;
		case VK_SHIFT: // Interpreted as LShift
		case VK_LSHIFT:
			return VKey::LShift;
		case VK_CONTROL: // Interpreted as LCtrl
		case VK_LCONTROL:
			return VKey::LCtrl;
		case VK_MENU: // Interpreted as LAlt
		case VK_LMENU:
			return VKey::LAlt;
		case VK_LWIN:
			return VKey::LSystem;
		case VK_RSHIFT:
			return VKey::RShift;
		case VK_RCONTROL:
			return VKey::RCtrl;
		case VK_RMENU:
			return VKey::RAlt;
		case VK_RWIN:
			return VKey::RSystem;

			// US QWERTY specific
		case VK_OEM_4:
			return VKey::LBracket;
		case VK_OEM_6:
			return VKey::RBracket;
		case VK_OEM_1:
			return VKey::SemiColon;
		case VK_OEM_COMMA:
			return VKey::Comma;
		case VK_OEM_PERIOD:
			return VKey::Period;
		case VK_OEM_7:
			return VKey::Quote;
		case VK_OEM_2:
			return VKey::Slash;
		case VK_OEM_5:
			return VKey::BackSlash;
		case VK_OEM_3:
			return VKey::Tilde;
		case VK_OEM_PLUS:
			return VKey::Equal;
		case VK_OEM_MINUS:
			return VKey::Dash;

			// Special
		case VK_SPACE:
			return VKey::Space;
		case VK_RETURN:
			return VKey::Enter;
		case VK_BACK:
			return VKey::BackSpace;
		case VK_TAB:
			return VKey::Tab;
		case VK_PRIOR:
			return VKey::PageUp;
		case VK_NEXT:
			return VKey::PageDown;
		case VK_END:
			return VKey::End;
		case VK_HOME:
			return VKey::Home;
		case VK_INSERT:
			return VKey::Insert;
		case VK_DELETE:
			return VKey::Delete;
		case VK_CAPITAL:
			return VKey::CapsLock;

			// Arrows
		case VK_LEFT:
			return VKey::Left;
		case VK_UP:
			return VKey::Up;
		case VK_RIGHT:
			return VKey::Right;
		case VK_DOWN:
			return VKey::Down;

			// Numpad
		case VK_NUMPAD0:
			return VKey::Numpad0;
		case VK_NUMPAD1:
			return VKey::Numpad1;
		case VK_NUMPAD2:
			return VKey::Numpad2;
		case VK_NUMPAD3:
			return VKey::Numpad3;
		case VK_NUMPAD4:
			return VKey::Numpad4;
		case VK_NUMPAD5:
			return VKey::Numpad5;
		case VK_NUMPAD6:
			return VKey::Numpad6;
		case VK_NUMPAD7:
			return VKey::Numpad7;
		case VK_NUMPAD8:
			return VKey::Numpad8;
		case VK_NUMPAD9:
			return VKey::Numpad9;
		case VK_DIVIDE:
			return VKey::NumpadDivide;
		case VK_MULTIPLY:
			return VKey::NumpadMultiply;
		case VK_ADD:
			return VKey::NumpadMinus;
		case VK_SUBTRACT:
			return VKey::NumpadPlus;
		case VK_SEPARATOR:
			return VKey::NumpadPeriod;

			// Function keys
		case VK_F1:
			return VKey::F1;
		case VK_F2:
			return VKey::F2;
		case VK_F3:
			return VKey::F3;
		case VK_F4:
			return VKey::F4;
		case VK_F5:
			return VKey::F5;
		case VK_F6:
			return VKey::F6;
		case VK_F7:
			return VKey::F7;
		case VK_F8:
			return VKey::F8;
		case VK_F9:
			return VKey::F9;
		case VK_F10:
			return VKey::F10;
		case VK_F11:
			return VKey::F11;
		case VK_F12:
			return VKey::F12;
		case VK_F13:
			return VKey::F13;
		case VK_F14:
			return VKey::F14;
		case VK_F15:
			return VKey::F15;
		case VK_F16:
			return VKey::F16;
		case VK_F17:
			return VKey::F17;
		case VK_F18:
			return VKey::F18;
		case VK_F19:
			return VKey::F19;
		case VK_F20:
			return VKey::F20;
		case VK_F21:
			return VKey::F21;
		case VK_F22:
			return VKey::F22;
		case VK_F23:
			return VKey::F23;
		case VK_F24:
			return VKey::F24;
		default:
			return VKey::None;
		}
	}

	b8 GetMousePosition(i32& x, i32& y)
	{
		POINT pos;
		b8 res = ::GetCursorPos(&pos);
		x = pos.x;
		y = pos.y;
		return res;
	}

}

#endif