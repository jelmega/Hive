// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// WindowsWindowSystem.cpp: Windows window system
#include "SystemPCH.h"
#include "Windows/WindowsWindowSystem.h"
#include "System/SystemEvent.h"
#include "HAL/HalSystem.h"
#include "WindowSystem/WindowManager.h"
#include "HAL/HalInput.h"

#ifdef HV_PLATFORM_WINDOWS

namespace Hv::HAL::WindowSystem {
	
	////////////////////////////////////////////////////////////////////////////////
	//	Monitor
	////////////////////////////////////////////////////////////////////////////////
	struct MonitorEnumData
	{
		HMONITOR monitor = nullptr;
		const WideChar* devName;
	};

	// Let's hope monitors are enumerated over in the right order
	BOOL MonitorEnumProc(HMONITOR monitor, HDC dc, LPRECT rc, LPARAM data)
	{
		HV_UNREFERENCED_PARAM(dc);
		HV_UNREFERENCED_PARAM(rc);

		MonitorEnumData* med = (MonitorEnumData*)data;
		MONITORINFOEXW mi;
		mi.cbSize = sizeof(MONITORINFOEXW);
		::GetMonitorInfoW(monitor, &mi);
		if (StringUtils::Compare(med->devName, mi.szDevice) == 0)
		{
			med->monitor = monitor;
			return false;
		}
		return true;
	}

	MonitorHandle GetMonitorFromIndex(i8 idx)
	{
		WindowsMonitorHandle* pHandle = HvNew WindowsMonitorHandle;

		DISPLAY_DEVICEW displayDev;
		displayDev.cb = sizeof(DISPLAY_DEVICEW);
		if (idx == -1)
		{
			for (u8 i = 0; EnumDisplayDevicesW(nullptr, i, &displayDev, 0); ++i)
			{
				if (displayDev.StateFlags & DISPLAY_DEVICE_PRIMARY_DEVICE)
					break;
			}
		}
		else
		{
			u8 j = 0;
			for (u8 i = 0; EnumDisplayDevicesW(nullptr, i, &displayDev, 0); ++i)
			{
				if (displayDev.StateFlags & DISPLAY_DEVICE_ACTIVE)
				{
					if (j == idx)
						break;
					++j;
				}
			}
		}
		Hv::Memory::Copy(pHandle->devName, displayDev.DeviceName, CCHDEVICENAME * sizeof(WideChar));

		MonitorEnumData data;
		data.devName = pHandle->devName;
		EnumDisplayMonitors(nullptr, nullptr, &MonitorEnumProc, LPARAM(&data));

		pHandle->handle = data.monitor;
		return (MonitorHandle)pHandle;
	}

	b8 GetMonitorRect(MonitorHandle& handle, i32rect& rc)
	{
		WindowsMonitorHandle* pMonHandle = (WindowsMonitorHandle*)handle;
		MONITORINFO mi;
		mi.cbSize = sizeof(MONITORINFO);
		b8 res = ::GetMonitorInfo(pMonHandle->handle, &mi);
		rc.left = mi.rcMonitor.left;
		rc.top = mi.rcMonitor.top;
		rc.right = mi.rcMonitor.right;
		rc.bottom = mi.rcMonitor.bottom;
		return res;
	}

	b8 GetMonitorRefreshRate(MonitorHandle& handle, u32& refreshRate)
	{
		WindowsMonitorHandle* pMonHandle = (WindowsMonitorHandle*)handle;
		DEVMODE dm;
		dm.dmSize = sizeof(DEVMODE);

		b8 res = EnumDisplaySettingsW(pMonHandle->devName, ENUM_CURRENT_SETTINGS, &dm);
		if (!res)
			return false;
		refreshRate = dm.dmDisplayFrequency;
		return true;
	}

	void DestroyMonitorHandle(MonitorHandle& handle)
	{
		delete (WindowsMonitorHandle*)handle;
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Window
	////////////////////////////////////////////////////////////////////////////////
	DWORD WindowStyleToWinStyle(u8 style)
	{
		WindowStyle wstyle = WindowStyle(style);
		DWORD wndStyle = 0;
		if (HV_IS_ENUM_FLAG_SET(wstyle, WindowStyle::CloseBox))
		{
			wndStyle |= WS_SYSMENU;
			wndStyle |= WS_CAPTION;
			if (HV_IS_ENUM_FLAG_SET(wstyle, WindowStyle::UserResizable))
				wndStyle |= WS_THICKFRAME;
			else
				wndStyle |= WS_BORDER;
		}
		else if (HV_IS_ENUM_FLAG_SET(wstyle, WindowStyle::Caption))
		{
			wndStyle |= WS_CAPTION;
			if (HV_IS_ENUM_FLAG_SET(wstyle, WindowStyle::UserResizable))
				wndStyle |= WS_THICKFRAME;
			else
				wndStyle |= WS_BORDER;
		}
		else if (HV_IS_ENUM_FLAG_SET(wstyle, WindowStyle::Border))
		{
			if (HV_IS_ENUM_FLAG_SET(wstyle, WindowStyle::UserResizable))
				wndStyle |= WS_THICKFRAME;
			else
				wndStyle |= WS_BORDER;
		}
		if (HV_IS_ENUM_FLAG_SET(wstyle, WindowStyle::MaximizeBox))
			wndStyle |= WS_MINIMIZEBOX;
		if (HV_IS_ENUM_FLAG_SET(wstyle, WindowStyle::MinimizeBox))
			wndStyle |= WS_MAXIMIZEBOX;
		return wndStyle;
	}

	DWORD WindowStyleToWinStyleEx(u8 style)
	{
		WindowStyle wstyle = WindowStyle(style);
		DWORD wndExStyle = 0;
		if (HV_IS_ENUM_FLAG_SET(wstyle, WindowStyle::InnerEdge))
			wndExStyle |= WS_EX_WINDOWEDGE;
		if (HV_IS_ENUM_FLAG_SET(wstyle, WindowStyle::OuterEdge))
			wndExStyle |= WS_EX_CLIENTEDGE;
		return wndExStyle;
	}

	// PlatformWindow

	LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		WindowUserData* pPwud = (WindowUserData*)::GetWindowLongPtrW(hWnd, GWLP_USERDATA);

		if (!pPwud)
			return DefWindowProc(hWnd, msg, wParam, lParam);

		Hv::System::SystemEvent event;
		b8 res = false;

		// Extract and convert to SystemEvent
		switch (msg)
		{
			// Window events
		case WM_CLOSE:
		{
			event.type = Hv::System::SystemEventType::Window;
			event.windowEvent.type = Hv::System::WindowEventType::Close;
			event.windowEvent.windowId = pPwud->id;
			break;
		}
		case WM_WINDOWPOSCHANGED:
		{
			const WINDOWPOS& wndPos = *((WINDOWPOS*)lParam);
			event.type = Hv::System::SystemEventType::Window;
			event.windowEvent.windowId = pPwud->id;
			if (!(wndPos.flags & SWP_NOSIZE))
			{
				event.windowEvent.type = Hv::System::WindowEventType::Resized;
				WindowStyle style = g_WindowManager.GetWindowFromId(pPwud->id)->GetWindowStyle();
				i32rect rc = GetAdjustedWindowRect(i32rect(0, 0, 0, 0), u8(style));
				event.windowEvent.data0 = wndPos.cx - (rc.right - rc.left);
				event.windowEvent.data1 = wndPos.cy - (rc.bottom - rc.top);
				res |= pPwud->msgDelegate(event);
			}
			if (!(wndPos.flags & SWP_NOMOVE))
			{
				event.windowEvent.type = Hv::System::WindowEventType::Moved;
				event.windowEvent.data0 = wndPos.x;
				event.windowEvent.data1 = wndPos.y;
				res |= pPwud->msgDelegate(event);
			}
			if (wndPos.flags & SWP_SHOWWINDOW)
			{
				event.windowEvent.type = Hv::System::WindowEventType::Shown;
				res |= pPwud->msgDelegate(event);
			}
			if (wndPos.flags & SWP_HIDEWINDOW)
			{
				event.windowEvent.type = Hv::System::WindowEventType::Hidden;
				res |= pPwud->msgDelegate(event);
			}
			if (!(wndPos.flags & SWP_NOZORDER) || !(wndPos.flags & SWP_NOREDRAW))
				return DefWindowProc(hWnd, msg, wParam, lParam);
			if (res)
				return 0;
			event.type = Hv::System::SystemEventType::Unknown;
			break;
		}
		case WM_SIZE:
		{
			event.type = Hv::System::SystemEventType::Window;
			event.windowEvent.windowId = pPwud->id;
			switch (wParam)
			{
			case SIZE_MAXIMIZED:
				event.windowEvent.type = Hv::System::WindowEventType::Maximized;
				break;
			case SIZE_MINIMIZED:
				event.windowEvent.type = Hv::System::WindowEventType::Minimized;
				break;
			case SIZE_RESTORED:
				event.windowEvent.type = Hv::System::WindowEventType::Restored;
				break;
			default:
				break;
			}
			break;
		}
		case WM_ENTERSIZEMOVE:
		{
			event.type = Hv::System::SystemEventType::Window;
			event.windowEvent.type = Hv::System::WindowEventType::StartGrab;
			event.windowEvent.windowId = pPwud->id;
			break;
		}
		case WM_EXITSIZEMOVE: // if message is not recieved, use WM_CAPTURECHANGED (if the window is grabbed and menu is used) ( https://stackoverflow.com/questions/1826165/wm-entersizemove-wm-exitsizemove-when-using-menu-not-always-paired )
		{
			event.type = Hv::System::SystemEventType::Window;
			event.windowEvent.type = Hv::System::WindowEventType::EndGrab;
			event.windowEvent.windowId = pPwud->id;
			break;
		}
		case WM_SIZING:
		{
			event.type = Hv::System::SystemEventType::Window;
			event.windowEvent.type = Hv::System::WindowEventType::UserResized;
			event.windowEvent.windowId = pPwud->id;
			RECT wndRect = *((RECT*)lParam);
			WindowStyle style = g_WindowManager.GetWindowFromId(pPwud->id)->GetWindowStyle();
			i32rect rc = GetAdjustedWindowRect(i32rect(0, 0, 0, 0), u8(style));
			event.windowEvent.data0 = (wndRect.right - wndRect.left) - (rc.right - rc.left);
			event.windowEvent.data1 = (wndRect.bottom - wndRect.top) - (rc.bottom - rc.top);
			break;
		}
		case WM_MOVE:
		{
			event.type = Hv::System::SystemEventType::Window;
			event.windowEvent.type = Hv::System::WindowEventType::Moved;
			event.windowEvent.windowId = pPwud->id;
			event.windowEvent.data0 = u32(LOWORD(lParam));
			event.windowEvent.data1 = u32(HIWORD(lParam));
			break;
		}

		case WM_KILLFOCUS:
		{
			event.type = Hv::System::SystemEventType::Window;
			event.windowEvent.type = Hv::System::WindowEventType::FocusLost;
			event.windowEvent.windowId = pPwud->id;
			break;
		}
		case WM_SETFOCUS:
		{
			event.type = Hv::System::SystemEventType::Window;
			event.windowEvent.type = Hv::System::WindowEventType::FocusGained;
			event.windowEvent.windowId = pPwud->id;
			break;
		}

		// Input
		case WM_KEYDOWN:
		{
			event.type = Hv::System::SystemEventType::Input;
			event.inputEvent.type = Hv::System::InputEventType::KeyPressed;
			event.inputEvent.key.key = Input::Win32KeyToVKey(i32(wParam));
			break;
		}
		case WM_KEYUP:
		{
			event.type = Hv::System::SystemEventType::Input;
			event.inputEvent.type = Hv::System::InputEventType::KeyReleased;
			event.inputEvent.key.key = Input::Win32KeyToVKey(i32(wParam));
			break;
		}
		case WM_LBUTTONDOWN:
		{
			event.type = Hv::System::SystemEventType::Input;
			event.inputEvent.type = Hv::System::InputEventType::MouseButtonPressed;
			event.inputEvent.mouse.button = MouseButton::LButton;
			break;
		}
		case WM_LBUTTONUP:
		case WM_NCLBUTTONUP: // Make sure mouse up is registered, even when the mouse is not in the window
		{
			event.type = Hv::System::SystemEventType::Input;
			event.inputEvent.type = Hv::System::InputEventType::MouseButtonReleased;
			event.inputEvent.mouse.button = MouseButton::LButton;
			break;
		}
		case WM_MBUTTONDOWN:
		{
			event.type = Hv::System::SystemEventType::Input;
			event.inputEvent.type = Hv::System::InputEventType::MouseButtonPressed;
			event.inputEvent.mouse.button = MouseButton::MButton;
			break;
		}
		case WM_MBUTTONUP:
		case WM_NCMBUTTONUP: // Make sure mouse up is registered, even when the mouse is not in the window
		{
			event.type = Hv::System::SystemEventType::Input;
			event.inputEvent.type = Hv::System::InputEventType::MouseButtonReleased;
			event.inputEvent.mouse.button = MouseButton::MButton;
			break;
		}
		case WM_RBUTTONDOWN:
		{
			event.type = Hv::System::SystemEventType::Input;
			event.inputEvent.type = Hv::System::InputEventType::MouseButtonPressed;
			event.inputEvent.mouse.button = MouseButton::RButton;
			break;
		}
		case WM_RBUTTONUP:
		case WM_NCRBUTTONUP: // Make sure mouse up is registered, even when the mouse is not in the window
		{
			event.type = Hv::System::SystemEventType::Input;
			event.inputEvent.type = Hv::System::InputEventType::MouseButtonReleased;
			event.inputEvent.mouse.button = MouseButton::RButton;
			break;
		}
		case WM_XBUTTONDOWN:
		{
			event.type = Hv::System::SystemEventType::Input;
			event.inputEvent.type = Hv::System::InputEventType::MouseButtonPressed;
			switch (HIWORD(wParam))
			{
			case XBUTTON1:
				event.inputEvent.mouse.button = MouseButton::Button4;
				break;
			case XBUTTON2:
				event.inputEvent.mouse.button = MouseButton::Button5;
				break;
			default:
				event.type = Hv::System::SystemEventType::Unknown; // Should never reach
				break;
			}
			break;
		}
		case WM_XBUTTONUP:
		case WM_NCXBUTTONUP: // Make sure mouse up is registered, even when the mouse is not in the window
		{
			event.type = Hv::System::SystemEventType::Input;
			event.inputEvent.type = Hv::System::InputEventType::MouseButtonReleased;
			switch (HIWORD(wParam))
			{
			case XBUTTON1:
				event.inputEvent.mouse.button = MouseButton::Button4;
				break;
			case XBUTTON2:
				event.inputEvent.mouse.button = MouseButton::Button5;
				break;
			default:
				event.type = Hv::System::SystemEventType::Unknown; // Should never reach
				break;
			}
			break;
		}
		case WM_MOUSEWHEEL:
		{
			event.type = Hv::System::SystemEventType::Input;
			event.inputEvent.type = Hv::System::InputEventType::MouseScroll;
			event.inputEvent.mouse.scroll = f32(i16(HIWORD(wParam))) / f32(WHEEL_DELTA);
			break;
		}
		case WM_MOUSEMOVE:
		{
			event.type = Hv::System::SystemEventType::Input;
			event.inputEvent.type = Hv::System::InputEventType::MouseMove;
			event.inputEvent.mouse.xPos = GET_X_LPARAM(lParam);
			event.inputEvent.mouse.yPos = GET_Y_LPARAM(lParam);
			break;
		}

		default:
			break;
		}

		// Process event
		if (event.type == Hv::System::SystemEventType::Unknown)
		{
			event.unknownEvent.id = msg;
			event.unknownEvent.data0 = u32(wParam);
			event.unknownEvent.data1 = u32(lParam);
		}
		res |= pPwud->msgDelegate(event);

		return res ? 0 : DefWindowProc(hWnd, msg, wParam, lParam);
	}

	WindowHandle CreateWindow(u32 width, u32 height, const String& title, u8 style, u16 id, SystemHandle& sysHandle, void* pUserData)
	{
		static String className = "HiveWndClass";
		static b8 isClassCreated = false;

		// Create class and register
		if (!isClassCreated)
		{
			WNDCLASSEXW wcx;
			wcx.cbSize = sizeof(WNDCLASSEX);
			wcx.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
			wcx.lpfnWndProc = WndProc;
			wcx.cbClsExtra = 0;
			wcx.cbWndExtra = 0;
			wcx.hInstance = (HINSTANCE)sysHandle;
			wcx.hIcon = wcx.hIconSm = LoadIcon(nullptr, IDI_APPLICATION);
			wcx.hCursor = LoadCursor(nullptr, IDC_ARROW);
			wcx.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
			wcx.lpszMenuName = nullptr;
			wcx.lpszClassName = className.CStr();

			if (!RegisterClassExW(&wcx))
			{
				g_Logger.LogError("Coulnd't register a windows WNDCLASSEX!");
				return WindowHandle();
			}
			isClassCreated = true;
		}

		// Concert the style to win32 style
		DWORD wndStyle = WindowStyleToWinStyle(style);
		DWORD wndExStyle = WindowStyleToWinStyleEx(style);

		// Adjust window size to style
		RECT rc;
		rc.left = 0;
		rc.right = width;
		rc.top = 0;
		rc.bottom = height;
		AdjustWindowRectEx(&rc, wndStyle, false, wndExStyle);
		width = rc.right - rc.left;
		height = rc.bottom - rc.top;

		// Create window
		WindowsWindowHandle* pHandle = HvNew WindowsWindowHandle();
		pHandle->instance = (HINSTANCE)sysHandle;
		pHandle->handle = CreateWindowExW(wndExStyle,
			className.CStr(),
			title.CStr(),
			wndStyle,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			i32(width),
			i32(height),
			nullptr,
			nullptr,
			(HINSTANCE)sysHandle,
			nullptr);
		if (!pHandle->handle)
		{
			g_Logger.LogError("Couldn't create win32 window!");
			HvDelete pHandle;
			return Hv::WindowSystem::InvalidWindowHandle;
		}

		// If style is set to none, disable window style, CreateWindowW adds border and caption styles when no flags are set
		if (style == u8(WindowStyle::None))
		{
			::SetWindowLongPtrW(pHandle->handle, GWL_STYLE, 0);

		}

		// Set userdata
		::SetWindowLongPtrW((HWND)pHandle->handle, GWLP_USERDATA, (LONG_PTR)pUserData);

		// Show and update window
		::ShowWindow((HWND)pHandle->handle, SW_SHOW);
		::UpdateWindow((HWND)pHandle->handle);

		return (WindowHandle)pHandle;
	}

	b8 CloseWindow(WindowHandle& handle)
	{
		WindowsWindowHandle* pWinHandle = (WindowsWindowHandle*)handle;
		b8 res = ::DestroyWindow((HWND)pWinHandle->handle);
		HvDelete pWinHandle;
		return res;
	}

	b8 UpdateWindow(WindowHandle& handle)
	{
		WindowsWindowHandle* pWinHandle = (WindowsWindowHandle*)handle;
		return ::UpdateWindow((HWND)pWinHandle->handle);
	}

	b8 ResizeWindow(WindowHandle& handle, u32 width, u32 height, u8 style)
	{
		// Concert the style to win32 style
		DWORD wndStyle = WindowStyleToWinStyle(style);
		DWORD wndExStyle = WindowStyleToWinStyleEx(style);

		// Adjust window size to style
		RECT rc;
		WindowsWindowHandle* pWinHandle = (WindowsWindowHandle*)handle;
		if (!::GetWindowRect((HWND)pWinHandle->handle, &rc))
			return false;
		rc.right = rc.left + width;
		rc.bottom = rc.top + height;
		AdjustWindowRectEx(&rc, wndStyle, false, wndExStyle);
		width = rc.right - rc.left;
		height = rc.bottom - rc.top;

		// Set window size
		return ::SetWindowPos((HWND)pWinHandle->handle, nullptr, 0, 0, width, height, SWP_NOMOVE | SWP_NOZORDER);
	}

	b8 MoveWindow(WindowHandle& handle, i32 x, i32 y, u8 style)
	{
		// Concert the style to win32 style
		DWORD wndStyle = WindowStyleToWinStyle(style);
		DWORD wndExStyle = WindowStyleToWinStyleEx(style);

		// Adjust window size to style
		RECT rc;
		rc.left = x;
		rc.right = x;
		rc.top = y;
		rc.bottom = y;
		AdjustWindowRectEx(&rc, wndStyle, false, wndExStyle);
		x = rc.left + 1;

		// Set window position
		WindowsWindowHandle* pWinHandle = (WindowsWindowHandle*)handle;
		return ::SetWindowPos((HWND)pWinHandle->handle, nullptr, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
	}

	b8 SetWindowTitle(WindowHandle& handle, String title)
	{
		WindowsWindowHandle* pWinHandle = (WindowsWindowHandle*)handle;
		return ::SetWindowTextW((HWND)pWinHandle->handle, title.CStr());
	}

	b8 GetWindowPosition(const WindowHandle& handle, i32& x, i32& y)
	{
		RECT rc;
		WindowsWindowHandle* pWinHandle = (WindowsWindowHandle*)handle;
		b8 res = ::GetWindowRect((HWND)pWinHandle->handle, &rc);
		x = rc.left;
		y = rc.top;
		return res;
	}

	b8 MinimizeWindow(WindowHandle& handle)
	{
		WindowsWindowHandle* pWinHandle = (WindowsWindowHandle*)handle;
		return ::ShowWindow((HWND)pWinHandle->handle, SW_MINIMIZE);
	}

	b8 MaximizeWindow(WindowHandle& handle)
	{
		WindowsWindowHandle* pWinHandle = (WindowsWindowHandle*)handle;
		return ::ShowWindow((HWND)pWinHandle->handle, SW_MAXIMIZE);
	}

	b8 RestoreWindow(WindowHandle& handle)
	{
		WindowsWindowHandle* pWinHandle = (WindowsWindowHandle*)handle;
		return ::ShowWindow((HWND)pWinHandle->handle, SW_RESTORE);
	}

	i32rect GetAdjustedWindowRect(i32rect rc, u8 style)
	{
		RECT wrc;
		wrc.left = rc.left;
		wrc.top = rc.top;
		wrc.right = rc.right;
		wrc.bottom = rc.bottom;

		DWORD wndStyle = WindowStyleToWinStyle(style);
		DWORD wndExStyle = WindowStyleToWinStyleEx(style);
		AdjustWindowRectEx(&wrc, wndStyle, false, wndExStyle);

		return i32rect(wrc.left, wrc.top, wrc.right, wrc.bottom);
	}

}

#endif
