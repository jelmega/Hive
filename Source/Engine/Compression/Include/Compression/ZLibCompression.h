// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ZLibCompression.h: ZLib compression
#pragma once
#include "CompressionPCH.h"

namespace Hv::Compression {

	namespace Detail {

		/**
		* zlib stream
		* @note				Specification: https://www.ietf.org/rfc/rfc1950.txt
		* @note				Length is not part of the specification, but is given, so that the algorithm knows the size of the compressed data!
		*/
		struct ZlibStream
		{
			u8 compressionAndFlags;	/**< Compression methods and flags */
			u8 additionalFlags;		/**< Additional flags */
			u32 dictId;
			DynArray<u8> data;		/**< Compressed data */
			u32 checksum;			/**< Checksum */
		};

		enum class ZlibCompressionMethod : u8
		{
			Deflate = 0x08	/**< Deflate compression */
		};

		enum class ZlibCompressionLevel : u8
		{
			Fastest,	/**< Fastest algorithm */
			Fast,		/**< Fast algorithm */
			Default,	/**< Default algorithm */
			Maximum		/**< Slowest algorithm, maximum compression */
		};

		/**
		* ZLib dictionary info
		*/
		struct ZLibDictInfo
		{
			b8 allow = false;	/** If the wrapper around zlib supports dictionaries, when false, stream that use dictionaries will fail to decompress */
		};
	}

	/**
	* Decompress a zlib stream
	* @param[in] stream			Zlib stream
	* @param[out] decompressed	Decompressed data
	*/
	HIVE_API void DecompressZlib(const DynArray<u8>& stream, DynArray<u8>& decompressed);
}