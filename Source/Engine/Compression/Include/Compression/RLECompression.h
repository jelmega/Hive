// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// BitmapCompression.h: Bitmap compression
#pragma once
#include "CompressionPCH.h"

namespace Hv::Compression {

	/**
	 * Decompress Bitmap RLE4 compressed data
	 * @param[in] compressed	Compressed data
	 * @param[in] lineLength	Length of a bitmap line
	 * @param[in] data			Decompressed data
	 */
	HIVE_API void DecompressRLE4Bmp(const DynArray<u8>& compressed, u32 lineLength, DynArray<u8>& data);

	/**
	 * Decompress Bitmap RLE4 compressed data
	 * @param[in] compressed	Compressed data
	 * @param[in] lineLength	Length of a bitmap line
	 * @param[in] data			Decompressed data
	 */
	HIVE_API void DecompressRLE8Bmp(const DynArray<u8>& compressed, u32 lineLength, DynArray<u8>& data);

}
