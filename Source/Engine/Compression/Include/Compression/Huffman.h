// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Huffman.h: Huffman table
#pragma once
#include "CompressionPCH.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv {
	class BitBuffer;
}

namespace Hv::Compression {
	
	class HIVE_API Huffman
	{
	private:
		struct Node
		{
			Node();
			Node(u8 length, u32 code);

			u8 length;
			u32 code;
		};

	public:
		Huffman();
		/**
		 * Create a Huffman tree from an array of bit lengths
		 * @param[in] pArray	Array of lengths (in bits) of the tree values (at the index of their corresponding value)
		 * @param[in] length	Length of the array
		 */
		Huffman(u8* pArray, u32 length);
		/**
		 * Create a Huffman tree from an array of bit lengths
		 * @param[in] array		Array of lengths (in bits) of the tree values (at the index of their corresponding value)
		 */
		Huffman(DynArray<u8> array);
		~Huffman();

		/**
		 * Get the next symbol from the huffman tree
		 */
		u32 GetNextSymbol(BitBuffer& buffer) const;

	private:

		/***
		 * Generate the Huffman tree
		 * @param[in] pArray	Array of lengths (in bits) of the tree values (at the index of their corresponding value)
		 * @param[in] length	Length of the array
		 */
		void GenerateTree(u8* pArray, u32 length);

		u8 m_MinBits;									/**< Minimum number of bits required */
		u8 m_MaxBits;									/**< Maximum number of bits allowed */
		DynArray<Node> m_Nodes;							/**< Nodes */
		DynArray<u32> m_Offsets;						/**< Offsets */

		static constexpr u8 m_MaxHuffmanLength = 17;	/** Maximum length of a huffman value */
	};

}

#pragma warning(pop)