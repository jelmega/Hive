// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// LZWCompression.h: LZW (Lempel-Ziv-Welsh) compression
#pragma once
#include "CompressionPCH.h"

namespace Hv::Compression {
	
	/**
	 * Decompress LZW with variable code length (version modified for Gif images)
	 * @param[in] compressed	Compressed data
	 * @param[in] codeLen		Code length
	 * @param[in] decompressed	Uncompressed data
	 */
	HIVE_API void DecompressLZWGif(const DynArray<u8>& compressed, u8 codeLen, DynArray<u8>& decompressed);

}