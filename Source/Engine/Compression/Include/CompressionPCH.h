// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// CompressionPCH.h: Compression Precompiled Header
#pragma once
#include <Core.h>
#include HV_INCLUDE_RTTI_MODULE(Compression)

HV_DECLARE_LOG_CATEGORY_EXTERN(Compress)
