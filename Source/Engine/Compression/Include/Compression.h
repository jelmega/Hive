// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Compression.h: Compression main header
#pragma once
#include "CompressionPCH.h"

#include "Compression/RLECompression.h"
#include "Compression/DeflateCompression.h"
#include "Compression/ZLibCompression.h"
#include "Compression/LZWCompression.h"
