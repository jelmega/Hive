// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DeflateCompression.cpp: Deflate compression
#include "CompressionPCH.h"
#include "Compression/DeflateCompression.h"

namespace Hv::Compression {


	Huffman& Detail::GetDefaultInflateLitLenTree()
	{
		static Huffman huffmanTree;
		static bool hasHuffmanTree = false;

		if (!hasHuffmanTree)
		{
			constexpr u16 defaultLitLenSize = 288;
			u8 litLenBitLengths[defaultLitLenSize] = {};
			u16 i = 0;
			for (; i < 144; ++i)
				litLenBitLengths[i] = 8;
			for (; i < 256; ++i)
				litLenBitLengths[i] = 9;
			for (; i < 280; ++i)
				litLenBitLengths[i] = 7;
			for (; i < 288; ++i)
				litLenBitLengths[i] = 8;

			huffmanTree = Huffman(litLenBitLengths, defaultLitLenSize);
			hasHuffmanTree = true;
		}
		return huffmanTree;
	}

	Huffman& Detail::GetDefaultInflateDistTree()
	{
		static Huffman huffmanTree;
		static bool hasHuffmanTree = false;

		if (!hasHuffmanTree)
		{
			constexpr u16 defaultDistSize = 30;
			u8 litLenBitLengths[defaultDistSize] = {};
			for (u16 i = 0; i < defaultDistSize; ++i)
				litLenBitLengths[i] = 5;
			huffmanTree = Huffman(litLenBitLengths, defaultDistSize);
			hasHuffmanTree = true;
		}
		return huffmanTree;
	}

	void Inflate(const DynArray<u8>& compressed, DynArray<u8>& decompressed)
	{
		u8 bfinal = 0;

		BitBuffer bitBuffer(compressed, true);

		u32 tmp = 0;
		while (!bfinal)
		{
			bfinal = bitBuffer.GetBit();
			u8 btype = u8(bitBuffer.GetValueLSB(2));

			switch (btype)
			{
			case 0: // Uncompressed
			{
				//g_Logger.LogInfo("Deflate: uncompressed block");
				bitBuffer.SkipToByteBoundry();
				u16 len = u16(bitBuffer.GetValueLSB(16));
				u16 nlen = ~u16(bitBuffer.GetValueLSB(16));
				if (len != nlen)
				{
					g_Logger.LogError("Deflate decompression: Invalid uncompressed chunk!");
				}
				sizeT startPos = decompressed.Size();
				decompressed.Resize(startPos + len);
				bitBuffer.CopyBytesFromBoundry(decompressed.Data() + startPos, len);
			}
			break;
			case 1: // Compressed: static huffman codes
			case 2: // Compressed: dynamic huffman codes
			{
				// Set the trees to the default trees
				Huffman litLenTree;
				Huffman distTree;

				// Modify the trees based on 'btype'
				if (btype == 1)
				{
					//g_Logger.LogInfo("Inflate: static compressed block");
					litLenTree = Detail::GetDefaultInflateLitLenTree();
					distTree = Detail::GetDefaultInflateDistTree();
				}
				else
				{
					// Read the huffman alphabet lengths
					u16 hlit = u16(bitBuffer.GetValueLSB(5)) + 257;
					u8 hdist = u8(bitBuffer.GetValueLSB(5)) + 1;
					u8 hclen = u8(bitBuffer.GetValueLSB(4)) + 4;

					// alphabet buffers
					u8 codeLengths[Detail::g_InflateNumDynCodeLens] = {};
					u8 codeListLens[Detail::g_InflateMaxDynCodeLits + Detail::g_InflateMaxDynCodeDists] = {};
					// Read length alphabet
					for (u8 i = 0; i < hclen; ++i)
					{
						u8 bits = u8(bitBuffer.GetValueLSB(3));
						codeLengths[Detail::g_InflateDynCodeLenOrder[i]] = bits;
					}
					// Create the huffman tree
					Huffman codeLenTree(codeLengths, Detail::g_InflateNumDynCodeLens);

					// Read literal/length alphabet
					u16 codeListLen = hlit + hdist;
					u16 codeLenIdx = 0;
					for (u16 i = 0; i < codeListLen;)
					{
						u8 lenCode = u8(codeLenTree.GetNextSymbol(bitBuffer));
						codeLenIdx %= Detail::g_InflateNumDynCodeLens;
						if (lenCode <= 15)
						{
							codeListLens[i++] = lenCode;
						}
						else if (lenCode == 16)
						{
							if (i == 0)
							{
								g_Logger.LogError("Inflate: dynamic tree: No previous code!");
								return;
							}

							u8 repCount = 3 + u8(bitBuffer.GetValueLSB(2));
							u8 prevCode = codeListLens[i - 1];
							for (u8 j = 0; j < repCount; ++j)
								codeListLens[i++] = prevCode;
						}
						else if (lenCode == 17)
						{
							u8 repCount = 3 + u8(bitBuffer.GetValueLSB(3));
							for (u8 j = 0; j < repCount; ++j)
								codeListLens[i++] = 0;
						}
						else
						{
							u8 repCount = 11 + u8(bitBuffer.GetValueLSB(7));
							for (u8 j = 0; j < repCount; ++j)
								codeListLens[i++] = 0;
						}
					}

					litLenTree = Huffman(codeListLens, hlit);
					distTree = Huffman(codeListLens + hlit, hdist);
				}

				// Decode
				b8 runBlock = true;

				while (runBlock)
				{
					u16 code = u16(litLenTree.GetNextSymbol(bitBuffer));

					if (code > 256) // Start of repeating segment
					{
						u16 len = 0;
						tmp = 0;

						// Get extra bytes and base length
						const u16* pair = Detail::g_InflateLenData[code - 257];
						len = pair[0];
						if (pair[1] > 0)
						{
							tmp = u32(bitBuffer.GetValueLSB(u8(pair[1])));
							len += u16(tmp);
						}

						if (len == 0)
						{
							g_Logger.LogError("Deflate decompression: Invalid static Huffman code (length calculation)!");
							return;
						}

						u16 dist = 0;
						u8 val = u8(distTree.GetNextSymbol(bitBuffer));
						tmp = 0;
						pair = Detail::g_InflateDistData[val];
						dist = pair[0];
						if (pair[1] > 0)
						{
							tmp = u32(bitBuffer.GetValueLSB(u8(pair[1])));
							dist += u16(tmp);
						}

						// Look back at data and write it to the end
						if (len > dist)
						{
							for (u16 i = 0; i < len; ++i)
							{
								u8 tmpVal = decompressed[decompressed.Size() - dist];
								decompressed.Push(tmpVal);
							}
						}
						else
						{
							sizeT startPos = decompressed.Size();
							decompressed.Resize(decompressed.Size() + len);
							u8* end = decompressed.Data() + startPos;
							Memory::Copy(end, end - dist, len);
						}
					}
					else if (code == 256) //End of block
					{
						runBlock = false;
					}
					else // Literal value: insert
					{
						decompressed.Push(u8(code));
					}
				}

			}
			break;
			default:
				g_Logger.LogError("Deflate decompression: Invalid btype!");
				return;
			}

		}
	}
}
