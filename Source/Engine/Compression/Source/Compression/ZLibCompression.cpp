// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ZLibCompression.cpp: ZLib compression
#include "CompressionPCH.h"
#include "Compression/ZLibCompression.h"
#include "Compression/DeflateCompression.h"

// TODO: Move to better file
HV_DECLARE_LOG_CATEGORY(Compress, Hv::LogLevel::All);

namespace Hv::Compression {
	
	void Compression::DecompressZlib(const DynArray<u8>& stream, DynArray<u8>& decompressed)
	{
		if (stream.Size() <= 6)
		{
			g_Logger.LogError(LogCompress(), "ZLib stream to short!");
			return;
		}

		Detail::ZlibStream zlib;
		zlib.compressionAndFlags = stream[0];
		zlib.additionalFlags = stream[1];
		zlib.dictId = 0;
		zlib.checksum = *((u32*)&stream[stream.Size() - 4]);
#if HV_LITTLE_ENDIAN
		zlib.checksum = SwitchEndianess(zlib.checksum);
#endif

		Detail::ZlibCompressionMethod compMethod = Detail::ZlibCompressionMethod(zlib.compressionAndFlags & 0x0F);
		if (compMethod != Detail::ZlibCompressionMethod::Deflate)
		{
			g_Logger.LogError("Unsupported zlib compression method!");
			return;
		}
		 
		u8 cinfo = (zlib.compressionAndFlags >> 4) & 0x0F;
		u16 wndSize = u16(1u << (cinfo + 8));

		u16 fcheck = (zlib.compressionAndFlags << 8) + zlib.additionalFlags;
		u16 fcheckMod = fcheck % 31;
		if (fcheckMod != 0)
		{
			g_Logger.LogError(LogCompress(), "zlib stream is invalid!");
			return;
		}

		b8 fdict = b8((zlib.additionalFlags >> 5) & 0x01);
		if (fdict)
		{
			g_Logger.LogError(LogCompress(), "No dictionaries supported!");
		}

		//ZlibCompressionLevel flevel = ZlibCompressionLevel((stream.additionalFlags & 0xC0) >> 6);
		//g_Logger.LogFormat(Utility::LogLevel::Info, "Decompressing zlib stream compressed with level %u", u8(flevel));

		zlib.data.Assign(stream.Data() + 2, stream.Size() - 6);
		Inflate(zlib.data, decompressed);

		if (decompressed.Size() == 0)
		{
			g_Logger.LogError("zlib compression: failed to inflate data!");
			return;
		}

		// Check uncompressed data
		u32 adler32 = Checksum::Adler32(decompressed.Data(), u64(decompressed.Size()), 1);
		if (adler32 != zlib.checksum)
		{
			g_Logger.LogError("zlib decompression failed: checksum is invalid!");
		}
		return;
	}

}
