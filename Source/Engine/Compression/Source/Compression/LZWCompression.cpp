// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// LZWCompression.cpp: LZW (Lempel-Ziv-Welsh) compression
#include "CompressionPCH.h"
#include "Compression/LZWCompression.h"
#include "Checksum/Adler.h"
#include "Checksum/Adler.h"

namespace Hv::Compression {

	void DecompressLZWGif(const DynArray<u8>& compressed, u8 codeLen, DynArray<u8>& uncompressed)
	{
		if (codeLen < 3 || codeLen >= 12)
		{
			g_Logger.LogError("Gif decompression: Invalid code length!");
			return;
		}

		BitBuffer bitBuffer(compressed, true);
		u16 clearCode = 1 << codeLen;
		u16 endCode = clearCode + 1;
		u32 dictCode = clearCode + 2;
		++codeLen;
		u8 initCodeLen = codeLen;
		u16 maxDictSize = 1 << 12;

		DynArray<Pair<u16, u8>> dict; // (prev, val)
		dict.Reserve(maxDictSize);

		// Reserve what is probably the minimal size for the uncompressed data
		uncompressed.Reserve(compressed.Size());

		u16 code = u16(bitBuffer.GetValueLSB(codeLen));
		if (code == clearCode)
			code = u16(bitBuffer.GetValueLSB(codeLen));
		u16 prev = code;

		uncompressed.Push(u8(code));

		// Init dict
		for (u16 i = 0; i < dictCode; ++i)
			dict.Push({ 0xFFFF, u8(i) });

		DynArray<u8> buffer;
		buffer.Reserve(16); // Reserve size for a 'sane' dictionary entry
		while (true)
		{
			// Check if we would reach the end of the stream
			u8 byteOffset = (codeLen + bitBuffer.GetBitIndex()) >> 3;
			if (bitBuffer.GetByteIndex() + byteOffset >= compressed.Size())
				return;

			code = u16(bitBuffer.GetValueLSB(codeLen));
			if (code == clearCode)
			{
				dict.Clear();
				for (u16 i = 0; i < dictCode; ++i)
					dict.Push({ 0xFFFF, u8(i) });
				codeLen = initCodeLen;

				code = u16(bitBuffer.GetValueLSB(codeLen));
				uncompressed.Push(u8(code));
				prev = code;
			}
			else if (code == endCode)
			{
				return;
			}
			else if (code >= dictCode)
			{
				if (code >= dict.Size())
				{
					if (dict.Size() < maxDictSize)
					{
						Pair<u16, u8>& prevEntry = dict[prev];
						Pair<u16, u8> it = prevEntry;
						u8 val0 = 0;
						while (true)
						{
							buffer.Push(it.second);
							u16 idx = it.first;
							if (idx == 0xFFFF)
							{
								val0 = it.second;
								break;
							}
							it = dict[idx];
						}
						while (buffer.Size())
						{
							uncompressed.Push(buffer[buffer.Size() - 1]);
							buffer.Pop();
						}
						uncompressed.Push(val0);

						Pair<u16, u8> newEntry = { prev, val0 };
						dict.Push(newEntry);

						prev = code;
					}
				}
				else // Code in dictionary
				{
					Pair<u16, u8>& entry = dict[code];
					Pair<u16, u8> it = entry;
					u8 val0 = 0;
					while (true)
					{
						buffer.Push(u8(it.second));
						u16 idx = it.first;
						if (idx == 0xFFFF)
						{
							val0 = it.second;
							break;
						}
						it = dict[idx];
					}
					while (buffer.Size())
					{
						uncompressed.Push(buffer[buffer.Size() - 1]);
						buffer.Pop();
					}

					if (dict.Size() < maxDictSize)
					{
						Pair<u16, u8> newEntry = { prev, val0 };
						dict.Push(newEntry);
						prev = code;
					}
				}
			}
			else
			{
				uncompressed.Push(u8(code));
				if (dict.Size() < maxDictSize)
				{
					Pair<u16, u8> newEntry = { prev, u8(code) };
					dict.Push(newEntry);
				}
				prev = code;
			}

			u16 lastDictEntry = u32(dict.Size());
			u16 maxVal = 1 << codeLen;
			if (maxVal <= lastDictEntry && codeLen < 12)
			{
				++codeLen;
			}
		}
	}
}
