// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// BitmapCompression.cpp: Bitmap compression
#include "CompressionPCH.h"
#include "Compression/RLECompression.h"

namespace Hv::Compression {

	void DecompressRLE4Bmp(const DynArray<u8>& compressed, u32 lineLength, DynArray<u8>& data)
	{
		data.Reserve(compressed.Size());

		for (sizeT i = 0; i < compressed.Size();)
		{
			u8 control = compressed[i++];
			if (control == 0)
			{
				u8 selector = compressed[i++];
				switch (selector)
				{
				case 0:
				{
					sizeT additional = lineLength - (data.Size() % lineLength);
					if (additional != 0 && additional != lineLength)
						data.Resize(data.Size() + additional);
					break;
				}
				case 1:
				{
					return;
				}
				case 2:
				{
					u8 hor = compressed[i++];
					u8 ver = compressed[i++];
					data.Resize(data.Size() + ver * lineLength + hor);
					break;
				}
				default:
				{
					for (u8 j = 0; j < selector; ++j)
					{
						u8 val1 = compressed[i++];
						u8 val0 = val1 >> 4;
						val1 &= 0x0F;
						data.Push(val0);

						++j;
						if (j < selector)
							data.Push(val1);
					}

					// Align to word boundary
					if (i & 1)
						++i;
				}
				}
			}
			else
			{
				u8 val1 = compressed[i++];
				u8 val0 = val1 >> 4;
				val1 &= 0x0F;
				for (u8 j = 0; j < control; ++j)
				{
					data.Push(j & 1 ? val1 : val0);
				}
			}
		}
	}

	void DecompressRLE8Bmp(const DynArray<u8>& compressed, u32 lineLength, DynArray<u8>& data)
	{
		data.Reserve(compressed.Size());

		for (sizeT i = 0; i < compressed.Size();)
		{
			u8 control = compressed[i++];
			if (control == 0)
			{
				u8 selector = compressed[i++];
				switch (selector)
				{
				case 0:
				{
					sizeT additional = lineLength - (data.Size() % lineLength);
					if (additional != 0 && additional != lineLength)
						data.Resize(data.Size() + additional);
					break;
				}
				case 1:
				{
					return;
				}
				case 2:
				{
					u8 hor = compressed[i++];
					u8 ver = compressed[i++];
					data.Resize(data.Size() + ver * lineLength + hor);
					break;
				}
				default:
				{
					for (u8 j = 0; j < selector; ++j)
					{
						data.Push(compressed[i++]);
					}

					// Align to word boundary
					if (i & 1)
						++i;
				}
				}
			}
			else
			{
				u8 val = compressed[i++];
				for (u8 j = 0; j < control; ++j)
				{
					data.Push(val);
				}
			}
		}
	}
}
