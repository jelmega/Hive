// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Huffman.cpp: Huffman table
#include "CompressionPCH.h"
#include "Compression/Huffman.h"

namespace Hv::Compression {

	Huffman::Node::Node()
		: length(0)
		, code(0)
	{
	}

	Huffman::Node::Node(u8 length, u32 code)
		: length(length)
		, code(code)
	{
	}

	Huffman::Huffman()
		: m_MinBits(0)
		, m_MaxBits(0)
	{
	}

	Huffman::Huffman(u8* pArray, u32 length)
		: m_MinBits(0)
		, m_MaxBits(0)
	{
		GenerateTree(pArray, length);
	}

	Huffman::Huffman(DynArray<u8> array)
		: m_MinBits(0)
		, m_MaxBits(0)
	{
		GenerateTree(array.Data(), u32(array.Size()));
	}

	Huffman::~Huffman()
	{
	}

	u32 Huffman::GetNextSymbol(BitBuffer& buffer) const
	{
		u32 val = u32(buffer.GetValueMSB(m_MinBits));

		for (u8 i = m_MinBits; i <= m_MaxBits; ++i)
		{
			// Offset in array for i bits
			u32 offset = m_Offsets[i];
			// leaf at index of "val"
			const Node& node = m_Nodes[val - offset];
			// Check value bit length (need to match up), if so, the code is found
			if (node.length == i)
				return node.code;
			// Else, if we didn't reach the final bit, we keep on searching
			if (i != m_MaxBits)
			{
				val <<= 1;
				val |= buffer.GetBit();
			}
		}

		// If we didn't find the code, return a "huge" value
		return Hm::g_Max<u32>;
	}

	void Huffman::GenerateTree(u8* pArray, u32 length)
	{
		u16 lenFreq[m_MaxHuffmanLength] = {}; // Frequencies with which lengths appear
		for (sizeT i = 0; i < length; ++i)
			lenFreq[pArray[i]]++;
		// ignore 0 bits:
		lenFreq[0] = 0;

		// Find shortest and longest + Get the total number of values in the tree
		int numValues = 0;
		m_MaxBits = 0;
		m_MinBits = m_MaxHuffmanLength;
		for (u8 i = 1; i < m_MaxHuffmanLength; ++i)
		{
			if (lenFreq[i] == 0)
				continue;

			m_MinBits = Hm::Min(m_MinBits, i);
			m_MaxBits = Hm::Max(m_MaxBits, i);
			numValues += lenFreq[i];
		}
		// Make enough space to hold the maximum number of leafes
		m_Nodes.Resize(numValues);

		// Generate the huffman codes

		// Generate the offsets for each bit length
		u32 codes[m_MaxHuffmanLength] = {};
		m_Offsets.Resize(m_MaxHuffmanLength);
		u32 curCode = 0;
		u32 valIdx = 0;
		for (u8 i = m_MinBits; i <= m_MaxBits; ++i)
		{
			u32 freq = lenFreq[i - 1];
			curCode += freq;
			curCode <<= 1;
			codes[i] = curCode;
			// Calculate offset per bit length, so that values follow eachother in 1 continuous array
			valIdx += freq;
			m_Offsets[i] = curCode - valIdx;
		}

		// Now assign the actual codes and create the leaves for it
		for (u32 i = 0; i < length; ++i)
		{
			u8 bitLen = pArray[i];
			if (bitLen == 0)
				continue;
			u32 code = codes[bitLen]++;
			// Don't need to reverse code, since we read the codes in reverse from the bitbuffer
			Node leaf(bitLen, i);
			// Insert leaf at code index
			u32 offset = m_Offsets[bitLen];
			m_Nodes[code - offset] = leaf;
		}
	}
}
