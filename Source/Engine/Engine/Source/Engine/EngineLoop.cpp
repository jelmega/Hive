// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// EngineLoop.cpp: Engine Loop
#include "EnginePCH.h"
#include "Engine/EngineLoop.h"
#include "WindowSystem/WindowManager.h"
#include "Input/InputManager.h"
#include "Renderer/Renderer.h"
#include "Threading/JobSystem.h"

#include "Components/Components.h"
#include "ECSSystems/ECSSystems.h"

HV_DECLARE_LOG_CATEGORY(Engine, Hv::LogLevel::All);

namespace Hv::Engine {


	EngineLoop::EngineLoop()
		: m_pApp(nullptr)
		, m_SystemHandle(InvalidSystemHandle)
		, m_Settings()
	{
	}

	EngineLoop::~EngineLoop()
	{
	}

	b8 EngineLoop::PreInit(IApplication* pApp, const Hv::String& cmdLine, SystemHandle& sysHandle)
	{
		m_pApp = pApp;
		m_SystemHandle = sysHandle;
		m_Settings = m_pApp->CreateSettings();

		return true;
	}

	i32 EngineLoop::Run()
	{
		if (!Init())
			return -1;

		while (g_WindowManager.GetMainWindow()->IsOpen())
		{
			System::TickSystem();
			Tick();
			Draw();
			PostTick();
		}

		if (!Shutdown())
			return -1;

		return 0;
	}

	b8 EngineLoop::Init()
	{
		// Mount resource folder
		HvFS::Mount("res", "Assets");
		HvFS::Mount("plugins", "Plugins");

		g_WindowManager.SetMessageDelegate(Delegate<b8(const System::SystemEvent&)>(this, &EngineLoop::SystemEventHandler));
		b8 res = g_WindowManager.Init(m_Settings.mainWindowDesc, m_SystemHandle);
		if (!res)
		{
			g_Logger.LogFatal(LogEngine(), "Failed to initialize window manager!");
			return false;
		}

		res = g_InputManager.Init();
		if (!res)
		{
			g_Logger.LogFatal(LogEngine(), "Failed to initialize input manager!");
			return false;
		}

		res = g_Renderer.Init(m_Settings.renderDesc);
		if (!res)
		{
			g_Logger.LogFatal(LogEngine(), "Failed to initialize renderer!");
			return false;
		}
		// Register main window
		g_Renderer.RegisterWindow(g_WindowManager.GetMainWindow());

		res = g_AssetManager.Init();
		if (!res)
		{
			g_Logger.LogFatal(LogEngine(), "Failed to initialize asset manager!");
			return false;
		}

		res = g_PluginManager.Init();
		if (!res)
		{
			g_Logger.LogFatal(LogEngine(), "Failed to initialize plugin manager!");
			return false;
		}

		u8 logicalProcessorCount = Threading::GetLogicalCoreCount();
		res = g_JobSystem.Init(logicalProcessorCount - 2);
		if (!res)
		{
			g_Logger.LogFatal(LogEngine(), "Failed to initialize job system!");
			return false;
		}

		res = g_ECS.Init();
		if (!res)
		{
			g_Logger.LogFatal(LogEngine(), "Failed to initialize ECS manager!");
			return false;
		}

		res = g_PluginManager.RegisterPlugins();
		if (!res)
		{
			g_Logger.LogFatal(LogEngine(), "Failed to register plugins!");
			return false;
		}

		RegisterECS();

		// Initialize application
		res = m_pApp->Init();
		if (!res)
		{
			g_Logger.LogFatal(LogEngine(), "Failed to initialize application!");
			return false;
		}

		return true;
	}

	void EngineLoop::Tick()
	{
		g_WindowManager.Tick();

		g_InputManager.Tick();

		g_ECS.Tick();

		m_pApp->Tick();

		g_JobSystem.Tick();
	}

	void EngineLoop::Draw()
	{
		g_Renderer.Draw();
	}

	void EngineLoop::PostTick()
	{
		g_InputManager.PostTick();
	}

	void EngineLoop::RegisterECS()
	{
		g_ECS.RegisterComponent<TransformComponent>();
		g_ECS.RegisterComponent<MeshComponent>();

		g_ECS.AddSystem<ECSSystems::RenderSystem>();
	}

	b8 EngineLoop::SystemEventHandler(const System::SystemEvent& evnt)
	{
		switch (evnt.type)
		{
		case System::SystemEventType::Window:
		{
			const System::WindowEvent& wndEvent = evnt.windowEvent;
			g_WindowManager.HandleEvents(wndEvent);
			return true;
		}
		case System::SystemEventType::Input:
		{
			const System::InputEvent& inputEvent = evnt.inputEvent;
			g_InputManager.HandleEvents(inputEvent);
			return true;
		}
		default:
			break;
		}

		return false;
	}

	b8 EngineLoop::Shutdown()
	{
		b8 tres = true;
		

		b8 res = m_pApp->Shutdown();
		if (!res)
		{
			g_Logger.LogError(LogEngine(), "Application shutdown failed!");
			tres = false;
		}

		res = g_PluginManager.UnregisterPlugins();
		if (!res)
		{
			g_Logger.LogError(LogEngine(), "Plugin unregistering failed!");
			tres = false;
		}

		res = g_ECS.Shutdown();
		if (!res)
		{
			g_Logger.LogError(LogEngine(), "ECS manager shutdown failed!");
			tres = false;
		}

		res = g_JobSystem.Shutdown();
		if (!res)
		{
			g_Logger.LogError(LogEngine(), "Job system shutdown failed!");
			tres = false;
		}

		res = g_AssetManager.Shutdown();
		if (!res)
		{
			g_Logger.LogError(LogEngine(), "Asset manager shutdown failed!");
			tres = false;
		}

		res = g_PluginManager.Shutdown();
		if (!res)
		{
			g_Logger.LogError(LogEngine(), "Plugin manager shutdown failed!");
			tres = false;
		}

		res = g_Renderer.Shutdown();
		if (!res)
		{
			g_Logger.LogError(LogEngine(), "Renderer shutdown failed!");
			tres = false;
		}

		res = g_WindowManager.Shutdown();
		if (!res)
		{
			g_Logger.LogError(LogEngine(), "Window manager shutdown failed!");
			tres = false;
		}

		return tres;
	}
}
