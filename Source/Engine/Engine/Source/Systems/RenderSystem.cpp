// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// RenderSystem.cpp: Render system
#pragma once
#include "EnginePCH.h"
#include "ECSSystems/RenderSystem.h"
#include "Components/Components.h"

namespace Hv::ECSSystems {


	RenderSystem::RenderSystem()
		: ISystem(ECS::SystemPriority::Render)
	{
	}

	RenderSystem::~RenderSystem()
	{
	}

	void RenderSystem::Init()
	{
	}

	void RenderSystem::Tick()
	{
		ECS::View<TransformComponent, MeshComponent> view;
		view.Each([](ECS::Entity& entity, TransformComponent& transform, MeshComponent& mesh)
		{
			HV_UNREFERENCED_PARAM(entity);

			// TODO: Figure out why the transpose is needed (vulkan)
			f32m4 transMat = f32m4::CreateTransform(transform.scale, transform.rotation, transform.position);
			g_Renderer.AddRenderable(transMat, mesh.material, mesh.mesh);
		});
	}
}
