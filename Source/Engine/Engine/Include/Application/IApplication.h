// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// IApplication.h: Application interface
#pragma once
#include "EnginePCH.h"
#include "WindowSystem/Window.h"
#include "Renderer/Renderer.h"

namespace Hv::Engine {
	class EngineLoop;
}

namespace Hv {
	/**
	 * Application settings
	 */
	struct ApplicationSettings
	{
		WindowDesc mainWindowDesc;			/**< Main window description */
		Renderer::RendererDesc renderDesc;	/**< Initial render description */
	};
	
	class HIVE_API IApplication
	{
	public:
		/**
		 * Create an application
		 */
		IApplication();
		virtual ~IApplication();

	protected:

		/**
		 * Create the settings for the application
		 * @return	Application settings
		 */
		virtual ApplicationSettings CreateSettings() = 0;
		/**
		 * Initialize the application
		 * @return	True if the initialization was succesful, false otherwise
		 */
		virtual b8 Init() = 0;
		/**
		 * Tick/update the application
		 */
		virtual void Tick() = 0;
		/**
		 * Additional drawing for the application
		 */
		virtual void Draw() = 0;
		/**
		 * Shutdown the application
		 * @return	True if the shutdown was succesful, false otherwise
		 */
		virtual b8 Shutdown() = 0;

	private:
		friend class Engine::EngineLoop;
	};


	// DynLib Interface
	using LoadApplicationFunc = IApplication*(*)();

	// Expected function in application:
	//extern "C"
	//{
	//	/**
	//	 * Generate the application for the engine to use
	//	 * @return	Generated application
	//	 */
	//	HIVE_API IApplication* LoadApplication();
	//}
	//
	// From here on, no other functions need to be loaded from the dynamic library

}
