// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Engine.h: Engine includes files
#pragma once

// Application
#include "Application/IApplication.h"

// Window System
#include "WindowSystem/WindowManager.h"
#include "WindowSystem/Window.h"
#include "WindowSystem/Monitor.h"

// Input
#include "Input/InputManager.h"

// Renderer
#include "Renderer/Renderer.h"

// Asset Systen
#include "AssetSystem.h"

#include "ECS.h"

// Renderer
#include "PluginSystem.h"

// Components
#include "Components/Components.h"
#include "ECSSystems/ECSSystems.h"