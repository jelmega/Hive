// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// RenderSystem.h: Render system
#pragma once
#include "EnginePCH.h"

namespace Hv::ECSSystems {
	
	class RenderSystem : public ECS::ISystem
	{
	public:
		RenderSystem();
		~RenderSystem();

		void Init() override final;
		void Tick() override final;
	};

}

