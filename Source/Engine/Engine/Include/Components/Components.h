// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Components.h: Default components
#pragma once
#include "ECSPCH.h"
#include "../../../../Plugins/FileHandlers/Include/FileHandlersPCH.h"

namespace Hv {
	
	HV_STRUCT(Component)
	struct TransformComponent
	{
		f32v3 position;
		f32q rotation = f32q::Identity;
		f32v3 scale = f32v3::One;
	};

	HV_STRUCT(Component)
	struct MeshComponent
	{
		MeshHandle mesh;
		Renderer::MaterialInstanceHandle material;
	};

}
