// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// EnginePCH.h: Engine Precompiled header
#pragma once
#include <Core.h>
#include <System.h>
#include <RendererCore.h>
#include <Renderer/Renderer.h>
#include <Compression.h>
#include <PluginSystem.h>
#include <AssetSystem.h>
#include <ECS.h>

#include HV_INCLUDE_RTTI_MODULE(Engine)

HV_DECLARE_LOG_CATEGORY_EXTERN(Engine);