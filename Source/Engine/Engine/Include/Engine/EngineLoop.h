// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// EngineLoop.h: Engine Loop
#pragma once
#include "EnginePCH.h"
#include "System/SystemEvent.h"
#include "Application/IApplication.h"

namespace Hv::Engine {
	
	/**
	 * Main engine loop
	 */
	class HIVE_API EngineLoop
	{
	public:
		/**
		* Create an engine loop
		*/
		EngineLoop();
		~EngineLoop();

		/**
		* Preinitialize the engineloop
		* @param[in] pApp			Application
		* @param[in] cmdLine		Command line string
		* @param[in] sysHandle		System handle
		* @return					If the preinitialization went successfully
		*/
		b8 PreInit(IApplication* pApp, const Hv::String& cmdLine, SystemHandle& sysHandle);

		/**
		* Run the engine loop
		*/
		i32 Run();

	private:
		/**
		* Initialize the engine
		*/
		b8 Init();

		/**
		* Update the engine
		*/
		void Tick();
		/**
		* Draw all objects
		*/
		void Draw();
		/**
		* Update the engine after processing is done
		*/
		void PostTick();

		/**
		 * Register data to the ECS (components and systems)
		 */
		void RegisterECS();

		/**
		* Handle system events
		*/
		b8 SystemEventHandler(const System::SystemEvent& evnt);

		/**
		* Shutdown engine
		*/
		b8 Shutdown();


		IApplication* m_pApp;			/**< Application */
		ApplicationSettings m_Settings;	/**< Application settings */
		SystemHandle m_SystemHandle;	/**< System handle */

		// Threads
		Threading::Thread m_RenderingThread;
		Threading::Thread m_IOAssetThread;

	};

}
