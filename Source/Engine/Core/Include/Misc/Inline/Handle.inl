// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Handle.inl: Handle to an object
#pragma once
#include "Misc/Handle.h"

namespace Hv {
	
	template <typename T>
	Handle<T>::Handle()
		: m_Id(IdMask)
		, m_Check(0)
		, m_pData(nullptr)
		, m_pManager(nullptr)
	{
	}

	template <typename T>
	Handle<T>::Handle(u64 id, u8 check, T* pData, HandleManager<T>* pManager)
		: m_Id(id & IdMask)
		, m_Check(check)
		, m_pData(pData)
		, m_pManager(pManager)
	{
	}

	template <typename T>
	Handle<T>::~Handle()
	{
	}

	template <typename T>
	HandleManager<T>::HandleManager()
	{
	}

	template <typename T>
	HandleManager<T>::~HandleManager()
	{
	}

	template <typename T>
	Handle<T> HandleManager<T>::Create(u64 handleId, T* pData)
	{
		if (handleId >= m_Checks.Size())
			m_Checks.Resize(handleId + 1);
			
		u8 check = m_Checks[handleId];
		return Handle<T>(handleId, check, pData, this);
	}

	template <typename T>
	void HandleManager<T>::Invalidate(Handle<T>& handle)
	{
		u64 id = handle.GetId();
		if (id < m_Checks.Size())
			++m_Checks[id];
	}

	template <typename T>
	void HandleManager<T>::Invalidate(u64 id)
	{
		if (id < m_Checks.Size())
			++m_Checks[id];
	}

	template <typename T>
	b8 HandleManager<T>::IsValid(const Handle<T>& handle)
	{
		u8 check = m_Checks[handle.GetId()];
		return handle.GetCheck() == check;
	}

}
