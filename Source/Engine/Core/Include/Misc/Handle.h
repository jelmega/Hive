// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Handle.h: Handle to an object
#pragma once
#include "Core/CoreHeaders.h"
#include "Containers/DynArray.h"

namespace Hv {

	template<typename T>
	class HandleManager;
	
	template<typename T>
	class Handle
	{
	public:
		Handle();
		Handle(u64 id, u8 check, T* pData, HandleManager<T>* pManager);
		~Handle();

		T* operator->() { return m_pData; }
		const T* operator->() const { return m_pData; }

		T& operator*() { return *m_pData; }
		const T& operator*() const { return *m_pData; }

		T* Get() { return m_pData; }
		const T* Get() const { return m_pData; }

		u64 GetId() const { return m_Id; }
		u8 GetCheck() const { return m_Check; }

		b8 IsValid() const { return m_pManager && m_pManager->IsValid(*this); }

		b8 operator==(const Handle& handle) { return m_Id == handle.m_Id && m_Check == handle.m_Check; }
		b8 operator!=(const Handle& handle) { return m_Id != handle.m_Id || m_Check != handle.m_Check; }

	private:
		static constexpr u64 IdMask = 0x00FF'FFFF'FFFF'FFFF;
		u64 m_Id : 56;
		u8 m_Check;
		T* m_pData;
		HandleManager<T>* m_pManager;
	};

	template<typename T>
	class HandleManager
	{
	public:
		HandleManager();
		~HandleManager();

		/**
		 * Create a handle from an id
		 * @param[in] handleId	Id of the handle
		 * @param[in] pData		Data the handle points to
		 */
		Handle<T> Create(u64 handleId, T* pData);
		/**
		 * Invalidate a handle
		 * @param[in] handle	Handle
		 * @note				All handles with the same Id, created before this call, will be invalid
		 */
		void Invalidate(Handle<T>& handle);
		/**
		 * Invalidate a handle from its id
		 * @param[in] id	Id
		 * @note			All handles with the same Id, created before this call, will be invalid
		 */
		void Invalidate(u64 id);
		/**
		 * Check if a handle is valid
		 * @param[in] handle	Handle
		 * @return				True
		 */
		b8 IsValid(const Handle<T>& handle);

	private:
		DynArray<u8> m_Checks;
	};

}

#include "Inline/Handle.inl"