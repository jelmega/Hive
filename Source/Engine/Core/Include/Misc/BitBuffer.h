// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// BitBuffer.h: Bit buffer
#pragma once
#include "Core/CoreHeaders.h"
#include "Containers/DynArray.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv {

	class HIVE_API BitBuffer
	{
	public:
		BitBuffer();
		/**
		* Create a bit buffer from a buffer
		* @param[in] buffer			Buffer
		* @param[in] readLSBFirst	Read starting from the least significant bit (end of the byte)
		*/
		BitBuffer(const DynArray<u8>& buffer, b8 readLSBFirst);
		~BitBuffer();

		/**
		* Skip forward until the next byte boundry
		*/
		void SkipToByteBoundry();
		/**
		* Skip an amount of bytes in the bit buffer
		* @param[in] bits	Amount of bits to skip
		*/
		void Skip(u8 bits);

		/**
		* Get 1 bit from the buffer
		* @return	Bit
		*/
		u8 GetBit();
		/**
		* Get the value in a certain amount of bits, with the Least Significant Bit read first
		* @param[in] bits	Amount of bits to read
		* @return	Value that was encoded in the bits
		*/
		u64 GetValueLSB(u8 bits);
		/**
		* Get the value in a certain amount of bits, with the Most Significant Bit read first
		* @param[in] bits	Amount of bits to read
		* @return	Value that was encoded in the bits
		*/
		u64 GetValueMSB(u8 bits);

		/**
		* Reset the bit buffer (goto front of buffer)
		*/
		void Reset();
		/**
		* Reset the bit buffer and assign a new buffer
		*/
		void Reset(const DynArray<u8>& pBuffer);

		/**
		* Copy bytes to buffer, starting from a byte boundry
		* @param[in] pDest		Destination buffer
		* @param[in] numBytes	Number of bytes to copy
		*/
		void CopyBytesFromBoundry(u8* pDest, u32 numBytes);

		/**
		* Get the current byte index
		* @return	Current byte index
		*/
		HV_FORCE_INL u32 GetByteIndex() const { return m_BytePos; }
		/**
		* Get the current byte index
		* @return	Current bit index
		*/
		HV_FORCE_INL u8 GetBitIndex() const { return m_BitPos; }

		/**
		* Get the current index
		* @return	Current index
		*/
		HV_FORCE_INL u8 GetCurByte() const { return m_Buffer[m_BytePos]; }

	private:
		DynArray<u8> m_Buffer;	/**< Buffer */
		u32 m_BytePos;			/**< Current position in buffer */
		u8 m_BitPos;			/**< Current bot position in byte */
		b8 m_LSB;				/**< If the bits need to be read with the least significant bit first */
	};

}

#pragma warning(pop)