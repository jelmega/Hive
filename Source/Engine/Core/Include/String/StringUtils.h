// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// StringUtils.h: String utilities
#pragma once
#include "Core/CoreHeaders.h"

namespace Hv {
	
	/**
	 * Character utilities
	 * @tparam CharT	Character type
	 * @note			Internal struct
	 */
	template<typename CharT>
	struct CharUtils
	{
		/**
		 * Convert a character of type AnsiChar to CharT
		 * @param[in] c	Character to convert
		 * @return		Converted character
		 */
		static CharT Convert(AnsiChar c);
		/**
		 * Convert a character of type WideChar to CharT
		 * @param[in] c	Character to convert
		 * @return		Converted character
		 */
		static CharT Convert(WideChar c);

		/**
		 * Convert a character to lowercase
		 * @param[in] c	Character to convert
		 * @return		Character in lowercase
		 */
		static AnsiChar ToLower(AnsiChar c);
		/**
		* Convert a character to lowercase
		* @param[in] c	Character to convert
		* @return		Character in lowercase
		*/
		static WideChar ToLower(WideChar c);
		/**
		 * Convert a character to uppercase
		 * @param[in] c	Character to convert
		 * @return		Character in uppercase
		 */
		static AnsiChar ToUpper(AnsiChar c);
		/**
		* Convert a character to uppercase
		* @param[in] c	Character to convert
		* @return		Character in uppercase
		*/
		static WideChar ToUpper(WideChar c);

		/**
		 * Is the character uppercase
		 * @param[in] c	Character to check
		 * @return		True if the character is uppercase, false otherwise
		 */
		static b8 IsUpper(CharT c);
		/**
		 * Is the character lowercase
		 * @param[in] c	Character to check
		 * @return		True if the character is lowercase, false otherwise
		 */
		static b8 IsLower(CharT c);
		/**
		 * Is the character an alphabetic character
		 * @param[in] c	Character to check
		 * @return		True if the character is an alphabetic character, false otherwise
		 */
		static b8 IsAlpha(CharT c);
		/**
		 * Can the character be grphically represented in the standard character set
		 * @param[in] c	Character to check
		 * @return		True if the character can be graphicaly represented in the standard character set, false otherwise
		 */
		static b8 IsGraph(CharT c);
		/**
		 * Can the character be printed
		 * @param[in] c	Character to check
		 * @return		True if the character can be printed, false otherwise
		 */
		static b8 IsPrint(CharT c);
		/**
		 * Is the character a punctuation mark
		 * @param[in] c	Character to check
		 * @return		True if the character is a punctuation mark, false otherwise
		 */
		static b8 IsPunct(CharT c);
		/**
		 * Is the character alpha-numerical
		 * @param[in] c	Character to check
		 * @return		True if the character is alpha-numerical, false otherwise
		 */
		static b8 IsAlnum(CharT c);
		/**
		 * Is the character an octal digit
		 * @param[in] c	Character to check
		 * @return		True if the character is an octal digit, false otherwise
		 */
		static b8 IsOctDigit(CharT c);
		/**
		 * Is the character a hexadecimal digit
		 * @param[in] c	Character to check
		 * @return		True if the character is a hexadecimal digit, false otherwise
		 */
		static b8 IsHexDigit(CharT c);

		/**
		 * Check wether a characater is a whitespace or not
		 * @param[in] c	Character to check
		 * @return		True if the character is a witespace, false otherwise
		 */
		static b8 IsWhitespace(CharT c);
		/**
		 * Parse the character as a digit
		 * @param[in] c		Character to parse
		 * @param[in] base	Digit base
		 * @return			Value of the digit (0xFF if invalid)
		 */
		static u8 ParseDigit(CharT c, u8 base = 10);
		/**
		* Parse the character as a digit
		* @tparam Base		Digit base
		* @param[in] c		Character to parse
		* @return			Value of the digit (0xFF if invalid)
		*/
		template<u8 Base>
		static u8 ParseDigit(CharT c);
	};

	/**
	 * String utils
	 * @note	Internal struct
	 */
	struct StringUtils {
		
		/**
		 * Get the length of a c-string
		 * @tparam C		Character type
		 * @param[in] str	C-string
		 * @return			Length of the c-string
		 */
		template<typename C>
		static sizeT CStringLength(const C* str);

		/**
		 * Compare 2 C-strings
		 * @tparam C		Character type
		 * @param[in] str0	C-string
		 * @param[in] str1	C-string
		 * @return			Relationship between the strings\n
		 *					0 : string and character match
		 *					-1: string comes lexicograpically before the character or the string is empty\n
		 *					1 : string comes lexicograpically after the character or 1st character matches, but string is longer
		 */
		template<typename C>
		static i8 Compare(const C* str0, const C* str1);

	};

}

#include "Inline/StringUtils.inl"