// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Regex.h: Regular expression
#pragma once
#include "TString.h"

namespace Hv::Regex {
	
	/**
	 * Check if a string matches a patern
	 * @param[in] str		String to check
	 * @param[in] patern	Patern to match to
	 * @return				True if the string matches the patern, false otherwise
	 * @note				Currently, only * is supported
	 */
	b8 Match(const String& str, const String& patern);


	namespace Detail {
		
		/**
		 * Check '*' for a match
		 * @param[in] str		String to check
		 * @param[in] patern	Patern to match to (expects first '*' to be removed from the patern)
		 * @return				True if the asterix patern matches, false otherwise
		 */
		b8 MatchAserix(String& str, String& patern);



		static AnsiChar g_RegexChars[] = { '*' };
	}

}

#include "Inline/Regex.inl"