// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// String.h: Main string header
#pragma once

#include "StringUtils.h"
#include "TString.h"

#include "ToString.h"

#include "StringParsing.h"

#include "StringFormatF.h"

#include "Regex.h"

// String hash overloads
namespace Hv {
	
	template<typename CharT, Container::GrowthPolicyFunc GrowtPolicy>
	struct HashFNV1a<TString<CharT, GrowtPolicy>>
	{
		hashT operator()(const TString<CharT, GrowtPolicy>& str) const
		{
			HashFNV1a<CharT> hash;
			return hash(str.CStr(), str.Length());
		}
	};

}