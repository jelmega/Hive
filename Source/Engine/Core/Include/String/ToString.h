// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ToString.h: Convertions to string
#pragma once
#include "TString.h"

namespace Hv {
	
	////////////////////////////////////////////////////////////////////////////////
	// ToString																	  //
	////////////////////////////////////////////////////////////////////////////////
	/**
	 * Convert a bool to a string
	 * @param[in] val	Value to convert
	 * @return			String
	 */
	String ToString(b8 val);
	/**
	 * Convert a bool to a string
	 * @param[in] val	Value to convert
	 * @return			String
	 */
	String ToString(i8 val);
	/**
	 * Convert a bool to a string
	 * @param[in] val	Value to convert
	 * @return			String
	 */
	String ToString(i16 val);
	/**
	 * Convert a bool to a string
	 * @param[in] val	Value to convert
	 * @return			String
	 */
	String ToString(i32 val);
	/**
	 * Convert a bool to a string
	 * @param[in] val	Value to convert
	 * @return			String
	 */
	String ToString(i64 val);
	/**
	 * Convert a bool to a string
	 * @param[in] val	Value to convert
	 * @return			String
	 */
	String ToString(u8 val);
	/**
	 * Convert a bool to a string
	 * @param[in] val	Value to convert
	 * @return			String
	 */
	String ToString(u16 val);
	/**
	 * Convert a bool to a string
	 * @param[in] val	Value to convert
	 * @return			String
	 */
	String ToString(u32 val);
	/**
	 * Convert a bool to a string
	 * @param[in] val	Value to convert
	 * @return			String
	 */
	String ToString(u64 val);
	/**
	 * Convert a bool to a string
	 * @param[in] val		Value to convert
	 * @param[in] precision	Precision
	 * @return				String
	 */
	String ToString(f32 val, u32 precision = 2);
	/**
	 * Convert a bool to a string
	 * @param[in] val		Value to convert
	 * @param[in] precision	Precision
	 * @return				String
	 */
	String ToString(f64 val, u32 precision = 2);

	////////////////////////////////////////////////////////////////////////////////
	// ToHexString																  //
	////////////////////////////////////////////////////////////////////////////////
	/**
	* Convert a value to a hexadecimal string
	* @param[in] val	Value to convert
	* @return			String
	*/
	String ToHexString(b8 val);
	/**
	* Convert a value to a hexadecimal string
	* @param[in] val	Value to convert
	* @return			String
	*/
	String ToHexString(i8 val);
	/**
	* Convert a value to a hexadecimal string
	* @param[in] val	Value to convert
	* @return			String
	*/
	String ToHexString(i16 val);
	/**
	* Convert a value to a hexadecimal string
	* @param[in] val	Value to convert
	* @return			String
	*/
	String ToHexString(i32 val);
	/**
	* Convert a value to a hexadecimal string
	* @param[in] val	Value to convert
	* @return			String
	*/
	String ToHexString(i64 val);
	/**
	* Convert a value to a hexadecimal string
	* @param[in] val	Value to convert
	* @return			String
	*/
	String ToHexString(u8 val);
	/**
	* Convert a value to a hexadecimal string
	* @param[in] val	Value to convert
	* @return			String
	*/
	String ToHexString(u16 val);
	/**
	* Convert a value to a hexadecimal string
	* @param[in] val	Value to convert
	* @return			String
	*/
	String ToHexString(u32 val);
	/**
	* Convert a value to a hexadecimal string
	* @param[in] val	Value to convert
	* @return			String
	*/
	String ToHexString(u64 val);

	/**
	* Convert a value to a hexadecimal string
	* @param[in] val	Value to convert
	* @return			String
	*/
	String ToHexString(f64 val);

	////////////////////////////////////////////////////////////////////////////////
	// ToOctString																  //
	////////////////////////////////////////////////////////////////////////////////
	/**
	* Convert a value to an octal string
	* @param[in] val	Value to convert
	* @return			String
	*/
	String ToOctString(i8 val);
	/**
	* Convert a value to an octal string
	* @param[in] val	Value to convert
	* @return			String
	*/
	String ToOctString(i16 val);
	/**
	* Convert a value to an octal string
	* @param[in] val	Value to convert
	* @return			String
	*/
	String ToOctString(i32 val);
	/**
	* Convert a value to an octal string
	* @param[in] val	Value to convert
	* @return			String
	*/
	String ToOctString(i64 val);
	/**
	* Convert a value to an octal string
	* @param[in] val	Value to convert
	* @return			String
	*/
	String ToOctString(u8 val);
	/**
	* Convert a value to an octal string
	* @param[in] val	Value to convert
	* @return			String
	*/
	String ToOctString(u16 val);
	/**
	* Convert a value to an octal string
	* @param[in] val	Value to convert
	* @return			String
	*/
	String ToOctString(u32 val);
	/**
	* Convert a value to an octal string
	* @param[in] val	Value to convert
	* @return			String
	*/
	String ToOctString(u64 val);

	////////////////////////////////////////////////////////////////////////////////
	// ToScientificString														  //
	////////////////////////////////////////////////////////////////////////////////
	/**
	* Convert a value to a string in scientific notation
	* @param[in] val		Value to convert
	* @param[in[ precision	Scientific notation precision
	* @return				String
	*/
	String ToScientificString(f64 val, u32 precision);

}
#include "Inline/ToString.inl"