// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// TString.h: General template string
#pragma once
#include "Core/CoreHeaders.h"
#include "Memory/Memory.h"
#include "Containers/ContainerUtils.h"
#include "Containers/DynArray.h"
#include "String/StringUtils.h"

namespace Hv {

	/**
	 * TString
	 * @tparam CharT		CharacterType
	 * @tparam GrowthPolicy	Growth policy (capacity will always be the GrowthPolicy(size + 1) - 1, to be able to store a null terminator)
	 */
	template<typename CharT = WideChar, Container::GrowthPolicyFunc GrowthPolicy = Container::GrowthDouble>
	class TString
	{
	private:
		HV_STATIC_ASSERT_MSG((std::is_same<CharT, AnsiChar>::value || std::is_same<CharT, WideChar>::value), "CharT is an unsupported character type!");
		using CU = CharUtils<CharT>;
		using SU = StringUtils;
	public:
		static constexpr sizeT NPos = sizeT(-1);
	public:
		////////////////////////////////////////////////////////////////////////////////
		//	Constructors
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Create an empty string
		 */
		TString();
		/**
		 * Create an empty string
		 * @param[in] pAlloc	AllocBackend
		 */
		TString(Memory::IAllocator * pAlloc);
		/**
		 * Create an empty string with a capacity
		 * @param[in] capacity	Capacity
		 * @param[in] pAlloc		AllocBackend
		 */
		TString(sizeT capacity, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a string filled with a size and a character to fill in the string
		 * @param[in] c			Character
		 * @param[in] count		Count
		 * @param[in] pAlloc	AllocBackend
		 */
		TString(AnsiChar c, sizeT count, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a string filled with a size and a character to fill in the string
		 * @param[in] c			Character
		 * @param[in] count		Count
		 * @param[in] pAlloc	AllocBackend
		 */
		TString(WideChar c, sizeT count, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a string from a C-string
		 * @param[in] str		C-string
		 * @param[in] pAlloc	AllocBackend
		 */
		TString(const AnsiChar * str, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a string from a C-string
		 * @param[in] str		C-string
		 * @param[in] pAlloc	AllocBackend
		 */
		TString(const WideChar * str, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a string from an array
		 * @param[in] arr		Array
		 * @param[in] len		Array size
		 * @param[in] pAlloc	AllocBackend
		 */
		TString(const AnsiChar * arr, sizeT len, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a string from an array
		 * @param[in] arr		Array
		 * @param[in] len		Array size
		 * @param[in] pAlloc	AllocBackend
		 */
		TString(const WideChar * arr, sizeT len, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a string from iterators
		 * @param[in] itFirst	Iterator to first character
		 * @param[in] itLast	Iterator after last character
		 * @param[in] pAlloc	AllocBackend
		 */
		TString(const AnsiChar * itFirst, const AnsiChar * itLast, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a string from iterators
		 * @param[in] itFirst	Iterator to first character
		 * @param[in] itLast	Iterator after last character
		 * @param[in] pAlloc	AllocBackend
		 */
		TString(const WideChar * itFirst, const WideChar * itLast, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a string from iterators
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to first character
		 * @param[in] itLast		Iterator after last character
		 * @param[in] pAlloc		AllocBackend
		 * @note					The user is responsible to make sure that the dereferenced type of the input iterator is a valid character type
		 */
		template<typename InputIterator>
		TString(const InputIterator& itFirst, const InputIterator& itLast, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a string from an initializer list
		 * @param[in] il		Initializer list
		 * @param[in] pAlloc	AllocBackend
		 */
		TString(std::initializer_list<AnsiChar> il, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a string from an initializer list
		 * @param[in] il		Initializer list
		 * @param[in] pAlloc	AllocBackend
		 */
		TString(std::initializer_list<WideChar> il, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a substring from another string
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @param[in] str		String
		 * @param[in] pos		Start position of substring
		 * @param[in] len		Length of substring, if NPos, until string's end
		 * @param[in] pAlloc	AllocBackend
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString(const TString<C, G>& str, sizeT pos, sizeT len = NPos, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a string from another string
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @param[in] str	String
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString(const TString<C, G>& str);
		/**
		 * Create a string from another string
		 * @param[in] str	String
		 */
		TString(const TString& str);
		/**
		 * Create a string from another string
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @param[in] str		String
		 * @param[in] pAlloc	AllocBackend
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString(const TString<C, G>& str, Memory::IAllocator * pAlloc);
		/**
		 * Move a string into this string
		 * @tparam C	Character type
		 * @tparam G	Growth policy
		 * @param str	String
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString(TString<C, G>&& str);
		/**
		 * Move a string into this string
		 * @param str	String
		 */
		TString(TString&& str) noexcept;
		~TString();

		////////////////////////////////////////////////////////////////////////////////
		//	Append (operator+)
		////////////////////////////////////////////////////////////////////////////////
		TString operator+(AnsiChar c) const;
		TString operator+(WideChar c) const;
		TString operator+(const AnsiChar * str) const;
		TString operator+(const WideChar * str) const;
		TString operator+(std::initializer_list<AnsiChar> il) const;
		TString operator+(std::initializer_list<WideChar> il) const;
		template<typename C, Container::GrowthPolicyFunc G>
		TString operator+(const TString<C, G>& str) const;

		////////////////////////////////////////////////////////////////////////////////
		//	Assign
		////////////////////////////////////////////////////////////////////////////////
		TString& operator=(AnsiChar c);
		TString& operator=(WideChar c);
		TString& operator=(const AnsiChar * str);
		TString& operator=(const WideChar * str);
		template<sizeT N>
		TString& operator=(const AnsiChar(&str)[N]);
		template<sizeT N>
		TString& operator=(const WideChar(&str)[N]);
		TString& operator=(std::initializer_list<AnsiChar> il);
		TString& operator=(std::initializer_list<WideChar> il);
		template<typename C, Container::GrowthPolicyFunc G>
		TString& operator=(const TString<C, G>& str);
		TString& operator=(const TString& str);
		TString& operator=(TString&& str) noexcept;

		////////////////////////////////////////////////////////////////////////////////
		//	Assign-Append
		////////////////////////////////////////////////////////////////////////////////
		TString& operator+=(AnsiChar c);
		TString& operator+=(WideChar c);
		TString& operator+=(const AnsiChar * str);
		TString& operator+=(const WideChar * str);
		TString& operator+=(std::initializer_list<AnsiChar> il);
		TString& operator+=(std::initializer_list<WideChar> il);
		template<typename C, Container::GrowthPolicyFunc G>
		TString& operator+=(const TString<C, G>& str);

		////////////////////////////////////////////////////////////////////////////////
		//	Access operators
		////////////////////////////////////////////////////////////////////////////////
		CharT& operator[](sizeT index);
		const CharT& operator[](sizeT index) const;

		////////////////////////////////////////////////////////////////////////////////
		//	Compare and lexicographical compare (operator==/!=/<=/</>=/>)
		////////////////////////////////////////////////////////////////////////////////
		b8 operator==(AnsiChar c) const;
		b8 operator==(WideChar c) const;
		b8 operator==(const AnsiChar * str) const;
		b8 operator==(const WideChar * str) const;
		template<typename C, Container::GrowthPolicyFunc G>
		b8 operator==(const TString<C, G>& str) const;

		b8 operator!=(AnsiChar c) const;
		b8 operator!=(WideChar c) const;
		b8 operator!=(const AnsiChar * str) const;
		b8 operator!=(const WideChar * str) const;
		template<typename C, Container::GrowthPolicyFunc G>
		b8 operator!=(const TString<C, G>& str) const;

		b8 operator<(AnsiChar c) const;
		b8 operator<(WideChar c) const;
		b8 operator<(const AnsiChar * str) const;
		b8 operator<(const WideChar * str) const;
		template<typename C, Container::GrowthPolicyFunc G>
		b8 operator<(const TString<C, G>& str) const;

		b8 operator<=(AnsiChar c) const;
		b8 operator<=(WideChar c) const;
		b8 operator<=(const AnsiChar * str) const;
		b8 operator<=(const WideChar * str) const;
		template<typename C, Container::GrowthPolicyFunc G>
		b8 operator<=(const TString<C, G>& str) const;

		b8 operator>(AnsiChar c) const;
		b8 operator>(WideChar c) const;
		b8 operator>(const AnsiChar * str) const;
		b8 operator>(const WideChar * str) const;
		template<typename C, Container::GrowthPolicyFunc G>
		b8 operator>(const TString<C, G>& str) const;

		b8 operator>=(AnsiChar c) const;
		b8 operator>=(WideChar c) const;
		b8 operator>=(const AnsiChar * str) const;
		b8 operator>=(const WideChar * str) const;
		template<typename C, Container::GrowthPolicyFunc G>
		b8 operator>=(const TString<C, G>& str) const;

		////////////////////////////////////////////////////////////////////////////////
		//	Append
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Append a character to the string
		 * @param[in] c	Character
		 * @return		Reference to string
		 */
		TString& Append(AnsiChar c);
		/**
		 * Append a character to the string
		 * @param[in] c	Character
		 * @return		Reference to string
		 */
		TString& Append(WideChar c);
		/**
		 * Append 'count' characters to the string
		 * @param[in] c		Character
		 * @param[in] count	Amount of characters to append to the string
		 * @return			Reference to string
		 */
		TString& Append(AnsiChar c, sizeT count);
		/**
		 * Append 'count' characters to the string
		 * @param[in] c		Character
		 * @param[in] count	Amount of characters to append to the string
		 * @return			Reference to string
		 */
		TString& Append(WideChar c, sizeT count);
		/**
		 * Append a c-string to the string
		 * @tparam N		String length
		 * @param[in] str	C-string
		 * @return			Reference to string
		 */
		template<sizeT N>
		TString& Append(const AnsiChar(&str)[N]);
		/**
		 * Append a c-string to the string
		 * @tparam N		String length
		 * @param[in] str	C-string
		 * @return			Reference to string
		 */
		template<sizeT N>
		TString& Append(const WideChar(&str)[N]);
		/**
		 * Append a c-string to the string
		 * @param[in] str	C-string
		 * @return			Reference to string
		 */
		TString& Append(const AnsiChar * str);
		/**
		 * Append a c-string to the string
		 * @param[in] str	C-string
		 * @return			Reference to string
		 */
		TString& Append(const WideChar * str);
		/**
		 * Append an array to the string
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 * @return			Reference to string
		 */
		TString& Append(const AnsiChar * arr, sizeT count);
		/**
		 * Append an array to the string
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 * @return			Reference to string
		 */
		TString& Append(const WideChar * arr, sizeT count);
		/**
		 * Append a range of characters to the string
		 * @param[in] itFirst	Iterator to first character
		 * @param[in] itLast	Iterator to the element after after last character
		 * @return				Reference to string
		 */
		TString& Append(const AnsiChar * itFirst, const AnsiChar * itLast);
		/**
		 * Append a range of characters to the string
		 * @param[in] itFirst	Iterator to first character
		 * @param[in] itLast	Iterator to the element after after last character
		 * @return				Reference to string
		 */
		TString& Append(const WideChar * itFirst, const WideChar * itLast);
		/**
		 * Append a range of characters to the string
		 * @tparam InputIterator	Forward Iterator
		 * @param[in] itFirst		Iterator to first character
		 * @param[in] itLast		Iterator to the element after after last character
		 * @return					Reference to string
		 */
		template<typename InputIterator>
		TString& Append(const InputIterator& itFirst, const InputIterator& itLast);
		/**
		 * Append an initializer list to the string
		 * @param[in] il	Initializer list
		 * @return			Reference to string
		 */
		TString& Append(std::initializer_list<AnsiChar> il);
		/**
		 * Append an initializer list to the string
		 * @param[in] il	Initializer list
		 * @return			Reference to string
		 */
		TString& Append(std::initializer_list<WideChar> il);
		/**
		 * Append a string to the string
		 * @tparam C		Character type
		 * @tparam G		Growth policy
		 * @param[in] str	String
		 * @return			Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Append(const TString<C, G>& str);
		/**
		 * Append a part of a string to the string
		 * @tparam C		Character type
		 * @tparam G		Growth policy
		 * @param[in] str	String
		 * @param[in] len	Length of the string to add
		 * @return			Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Append(const TString<C, G>& str, sizeT len);
		/**
		 * Append a substring to the string
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @param[in] str		String
		 * @param[in] subPos	Start position of substring
		 * @param[in] len		Length of the string to add
		 * @return				Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Append(const TString<C, G>& str, sizeT subPos, sizeT len);

		////////////////////////////////////////////////////////////////////////////////
		//	Insert
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Insert a character to the string
		 * @param[in] pos	Position to insert
		 * @param[in] c		Character
		 * @return			Reference to string
		 */
		TString& Insert(sizeT pos, AnsiChar c);
		/**
		 * Insert a character to the string
		 * @param[in] pos	Position to insert
		 * @param[in] c		Character
		 * @return			Reference to string
		 */
		TString& Insert(sizeT pos, WideChar c);
		/**
		 * Insert a character to the string
		 * @param[in] it	Iterator to position to insert
		 * @param[in] c		Character
		 * @return			Reference to string
		 */
		TString& Insert(const CharT * it, AnsiChar c);
		/**
		 * Insert a character to the string
		 * @param[in] it	Iterator to position to insert
		 * @param[in] c		Character
		 * @return			Reference to string
		 */
		TString& Insert(const CharT * it, WideChar c);
		/**
		 * Insert 'count' characters to the string
		 * @param[in] pos	Position to insert
		 * @param[in] c		Character
		 * @param[in] count	Amount of characters to append to the string
		 * @return			Reference to string
		 */
		TString& Insert(sizeT pos, AnsiChar c, sizeT count);
		/**
		 * Insert 'count' characters to the string
		 * @param[in] pos	Position to insert
		 * @param[in] c		Character
		 * @param[in] count	Amount of characters to append to the string
		 * @return			Reference to string
		 */
		TString& Insert(sizeT pos, WideChar c, sizeT count);
		/**
		 * Insert 'count' characters to the string
		 * @param[in] it	Iterator to position to insert
		 * @param[in] c		Character
		 * @param[in] count	Amount of characters to append to the string
		 * @return			Reference to string
		 */
		TString& Insert(const CharT * it, AnsiChar c, sizeT count);
		/**
		 * Insert 'count' characters to the string
		 * @param[in] it	Iterator to position to insert
		 * @param[in] c		Character
		 * @param[in] count	Amount of characters to append to the string
		 * @return			Reference to string
		 */
		TString& Insert(const CharT * it, WideChar c, sizeT count);
		/**
		 * Insert a c-string to the string
		 * @param[in] pos	Position to insert
		 * @param[in] str	C-string
		 * @return			Reference to string
		 */
		TString& Insert(sizeT pos, const AnsiChar * str);
		/**
		 * Insert a c-string to the string
		 * @param[in] pos	Position to insert
		 * @param[in] str	C-string
		 * @return			Reference to string
		 */
		TString& Insert(sizeT pos, const WideChar * str);
		/**
		 * Insert a c-string to the string
		 * @param[in] it	Iterator to position to insert
		 * @param[in] str	C-string
		 * @return			Reference to string
		 */
		TString& Insert(const CharT * it, const AnsiChar * str);
		/**
		 * Insert a c-string to the string
		 * @param[in] it	Iterator to position to insert
		 * @param[in] str	C-string
		 * @return			Reference to string
		 */
		TString& Insert(const CharT * it, const WideChar * str);
		/**
		 * Insert an array to the string
		 * @param[in] pos	Position to insert
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 * @return			Reference to string
		 */
		TString& Insert(sizeT pos, const AnsiChar * arr, sizeT count);
		/**
		 * Insert an array to the string
		 * @param[in] pos	Position to insert
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 * @return			Reference to string
		 */
		TString& Insert(sizeT pos, const WideChar * arr, sizeT count);
		/**
		 * Insert an array to the string
		 * @param[in] it	Iterator to position to insert
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 * @return			Reference to string
		 */
		TString& Insert(const CharT * it, const AnsiChar * arr, sizeT count);
		/**
		 * Insert an array to the string
		 * @param[in] it	Iterator to position to insert
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 * @return			Reference to string
		 */
		TString& Insert(const CharT * it, const WideChar * arr, sizeT count);
		/**
		 * Insert a range of characters to the string
		 * @param[in] pos		Position to insert
		 * @param[in] itFirst	Iterator to first character
		 * @param[in] itLast	Iterator to the element after after last character
		 * @return				Reference to string
		 */
		TString& Insert(sizeT pos, const AnsiChar * itFirst, const AnsiChar * itLast);
		/**
		 * Insert a range of characters to the string
		 * @param[in] pos		Position to insert
		 * @param[in] itFirst	Iterator to first character
		 * @param[in] itLast	Iterator to the element after after last character
		 * @return				Reference to string
		 */
		TString& Insert(sizeT pos, const WideChar * itFirst, const WideChar * itLast);
		/**
		 * Insert a range of characters to the string
		 * @param[in] it		Iterator to position to insert
		 * @param[in] itFirst	Iterator to first character
		 * @param[in] itLast	Iterator to the element after after last character
		 * @return				Reference to string
		 */
		TString& Insert(const CharT * it, const AnsiChar * itFirst, const AnsiChar * itLast);
		/**
		 * Insert a range of characters to the string
		 * @param[in] it		Iterator to position to insert
		 * @param[in] itFirst	Iterator to first character
		 * @param[in] itLast	Iterator to the element after after last character
		 * @return				Reference to string
		 */
		TString& Insert(const CharT * it, const WideChar * itFirst, const WideChar * itLast);
		/**
		 * Insert a range of characters to the string
		 * @tparam InputIterator	Forward Iterator
		 * @param[in] pos			Position to insert
		 * @param[in] itFirst		Iterator to first character
		 * @param[in] itLast		Iterator to the element after after last character
		 * @return					Reference to string
		 */
		template<typename InputIterator>
		TString& Insert(sizeT pos, const InputIterator& itFirst, const InputIterator& itLast);
		/**
		 * Insert a range of characters to the string
		 * @tparam InputIterator	Forward Iterator
		 * @param[in] it			Iterator to position to insert
		 * @param[in] itFirst		Iterator to first character
		 * @param[in] itLast		Iterator to the element after after last character
		 * @return					Reference to string
		 */
		template<typename InputIterator>
		TString& Insert(const CharT * it, const InputIterator& itFirst, const InputIterator& itLast);
		/**
		 * Insert an initializer list to the string
		 * @param[in] pos	Position to insert
		 * @param[in] il	Initializer list
		 * @return			Reference to string
		 */
		TString& Insert(sizeT pos, std::initializer_list<AnsiChar> il);
		/**
		 * Insert an initializer list to the string
		 * @param[in] pos	Position to insert
		 * @param[in] il	Initializer list
		 * @return			Reference to string
		 */
		TString& Insert(sizeT pos, std::initializer_list<WideChar> il);
		/**
		 * Insert an initializer list to the string
		 * @param[in] it	Iterator to position to insert
		 * @param[in] il	Initializer list
		 * @return			Reference to string
		 */
		TString& Insert(const CharT * it, std::initializer_list<AnsiChar> il);
		/**
		 * Insert an initializer list to the string
		 * @param[in] it	Iterator to position to insert
		 * @param[in] il	Initializer list
		 * @return			Reference to string
		 */
		TString& Insert(const CharT * it, std::initializer_list<WideChar> il);
		/**
		/**
		 * Insert a string to the string
		 * @tparam C		Character type
		 * @tparam G		Growth policy
		 * @param[in] pos	Position to insert
		 * @param[in] str	String
		 * @return			Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Insert(sizeT pos, const TString<C, G>& str);
		/**
		 * Insert a string to the string
		 * @tparam C		Character type
		 * @tparam G		Growth policy
		 * @param[in] it	Iterator to position to insert
		 * @param[in] str	String
		 * @return			Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Insert(const CharT * it, const TString<C, G>& str);
		/**
		 * Insert a part of a string to the string
		 * @tparam C		Character type
		 * @tparam G		Growth policy
		 * @param[in] pos	Position to insert
		 * @param[in] str	String
		 * @param[in] len	Length of the string to add
		 * @return			Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Insert(sizeT pos, const TString<C, G>& str, sizeT len);
		/**
		 * Insert a part of a string to the string
		 * @tparam C		Character type
		 * @tparam G		Growth policy
		 * @param[in] it	Iterator to position to insert
		 * @param[in] str	String
		 * @param[in] len	Length of the string to add
		 * @return			Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Insert(const CharT * it, const TString<C, G>& str, sizeT len);
		/**
		 * Insert a substring to the string
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @param[in] pos		Position to insert
		 * @param[in] str		String
		 * @param[in] subPos	Start position of substring
		 * @param[in] len		Length of the string to add
		 * @return				Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Insert(sizeT pos, const TString<C, G>& str, sizeT subPos, sizeT len);
		/**
		 * Insert a substring to the string
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @param[in] it		Iterator to position to insert
		 * @param[in] str		String
		 * @param[in] subPos	Start position of substring
		 * @param[in] len		Length of the string to add
		 * @return				Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Insert(const CharT * it, const TString<C, G>& str, sizeT subPos, sizeT len);

		////////////////////////////////////////////////////////////////////////////////
		//	Erase and PopBack
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Erase a character
		 * @param[in] pos	Position to erase
		 * @return			Iterator to element which is now at pos, or Back() if no other element is available
		 */
		CharT * Erase(sizeT pos);
		/**
		 * Erase a character
		 * @param[in] it	Iterator to position to erase
		 * @return			Iterator to element which is now at it, or Back() if no other element is available
		 */
		CharT * Erase(const CharT * it);
		/**
		 * Erase a character
		 * @param[in] pos	Position to erase
		 * @param[in] count	Amount of characters to erase
		 * @return			Iterator to element which is now at pos, or Back() if no other element is available
		 */
		CharT * Erase(sizeT pos, sizeT count);
		/**
		 * Erase a character
		 * @param[in] it	Iterator to position to erase
		 * @param[in] count	Amount of characters to erase
		 * @return			Iterator to element which is now at it, or Back() if no other element is available
		 */
		CharT * Erase(const CharT * it, sizeT count);
		/**
		 * Erase a character
		 * @param[in] itFirst	Iterator to first character to erase
		 * @param[in] itLast		Iterator to element after last character to erase
		 * @return				Iterator to element which is now at itFirst, or Back() if no other element is available
		 */
		CharT * Erase(const CharT * itFirst, const CharT * itLast);

		/**
		 * Erase the last character of the string
		 */
		TString& PopBack();

		////////////////////////////////////////////////////////////////////////////////
		//	Replace
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Replace a character at a position with another character
		 * @param[in] pos	Position to replace
		 * @param[in] c		Character to replace with
		 * @return			Reference to string
		 */
		TString& Replace(sizeT pos, AnsiChar c);
		/**
		 * Replace a character at a position with another character
		 * @param[in] pos	Position to replace
		 * @param[in] c		Character to replace with
		 * @return			Reference to string
		 */
		TString& Replace(sizeT pos, WideChar c);
		/**
		 * Replace a character at a position with another character
		 * @param[in] it	Iterator to position to replace
		 * @param[in] c		Character to replace with
		 * @return			Reference to string
		 */
		TString& Replace(const CharT * it, AnsiChar c);
		/**
		 * Replace a character at a position with another character
		 * @param[in] it	Iterator to position to replace
		 * @param[in] c		Character to replace with
		 * @return			Reference to string
		 */
		TString& Replace(const CharT * it, WideChar c);
		/**
		 * Replace a character at a position with an amount of characters
		 * @param[in] pos		Position to replace
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 * @return				Reference to string
		 */
		TString& Replace(sizeT pos, AnsiChar c, sizeT amount);
		/**
		 * Replace a character at a position with an amount of characters
		 * @param[in] pos		Position to replace
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 * @return				Reference to string
		 */
		TString& Replace(sizeT pos, WideChar c, sizeT amount);
		/**
		 * Replace a character at a position with an amount of characters
		 * @param[in] it		Iterator to position to replace
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 * @return				Reference to string
		 */
		TString& Replace(const CharT * it, AnsiChar c, sizeT amount);
		/**
		 * Replace a character at a position with an amount of characters
		 * @param[in] it		Iterator to position to replace
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 * @return				Reference to string
		 */
		TString& Replace(const CharT * it, WideChar c, sizeT amount);
		/**
		 * Replace a character at a position with a c-string
		 * @param[in] pos	Position to replace
		 * @param[in] str	C-string
		 * @return			Reference to string
		 */
		TString& Replace(sizeT pos, const AnsiChar * str);
		/**
		 * Replace a character at a position with a c-string
		 * @param[in] pos	Position to replace
		 * @param[in] str	C-string
		 * @return			Reference to string
		 */
		TString& Replace(sizeT pos, const WideChar * str);
		/**
		 * Replace a character at a position with a c-string
		 * @param[in] it	Iterator to position to replace
		 * @param[in] str	C-string
		 * @return			Reference to string
		 */
		TString& Replace(const CharT * it, const AnsiChar * str);
		/**
		 * Replace a character at a position with a c-string
		 * @param[in] it	Iterator to position to replace
		 * @param[in] str	C-string
		 * @return			Reference to string
		 */
		TString& Replace(const CharT * it, const WideChar * str);
		/**
		 * Replace a character at a position with a string
		 * @tparam C		Character type
		 * @tparam G		Growth policy
		 * @param[in] pos	Position to replace
		 * @param[in] str	String
		 * @return			Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Replace(sizeT pos, const TString<C, G>& str);
		/**
		 * Replace a character at a position with a string
		 * @tparam C		Character type
		 * @tparam G		Growth policy
		 * @param[in]it		Iterator to position to replace
		 * @param[in] str	String
		 * @return			Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Replace(const CharT * it, const TString<C, G>& str);
		/**
		 * Replace a character at a position with a substring
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @param[in] pos		Position to replace
		 * @param[in] str		String
		 * @param[in] subPos	Start position of substring
		 * @param[in] subLen	Length of substring, if NPos, until string's end
		 * @return				Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Replace(sizeT pos, const TString<C, G>& str, sizeT subPos, sizeT subLen);
		/**
		 * Replace a character at a position with a substring
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @param[in]it			Iterator to position to replace
		 * @param[in] str		String
		 * @param[in] subPos	Start position of substring
		 * @param[in] subLen	Length of substring, if NPos, until string's end
		 * @return				Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Replace(const CharT * it, const TString<C, G>& str, sizeT subPos, sizeT subLen);


		/**
		 * Replace a character at a position with another character
		 * @param[in] pos	Position to replace
		 * @param[in] count	Amount of characters to remove
		 * @param[in] c		Character to replace with
		 * @return			Reference to string
		 */
		TString& Replace(sizeT pos, sizeT count, AnsiChar c);
		/**
		 * Replace a character at a position with another character
		 * @param[in] pos	Position to replace
		 * @param[in] count	Amount of characters to remove
		 * @param[in] c		Character to replace with
		 * @return			Reference to string
		 */
		TString& Replace(sizeT pos, sizeT count, WideChar c);
		/**
		 * Replace a character at a position with another character
		 * @param[in] it	Iterator to position to replace
		 * @param[in] count	Amount of characters to remove
		 * @param[in] c		Character to replace with
		 * @return			Reference to string
		 */
		TString& Replace(const CharT * it, sizeT count, AnsiChar c);
		/**
		 * Replace a character at a position with another character
		 * @param[in] it	Iterator to position to replace
		 * @param[in] count	Amount of characters to remove
		 * @param[in] c		Character to replace with
		 * @return			Reference to string
		 */
		TString& Replace(const CharT * it, sizeT count, WideChar c);
		/**
		 * Replace a character at a position with another character
		 * @param[in] itFirst	Iterator first character to replace
		 * @param[in] itLast	Iterator to character after last character to replace
		 * @param[in] c			Character to replace with
		 * @return				Reference to string
		 */
		TString& Replace(const CharT * itFirst, const CharT * itLast, AnsiChar c);
		/**
		 * Replace a character at a position with another character
		 * @param[in] itFirst	Iterator first character to replace
		 * @param[in] itLast	Iterator to character after last character to replace
		 * @param[in] c			Character to replace with
		 * @return				Reference to string
		 */
		TString& Replace(const CharT * itFirst, const CharT * itLast, WideChar c);
		/**
		 * Replace a character at a position with an amount of characters
		 * @param[in] pos		Position to replace
		 * @param[in] count		Amount of characters to remove
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 * @return				Reference to string
		 */
		TString& Replace(sizeT pos, sizeT count, AnsiChar c, sizeT amount);
		/**
		 * Replace a character at a position with an amount of characters
		 * @param[in] pos		Position to replace
		 * @param[in] count		Amount of characters to remove
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 * @return				Reference to string
		 */
		TString& Replace(sizeT pos, sizeT count, WideChar c, sizeT amount);
		/**
		 * Replace a character at a position with an amount of characters
		 * @param[in] it		Iterator to position to replace
		 * @param[in] count		Amount of characters to remove
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 * @return				Reference to string
		 */
		TString& Replace(const CharT * it, sizeT count, AnsiChar c, sizeT amount);
		/**
		 * Replace a character at a position with an amount of characters
		 * @param[in] it		Iterator to position to replace
		 * @param[in] count		Amount of characters to remove
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 * @return				Reference to string
		 */
		TString& Replace(const CharT * it, sizeT count, WideChar c, sizeT amount);
		/**
		 * Replace a character at a position with an amount of characters
		 * @param[in] itFirst	Iterator first character to replace
		 * @param[in] itLast	Iterator to character after last character to replace
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 * @return				Reference to string
		 */
		TString& Replace(const CharT * itFirst, const CharT * itLast, AnsiChar c, sizeT amount);
		/**
		 * Replace a character at a position with an amount of characters
		 * @param[in] itFirst	Iterator first character to replace
		 * @param[in] itLast	Iterator to character after last character to replace
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 * @return				Reference to string
		 */
		TString& Replace(const CharT * itFirst, const CharT * itLast, WideChar c, sizeT amount);
		/**
		 * Replace a character at a position with a c-string
		 * @param[in] pos	Position to replace
		 * @param[in] count	Amount of characters to remove
		 * @param[in] str	C-string
		 * @return			Reference to string
		 */
		TString& Replace(sizeT pos, sizeT count, const AnsiChar * str);
		/**
		 * Replace a character at a position with a c-string
		 * @param[in] pos	Position to replace
		 * @param[in] count	Amount of characters to remove
		 * @param[in] str	C-string
		 * @return			Reference to string
		 */
		TString& Replace(sizeT pos, sizeT count, const WideChar * str);
		/**
		 * Replace a character at a position with a c-string
		 * @param[in] it	Iterator to position to replace
		 * @param[in] count	Amount of characters to remove
		 * @param[in] str	C-string
		 * @return			Reference to string
		 */
		TString& Replace(const CharT * it, sizeT count, const AnsiChar * str);
		/**
		 * Replace a character at a position with a c-string
		 * @param[in] it	Iterator to position to replace
		 * @param[in] count	Amount of characters to remove
		 * @param[in] str	C-string
		 * @return			Reference to string
		 */
		TString& Replace(const CharT * it, sizeT count, const WideChar * str);
		/**
		 * Replace a character at a position with a c-string
		 * @param[in] itFirst	Iterator first character to replace
		 * @param[in] itLast	Iterator to character after last character to replace
		 * @param[in] str		C-string
		 * @return				Reference to string
		 */
		TString& Replace(const CharT * itFirst, const CharT * itLast, const AnsiChar * str);
		/**
		 * Replace a character at a position with a c-string
		 * @param[in] itFirst	Iterator first character to replace
		 * @param[in] itLast	Iterator to character after last character to replace
		 * @param[in] str		C-string
		 * @return				Reference to string
		 */
		TString& Replace(const CharT * itFirst, const CharT * itLast, const WideChar * str);
		/**
		 * Replace a character at a position with a string
		 * @tparam C		Character type
		 * @tparam G		Other growth policy
		 * @param[in] pos	Position to replace
		 * @param[in] count	Amount of characters to remove
		 * @param[in] str	String
		 * @return			Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Replace(sizeT pos, sizeT count, const TString<C, G>& str);
		/**
		 * Replace a character at a position with a string
		 * @tparam C		Character type
		 * @tparam G		Other growth policy
		 * @param[in] it	Iterator to position to replace
		 * @param[in] count	Amount of characters to remove
		 * @param[in] str	String
		 * @return			Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Replace(const CharT * it, sizeT count, const TString<C, G>& str);
		/**
		 * Replace a character at a position with a string
		 * @tparam C			Character type
		 * @tparam G			Other growth policy
		 * @param[in] itFirst	Iterator first character to replace
		 * @param[in] itLast	Iterator to character after last character to replace
		 * @param[in] str		String
		 * @return				Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Replace(const CharT * itFirst, const CharT * itLast, const TString<C, G>& str);
		/**
		 * Replace a character at a position with a string
		 * @tparam C			Character type
		 * @tparam G			Other growth policy
		 * @param[in] pos		Position to replace
		 * @param[in] count		Amount of characters to remove
		 * @param[in] str		String
		 * @param[in] subPos	Start position of substring
		 * @param[in] subLen	Length of substring, if NPos, until string's end
		 * @return				Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Replace(sizeT pos, sizeT count, const TString<C, G>& str, sizeT subPos, sizeT subLen);
		/**
		 * Replace a character at a position with a string
		 * @tparam C			Character type
		 * @tparam G			Other growth policy
		 * @param[in] it		Iterator to position to replace
		 * @param[in] count		Amount of characters to remove
		 * @param[in] str		String
		 * @param[in] subPos	Start position of substring
		 * @param[in] subLen	Length of substring, if NPos, until string's end
		 * @return				Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Replace(const CharT * it, sizeT count, const TString<C, G>& str, sizeT subPos, sizeT subLen);
		/**
		 * Replace a character at a position with a string
		 * @tparam C			Character type
		 * @tparam G			Other growth policy
		 * @param[in] itFirst	Iterator first character to replace
		 * @param[in] itLast	Iterator to character after last character to replace
		 * @param[in] str		String
		 * @param[in] subPos	Start position of substring
		 * @param[in] subLen	Length of substring, if NPos, until string's end
		 * @return				Reference to string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& Replace(const CharT * itFirst, const CharT * itLast, const TString<C, G>& str, sizeT subPos, sizeT subLen);

		////////////////////////////////////////////////////////////////////////////////
		//	ReplaceChar
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Replace any occurance of a character with another character
		 * @param[in] c0	Character to replace
		 * @param[in] c1	Character to replace with
		 */
		TString& ReplaceChar(AnsiChar c0, AnsiChar c1);
		/**
		 * Replace any occurance of a character with another character
		 * @param[in] c0	Character to replace
		 * @param[in] c1	Character to replace with
		 */
		TString& ReplaceChar(AnsiChar c0, WideChar c1);
		/**
		 * Replace any occurance of a character with another character
		 * @param[in] c0	Character to replace
		 * @param[in] c1	Character to replace with
		 */
		TString& ReplaceChar(WideChar c0, AnsiChar c1);
		/**
		 * Replace any occurance of a character with another character
		 * @param[in] c0	Character to replace
		 * @param[in] c1	Character to replace with
		 */
		TString& ReplaceChar(WideChar c0, WideChar c1);
		/**
		 * Replace any occurance of a character with another character
		 * @param[in] chars	Array with characters to replace
		 * @param[in] count	Array size
		 * @param[in] c		Character to replace with
		 */
		TString& ReplaceChar(const AnsiChar * chars, sizeT count, AnsiChar c);
		/**
		 * Replace any occurance of a character with another character
		 * @param[in] chars	Array with characters to replace
		 * @param[in] count	Array size
		 * @param[in] c		Character to replace with
		 */
		TString& ReplaceChar(const AnsiChar * chars, sizeT count, WideChar c);
		/**
		 * Replace any occurance of a character with another character
		 * @param[in] chars	Array with characters to replace
		 * @param[in] count	Array size
		 * @param[in] c		Character to replace with
		 */
		TString& ReplaceChar(const WideChar * chars, sizeT count, AnsiChar c);
		/**
		 * Replace any occurance of a character with another character
		 * @param[in] chars	Array with characters to replace
		 * @param[in] count	Array size
		 * @param[in] c		Character to replace with
		 */
		TString& ReplaceChar(const WideChar * chars, sizeT count, WideChar c);
		/**
		 * Replace any occurance of a character with another character
		 * @tparam N			Array size
		 * @param[in] chars	Array with characters to replace
		 * @param[in] c		Character to replace with
		 */
		template<sizeT N>
		TString& ReplaceChar(const AnsiChar(&chars)[N], AnsiChar c);
		/**
		 * Replace any occurance of a character with another character
		 * @tparam N			Array size
		 * @param[in] chars	Array with characters to replace
		 * @param[in] c		Character to replace with
		 */
		template<sizeT N>
		TString& ReplaceChar(const AnsiChar(&chars)[N], WideChar c);
		/**
		 * Replace any occurance of a character with another character
		 * @tparam N			Array size
		 * @param[in] chars	Array with characters to replace
		 * @param[in] c		Character to replace with
		 */
		template<sizeT N>
		TString& ReplaceChar(const WideChar(&chars)[N], AnsiChar c);
		/**
		 * Replace any occurance of a character with another character
		 * @tparam N			Array size
		 * @param[in] chars	Array with characters to replace
		 * @param[in] c		Character to replace with
		 */
		template<sizeT N>
		TString& ReplaceChar(const WideChar(&chars)[N], WideChar c);
		/**
		 * Replace any occurance of a character with an amount of characters
		 * @param[in] c0		Character to replace
		 * @param[in] c1		Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 */
		TString& ReplaceChar(AnsiChar c0, AnsiChar c1, sizeT amount);
		/**
		 * Replace any occurance of a character with an amount of characters
		 * @param[in] c0		Character to replace
		 * @param[in] c1		Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 */
		TString& ReplaceChar(AnsiChar c0, WideChar c1, sizeT amount);
		/**
		 * Replace any occurance of a character with an amount of characters
		 * @param[in] c0		Character to replace
		 * @param[in] c1		Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 */
		TString& ReplaceChar(WideChar c0, AnsiChar c1, sizeT amount);
		/**
		 * Replace any occurance of a character with an amount of characters
		 * @param[in] c0		Character to replace
		 * @param[in] c1		Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 */
		TString& ReplaceChar(WideChar c0, WideChar c1, sizeT amount);
		/**
		 * Replace any occurance of a character with an amount of characters
		 * @param[in] chars	Array with characters to replace
		 * @param[in] count	Array size
		 * @param[in] c		Character to replace with
		 */
		TString& ReplaceChar(const AnsiChar * chars, sizeT count, AnsiChar c, sizeT amount);
		/**
		 * Replace any occurance of a character with an amount of characters
		 * @param[in] chars	Array with characters to replace
		 * @param[in] count	Array size
		 * @param[in] c		Character to replace with
		 */
		TString& ReplaceChar(const AnsiChar * chars, sizeT count, WideChar c, sizeT amount);
		/**
		 * Replace any occurance of a character with an amount of characters
		 * @param[in] chars	Array with characters to replace
		 * @param[in] count	Array size
		 * @param[in] c		Character to replace with
		 */
		TString& ReplaceChar(const WideChar * chars, sizeT count, AnsiChar c, sizeT amount);
		/**
		 * Replace any occurance of a character with an amount of characters
		 * @param[in] chars	Array with characters to replace
		 * @param[in] count	Array size
		 * @param[in] c		Character to replace with
		 */
		TString& ReplaceChar(const WideChar * chars, sizeT count, WideChar c, sizeT amount);
		/**
		 * Replace any occurance of a character with an amount of characters
		 * @tparam N		Array size
		 * @param[in] chars	Array with characters to replace
		 * @param[in] c		Character to replace with
		 */
		template<sizeT N>
		TString& ReplaceChar(const AnsiChar(&chars)[N], AnsiChar c, sizeT amount);
		/**
		 * Replace any occurance of a character with an amount of characters
		 * @tparam N		Array size
		 * @param[in] chars	Array with characters to replace
		 * @param[in] c		Character to replace with
		 */
		template<sizeT N>
		TString& ReplaceChar(const AnsiChar(&chars)[N], WideChar c, sizeT amount);
		/**
		 * Replace any occurance of a character with an amount of characters
		 * @tparam N		Array size
		 * @param[in] chars	Array with characters to replace
		 * @param[in] c		Character to replace with
		 */
		template<sizeT N>
		TString& ReplaceChar(const WideChar(&chars)[N], AnsiChar c, sizeT amount);
		/**
		 * Replace any occurance of a character with an amount of characters
		 * @tparam N		Array size
		 * @param[in] chars	Array with characters to replace
		 * @param[in] c		Character to replace with
		 */
		template<sizeT N>
		TString& ReplaceChar(const WideChar(&chars)[N], WideChar c, sizeT amount);
		/**
		 * Replace any occurance of a character with a c-string
		 * @param[in] c		Character to replace
		 * @param[in] str	C-string
		 */
		TString& ReplaceChar(AnsiChar c, const AnsiChar * str);
		/**
		 * Replace any occurance of a character with a c-string
		 * @param[in] c		Character to replace
		 * @param[in] str	C-string
		 */
		TString& ReplaceChar(AnsiChar c, const WideChar * str);
		/**
		 * Replace any occurance of a character with a c-string
		 * @param[in] c		Character to replace
		 * @param[in] str	C-string
		 */
		TString& ReplaceChar(WideChar c, const AnsiChar * str);
		/**
		 * Replace any occurance of a character with a c-string
		 * @param[in] c		Character to replace
		 * @param[in] str	C-string
		 */
		TString& ReplaceChar(WideChar c, const WideChar * str);
		/**
		 * Replace any occurance of a character with a c-stSring
		 * @param[in] chars	Array with characters to replace
		 * @param[in] count	Array size
		 * @param[in] str	C-string
		 */
		TString& ReplaceChar(const AnsiChar * chars, sizeT count, const AnsiChar * str);
		/**
		 * Replace any occurance of a character with a c-stSring
		 * @param[in] chars	Array with characters to replace
		 * @param[in] count	Array size
		 * @param[in] str	C-string
		 */
		TString& ReplaceChar(const AnsiChar * chars, sizeT count, const WideChar * str);
		/**
		 * Replace any occurance of a character with a c-stSring
		 * @param[in] chars	Array with characters to replace
		 * @param[in] count	Array size
		 * @param[in] str	C-string
		 */
		TString& ReplaceChar(const WideChar * chars, sizeT count, const AnsiChar * str);
		/**
		 * Replace any occurance of a character with a c-stSring
		 * @param[in] chars	Array with characters to replace
		 * @param[in] count	Array size
		 * @param[in] str	C-string
		 */
		TString& ReplaceChar(const WideChar * chars, sizeT count, const WideChar * str);
		/**
		 * Replace any occurance of a character with a c-string
		 * @tparam N		Array size
		 * @param[in] chars	Array with characters to replace
		 * @param[in] str	C-string
		 */
		template<sizeT N>
		TString& ReplaceChar(const AnsiChar(&chars)[N], const AnsiChar * str);
		/**
		 * Replace any occurance of a character with a c-string
		 * @tparam N		Array size
		 * @param[in] chars	Array with characters to replace
		 * @param[in] str	C-string
		 */
		template<sizeT N>
		TString& ReplaceChar(const AnsiChar(&chars)[N], const WideChar * str);
		/**
		 * Replace any occurance of a character with a c-string
		 * @tparam N		Array size
		 * @param[in] chars	Array with characters to replace
		 * @param[in] str	C-string
		 */
		template<sizeT N>
		TString& ReplaceChar(const WideChar(&chars)[N], const AnsiChar * str);
		/**
		 * Replace any occurance of a character with a c-string
		 * @tparam N		Array size
		 * @param[in] chars	Array with characters to replace
		 * @param[in] str	C-string
		 */
		template<sizeT N>
		TString& ReplaceChar(const WideChar(&chars)[N], const WideChar * str);
		/**
		 * Replace any occurance of a character with a string
		 * @tparam C		Character type
		 * @tparam G		Growth policy
		 * @param[in] c		Character to replace
		 * @param[in] str	String
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceChar(AnsiChar c, const TString<C, G>& str);
		/**
		 * Replace any occurance of a character with a string
		 * @tparam C		Character type
		 * @tparam G		Growth policy
		 * @param[in] c		Character to replace
		 * @param[in] str	String
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceChar(WideChar c, const TString<C, G>& str);
		/**
		 * Replace any occurance of a character with a string
		 * @tparam C		Character type
		 * @tparam G		Growth policy
		 * @param[in] chars	Array with characters to replace
		 * @param[in] count	Array size
		 * @param[in] str	String
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceChar(const AnsiChar * chars, sizeT count, const TString<C, G>& str);
		/**
		 * Replace any occurance of a character with a string
		 * @tparam C		Character type
		 * @tparam G		Growth policy
		 * @param[in] chars	Array with characters to replace
		 * @param[in] count	Array size
		 * @param[in] str	String
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceChar(const WideChar * chars, sizeT count, const TString<C, G>& str);
		/**
		 * Replace any occurance of a character with a string
		 * @tparam C		Character type
		 * @tparam G		growth policy
		 * @tparam N		Array size
		 * @param[in] chars	Array with characters to replace
		 * @param[in] str	String
		 */
		template<typename C, sizeT N, Container::GrowthPolicyFunc G>
		TString& ReplaceChar(const AnsiChar(&chars)[N], const TString<C, G>& str);
		/**
		 * Replace any occurance of a character with a string
		 * @tparam C		Character type
		 * @tparam G		growth policy
		 * @tparam N		Array size
		 * @param[in] chars	Array with characters to replace
		 * @param[in] str	String
		 */
		template<typename C, sizeT N, Container::GrowthPolicyFunc G>
		TString& ReplaceChar(const WideChar(&chars)[N], const TString<C, G>& str);

		////////////////////////////////////////////////////////////////////////////////
		//	ReplaceStr
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Replace any occurance of a string with a character
		 * @param[in] str	String to replace
		 * @param[in] c		Character to replace with
		 */
		TString& ReplaceStr(const AnsiChar * str, AnsiChar c);
		/**
		 * Replace any occurance of a string with a character
		 * @param[in] str	String to replace
		 * @param[in] c		Character to replace with
		 */
		TString& ReplaceStr(const AnsiChar * str, WideChar c);
		/**
		 * Replace any occurance of a string with a character
		 * @param[in] str	String to replace
		 * @param[in] c		Character to replace with
		 */
		TString& ReplaceStr(const WideChar * str, AnsiChar c);
		/**
		 * Replace any occurance of a string with a character
		 * @param[in] str	String to replace
		 * @param[in] c		Character to replace with
		 */
		TString& ReplaceStr(const WideChar * str, WideChar c);
		/**
		 * Replace any occurance of a string with a character
		 * @param[in] strings	Array with strings to replace
		 * @param[in] count		Array size
		 * @param[in] c			Character to replace with
		 */
		TString& ReplaceStr(const AnsiChar* * strings, sizeT count, AnsiChar c);
		/**
		 * Replace any occurance of a string with a character
		 * @param[in] strings	Array with strings to replace
		 * @param[in] count		Array size
		 * @param[in] c			Character to replace with
		 */
		TString& ReplaceStr(const AnsiChar* * strings, sizeT count, WideChar c);
		/**
		 * Replace any occurance of a string with a character
		 * @param[in] strings	Array with strings to replace
		 * @param[in] count		Array size
		 * @param[in] c			Character to replace with
		 */
		TString& ReplaceStr(const WideChar* * strings, sizeT count, AnsiChar c);
		/**
		 * Replace any occurance of a string with a character
		 * @param[in] strings	Array with strings to replace
		 * @param[in] count		Array size
		 * @param[in] c			Character to replace with
		 */
		TString& ReplaceStr(const WideChar* * strings, sizeT count, WideChar c);
		/**
		 * Replace any occurance of a string with a character
		 * @tparam N			Array size
		 * @param[in] strings	Array with strings to replace
		 * @param[in] c			Character to replace with
		 */
		template<sizeT N>
		TString& ReplaceStr(const AnsiChar*(&strings)[N], AnsiChar c);
		/**
		 * Replace any occurance of a string with a character
		 * @tparam N			Array size
		 * @param[in] strings	Array with strings to replace
		 * @param[in] c			Character to replace with
		 */
		template<sizeT N>
		TString& ReplaceStr(const AnsiChar*(&strings)[N], WideChar c);
		/**
		 * Replace any occurance of a string with a character
		 * @tparam N			Array size
		 * @param[in] strings	Array with strings to replace
		 * @param[in] c			Character to replace with
		 */
		template<sizeT N>
		TString& ReplaceStr(const WideChar*(&strings)[N], AnsiChar c);
		/**
		 * Replace any occurance of a string with a character
		 * @tparam N			Array size
		 * @param[in] strings	Array with strings to replace
		 * @param[in] c			Character to replace with
		 */
		template<sizeT N>
		TString& ReplaceStr(const WideChar*(&strings)[N], WideChar c);
		/**
		 * Replace any occurance of a string with a character
		 * @tparam C		Character type
		 * @tparam G		Growth policy
		 * @param[in] str	String to replace
		 * @param[in] c		Character to replace with
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const TString<C, G>& str, AnsiChar c);
		/**
		 * Replace any occurance of a string with a character
		 * @tparam C		Character type
		 * @tparam G		Growth policy
		 * @param[in] str	String to replace
		 * @param[in] c		Character to replace with
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const TString<C, G>& str, WideChar c);
		/**
		 * Replace any occurance of a string with a character
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @param[in] strings	Array with strings to replace
		 * @param[in] count		Array size
		 * @param[in] c			Character to replace with
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const TString<C, G> * strings, sizeT count, AnsiChar c);
		/**
		 * Replace any occurance of a string with a character
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @param[in] strings	Array with strings to replace
		 * @param[in] count		Array size
		 * @param[in] c			Character to replace with
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const TString<C, G> * strings, sizeT count, WideChar c);
		/**
		 * Replace any occurance of a string with a character
		 * @tparam C			Character type
		 * @tparam N			Array size
		 * @tparam G			Growth policy
		 * @param[in] strings	Array with strings to replace
		 * @param[in] c			Character to replace with
		 */
		template<typename C, sizeT N, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const TString<C, G>(&strings)[N], AnsiChar c);
		/**
		 * Replace any occurance of a string with a character
		 * @tparam C			Character type
		 * @tparam N			Array size
		 * @tparam G			Growth policy
		 * @param[in] strings	Array with strings to replace
		 * @param[in] c			Character to replace with
		 */
		template<typename C, sizeT N, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const TString<C, G>(&strings)[N], WideChar c);
		/**
		 * Replace any occurance of a string with an amount of characters
		 * @param[in] str		Character to replace
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 */
		TString& ReplaceStr(const AnsiChar * str, AnsiChar c, sizeT amount);
		/**
		 * Replace any occurance of a string with an amount of characters
		 * @param[in] str		Character to replace
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 */
		TString& ReplaceStr(const AnsiChar * str, WideChar c, sizeT amount);
		/**
		 * Replace any occurance of a string with an amount of characters
		 * @param[in] str		Character to replace
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 */
		TString& ReplaceStr(const WideChar * str, AnsiChar c, sizeT amount);
		/**
		 * Replace any occurance of a string with an amount of characters
		 * @param[in] str		Character to replace
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 */
		TString& ReplaceStr(const WideChar * str, WideChar c, sizeT amount);
		/**
		 * Replace any occurance of a string with an amount of characters
		 * @param[in] strings	Array with characters to replace
		 * @param[in] count		Array size
		 * @param[in] c			Character to replace with
		 */
		TString& ReplaceStr(const AnsiChar* * strings, sizeT count, AnsiChar c, sizeT amount);
		/**
		 * Replace any occurance of a string with an amount of characters
		 * @param[in] strings	Array with characters to replace
		 * @param[in] count		Array size
		 * @param[in] c			Character to replace with
		 */
		TString& ReplaceStr(const AnsiChar* * strings, sizeT count, WideChar c, sizeT amount);
		/**
		 * Replace any occurance of a string with an amount of characters
		 * @param[in] strings	Array with characters to replace
		 * @param[in] count		Array size
		 * @param[in] c			Character to replace with
		 */
		TString& ReplaceStr(const WideChar* * strings, sizeT count, AnsiChar c, sizeT amount);
		/**
		 * Replace any occurance of a string with an amount of characters
		 * @param[in] strings	Array with characters to replace
		 * @param[in] count		Array size
		 * @param[in] c			Character to replace with
		 */
		TString& ReplaceStr(const WideChar* * strings, sizeT count, WideChar c, sizeT amount);
		/**
		 * Replace any occurance of a string with an amount of characters
		 * @tparam N			Array size
		 * @param[in] strings	Array with characters to replace
		 * @param[in] c			Character to replace with
		 */
		template<sizeT N>
		TString& ReplaceStr(const AnsiChar*(&strings)[N], AnsiChar c, sizeT amount);
		/**
		 * Replace any occurance of a string with an amount of characters
		 * @tparam N			Array size
		 * @param[in] strings	Array with characters to replace
		 * @param[in] c			Character to replace with
		 */
		template<sizeT N>
		TString& ReplaceStr(const AnsiChar*(&strings)[N], WideChar c, sizeT amount);
		/**
		 * Replace any occurance of a string with an amount of characters
		 * @tparam N			Array size
		 * @param[in] strings	Array with characters to replace
		 * @param[in] c			Character to replace with
		 */
		template<sizeT N>
		TString& ReplaceStr(const WideChar*(&strings)[N], AnsiChar c, sizeT amount);
		/**
		 * Replace any occurance of a string with an amount of characters
		 * @tparam N			Array size
		 * @param[in] strings	Array with characters to replace
		 * @param[in] c			Character to replace with
		 */
		template<sizeT N>
		TString& ReplaceStr(const WideChar*(&strings)[N], WideChar c, sizeT amount);
		/**
		 * Replace any occurance of a string with a character
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @param[in] str		String to replace
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const TString<C, G>& str, AnsiChar c, sizeT amount);
		/**
		 * Replace any occurance of a string with a character
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @param[in] str		String to replace
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const TString<C, G>& str, WideChar c, sizeT amount);
		/**
		 * Replace any occurance of a string with a character
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @param[in] strings	Array with strings to replace
		 * @param[in] count		Array size
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const TString<C, G> * strings, sizeT count, AnsiChar c, sizeT amount);
		/**
		 * Replace any occurance of a string with a character
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @param[in] strings	Array with strings to replace
		 * @param[in] count		Array size
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const TString<C, G> * strings, sizeT count, WideChar c, sizeT amount);
		/**
		 * Replace any occurance of a string with a character
		 * @tparam C			Character type
		 * @tparam N			Array size
		 * @tparam G			Growth policy
		 * @param[in] strings	Array with strings to replace
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 */
		template<typename C, sizeT N, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const TString<C, G>(&strings)[N], AnsiChar c, sizeT amount);
		/**
		 * Replace any occurance of a string with a character
		 * @tparam C			Character type
		 * @tparam N			Array size
		 * @tparam G			Growth policy
		 * @param[in] strings	Array with strings to replace
		 * @param[in] c			Character to replace with
		 * @param[in] amount	Amount of characters to replace with
		 */
		template<typename C, sizeT N, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const TString<C, G>(&strings)[N], WideChar c, sizeT amount);
		/**
		 * Replace any occurance of a string with a c-string
		 * @param[in] str0	String to replace
		 * @param[in] str1	C-string
		 */
		TString& ReplaceStr(const AnsiChar * str0, const AnsiChar * str1);
		/**
		 * Replace any occurance of a string with a c-string
		 * @param[in] str0	String to replace
		 * @param[in] str1	C-string
		 */
		TString& ReplaceStr(const AnsiChar * str0, const WideChar * str1);
		/**
		 * Replace any occurance of a string with a c-string
		 * @param[in] str0	String to replace
		 * @param[in] str1	C-string
		 */
		TString& ReplaceStr(const WideChar * str0, const AnsiChar * str1);
		/**
		 * Replace any occurance of a string with a c-string
		 * @param[in] str0	String to replace
		 * @param[in] str1	C-string
		 */
		TString& ReplaceStr(const WideChar * str0, const WideChar * str1);
		/**
		 * Replace any occurance of a string with a c-string
		 * @param[in] strings	Array with characters to replace
		 * @param[in] count		Array size
		 * @param[in] str		C-string
		 */
		TString& ReplaceStr(const AnsiChar* * strings, sizeT count, const AnsiChar * str);
		/**
		 * Replace any occurance of a string with a c-string
		 * @param[in] strings	Array with characters to replace
		 * @param[in] count		Array size
		 * @param[in] str		C-string
		 */
		TString& ReplaceStr(const AnsiChar* * strings, sizeT count, const WideChar * str);
		/**
		 * Replace any occurance of a string with a c-string
		 * @param[in] strings	Array with characters to replace
		 * @param[in] count		Array size
		 * @param[in] str		C-string
		 */
		TString& ReplaceStr(const WideChar* * strings, sizeT count, const AnsiChar * str);
		/**
		 * Replace any occurance of a string with a c-string
		 * @param[in] strings	Array with characters to replace
		 * @param[in] count		Array size
		 * @param[in] str		C-string
		 */
		TString& ReplaceStr(const WideChar* * strings, sizeT count, const WideChar * str);
		/**
		 * Replace any occurance of a string with a c-string
		 * @tparam N			Array size
		 * @param[in] strings	Array with characters to replace
		 * @param[in] str		C-string
		 */
		template<sizeT N>
		TString& ReplaceStr(const AnsiChar*(&strings)[N], const AnsiChar * str);
		/**
		 * Replace any occurance of a string with a c-string
		 * @tparam N			Array size
		 * @param[in] strings	Array with characters to replace
		 * @param[in] str		C-string
		 */
		template<sizeT N>
		TString& ReplaceStr(const AnsiChar*(&strings)[N], const WideChar * str);
		/**
		 * Replace any occurance of a string with a c-string
		 * @tparam N			Array size
		 * @param[in] strings	Array with characters to replace
		 * @param[in] str		C-string
		 */
		template<sizeT N>
		TString& ReplaceStr(const WideChar*(&strings)[N], const AnsiChar * str);
		/**
		 * Replace any occurance of a string with a c-string
		 * @tparam N			Array size
		 * @param[in] strings	Array with characters to replace
		 * @param[in] str		C-string
		 */
		template<sizeT N>
		TString& ReplaceStr(const WideChar*(&strings)[N], const WideChar * str);
		/**
		 * Replace any occurance of a string with a c-string
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @param[in] str0		String to replace
		 * @param[in] str1		C-string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const TString<C, G>& str0, const AnsiChar * str1);
		/**
		 * Replace any occurance of a string with a c-string
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @param[in] str0		String to replace
		 * @param[in] str1		C-string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const TString<C, G>& str0, const WideChar * str1);
		/**
		 * Replace any occurance of a string with a c-string
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @param[in] strings	Array with strings to replace
		 * @param[in] count		Array size
		 * @param[in] str		C-string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const TString<C, G> * strings, sizeT count, const AnsiChar * str);
		/**
		 * Replace any occurance of a string with a c-string
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @param[in] strings	Array with strings to replace
		 * @param[in] count		Array size
		 * @param[in] str		C-string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const TString<C, G> * strings, sizeT count, const WideChar * str);
		/**
		 * Replace any occurance of a string with a c-string
		 * @tparam C			Character type
		 * @tparam N			Array size
		 * @tparam G			Growth policy
		 * @param[in] strings	Array with strings to replace
		 * @param[in] str		C-string
		 */
		template<typename C, sizeT N, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const TString<C, G>(&strings)[N], const AnsiChar * str);
		/**
		 * Replace any occurance of a string with a c-string
		 * @tparam C			Character type
		 * @tparam N			Array size
		 * @tparam G			Growth policy
		 * @param[in] strings	Array with strings to replace
		 * @param[in] str		C-string
		 */
		template<typename C, sizeT N, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const TString<C, G>(&strings)[N], const WideChar * str);
		/**
		 * Replace any occurance of a string with a string
		 * @tparam C			Character type
		 * @tparam G			Other growth policy
		 * @param[in] str0		C-string to replace
		 * @param[in] str1		String
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const AnsiChar * str0, const TString<C, G>& str1);
		/**
		 * Replace any occurance of a string with a string
		 * @tparam C			Character type
		 * @tparam G			Other growth policy
		 * @param[in] str0		C-string to replace
		 * @param[in] str1		String
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const WideChar * str0, const TString<C, G>& str1);
		/**
		 * Replace any occurance of a string with a string
		 * @tparam C			Character type
		 * @tparam G			Other growth policy
		 * @param[in] strings	Array with characters to replace
		 * @param[in] count		Array size
		 * @param[in] str		String
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const AnsiChar* * strings, sizeT count, const TString<C, G>& str);
		/**
		 * Replace any occurance of a string with a string
		 * @tparam C			Character type
		 * @tparam G			Other growth policy
		 * @param[in] strings	Array with characters to replace
		 * @param[in] count		Array size
		 * @param[in] str		String
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const WideChar* * strings, sizeT count, const TString<C, G>& str);
		/**
		 * Replace any occurance of a string with a string
		 * @tparam N			Array size
		 * @tparam C			Character type
		 * @tparam G			Other growth policy
		 * @param[in] strings	Array with characters to replace
		 * @param[in] str		String
		 */
		template<sizeT N, typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const AnsiChar*(&strings)[N], const TString<C, G>& str);
		/**
		 * Replace any occurance of a string with a string
		 * @tparam N			Array size
		 * @tparam C			Character type
		 * @tparam G			Other growth policy
		 * @param[in] strings	Array with characters to replace
		 * @param[in] str		String
		 */
		template<sizeT N, typename C, Container::GrowthPolicyFunc G>
		TString& ReplaceStr(const WideChar*(&strings)[N], const TString<C, G>& str);
		/**
		 * Replace any occurance of a string with a c-string
		 * @tparam C0			Character type
		 * @tparam C1			Character type
		 * @tparam G0			Growth policy
		 * @tparam G1			Growth policy
		 * @param[in] str0		String to replace
		 * @param[in] str1		C-string
		 */
		template<typename C0, typename C1, Container::GrowthPolicyFunc G0, Container::GrowthPolicyFunc G1>
		TString& ReplaceStr(const TString<C0, G0>& str0, const TString<C1, G1>& str1);
		/**
		 * Replace any occurance of a string with a c-string
		 * @tparam C0			Character type
		 * @tparam C1			Character type
		 * @tparam G0			Growth policy
		 * @tparam G1			Growth policy
		 * @param[in] strings	Array with strings to replace
		 * @param[in] count		Array size
		 * @param[in] str		C-string
		 */
		template<typename C0, typename C1, Container::GrowthPolicyFunc G0, Container::GrowthPolicyFunc G1>
		TString& ReplaceStr(const TString<C0, G0> * strings, sizeT count, const TString<C1, G1>& str);
		/**
		 * Replace any occurance of a string with a c-string
		 * @tparam C0			Character type
		 * @tparam C1			Character type
		 * @tparam N			Array size
		 * @tparam G0			Growth policy
		 * @tparam G1			Growth policy
		 * @param[in] strings	Array with strings to replace
		 * @param[in] str		C-string
		 */
		template<typename C0, typename C1, sizeT N, Container::GrowthPolicyFunc G0, Container::GrowthPolicyFunc G1>
		TString& ReplaceStr(const TString<C0, G0>(&strings)[N], const TString<C1, G1>& str);

		////////////////////////////////////////////////////////////////////////////////
		//	Find
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Find the first occurance of a character in the string
		 * @param[in] c				Character to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the character, NPos if nothing was found
		 */
		sizeT Find(AnsiChar c, sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a character in the string
		 * @param[in] c				Character to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the character, NPos if nothing was found
		 */
		sizeT Find(WideChar c, sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a c-string in the string
		 * @param[in] str			C-string to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the c-string, NPos if nothing was found
		 */
		sizeT Find(const AnsiChar * str, sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a c-string in the string
		 * @param[in] str			C-string to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the c-string, NPos if nothing was found
		 */
		sizeT Find(const WideChar * str, sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a c-string in the string
		 * @param[in] str			C-string to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] count			Amount of characters to compare
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the c-string, NPos if nothing was found
		 */
		sizeT Find(const AnsiChar * str, sizeT pos, sizeT count, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a c-string in the string
		 * @param[in] str			C-string to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] count			Amount of characters to compare
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the c-string, NPos if nothing was found
		 */
		sizeT Find(const WideChar * str, sizeT pos, sizeT count, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a c-string in the string
		 * @param[in] str			C-string to find
		 * @param[in] len			String length
		 * @param[in] pos			Position to start searching from
		 * @param[in] count			Amount of characters to compare
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the c-string, NPos if nothing was found
		 */
		sizeT Find(const AnsiChar * str, sizeT len, sizeT pos, sizeT count, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a c-string in the string
		 * @param[in] str			C-string to find
		 * @param[in] len			String length
		 * @param[in] pos			Position to start searching from
		 * @param[in] count			Amount of characters to compare
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the c-string, NPos if nothing was found
		 */
		sizeT Find(const WideChar * str, sizeT len, sizeT pos, sizeT count, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a string in the string
		 * @tparam C				Character type
		 * @tparam G				Growth polict
		 * @param[in] str			String to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the string, NPos if nothing was found
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		sizeT Find(const TString<C, G>& str, sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a string in the string
		 * @tparam C				Character type
		 * @tparam G				Growth polict
		 * @param[in] str			String to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] count			Amount of characters to compare
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the string, NPos if nothing was found
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		sizeT Find(const TString<C, G>& str, sizeT pos, sizeT count, b8 caseSensitive = true) const;

		////////////////////////////////////////////////////////////////////////////////
		//	FindAny
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Find the first occurance of any character in the string
		 * @param[in] arr			Array of characters to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character, NPos if nothing was found
		 */
		sizeT FindAny(const AnsiChar * arr, sizeT count, sizeT pos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any character in the string
		 * @param[in] arr			Array of characters to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character, NPos if nothing was found
		 */
		sizeT FindAny(const WideChar * arr, sizeT count, sizeT pos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any character of a c-string in the string
		 * @param[in] str			C-string with characters to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character, NPos if nothing was found
		 */
		sizeT FindAny(const AnsiChar * str, sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any character of a c-string in the string
		 * @param[in] str			C-string with characters to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character, NPos if nothing was found
		 */
		sizeT FindAny(const WideChar * str, sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any c-string in the string
		 * @param[in] arr			Array of c-strings to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character of a c-string, NPos if nothing was found
		 */
		sizeT FindAny(const AnsiChar* * arr, sizeT count, sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any c-string in the string
		 * @param[in] arr			Array of c-strings to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character of a c-string, NPos if nothing was found
		 */
		sizeT FindAny(const WideChar* * arr, sizeT count, sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any c-string in the string
		 * @tparam N				Array size
		 * @param[in] arr			Array of c-strings to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any c-string, NPos if nothing was found
		 */
		template<sizeT N>
		sizeT FindAny(const AnsiChar*(&arr)[N], sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any c-string in the string
		 * @tparam N				Array size
		 * @param[in] arr			Array of c-strings to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any c-string, NPos if nothing was found
		 */
		template<sizeT N>
		sizeT FindAny(const WideChar*(&arr)[N], sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any character of a string in the string
		 * @tparam C				Character type
		 * @tparam G				Growth polict
		 * @param[in] str			String with characters to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character of a string, NPos if nothing was found
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		sizeT FindAny(const TString<C, G>& str, sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any string in the string
		 * @tparam C				Character type
		 * @tparam G				Growth polict
		 * @param[in] arr			Array with strings to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any string, NPos if nothing was found
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		sizeT FindAny(const TString<C, G> * arr, sizeT count, sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any string in the string
		 * @tparam C				Character type
		 * @tparam G				Growth polict
		 * @tparam N				Array size
		 * @param[in] arr			Array with strings to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any string, NPos if nothing was found
		 */
		template<typename C, Container::GrowthPolicyFunc G, sizeT N>
		sizeT FindAny(const TString<C, G>(&arr)[N], sizeT pos = 0, b8 caseSensitive = true) const;

		////////////////////////////////////////////////////////////////////////////////
		//	FindAnyWithLength
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Find the first occurance of any c-string in the string
		 * @param[in] arr			Array of c-strings to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Pair with:
		*							-index to the first occurance of any character of a c-string, NPos if nothing was found
		*							-length of the found string
		 */
		Pair<sizeT, sizeT> FindAnyWithLength(const AnsiChar* * arr, sizeT count, sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any c-string in the string
		 * @param[in] arr			Array of c-strings to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Pair with:
		*							-index to the first occurance of any character of a c-string, NPos if nothing was found
		*							-length of the found string
		 */
		Pair<sizeT, sizeT> FindAnyWithLength(const WideChar* * arr, sizeT count, sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any c-string in the string
		 * @tparam N				Array size
		 * @param[in] arr			Array of c-strings to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Pair with:
		*							-index to the first occurance of any character of a c-string, NPos if nothing was found
		*							-length of the found string
		 */
		template<sizeT N>
		Pair<sizeT, sizeT> FindAnyWithLength(const AnsiChar*(&arr)[N], sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any c-string in the string
		 * @tparam N				Array size
		 * @param[in] arr			Array of c-strings to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Pair with:
		*							-index to the first occurance of any character of a c-string, NPos if nothing was found
		*							-length of the found string
		 */
		template<sizeT N>
		Pair<sizeT, sizeT> FindAnyWithLength(const WideChar*(&arr)[N], sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any string in the string
		 * @tparam C				Character type
		 * @tparam G				Growth polict
		 * @param[in] arr			Array with strings to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Pair with:
		*							-index to the first occurance of any character of a c-string, NPos if nothing was found
		*							-length of the found string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		Pair<sizeT, sizeT> FindAnyWithLength(const TString<C, G> * arr, sizeT count, sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any string in the string
		 * @tparam C				Character type
		 * @tparam G				Growth polict
		 * @tparam N				Array size
		 * @param[in] arr			Array with strings to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Pair with:
		*							-index to the first occurance of any character of a c-string, NPos if nothing was found
		*							-length of the found string
		 */
		template<typename C, Container::GrowthPolicyFunc G, sizeT N>
		Pair<sizeT, sizeT> FindAnyWithLength(const TString<C, G>(&arr)[N], sizeT pos = 0, b8 caseSensitive = true) const;

		////////////////////////////////////////////////////////////////////////////////
		//	FindAnyNotOf
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Find the first occurance of a character, which is not in a given array, in the string
		 * @param[in] arr			Array of characters to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character, NPos if nothing was found
		 */
		sizeT FindAnyNotOf(const AnsiChar * arr, sizeT count, sizeT pos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a character, which is not in a given array, in the string
		 * @param[in] arr			Array of characters to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character, NPos if nothing was found
		 */
		sizeT FindAnyNotOf(const WideChar * arr, sizeT count, sizeT pos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a character, which is not in a given c-string, in the string
		 * @param[in] str			C-string with characters to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character, NPos if nothing was found
		 */
		sizeT FindAnyNotOf(const AnsiChar * str, sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a character, which is not in a given c-string, in the string
		 * @param[in] str			C-string with characters to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character, NPos if nothing was found
		 */
		sizeT FindAnyNotOf(const WideChar * str, sizeT pos = 0, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a character, which is not in a given string, in the string
		 * @tparam C				Character type
		 * @tparam G				Growth polict
		 * @param[in] str			String with characters to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of character of a string string, NPos if nothing was found
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		sizeT FindAnyNotOf(const TString<C, G>& str, sizeT pos = 0, b8 caseSensitive = true) const;

		////////////////////////////////////////////////////////////////////////////////
		//	RFind
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Find the first occurance of a character in the string, iterating from back to front
		 * @param[in] c				Character to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the character, NPos if nothing was found
		 */
		sizeT RFind(AnsiChar c, sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a character in the string, iterating from back to front
		 * @param[in] c				Character to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the character, NPos if nothing was found
		 */
		sizeT RFind(WideChar c, sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a c-string in the string, iterating from back to front
		 * @param[in] str			C-string to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the c-string, NPos if nothing was found
		 */
		sizeT RFind(const AnsiChar * str, sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a c-string in the string, iterating from back to front
		 * @param[in] str			C-string to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the c-string, NPos if nothing was found
		 */
		sizeT RFind(const WideChar * str, sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a c-string in the string, iterating from back to front
		 * @param[in] str			C-string to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] count			Amount of characters to compare
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the c-string, NPos if nothing was found
		 */
		sizeT RFind(const AnsiChar * str, sizeT pos, sizeT count, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a c-string in the string, iterating from back to front
		 * @param[in] str			C-string to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] count			Amount of characters to compare
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the c-string, NPos if nothing was found
		 */
		sizeT RFind(const WideChar * str, sizeT pos, sizeT count, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a c-string in the string
		 * @param[in] str			C-string to find
		 * @param[in] len			String length
		 * @param[in] pos			Position to start searching from
		 * @param[in] count			Amount of characters to compare
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the c-string, NPos if nothing was found
		 */
		sizeT RFind(const AnsiChar * str, sizeT len, sizeT pos, sizeT count, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a c-string in the string
		 * @param[in] str			C-string to find
		 * @param[in] len			String length
		 * @param[in] pos			Position to start searching from
		 * @param[in] count			Amount of characters to compare
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the c-string, NPos if nothing was found
		 */
		sizeT RFind(const WideChar * str, sizeT len, sizeT pos, sizeT count, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a string in the string, iterating from back to front
		 * @tparam C				Character type
		 * @tparam G				Growth polict
		 * @param[in] str			String to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the string, NPos if nothing was found
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		sizeT RFind(const TString<C, G>& str, sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a string in the string, iterating from back to front
		 * @tparam C				Character type
		 * @tparam G				Growth polict
		 * @param[in] str			String to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] count			Amount of characters to compare
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of the string, NPos if nothing was found
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		sizeT RFind(const TString<C, G>& str, sizeT pos, sizeT count, b8 caseSensitive = true) const;

		////////////////////////////////////////////////////////////////////////////////
		//	RFindAny
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Find the first occurance of any character in the string, iterating from back to front
		 * @param[in] arr			Array of characters to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character, NPos if nothing was found
		 */
		sizeT RFindAny(const AnsiChar * arr, sizeT count, sizeT pos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any character in the string, iterating from back to front
		 * @param[in] arr			Array of characters to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character, NPos if nothing was found
		 */
		sizeT RFindAny(const WideChar * arr, sizeT count, sizeT pos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any character of a c-string in the string, iterating from back to front
		 * @param[in] str			C-string with characters to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character, NPos if nothing was found
		 */
		sizeT RFindAny(const AnsiChar * str, sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any character of a c-string in the string, iterating from back to front
		 * @param[in] str			C-string with characters to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character, NPos if nothing was found
		 */
		sizeT RFindAny(const WideChar * str, sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any c-string in the string, iterating from back to front
		 * @param[in] arr			Array of c-strings to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character of a c-string, NPos if nothing was found
		 */
		sizeT RFindAny(const AnsiChar* * arr, sizeT count, sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any c-string in the string, iterating from back to front
		 * @param[in] arr			Array of c-strings to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character of a c-string, NPos if nothing was found
		 */
		sizeT RFindAny(const WideChar* * arr, sizeT count, sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any c-string in the string, iterating from back to front
		 * @tparam N				Array size
		 * @param[in] arr			Array of c-strings to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any c-string, NPos if nothing was found
		 */
		template<sizeT N>
		sizeT RFindAny(const AnsiChar*(&arr)[N], sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any c-string in the string, iterating from back to front
		 * @tparam N				Array size
		 * @param[in] arr			Array of c-strings to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any c-string, NPos if nothing was found
		 */
		template<sizeT N>
		sizeT RFindAny(const WideChar*(&arr)[N], sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any character of a string in the string, iterating from back to front
		 * @tparam C				Character type
		 * @tparam G				Growth polict
		 * @param[in] str			String with characters to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character of a string, NPos if nothing was found
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		sizeT RFindAny(const TString<C, G>& str, sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any string in the string, iterating from back to front
		 * @tparam C				Character type
		 * @tparam G				Growth polict
		 * @param[in] arr			Array with strings to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any string, NPos if nothing was found
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		sizeT RFindAny(const TString<C, G> * arr, sizeT count, sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any string in the string, iterating from back to front
		 * @tparam C				Character type
		 * @tparam G				Growth polict
		 * @tparam N				Array size
		 * @param[in] arr			Array with strings to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any string, NPos if nothing was found
		 */
		template<typename C, Container::GrowthPolicyFunc G, sizeT N>
		sizeT RFindAny(const TString<C, G>(&arr)[N], sizeT pos = NPos, b8 caseSensitive = true) const;

		////////////////////////////////////////////////////////////////////////////////
		//	RFindAnyWithLength
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Find the first occurance of any c-string in the string, iterating from back to front
		 * @param[in] arr			Array of c-strings to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Pair with:
		*							-index to the first occurance of any character of a c-string, NPos if nothing was found
		*							-length of the found string
		 */
		Pair<sizeT, sizeT> RFindAnyWithLength(const AnsiChar* * arr, sizeT count, sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any c-string in the string, iterating from back to front
		 * @param[in] arr			Array of c-strings to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Pair with:
		*							-index to the first occurance of any character of a c-string, NPos if nothing was found
		*							-length of the found string
		 */
		Pair<sizeT, sizeT> RFindAnyWithLength(const WideChar* * arr, sizeT count, sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any c-string in the string, iterating from back to front
		 * @tparam N				Array size
		 * @param[in] arr			Array of c-strings to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Pair with:
		*							-index to the first occurance of any character of a c-string, NPos if nothing was found
		*							-length of the found string
		 */
		template<sizeT N>
		Pair<sizeT, sizeT> RFindAnyWithLength(const AnsiChar*(&arr)[N], sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any c-string in the string, iterating from back to front
		 * @tparam N				Array size
		 * @param[in] arr			Array of c-strings to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Pair with:
		*							-index to the first occurance of any character of a c-string, NPos if nothing was found
		*							-length of the found string
		 */
		template<sizeT N>
		Pair<sizeT, sizeT> RFindAnyWithLength(const WideChar*(&arr)[N], sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any string in the string, iterating from back to front
		 * @tparam C				Character type
		 * @tparam G				Growth polict
		 * @param[in] arr			Array with strings to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Pair with:
		*							-index to the first occurance of any character of a c-string, NPos if nothing was found
		*							-length of the found string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		Pair<sizeT, sizeT> RFindAnyWithLength(const TString<C, G> * arr, sizeT count, sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of any string in the string, iterating from back to front
		 * @tparam C				Character type
		 * @tparam G				Growth polict
		 * @tparam N				Array size
		 * @param[in] arr			Array with strings to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Pair with:
		*							-index to the first occurance of any character of a c-string, NPos if nothing was found
		*							-length of the found string
		 */
		template<typename C, Container::GrowthPolicyFunc G, sizeT N>
		Pair<sizeT, sizeT> RFindAnyWithLength(const TString<C, G>(&arr)[N], sizeT pos = NPos, b8 caseSensitive = true) const;

		////////////////////////////////////////////////////////////////////////////////
		//	RFindAnyNotOf
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Find the first occurance of a character, which is not in a given array, in the string, iterating from back to front
		 * @param[in] arr			Array of characters to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character, NPos if nothing was found
		 */
		sizeT RFindAnyNotOf(const AnsiChar * arr, sizeT count, sizeT pos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a character, which is not in a given array, in the string, iterating from back to front
		 * @param[in] arr			Array of characters to find
		 * @param[in] count			Array size
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character, NPos if nothing was found
		 */
		sizeT RFindAnyNotOf(const WideChar * arr, sizeT count, sizeT pos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a character, which is not in a given c-string, in the string, iterating from back to front
		 * @param[in] str			C-string with characters to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character, NPos if nothing was found
		 */
		sizeT RFindAnyNotOf(const AnsiChar * str, sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a character, which is not in a given c-string, in the string, iterating from back to front
		 * @param[in] str			C-string with characters to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of any character, NPos if nothing was found
		 */
		sizeT RFindAnyNotOf(const WideChar * str, sizeT pos = NPos, b8 caseSensitive = true) const;
		/**
		 * Find the first occurance of a character, which is not in a given string, in the string, iterating from back to front
		 * @tparam C				Character type
		 * @tparam G				Growth polict
		 * @param[in] str			String with characters to find
		 * @param[in] pos			Position to start searching from
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Index to the first occurance of character of a string string, NPos if nothing was found
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		sizeT RFindAnyNotOf(const TString<C, G>& str, sizeT pos = NPos, b8 caseSensitive = true) const;

		////////////////////////////////////////////////////////////////////////////////
		//	Compare
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Compare the string with a character
		 * @param[in] c				Character to compare with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Relationship between the string and a character\n
		*							0 : string and character match
		*							-1: string comes lexicograpically before the character or the string is empty\n
		*							1 : string comes lexicograpically after the character or 1st character matches, but string is longer
		 */
		i8 Compare(AnsiChar c, b8 caseSensitive = true) const;
		/**
		 * Compare the string with a character
		 * @param[in] c				Character to compare with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Relationship between the string and a character\n
		*							0 : string and character match
		*							-1: string comes lexicograpically before the character or the string is empty\n
		*							1 : string comes lexicograpically after the character or 1st character matches, but string is longer
		 */
		i8 Compare(WideChar c, b8 caseSensitive = true) const;
		/**
		 * Compare a character in the string with another character
		 * @param[in] pos			Index of character to compare to
		 * @param[in] c				Character to compare with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					True if characters match, false otherwise
		 */
		b8 Compare(sizeT pos, AnsiChar c, b8 caseSensitive = true) const;
		/**
		 * Compare a character in the string with another character
		 * @param[in] pos			Index of character to compare to
		 * @param[in] c				Character to compare with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					True if characters match, false otherwise
		 */
		b8 Compare(sizeT pos, WideChar c, b8 caseSensitive = true) const;
		/**
		 * Compare the string with a c-string
		 * @param[in] str			C-string to compare with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Relationship between the string and a character\n
		*							0 : strings match
		*							-1: string comes lexicograpically before the string or strings partially matches, but string is shorter than the given string\n
		*							1 : string comes lexicograpically after the string or strings partially matches, but string is longer than the given string
		 */
		i8 Compare(const AnsiChar * str, b8 caseSensitive = true) const;
		/**
		 * Compare the string with a c-string
		 * @param[in] str			C-string to compare with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Relationship between the string and a character\n
		*							0 : strings match
		*							-1: string comes lexicograpically before the string or strings partially matches, but string is shorter than the given string\n
		*							1 : string comes lexicograpically after the string or strings partially matches, but string is longer than the given string
		 */
		i8 Compare(const WideChar * str, b8 caseSensitive = true) const;
		/**
		 * Compare a part of the string with a c-string
		 * @param[in] pos			Position in the string to start comparing from
		 * @param[in] len			Amount of characters in the string to compare with
		 * @param[in] str			C-string to compare with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Relationship between 2 strings\n
		*							0 : strings match
		*							-1: string comes lexicograpically before the string or strings partially matches, but string is shorter than the given string\n
		*							1 : string comes lexicograpically after the string or strings partially matches, but string is longer than the given string
		 */
		i8 Compare(sizeT pos, sizeT len, const AnsiChar * str, b8 caseSensitive = true) const;
		/**
		 * Compare a part of the string with a c-string
		 * @param[in] pos			Position in the string to start comparing from
		 * @param[in] len			Amount of characters in the string to compare with
		 * @param[in] str			C-string to compare with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Relationship between 2 strings\n
		*							0 : strings match
		*							-1: string comes lexicograpically before the string or strings partially matches, but string is shorter than the given string\n
		*							1 : string comes lexicograpically after the string or strings partially matches, but string is longer than the given string
		 */
		i8 Compare(sizeT pos, sizeT len, const WideChar * str, b8 caseSensitive = true) const;
		/**
		 * Compare a part of the string with a part of a c-string
		 * @param[in] pos			Position in the string to start comparing from
		 * @param[in] len			Amount of characters in the string to compare with
		 * @param[in] str			C-string to compare with
		 * @param[in] count			Amount of characters of the given string to compare to
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Relationship between 2 strings\n
		*							0 : strings match
		*							-1: string comes lexicograpically before the string or strings partially matches, but string is shorter than the given string\n
		*							1 : string comes lexicograpically after the string or strings partially matches, but string is longer than the given string
		 */
		i8 Compare(sizeT pos, sizeT len, const AnsiChar * str, sizeT count, b8 caseSensitive = true) const;
		/**
		 * Compare a part of the string with a part of a c-string
		 * @param[in] pos			Position in the string to start comparing from
		 * @param[in] len			Amount of characters in the string to compare with
		 * @param[in] str			C-string to compare with
		 * @param[in] count			Amount of characters of the given string to compare to
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Relationship between 2 strings\n
		*							0 : strings match
		*							-1: string comes lexicograpically before the string or strings partially matches, but string is shorter than the given string\n
		*							1 : string comes lexicograpically after the string or strings partially matches, but string is longer than the given string
		 */
		i8 Compare(sizeT pos, sizeT len, const WideChar * str, sizeT count, b8 caseSensitive = true) const;
		/**
		 * Compare a part of the string with a part of a c-string
		 * @param[in] pos			Position in the string to start comparing from
		 * @param[in] len			Amount of characters in the string to compare with
		 * @param[in] str			C-string to compare with
		 * @param[in] subPos		Position in the given string to start comparing from
		 * @param[in] subLen		Amount of character in a given string to compare to
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Relationship between 2 strings\n
		*							0 : strings match
		*							-1: string comes lexicograpically before the string or strings partially matches, but string is shorter than the given string\n
		*							1 : string comes lexicograpically after the string or strings partially matches, but string is longer than the given string
		 */
		i8 Compare(sizeT pos, sizeT len, const AnsiChar * str, sizeT subPos, sizeT subLen, b8 caseSensitive = true) const;
		/**
		 * Compare a part of the string with a part of a c-string
		 * @param[in] pos			Position in the string to start comparing from
		 * @param[in] len			Amount of characters in the string to compare with
		 * @param[in] str			C-string to compare with
		 * @param[in] subPos		Position in the given string to start comparing from
		 * @param[in] subLen		Amount of character in a given string to compare to
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Relationship between 2 strings\n
		*							0 : strings match
		*							-1: string comes lexicograpically before the string or strings partially matches, but string is shorter than the given string\n
		*							1 : string comes lexicograpically after the string or strings partially matches, but string is longer than the given string
		 */
		i8 Compare(sizeT pos, sizeT len, const WideChar * str, sizeT subPos, sizeT subLen, b8 caseSensitive = true) const;

		/**
		 * Compare the string with a c-string
		 * @tparam C				Chararacter type
		 * @tparam G				Growth policy
		 * @param[in] str			C-string to compare with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Relationship between 2 strings\n
		*							0 : strings match
		*							-1: string comes lexicograpically before the string or strings partially matches, but string is shorter than the given string\n
		*							1 : string comes lexicograpically after the string or strings partially matches, but string is longer than the given string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		i8 Compare(const TString<C, G>& str, b8 caseSensitive = true) const;
		/**
		 * Compare a part of the string with a c-string
		 * @tparam C				Chararacter type
		 * @tparam G				Growth policy
		 * @param[in] pos			Position in the string to start comparing from
		 * @param[in] len			Amount of characters in the string to compare with
		 * @param[in] str			C-string to compare with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Relationship between 2 strings\n
		*							0 : strings match
		*							-1: string comes lexicograpically before the string or strings partially matches, but string is shorter than the given string\n
		*							1 : string comes lexicograpically after the string or strings partially matches, but string is longer than the given string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		i8 Compare(sizeT pos, sizeT len, const TString<C, G>& str, b8 caseSensitive = true) const;
		/**
		 * Compare a part of the string with a part of a c-string
		 * @tparam C				Chararacter type
		 * @tparam G				Growth policy
		 * @param[in] pos			Position in the string to start comparing from
		 * @param[in] len			Amount of characters in the string to compare with
		 * @param[in] str			C-string to compare with
		 * @param[in] count			Amount of characters of the given string to compare to
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Relationship between 2 strings\n
		*							0 : strings match
		*							-1: string comes lexicograpically before the string or strings partially matches, but string is shorter than the given string\n
		*							1 : string comes lexicograpically after the string or strings partially matches, but string is longer than the given string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		i8 Compare(sizeT pos, sizeT len, const TString<C, G>& str, sizeT count, b8 caseSensitive = true) const;
		/**
		 * Compare a part of the string with a part of a c-string
		 * @tparam C				Chararacter type
		 * @tparam G				Growth policy
		 * @param[in] pos			Position in the string to start comparing from
		 * @param[in] len			Amount of characters in the string to compare with
		 * @param[in] str			C-string to compare with
		 * @param[in] subPos		Position in the given string to start comparing from
		 * @param[in] subLen		Amount of character in a given string to compare to
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					Relationship between 2 strings\n
		*							0 : strings match
		*							-1: string comes lexicograpically before the string or strings partially matches, but string is shorter than the given string\n
		*							1 : string comes lexicograpically after the string or strings partially matches, but string is longer than the given string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		i8 Compare(sizeT pos, sizeT len, const TString<C, G>& str, sizeT subPos, sizeT subLen, b8 caseSensitive = true) const;

		////////////////////////////////////////////////////////////////////////////////
		//	StartWith
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Check if the string starts with a given character
		 * @param[in] c				Character to check with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					True if the string starts with a given character
		 */
		b8 StartsWith(AnsiChar c, b8 caseSensitive = true) const;
		/**
		 * Check if the string starts with a given character
		 * @param[in] c				Character to check with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					True if the string starts with a given character
		 */
		b8 StartsWith(WideChar c, b8 caseSensitive = true) const;
		/**
		 * Check if the string starts with a given c-string
		 * @param[in] str	C-string to check with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return			True if the string starts with a given c-string
		 */
		b8 StartsWith(const AnsiChar * str, b8 caseSensitive = true) const;
		/**
		 * Check if the string starts with a given c-string
		 * @param[in] str			C-string to check with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					True if the string starts with a given c-string
		 */
		b8 StartsWith(const WideChar * str, b8 caseSensitive = true) const;
		/**
		 * Check if the string starts with a given c-string
		 * @param[in] str			C-string to check with
		 * @param[in] len			Amount of characters to compare
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					True if the string starts with a given c-string
		 */
		b8 StartsWith(const AnsiChar * str, sizeT len, b8 caseSensitive = true) const;
		/**
		 * Check if the string starts with a given c-string
		 * @param[in] str			C-string to check with
		 * @param[in] len			Amount of characters to compare
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					True if the string starts with a given c-string
		 */
		b8 StartsWith(const WideChar * str, sizeT len, b8 caseSensitive = true) const;
		/**
		 * Check if the string starts with a given string
		 * @param[in] str			C-string to check with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					True if the string starts with a given string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		b8 StartsWith(const TString<C, G> str, b8 caseSensitive = true) const;
		/**
		 * Check if the string starts with a given string
		 * @param[in] str			C-string to check with
		 * @param[in] len			Amount of characters to compare
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					True if the string starts with a given string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		b8 StartsWith(const TString<C, G> str, sizeT len, b8 caseSensitive = true) const;

		////////////////////////////////////////////////////////////////////////////////
		//	EndsWith
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Check if the string ends with a given character
		 * @param[in] c				Character to check with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					True if the string starts with a given character
		 */
		b8 EndsWith(AnsiChar c, b8 caseSensitive = true) const;
		/**
		 * Check if the string ends with a given character
		 * @param[in] c				Character to check with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					True if the string starts with a given character
		 */
		b8 EndsWith(WideChar c, b8 caseSensitive = true) const;
		/**
		 * Check if the string ends with a given c-string
		 * @param[in] str			C-string to check with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					True if the string starts with a given c-string
		 */
		b8 EndsWith(const AnsiChar * str, b8 caseSensitive = true) const;
		/**
		 * Check if the string ends with a given c-string
		 * @param[in] str			C-string to check with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					True if the string starts with a given c-string
		 */
		b8 EndsWith(const WideChar * str, b8 caseSensitive = true) const;
		/**
		 * Check if the string ends with a given c-string
		 * @param[in] str			C-string to check with
		 * @param[in] len			Amount of characters to compare
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					True if the string starts with a given c-string
		 */
		b8 EndsWith(const AnsiChar * str, sizeT len, b8 caseSensitive = true) const;
		/**
		 * Check if the string ends with a given c-string
		 * @param[in] str			C-string to check with
		 * @param[in] len			Amount of characters to compare
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					True if the string starts with a given c-string
		 */
		b8 EndsWith(const WideChar * str, sizeT len, b8 caseSensitive = true) const;
		/**
		 * Check if the string ends with a given string
		 * @param[in] str			C-string to check with
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					True if the string starts with a given string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		b8 EndsWith(const TString<C, G> str, b8 caseSensitive = true) const;
		/**
		 * Check if the string ends with a given string
		 * @param[in] str			C-string to check with
		 * @param[in] len			Amount of characters to compare
		 * @param[in] caseSensitive	If the character's case needs to be honored or ignored
		 * @return					True if the string starts with a given string
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		b8 EndsWith(const TString<C, G> str, sizeT len, b8 caseSensitive = true) const;

		////////////////////////////////////////////////////////////////////////////////
		//	SubString
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Create a substring from the string
		 * @param[in] pos	Start position of substring
		 * @param[in] len	Length of substring, if NPos, use total length to end
		 * @return			Substring
		 */
		TString SubString(sizeT pos = 0, sizeT len = NPos) const;

		////////////////////////////////////////////////////////////////////////////////
		//	Pad
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Pad the string with a spaces
		 * @param[in] size	Amount of spaces to pad with
		 * @return			Reference to the string
		 */
		TString& Pad(sizeT size);
		/**
		 * Pad the string with a character
		 * @param[in] c		Character to pad with
		 * @param[in] size	Amount of characters to pad with
		 * @return			Reference to the string
		 */
		TString& Pad(AnsiChar c, sizeT size);
		/**
		 * Pad the string with a character
		 * @param[in] c		Character to pad with
		 * @param[in] size	Amount of characters to pad with
		 * @return			Reference to the string
		 */
		TString& Pad(WideChar c, sizeT size);
		/**
		 * Pad the string with spaces
		 * @param[in] left	Amount of spaces to pad with on the left
		 * @param[in] right	Amount of spaces to pad with on the right
		 * @return			Reference to the string
		 */
		TString& Pad(sizeT left, sizeT right);
		/**
		 * Pad the string with spaces
		 * @param[in] c		Character to pad with
		 * @param[in] left	Amount of characters to pad with on the left
		 * @param[in] right	Amount of characters to pad with on the right
		 * @return			Reference to the string
		 */
		TString& Pad(AnsiChar c, sizeT left, sizeT right);
		/**
		 * Pad the string with spaces
		 * @param[in] c		Character to pad with
		 * @param[in] left	Amount of characters to pad with on the left
		 * @param[in] right	Amount of characters to pad with on the right
		 * @return			Reference to the string
		 */
		TString& Pad(WideChar c, sizeT left, sizeT right);
		/**
		 * Pad the left of the string with a spaces
		 * @param[in] size	Amount of spaces to pad with
		 * @return			Reference to the string
		 */
		TString& PadLeft(sizeT size);
		/**
		 * Pad the left of the string with a character
		 * @param[in] c		Character to pad with
		 * @param[in] size	Amount of characters to pad with
		 * @return			Reference to the string
		 */
		TString& PadLeft(AnsiChar c, sizeT size);
		/**
		 * Pad the left of the string with a character
		 * @param[in] c		Character to pad with
		 * @param[in] size	Amount of characters to pad with
		 * @return			Reference to the string
		 */
		TString& PadLeft(WideChar c, sizeT size);
		/**
		 * Pad the right of the string with a spaces
		 * @param[in] size	Amount of spaces to pad with
		 * @return			Reference to the string
		 */
		TString& PadRight(sizeT size);
		/**
		 * Pad the right of the string with a character
		 * @param[in] c		Character to pad with
		 * @param[in] size	Amount of characters to pad with
		 * @return			Reference to the string
		 */
		TString& PadRight(AnsiChar c, sizeT size);
		/**
		 * Pad the right of the string with a character
		 * @param[in] c		Character to pad with
		 * @param[in] size	Amount of characters to pad with
		 * @return			Reference to the string
		 */
		TString& PadRight(WideChar c, sizeT size);

		////////////////////////////////////////////////////////////////////////////////
		//	Trim
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Trim spaces on both ends of the string
		 * @return	Reference to the string
		 */
		TString& Trim();
		/**
		 * Trim a character on both ends of the string
		 * @return	Reference to the string
		 */
		TString& Trim(AnsiChar c);
		/**
		 * Trim a character on both ends of the string
		 * @return	Reference to the string
		 */
		TString& Trim(WideChar c);
		/**
		 * Trim spaces on the left of the string
		 * @return	Reference to the string
		 */
		TString& TrimLeft();
		/**
		 * Trim a character on the left of the string
		 * @return	Reference to the string
		 */
		TString& TrimLeft(AnsiChar c);
		/**
		 * Trim a character on the left of the string
		 * @return	Reference to the string
		 */
		TString& TrimLeft(WideChar c);
		/**
		 * Trim spaces on the right of the string
		 * @return	Reference to the string
		 */
		TString& TrimRight();
		/**
		 * Trim a character on the right of the string
		 * @return	Reference to the string
		 */
		TString& TrimRight(AnsiChar c);
		/**
		 * Trim a character on the right of the string
		 * @return	Reference to the string
		 */
		TString& TrimRight(WideChar c);

		////////////////////////////////////////////////////////////////////////////////
		//	SplitChar
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Split the string at every occurance of a character
		 * @tparam G						DynArray growth policy
		 * @param[in] c						Character to split at
		 * @param[in] removeEmptyEntries	If empty entries need to be removed from the resulting DynArray
		 * @param[in] pAlloc				DynArray allocator, if nullptr, use string's allocator
		 * @return							DynArray with resulting substrings
		 */
		template<Container::GrowthPolicyFunc G = GrowthPolicy>
		DynArray<TString, G> SplitChar(AnsiChar c, b8 removeEmptyEntries = false, Memory::IAllocator* pAlloc = nullptr) const;
		/**
		 * Split the string at every occurance of a character
		 * @tparam G						DynArray growth policy
		 * @param[in] c						Character to split at
		 * @param[in] removeEmptyEntries	If empty entries need to be removed from the resulting DynArray
		 * @param[in] pAlloc				DynArray allocator, if nullptr, use string's allocator
		 * @return							DynArray with resulting substrings
		 */
		template<Container::GrowthPolicyFunc G = GrowthPolicy>
		DynArray<TString, G> SplitChar(WideChar c, b8 removeEmptyEntries = false, Memory::IAllocator* pAlloc = nullptr) const;
		/**
		 * Split the string at every occurance of a character
		 * @tparam G						DynArray growth policy
		 * @param[in] arr					Array of characters to split at
		 * @param[in] count					Array size
		 * @param[in] removeEmptyEntries	If empty entries need to be removed from the resulting DynArray
		 * @param[in] pAlloc				DynArray allocator, if nullptr, use string's allocator
		 * @return							DynArray with resulting substrings
		 */
		template<Container::GrowthPolicyFunc G = GrowthPolicy>
		DynArray<TString, G> SplitChar(const AnsiChar * arr, sizeT count, b8 removeEmptyEntries = false, Memory::IAllocator* pAlloc = nullptr) const;
		/**
		 * Split the string at every occurance of a character
		 * @tparam G						DynArray growth policy
		 * @param[in] arr					Array of characters to split at
		 * @param[in] count					Array size
		 * @param[in] removeEmptyEntries	If empty entries need to be removed from the resulting DynArray
		 * @param[in] pAlloc				DynArray allocator, if nullptr, use string's allocator
		 * @return							DynArray with resulting substrings
		 */
		template<Container::GrowthPolicyFunc G = GrowthPolicy>
		DynArray<TString, G> SplitChar(const WideChar * arr, sizeT count, b8 removeEmptyEntries = false, Memory::IAllocator* pAlloc = nullptr) const;
		/**
		 * Split the string at every occurance of a character
		 * @tparam G						DynArray growth policy
		 * @tparam N						Array size
		 * @param[in] arr					Array of characters to split at
		 * @param[in] removeEmptyEntries	If empty entries need to be removed from the resulting DynArray
		 * @param[in] pAlloc				DynArray allocator, if nullptr, use string's allocator
		 * @return							DynArray with resulting substrings
		 */
		template<Container::GrowthPolicyFunc G = GrowthPolicy, sizeT N>
		DynArray<TString, G> SplitChar(const AnsiChar(&arr)[N], b8 removeEmptyEntries = false, Memory::IAllocator* pAlloc = nullptr) const;
		/**
		 * Split the string at every occurance of a character
		 * @tparam G						DynArray growth policy
		 * @tparam N						Array size
		 * @param[in] arr					Array of characters to split at
		 * @param[in] removeEmptyEntries	If empty entries need to be removed from the resulting DynArray
		 * @param[in] pAlloc				DynArray allocator, if nullptr, use string's allocator
		 * @return							DynArray with resulting substrings
		 */
		template<Container::GrowthPolicyFunc G = GrowthPolicy, sizeT N>
		DynArray<TString, G> SplitChar(const WideChar(&arr)[N], b8 removeEmptyEntries = false, Memory::IAllocator* pAlloc = nullptr) const;
		/**
		 * Split the string at every occurance of a character
		 * @tparam G1						DynArray growth policy
		 * @param[in] str					Strign wtih characters to split at
		 * @param[in] removeEmptyEntries	If empty entries need to be removed from the resulting DynArray
		 * @param[in] pAlloc				DynArray allocator, if nullptr, use string's allocator
		 * @return							DynArray with resulting substrings
		 */
		template<typename C0, Container::GrowthPolicyFunc G0, Container::GrowthPolicyFunc G1 = GrowthPolicy>
		DynArray<TString, G1> SplitChar(const TString<C0, G0>& str, b8 removeEmptyEntries = false, Memory::IAllocator* pAlloc = nullptr) const;

		////////////////////////////////////////////////////////////////////////////////
		//	SplitStr
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Split the string at every occurance of a c-string
		 * @tparam G						DynArray growth policy
		 * @param[in] str					C-string to split at
		 * @param[in] removeEmptyEntries	If empty entries need to be removed from the resulting DynArray
		 * @param[in] pAlloc				DynArray allocator, if nullptr, use string's allocator
		 * @return							DynArray with resulting substrings
		 */
		template<Container::GrowthPolicyFunc G = GrowthPolicy>
		DynArray<TString, G> SplitStr(const AnsiChar * str, b8 removeEmptyEntries = false, Memory::IAllocator* pAlloc = nullptr) const;
		/**
		 * Split the string at every occurance of a c-string
		 * @tparam G						DynArray growth policy
		 * @param[in] str					C-string to split at
		 * @param[in] removeEmptyEntries	If empty entries need to be removed from the resulting DynArray
		 * @param[in] pAlloc				DynArray allocator, if nullptr, use string's allocator
		 * @return							DynArray with resulting substrings
		 */
		template<Container::GrowthPolicyFunc G = GrowthPolicy>
		DynArray<TString, G> SplitStr(const WideChar * str, b8 removeEmptyEntries = false, Memory::IAllocator* pAlloc = nullptr) const;
		/**
		 * Split the string at every occurance of a c-string
		 * @tparam G						DynArray growth policy
		 * @param[in] arr					Array of c-strings to split at
		 * @param[in] count					Array size
		 * @param[in] removeEmptyEntries	If empty entries need to be removed from the resulting DynArray
		 * @param[in] pAlloc				DynArray allocator, if nullptr, use string's allocator
		 * @return							DynArray with resulting substrings
		 */
		template<Container::GrowthPolicyFunc G = GrowthPolicy>
		DynArray<TString, G> SplitStr(const AnsiChar* * arr, sizeT count, b8 removeEmptyEntries = false, Memory::IAllocator* pAlloc = nullptr) const;
		/**
		 * Split the string at every occurance of a c-string
		 * @tparam G						DynArray growth policy
		 * @param[in] arr					Array of c-strings to split at
		 * @param[in] count					Array size
		 * @param[in] removeEmptyEntries	If empty entries need to be removed from the resulting DynArray
		 * @param[in] pAlloc				DynArray allocator, if nullptr, use string's allocator
		 * @return							DynArray with resulting substrings
		 */
		template<Container::GrowthPolicyFunc G = GrowthPolicy>
		DynArray<TString, G> SplitStr(const WideChar* * arr, sizeT count, b8 removeEmptyEntries = false, Memory::IAllocator* pAlloc = nullptr) const;
		/**
		 * Split the string at every occurance of a c-string
		 * @tparam G						DynArray growth policy
		 * @tparam N						Array suze
		 * @param[in] arr					Array of c-strings to split at
		 * @param[in] removeEmptyEntries	If empty entries need to be removed from the resulting DynArray
		 * @param[in] pAlloc				DynArray allocator, if nullptr, use string's allocator
		 * @return							DynArray with resulting substrings
		 */
		template<Container::GrowthPolicyFunc G = GrowthPolicy, sizeT N>
		DynArray<TString, G> SplitStr(const AnsiChar*(&arr)[N], b8 removeEmptyEntries = false, Memory::IAllocator* pAlloc = nullptr) const;
		/**
		 * Split the string at every occurance of a c-string
		 * @tparam G						DynArray growth policy
		 * @tparam N						Array suze
		 * @param[in] arr					Array of c-strings to split at
		 * @param[in] removeEmptyEntries	If empty entries need to be removed from the resulting DynArray
		 * @param[in] pAlloc				DynArray allocator, if nullptr, use string's allocator
		 * @return							DynArray with resulting substrings
		 */
		template<Container::GrowthPolicyFunc G = GrowthPolicy, sizeT N>
		DynArray<TString, G> SplitStr(const WideChar*(&arr)[N], b8 removeEmptyEntries = false, Memory::IAllocator* pAlloc = nullptr) const;
		/**
		 * Split the string at every occurance of a c-string
		 * @tparam C						Character type
		 * @tparam G0						String growth policy
		 * @tparam G1						DynArray growth policy
		 * @param[in] str					C-string to split at
		 * @param[in] removeEmptyEntries	If empty entries need to be removed from the resulting DynArray
		 * @param[in] pAlloc				DynArray allocator, if nullptr, use string's allocator
		 * @return							DynArray with resulting substrings
		 */
		template<typename C, Container::GrowthPolicyFunc G0, Container::GrowthPolicyFunc G1 = GrowthPolicy>
		DynArray<TString, G1> SplitStr(const TString<C, G0>& str, b8 removeEmptyEntries = false, Memory::IAllocator* pAlloc = nullptr) const;
		/**
		 * Split the string at every occurance of a c-string
		 * @tparam C						Character type
		 * @tparam G0						String growth policy
		 * @tparam G1						DynArray growth policy
		 * @param[in] arr					Array of c-strings to split at
		 * @param[in] count					Array size
		 * @param[in] removeEmptyEntries	If empty entries need to be removed from the resulting DynArray
		 * @param[in] pAlloc				DynArray allocator, if nullptr, use string's allocator
		 * @return							DynArray with resulting substrings
		 */
		template<typename C, Container::GrowthPolicyFunc G0, Container::GrowthPolicyFunc G1 = GrowthPolicy>
		DynArray<TString, G1> SplitStr(const TString<C, G0> * arr, sizeT count, b8 removeEmptyEntries = false, Memory::IAllocator* pAlloc = nullptr) const;
		/**
		 * Split the string at every occurance of a c-string
		 * @tparam C						Character type
		 * @tparam G0						String growth policy
		 * @tparam G1						DynArray growth policy
		 * @tparam N						Array suze
		 * @param[in] arr					Array of c-strings to split at
		 * @param[in] removeEmptyEntries	If empty entries need to be removed from the resulting DynArray
		 * @param[in] pAlloc				DynArray allocator, if nullptr, use string's allocator
		 * @return							DynArray with resulting substrings
		 */
		template<typename C, Container::GrowthPolicyFunc G0, Container::GrowthPolicyFunc G1 = GrowthPolicy, sizeT N>
		DynArray<TString, G1> SplitStr(const TString<C, G0>*(&arr)[N], b8 removeEmptyEntries = false, Memory::IAllocator* pAlloc = nullptr) const;

		////////////////////////////////////////////////////////////////////////////////
		//	Case convertions
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Convert the string to lower case
		 * @return	Reference to the string
		 */
		TString& ToLower();
		/**
		 * Convert the string to upper case
		 * @return	Reference to the string
		 */
		TString& ToUpper();

		////////////////////////////////////////////////////////////////////////////////
		//	Format
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Format a string
		 * @tparam C			Character type
		 * @tparam G			Growth policy
		 * @tparam Args			Argument types
		 * @param[in] format	String format
		 * @param[in] args		Arguments
		 */
		template<typename C, Container::GrowthPolicyFunc G, typename... Args>
		TString& FormatF(const TString<C, G> format, Args... args);

		////////////////////////////////////////////////////////////////////////////////
		//	Size modifications
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Resize the string
		 * @param[in] size	New size
		 * @note			If 'size' is larger than the current length, nothing will happen.\n
		*					To set the length to a larger value, use Resize(size, '\0')
		 */
		void Resize(sizeT size);
		/**
		 * Resize the string
		 * @param[in] size	New size
		 * @param[in] c		Character to add if size is bigger than the current size
		 * @note			When the 'c' is '\0', space will be reserved and the length assigned,\n
		*					but no characters will be set
		 */
		void Resize(sizeT size, AnsiChar c);
		/**
		 * Resize the string
		 * @param[in] size	New size
		 * @param[in] c		Character to add if size is bigger than the current size
		 * @note			When the 'c' is '\0', space will be reserved and the length assigned,\n
		*					but no characters will be set
		 */
		void Resize(sizeT size, WideChar c);
		/**
		 * Reserve space in the string
		 * @param[in] size	Min size to reserve
		 */
		void Reserve(sizeT size);
		/**
		 * Reduce the capacity to fit its size
		 */
		void ShrinkToFit();

		/**
		 * Clear the string
		 */
		void Clear(b8 resize = true);

		////////////////////////////////////////////////////////////////////////////////
		//	Other
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Swap the content of 2 strings
		 * @param[in] str	String to swap content with
		 */
		template<Container::GrowthPolicyFunc G>
		void Swap(TString<CharT, G>& str);
		/**
		 * Check if the string is filled with whitespace or empty
		 * @return	If the string is filled with whitespace or empty
		 */
		b8 IsWhitespaceOrEmpty() const;
		/**
		 * Check if the string is empty
		 * @return	If the string is empty
		 */
		b8 IsEmpty() const;
		/**
		 * Get the size of the string
		 * @return	Size of the string
		 */
		sizeT Length() const;
		/**
		 * Get the size of the string
		 * @return	Capacity of the string
		 */
		sizeT Capacity() const;
		/**
		 * Get a string as a c-string
		 * @return	String as a c-string
		 */
		CharT * CStr();
		/**
		 * Get a string as a c-string
		 * @return	String as a c-string
		 */
		const CharT * CStr() const;
		/**
		 * Get a pointer to the allocator
		 * @return	Pointer to the allocator
		 */
		Memory::IAllocator * GetAllocator();
		/**
		 * Get a pointer to the allocator
		 * @return	Pointer to the allocator
		 */
		Memory::IAllocator * GetAllocator() const;

		////////////////////////////////////////////////////////////////////////////////
		//	Iterators
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Return an iterator to the front/begin of the string
		 * @return	Iterator to the front/begin of the string
		 */
		CharT * Front();
		/**
		 * Return an iterator to the front/begin of the string
		 * @return	Iterator to the front/begin of the string
		 */
		const CharT * Front() const;
		/**
		 * Return an iterator to the last element of the string
		 * @return	Iterator to the last element of the string
		 */
		CharT * Last();
		/**
		 * Return an iterator to the last element of the string
		 * @return	InputIterator to the last element of the string
		 */
		const CharT * Last() const;
		/**
		 * Return an iterator to the back/end of the string
		 * @return	Iterator to the back/end of the string
		 */
		CharT * Back();
		/**
		 * Return an iterator to the back/end of the string
		 * @return	Iterator to the back/end of the string
		 */
		const CharT * Back() const;

		/**
		 * Return an iterator to the begin of the string
		 * @note	Function meant to be used for range-based for-loops, use Front instead
		 * @return	Iterator to the begin of the string
		 */
		CharT * begin();
		/**
		 * Return an iterator to the begin of the string
		 * @note	Function meant to be used for range-based for-loops, use Front instead
		 * @return	Iterator to the begin of the string
		 */
		const CharT * begin() const;
		/**
		 * Return an iterator to the end of the string
		 * @note	Function meant to be used for range-based for-loops, use Back instead
		 * @return	Iterator to the end of the string
		 */
		CharT * end();
		/**
		 * Return an iterator to the end of the string
		 * @note	Function meant to be used for range-based for-loops, use Back instead
		 * @return	Iterator to the end of the string
		 */
		const CharT * end() const;


	private:
		template<typename C00, Container::GrowthPolicyFunc G0>
		friend class TString;

		////////////////////////////////////////////////////////////////////////////////
		//	Helper functions
		////////////////////////////////////////////////////////////////////////////////

		void CopyToData(sizeT pos, AnsiChar c, sizeT count);
		void CopyToData(sizeT pos, WideChar c, sizeT count);
		void CopyToData(sizeT pos, const AnsiChar * str, sizeT size);
		void CopyToData(sizeT pos, const WideChar * str, sizeT size);
		template<typename InputIterator>
		void CopyToData(sizeT pos, const InputIterator& itFirst, const InputIterator& itLast);
		template<sizeT N>
		void CopyToData(sizeT pos, const AnsiChar(&str)[N]);
		template<sizeT N>
		void CopyToData(sizeT pos, const WideChar(&str)[N]);

		void MoveData(sizeT from, sizeT to);

		// Variables
		CharT * m_Data;					/**< Internal string */
		sizeT m_Length;					/**< Size */
		sizeT m_Capacity;				/**< Capacity */
		Memory::IAllocator * m_pAlloc;	/**< Allocator */
	};

	////////////////////////////////////////////////////////////////////////////////
	//	External functions
	////////////////////////////////////////////////////////////////////////////////
	template<typename C, Container::GrowthPolicyFunc G>
	TString<C, G> operator+(AnsiChar c, const TString<C, G>& str);
	template<typename C, Container::GrowthPolicyFunc G>
	TString<C, G> operator+(WideChar c, const TString<C, G>& str);
	template<typename C, Container::GrowthPolicyFunc G>
	TString<C, G> operator+(const AnsiChar * str0, const TString<C, G>& str1);
	template<typename C, Container::GrowthPolicyFunc G>
	TString<C, G> operator+(const WideChar * str0, const TString<C, G>& str1);
	template<typename C, Container::GrowthPolicyFunc G>
	TString<C, G> operator+(std::initializer_list<AnsiChar> il, const TString<C, G>& str);
	template<typename C, Container::GrowthPolicyFunc G>
	TString<C, G> operator+(std::initializer_list<WideChar> il, const TString<C, G>& str);

	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator==(AnsiChar c, const TString<C, G>& str);
	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator==(WideChar c, const TString<C, G>& str);
	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator==(const AnsiChar * str0, const TString<C, G>& str1);
	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator==(const WideChar * str0, const TString<C, G>& str1);

	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator!=(AnsiChar c, const TString<C, G>& str);
	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator!=(WideChar c, const TString<C, G>& str);
	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator!=(const AnsiChar * str0, const TString<C, G>& str1);
	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator!=(const WideChar * str0, const TString<C, G>& str1);

	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator<(AnsiChar c, const TString<C, G>& str);
	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator<(WideChar c, const TString<C, G>& str);
	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator<(const AnsiChar * str0, const TString<C, G>& str1);
	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator<(const WideChar * str0, const TString<C, G>& str1);

	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator<=(AnsiChar c, const TString<C, G>& str);
	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator<=(WideChar c, const TString<C, G>& str);
	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator<=(const AnsiChar * str0, const TString<C, G>& str1);
	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator<=(const WideChar * str0, const TString<C, G>& str1);

	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator>(AnsiChar c, const TString<C, G>& str);
	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator>(WideChar c, const TString<C, G>& str);
	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator>(const AnsiChar * str0, const TString<C, G>& str1);
	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator>(const WideChar * str0, const TString<C, G>& str1);

	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator>=(AnsiChar c, const TString<C, G>& str);
	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator>=(WideChar c, const TString<C, G>& str);
	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator>=(const AnsiChar * str0, const TString<C, G>& str1);
	template<typename C, Container::GrowthPolicyFunc G>
	b8 operator>=(const WideChar * str0, const TString<C, G>& str1);

	template<typename C, Container::GrowthPolicyFunc G>
	std::ostream& operator<<(std::ostream& out, const TString<C, G>& str);
	template<typename C, Container::GrowthPolicyFunc G>
	std::wostream& operator<<(std::wostream& out, const TString<C, G>& str);

	/**
	 * Ansi string
	 */
	using AnsiString = TString<AnsiChar>;
	/**
	 * Wide string
	 */
	using WideString = TString<WideChar>;
	/**
	 * String with characters dependant on whether unicode is used or nor
	 */
	using String = WideString;
	
}

#include "Inline/TString.inl"

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename C, Hv::Container::GrowthPolicyFunc G), HV_TTYPE(Hv::TString, C, G))