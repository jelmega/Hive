// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// StringParsing.h: String parsing functions
#pragma once
#include "Core/CoreHeaders.h"
#include "TString.h"

namespace Hv {

	namespace Detail {
	
		/**
		 * Parse a string to a u64 (in base 10)
		 * @tparam C		Character type
		 * @tparam G		Growth policy
		 * @param[in] str	String to parse
		 * @param[in] pos	Offset in string (<= 22 ('-' + max len of value))
		 * @param[in] len	Lenght to parse (<= 20)
		 * @param[in] res	[OPTIONAL]	If the string could be parsed
		 * @return			Parsed value
		 */
		template<typename C, Container::GrowthPolicyFunc G>
		u64 ParseU64(const TString<C, G>& str, u8 pos, u8 len, b8* res = nullptr);

		/**
		* Parse a string to a i64 (in base 10)
		* @tparam C		Character type
		* @tparam G		Growth policy
		* @param[in] str	String to parse
		* @param[in] pos	Offset in string (<= 22 (max len of value))
		* @param[in] len	Lenght to parse (<= 20)
		* @param[in] res	[OPTIONAL]	If the string could be parsed
		* @return			Parsed value
		*/
		template<typename C, Container::GrowthPolicyFunc G>
		i64 ParseI64(const TString<C, G>& str, u8 pos, u8 len, b8* res = nullptr);

		/**
		* Parse a string to a u64 (in base 10)
		* @tparam C		Character type
		* @tparam G		Growth policy
		* @param[in] str	String to parse
		* @param[in] pos	Offset in string (<= 22 ('-' + max len of value + '.'))
		* @param[in] len	Lenght to parse (<= 20)
		* @param[in] res	[OPTIONAL]	If the string could be parsed
		* @return			Parsed value
		*/
		template<typename C, Container::GrowthPolicyFunc G>
		f64 ParseF64(const TString<C, G>& str, u8 pos, u8 len, b8* res = nullptr);
	}
	
	/**
	 * Parse a string to a u64 (in base 10)
	 * @tparam C		Character type
	 * @tparam G		Growth policy
	 * @param[in] str	String to parse
	 * @param[in] res	[OPTIONAL]	If the string could be parsed
	 * @return			Parsed value
	 */
	template<typename C, Container::GrowthPolicyFunc G>
	u64 ParseU64(const TString<C, G>& str, b8* res = nullptr);
	/**
	 * Parse a string to a u32 (in base 10)
	 * @tparam C		Character type
	 * @tparam G		Growth policy
	 * @param[in] str	String to parse
	 * @param[in] res	[OPTIONAL]	If the string could be parsed
	 * @return			Parsed value
	 */
	template<typename C, Container::GrowthPolicyFunc G>
	u32 ParseU32(const TString<C, G>& str, b8* res = nullptr);
	/**
	 * Parse a string to a u16 (in base 10)
	 * @tparam C		Character type
	 * @tparam G		Growth policy
	 * @param[in] str	String to parse
	 * @param[in] res	[OPTIONAL]	If the string could be parsed
	 * @return			Parsed value
	 */
	template<typename C, Container::GrowthPolicyFunc G>
	u16 ParseU16(const TString<C, G>& str, b8* res = nullptr);
	/**
	 * Parse a string to a u8 (in base 10)
	 * @tparam C		Character type
	 * @tparam G		Growth policy
	 * @param[in] str	String to parse
	 * @param[in] res	[OPTIONAL]	If the string could be parsed
	 * @return			Parsed value
	 */
	template<typename C, Container::GrowthPolicyFunc G>
	u8 ParseU8(const TString<C, G>& str, b8* res = nullptr);


	/**
	 * Parse a string to an i64 (in base 10)
	 * @tparam C		Character type
	 * @tparam G		Growth policy
	 * @param[in] str	String to parse
	 * @param[in] res	[OPTIONAL]	If the string could be parsed
	 * @return			Parsed value
	 */
	template<typename C, Container::GrowthPolicyFunc G>
	i64 ParseI64(const TString<C, G>& str, b8* res = nullptr);
	/**
	 * Parse a string to an i32 (in base 10)
	 * @tparam C		Character type
	 * @tparam G		Growth policy
	 * @param[in] str	String to parse
	 * @param[in] res	[OPTIONAL]	If the string could be parsed
	 * @return			Parsed value
	 */
	template<typename C, Container::GrowthPolicyFunc G>
	i32 ParseI32(const TString<C, G>& str, b8* res = nullptr);
	/**
	 * Parse a string to an i16 (in base 10)
	 * @tparam C		Character type
	 * @tparam G		Growth policy
	 * @param[in] str	String to parse
	 * @param[in] res	[OPTIONAL]	If the string could be parsed
	 * @return			Parsed value
	 */
	template<typename C, Container::GrowthPolicyFunc G>
	i16 ParseI16(const TString<C, G>& str, b8* res = nullptr);
	/**
	 * Parse a string to an i8 (in base 10)
	 * @tparam C		Character type
	 * @tparam G		Growth policy
	 * @param[in] str	String to parse
	 * @param[in] res	[OPTIONAL]	If the string could be parsed
	 * @return			Parsed value
	 */
	template<typename C, Container::GrowthPolicyFunc G>
	i8 ParseI8(const TString<C, G>& str, b8* res = nullptr);

	/**
	* Parse a string to an f64 (in base 10)
	* @tparam C		Character type
	* @tparam G		Growth policy
	* @param[in] str	String to parse
	* @param[in] res	[OPTIONAL]	If the string could be parsed
	* @return			Parsed value
	*/
	template<typename C, Container::GrowthPolicyFunc G>
	f64 ParseF64(const TString<C, G>& str, b8* res = nullptr);
	/**
	* Parse a string to an f64 (in base 10 and in scientific notation)
	* @tparam C		Character type
	* @tparam G		Growth policy
	* @param[in] str	String to parse
	* @param[in] res	[OPTIONAL]	If the string could be parsed
	* @return			Parsed value
	*/
	template<typename C, Container::GrowthPolicyFunc G>
	f64 ParseF64Sci(const TString<C, G>& str, b8* res = nullptr);
	/**
	* Parse a string to an i32 (in base 10)
	* @tparam C		Character type
	* @tparam G		Growth policy
	* @param[in] str	String to parse
	* @param[in] res	[OPTIONAL]	If the string could be parsed
	* @return			Parsed value
	*/
	template<typename C, Container::GrowthPolicyFunc G>
	f32 ParseF32(const TString<C, G>& str, b8* res = nullptr);
	
}

#include "Inline/StringParsing.inl"