// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// StringUtils.inl: String utilities
#pragma once
#include "String/StringUtils.h"

// std::is...
#include <ctype.h>

// std::isw...
#include <cwctype>

namespace Hv {

	template <typename CharT>
	HV_FORCE_INL CharT CharUtils<CharT>::Convert(AnsiChar c)
	{
		return c;
	}

	template <>
	HV_FORCE_INL WideChar CharUtils<WideChar>::Convert(AnsiChar c)
	{
		return WideChar(c);
	}

#pragma warning(push)
#pragma warning(disable: 4310)
	template <typename CharT>
	HV_FORCE_INL CharT CharUtils<CharT>::Convert(WideChar c)
	{
		// if character is out of ansi range, return 0xFF (small latin y with diaeresis/trema)
		return CharT(c < 0xFF ? c : 0xFF);
	}
#pragma warning(pop)

	template <typename CharT>
	HV_FORCE_INL AnsiChar CharUtils<CharT>::ToLower(AnsiChar c)
	{
		return (c >= 'A' && c <= 'Z') ? c | 0x20 /*+ 32*/ : c;
	}

	template <>
	HV_FORCE_INL WideChar CharUtils<WideChar>::ToLower(WideChar c)
	{
		return WideChar(tolower(c));
	}

	template <typename CharT>
	HV_FORCE_INL AnsiChar CharUtils<CharT>::ToUpper(AnsiChar c)
	{
		return (c >= 'a' && c <= 'z') ? c & ~0x20 /*- 32*/ : c;
	}

	template <>
	HV_FORCE_INL WideChar CharUtils<WideChar>::ToUpper(WideChar c)
	{
		return WideChar(toupper(c));
	}

	template <typename CharT>
	HV_FORCE_INL b8 CharUtils<CharT>::IsUpper(CharT c)
	{
		return b8(std::isupper(c));
	}

	template <>
	HV_FORCE_INL b8 CharUtils<WideChar>::IsUpper(WideChar c)
	{
		return b8(std::iswupper(c));
	}

	template <typename CharT>
	HV_FORCE_INL b8 CharUtils<CharT>::IsLower(CharT c)
	{
		return b8(std::islower(c));
	}

	template <>
	HV_FORCE_INL b8 CharUtils<WideChar>::IsLower(WideChar c)
	{
		return b8(std::iswlower(c));
	}

	template <typename CharT>
	HV_FORCE_INL b8 CharUtils<CharT>::IsAlpha(CharT c)
	{
		return b8(std::isalpha(c));
	}

	template <>
	HV_FORCE_INL b8 CharUtils<WideChar>::IsAlpha(WideChar c)
	{
		return b8(std::iswalpha(c));
	}

	template <typename CharT>
	HV_FORCE_INL b8 CharUtils<CharT>::IsGraph(CharT c)
	{
		return b8(std::isgraph(c));
	}

	template <>
	HV_FORCE_INL b8 CharUtils<WideChar>::IsGraph(WideChar c)
	{
		return b8(std::iswgraph(c));
	}

	template <typename CharT>
	HV_FORCE_INL b8 CharUtils<CharT>::IsPrint(CharT c)
	{
		return b8(std::isprint(c));
	}

	template <>
	HV_FORCE_INL b8 CharUtils<WideChar>::IsPrint(WideChar c)
	{
		return b8(std::iswprint(c));
	}

	template <typename CharT>
	HV_FORCE_INL b8 CharUtils<CharT>::IsPunct(CharT c)
	{
		return b8(std::ispunct(c));
	}

	template <>
	HV_FORCE_INL b8 CharUtils<WideChar>::IsPunct(WideChar c)
	{
		return b8(std::iswpunct(c));
	}

	template <typename CharT>
	HV_FORCE_INL b8 CharUtils<CharT>::IsAlnum(CharT c)
	{
		return b8(std::isalnum(c));
	}

	template <>
	HV_FORCE_INL b8 CharUtils<WideChar>::IsAlnum(WideChar c)
	{
		return b8(std::iswalnum(c));
	}

	template <typename CharT>
	HV_FORCE_INL b8 CharUtils<CharT>::IsOctDigit(CharT c)
	{
		return c >= '0' && c <= '7';
	}

	template <typename CharT>
	HV_FORCE_INL b8 CharUtils<CharT>::IsHexDigit(CharT c)
	{
		return (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
	}

	template <typename CharT>
	HV_FORCE_INL b8 CharUtils<CharT>::IsWhitespace(CharT c)
	{
		return c == CharT(' ') || c == CharT('\t') || c == CharT('\n') || c == CharT('\r') || c == CharT('\x0B') || c == CharT('\x0C');
	}

	template <typename CharT>
	u8 CharUtils<CharT>::ParseDigit(CharT c, u8 base)
	{
		if (base < 2 || base > 16)
			return 0xFF;
		if (base <= 10)
		{
			u16 val = u16(c - '0');
			if (val >= base)
				return 0xFF;
			return u8(val);
		}
		else
		{
			u16 val = u16(c - '0');
			if (val > 10)
			{
				val = u16(ToLower(c) - 'a');
				if (val >= 6)
					return 0xFF;
				return u8(val + 10);
			}
			return u8(val);
		}
	}

	template <typename CharT>
	template <u8 Base>
	u8 CharUtils<CharT>::ParseDigit(CharT c)
	{
		if constexpr (Base < 2 || Base > 16)
			return 0xFF;
		else if constexpr (Base <= 10)
		{
			u16 val = u16(c - '0');
			if (val >= Base)
				return 0xFF;
			return u8(val);
		}
		else
		{
			u16 val = u16(c - '0');
			if (val > 10)
			{
				val = u16(ToLower(c) - 'a');
				if (val >= 6)
					return 0xFF;
				return u8(val + 10);
			}
			return u8(val);
		}
	}

	template <>
	HV_FORCE_INL WideChar CharUtils<WideChar>::Convert(WideChar c)
	{
		return c;
	}


	template <typename C>
	HV_FORCE_INL sizeT StringUtils::CStringLength(const C* str)
	{
		return std::strlen(str);
	}

	template <>
	HV_FORCE_INL sizeT StringUtils::CStringLength<WideChar>(const WideChar* str)
	{
		return std::wcslen(str);
	}

	template <typename C>
	HV_FORCE_INL i8 StringUtils::Compare(const C* str0, const C* str1)
	{
		HV_STATIC_ASSERT_MSG((std::is_same_v<C, AnsiChar>), "Incompatible character type!");
		return i8(strcmp(str0, str1));
	}

	template <>
	HV_FORCE_INL i8 StringUtils::Compare<WideChar>(const WideChar* str0, const WideChar* str1)
	{
		return i8(wcscmp(str0, str1));
	}

}
