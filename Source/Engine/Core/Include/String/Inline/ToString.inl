// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ToString.h: Convertions to string
#pragma once
#include "String/ToString.h"

namespace Hv {
	
	////////////////////////////////////////////////////////////////////////////////
	// ToString																	  //
	////////////////////////////////////////////////////////////////////////////////

	HV_INL String ToString(b8 val)
	{
		return val ? String("True") : String("False");
	}

	HV_INL String ToString(i8 val)
	{
		return ToString(i64(val));
	}

	HV_INL String ToString(i16 val)
	{
		return ToString(i64(val));
	}

	HV_INL String ToString(i32 val)
	{
		return ToString(i64(val));
	}

	HV_INL String ToString(i64 val)
	{
		String str;
		u64 uval = u64(val);
		if (val < 0)
		{
			str.Append('-');
			uval = ~val + 1;
		}
		return str.Append(ToString(uval));
	}

	HV_INL String ToString(u8 val)
	{
		return ToString(u64(val));
	}

	HV_INL String ToString(u16 val)
	{
		return ToString(u64(val));
	}

	HV_INL String ToString(u32 val)
	{
		return ToString(u64(val));
	}

	HV_INL String ToString(u64 val)
	{
		WideChar arr[21]; // can store 2 ^ 64 (18'446'744'073'709'551'616) and \0
		arr[20] = '\0';
		WideChar* it = &arr[19];
		if (val == 0)
			*it-- = '0';
		for (; val > 0; val /= 10, --it)
		{
			*it = WideChar('0' + val % 10);
		}
		++it;
		return String(it);
	}

	HV_INL String ToString(f32 val, u32 precision)
	{
		return ToString(f64(val), precision);
	}

	HV_INL String ToString(f64 val, u32 precision)
	{
		// Integer part
		String str = ToString(i64(val));
		// floating point part
		if (val < 0)
			val = -val;
		val -= i64(val);
		for (u8 i = 0; i < precision; ++i)
			val *= 10.f;
		str.Append('.');
		str.Append(ToString(i64(val)));
		return str;
	}

	////////////////////////////////////////////////////////////////////////////////
	// ToHexString																  //
	////////////////////////////////////////////////////////////////////////////////
	HV_INL String ToHexString(i8 val)
	{
		return ToHexString(*(u64*)&val);
	}

	HV_INL String ToHexString(i16 val)
	{
		return ToHexString(*(u64*)&val);
	}

	HV_INL String ToHexString(i32 val)
	{
		return ToHexString(*(u64*)&val);
	}

	HV_INL String ToHexString(i64 val)
	{
		return ToHexString(*(u64*)&val);
	}

	HV_INL String ToHexString(u8 val)
	{
		return ToHexString(*(u64*)&val);
	}

	HV_INL String ToHexString(u16 val)
	{
		return ToHexString(*(u64*)&val);
	}

	HV_INL String ToHexString(u32 val)
	{
		return ToHexString(*(u64*)&val);
	}

	HV_INL String ToHexString(u64 val)
	{
		WideChar arr[17]; // can store 2 ^ 64 (0xFFFF'FFFF'FFFF'FFFF) and \0 + '0x'
		arr[16] = '\0';
		WideChar* it = &arr[15];
		for (; val > 0; val >>= 4, --it)
		{
			u8 tmp = u8(val & 0x0F);
			if (tmp < 10)
				*it = WideChar('0' + tmp);
			else
				*it = WideChar('A' + tmp - 10);
		}
		++it;
		return String(it);
	}

	HV_INL String ToHexString(f64 val)
	{
		u64 bin = *(u64*)&val;
		u8 sign = bin >> 63;
		u64 mantissa = bin & 0xF'FFFF'FFFF'FFFF;
		i16 power = (bin >> 52) & 0x7FF;
		
		return String(sign ? "-" : "").Append(ToHexString(mantissa)).Append('e').Append(ToHexString(power - 128), 2, String::NPos);
	}

	HV_INL String ToOctString(i8 val)
	{
		return ToOctString(i64(val));
	}

	HV_INL String ToOctString(i16 val)
	{
		return ToOctString(i64(val));
	}

	HV_INL String ToOctString(i32 val)
	{
		return ToOctString(i64(val));
	}

	HV_INL String ToOctString(i64 val)
	{
		String str;
		u64 uval = u64(val);
		if (val < 0)
		{
			str.Append('-');
			uval = ~val + 1;
		}
		return str.Append(ToOctString(uval));
	}

	HV_INL String ToOctString(u8 val)
	{
		return ToOctString(u64(val));
	}

	HV_INL String ToOctString(u16 val)
	{
		return ToOctString(u64(val));
	}

	HV_INL String ToOctString(u32 val)
	{
		return ToOctString(u64(val));
	}

	HV_INL String ToOctString(u64 val)
	{
		WideChar arr[23]; // can store 2 ^ 64 (0xFFFF'FFFF'FFFF'FFFF) and \0
		arr[22] = '\0';
		WideChar* it = &arr[21];
		for (; val > 0; val >>= 3, --it)
		{
			u8 tmp = u8(val & 0x07);
			*it = WideChar('0' + tmp);
		}
		++it;
		return String(it);
	}

	////////////////////////////////////////////////////////////////////////////////
	// ToScientificString														  //
	////////////////////////////////////////////////////////////////////////////////
	HV_INL String ToScientificString(f64 val, u32 precision)
	{
		if (val > 10)
		{
			u64 pos = 0;
			while (val > 10)
			{
				val /= 10.0;
				++pos;
			}
			return ToString(val, precision).Append("e-").Append(ToString(pos));
		}
		else if (val < 1)
		{
			u64 pos = 0;
			while (val < 1)
			{
				val *= 10.0;
				++pos;
			}
			return ToString(val).Append('e').Append(ToString(pos));
		}
		else
		{
			return ToString(val).Append("e1");
		}
	}

}
