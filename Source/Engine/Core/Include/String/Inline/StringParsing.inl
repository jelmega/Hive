// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// StringParsing.inl: String parsing functions
#pragma once
#include "String/StringParsing.h"
#include "Math/MathConstants.h"

namespace Hv {

	template <typename C, Container::GrowthPolicyFunc G>
	u64 Detail::ParseU64(const TString<C, G>& str, u8 pos, u8 len, b8* res)
	{
		if (str.Length() == 0)
		{
			if (res)
				*res = false;
			return 0;
		}

		constexpr sizeT maxNumDigits = 20;
		u64 acc = 0;
		u64 lim;
		u64 maxValDivisor = 1'000'000'000'000'000'000;
		if (len > str.Length())
			len = u8(str.Length());

		if (len > maxNumDigits)
		{
			if (res)
				*res = false;
			return 0;
		}

		for (sizeT i = pos, end = pos + len; i < end; ++i)
		{
			u8 tmp = CharUtils<C>::ParseDigit<10>(str[i]);
			if (tmp == 0xFF)
			{
				if (res)
					*res = false;
				return 0;
			}
			acc += tmp;

			if (len == maxNumDigits) // If the value is close to the max, make sure it doesn't 'overflow'
			{
				lim = Math::g_Max<u64> / maxValDivisor;
				if (acc > lim)
				{
					if (res)
						*res = false;
					return 0;
				}
				maxValDivisor /= 10;
			}

			if (i != end - 1)
				acc *= 10;
		}
		if (res)
			*res = true;
		return acc;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	i64 Detail::ParseI64(const TString<C, G>& str, u8 pos, u8 len, b8* res)
	{
		if (str.Length() == 0)
		{
			if (res)
				*res = false;
			return 0;
		}

		constexpr sizeT maxNumDigits = 20;
		if (len > str.Length())
			len = u8(str.Length());

		if (len > maxNumDigits)
		{
			if (res)
				*res = false;
			return 0;
		}

		u8 neg = pos;
		if (str[pos] == '-')
		{
			neg = pos + 1;
		}

		b8 r;
		u64 val = Detail::ParseU64(str, neg, len - neg, &r);

		if (val > 0x7FFF'FFFF'FFFF'FFFF)
		{
			if (res)
				*res = false;
			return 0;
		}
		if (res)
			*res = true;

		return neg != pos ? -i64(val) : val;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	f64 Detail::ParseF64(const TString<C, G>& str, u8 pos, u8 len, b8* res)
	{
		sizeT point = str.Find('.', pos);
		if (point == String::NPos)
		{
			return f64(ParseI64(str, pos, len, res));
		}
		else
		{
			b8 tmpRes0;
			u8 neg = pos;
			if (str[pos] == '-')
			{
				neg = pos + 1;
			}

			f64 val = f64(Detail::ParseI64(str, neg, u8(point), &tmpRes0));

			// decimals
			b8 tmpRes1;
			++point;
			f64 decimals = f64(Detail::ParseU64(str, u8(point), u8(len - point), &tmpRes1));
			tmpRes0 &= tmpRes1;
			if (res)
				*res = tmpRes0;
			if (!tmpRes0)
			{
				return 0;
			}

			// Get the value past the decimal point
			u8 decimalChars = u8(len - point);
			for (;decimalChars > 0; --decimalChars)
			{
				decimals /= 10.0;
			}
			val += decimals;
			return neg != pos ? -val : val;
		}
	}

	template<typename C, Container::GrowthPolicyFunc G>
	HV_INL u64 ParseU64(const TString<C, G>& str, b8* res)
	{
		return Detail::ParseU64(str, 0, u8(str.Length()), res);
	}

	template<typename C, Container::GrowthPolicyFunc G>
	HV_INL u32 ParseU32(const TString<C, G>& str, b8* res)
	{
		b8 r;
		u64 val = Detail::ParseU64(str, 0, u8(str.Length()), &r);
		if (!r || val > Math::g_Max<u32>)
		{
			if (res)
				*res = false;
			return 0;
		}
		if (res)
			*res = true;
		return u32(val);
	}

	template<typename C, Container::GrowthPolicyFunc G>
	HV_INL u16 ParseU16(const TString<C, G>& str, b8* res)
	{
		b8 r;
		u64 val = Detail::ParseU64(str, 0, u8(str.Length()), &r);
		if (!r || val > Math::g_Max<u16>)
		{
			if (res)
				*res = false;
			return 0;
		}
		if (res)
			*res = true;
		return u16(val);
	}

	template<typename C, Container::GrowthPolicyFunc G>
	HV_INL u8 ParseU8(const TString<C, G>& str, b8* res)
	{
		b8 r;
		u64 val = Detail::ParseU64(str, 0, u8(str.Length()), &r);
		if (!r || val > Math::g_Max<u8>)
		{
			if (res)
				*res = false;
			return 0;
		}
		if (res)
			*res = true;
		return u8(val);
	}

	template<typename C, Container::GrowthPolicyFunc G>
	HV_INL i64 ParseI64(const TString<C, G>& str, b8* res)
	{
		return Detail::ParseI64(str, 0, u8(str.Length()), res);
	}

	template<typename C, Container::GrowthPolicyFunc G>
	HV_INL i32 ParseI32(const TString<C, G>& str, b8* res)
	{
		b8 r;
		i64 val = Detail::ParseI64(str, 0, u8(str.Length()), &r);
		if (!r || val < Math::g_Min<i32> || val > Math::g_Max<i32>)
		{
			if (res)
				*res = false;
			return 0;
		}
		if (res)
			*res = true;
		return i32(val);
	}

	template<typename C, Container::GrowthPolicyFunc G>
	HV_INL i16 ParseI16(const TString<C, G>& str, b8* res)
	{
		b8 r;
		i64 val = Detail::ParseI64(str, u8(str.Length()), &r);
		if (!r || val < Math::g_Min<i16> || val > Math::g_Max<i16>)
		{
			if (res)
				*res = false;
			return 0;
		}
		if (res)
			*res = true;
		return i16(val);
	}

	template<typename C, Container::GrowthPolicyFunc G>
	HV_INL i8 ParseI8(const TString<C, G>& str, b8* res)
	{
		b8 r;
		i64 val = Detail::ParseI64(str, u8(str.Length()), &r);
		if (!r || val < Math::g_Min<i8> || val > Math::g_Max<i8>)
		{
			if (res)
				*res = false;
			return 0;
		}
		if (res)
			*res = true;
		return i8(val);
	}

	template <typename C, Container::GrowthPolicyFunc G>
	f64 ParseF64(const TString<C, G>& str, b8* res)
	{
		return Detail::ParseF64(str, 0, u8(str.Length()), res);
	}

	template <typename C, Container::GrowthPolicyFunc G>
	f64 ParseF64Sci(const TString<C, G>& str, b8* res)
	{
		b8 tres;
		sizeT pos = str.FindAny("eE");
		f64 val = Detail::ParseF64(str, 0, u8(pos), &tres);
		if (!tres)
		{
			if (res)
				*res = tres;
			return 0;
		}
		u64 exp = Detail::ParseI64(str, u8(pos + 1), u8(str.Length() - pos - 1), &tres);
		if (res)
			*res = tres;
		if (!tres)
		{
			return 0;
		}
		if (exp > 0)
		{
			for (; exp > 0; --exp)
			{
				val *= 10.0;
			}
		}
		else
		{
			for (; exp < 0; ++exp)
			{
				val *= 0.1;
			}
		}
		return val;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	f32 ParseF32(const TString<C, G>& str, b8* res)
	{
		return f32(ParseF64(str, res));
	}

}
