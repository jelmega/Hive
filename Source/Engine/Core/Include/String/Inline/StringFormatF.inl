// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// StringFormat.inl: String formatting
// Template meta-programming is a bit ugly and complicated, but it works very well!
#pragma once
#include "String/StringFormatF.h"
#include "String/ToString.h"
#include "String/String.h"

namespace Hv {

	// ReSharper disable CppDependentTemplateWithoutTemplateKeyword
	namespace Detail::Format {
		
		template<sizeT index, typename C, Container::GrowthPolicyFunc G, typename ...Args>
		void InterpSigned(TString<C, G>& out, Tuple<Args...> params)
		{
			if constexpr (Traits::IsArithmaticV<decltype(params.Get<index>())>)
			{
				u64 val = i64(params.Get<index>());
				out = ToString(val);
			}
			HV_UNREFERENCED_PARAM(out);
		}

		template<sizeT index, typename C, Container::GrowthPolicyFunc G, typename ...Args>
		void InterpUnsigned(TString<C, G>& out, Tuple<Args...> params)
		{
			if constexpr (Traits::IsArithmaticV<decltype(params.Get<index>())>)
			{
				u64 val = u64(params.Get<index>());
				out = ToString(val);
			}
			HV_UNREFERENCED_PARAM(out);
		}

		template<sizeT index, typename C, Container::GrowthPolicyFunc G, typename ...Args>
		void InterpOct(TString<C, G>& out, Tuple<Args...> params)
		{
			if constexpr (Traits::IsArithmaticV<decltype(params.Get<index>())>)
			{
				i64 val = i64(params.Get<index>());
				out = ToOctString(val);
			}
			HV_UNREFERENCED_PARAM(out);
		}

		template<sizeT index, typename C, Container::GrowthPolicyFunc G, typename ...Args>
		void InterpHex(TString<C, G>& out, Tuple<Args...> params)
		{
			if constexpr (Traits::IsIntegralV<decltype(params.Get<index>())>)
			{
				u64 val = u64(params.Get<index>());
				out = ToHexString(val);
			}
			HV_UNREFERENCED_PARAM(out);
		}

		template<sizeT index, typename C, Container::GrowthPolicyFunc G, typename ...Args>
		void InterpFloat(TString<C, G>& out, Tuple<Args...> params, u32 precision)
		{
			if constexpr (Traits::IsArithmaticV<decltype(params.Get<index>())>)
			{
				f64 val = f64(params.Get<index>());
				out = ToString(val, precision);
			}
			HV_UNREFERENCED_PARAM(out);
			HV_UNREFERENCED_PARAM(precision);
		}

		template<sizeT index, typename C, Container::GrowthPolicyFunc G, typename ...Args>
		void InterpScientific(TString<C, G>& out, Tuple<Args...> params, u32 precision)
		{
			if constexpr (Traits::IsArithmaticV<decltype(params.Get<index>())>)
			{
				f64 val = f64(params.Get<index>());
				out = ToScientificString(val, precision);
			}
			HV_UNREFERENCED_PARAM(out);
			HV_UNREFERENCED_PARAM(precision);
		}

		template<sizeT index, typename C, Container::GrowthPolicyFunc G, typename ...Args>
		void InterpShortRep(TString<C, G>& out, Tuple<Args...> params, u32 precision)
		{
			if constexpr (Traits::IsArithmaticV<decltype(params.Get<index>())>)
			{
				f64 val = f64(params.Get<index>());
				TString<C, G> str0 = ToString(val, precision);
				TString<C, G> str1 = ToScientificString(val, precision);
				out = str0.Length() < str1.Length() ? str0 : str1;
			}
			HV_UNREFERENCED_PARAM(out);
			HV_UNREFERENCED_PARAM(precision);
		}

		template<sizeT index, typename C, Container::GrowthPolicyFunc G, typename ...Args>
		void InterpHexFloat(TString<C, G>& out, Tuple<Args...> params)
		{
			if constexpr (Traits::IsFloatingPointV<decltype(params.Get<index>())>)
			{
				f64 val = f64(params.Get<index>());
				out = ToHexString(val);
			}
			HV_UNREFERENCED_PARAM(out);
		}

		template<sizeT index, typename C, Container::GrowthPolicyFunc G, typename ...Args>
		void InterpChar(TString<C, G>& out, Tuple<Args...> params)
		{
			if constexpr (Traits::IsSameV<AnsiChar, decltype(params.Get<index>())> || Traits::IsSameV<WideChar, decltype(params.Get<index>())>)
			{
				WideChar val = WideChar(params.Get<index>());
				out = val;
			}
			HV_UNREFERENCED_PARAM(out);
		}

		template<sizeT index, typename C, Container::GrowthPolicyFunc G, typename ...Args>
		void InterpString(TString<C, G>& out, Tuple<Args...> params)
		{
			if constexpr(Traits::IsSameV<Hv::WideString, decltype(params.Get<index>())>)
			{
				WideString val = params.Get<index>();
				out = val;
			}
			else if constexpr (Traits::IsSameV<Hv::AnsiString, decltype(params.Get<index>())>)
			{
				AnsiString val = params.Get<index>();
				out = val;
			}
			else if constexpr (Traits::IsSameV<AnsiChar*, Traits::RemoveConstT<Traits::DecayT<decltype(params.Get<index>())>>> ||
								Traits::IsSameV<AnsiChar*, Traits::RemoveConstT<decltype(params.Get<index>())>> ||
								Traits::IsSameV<const AnsiChar*, Traits::RemoveConstT<Traits::DecayT<decltype(params.Get<index>())>>> ||
								Traits::IsSameV<const AnsiChar*, Traits::RemoveConstT<decltype(params.Get<index>())>>)
			{
				const AnsiChar* val = params.Get<index>();
				out = val;
			}
			else if constexpr (Traits::IsSameV<WideChar*, Traits::RemoveConstT<Traits::DecayT<decltype(params.Get<index>())>>> ||
								Traits::IsSameV<WideChar*, Traits::RemoveConstT<decltype(params.Get<index>())>> ||
								Traits::IsSameV<const WideChar*, Traits::RemoveConstT<Traits::DecayT<decltype(params.Get<index>())>>> ||
								Traits::IsSameV<const WideChar*, Traits::RemoveConstT<decltype(params.Get<index>())>>)
			{
				const WideChar* val = params.Get<index>();
				out = val;
			}
			HV_UNREFERENCED_PARAM(out);
		}

		template<sizeT index, typename C, Container::GrowthPolicyFunc G, typename ...Args>
		void InterpPointer(TString<C, G>& out, Tuple<Args...> params)
		{
			if constexpr (Traits::IsPointerV<decltype(params.Get<index>())> || Traits::IsPointerV<Traits::DecayT<decltype(params.Get<index>())>>)
			{
				const void* val = params.Get<index>();
				out.Append('p');
				out.Append(ToHexString(u64(val)), 2, TString<C, G>::NPos);
			}
			HV_UNREFERENCED_PARAM(out);
		}

		template<typename C, Container::GrowthPolicyFunc G>
		void ApplyMods(TString<C, G>& str, AnsiChar specifier, u8 flag, u32 width, u32 precision)
		{
			if (flag == 0 && width == 0 && precision == 0)
				return;

			switch (specifier)
			{
			case 'i':
			case 'u':
			{
				if (str.Length() > 0)
				{
					switch (flag)
					{
					case '+':
						if (str[0] != '-')
							str.Insert(sizeT(0), '+');
						break;
					case ' ':
						if (str[0] != '-')
							str.Insert(sizeT(0), ' ');
						break;
					default:
						break;
					}
				}

				if (precision != 0)
				{
					i64 diff = precision - str.Length();
					if (diff > 0)
					{
						if (str.Length() > 0 && str[0] == '-')
							str.PadLeft('0', sizeT(diff));
						else
							str.Insert(1, '0', sizeT(diff));
					}
				}

				if (width != 0)
				{
					i64 diff = width - str.Length();
					if (diff > 0)
					{
						if (flag == '-')
							str.PadRight(sizeT(diff));
						else
							str.PadLeft(flag == '0' ? '0' : ' ', sizeT(diff));
					}
				}
				break;
			}
			case 'o':
			{
				if (str.Length() > 0)
				{
					switch (flag)
					{
					case '+':
						if (str[0] != '-')
							str.Insert(sizeT(0), '+');
						break;
					case ' ':
						if (str[0] != '-')
							str.Insert(sizeT(0), ' ');
						break;
					default:
						break;
					}
				}

				if (flag == '#')
				{
					str.PadLeft('0', 1);
				}

				if (precision != 0)
				{
					sizeT pos = flag == '#' ? 1 : 0;
					i64 diff = precision - str.Length() + pos;
					if (diff > 0)
					{
						str.Insert(pos, '0', sizeT(diff));
					}
				}

				if (width != 0)
				{
					i64 diff = width - str.Length();
					if (diff > 0)
					{
						if (flag == '-')
							str.PadRight(sizeT(diff));
						else
							str.PadLeft(flag == '0' ? '0' : ' ', sizeT(diff));
					}
				}
				break;
			}
			case 'x':
			{
				if (flag == '#')
				{
					str.Insert(sizeT(0), "0x");
				}
				if (precision != 0)
				{
					sizeT pos = flag == '#' ? 2 : 0;
					i64 diff = precision - str.Length() + pos;
					if (diff > 0)
					{
						str.Insert(pos, '0', sizeT(diff));
					}
				}

				if (width != 0)
				{
					i64 diff = width - str.Length();
					if (diff > 0)
					{
						if (flag == '-')
							str.PadRight(sizeT(diff));
						else
							str.PadLeft(flag == '0' ? '0' : ' ', sizeT(diff));
					}
				}
				break;
			}
			case 'g':
			{
				if (flag == '0')
				{
					str.Insert(1, "0x");
				}
				if (precision != 0) // Precision: max decimal digits
				{
					sizeT eLoc = str.Find('e');
					sizeT point = str.Find('.');

					if (eLoc != TString<C, G>::NPos)
					{
						if (point != TString<C, G>::NPos)
						{
							sizeT len = eLoc - point;
							if (len > precision)
							{
								sizeT diff = len - precision;
								str.Erase(eLoc - diff, diff);
							}
						}
					}
					else
					{
						// just use eLoc as the end of the string
						eLoc = str.Length();
						if (point != TString<C, G>::NPos)
						{
							sizeT len = eLoc - point;
							if (len > precision)
							{
								sizeT diff = len - precision;
								str.Erase(eLoc - diff, diff);
							}
						}
					}

					i64 diff = (str.Length() - eLoc) - precision;
					if (diff > 0)
					{
						str.Erase(eLoc + precision, TString<C, G>::NPos);
					}
				}

				if (width != 0)
				{
					i64 diff = width - str.Length();
					if (diff > 0)
					{
						if (str.Length() > 0 && str[0] == '-')
						{
							if (flag == '-')
								str.PadRight(sizeT(diff));
							else
								str.PadLeft(flag == '0' ? '0' : ' ', sizeT(diff));
						}
						else
						{
							if (flag == '-')
								str.PadRight(sizeT(diff));
							else
								str.Insert(2, flag == '0' ? '0' : ' ', sizeT(diff));
						}
					}
				}
				break;
			}
			case 's':
			{
				if (precision != 0 && str.Length() > precision)
					str.Erase(precision, TString<C, G>::NPos);

				if (width != 0)
				{
					i64 diff = width - str.Length();
					if (diff > 0)
					{
						if (str.Length() > 0 && str[0] == '-')
						{
							if (flag == '-')
								str.PadRight(sizeT(diff));
							else
								str.PadLeft(flag == '0' ? '0' : ' ', sizeT(diff));
						}
						else
						{
							if (flag == '-')
								str.PadRight(sizeT(diff));
							else
								str.Insert(2, flag == '0' ? '0' : ' ', sizeT(diff));
						}
					}
				}
				break;
			}
			case 'e':
			{
				if (width != 0)
				{
					i64 diff = width - str.Length();
					if (diff > 0)
					{
						if (flag == '-')
							str.PadRight(sizeT(diff));
						else
							str.PadLeft(flag == '0' ? '0' : ' ', sizeT(diff));
					}
				}
				break;
			}
			default: // a, e, f, g
			{
				sizeT point = str.Find('.');
				if (flag == '#')
				{
					if (point == TString<C, G>::NPos)
					{
						str.Append('.');
						point = str.Length() - 1;
					}
				}

				if (point != TString<C, G>::NPos)
				{
					i64 diff = precision - (str.Length() - point - 1);
					if (diff > 0)
					{
						str.Append('0', sizeT(diff));
					}
				}

				if (width != 0)
				{
					i64 diff = width - str.Length();
					if (diff > 0)
					{
						if (str[0] == '-')
						{
							if (str.Length() > 0 && flag == '-')
								str.PadRight(sizeT(diff));
							else
								str.PadLeft(flag == '0' ? '0' : ' ', sizeT(diff));
						}
						else
						{
							if (flag == '-')
								str.PadRight(sizeT(diff));
							else
								str.Insert(2, flag == '0' ? '0' : ' ', sizeT(diff));
						}
					}
				}
				break;
			}
			}

			
		}

	}
	// ReSharper restore CppDependentTemplateWithoutTemplateKeyword

#define SIGNED1(index, out, params) Hv::Detail::Format::InterpSigned<index>(out, params)
#define SIGNED16(index, out, params)\
	switch(index)\
	{\
		case 0: SIGNED1(0, out, params); break;\
		case 1: SIGNED1(1, out, params); break;\
		case 2: SIGNED1(2, out, params); break;\
		case 3: SIGNED1(3, out, params); break;\
		case 4: SIGNED1(4, out, params); break;\
		case 5: SIGNED1(5, out, params); break;\
		case 6: SIGNED1(6, out, params); break;\
		case 7: SIGNED1(7, out, params); break;\
		case 8: SIGNED1(8, out, params); break;\
		case 9: SIGNED1(9, out, params); break;\
		case 10: SIGNED1(10, out, params); break;\
		case 11: SIGNED1(11, out, params); break;\
		case 12: SIGNED1(12, out, params); break;\
		case 13: SIGNED1(13, out, params); break;\
		case 14: SIGNED1(14, out, params); break;\
		case 15: SIGNED1(15, out, params); break;\
		default: SIGNED1(16, out, params); break;\
	}

#define UNSIGNED1(index, out, params) Hv::Detail::Format::InterpUnsigned<index>(out, params)
#define UNSIGNED16(index, out, params)\
	switch(index)\
	{\
		case 0: UNSIGNED1(0, out, params); break;\
		case 1: UNSIGNED1(1, out, params); break;\
		case 2: UNSIGNED1(2, out, params); break;\
		case 3: UNSIGNED1(3, out, params); break;\
		case 4: UNSIGNED1(4, out, params); break;\
		case 5: UNSIGNED1(5, out, params); break;\
		case 6: UNSIGNED1(6, out, params); break;\
		case 7: UNSIGNED1(7, out, params); break;\
		case 8: UNSIGNED1(8, out, params); break;\
		case 9: UNSIGNED1(9, out, params); break;\
		case 10: UNSIGNED1(10, out, params); break;\
		case 11: UNSIGNED1(11, out, params); break;\
		case 12: UNSIGNED1(12, out, params); break;\
		case 13: UNSIGNED1(13, out, params); break;\
		case 14: UNSIGNED1(14, out, params); break;\
		case 15: UNSIGNED1(15, out, params); break;\
		default: UNSIGNED1(16, out, params); break;\
	}

#define HEX1(index, out, params) Hv::Detail::Format::InterpHex<index>(out, params)
#define HEX16(index, out, params)\
	switch(index)\
	{\
		case 0: HEX1(0, out, params); break;\
		case 1: HEX1(1, out, params); break;\
		case 2: HEX1(2, out, params); break;\
		case 3: HEX1(3, out, params); break;\
		case 4: HEX1(4, out, params); break;\
		case 5: HEX1(5, out, params); break;\
		case 6: HEX1(6, out, params); break;\
		case 7: HEX1(7, out, params); break;\
		case 8: HEX1(8, out, params); break;\
		case 9: HEX1(9, out, params); break;\
		case 10: HEX1(10, out, params); break;\
		case 11: HEX1(11, out, params); break;\
		case 12: HEX1(12, out, params); break;\
		case 13: HEX1(13, out, params); break;\
		case 14: HEX1(14, out, params); break;\
		case 15: HEX1(15, out, params); break;\
		default: HEX1(16, out, params); break;\
	}

#define OCT1(index, out, params) Hv::Detail::Format::InterpOct<index>(out, params)
#define OCT16(index, out, params)\
	switch(index)\
	{\
		case 0: OCT1(0, out, params); break;\
		case 1: OCT1(1, out, params); break;\
		case 2: OCT1(2, out, params); break;\
		case 3: OCT1(3, out, params); break;\
		case 4: OCT1(4, out, params); break;\
		case 5: OCT1(5, out, params); break;\
		case 6: OCT1(6, out, params); break;\
		case 7: OCT1(7, out, params); break;\
		case 8: OCT1(8, out, params); break;\
		case 9: OCT1(9, out, params); break;\
		case 10: OCT1(10, out, params); break;\
		case 11: OCT1(11, out, params); break;\
		case 12: OCT1(12, out, params); break;\
		case 13: OCT1(13, out, params); break;\
		case 14: OCT1(14, out, params); break;\
		case 15: OCT1(15, out, params); break;\
		default: OCT1(16, out, params); break;\
	}

#define FLT1(index, out, params, precision) Hv::Detail::Format::InterpFloat<index>(out, params, precision)
#define FLT16(index, out, params, precision)\
	switch(index)\
	{\
		case 0: FLT1(0, out, params, precision); break;\
		case 1: FLT1(1, out, params, precision); break;\
		case 2: FLT1(2, out, params, precision); break;\
		case 3: FLT1(3, out, params, precision); break;\
		case 4: FLT1(4, out, params, precision); break;\
		case 5: FLT1(5, out, params, precision); break;\
		case 6: FLT1(6, out, params, precision); break;\
		case 7: FLT1(7, out, params, precision); break;\
		case 8: FLT1(8, out, params, precision); break;\
		case 9: FLT1(9, out, params, precision); break;\
		case 10: FLT1(10, out, params, precision); break;\
		case 11: FLT1(11, out, params, precision); break;\
		case 12: FLT1(12, out, params, precision); break;\
		case 13: FLT1(13, out, params, precision); break;\
		case 14: FLT1(14, out, params, precision); break;\
		case 15: FLT1(15, out, params, precision); break;\
		default: FLT1(16, out, params, precision); break;\
	}

#define SCI1(index, out, params, precision) Hv::Detail::Format::InterpScientific<index>(out, params, precision)
#define SCI16(index, out, params, precision)\
	switch(index)\
	{\
		case 0: SCI1(0, out, params, precision); break;\
		case 1: SCI1(1, out, params, precision); break;\
		case 2: SCI1(2, out, params, precision); break;\
		case 3: SCI1(3, out, params, precision); break;\
		case 4: SCI1(4, out, params, precision); break;\
		case 5: SCI1(5, out, params, precision); break;\
		case 6: SCI1(6, out, params, precision); break;\
		case 7: SCI1(7, out, params, precision); break;\
		case 8: SCI1(8, out, params, precision); break;\
		case 9: SCI1(9, out, params, precision); break;\
		case 10: SCI1(10, out, params, precision); break;\
		case 11: SCI1(11, out, params, precision); break;\
		case 12: SCI1(12, out, params, precision); break;\
		case 13: SCI1(13, out, params, precision); break;\
		case 14: SCI1(14, out, params, precision); break;\
		case 15: SCI1(15, out, params, precision); break;\
		default: SCI1(16, out, params, precision); break;\
	}

#define SREP1(index, out, params, precision) Hv::Detail::Format::InterpShortRep<index>(out, params, precision)
#define SREP16(index, out, params, precision)\
	switch(index)\
	{\
		case 0: SREP1(0, out, params, precision); break;\
		case 1: SREP1(1, out, params, precision); break;\
		case 2: SREP1(2, out, params, precision); break;\
		case 3: SREP1(3, out, params, precision); break;\
		case 4: SREP1(4, out, params, precision); break;\
		case 5: SREP1(5, out, params, precision); break;\
		case 6: SREP1(6, out, params, precision); break;\
		case 7: SREP1(7, out, params, precision); break;\
		case 8: SREP1(8, out, params, precision); break;\
		case 9: SREP1(9, out, params, precision); break;\
		case 10: SREP1(10, out, params, precision); break;\
		case 11: SREP1(11, out, params, precision); break;\
		case 12: SREP1(12, out, params, precision); break;\
		case 13: SREP1(13, out, params, precision); break;\
		case 14: SREP1(14, out, params, precision); break;\
		case 15: SREP1(15, out, params, precision); break;\
		default: SREP1(16, out, params, precision); break;\
	}

#define HEXFLT1(index, out, params) Hv::Detail::Format::InterpHexFloat<index>(out, params)
#define HEXFLT16(index, out, params)\
	switch(index)\
	{\
		case 0: HEXFLT1(0, out, params); break;\
		case 1: HEXFLT1(1, out, params); break;\
		case 2: HEXFLT1(2, out, params); break;\
		case 3: HEXFLT1(3, out, params); break;\
		case 4: HEXFLT1(4, out, params); break;\
		case 5: HEXFLT1(5, out, params); break;\
		case 6: HEXFLT1(6, out, params); break;\
		case 7: HEXFLT1(7, out, params); break;\
		case 8: HEXFLT1(8, out, params); break;\
		case 9: HEXFLT1(9, out, params); break;\
		case 10: HEXFLT1(10, out, params); break;\
		case 11: HEXFLT1(11, out, params); break;\
		case 12: HEXFLT1(12, out, params); break;\
		case 13: HEXFLT1(13, out, params); break;\
		case 14: HEXFLT1(14, out, params); break;\
		case 15: HEXFLT1(15, out, params); break;\
		default: HEXFLT1(16, out, params); break;\
	}

#define CHAR1(index, out, params) Hv::Detail::Format::InterpChar<index>(out, params)
#define CHAR16(index, out, params)\
	switch(index)\
	{\
		case 0: CHAR1(0, out, params); break;\
		case 1: CHAR1(1, out, params); break;\
		case 2: CHAR1(2, out, params); break;\
		case 3: CHAR1(3, out, params); break;\
		case 4: CHAR1(4, out, params); break;\
		case 5: CHAR1(5, out, params); break;\
		case 6: CHAR1(6, out, params); break;\
		case 7: CHAR1(7, out, params); break;\
		case 8: CHAR1(8, out, params); break;\
		case 9: CHAR1(9, out, params); break;\
		case 10: CHAR1(10, out, params); break;\
		case 11: CHAR1(11, out, params); break;\
		case 12: CHAR1(12, out, params); break;\
		case 13: CHAR1(13, out, params); break;\
		case 14: CHAR1(14, out, params); break;\
		case 15: CHAR1(15, out, params); break;\
		default: CHAR1(16, out, params); break;\
	}

#define STR1(index, out, params) Hv::Detail::Format::InterpString<index>(out, params)
#define STR16(index, out, params)\
	switch(index)\
	{\
		case 0: STR1(0, out, params); break;\
		case 1: STR1(1, out, params); break;\
		case 2: STR1(2, out, params); break;\
		case 3: STR1(3, out, params); break;\
		case 4: STR1(4, out, params); break;\
		case 5: STR1(5, out, params); break;\
		case 6: STR1(6, out, params); break;\
		case 7: STR1(7, out, params); break;\
		case 8: STR1(8, out, params); break;\
		case 9: STR1(9, out, params); break;\
		case 10: STR1(10, out, params); break;\
		case 11: STR1(11, out, params); break;\
		case 12: STR1(12, out, params); break;\
		case 13: STR1(13, out, params); break;\
		case 14: STR1(14, out, params); break;\
		case 15: STR1(15, out, params); break;\
		default: STR1(16, out, params); break;\
	}

#define PTR1(index, out, params) Hv::Detail::Format::InterpPointer<index>(out, params)
#define PTR16(index, out, params)\
	switch(index)\
	{\
		case 0: PTR1(0, out, params); break;\
		case 1: PTR1(1, out, params); break;\
		case 2: PTR1(2, out, params); break;\
		case 3: PTR1(3, out, params); break;\
		case 4: PTR1(4, out, params); break;\
		case 5: PTR1(5, out, params); break;\
		case 6: PTR1(6, out, params); break;\
		case 7: PTR1(7, out, params); break;\
		case 8: PTR1(8, out, params); break;\
		case 9: PTR1(9, out, params); break;\
		case 10: PTR1(10, out, params); break;\
		case 11: PTR1(11, out, params); break;\
		case 12: PTR1(12, out, params); break;\
		case 13: PTR1(13, out, params); break;\
		case 14: PTR1(14, out, params); break;\
		case 15: PTR1(15, out, params); break;\
		default: PTR1(16, out, params); break;\
	}

	template<typename C, Container::GrowthPolicyFunc G, typename... Args>
	TString<C, G> Hv::FormatF(const TString<C, G>& format, const Args&... args)
	{
		constexpr sizeT npos = TString<C, G>::NPos;
		constexpr u8 defFloatPrecision = 6;
		TString<C, G> out;

		Tuple<Args...> params(args...);
		HV_ASSERT_MSG(params.Count <= 16, "Max 16 parameters allowed!");
		HV_UNREFERENCED_PARAM(params);

		sizeT index = 0;
		sizeT paramIdx = 0;
		while (index < format.Length())
		{
			sizeT prevIdx = index;
			index = format.Find('%', index);

			if (index == npos)
			{
				out.Append(format, prevIdx, npos);
				break;
			}

			out.Append(format, prevIdx, index - prevIdx);

			if (format[index + 1] == '\0') // End of format
			{
				break;
			}
			else if (format[index + 1] == '%') // Double %% -> %
			{
				out.Append('%');
				index += 2;
			}
			else
			{
				sizeT specifier = format.FindAny(Detail::FormatF::g_Specifiers, index);
				sizeT space = format.Find(' ', index);
				if (space == index + 1)
					space = format.Find(' ', index + 2);
				++index;
				if (specifier != npos && specifier < space)
				{
					// Find flags, width and presicion
					AnsiChar flag = '\0';
					u32 width = 0;
					u32 precision = 0;

					if (index != specifier)
					{
						//flag
						switch (format[index])
						{
						case '-':
						case '+':
						case ' ':
						case '#':
						case '0':
							flag = AnsiChar(format[index]);
							++index;
						default:
							break;
						}

						sizeT point = format.Find('.', index);
						if (point < specifier)
						{
							if (index != point)
							{
								if (format[index] == '*')
								{
									width = 0xFFFF'FFFF;
									HV_ASSERT(false);
								}
								else
								{
									b8 res;
									width = ParseU8(format.SubString(index, point - index), &res);
									index = point + 1;
									HV_ASSERT(res);
								}
							}
							if (point + 1 != specifier)
							{
								if (format[index] == '*')
								{
									precision = 0xFFFF'FFFF;
									HV_ASSERT(false);
								}
								else
								{
									b8 res;
									precision = ParseU8(format.SubString(point + 1, specifier - point - 1), &res);
									index = specifier;
									HV_ASSERT(res);
								}
							}
						}
						else if (index != specifier)
						{
							b8 res;
							width = ParseU8(format.SubString(index, specifier - index), &res);
							index = specifier;
							HV_ASSERT(res);
						}
					}

					// Handle formats
					switch (format[specifier])
					{
					case 'i':
					case 'd':
					{
						TString<C, G> tmp;
						SIGNED16(paramIdx, tmp, params);
						Detail::Format::ApplyMods(tmp, 'i', flag, width, precision);
						out.Append(tmp);
						++index;
						++paramIdx;
						break;
					}
					case 'u':
					{
						TString<C, G> tmp;
						UNSIGNED16(paramIdx, tmp, params);
						Detail::Format::ApplyMods(tmp, 'u', flag, width, precision);
						out.Append(tmp);
						++index;
						++paramIdx;
						break;
					}
					case 'o':
					{
						TString<C, G> tmp;
						OCT16(paramIdx, tmp, params);
						Detail::Format::ApplyMods(tmp, 'o', flag, width, precision);
						out.Append(tmp);
						++index;
						++paramIdx;
						break;
					}
					case 'x':
					case 'X':
					{
						TString<C, G> tmp;
						HEX16(paramIdx, tmp, params);
						Detail::Format::ApplyMods(tmp, 'x', flag, width, precision);
						out.Append(tmp);
						++index;
						++paramIdx;
						break;
					}
					case 'f':
					case 'F':
					{
						TString<C, G> tmp;
						if (precision == 0)
							precision = defFloatPrecision;
						FLT16(paramIdx, tmp, params, precision);
						Detail::Format::ApplyMods(tmp, 'f', flag, width, precision);
						out.Append(tmp);
						++index;
						++paramIdx;
						break;
					}
					case 'e':
					case 'E':
					{
						TString<C, G> tmp;
						if (precision == 0)
							precision = defFloatPrecision;
						SCI16(paramIdx, tmp, params, precision);
						Detail::Format::ApplyMods(tmp, 'e', flag, width, precision);
						out.Append(tmp);
						++index;
						++paramIdx;
						break;
					}
					case 'g':
					case 'G':
					{
						TString<C, G> tmp;
						if (precision == 0)
							precision = defFloatPrecision;
						SREP16(paramIdx, tmp, params, precision);
						Detail::Format::ApplyMods(tmp, 'g', flag, width, precision);
						out.Append(tmp);
						++index;
						++paramIdx;
						break;
					}
					case 'a':
					case 'A':
					{
						TString<C, G> tmp;
						HEXFLT16(paramIdx, tmp, params);
						if (precision == 0)
							precision = defFloatPrecision;
						Detail::Format::ApplyMods(tmp, 'a', flag, width, precision);
						out.Append(tmp);
						++index;
						++paramIdx;
						break;
					}
					case 'c':
					{
						TString<C, G> tmp;
						CHAR16(paramIdx, tmp, params);
						//Detail::Format::ApplyMods(tmp, 'c', flag, width, precision);
						out.Append(tmp);
						++index;
						++paramIdx;
						break;
					}
					case 's':
					{
						TString<C, G> tmp;
						STR16(paramIdx, tmp, params);
						Detail::Format::ApplyMods(tmp, 's', flag, width, precision);
						out.Append(tmp);
						++index;
						++paramIdx;
						break;
					}
					case 'p':
					{
						TString<C, G> tmp;
						PTR16(paramIdx, tmp, params);
						// Handle as 'x'
						Detail::Format::ApplyMods(tmp, 'x', flag, width > 0 ? width - 1 : 0, precision);
						out.Insert(sizeT(0), 'p');
						out.Append(tmp);
						++index;
						++paramIdx;
						break;
					}
					default: break;
					}
				}
				else // Skip arg
				{
					index = space;
					if (index != npos)
						++index;
				}
			}
		}

		return out;
	}

	/*template <typename C, Container::GrowthPolicyFunc G, typename T>
	TString<C, G> FormatF(const TString<C, G>& format, const T& arg)
	{
		return TString<C, G>();
	}*/

#undef SIGNED
#undef SIGNED16
#undef CHAR
#undef CHAR16

}
