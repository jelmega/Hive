// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// TString.inl: General template string
#pragma once
#include "String/TString.h"
#include "StringFormatF.inl"

namespace Hv {
	
	////////////////////////////////////////////////////////////////////////////////
	//	Constructors
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>::TString()
		: m_Data(nullptr)
		, m_Length(0)
		, m_Capacity(0)
		, m_pAlloc(g_pAllocator)
	{
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>::TString(Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Length(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>::TString(sizeT capacity, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Length(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		Reserve(capacity);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>::TString(AnsiChar c, sizeT count, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Length(count)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		Append(c, count);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>::TString(WideChar c, sizeT count, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Length(count)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		Append(c, count);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>::TString(const AnsiChar* str, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Length(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		Append(str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>::TString(const WideChar* str, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Length(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		Append(str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>::TString(const AnsiChar* arr, sizeT len, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Length(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		Append(arr, len);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>::TString(const WideChar* arr, sizeT len, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Length(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		Append(arr, len);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>::TString(const AnsiChar* itFirst, const AnsiChar* itLast, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Length(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(itFirst, "itFirst can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "itLast can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "itFirst must come before itLast!");

		Append(itFirst, sizeT(itLast - itFirst));
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>::TString(const WideChar* itFirst, const WideChar* itLast, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Length(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(itFirst, "itFirst can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "itLast can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "itFirst must come before itLast!");

		Append(itFirst, sizeT(itLast - itFirst));
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename InputIterator>
	TString<CharT, GrowthPolicy>::TString(const InputIterator& itFirst, const InputIterator& itLast,
		Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Length(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(itFirst != itLast, "itFirst cannot be the same as itLast!");

		Append(itFirst, itLast);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>::TString(std::initializer_list<AnsiChar> il, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Length(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		Append(il.begin(), il.size());
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>::TString(std::initializer_list<WideChar> il, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Length(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		Append(il.begin(), il.size());
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>::TString(const TString<C, G>& str, sizeT pos, sizeT len, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Length(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		Append(str, pos, len);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>::TString(const TString<C, G>& str)
		: m_Data(nullptr)
		, m_Length(0)
		, m_Capacity(0)
		, m_pAlloc(str.m_pAlloc)
	{
		Append(str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>::TString(const TString& str)
		: m_Data(nullptr)
		, m_Length(0)
		, m_Capacity(0)
		, m_pAlloc(str.m_pAlloc)
	{
		Append(str.m_Data, str.m_Length);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>::TString(const TString<C, G>& str, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Length(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		Append(str.m_Data, str.m_Length);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>::TString(TString<C, G>&& str)
		: m_Data(nullptr)
		, m_Length(0)
		, m_Capacity(0)
		, m_pAlloc(str.m_pAlloc)
	{
		Append(str.m_Data, str.m_Length);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>::TString(TString&& str) noexcept
		: m_Data(nullptr)
		, m_Length(0)
		, m_Capacity(0)
		, m_pAlloc(str.m_pAlloc)
	{
		if (m_pAlloc == str.m_pAlloc)
		{
			Hv::Swap(m_Data, str.m_Data);
			Hv::Swap(m_Length, str.m_Length);
			Hv::Swap(m_Capacity, str.m_Capacity);
		}
		else
		{
			Append(str);
			str.Clear(true);
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>::~TString()
	{
		Clear(true);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Append (operator+)
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy> TString<CharT, GrowthPolicy>::operator+(AnsiChar c) const
	{
		return TString(*this).Append(c);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy> TString<CharT, GrowthPolicy>::operator+(WideChar c) const
	{
		return TString(*this).Append(c);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy> TString<CharT, GrowthPolicy>::operator+(const AnsiChar* str) const
	{
		return TString(*this).Append(str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy> TString<CharT, GrowthPolicy>::operator+(const WideChar* str) const
	{
		return TString(*this).Append(str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy> TString<CharT, GrowthPolicy>::operator+(
		std::initializer_list<AnsiChar> il) const
	{
		return TString(*this).Append(il);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy> TString<CharT, GrowthPolicy>::operator+(
		std::initializer_list<WideChar> il) const
	{
		return TString(*this).Append(il);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy> TString<CharT, GrowthPolicy>::operator+(
		const TString<C, G>& str) const
	{
		return TString(*this).Append(str);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Assign
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::operator=(AnsiChar c)
	{
		Clear();
		Append(c);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::operator=(WideChar c)
	{
		Clear();
		Append(c);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::operator=(const AnsiChar* str)
	{
		Clear();
		Append(str);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::operator=(const WideChar* str)
	{
		Clear();
		Append(str);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::operator=(const AnsiChar(&str)[N])
	{
		Clear();
		Append(str, N);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::operator=(const WideChar(&str)[N])
	{
		Clear();
		Append(str, N);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::operator=(
		std::initializer_list<AnsiChar> il)
	{
		Clear();
		Append(il.begin(), il.size());
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::operator=(
		std::initializer_list<WideChar> il)
	{
		Clear();
		Append(il.begin(), il.size());
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::operator=(
		const TString<C, G>& str)
	{
		Clear();
		Append(str.m_Data, str.m_Length);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::operator=(const TString& str)
	{
		if (m_pAlloc)
			Clear();
		else
			m_pAlloc = str.m_pAlloc;
		Append(str.m_Data, str.m_Length);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::operator=(TString&& str) noexcept
	{
		if (m_pAlloc)
			Clear();
		else
			m_pAlloc = str.m_pAlloc;

		if (m_pAlloc == str.m_pAlloc)
		{
			Hv::Swap(m_Data, str.m_Data);
			Hv::Swap(m_Length, str.m_Length);
			Hv::Swap(m_Capacity, str.m_Capacity);
		}
		else
		{
			Append(str.m_Data, str.m_Length);
			str.Clear(true);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::operator+=(AnsiChar c)
	{
		Append(c);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::operator+=(WideChar c)
	{
		Append(c);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::operator+=(const AnsiChar* str)
	{
		Append(str);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::operator+=(const WideChar* str)
	{
		Append(str);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::operator+=(
		std::initializer_list<AnsiChar> il)
	{
		Append(il.begin(), il.size());
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::operator+=(
		std::initializer_list<WideChar> il)
	{
		Append(il.begin(), il.size());
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::operator+=(
		const TString<C, G>& str)
	{
		Append(str);
		return *this;
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Access operators
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	CharT& TString<CharT, GrowthPolicy>::operator[](sizeT index)
	{
		HV_ASSERT_MSG(index < m_Length, "Iterator out of range!");
		return m_Data[index];
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	const CharT& TString<CharT, GrowthPolicy>::operator[](sizeT index) const
	{
		HV_ASSERT_MSG(index < m_Length, "Iterator out of range!");
		return m_Data[index];
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Compare and lexicographical compare (operator==/!=/<=/</>=/>)
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator==(AnsiChar c) const
	{
		return Compare(c) == 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator==(WideChar c) const
	{
		return Compare(c) == 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator==(const AnsiChar* str) const
	{
		return Compare(str) == 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator==(const WideChar* str) const
	{
		return Compare(str) == 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	b8 TString<CharT, GrowthPolicy>::operator==(const TString<C, G>& str) const
	{
		return Compare(str) == 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator!=(AnsiChar c) const
	{
		return Compare(c) != 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator!=(WideChar c) const
	{
		return Compare(c) != 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator!=(const AnsiChar* str) const
	{
		return Compare(str) != 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator!=(const WideChar* str) const
	{
		return Compare(str) != 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	b8 TString<CharT, GrowthPolicy>::operator!=(const TString<C, G>& str) const
	{
		return Compare(str) != 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator<(AnsiChar c) const
	{
		return Compare(c) < 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator<(WideChar c) const
	{
		return Compare(c) < 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator<(const AnsiChar* str) const
	{
		return Compare(str) < 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator<(const WideChar* str) const
	{
		return Compare(str) < 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	b8 TString<CharT, GrowthPolicy>::operator<(const TString<C, G>& str) const
	{
		return Compare(str) < 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator<=(AnsiChar c) const
	{
		return Compare(c) <= 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator<=(WideChar c) const
	{
		return Compare(c) <= 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator<=(const AnsiChar* str) const
	{
		return Compare(str) <= 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator<=(const WideChar* str) const
	{
		return Compare(str) <= 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	b8 TString<CharT, GrowthPolicy>::operator<=(const TString<C, G>& str) const
	{
		return Compare(str) <= 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator>(AnsiChar c) const
	{
		return Compare(c) > 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator>(WideChar c) const
	{
		return Compare(c) > 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator>(const AnsiChar* str) const
	{
		return Compare(str) > 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator>(const WideChar* str) const
	{
		return Compare(str) > 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	b8 TString<CharT, GrowthPolicy>::operator>(const TString<C, G>& str) const
	{
		return Compare(str) > 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator>=(AnsiChar c) const
	{
		return Compare(c) >= 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator>=(WideChar c) const
	{
		return Compare(c) >= 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator>=(const AnsiChar* str) const
	{
		return Compare(str) >= 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::operator>=(const WideChar* str) const
	{
		return Compare(str) >= 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	b8 TString<CharT, GrowthPolicy>::operator>=(const TString<C, G>& str) const
	{
		return Compare(str) >= 0;
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Append
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Append(AnsiChar c)
	{
		Reserve(m_Length + 1);
		m_Data[m_Length] = CU::Convert(c);
		++m_Length;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Append(WideChar c)
	{
		Reserve(m_Length + 1);
		m_Data[m_Length] = CU::Convert(c);
		++m_Length;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Append(AnsiChar c, sizeT count)
	{
		if (count == 0)
			return *this;

		Reserve(m_Length + count);
		CopyToData(m_Length, c, count);
		m_Length += count;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Append(WideChar c, sizeT count)
	{
		if (count == 0)
			return *this;

		Reserve(m_Length + count);
		CopyToData(m_Length, c, count);
		m_Length += count;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Append(const AnsiChar(&str)[N])
	{
		Append(str, N);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Append(const WideChar(&str)[N])
	{
		Append(str, N);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Append(const AnsiChar* str)
	{
		sizeT len = SU::CStringLength(str);
		Append(str, len);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Append(const WideChar* str)
	{
		sizeT len = SU::CStringLength(str);
		Append(str, len);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Append(const AnsiChar* arr,
		sizeT count)
	{
		if (count == 0)
			return *this;
		Reserve(m_Length + count);
		CopyToData(m_Length, arr, count);
		m_Length += count;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Append(const WideChar* arr,
		sizeT count)
	{
		if (count == 0)
			return *this;
		Reserve(m_Length + count);
		CopyToData(m_Length, arr, count);
		m_Length += count;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Append(const AnsiChar* itFirst,
		const AnsiChar* itLast)
	{
		HV_ASSERT_MSG(itFirst, "itFirst can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "itLast can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "itFirst must come before itLast!");
		Append(itFirst, sizeT(itLast - itFirst));
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Append(const WideChar* itFirst,
		const WideChar* itLast)
	{
		HV_ASSERT_MSG(itFirst, "itFirst can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "itLast can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "itFirst must come before itLast!");
		Append(itFirst, sizeT(itLast - itFirst));
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename InputIterator>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Append(const InputIterator& itFirst,
		const InputIterator& itLast)
	{
		HV_ASSERT_MSG(itFirst != itLast, "itFirst cannot be the same as itLast!");
		sizeT len = 0;
		for (InputIterator it = itFirst; it != itLast; ++it)
			++len;
		if (len == 0)
			return *this;
		Reserve(m_Length + len);
		CopyToData(m_Length, itFirst, len);
		m_Length += len;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Append(
		std::initializer_list<AnsiChar> il)
	{
		Append(il.begin(), il.size());
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Append(
		std::initializer_list<WideChar> il)
	{
		Append(il.begin(), il.size());
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Append(const TString<C, G>& str)
	{
		Append(str.m_Data, str.m_Length);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Append(const TString<C, G>& str,
		sizeT len)
	{
		if (len > str.m_Length)
			len = str.m_Length;
		if (str.m_Length == 0)
			return *this;
		Append(str.m_Data, len);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Append(const TString<C, G>& str,
		sizeT subPos, sizeT len)
	{
		HV_ASSERT_MSG(subPos < str.m_Length, "subPos out of range!");
		if (len > str.m_Length - subPos)
			len = str.m_Length - subPos;
		if (str.m_Length == 0)
			return *this;
		Append(str.m_Data + subPos, len);
		return *this;
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Insert
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(sizeT pos, AnsiChar c)
	{
		HV_ASSERT_MSG(pos <= m_Length, "Position out of range!");
		Reserve(m_Length + 1);
		MoveData(pos, pos + 1);
		m_Data[pos] = CU::Convert(c);
		++m_Length;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(sizeT pos, WideChar c)
	{
		HV_ASSERT_MSG(pos <= m_Length, "Position out of range!");
		Reserve(m_Length + 1);
		MoveData(pos, pos + 1);
		m_Data[pos] = CU::Convert(c);
		++m_Length;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(const CharT* it, AnsiChar c)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Length, "Iterator out of range!");
		Insert(sizeT(it - m_Data), c);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(const CharT* it, WideChar c)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Length, "Iterator out of range!");
		Insert(sizeT(it - m_Data), c);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(sizeT pos, AnsiChar c,
		sizeT count)
	{
		HV_ASSERT_MSG(pos <= m_Length, "Position out of range!");
		if (count == 0)
			return *this;

		Reserve(m_Length + count);
		MoveData(pos, pos + count);
		CopyToData(pos, c, count);
		m_Length += count;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(sizeT pos, WideChar c,
		sizeT count)
	{
		HV_ASSERT_MSG(pos <= m_Length, "Position out of range!");
		if (count == 0)
			return *this;

		Reserve(m_Length + count);
		MoveData(pos, pos + count);
		CopyToData(pos, c, count);
		m_Length += count;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(const CharT* it, AnsiChar c,
		sizeT count)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Length, "Iterator out of range!");
		Insert(sizeT(it - m_Data), c, count);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(const CharT* it, WideChar c,
		sizeT count)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Length, "Iterator out of range!");
		Insert(sizeT(it - m_Data), c, count);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(sizeT pos,
		const AnsiChar* str)
	{
		HV_ASSERT_MSG(pos <= m_Length, "Position out of range!");
		sizeT len = SU::CStringLength(str);
		Insert(pos, str, len);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(sizeT pos,
		const WideChar* str)
	{
		HV_ASSERT_MSG(pos <= m_Length, "Position out of range!");
		sizeT len = SU::CStringLength(str);
		Insert(pos, str, len);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(const CharT* it,
		const AnsiChar* str)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Length, "Iterator out of range!");
		sizeT len = SU::CStringLength(str);
		Insert(sizeT(it - m_Data), str, len);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(const CharT* it,
		const WideChar* str)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Length, "Iterator out of range!");
		sizeT len = SU::CStringLength(str);
		Insert(sizeT(it - m_Data), str, len);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(sizeT pos,
		const AnsiChar* arr, sizeT count)
	{
		HV_ASSERT_MSG(pos <= m_Length, "Position out of range!");
		if (count == 0)
			return *this;

		Reserve(m_Length + count);
		MoveData(pos, pos + count);
		CopyToData(pos, arr, count);
		m_Length += count;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(sizeT pos,
		const WideChar* arr, sizeT count)
	{
		HV_ASSERT_MSG(pos <= m_Length, "Position out of range!");
		if (count == 0)
			return *this;

		Reserve(m_Length + count);
		MoveData(pos, pos + count);
		CopyToData(pos, arr, count);
		m_Length += count;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(const CharT* it,
		const AnsiChar* arr, sizeT count)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Length, "Iterator out of range!");
		Insert(sizeT(it - m_Data), arr, count);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(const CharT* it,
		const WideChar* arr, sizeT count)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Length, "Iterator out of range!");
		Insert(sizeT(it - m_Data), arr, count);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(sizeT pos,
		const AnsiChar* itFirst, const AnsiChar* itLast)
	{
		HV_ASSERT_MSG(pos <= m_Length, "Position out of range!");
		HV_ASSERT_MSG(itFirst, "itFirst can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "itLast can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "itFirst must come before itLast!");
		Insert(pos, itFirst, sizeT(itLast - itFirst));
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(sizeT pos,
		const WideChar* itFirst, const WideChar* itLast)
	{
		HV_ASSERT_MSG(pos <= m_Length, "Position out of range!");
		HV_ASSERT_MSG(itFirst, "itFirst can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "itLast can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "itFirst must come before itLast!");
		Insert(pos, itFirst, sizeT(itLast - itFirst));
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(const CharT* it,
		const AnsiChar* itFirst, const AnsiChar* itLast)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Length, "Iterator out of range!");
		HV_ASSERT_MSG(itFirst, "itFirst can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "itLast can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "itFirst must come before itLast!");
		Insert(sizeT(it - m_Data), itFirst, sizeT(itLast - itFirst));
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(const CharT* it,
		const WideChar* itFirst, const WideChar* itLast)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Length, "Iterator out of range!");
		HV_ASSERT_MSG(itFirst, "itFirst can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "itLast can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "itFirst must come before itLast!");
		Insert(sizeT(it - m_Data), itFirst, sizeT(itLast - itFirst));
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename InputIterator>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(sizeT pos,
		const InputIterator& itFirst, const InputIterator& itLast)
	{
		HV_ASSERT_MSG(pos <= m_Length, "Position out of range!");
		HV_ASSERT_MSG(itFirst != itLast, "itFirst cannot be the same as itLast!");
		sizeT len = 0;
		for (InputIterator it = itFirst; it != itLast; ++it)
			++len;

		Reserve(m_Length + len);
		MoveData(pos, pos + len);
		CopyToData(pos, itFirst, len);
		m_Length += len;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename InputIterator>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(const CharT* it,
		const InputIterator& itFirst, const InputIterator& itLast)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Length, "Iterator out of range!");
		HV_ASSERT_MSG(itFirst != itLast, "itFirst cannot be the same as itLast!");
		sizeT len = 0;
		for (InputIterator itData = itFirst; itData != itLast; ++itData)
			++len;

		sizeT pos = sizeT(it - m_Data);
		Reserve(m_Length + len);
		MoveData(pos, pos + len);
		CopyToData(pos, itFirst, itLast);
		m_Length += len;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(sizeT pos,
		std::initializer_list<AnsiChar> il)
	{
		HV_ASSERT_MSG(pos <= m_Length, "Position out of range!");
		Insert(pos, il.begin(), il.size());
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(sizeT pos,
		std::initializer_list<WideChar> il)
	{
		HV_ASSERT_MSG(pos <= m_Length, "Position out of range!");
		Insert(pos, il.begin(), il.size());
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(const CharT* it,
		std::initializer_list<AnsiChar> il)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Length, "Iterator out of range!");
		Insert(sizeT(it - m_Data), il.begin(), il.size());
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(const CharT* it,
		std::initializer_list<WideChar> il)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Length, "Iterator out of range!");
		Insert(sizeT(it - m_Data), il.begin(), il.size());
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(sizeT pos,
		const TString<C, G>& str)
	{
		HV_ASSERT_MSG(pos <= m_Length, "Position out of range!");
		Insert(pos, str.m_Data, str.m_Length);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(const CharT* it,
		const TString<C, G>& str)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Length, "Iterator out of range!");
		Insert(sizeT(it - m_Data), str.m_Data, str.m_Length);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(sizeT pos,
		const TString<C, G>& str, sizeT len)
	{
		HV_ASSERT_MSG(pos <= m_Length, "Position out of range!");
		if (len > str.m_Length)
			len = str.m_Length;
		Insert(pos, str.m_Data, len);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(const CharT* it,
		const TString<C, G>& str, sizeT len)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Length, "Iterator out of range!");
		if (len > str.m_Length)
			len = str.m_Length;
		Insert(sizeT(it - m_Data), str.m_Data, len);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(sizeT pos,
		const TString<C, G>& str, sizeT subPos, sizeT len)
	{
		HV_ASSERT_MSG(pos <= m_Length, "Position out of range!");
		HV_ASSERT_MSG(subPos < str.m_Length, "subPos out of range!");
		if (len > str.m_Length - subPos)
			len = str.m_Length - subPos;
		Insert(pos, str.m_Data, len);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Insert(const CharT* it,
		const TString<C, G>& str, sizeT subPos, sizeT len)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Length, "Iterator out of range!");
		HV_ASSERT_MSG(subPos < str.m_Length, "subPos out of range!");
		if (len > str.m_Length - subPos)
			len = str.m_Length - subPos;
		Insert(sizeT(it - m_Data), str.m_Data, len);
		return *this;
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Erase and PopBack
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	CharT* TString<CharT, GrowthPolicy>::Erase(sizeT pos)
	{
		HV_ASSERT_MSG(pos < m_Length, "Position out of range!");
		MoveData(pos + 1, pos);
		--m_Length;
		m_Data[m_Length] = CharT('\0');
		return m_Data + pos;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	CharT* TString<CharT, GrowthPolicy>::Erase(const CharT* it)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Length, "Iterator out of range!");
		return Erase(sizeT(it - m_Data));
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	CharT* TString<CharT, GrowthPolicy>::Erase(sizeT pos, sizeT count)
	{
		HV_ASSERT_MSG(pos < m_Length, "Position out of range!");
		if (count == 0)
			return m_Data + pos;
		if (count > m_Length - pos)
			count = m_Length - pos;
		MoveData(pos + count, pos);
		m_Length -= count;
		Memory::Clear(m_Data + m_Length, count);
		return m_Data + pos;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	CharT* TString<CharT, GrowthPolicy>::Erase(const CharT* it, sizeT count)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Length, "Iterator out of range!");
		return Erase(sizeT(it - m_Data), count);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	CharT* TString<CharT, GrowthPolicy>::Erase(const CharT* itFirst, const CharT* itLast)
	{
		HV_ASSERT_MSG(itFirst >= m_Data && itFirst < m_Data + m_Length, "itFirst out of range!");
		HV_ASSERT_MSG(itLast >= m_Data && itLast < m_Data + m_Length, "itLast out of range!");
		HV_ASSERT_MSG(itFirst < itLast, "itFirst must come before itLast!");
		return Erase(sizeT(itFirst - m_Data), sizeT(itLast - itFirst));
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::PopBack()
	{
		if (m_Length > 0)
		{
			--m_Length;
			m_Data[m_Length] = CharT('\0');
		}
		return *this;
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Replace
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(sizeT pos, AnsiChar c)
	{
		HV_ASSERT_MSG(pos < m_Length, "Position out of range!");
		m_Data[pos] = CU::Convert(c);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(sizeT pos, WideChar c)
	{
		HV_ASSERT_MSG(pos < m_Length, "Position out of range!");
		m_Data[pos] = CU::Convert(c);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* it, AnsiChar c)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Length, "Iterator out of range!");
		sizeT pos = sizeT(it - m_Data);
		m_Data[pos] = CU::Convert(c);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* it, WideChar c)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Length, "Iterator out of range!");
		sizeT pos = sizeT(it - m_Data);
		m_Data[pos] = CU::Convert(c);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(sizeT pos, AnsiChar c,
		sizeT amount)
	{
		return Replace(pos, 1, c, 1);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(sizeT pos, WideChar c,
		sizeT amount)
	{
		return Replace(pos, 1, c, 1);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* it, AnsiChar c,
		sizeT amount)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Length, "Iterator out of range!");
		return Replace(sizeT(it - m_Data), 1, c, 1);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* it, WideChar c,
		sizeT amount)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Length, "Iterator out of range!");
		return Replace(sizeT(it - m_Data), 1, c, 1);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(sizeT pos,
		const AnsiChar* str)
	{
		return Replace(pos, 1, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(sizeT pos,
		const WideChar* str)
	{
		return Replace(pos, 1, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* it,
		const AnsiChar* str)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Length, "Iterator out of range!");
		return Replace(sizeT(it - m_Data), 1, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* it,
		const WideChar* str)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Length, "Iterator out of range!");
		return Replace(sizeT(it - m_Data), 1, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(sizeT pos,
		const TString<C, G>& str)
	{
		return Replace(pos, 1, str, 0, str.m_Length);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* it,
		const TString<C, G>& str)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Length, "Iterator out of range!");
		return Replace(sizeT(it - m_Data), 1, str, 0, str.m_Length);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(sizeT pos,
		const TString<C, G>& str, sizeT subPos, sizeT subLen)
	{
		return Replace(pos, 1, str, subPos, subLen);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* it,
		const TString<C, G>& str, sizeT subPos, sizeT subLen)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Length, "Iterator out of range!");
		return Replace(sizeT(it - m_Data), 1, str, subPos, subLen);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(sizeT pos, sizeT count,
		AnsiChar c)
	{
		return Replace(pos, count, c, 1);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(sizeT pos, sizeT count,
		WideChar c)
	{
		return Replace(pos, count, c, 1);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* it, sizeT count,
		AnsiChar c)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Length, "Iterator out of range!");
		return Replace(sizeT(it - m_Data), count, c, 1);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* it, sizeT count,
		WideChar c)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Length, "Iterator out of range!");
		return Replace(sizeT(it - m_Data), count, c, 1);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* itFirst,
		const CharT* itLast, AnsiChar c)
	{
		HV_ASSERT_MSG(itFirst >= m_Data && itFirst < m_Data + m_Length, "itFirst out of range!");
		HV_ASSERT_MSG(itLast >= m_Data && itLast < m_Data + m_Length, "itLast out of range!");
		HV_ASSERT_MSG(itFirst < itLast, "itFirst must come before itLast!");
		return Replace(sizeT(itFirst - m_Data), sizeT(itLast - itFirst), c, 1);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* itFirst,
		const CharT* itLast, WideChar c)
	{
		HV_ASSERT_MSG(itFirst >= m_Data && itFirst < m_Data + m_Length, "itFirst out of range!");
		HV_ASSERT_MSG(itLast >= m_Data && itLast < m_Data + m_Length, "itLast out of range!");
		HV_ASSERT_MSG(itFirst < itLast, "itFirst must come before itLast!");
		return Replace(sizeT(itFirst - m_Data), sizeT(itLast - itFirst), c, 1);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(sizeT pos, sizeT count,
		AnsiChar c, sizeT amount)
	{
		HV_ASSERT_MSG(pos < m_Length, "Position out of range!");
		if (count == 0)
			return *this;
		if (amount > 1)
			Reserve(m_Length + amount - 1);
		MoveData(pos + count, pos + amount);
		CopyToData(pos, c, amount);
		if (count > amount)
			m_Length -= count - amount;
		else
			m_Length += amount - count;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(sizeT pos, sizeT count,
		WideChar c, sizeT amount)
	{
		HV_ASSERT_MSG(pos < m_Length, "Position out of range!");
		if (count == 0)
			return *this;
		if (amount > 1)
			Reserve(m_Length + amount - 1);
		MoveData(pos + count, pos + amount);
		CopyToData(pos, c, amount);
		if (count > amount)
			m_Length -= count - amount;
		else
			m_Length += amount - count;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* it, sizeT count,
		AnsiChar c, sizeT amount)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Length, "Iterator out of range!");
		return Replace(sizeT(it - m_Data), count, c, amount);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* it, sizeT count,
		WideChar c, sizeT amount)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Length, "Iterator out of range!");;
		return Replace(sizeT(it - m_Data), count, c, amount);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* itFirst,
		const CharT* itLast, AnsiChar c, sizeT amount)
	{
		HV_ASSERT_MSG(itFirst >= m_Data && itFirst < m_Data + m_Length, "itFirst out of range!");
		HV_ASSERT_MSG(itLast >= m_Data && itLast < m_Data + m_Length, "itLast out of range!");
		HV_ASSERT_MSG(itFirst < itLast, "itFirst must come before itLast!");
		return Replace(sizeT(itLast - itFirst), sizeT(itFirst - m_Data), c, amount);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* itFirst,
		const CharT* itLast, WideChar c, sizeT amount)
	{
		HV_ASSERT_MSG(itFirst >= m_Data && itFirst < m_Data + m_Length, "itFirst out of range!");
		HV_ASSERT_MSG(itLast >= m_Data && itLast < m_Data + m_Length, "itLast out of range!");
		HV_ASSERT_MSG(itFirst < itLast, "itFirst must come before itLast!");
		return Replace(sizeT(itFirst - m_Data), sizeT(itLast - itFirst), c, amount);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(sizeT pos, sizeT count,
		const AnsiChar* str)
	{
		HV_ASSERT_MSG(pos < m_Length, "Position out of range!");
		if (count == 0)
			return *this;
		sizeT len = SU::CStringLength(str);
		if (len > 1)
			Reserve(m_Length + len - 1);
		MoveData(pos + count, pos + len);
		CopyToData(pos, str, len);
		if (count > len)
			m_Length -= count - len;
		else
			m_Length += len - count;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(sizeT pos, sizeT count,
		const WideChar* str)
	{
		HV_ASSERT_MSG(pos < m_Length, "Position out of range!");
		if (count == 0)
			return *this;
		sizeT len = SU::CStringLength(str);
		if (len > 1)
			Reserve(m_Length + len - 1);
		MoveData(pos + count, pos + len);
		CopyToData(pos, str, len);
		if (count > len)
			m_Length -= count - len;
		else
			m_Length += len - count;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* it, sizeT count,
		const AnsiChar* str)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Length, "Iterator out of range!");
		return Replace(sizeT(it - m_Data), count, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* it, sizeT count,
		const WideChar* str)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Length, "Iterator out of range!");
		return Replace(sizeT(it - m_Data), count, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* itFirst,
		const CharT* itLast, const AnsiChar* str)
	{
		HV_ASSERT_MSG(itFirst >= m_Data && itFirst < m_Data + m_Length, "itFirst out of range!");
		HV_ASSERT_MSG(itLast >= m_Data && itLast < m_Data + m_Length, "itLast out of range!");
		HV_ASSERT_MSG(itFirst < itLast, "itFirst must come before itLast!");
		return Replace(sizeT(itFirst - m_Data), sizeT(itLast - itFirst), str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* itFirst,
		const CharT* itLast, const WideChar* str)
	{
		HV_ASSERT_MSG(itFirst >= m_Data && itFirst < m_Data + m_Length, "itFirst out of range!");
		HV_ASSERT_MSG(itLast >= m_Data && itLast < m_Data + m_Length, "itLast out of range!");
		HV_ASSERT_MSG(itFirst < itLast, "itFirst must come before itLast!");
		return Replace(sizeT(itFirst - m_Data), sizeT(itLast - itFirst), str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(sizeT pos, sizeT count,
		const TString<C, G>& str)
	{
		return Replace(pos, count, str, 0, str.m_Length);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* it, sizeT count,
		const TString<C, G>& str)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Length, "Iterator out of range!");
		return Replace(sizeT(it - m_Data), count, str, 0, str.m_Length);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* itFirst,
		const CharT* itLast, const TString<C, G>& str)
	{
		HV_ASSERT_MSG(itFirst >= m_Data && itFirst < m_Data + m_Length, "itFirst out of range!");
		HV_ASSERT_MSG(itLast >= m_Data && itLast < m_Data + m_Length, "itLast out of range!");
		HV_ASSERT_MSG(itFirst < itLast, "itFirst must come before itLast!");
		return Replace(sizeT(itFirst - m_Data), sizeT(itLast - itFirst), str, 0, str.m_Length);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(sizeT pos, sizeT count,
		const TString<C, G>& str, sizeT subPos, sizeT subLen)
	{
		HV_ASSERT_MSG(pos < m_Length, "Position out of range!");
		HV_ASSERT_MSG(subPos < str.m_Length, "subPos out of range!");
		if (subLen > str.m_Length - subPos)
			subLen = str.m_Length - subPos;
		if (subLen > 1)
			Reserve(m_Length + subLen - 1);
		MoveData(pos + count, pos + subLen);
		CopyToData(pos, str.m_Data + subPos, subLen);
		if (count > subLen)
			m_Length -= count - subLen;
		else
			m_Length += subLen - count;
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* it, sizeT count,
		const TString<C, G>& str, sizeT subPos, sizeT subLen)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Length, "Iterator out of range!");
		return Replace(sizeT(it - m_Data), count, str, subPos, subLen);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Replace(const CharT* itFirst,
		const CharT* itLast, const TString<C, G>& str, sizeT subPos, sizeT subLen)
	{
		HV_ASSERT_MSG(itFirst >= m_Data && itFirst < m_Data + m_Length, "itFirst out of range!");
		HV_ASSERT_MSG(itLast >= m_Data && itLast < m_Data + m_Length, "itLast out of range!");
		HV_ASSERT_MSG(itFirst < itLast, "itFirst must come before itLast!");
		return Replace(sizeT(itFirst - m_Data), sizeT(itLast - itFirst), str, subPos, subLen);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	ReplaceChar
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(AnsiChar c0, AnsiChar c1)
	{
		if (c0 == c1)
			return *this;
		CharT ch = CU::Convert(c1);
		sizeT pos = Find(c0);
		while (pos != NPos)
		{
			m_Data[pos] = ch;
			pos = Find(c0, pos + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(AnsiChar c0, WideChar c1)
	{
		if (c0 == c1)
			return *this;
		CharT ch = CU::Convert(c1);
		sizeT pos = Find(c0);
		while (pos != NPos)
		{
			m_Data[pos] = ch;
			pos = Find(c0, pos + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(WideChar c0, AnsiChar c1)
	{
		if (c0 == c1)
			return *this;
		CharT ch = CU::Convert(c1);
		sizeT pos = Find(c0);
		while (pos != NPos)
		{
			m_Data[pos] = ch;
			pos = Find(c0, pos + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(WideChar c0, WideChar c1)
	{
		if (c0 == c1)
			return *this;
		CharT ch = CU::Convert(c1);
		sizeT pos = Find(c0);
		while (pos != NPos)
		{
			m_Data[pos] = ch;
			pos = Find(c0, pos + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(const AnsiChar* chars,
		sizeT count, AnsiChar c)
	{
		CharT ch = CU::Convert(c);
		sizeT pos = FindAny(chars, count, sizeT(0));
		while (pos != NPos)
		{
			m_Data[pos] = ch;
			pos = FindAny(chars, count, pos + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(const AnsiChar* chars,
		sizeT count, WideChar c)
	{
		CharT ch = CU::Convert(c);
		sizeT pos = FindAny(chars, count, sizeT(0));
		while (pos != NPos)
		{
			m_Data[pos] = ch;
			pos = FindAny(chars, count, pos + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(const WideChar* chars,
		sizeT count, AnsiChar c)
	{
		CharT ch = CU::Convert(c);
		sizeT pos = FindAny(chars, count, sizeT(0));
		while (pos != NPos)
		{
			m_Data[pos] = ch;
			pos = FindAny(chars, count, pos + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(const WideChar* chars,
		sizeT count, WideChar c)
	{
		CharT ch = CU::Convert(c);
		sizeT pos = FindAny(chars, count, sizeT(0));
		while (pos != NPos)
		{
			m_Data[pos] = ch;
			pos = FindAny(chars, count, pos + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(
		const AnsiChar(&chars)[N], AnsiChar c)
	{
		return ReplaceChar(chars, N, c);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(
		const AnsiChar(&chars)[N], WideChar c)
	{
		return ReplaceChar(chars, N, c);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(
		const WideChar(&chars)[N], AnsiChar c)
	{
		return ReplaceChar(chars, N, c);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(
		const WideChar(&chars)[N], WideChar c)
	{
		return ReplaceChar(chars, N, c);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(AnsiChar c0, AnsiChar c1,
		sizeT amount)
	{
		sizeT pos = Find(c0);
		while (pos != NPos)
		{
			Replace(pos, c1, amount);
			pos = Find(c0, pos + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(AnsiChar c0, WideChar c1,
		sizeT amount)
	{
		sizeT pos = Find(c0);
		while (pos != NPos)
		{
			Replace(pos, c1, amount);
			pos = Find(c0, pos + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(WideChar c0, AnsiChar c1,
		sizeT amount)
	{
		sizeT pos = Find(c0);
		while (pos != NPos)
		{
			Replace(pos, c1, amount);
			pos = Find(c0, pos + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(WideChar c0, WideChar c1,
		sizeT amount)
	{
		sizeT pos = Find(c0);
		while (pos != NPos)
		{
			Replace(pos, c1, amount);
			pos = Find(c0, pos + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(const AnsiChar* chars,
		sizeT count, AnsiChar c, sizeT amount)
	{
		sizeT pos = FindAny(chars, count, sizeT(0));
		while (pos != NPos)
		{
			Replace(pos, c, amount);
			pos = FindAny(chars, count, pos + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(const AnsiChar* chars,
		sizeT count, WideChar c, sizeT amount)
	{
		sizeT pos = FindAny(chars, count, sizeT(0));
		while (pos != NPos)
		{
			Replace(pos, c, amount);
			pos = FindAny(chars, count, pos + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(const WideChar* chars,
		sizeT count, AnsiChar c, sizeT amount)
	{
		sizeT pos = FindAny(chars, count, sizeT(0));
		while (pos != NPos)
		{
			Replace(pos, c, amount);
			pos = FindAny(chars, count, pos + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(const WideChar* chars,
		sizeT count, WideChar c, sizeT amount)
	{
		sizeT pos = FindAny(chars, count, sizeT(0));
		while (pos != NPos)
		{
			Replace(pos, c, amount);
			pos = FindAny(chars, count, pos + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(
		const AnsiChar(&chars)[N], AnsiChar c, sizeT amount)
	{
		return ReplaceChar(chars, N, c, amount);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(
		const AnsiChar(&chars)[N], WideChar c, sizeT amount)
	{
		return ReplaceChar(chars, N, c, amount);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(
		const WideChar(&chars)[N], AnsiChar c, sizeT amount)
	{
		return ReplaceChar(chars, N, c, amount);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(
		const WideChar(&chars)[N], WideChar c, sizeT amount)
	{
		return ReplaceChar(chars, N, c, amount);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(AnsiChar c,
		const AnsiChar* str)
	{
		sizeT len = SU::CStringLength(str);
		sizeT pos = Find(c);
		while (pos != NPos)
		{
			Replace(pos, str);
			pos = Find(c, pos + len);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(AnsiChar c,
		const WideChar* str)
	{
		sizeT len = SU::CStringLength(str);
		sizeT pos = Find(c);
		while (pos != NPos)
		{
			Replace(pos, str);
			pos = Find(c, pos + len);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(WideChar c,
		const AnsiChar* str)
	{
		sizeT len = SU::CStringLength(str);
		sizeT pos = Find(c);
		while (pos != NPos)
		{
			Replace(pos, str);
			pos = Find(c, pos + len);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(WideChar c,
		const WideChar* str)
	{
		sizeT len = SU::CStringLength(str);
		sizeT pos = Find(c);
		while (pos != NPos)
		{
			Replace(pos, str);
			pos = Find(c, pos + len);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(const AnsiChar* chars,
		sizeT count, const AnsiChar* str)
	{
		sizeT len = SU::CStringLength(str);
		sizeT pos = FindAny(chars, count, sizeT(0));
		while (pos != NPos)
		{
			Replace(pos, str);
			pos = FindAny(chars, count, pos + len);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(const AnsiChar* chars,
		sizeT count, const WideChar* str)
	{
		sizeT pos = FindAny(chars, count, sizeT(0));
		while (pos != NPos)
		{
			Replace(pos, str);
			pos = FindAny(chars, count, pos + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(const WideChar* chars,
		sizeT count, const AnsiChar* str)
	{
		sizeT len = SU::CStringLength(str);
		sizeT pos = FindAny(chars, count, sizeT(0));
		while (pos != NPos)
		{
			Replace(pos, str);
			pos = FindAny(chars, count, pos + len);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(const WideChar* chars,
		sizeT count, const WideChar* str)
	{
		sizeT len = SU::CStringLength(str);
		sizeT pos = FindAny(chars, count, sizeT(0));
		while (pos != NPos)
		{
			Replace(pos, str);
			pos = FindAny(chars, count, pos + len);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(
		const AnsiChar(&chars)[N], const AnsiChar* str)
	{
		return ReplaceChar(chars, N, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(
		const AnsiChar(&chars)[N], const WideChar* str)
	{
		return ReplaceChar(chars, N, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(
		const WideChar(&chars)[N], const AnsiChar* str)
	{
		return ReplaceChar(chars, N, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(
		const WideChar(&chars)[N], const WideChar* str)
	{
		return ReplaceChar(chars, N, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(AnsiChar c,
		const TString<C, G>& str)
	{
		sizeT pos = Find(c);
		while (pos != NPos)
		{
			Replace(pos, str);
			pos = Find(c, pos + str.m_Length);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(WideChar c,
		const TString<C, G>& str)
	{
		sizeT pos = Find(c);
		while (pos != NPos)
		{
			Replace(pos, str);
			pos = Find(c, pos + str.m_Length);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(const AnsiChar* chars,
		sizeT count, const TString<C, G>& str)
	{
		sizeT pos = FindAny(chars, count, sizeT(0));
		while (pos != NPos)
		{
			Replace(pos, str);
			pos = FindAny(chars, count, pos + str.m_Length);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(const WideChar* chars,
		sizeT count, const TString<C, G>& str)
	{
		sizeT pos = FindAny(chars, count, sizeT(0));
		while (pos != NPos)
		{
			Replace(pos, str);
			pos = FindAny(chars, count, pos + str.m_Length);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, sizeT N, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(
		const AnsiChar(&chars)[N], const TString<C, G>& str)
	{
		return ReplaceChar(chars, N, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, sizeT N, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceChar(
		const WideChar(&chars)[N], const TString<C, G>& str)
	{
		return ReplaceChar(chars, N, str);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	ReplaceStr
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const AnsiChar* str,
		AnsiChar c)
	{
		sizeT len = SU::CStringLength(str);
		sizeT loc = Find(str);
		while (loc == NPos)
		{
			Replace(loc, len, c);
			loc = Find(str, loc + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const AnsiChar* str,
		WideChar c)
	{
		sizeT len = SU::CStringLength(str);
		sizeT loc = Find(str);
		while (loc == NPos)
		{
			Replace(loc, len, c);
			loc = Find(str, loc + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const WideChar* str,
		AnsiChar c)
	{
		sizeT len = SU::CStringLength(str);
		sizeT loc = Find(str);
		while (loc == NPos)
		{
			Replace(loc, len, c);
			loc = Find(str, loc + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const WideChar* str,
		WideChar c)
	{
		sizeT len = SU::CStringLength(str);
		sizeT loc = Find(str);
		while (loc == NPos)
		{
			Replace(loc, len, c);
			loc = Find(str, loc + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const AnsiChar** strings,
		sizeT count, AnsiChar c)
	{
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, c);
			pair = FindAnyWithLength(strings, count, pair.first + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const AnsiChar** strings,
		sizeT count, WideChar c)
	{
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, c);
			pair = FindAnyWithLength(strings, count, pair.first + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const WideChar** strings,
		sizeT count, AnsiChar c)
	{
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, c);
			pair = FindAnyWithLength(strings, count, pair.first + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const WideChar** strings,
		sizeT count, WideChar c)
	{
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, c);
			pair = FindAnyWithLength(strings, count, pair.first + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const AnsiChar*(&strings)[N], AnsiChar c)
	{
		return ReplaceStr(strings, N, c);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const AnsiChar*(&strings)[N], WideChar c)
	{
		return ReplaceStr(strings, N, c);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const WideChar*(&strings)[N], AnsiChar c)
	{
		return ReplaceStr(strings, N, c);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const WideChar*(&strings)[N], WideChar c)
	{
		return ReplaceStr(strings, N, c);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const TString<C, G>& str, AnsiChar c)
	{
		sizeT loc = Find(str);
		while (loc == NPos)
		{
			Replace(loc, str.m_Length, c);
			loc = Find(str, loc + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const TString<C, G>& str, WideChar c)
	{
		sizeT loc = Find(str);
		while (loc == NPos)
		{
			Replace(loc, str.m_Length, c);
			loc = Find(str, loc + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const TString<C, G>* strings, sizeT count, AnsiChar c)
	{
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, c);
			pair = FindAnyWithLength(strings, count, pair.first + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const TString<C, G>* strings, sizeT count, WideChar c)
	{
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, c);
			pair = FindAnyWithLength(strings, count, pair.first + 1);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, sizeT N, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const TString<C, G>(&strings)[N], AnsiChar c)
	{
		return ReplaceStr(strings, N, c);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, sizeT N, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const TString<C, G>(&strings)[N], WideChar c)
	{
		return ReplaceStr(strings, N, c);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const AnsiChar* str,
		AnsiChar c, sizeT amount)
	{
		sizeT len = SU::CStringLength(str);
		sizeT loc = Find(str);
		while (loc == NPos)
		{
			Replace(loc, len, c, amount);
			loc = Find(str, loc + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const AnsiChar* str,
		WideChar c, sizeT amount)
	{
		sizeT len = SU::CStringLength(str);
		sizeT loc = Find(str);
		while (loc == NPos)
		{
			Replace(loc, len, c, amount);
			loc = Find(str, loc + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const WideChar* str,
		AnsiChar c, sizeT amount)
	{
		sizeT len = SU::CStringLength(str);
		sizeT loc = Find(str);
		while (loc == NPos)
		{
			Replace(loc, len, c, amount);
			loc = Find(str, loc + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const WideChar* str,
		WideChar c, sizeT amount)
	{
		sizeT len = SU::CStringLength(str);
		sizeT loc = Find(str);
		while (loc == NPos)
		{
			Replace(loc, len, c, amount);
			loc = Find(str, loc + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const AnsiChar** strings,
		sizeT count, AnsiChar c, sizeT amount)
	{
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, c, amount);
			pair = FindAnyWithLength(strings, count, pair.first + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const AnsiChar** strings,
		sizeT count, WideChar c, sizeT amount)
	{
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, c, amount);
			pair = FindAnyWithLength(strings, count, pair.first + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const WideChar** strings,
		sizeT count, AnsiChar c, sizeT amount)
	{
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, c, amount);
			pair = FindAnyWithLength(strings, count, pair.first + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const WideChar** strings,
		sizeT count, WideChar c, sizeT amount)
	{
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, c, amount);
			pair = FindAnyWithLength(strings, count, pair.first + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const AnsiChar*(&strings)[N], AnsiChar c, sizeT amount)
	{
		return ReplaceStr(strings, N, c, amount);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const AnsiChar*(&strings)[N], WideChar c, sizeT amount)
	{
		return ReplaceStr(strings, N, c, amount);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const WideChar*(&strings)[N], AnsiChar c, sizeT amount)
	{
		return ReplaceStr(strings, N, c, amount);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const WideChar*(&strings)[N], WideChar c, sizeT amount)
	{
		return ReplaceStr(strings, N, c, amount);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const TString<C, G>& str, AnsiChar c, sizeT amount)
	{
		sizeT loc = Find(str);
		while (loc == NPos)
		{
			Replace(loc, str.m_Length, c, amount);
			loc = Find(str, loc + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const TString<C, G>& str, WideChar c, sizeT amount)
	{
		sizeT loc = Find(str);
		while (loc == NPos)
		{
			Replace(loc, str.m_Length, c, amount);
			loc = Find(str, loc + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const TString<C, G>* strings, sizeT count, AnsiChar c, sizeT amount)
	{
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, 0);
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, c, amount);
			pair = FindAnyWithLength(strings, count, pair.first + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const TString<C, G>* strings, sizeT count, WideChar c, sizeT amount)
	{
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, c, amount);
			pair = FindAnyWithLength(strings, count, pair.first + amount);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, sizeT N, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const TString<C, G>(&strings)[N], AnsiChar c, sizeT amount)
	{
		return ReplaceStr(strings, N, c, amount);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, sizeT N, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const TString<C, G>(&strings)[N], WideChar c, sizeT amount)
	{
		return ReplaceStr(strings, N, c, amount);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const AnsiChar* str0,
		const AnsiChar* str1)
	{
		sizeT len = SU::CStringLength(str0);
		sizeT rlen = SU::CStringLength(str1);
		sizeT loc = Find(str0);
		while (loc == NPos)
		{
			Replace(loc, len, str1);
			loc = Find(str0, loc + rlen);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const AnsiChar* str0,
		const WideChar* str1)
	{
		sizeT len = SU::CStringLength(str0);
		sizeT rlen = SU::CStringLength(str1);
		sizeT loc = Find(str0);
		while (loc == NPos)
		{
			Replace(loc, len, str1);
			loc = Find(str0, loc + rlen);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const WideChar* str0,
		const AnsiChar* str1)
	{
		sizeT len = SU::CStringLength(str0);
		sizeT rlen = SU::CStringLength(str1);
		sizeT loc = Find(str0);
		while (loc == NPos)
		{
			Replace(loc, len, str1);
			loc = Find(str0, loc + rlen);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const WideChar* str0,
		const WideChar* str1)
	{
		sizeT len = SU::CStringLength(str0);
		sizeT rlen = SU::CStringLength(str1);
		sizeT loc = Find(str0);
		while (loc == NPos)
		{
			Replace(loc, len, str1);
			loc = Find(str0, loc + rlen);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const AnsiChar** strings,
		sizeT count, const AnsiChar* str)
	{
		sizeT rlen = SU::CStringLength(str);
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, str);
			pair = FindAnyWithLength(strings, count, pair.first + rlen);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const AnsiChar** strings,
		sizeT count, const WideChar* str)
	{
		sizeT rlen = SU::CStringLength(str);
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, str);
			pair = FindAnyWithLength(strings, count, pair.first + rlen);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const WideChar** strings,
		sizeT count, const AnsiChar* str)
	{
		sizeT rlen = SU::CStringLength(str);
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, str);
			pair = FindAnyWithLength(strings, count, pair.first + rlen);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const WideChar** strings,
		sizeT count, const WideChar* str)
	{
		sizeT rlen = SU::CStringLength(str);
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, str);
			pair = FindAnyWithLength(strings, count, pair.first + rlen);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const AnsiChar*(&strings)[N], const AnsiChar* str)
	{
		return ReplaceStr(strings, N, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const AnsiChar*(&strings)[N], const WideChar* str)
	{
		return ReplaceStr(strings, N, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const WideChar*(&strings)[N], const AnsiChar* str)
	{
		return ReplaceStr(strings, N, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const WideChar*(&strings)[N], const WideChar* str)
	{
		return ReplaceStr(strings, N, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const TString<C, G>& str0, const AnsiChar* str1)
	{
		sizeT rlen = SU::CStringLength(str1);
		sizeT loc = Find(str0);
		while (loc == NPos)
		{
			Replace(loc, str0.m_Length, str1);
			loc = Find(str0, loc + rlen);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const TString<C, G>& str0, const WideChar* str1)
	{
		sizeT rlen = SU::CStringLength(str1);
		sizeT loc = Find(str0);
		while (loc == NPos)
		{
			Replace(loc, str0.m_Length, str1);
			loc = Find(str0, loc + rlen);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const TString<C, G>* strings, sizeT count, const AnsiChar* str)
	{
		sizeT rlen = SU::CStringLength(str);
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, str);
			pair = FindAnyWithLength(strings, count, pair.first + rlen);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const TString<C, G>* strings, sizeT count, const WideChar* str)
	{
		sizeT rlen = SU::CStringLength(str);
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, str);
			pair = FindAnyWithLength(strings, count, pair.first + rlen);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, sizeT N, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const TString<C, G>(&strings)[N], const AnsiChar* str)
	{
		return ReplaceStr(strings, N, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, sizeT N, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const TString<C, G>(&strings)[N], const WideChar* str)
	{
		return ReplaceStr(strings, N, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const AnsiChar* str0,
		const TString<C, G>& str1)
	{
		sizeT len = SU::CStringLength(str0);
		sizeT loc = Find(str0);
		while (loc == NPos)
		{
			Replace(loc, len, str1);
			loc = Find(str0, loc + str1.m_Length);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const WideChar* str0,
		const TString<C, G>& str1)
	{
		sizeT len = SU::CStringLength(str0);
		sizeT loc = Find(str0);
		while (loc == NPos)
		{
			Replace(loc, len, str1);
			loc = Find(str0, loc + str1.m_Length);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const AnsiChar** strings,
		sizeT count, const TString<C, G>& str)
	{
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, str);
			pair = FindAnyWithLength(strings, count, pair.first + str.m_Length);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const WideChar** strings,
		sizeT count, const TString<C, G>& str)
	{
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, str);
			pair = FindAnyWithLength(strings, count, pair.first + str.m_Length);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N, typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const AnsiChar*(&strings)[N], const TString<C, G>& str)
	{
		return ReplaceStr(strings, N, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N, typename C, Container::GrowthPolicyFunc G>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(
		const WideChar*(&strings)[N], const TString<C, G>& str)
	{
		return ReplaceStr(strings, N, str);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C0, typename C1, Container::GrowthPolicyFunc G0, Container::GrowthPolicyFunc G1>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const TString<C0, G0>& str0, const TString<C1, G1>& str1)
	{
		sizeT loc = Find(str0);
		while (loc == NPos)
		{
			Replace(loc, str0.m_Length, str1);
			loc = Find(str0, loc + str1.m_Length);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C0, typename C1, Container::GrowthPolicyFunc G0, Container::GrowthPolicyFunc G1>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const TString<C0, G0>* strings, sizeT count, const TString<C1, G1>& str)
	{
		Pair<sizeT, sizeT> pair = FindAnyWithLength(strings, count, sizeT(0));
		while (pair.first == NPos)
		{
			Replace(pair.first, pair.second, str);
			pair = FindAnyWithLength(strings, count, pair.first + str.m_Length);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C0, typename C1, sizeT N, Container::GrowthPolicyFunc G0, Container::GrowthPolicyFunc G1>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ReplaceStr(const TString<C0, G0>(&strings)[N], const TString<C1, G1>& str)
	{
		return ReplaceStr(strings, N, str);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Find
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::Find(AnsiChar c, sizeT pos, b8 caseSensitive) const
	{
		if (pos >= m_Length)
			return NPos;
		if (caseSensitive)
		{
			for (sizeT i = pos; i < m_Length; ++i)
			{
				if (m_Data[i] == c)
					return i;
			}
		}
		else
		{
			AnsiChar ch = CU::ToLower(c);
			for (sizeT i = pos; i < m_Length; ++i)
			{
				if (CU::ToLower(m_Data[i]) == ch)
					return i;
			}
		}
		return NPos;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::Find(WideChar c, sizeT pos, b8 caseSensitive) const
	{
		if (pos >= m_Length)
			return NPos;
		if (caseSensitive)
		{
			for (sizeT i = pos; i < m_Length; ++i)
			{
				if (m_Data[i] == c)
					return i;
			}
		}
		else
		{
			WideChar ch = CU::ToLower(c);
			for (sizeT i = pos; i < m_Length; ++i)
			{
				if (CU::ToLower(m_Data[i]) == ch)
					return i;
			}
		}
		return NPos;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::Find(const AnsiChar* str, sizeT pos, b8 caseSensitive) const
	{
		return Find(str, SU::CStringLength(str), pos, m_Length - pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::Find(const WideChar* str, sizeT pos, b8 caseSensitive) const
	{
		return Find(str, SU::CStringLength(str), pos, m_Length - pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::Find(const AnsiChar* str, sizeT pos, sizeT count, b8 caseSensitive) const
	{
		return Find(str, SU::CStringLength(str), pos, count, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::Find(const WideChar* str, sizeT pos, sizeT count, b8 caseSensitive) const
	{
		return Find(str, SU::CStringLength(str), pos, count, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::Find(const AnsiChar* str, sizeT len, sizeT pos, sizeT count,
		b8 caseSensitive) const
	{
		if (len == NPos)
			len = SU::CStringLength(str);
		if (len == 0)
			return pos;
		if (pos + len >= m_Length)
			return NPos;
		if (count > m_Length - pos)
			count = m_Length - pos;
		sizeT end = m_Length - len + 1;
		if (end > pos + count)
			end = pos + count;
		AnsiChar fc = str[0];
		if (caseSensitive)
		{
			for (sizeT i = pos; i < end; ++i)
			{
				if (m_Data[i] != fc)
					continue;
				if (len == 1)
					return i;
				for (sizeT j = 1; j < len;)
				{
					if (m_Data[i + j] != str[j])
						break;
					++j;
					if (j == len)
						return i;
				}
			}
		}
		else
		{
			fc = CU::ToLower(fc);
			for (sizeT i = pos; i < end; ++i)
			{
				if (m_Data[i] != fc)
					continue;
				if (len == 1)
					return i;
				for (sizeT j = 1; j < len;)
				{
					if (CU::ToLower(m_Data[i + j]) != CU::ToLower(str[j]))
						break;
					++j;
					if (j == len)
						return i;
				}
			}
		}
		return NPos;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::Find(const WideChar* str, sizeT len, sizeT pos, sizeT count,
		b8 caseSensitive) const
	{
		if (len == NPos)
			len = SU::CStringLength(str);
		if (len == 0)
			return pos;
		if (pos + len >= m_Length)
			return NPos;
		if (count > m_Length - pos)
			count = m_Length - pos;
		sizeT end = m_Length - len + 1;
		if (end > pos + count)
			end = pos + count;
		WideChar fc = str[0];
		if (caseSensitive)
		{
			for (sizeT i = pos; i < end; ++i)
			{
				if (m_Data[i] != fc)
					continue;
				if (len == 1)
					return i;
				for (sizeT j = 1; j < len;)
				{
					if (m_Data[i + j] != str[j])
						break;
					++j;
					if (j == len)
						return i;
				}
			}
		}
		else
		{
			fc = CU::ToLower(fc);
			for (sizeT i = pos; i < end; ++i)
			{
				if (m_Data[i] != fc)
					continue;
				if (len == 1)
					return i;
				for (sizeT j = 1; j < len;)
				{
					if (CU::ToLower(m_Data[i + j]) != CU::ToLower(str[j]))
						break;
					++j;
					if (j == len)
						return i;
				}
			}
		}
		return NPos;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	sizeT TString<CharT, GrowthPolicy>::Find(const TString<C, G>& str, sizeT pos, b8 caseSensitive) const
	{
		return Find(str.m_Data, str.m_Length, pos, m_Length - pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	sizeT TString<CharT, GrowthPolicy>::Find(const TString<C, G>& str, sizeT pos, sizeT count,
		b8 caseSensitive) const
	{
		return Find(str.m_Data, str.m_Length, pos, count, caseSensitive);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	FindAny
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::FindAny(const AnsiChar* arr, sizeT count, sizeT pos,
		b8 caseSensitive) const
	{
		if (pos >= m_Length || count == 0)
			return NPos;
		if (caseSensitive)
		{
			for (sizeT i = pos; i < m_Length; ++i)
			{
				for (sizeT j = 0; j < count; ++j)
				{
					if (m_Data[i] == arr[j])
						return i;
				}
			}
		}
		else
		{
			for (sizeT i = pos; i < m_Length; ++i)
			{
				for (sizeT j = 0; j < count; ++j)
				{
					if (CU::ToLower(m_Data[i]) == CU::ToLower(arr[j]))
						return i;
				}
			}
		}
		return NPos;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::FindAny(const WideChar* arr, sizeT count, sizeT pos,
		b8 caseSensitive) const
	{
		if (pos >= m_Length || count == 0)
			return NPos;
		if (caseSensitive)
		{
			for (sizeT i = pos; i < m_Length; ++i)
			{
				for (sizeT j = 0; j < count; ++j)
				{
					if (m_Data[i] == arr[j])
						return i;
				}
			}
		}
		else
		{
			for (sizeT i = pos; i < m_Length; ++i)
			{
				for (sizeT j = 0; j < count; ++j)
				{
					if (CU::ToLower(m_Data[i]) == CU::ToLower(arr[j]))
						return i;
				}
			}
		}
		return NPos;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::FindAny(const AnsiChar* str, sizeT pos, b8 caseSensitive) const
	{
		return FindAny(str, SU::CStringLength(str), pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::FindAny(const WideChar* str, sizeT pos, b8 caseSensitive) const
	{
		return FindAny(str, SU::CStringLength(str), pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::FindAny(const AnsiChar** arr, sizeT count, sizeT pos,
		b8 caseSensitive) const
	{

		if (count == 0)
			return NPos;
		sizeT idx = NPos;
		sizeT searchCount = m_Length - pos;
		for (sizeT i = 0; i < count; ++i)
		{
			const AnsiChar* str = arr[i];
			sizeT len = SU::CStringLength(str);
			sizeT tmp = Find(str, len, pos, searchCount, caseSensitive);
			if (tmp != NPos && (idx == NPos || tmp < idx))
				idx = tmp;
		}
		return idx;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::FindAny(const WideChar** arr, sizeT count, sizeT pos,
		b8 caseSensitive) const
	{
		if (count == 0)
			return NPos;
		sizeT idx = NPos;
		sizeT searchCount = m_Length - pos;
		for (sizeT i = 0; i < count; ++i)
		{
			const WideChar* str = arr[i];
			sizeT len = SU::CStringLength(str);
			sizeT tmp = Find(str, len, pos, searchCount, caseSensitive);
			if (tmp != NPos && (idx == NPos || tmp < idx))
				idx = tmp;
		}
		return idx;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	sizeT TString<CharT, GrowthPolicy>::FindAny(const AnsiChar*(&arr)[N], sizeT pos, b8 caseSensitive) const
	{
		return FindAny(arr, N, pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	sizeT TString<CharT, GrowthPolicy>::FindAny(const WideChar*(&arr)[N], sizeT pos, b8 caseSensitive) const
	{
		return FindAny(arr, N, pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	sizeT TString<CharT, GrowthPolicy>::FindAny(const TString<C, G>& str, sizeT pos, b8 caseSensitive) const
	{
		return FindAny(str.m_Data, str.m_Length, pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	sizeT TString<CharT, GrowthPolicy>::FindAny(const TString<C, G>* arr, sizeT count, sizeT pos,
		b8 caseSensitive) const
	{
		if (count == 0)
			return NPos;
		sizeT idx = NPos;
		sizeT searchCount = m_Length - pos;
		for (sizeT i = 0; i < count; ++i)
		{
			const TString<C, G>& str = arr[i];
			sizeT tmp = Find(str.m_Data, str.m_Length, pos, searchCount, caseSensitive);
			if (tmp != NPos && (idx == NPos || tmp < idx))
				idx = tmp;
		}
		return idx;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G, sizeT N>
	sizeT TString<CharT, GrowthPolicy>::FindAny(const TString<C, G>(&arr)[N], sizeT pos,
		b8 caseSensitive) const
	{
		return FindAny(arr, N, pos, caseSensitive);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	FindAnyWithLength
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	Pair<sizeT, sizeT> TString<CharT, GrowthPolicy>::FindAnyWithLength(const AnsiChar** arr, sizeT count,
		sizeT pos, b8 caseSensitive) const
	{
		if (count == 0)
			return Pair<sizeT, sizeT>(NPos, 0);
		Pair<sizeT, sizeT> pair(NPos, 0);
		sizeT searchCount = m_Length - pos;
		for (sizeT i = 0; i < count; ++i)
		{
			const AnsiChar* str = arr[i];
			sizeT len = SU::CStringLength(str);
			sizeT tmp = Find(str, len, pos, searchCount, caseSensitive);
			if (tmp != NPos && (pair.first == NPos || tmp < pair.first))
			{
				pair.first = tmp;
				pair.second = len;
			}
		}
		return pair;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	Pair<sizeT, sizeT> TString<CharT, GrowthPolicy>::FindAnyWithLength(const WideChar** arr, sizeT count,
		sizeT pos, b8 caseSensitive) const
	{
		if (count == 0)
			return Pair<sizeT, sizeT>(NPos, 0);
		Pair<sizeT, sizeT> pair(NPos, 0);
		sizeT searchCount = m_Length - pos;
		for (sizeT i = 0; i < count; ++i)
		{
			const WideChar* str = arr[i];
			sizeT len = SU::CStringLength(str);
			sizeT tmp = Find(str, len, pos, searchCount, caseSensitive);
			if (tmp != NPos && (pair.first == NPos || tmp < pair.first))
			{
				pair.first = tmp;
				pair.second = len;
			}
		}
		return pair;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	Pair<sizeT, sizeT> TString<CharT, GrowthPolicy>::FindAnyWithLength(const AnsiChar*(&arr)[N], sizeT pos,
		b8 caseSensitive) const
	{
		return FindAnyWithLength(arr, N, pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	Pair<sizeT, sizeT> TString<CharT, GrowthPolicy>::FindAnyWithLength(const WideChar*(&arr)[N], sizeT pos,
		b8 caseSensitive) const
	{
		return FindAnyWithLength(arr, N, pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	Pair<sizeT, sizeT> TString<CharT, GrowthPolicy>::FindAnyWithLength(const TString<C, G>* arr, sizeT count,
		sizeT pos, b8 caseSensitive) const
	{
		if (count == 0)
			return Pair<sizeT, sizeT>(NPos, 0);
		Pair<sizeT, sizeT> pair(NPos, 0);
		sizeT searchCount = m_Length - pos;
		for (sizeT i = 0; i < count; ++i)
		{
			const TString<C, G>& str = arr[i];
			sizeT tmp = Find(str.m_Data, str.m_Data, pos, searchCount, caseSensitive);
			if (tmp != NPos && (pair.first == NPos || tmp < pair.first))
			{
				pair.first = tmp;
				pair.second = str.m_Length;
			}
		}
		return pair;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G, sizeT N>
	Pair<sizeT, sizeT> TString<CharT, GrowthPolicy>::FindAnyWithLength(const TString<C, G>(&arr)[N],
		sizeT pos, b8 caseSensitive) const
	{
		return FindAnyWithLength(arr, N, pos, caseSensitive);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	FindAnyNotOf
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::FindAnyNotOf(const AnsiChar* arr, sizeT count, sizeT pos,
		b8 caseSensitive) const
	{
		if (pos >= m_Length || count == 0)
			return NPos;
		if (caseSensitive)
		{
			for (sizeT i = pos; i < m_Length; ++i)
			{
				sizeT j = 0;
				for (; j < count; ++j)
				{
					if (m_Data[i] == arr[j])
						break;
				}
				if (j == count)
					return i;
			}
		}
		else
		{
			for (sizeT i = pos; i < m_Length; ++i)
			{
				sizeT j = 0;
				for (; j < count; ++j)
				{
					if (CU::ToLower(m_Data[i]) != CU::ToLower(arr[j]))
						break;
					if (j == count)
						return i;
				}
			}
		}
		return NPos;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::FindAnyNotOf(const WideChar* arr, sizeT count, sizeT pos,
		b8 caseSensitive) const
	{
		if (pos >= m_Length || count == 0)
			return NPos;
		if (caseSensitive)
		{
			for (sizeT i = pos; i < m_Length; ++i)
			{
				sizeT j = 0;
				for (; j < count; ++j)
				{
					if (m_Data[i] == arr[j])
						break;
				}
				if (j == count)
					return i;
			}
		}
		else
		{
			for (sizeT i = pos; i < m_Length; ++i)
			{
				sizeT j = 0;
				for (; j < count; ++j)
				{
					if (CU::ToLower(m_Data[i]) != CU::ToLower(arr[j]))
						break;
					if (j == count)
						return i;
				}
			}
		}
		return NPos;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::FindAnyNotOf(const AnsiChar* str, sizeT pos, b8 caseSensitive) const
	{
		return FindAnyNotOf(str, SU::CStringLength(str), pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::FindAnyNotOf(const WideChar* str, sizeT pos, b8 caseSensitive) const
	{
		return FindAnyNotOf(str, SU::CStringLength(str), pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	sizeT TString<CharT, GrowthPolicy>::FindAnyNotOf(const TString<C, G>& str, sizeT pos,
		b8 caseSensitive) const
	{
		return FindAnyNotOf(str.m_Data, str.m_Length, pos, caseSensitive);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	RFind
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::RFind(AnsiChar c, sizeT pos, b8 caseSensitive) const
	{
		if (m_Length == 0)
			return NPos;
		if (pos > m_Length - 1)
			pos = m_Length - 1;
		if (caseSensitive)
		{
			for (sizeT i = pos;; --i)
			{
				if (m_Data[i] == c)
					return i;
				if (i == 0)
					return NPos;
			}
		}
		else
		{
			AnsiChar ch = CU::ToLower(c);
			for (sizeT i = pos;; --i)
			{
				if (CU::ToLower(m_Data[i]) == ch)
					return i;
				if (i == 0)
					return NPos;
			}
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::RFind(WideChar c, sizeT pos, b8 caseSensitive) const
	{
		if (m_Length == 0)
			return NPos;
		if (pos > m_Length - 1)
			pos = m_Length - 1;
		if (caseSensitive)
		{
			for (sizeT i = pos;; --i)
			{
				if (m_Data[i] == c)
					return i;
				if (i == 0)
					return NPos;
			}
		}
		else
		{
			WideChar ch = CU::ToLower(c);
			for (sizeT i = pos;; --i)
			{
				if (CU::ToLower(m_Data[i]) == ch)
					return i;
				if (i == 0)
					return NPos;
			}
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::RFind(const AnsiChar* str, sizeT pos, b8 caseSensitive) const
	{
		return RFind(str, SU::CStringLength(str), pos, NPos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::RFind(const WideChar* str, sizeT pos, b8 caseSensitive) const
	{
		return RFind(str, SU::CStringLength(str), pos, NPos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::RFind(const AnsiChar* str, sizeT pos, sizeT count, b8 caseSensitive) const
	{
		return RFind(str, SU::CStringLength(str), pos, count, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::RFind(const WideChar* str, sizeT pos, sizeT count, b8 caseSensitive) const
	{
		return RFind(str, SU::CStringLength(str), pos, count, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::RFind(const AnsiChar* str, sizeT len, sizeT pos, sizeT count,
		b8 caseSensitive) const
	{
		if (m_Length == 0)
			return NPos;
		if (len == NPos)
			len = SU::CStringLength(str);
		if (len == 0)
			return pos;
		if (pos > m_Length - len)
			pos = m_Length - len;
		sizeT end = pos < count ? 0 : pos + 1 - count;
		AnsiChar fc = str[0];
		if (caseSensitive)
		{
			for (sizeT i = pos;; --i)
			{
				if (m_Data[i] != fc)
				{
					if (i == end)
						return NPos;
					continue;
				}
				if (len == 1)
					return i;
				sizeT j = 1;
				for (; j < len; ++j)
				{
					if (m_Data[i + j] != str[j])
						break;
				}
				if (j == len)
					return i;
				if (i == end)
					return NPos;
			}
		}
		else
		{
			fc = CU::ToLower(fc);
			for (sizeT i = pos;; --i)
			{
				if (m_Data[i] != fc)
				{
					if (i == end)
						return NPos;
					continue;
				}
				if (len == 1)
					return i;
				sizeT j = 1;
				for (; j < len; ++j)
				{
					if (CU::ToLower(m_Data[i + j]) != CU::ToLower(str[j]))
						break;
				}
				if (j == len)
					return i;
				if (i == end)
					return NPos;
			}
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::RFind(const WideChar* str, sizeT len, sizeT pos, sizeT count,
		b8 caseSensitive) const
	{
		if (m_Length == 0)
			return NPos;
		if (len == NPos)
			len = SU::CStringLength(str);
		if (len == 0)
			return pos;
		if (pos > m_Length - len)
			pos = m_Length - len;
		sizeT end = pos < count ? 0 : pos + 1 - count;
		WideChar fc = str[0];
		if (caseSensitive)
		{
			for (sizeT i = pos;; --i)
			{
				if (m_Data[i] != fc)
				{
					if (i == end)
						return NPos;
					continue;
				}
				if (len == 1)
					return i;
				sizeT j = 1;
				for (; j < len; ++j)
				{
					if (m_Data[i + j] != str[j])
						break;
				}
				if (j == len)
					return i;
				if (i == end)
					return NPos;
			}
		}
		else
		{
			fc = CU::ToLower(fc);
			for (sizeT i = pos;; --i)
			{
				if (m_Data[i] != fc)
				{
					if (i == end)
						return NPos;
					continue;
				}
				if (len == 1)
					return i;
				sizeT j = 1;
				for (; j < len; ++j)
				{
					if (CU::ToLower(m_Data[i + j]) != CU::ToLower(str[j]))
						break;
				}
				if (j == len)
					return i;
				if (i == end)
					return NPos;
			}
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	sizeT TString<CharT, GrowthPolicy>::RFind(const TString<C, G>& str, sizeT pos, b8 caseSensitive) const
	{
		return RFind(str.m_Data, str.m_Length, pos, NPos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	sizeT TString<CharT, GrowthPolicy>::RFind(const TString<C, G>& str, sizeT pos, sizeT count,
		b8 caseSensitive) const
	{
		return RFind(str.m_Data, str.m_Length, pos, count, caseSensitive);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	RFindAny
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::RFindAny(const AnsiChar* arr, sizeT count, sizeT pos,
		b8 caseSensitive) const
	{
		if (count == 0 || m_Length == 0)
			return NPos;
		if (pos >= m_Length)
			pos = m_Length - 1;
		if (caseSensitive)
		{
			for (sizeT i = pos;; --i)
			{
				for (sizeT j = 0; j < count; ++j)
				{
					if (m_Data[i] == arr[j])
						return i;
				}
				if (i == 0)
					return NPos;
			}
		}
		else
		{
			for (sizeT i = pos;; --i)
			{
				for (sizeT j = 0; j < count; ++j)
				{
					if (CU::ToLower(m_Data[i]) == CU::ToLower(arr[j]))
						return i;
				}
				if (i == 0)
					return NPos;
			}
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::RFindAny(const WideChar* arr, sizeT count, sizeT pos,
		b8 caseSensitive) const
	{
		if (count == 0 || m_Length == 0)
			return NPos;
		if (pos >= m_Length)
			pos = m_Length - 1;
		if (caseSensitive)
		{
			for (sizeT i = pos;; --i)
			{
				for (sizeT j = 0; j < count; ++j)
				{
					if (m_Data[i] == arr[j])
						return i;
				}
				if (i == 0)
					return NPos;
			}
		}
		else
		{
			for (sizeT i = pos;; --i)
			{
				for (sizeT j = 0; j < count; ++j)
				{
					if (CU::ToLower(m_Data[i]) == CU::ToLower(arr[j]))
						return i;
				}
				if (i == 0)
					return NPos;
			}
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::RFindAny(const AnsiChar* str, sizeT pos, b8 caseSensitive) const
	{
		return RFindAny(str, SU::CStringLength(str), pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::RFindAny(const WideChar* str, sizeT pos, b8 caseSensitive) const
	{
		return RFindAny(str, SU::CStringLength(str), pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::RFindAny(const AnsiChar** arr, sizeT count, sizeT pos,
		b8 caseSensitive) const
	{
		if (count == 0 || m_Length == 0)
			return NPos;
		sizeT idx = NPos;
		sizeT searchCount = pos + 1;
		for (sizeT i = 0; i < count; ++i)
		{
			const AnsiChar* str = arr[i];
			sizeT len = SU::CStringLength(str);
			sizeT tmp = RFind(str, len, pos, searchCount, caseSensitive);
			if (tmp != NPos && (idx == NPos || tmp > idx))
				idx = tmp;
		}
		return idx;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::RFindAny(const WideChar** arr, sizeT count, sizeT pos,
		b8 caseSensitive) const
	{
		if (count == 0 || m_Length == 0)
			return NPos;
		sizeT idx = NPos;
		sizeT searchCount = pos + 1;
		for (sizeT i = 0; i < count; ++i)
		{
			const WideChar* str = arr[i];
			sizeT len = SU::CStringLength(str);
			sizeT tmp = RFind(str, len, pos, searchCount, caseSensitive);
			if (tmp != NPos && (idx == NPos || tmp > idx))
				idx = tmp;
		}
		return idx;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	sizeT TString<CharT, GrowthPolicy>::RFindAny(const AnsiChar*(&arr)[N], sizeT pos, b8 caseSensitive) const
	{
		return FindAny(arr, N, pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	sizeT TString<CharT, GrowthPolicy>::RFindAny(const WideChar*(&arr)[N], sizeT pos, b8 caseSensitive) const
	{
		return FindAny(arr, N, pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	sizeT TString<CharT, GrowthPolicy>::RFindAny(const TString<C, G>& str, sizeT pos, b8 caseSensitive) const
	{
		return RFindAny(str.m_Data, str.m_Length, pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	sizeT TString<CharT, GrowthPolicy>::RFindAny(const TString<C, G>* arr, sizeT count, sizeT pos,
		b8 caseSensitive) const
	{
		if (count == 0 || m_Length == 0)
			return NPos;
		sizeT idx = NPos;
		sizeT searchCount = pos + 1;
		for (sizeT i = 0; i < count; ++i)
		{
			const TString<C, G>& str = arr[i];
			sizeT tmp = Find(str.m_Data, str.m_Length, pos, searchCount, caseSensitive);
			if (tmp != NPos && (idx == NPos || tmp > idx))
				idx = tmp;
		}
		return idx;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G, sizeT N>
	sizeT TString<CharT, GrowthPolicy>::RFindAny(const TString<C, G>(&arr)[N], sizeT pos,
		b8 caseSensitive) const
	{
		return RFindAny(arr, N, pos, caseSensitive);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	RFindAnyWithLength
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	Pair<sizeT, sizeT> TString<CharT, GrowthPolicy>::RFindAnyWithLength(const AnsiChar** arr, sizeT count,
		sizeT pos, b8 caseSensitive) const
	{
		if (count == 0 || m_Length == 0)
			return Pair<sizeT, sizeT>(NPos, 0);
		Pair<sizeT, sizeT> pair(NPos, 0);
		sizeT searchCount = pos + 1;
		for (sizeT i = 0; i < count; ++i)
		{
			const AnsiChar* str = arr[i];
			sizeT len = SU::CStringLength(str);
			sizeT tmp = RFind(str, len, pos, searchCount, caseSensitive);
			if (tmp != NPos && (pair.first == NPos || tmp > pair.first))
			{
				pair.first = tmp;
				pair.second = len;
			}
		}
		return pair;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	Pair<sizeT, sizeT> TString<CharT, GrowthPolicy>::RFindAnyWithLength(const WideChar** arr, sizeT count,
		sizeT pos, b8 caseSensitive) const
	{
		if (count == 0 || m_Length == 0)
			return Pair<sizeT, sizeT>(NPos, 0);
		Pair<sizeT, sizeT> pair(NPos, 0);
		sizeT searchCount = pos + 1;
		for (sizeT i = 0; i < count; ++i)
		{
			const WideChar* str = arr[i];
			sizeT len = SU::CStringLength(str);
			sizeT tmp = RFind(str, len, pos, searchCount, caseSensitive);
			if (tmp != NPos && (pair.first == NPos || tmp > pair.first))
			{
				pair.first = tmp;
				pair.second = len;
			}
		}
		return pair;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	Pair<sizeT, sizeT> TString<CharT, GrowthPolicy>::RFindAnyWithLength(const AnsiChar*(&arr)[N], sizeT pos,
		b8 caseSensitive) const
	{
		return RFindAnyWithLength(arr, N, pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	Pair<sizeT, sizeT> TString<CharT, GrowthPolicy>::RFindAnyWithLength(const WideChar*(&arr)[N], sizeT pos,
		b8 caseSensitive) const
	{
		return RFindAnyWithLength(arr, N, pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	Pair<sizeT, sizeT> TString<CharT, GrowthPolicy>::RFindAnyWithLength(const TString<C, G>* arr,
		sizeT count, sizeT pos, b8 caseSensitive) const
	{
		if (count == 0 || m_Length == 0)
			return Pair<sizeT, sizeT>(NPos, 0);
		Pair<sizeT, sizeT> pair(NPos, 0);
		sizeT searchCount = pos + 1;
		for (sizeT i = 0; i < count; ++i)
		{
			const TString<C, G>& str = arr[i];
			sizeT tmp = RFind(str.m_Data, str.m_Data, pos, searchCount, caseSensitive);
			if (tmp != NPos && (pair.first == NPos || tmp > pair.first))
			{
				pair.first = tmp;
				pair.second = str.m_Length;
			}
		}
		return pair;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G, sizeT N>
	Pair<sizeT, sizeT> TString<CharT, GrowthPolicy>::RFindAnyWithLength(const TString<C, G>(&arr)[N],
		sizeT pos, b8 caseSensitive) const
	{
		return RFindAnyWithLength(arr, N, pos, caseSensitive);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	RFindAnyNotOf
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::RFindAnyNotOf(const AnsiChar* arr, sizeT count, sizeT pos,
		b8 caseSensitive) const
	{
		if (count == 0 || m_Length == 0)
			return NPos;
		if (pos >= m_Length)
			pos = m_Length - 1;
		if (caseSensitive)
		{
			for (sizeT i = pos;; --i)
			{
				sizeT j = 0;
				for (; j < count; ++j)
				{
					if (m_Data[i] == arr[j])
						break;
				}
				if (j == count)
					return i;
				if (i == 0)
					return NPos;
			}
		}
		else
		{
			for (sizeT i = pos;; --i)
			{
				sizeT j = 0;
				for (; j < count; ++j)
				{
					if (CU::ToLower(m_Data[i]) == CU::ToLower(arr[j]))
						break;
				}
				if (j == count)
					return i;
				if (i == 0)
					return NPos;
			}
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::RFindAnyNotOf(const WideChar* arr, sizeT count, sizeT pos,
		b8 caseSensitive) const
	{
		if (count == 0 || m_Length == 0)
			return NPos;
		if (pos >= m_Length)
			pos = m_Length - 1;
		if (caseSensitive)
		{
			for (sizeT i = pos;; --i)
			{
				sizeT j = 0;
				for (; j < count; ++j)
				{
					if (m_Data[i] == arr[j])
						break;
				}
				if (j == count)
					return i;
				if (i == 0)
					return NPos;
			}
		}
		else
		{
			for (sizeT i = pos;; --i)
			{
				sizeT j = 0;
				for (; j < count; ++j)
				{
					if (CU::ToLower(m_Data[i]) == CU::ToLower(arr[j]))
						break;
				}
				if (j == count)
					return i;
				if (i == 0)
					return NPos;
			}
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::RFindAnyNotOf(const AnsiChar* str, sizeT pos, b8 caseSensitive) const
	{
		return RFindAnyNotOf(str, SU::CStringLength(str), pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::RFindAnyNotOf(const WideChar* str, sizeT pos, b8 caseSensitive) const
	{
		return RFindAnyNotOf(str, SU::CStringLength(str), pos, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	sizeT TString<CharT, GrowthPolicy>::RFindAnyNotOf(const TString<C, G>& str, sizeT pos,
		b8 caseSensitive) const
	{
		return RFindAnyNotOf(str.m_Data, str.m_Length, pos, caseSensitive);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Compare
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	i8 TString<CharT, GrowthPolicy>::Compare(AnsiChar c, b8 caseSensitive) const
	{
		if (m_Length == 0)
			return -1;
		if (caseSensitive)
		{
			if (m_Length == 1 && *m_Data == c)
				return 0;
			if (c < *m_Data)
				return 1;
			return -1;
		}
		else
		{
			c = CU::ToLower(c);
			CharT ch = CU::ToLower(*m_Data);
			if (m_Length == 1 && ch == c)
				return 0;
			if (c < ch)
				return 1;
			return -1;
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	i8 TString<CharT, GrowthPolicy>::Compare(WideChar c, b8 caseSensitive) const
	{
		if (m_Length == 0)
			return -1;
		if (caseSensitive)
		{
			if (m_Length == 1 && *m_Data == c)
				return 0;
			if (c < *m_Data)
				return 1;
			return -1;
		}
		else
		{
			c = CU::ToLower(c);
			CharT ch = CU::ToLower(*m_Data);
			if (m_Length == 1 && ch == c)
				return 0;
			if (c < ch)
				return 1;
			return -1;
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::Compare(sizeT pos, AnsiChar c, b8 caseSensitive) const
	{
		if (pos >= m_Length)
			return false;
		if (caseSensitive)
			return m_Data[pos] == c;
		return CU::ToLower(m_Data[pos]) == CU::ToLower(c);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::Compare(sizeT pos, WideChar c, b8 caseSensitive) const
	{
		if (pos >= m_Length)
			return false;
		if (caseSensitive)
			return m_Data[pos] == c;
		return CU::ToLower(m_Data[pos]) == CU::ToLower(c);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	i8 TString<CharT, GrowthPolicy>::Compare(const AnsiChar* str, b8 caseSensitive) const
	{
		return Compare(0, m_Length, str, 0, SU::CStringLength(str), caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	i8 TString<CharT, GrowthPolicy>::Compare(const WideChar* str, b8 caseSensitive) const
	{
		return Compare(0, m_Length, str, 0, SU::CStringLength(str), caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	i8 TString<CharT, GrowthPolicy>::Compare(sizeT pos, sizeT len, const AnsiChar* str, b8 caseSensitive) const
	{
		return Compare(pos, len, str, 0, SU::CStringLength(str), caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	i8 TString<CharT, GrowthPolicy>::Compare(sizeT pos, sizeT len, const WideChar* str, b8 caseSensitive) const
	{
		return Compare(pos, len, str, 0, SU::CStringLength(str), caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	i8 TString<CharT, GrowthPolicy>::Compare(sizeT pos, sizeT len, const AnsiChar* str, sizeT count,
		b8 caseSensitive) const
	{
		return Compare(pos, len, str, 0, count, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	i8 TString<CharT, GrowthPolicy>::Compare(sizeT pos, sizeT len, const WideChar* str, sizeT count,
		b8 caseSensitive) const
	{
		return Compare(pos, len, str, 0, count, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	i8 TString<CharT, GrowthPolicy>::Compare(sizeT pos, sizeT len, const AnsiChar* str, sizeT subPos,
		sizeT subLen, b8 caseSensitive) const
	{
		if (m_Length == 0 || len == 0)
		{
			if (m_Length == len)
				return 0;
			if (m_Length == 0)
				return -1;
			return 1;
		}

		HV_ASSERT_CODE(sizeT assertLen = str ? SU::CStringLength(str) : 0);
		HV_ASSERT_MSG((str ? subPos < assertLen : true) || (assertLen == 0 && subPos == 0), "subPos out of range!");
		HV_ASSERT_MSG(subPos + subLen <= assertLen, "subLen is too big!");
		HV_ASSERT_MSG(pos < m_Length || m_Length == 0 && pos == 0, "pos out of range!");
		HV_ASSERT_MSG(pos + len <= m_Length, "len is too big!");

		sizeT clen = len < subLen ? len : subLen;

		if (caseSensitive)
		{
			if (std::is_same_v<CharT, AnsiChar>)
			{
				i8 res = i8(Memory::Compare(m_Data + pos, str + subPos, clen * sizeof(CharT)));
				if (res == 0)
				{
					if (len == subLen)
						return 0;
					if (subLen < len)
						return 1;
					return -1;
				}
				return res;
			}
			else
			{
				CharT* data0 = m_Data + pos;
				const AnsiChar* data1 = str + subPos;
				for (sizeT i = 0; i < len; ++i)
				{
					CharT c0 = data0[i];
					AnsiChar c1 = data1[i];
					if (c0 == c1)
						continue;
					if (c0 < c1)
						return -1;
					return 1;
				}
				if (len == subLen)
					return 0;
				if (subLen < len)
					return 1;
				return -1;
			}
		}
		else
		{
			CharT* data0 = m_Data + pos;
			const AnsiChar* data1 = str + subPos;
			for (sizeT i = 0; i < len; ++i)
			{
				CharT c0 = CU::ToLower(data0[i]);
				AnsiChar c1 = CU::ToLower(data1[i]);
				if (c0 == c1)
					continue;
				if (c0 < c1)
					return -1;
				return 1;
			}
			if (len == subLen)
				return 0;
			if (subLen < len)
				return 1;
			return -1;
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	i8 TString<CharT, GrowthPolicy>::Compare(sizeT pos, sizeT len, const WideChar* str, sizeT subPos,
		sizeT subLen, b8 caseSensitive) const
	{
		HV_ASSERT_CODE(sizeT assertLen = str ? SU::CStringLength(str) : 0);
		HV_ASSERT_MSG((str ? subPos < assertLen : true) || (assertLen == 0 && subPos == 0), "subPos out of range!");
		HV_ASSERT_MSG(subPos + subLen <= assertLen, "subLen is too big!");
		HV_ASSERT_MSG(pos < m_Length || m_Length == 0 && pos == 0, "pos out of range!");
		HV_ASSERT_MSG(pos + len <= m_Length, "len is too big!");

		if (len == 0 || subLen == 0)
		{
			if (len == subLen)
				return 0;
			if (len == 0)
				return -1;
			return 1;
		}

		sizeT clen = len < subLen ? len : subLen;

		if (caseSensitive)
		{
			if (std::is_same_v<CharT, WideChar>)
			{
				i8 res = i8(Memory::Compare(m_Data + pos, str + subPos, clen * sizeof(CharT)));
				if (res == 0)
				{
					if (len == subLen)
						return 0;
					if (subLen < len)
						return 1;
					return -1;
				}
				return res;
			}
			else
			{
				CharT* data0 = m_Data + pos;
				const WideChar* data1 = str + subPos;
				for (sizeT i = 0; i < len; ++i)
				{
					CharT c0 = data0[i];
					WideChar c1 = data1[i];
					if (c0 == c1)
						continue;
					if (c0 < c1)
						return -1;
					return 1;
				}
				if (len == subLen)
					return 0;
				if (subLen < len)
					return 1;
				return -1;
			}
		}
		else
		{
			CharT* data0 = m_Data + pos;
			const WideChar* data1 = str + subPos;
			for (sizeT i = 0; i < len; ++i)
			{
				CharT c0 = CU::ToLower(data0[i]);
				WideChar c1 = CU::ToLower(data1[i]);
				if (c0 == c1)
					continue;
				if (c0 < c1)
					return -1;
				return 1;
			}
			if (len == subLen)
				return 0;
			if (subLen < len)
				return 1;
			return -1;
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	i8 TString<CharT, GrowthPolicy>::Compare(const TString<C, G>& str, b8 caseSensitive) const
	{
		return Compare(0, m_Length, str.m_Data, 0, str.m_Length, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	i8 TString<CharT, GrowthPolicy>::Compare(sizeT pos, sizeT len, const TString<C, G>& str,
		b8 caseSensitive) const
	{
		return Compare(pos, len, str.m_Data, 0, str.m_Length, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	i8 TString<CharT, GrowthPolicy>::Compare(sizeT pos, sizeT len, const TString<C, G>& str, sizeT count,
		b8 caseSensitive) const
	{
		return Compare(pos, len, str.m_Data, 0, len, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	i8 TString<CharT, GrowthPolicy>::Compare(sizeT pos, sizeT len, const TString<C, G>& str, sizeT subPos,
		sizeT subLen, b8 caseSensitive) const
	{
		return Compare(pos, len, str.m_Data, subPos, subLen, caseSensitive);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	StartWith
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::StartsWith(AnsiChar c, b8 caseSensitive) const
	{
		if (m_Length == 0)
			return false;
		if (caseSensitive)
			return m_Data[0] == c;
		return CU::ToLower(m_Data[0]) == CU::ToLower(c);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::StartsWith(WideChar c, b8 caseSensitive) const
	{
		if (m_Length == 0)
			return false;
		if (caseSensitive)
			return m_Data[0] == c;
		return CU::ToLower(m_Data[0]) == CU::ToLower(c);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::StartsWith(const AnsiChar* str, b8 caseSensitive) const
	{
		return StartsWith(str, SU::CStringLength(str), caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::StartsWith(const WideChar* str, b8 caseSensitive) const
	{
		return StartsWith(str, SU::CStringLength(str), caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::StartsWith(const AnsiChar* str, sizeT len, b8 caseSensitive) const
	{
		if (m_Length < len)
			return true;
		if (caseSensitive)
		{
			for (sizeT i = 0; i < len; ++i)
			{
				if (m_Data[i] != str[i])
					return false;
			}
		}
		else
		{
			for (sizeT i = 0; i < len; ++i)
			{
				if (CU::ToLower(m_Data[i]) != CU::ToLower(str[i]))
					return false;
			}
		}
		return true;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::StartsWith(const WideChar* str, sizeT len, b8 caseSensitive) const
	{
		if (m_Length < len)
			return true;
		if (caseSensitive)
		{
			for (sizeT i = 0; i < len; ++i)
			{
				if (m_Data[i] != str[i])
					return false;
			}
		}
		else
		{
			for (sizeT i = 0; i < len; ++i)
			{
				if (CU::ToLower(m_Data[i]) != CU::ToLower(str[i]))
					return false;
			}
		}
		return true;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	b8 TString<CharT, GrowthPolicy>::StartsWith(const TString<C, G> str, b8 caseSensitive) const
	{
		return StartsWith(str.m_Data, str.m_Length, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	b8 TString<CharT, GrowthPolicy>::StartsWith(const TString<C, G> str, sizeT len, b8 caseSensitive) const
	{
		return StartsWith(str.m_Data, len, caseSensitive);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	EndsWith
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::EndsWith(AnsiChar c, b8 caseSensitive) const
	{
		if (m_Length == 0)
			return false;
		if (caseSensitive)
			return m_Data[m_Length - 1] == c;
		return CU::ToLower(m_Data[m_Length - 1]) == CU::ToLower(c);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::EndsWith(WideChar c, b8 caseSensitive) const
	{
		if (m_Length == 0)
			return false;
		if (caseSensitive)
			return m_Data[m_Length - 1] == c;
		return CU::ToLower(m_Data[m_Length - 1]) == CU::ToLower(c);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::EndsWith(const AnsiChar* str, b8 caseSensitive) const
	{
		return EndsWith(str, SU::CStringLength(str), caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::EndsWith(const WideChar* str, b8 caseSensitive) const
	{
		return EndsWith(str, SU::CStringLength(str), caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::EndsWith(const AnsiChar* str, sizeT len, b8 caseSensitive) const
	{
		if (m_Length < len)
			return true;
		sizeT offset = m_Length - len;
		CharT* data = m_Data + offset;
		if (caseSensitive)
		{
			for (sizeT i = 0; i < len; ++i)
			{
				if (data[i] != str[i])
					return false;
			}
		}
		else
		{
			for (sizeT i = 0; i < len; ++i)
			{
				if (CU::ToLower(data[i]) != CU::ToLower(str[i]))
					return false;
			}
		}
		return true;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::EndsWith(const WideChar* str, sizeT len, b8 caseSensitive) const
	{
		if (m_Length < len)
			return true;
		sizeT offset = m_Length - len;
		CharT* data = m_Data + offset;
		if (caseSensitive)
		{
			for (sizeT i = 0; i < len; ++i)
			{
				if (data[i] != str[i])
					return false;
			}
		}
		else
		{
			for (sizeT i = 0; i < len; ++i)
			{
				if (CU::ToLower(data[i]) != CU::ToLower(str[i]))
					return false;
			}
		}
		return true;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	b8 TString<CharT, GrowthPolicy>::EndsWith(const TString<C, G> str, b8 caseSensitive) const
	{
		return EndsWith(str.m_Data, str.m_Length, caseSensitive);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G>
	b8 TString<CharT, GrowthPolicy>::EndsWith(const TString<C, G> str, sizeT len, b8 caseSensitive) const
	{
		return EndsWith(str.m_Data, len, caseSensitive);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	SubString
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy> TString<CharT, GrowthPolicy>::SubString(sizeT pos, sizeT len) const
	{
		if (pos >= m_Length)
			return TString();
		return TString(*this, pos, len, m_pAlloc);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Pad
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Pad(sizeT size)
	{
		return Pad(CharT(' '), size, size);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Pad(AnsiChar c, sizeT size)
	{
		return Pad(c, size, size);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Pad(WideChar c, sizeT size)
	{
		return Pad(c, size, size);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Pad(sizeT left, sizeT right)
	{
		return Pad(CharT(' '), left, right);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Pad(AnsiChar c, sizeT left,
		sizeT right)
	{
		if (left > 0)
			Insert(sizeT(0), c, left);
		if (right > 0)
			Append(c, right);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Pad(WideChar c, sizeT left,
		sizeT right)
	{
		if (left > 0)
			Insert(sizeT(0), c, left);
		if (right > 0)
			Append(c, right);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::PadLeft(sizeT size)
	{
		return Pad(CharT(' '), size, 0);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::PadLeft(AnsiChar c, sizeT size)
	{
		return Pad(c, size, 0);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::PadLeft(WideChar c, sizeT size)
	{
		return Pad(c, size, 0);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::PadRight(sizeT size)
	{
		return Pad(CharT(' '), 0, size);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::PadRight(AnsiChar c, sizeT size)
	{
		return Pad(c, 0, size);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::PadRight(WideChar c, sizeT size)
	{
		return Pad(c, 0, size);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Trim
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Trim()
	{
		TrimLeft();
		TrimRight();
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Trim(AnsiChar c)
	{
		TrimLeft(c);
		TrimRight(c);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::Trim(WideChar c)
	{
		TrimLeft(c);
		TrimRight(c);
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::TrimLeft()
	{
		for (sizeT i = 0; i < m_Length; ++i)
		{
			if (!CU::IsWhitespace(m_Data[i]))
			{
				if (i > 0)
					Erase(sizeT(0), i);
				break;
			}
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::TrimLeft(AnsiChar c)
	{
		for (sizeT i = 0; i < m_Length; ++i)
		{
			if (m_Data[i] != c)
			{
				if (i > 0)
					Erase(sizeT(0), i);
				break;
			}
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::TrimLeft(WideChar c)
	{
		for (sizeT i = 0; i < m_Length; ++i)
		{
			if (m_Data[i] != c)
			{
				if (i > 0)
					Erase(sizeT(0), i);
				break;
			}
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::TrimRight()
	{
		for (sizeT i = m_Length - 1;; --i)
		{
			if (!CU::IsWhitespace(m_Data[i]))
			{
				if (i < m_Length - 1)
					Erase(i + 1, m_Length - i - 1);
				break;
			}
			if (i == 0)
				break;
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::TrimRight(AnsiChar c)
	{
		for (sizeT i = m_Length - 1;; --i)
		{
			if (m_Data[i] != c)
			{
				if (i > 0)
					Erase(i + 1, m_Length - i - 1);
				break;
			}
			if (i == 0)
				break;
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::TrimRight(WideChar c)
	{
		for (sizeT i = m_Length - 1;; --i)
		{
			if (m_Data[i] != c)
			{
				if (i > 0)
					Erase(i + 1, m_Length - i - 1);
				break;
			}
			if (i == 0)
				break;
		}
		return *this;
	}

	////////////////////////////////////////////////////////////////////////////////
	//	SplitChar
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc G>
	DynArray<TString<CharT, GrowthPolicy>, G> TString<CharT, GrowthPolicy>::SplitChar(AnsiChar c,
		b8 removeEmptyEntries, Memory::IAllocator* pAlloc) const
	{
		if (pAlloc == nullptr)
			pAlloc = m_pAlloc;
		DynArray<TString, G> da(pAlloc);
		sizeT idx = Find(c);
		if (idx == NPos)
		{
			if (m_Length != 0 || (m_Length == 0 && !removeEmptyEntries))
				da.Push(*this);
			return da;
		}
		sizeT prevIdx = 0;
		while (idx != NPos)
		{
			if (prevIdx == idx)
			{
				if (!removeEmptyEntries)
					da.Push(TString(m_pAlloc));
			}
			else
				da.Push(SubString(prevIdx, idx - prevIdx));
			prevIdx = idx + 1;
			idx = Find(c, prevIdx);
		}
		if (prevIdx != m_Length - 1)
			da.Push(SubString(prevIdx, NPos));
		return da;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc G>
	DynArray<TString<CharT, GrowthPolicy>, G> TString<CharT, GrowthPolicy>::SplitChar(WideChar c,
		b8 removeEmptyEntries, Memory::IAllocator* pAlloc) const
	{
		if (pAlloc == nullptr)
			pAlloc = m_pAlloc;
		DynArray<TString, G> da(pAlloc);
		sizeT idx = Find(c);
		if (idx == NPos)
		{
			if (m_Length != 0 || (m_Length == 0 && !removeEmptyEntries))
				da.Push(*this);
			return da;
		}
		sizeT prevIdx = 0;
		while (idx != NPos)
		{
			if (prevIdx == idx)
			{
				if (!removeEmptyEntries)
					da.Push(TString(m_pAlloc));
			}
			else
				da.Push(SubString(prevIdx, idx - prevIdx));
			prevIdx = idx + 1;
			idx = Find(c, prevIdx);
		}
		if (prevIdx != m_Length - 1)
			da.Push(SubString(prevIdx, NPos));
		return da;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc G>
	DynArray<TString<CharT, GrowthPolicy>, G> TString<CharT, GrowthPolicy>::SplitChar(
		const AnsiChar* arr, sizeT count, b8 removeEmptyEntries, Memory::IAllocator* pAlloc) const
	{
		if (pAlloc == nullptr)
			pAlloc = m_pAlloc;
		DynArray<TString, G> da(pAlloc);
		sizeT idx = FindAny(arr, count, sizeT(0));
		if (idx == NPos)
		{
			if (m_Length != 0 || (m_Length == 0 && !removeEmptyEntries))
				da.Push(*this);
			return da;
		}
		sizeT prevIdx = 0;
		while (idx != NPos)
		{
			if (prevIdx == idx)
			{
				if (!removeEmptyEntries)
					da.Push(TString(m_pAlloc));
			}
			else
				da.Push(SubString(prevIdx, idx - prevIdx));
			prevIdx = idx + 1;
			idx = FindAny(arr, count, prevIdx);
		}
		if (prevIdx != m_Length - 1)
			da.Push(SubString(prevIdx, NPos));
		return da;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc G>
	DynArray<TString<CharT, GrowthPolicy>, G> TString<CharT, GrowthPolicy>::SplitChar(
		const WideChar* arr, sizeT count, b8 removeEmptyEntries, Memory::IAllocator* pAlloc) const
	{
		if (pAlloc == nullptr)
			pAlloc = m_pAlloc;
		DynArray<TString, G> da(pAlloc);
		sizeT idx = FindAny(arr, count, sizeT(0));
		if (idx == NPos)
		{
			if (m_Length != 0 || (m_Length == 0 && !removeEmptyEntries))
				da.Push(*this);
			return da;
		}
		sizeT prevIdx = 0;
		while (idx != NPos)
		{
			if (prevIdx == idx)
			{
				if (!removeEmptyEntries)
					da.Push(TString(m_pAlloc));
			}
			else
				da.Push(SubString(prevIdx, idx - prevIdx));
			prevIdx = idx + 1;
			idx = FindAny(arr, count, prevIdx);
		}
		if (prevIdx != m_Length - 1)
			da.Push(SubString(prevIdx, NPos));
		return da;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc G, sizeT N>
	DynArray<TString<CharT, GrowthPolicy>, G> TString<CharT, GrowthPolicy>::SplitChar(
		const AnsiChar(&arr)[N], b8 removeEmptyEntries, Memory::IAllocator* pAlloc) const
	{
		return SplitChar(arr, N, removeEmptyEntries, pAlloc);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc G, sizeT N>
	DynArray<TString<CharT, GrowthPolicy>, G> TString<CharT, GrowthPolicy>::SplitChar(
		const WideChar(&arr)[N], b8 removeEmptyEntries, Memory::IAllocator* pAlloc) const
	{
		return SplitChar(arr, N, removeEmptyEntries, pAlloc);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G0, Container::GrowthPolicyFunc G1>
	DynArray<TString<CharT, GrowthPolicy>, G1> TString<CharT, GrowthPolicy>::SplitChar(
		const TString<C, G0>& str, b8 removeEmptyEntries, Memory::IAllocator* pAlloc) const
	{
		return SplitChar(str.m_Data, str.m_Length, removeEmptyEntries, pAlloc);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	SplitStr
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc G>
	DynArray<TString<CharT, GrowthPolicy>, G> TString<CharT, GrowthPolicy>::SplitStr(
		const AnsiChar* str, b8 removeEmptyEntries, Memory::IAllocator* pAlloc) const
	{
		if (pAlloc == nullptr)
			pAlloc = m_pAlloc;
		DynArray<TString, G> da(pAlloc);
		sizeT len = SU::CStringLength(str);
		sizeT idx = Find(str, len, 0, m_Length);
		if (idx == NPos)
		{
			if (m_Length != 0 || (m_Length == 0 && !removeEmptyEntries))
				da.Push(*this);
			return da;
		}
		sizeT prevIdx = 0;
		while (idx != NPos)
		{
			if (prevIdx == idx)
			{
				if (!removeEmptyEntries)
					da.Push(TString(m_pAlloc));
			}
			else
				da.Push(SubString(prevIdx, idx - prevIdx));
			prevIdx = idx + 1;
			idx = Find(str, len, prevIdx, m_Length - prevIdx);
		}
		return da;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc G>
	DynArray<TString<CharT, GrowthPolicy>, G> TString<CharT, GrowthPolicy>::SplitStr(
		const WideChar* str, b8 removeEmptyEntries, Memory::IAllocator* pAlloc) const
	{
		if (pAlloc == nullptr)
			pAlloc = m_pAlloc;
		DynArray<TString, G> da(pAlloc);
		sizeT len = SU::CStringLength(str);
		sizeT idx = Find(str, len, 0, m_Length);
		if (idx == NPos)
		{
			if (m_Length != 0 || (m_Length == 0 && !removeEmptyEntries))
				da.Push(*this);
			return da;
		}
		sizeT prevIdx = 0;
		while (idx != NPos)
		{
			if (prevIdx == idx)
			{
				if (!removeEmptyEntries)
					da.Push(TString(m_pAlloc));
			}
			else
				da.Push(SubString(prevIdx, idx - prevIdx));
			prevIdx = idx + len;
			idx = Find(str, len, prevIdx, m_Length - prevIdx);
		}
		return da;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc G>
	DynArray<TString<CharT, GrowthPolicy>, G> TString<CharT, GrowthPolicy>::SplitStr(
		const AnsiChar** arr, sizeT count, b8 removeEmptyEntries, Memory::IAllocator* pAlloc) const
	{
		if (pAlloc == nullptr)
			pAlloc = m_pAlloc;
		DynArray<TString, G> da(pAlloc);
		Pair<sizeT, sizeT> pair = FindAnyWithLength(arr, count, sizeT(0));
		if (pair.first == NPos)
		{
			if (m_Length != 0 || (m_Length == 0 && !removeEmptyEntries))
				da.Push(*this);
			return da;
		}
		sizeT prevIdx = 0;
		while (pair.first != NPos)
		{
			if (prevIdx == pair.first)
			{
				if (!removeEmptyEntries)
					da.Push(TString(m_pAlloc));
			}
			else
				da.Push(SubString(prevIdx, pair.first - prevIdx));
			prevIdx = pair.first + pair.second;
			pair = FindAnyWithLength(arr, count, prevIdx);
		}
		return da;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc G>
	DynArray<TString<CharT, GrowthPolicy>, G> TString<CharT, GrowthPolicy>::SplitStr(
		const WideChar** arr, sizeT count, b8 removeEmptyEntries, Memory::IAllocator* pAlloc) const
	{
		if (pAlloc == nullptr)
			pAlloc = m_pAlloc;
		DynArray<TString, G> da(pAlloc);
		Pair<sizeT, sizeT> pair = FindAnyWithLength(arr, count, sizeT(0));
		if (pair.first == NPos)
		{
			if (m_Length != 0 || (m_Length == 0 && !removeEmptyEntries))
				da.Push(*this);
			return da;
		}
		sizeT prevIdx = 0;
		while (pair.first != NPos)
		{
			if (prevIdx == pair.first)
			{
				if (!removeEmptyEntries)
					da.Push(TString(m_pAlloc));
			}
			else
				da.Push(SubString(prevIdx, pair.first - prevIdx));
			prevIdx = pair.first + pair.second;
			pair = FindAnyWithLength(arr, count, prevIdx);
		}
		return da;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc G, sizeT N>
	DynArray<TString<CharT, GrowthPolicy>, G> TString<CharT, GrowthPolicy>::SplitStr(
		const AnsiChar*(&arr)[N], b8 removeEmptyEntries, Memory::IAllocator* pAlloc) const
	{
		return SplitStr(arr, N, removeEmptyEntries, pAlloc);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc G, sizeT N>
	DynArray<TString<CharT, GrowthPolicy>, G> TString<CharT, GrowthPolicy>::SplitStr(
		const WideChar*(&arr)[N], b8 removeEmptyEntries, Memory::IAllocator* pAlloc) const
	{
		return SplitStr(arr, N, removeEmptyEntries, pAlloc);
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G0, Container::GrowthPolicyFunc G1>
	DynArray<TString<CharT, GrowthPolicy>, G1> TString<CharT, GrowthPolicy>::SplitStr(
		const TString<C, G0>& str, b8 removeEmptyEntries, Memory::IAllocator* pAlloc) const
	{
		if (pAlloc == nullptr)
			pAlloc = m_pAlloc;
		DynArray<TString, G1> da(pAlloc);
		sizeT len = str.m_Lenght;
		sizeT idx = Find(str.m_Data, len, 0, m_Length);
		if (idx == NPos)
		{
			if (m_Length != 0 || (m_Length == 0 && !removeEmptyEntries))
				da.Push(*this);
			return da;
		}
		sizeT prevIdx = 0;
		while (idx != NPos)
		{
			if (prevIdx == idx)
			{
				if (!removeEmptyEntries)
					da.Push(TString(m_pAlloc));
			}
			else
				da.Push(SubString(prevIdx, idx - prevIdx));
			prevIdx = idx + 1;
			idx = Find(str.m_Data, len, prevIdx, m_Length - prevIdx);
		}
		return da;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G0, Container::GrowthPolicyFunc G1>
	DynArray<TString<CharT, GrowthPolicy>, G1> TString<CharT, GrowthPolicy>::SplitStr(
		const TString<C, G0>* arr, sizeT count, b8 removeEmptyEntries, Memory::IAllocator* pAlloc) const
	{
		if (pAlloc == nullptr)
			pAlloc = m_pAlloc;
		DynArray<TString, G1> da(pAlloc);
		Pair<sizeT, sizeT> pair = FindAnyWithLength(arr, count, sizeT(0));
		if (pair.first == NPos)
		{
			if (m_Length != 0 || (m_Length == 0 && !removeEmptyEntries))
				da.Push(*this);
			return da;
		}
		sizeT prevIdx = 0;
		while (pair.first != NPos)
		{
			if (prevIdx == pair.first)
			{
				if (!removeEmptyEntries)
					da.Push(TString(m_pAlloc));
			}
			else
				da.Push(SubString(prevIdx, pair.first - prevIdx));
			prevIdx = pair.first + pair.second;
			pair = FindAnyWithLength(arr, count, prevIdx);
		}
		return da;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G0, Container::GrowthPolicyFunc G1, sizeT N>
	DynArray<TString<CharT, GrowthPolicy>, G1> TString<CharT, GrowthPolicy>::SplitStr(
		const TString<C, G0>*(&arr)[N], b8 removeEmptyEntries, Memory::IAllocator* pAlloc) const
	{
		return SplitStr(arr, N, removeEmptyEntries, pAlloc);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Case convertions
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ToLower()
	{
		if (m_Length == 0)
			return *this;
		for (sizeT i = 0; i < m_Length; ++i)
		{
			m_Data[i] = CU::ToLower(m_Data[i]);
		}
		return *this;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::ToUpper()
	{
		if (m_Length == 0)
			return *this;
		for (sizeT i = 0; i < m_Length; ++i)
		{
			m_Data[i] = CU::ToUpper(m_Data[i]);
		}
		return *this;
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Format
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename C, Container::GrowthPolicyFunc G, typename... Args>
	TString<CharT, GrowthPolicy>& TString<CharT, GrowthPolicy>::FormatF(const TString<C, G> format,
		Args... args)
	{
		*this = Hv::FormatF(format, Forward<Args>(args)...);
		return *this;
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Size modifications
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	void TString<CharT, GrowthPolicy>::Resize(sizeT size)
	{
		if (size < m_Length)
		{
			Memory::Clear(m_Data + size, m_Length - size);
			m_Length = size;
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	void TString<CharT, GrowthPolicy>::Resize(sizeT size, AnsiChar c)
	{
		if (size < m_Length)
			Erase(size, m_Length - size);
		else if (size > m_Length)
		{
			if (c == '\0')
			{
				Reserve(size);
				m_Length = size;
			}
			else
				Append(c, size - m_Length);
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	void TString<CharT, GrowthPolicy>::Resize(sizeT size, WideChar c)
	{
		if (size < m_Length)
			Erase(size, m_Length - size);
		else if (size > m_Length)
		{
			if (c == '\0')
			{
				Reserve(size);
				m_Length = size;
			}
			else
				Append(c, size - m_Length);
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	void TString<CharT, GrowthPolicy>::Reserve(sizeT size)
	{
		if (size > m_Capacity)
		{
			if (m_Data)
			{
				CharT* old = m_Data;

				m_Capacity = GrowthPolicy(size + 1);
				m_Data = (CharT*)m_pAlloc->Allocate(m_Capacity * sizeof(CharT), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::String));
				Memory::Clear(m_Data + m_Length, (m_Capacity - m_Length) * sizeof(CharT));
				--m_Capacity;

				if (m_Length > 0)
					Memory::Copy(m_Data, old, m_Length * sizeof(CharT));
				m_pAlloc->Free(old);
			}
			else
			{
				m_Capacity = GrowthPolicy(size + 1);
				m_Data = (CharT*)m_pAlloc->Allocate(m_Capacity * sizeof(CharT), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::String));
				Memory::Clear(m_Data, m_Capacity * sizeof(CharT));
				--m_Capacity;
			}
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	void TString<CharT, GrowthPolicy>::ShrinkToFit()
	{
		if (m_Length == 0)
			Clear();
		else
		{
			CharT* old = m_Data;

			m_Capacity = m_Length + 1;
			m_Data = (CharT*)m_pAlloc->Allocate(m_Capacity * sizeof(CharT), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::String));
			m_Data[m_Length] = CharT('\0');
			--m_Capacity;

			Memory::Copy(m_Data, old, m_Length * sizeof(CharT));
			m_pAlloc->Free(old);
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	void TString<CharT, GrowthPolicy>::Clear(b8 resize)
	{
		if (m_Data)
		{
			if (resize)
			{
				m_pAlloc->Free(m_Data);
				m_Data = nullptr;
				m_Capacity = 0;
			}
			else if (m_Length > 0)
				Memory::Clear(m_Data, m_Length * sizeof(CharT));

			m_Length = 0;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Other
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc G>
	void TString<CharT, GrowthPolicy>::Swap(TString<CharT, G>& str)
	{
		if (m_pAlloc == str.m_pAlloc)
		{
			Hv::Swap(m_Data, str.m_Data);
			Hv::Swap(m_Length, str.m_Length);
			Hv::Swap(m_Capacity, str.m_Capacity);
		}
		else
		{
			TString tmp(Move(str));
			str = Move(*this);
			*this = Move(tmp);
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::IsWhitespaceOrEmpty() const
	{
		if (m_Length == 0)
			return true;

		for (sizeT i = 0; i < m_Length; ++i)
		{
			if (!CU::IsWhitespace(m_Data[i]))
				return false;
		}
		return true;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	b8 TString<CharT, GrowthPolicy>::IsEmpty() const
	{
		return m_Length == 0;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::Length() const
	{
		return m_Length;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT TString<CharT, GrowthPolicy>::Capacity() const
	{
		return m_Capacity;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	CharT* TString<CharT, GrowthPolicy>::CStr()
	{
		return m_Data;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	const CharT* TString<CharT, GrowthPolicy>::CStr() const
	{
		return m_Data;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	Memory::IAllocator* TString<CharT, GrowthPolicy>::GetAllocator()
	{
		return m_pAlloc;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	Memory::IAllocator* TString<CharT, GrowthPolicy>::GetAllocator() const
	{
		return m_pAlloc;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	CharT* TString<CharT, GrowthPolicy>::Front()
	{
		return m_Data;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	const CharT* TString<CharT, GrowthPolicy>::Front() const
	{
		return m_Data;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	CharT* TString<CharT, GrowthPolicy>::Last()
	{
		return m_Data + m_Length - 1;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	const CharT* TString<CharT, GrowthPolicy>::Last() const
	{
		return m_Data + m_Length - 1;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	CharT* TString<CharT, GrowthPolicy>::Back()
	{
		return m_Data + m_Length;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	const CharT* TString<CharT, GrowthPolicy>::Back() const
	{
		return m_Data + m_Length;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	CharT* TString<CharT, GrowthPolicy>::begin()
	{
		return m_Data;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	const CharT* TString<CharT, GrowthPolicy>::begin() const
	{
		return m_Data;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	CharT* TString<CharT, GrowthPolicy>::end()
	{
		return m_Data + m_Length;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	const CharT* TString<CharT, GrowthPolicy>::end() const
	{
		return m_Data + m_Length;
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Helper functions
	////////////////////////////////////////////////////////////////////////////////
	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL void TString<CharT, GrowthPolicy>::CopyToData(sizeT pos, AnsiChar c, sizeT count)
	{
		CharT ch = CU::Convert(c);
		CharT* data = m_Data + pos;
		for (sizeT i = 0; i < count; ++i)
			data[i] = ch;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL void TString<CharT, GrowthPolicy>::CopyToData(sizeT pos, WideChar c, sizeT count)
	{
		CharT ch = CU::Convert(c);
		CharT* data = m_Data + pos;
		for (sizeT i = 0; i < count; ++i)
			data[i] = ch;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL void TString<CharT, GrowthPolicy>::CopyToData(sizeT pos, const AnsiChar* str, sizeT size)
	{
		if (std::is_same_v<CharT, AnsiChar>)
		{
			Memory::Copy(m_Data + pos, str, size * sizeof(CharT));
		}
		else
		{
			CharT* data = m_Data + pos;
			for (sizeT i = 0; i < size; ++i)
				data[i] = CU::Convert(str[i]);
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL void TString<CharT, GrowthPolicy>::CopyToData(sizeT pos, const WideChar* str, sizeT size)
	{
		if (std::is_same_v<CharT, WideChar>)
		{
			Memory::Copy(m_Data + pos, str, size * sizeof(CharT));
		}
		else
		{
			CharT* data = m_Data + pos;
			for (sizeT i = 0; i < size; ++i)
				data[i] = CU::Convert(str[i]);
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename InputIterator>
	HV_FORCE_INL void TString<CharT, GrowthPolicy>::CopyToData(sizeT pos, const InputIterator& itFirst,
		const InputIterator& itLast)
	{
		sizeT i = 0;
		CharT* data = m_Data + pos;
		for (InputIterator it = itFirst; it != itLast; ++it, ++i)
			data[i] = *it;
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	HV_FORCE_INL void TString<CharT, GrowthPolicy>::CopyToData(sizeT pos, const AnsiChar(&str)[N])
	{
		if (std::is_same_v<CharT, AnsiChar>)
		{
			Memory::Copy(m_Data + pos, str, N * sizeof(CharT));
		}
		else
		{
			CharT* data = m_Data + pos;
			for (sizeT i = 0; i < N; ++i)
				data[i] = CU::Convert(str[i]);
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	HV_FORCE_INL void TString<CharT, GrowthPolicy>::CopyToData(sizeT pos, const WideChar(&str)[N])
	{
		if (std::is_same_v<CharT, WideChar>)
		{
			Memory::Copy(m_Data + pos, str, N * sizeof(CharT));
		}
		else
		{
			CharT* data = m_Data + pos;
			for (sizeT i = 0; i < N; ++i)
				data[i] = CU::Convert(str[i]);
		}
	}

	template <typename CharT, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL void TString<CharT, GrowthPolicy>::MoveData(sizeT from, sizeT to)
	{
		if (from == to)
			return;
		if (from < to)
		{
			if (from == m_Length)
				return;
			Memory::Move(m_Data + to, m_Data + from, (m_Length - from) * sizeof(CharT));
		}
		else
		{
			sizeT len = m_Length - from;
			if (len > 0)
				Memory::Move(m_Data + to, m_Data + from, len * sizeof(CharT));
			sizeT diff = from - to;
			Memory::Clear(m_Data + to + len, diff * sizeof(CharT));
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	//	External functions
	////////////////////////////////////////////////////////////////////////////////
	template <typename C, Container::GrowthPolicyFunc G>
	TString<C, G> operator+(AnsiChar c, const TString<C, G>& str)
	{
		return TString<C, G>(c, str.GetAllocator()).Append(str);
	}

	template <typename C, Container::GrowthPolicyFunc G>
	TString<C, G> operator+(WideChar c, const TString<C, G>& str)
	{
		return TString<C, G>(c, str.GetAllocator()).Append(str);
	}

	template <typename C, Container::GrowthPolicyFunc G>
	TString<C, G> operator+(const AnsiChar* str0, const TString<C, G>& str1)
	{
		return TString<C, G>(str0, str1.GetAllocator()).Append(str1);
	}

	template <typename C, Container::GrowthPolicyFunc G>
	TString<C, G> operator+(const WideChar* str0, const TString<C, G>& str1)
	{
		return TString<C, G>(str0, str1.GetAllocator()).Append(str1);
	}

	template <typename C, Container::GrowthPolicyFunc G>
	TString<C, G> operator+(std::initializer_list<AnsiChar> il, const TString<C, G>& str)
	{
		return TString<C, G>(il, str.GetAllocator()).Append(str);
	}

	template <typename C, Container::GrowthPolicyFunc G>
	TString<C, G> operator+(std::initializer_list<WideChar> il, const TString<C, G>& str)
	{
		return TString<C, G>(il, str.GetAllocator()).Append(str);
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator==(AnsiChar c, const TString<C, G>& str)
	{
		return str.Compare(c) == 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator==(WideChar c, const TString<C, G>& str)
	{
		return str.Compare(c) == 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator==(const AnsiChar* str0, const TString<C, G>& str1)
	{
		return str1.Compare(str0) == 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator==(const WideChar* str0, const TString<C, G>& str1)
	{
		return str1.Compare(str0) == 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator!=(AnsiChar c, const TString<C, G>& str)
	{
		return str.Compare(c) != 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator!=(WideChar c, const TString<C, G>& str)
	{
		return str.Compare(c) != 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator!=(const AnsiChar* str0, const TString<C, G>& str1)
	{
		return str1.Compare(str0) != 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator!=(const WideChar* str0, const TString<C, G>& str1)
	{
		return str1.Compare(str0) != 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator<(AnsiChar c, const TString<C, G>& str)
	{
		return str.Compare(c) > 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator<(WideChar c, const TString<C, G>& str)
	{
		return str.Compare(c) > 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator<(const AnsiChar* str0, const TString<C, G>& str1)
	{
		return str1.Compare(str0) > 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator<(const WideChar* str0, const TString<C, G>& str1)
	{
		return str1.Compare(str0) > 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator<=(AnsiChar c, const TString<C, G>& str)
	{
		return str.Compare(c) >= 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator<=(WideChar c, const TString<C, G>& str)
	{
		return str.Compare(c) >= 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator<=(const AnsiChar* str0, const TString<C, G>& str1)
	{
		return str1.Compare(str0) >= 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator<=(const WideChar* str0, const TString<C, G>& str1)
	{
		return str1.Compare(str0) >= 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator>(AnsiChar c, const TString<C, G>& str)
	{
		return str.Compare(c) < 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator>(WideChar c, const TString<C, G>& str)
	{
		return str.Compare(c) < 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator>(const AnsiChar* str0, const TString<C, G>& str1)
	{
		return str1.Compare(str0) < 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator>(const WideChar* str0, const TString<C, G>& str1)
	{
		return str1.Compare(str0) < 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator>=(AnsiChar c, const TString<C, G>& str)
	{
		return str.Compare(c) <= 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator>=(WideChar c, const TString<C, G>& str)
	{
		return str.Compare(c) <= 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator>=(const AnsiChar* str0, const TString<C, G>& str1)
	{
		return str1.Compare(str0) <= 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	b8 operator>=(const WideChar* str0, const TString<C, G>& str1)
	{
		return str1.Compare(str0) <= 0;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	std::ostream& operator<<(std::ostream& out, const TString<C, G>& str)
	{
		if (std::is_same_v<C, AnsiChar>)
			out << str.CStr();
		else
		{
			const C* end = str.Back();
			for (const C* it = str.Front(); it < end; ++it)
				out << CharUtils<AnsiChar>::Convert(*it);
		}
		return out;
	}

	template <typename C, Container::GrowthPolicyFunc G>
	std::wostream& operator<<(std::wostream& out, const TString<C, G>& str)
	{
		if (std::is_same_v<C, WideChar>)
			out << str.CStr();
		else
		{
			const C* end = str.Back();
			for (const C* it = str.Front(); it < end; ++it)
				out << CharUtils<WideChar>::Convert(*it);
		}
		return out;
	}

}
