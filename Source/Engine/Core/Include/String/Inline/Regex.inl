// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Regex.h: Regular expression
#pragma once
#include "String/Regex.h"

namespace Hv::Regex {

	// TODO: more regex symbols
	HV_INL b8 Match(const String& str, const String& patern)
	{
		sizeT loc = patern.Find(Detail::g_RegexChars);

		if (loc == String::NPos)
			return str == patern;

		String tmp = str;
		String tmpPatern = patern;

		while (loc != String::NPos)
		{
			if (tmp.Compare(0, loc, tmpPatern, 0, loc) != 0)
				return false;

			AnsiChar ch = AnsiChar(tmpPatern[loc]);
			tmpPatern = tmpPatern.SubString(loc + 1);
			tmp = tmp.SubString(loc);

			switch (ch)
			{
			case '*':
			{
				if (!Detail::MatchAserix(tmp, tmpPatern))
					return false;
				break;
			}
			default:
				return false;
			}

			loc = tmpPatern.Find(Detail::g_RegexChars);
		}

		return tmp == tmpPatern;
	}

	


	namespace Detail {

		HV_INL b8 MatchAserix(String& str, String& patern)
		{
			if (patern.Length() == 0)
			{
				str = String();
				return true;
			}

			sizeT loc = patern.Find(g_RegexChars);
			String matchStr = patern.SubString(0, loc);
			sizeT end = str.Find(matchStr);

			if (end == String::NPos)
				return false;

			str = str.SubString(end);

			return true;
		}

	}
}
