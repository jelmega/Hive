// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// StringFormat.h: String formatting
#pragma once
#include "TString.h"

namespace Hv {

	namespace Detail::FormatF {
		
		constexpr AnsiChar g_Specifiers[] = {
			'i', 'd',	/**< Signed value */
			'u',		/**< Unsigned value */
			'o',		/**< Octal */
			'x', 'X',	/**< Hexadecimal */
			'f', 'F',	/**< Floating point value */
			'e', 'E',	/**< Scientific notation */
			'g', 'G',	/**< Shortest notation (%f or %e) */
			'a', 'A',	/**< Floating point hexadecimal */
			'c',		/**< Character */
			's',		/**< String */
			'p',		/**< Pointer address */
		};

		constexpr AnsiChar g_Flags[] = {
			'-',	/**< Left-justify */
			'+',	/**< Force sign */
			' ',	/**< Add space if no sign was written */
			'#',	/**< Force oct to be preceded by 0, hex by 0x and others have forced decimal point */
			'0',	/**< Left pad with zero */
		};

	}
	
	/**
	 * Format a string (printf format)
	 * @param[in] format	Format
	 * @param[in] args		Arguments
	 * @return				Formatted string
	 * @note				'*' for width and precision is not supported!!!
	 */
	template<typename C, Container::GrowthPolicyFunc G, typename... Args>
	TString<C, G> FormatF(const TString<C, G>& format, const Args&... args);

	/**
	 * Format a string (printf format)
	 * @param[in] format	Format
	 * @param[in] arg		Argument
	 * @return				Formatted string
	 * @note				'*' for width and precision is not supported!!!
	 */
	/*template<typename C, Container::GrowthPolicyFunc G, typename T>
	TString<C, G> FormatF(const TString<C, G>& format, const T& arg);*/

}

#include "Inline/StringFormatF.inl"
