// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Json.h: Json 'object'
#pragma once
#include "Core/CoreHeaders.h"
#include "String/String.h"
#include "Containers/HashMap.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv {
	
	class HIVE_API Json
	{
	public:
		enum class Type : u8
		{
			Null,
			Object,
			Array,
			String,
			Float,
			Integer,
			Unsigned,
			Bool
		};

	public:
		Json();
		/**
		 * Create a json boolean
		 * @param[in] val	Value
		 */
		Json(b8 val);
		/**
		 * Create a json floating point number
		 * @param[in] val	Value
		 */
		Json(f64 val);
		/**
		 * Create a json integer number
		 * @param[in] val	Value
		 */
		Json(i64 val);
		/**
		 * Create a json integer number
		 * @param[in] val	Value
		 */
		Json(i32 val);
		/**
		 * Create a json unsigned number
		 * @param[in] val	Value
		 */
		Json(u64 val);
		/**
		 * Create a json unsigned number
		 * @param[in] val	Value
		 */
		Json(u32 val);
		/**
		 * Create a json string
		 * @param[in] val	Value
		 */
		Json(const AnsiChar* val);
		/**
		 * Create a json string
		 * @param[in] val	Value
		 */
		Json(const WideChar* val);
		/**
		 * Create a json string
		 * @param[in] val	Value
		 */
		Json(const AnsiString& val);
		/**
		 * Create a json string
		 * @param[in] val	Value
		 */
		Json(const WideString& val);
		Json(const Json& json);
		Json(Json&& json) noexcept;
		~Json();

		Json& operator=(const Json& json);
		Json& operator=(Json&& json) noexcept;
		Json& operator=(b8 val)					{ SetBool(val); return *this; }
		Json& operator=(f64 val)				{ SetFloat(val); return *this; }
		Json& operator=(i64 val)				{ SetInteger(val); return *this; }
		Json& operator=(i32 val)				{ SetInteger(val); return *this; }
		Json& operator=(u64 val)				{ SetUnsigned(val); return *this; }
		Json& operator=(u32 val)				{ SetUnsigned(val); return *this; }
		Json& operator=(const AnsiChar* val)	{ SetString(val); return *this; }
		Json& operator=(const WideChar* val)	{ SetString(val); return *this; }
		Json& operator=(const AnsiString& val)	{ SetString(val); return *this; }
		Json& operator=(const WideString& val)	{ SetString(val); return *this; }

		/**
		 * @Note	If the key has no value, a default value will be inserted into the object
		 */
		Json& operator[](const AnsiChar* key);
		/**
		* @Note	If the key has no value, a default value will be inserted into the object
		*/
		Json& operator[](const WideChar* key);
		/**
		* @Note	If the key has no value, a default value will be inserted into the object
		*/
		Json& operator[](const AnsiString& key);
		/**
		* @Note	If the key has no value, a default value will be inserted into the object
		*/
		Json& operator[](const WideString& key);
		Json& operator[](u32 idx);

		operator b8()     const { return m_Type == Type::Bool	  ? m_Bool          : false;    }
		operator f64()    const { return m_Type == Type::Float	  ? m_Float         : 0.0;      }
		operator f32()    const { return m_Type == Type::Float	  ? f32(m_Float)    : 0.0f;     }
		operator u64()    const { return m_Type == Type::Unsigned ? u64(m_Unsigned) : 0;        }
		operator u32()    const { return m_Type == Type::Unsigned ? u32(m_Unsigned) : 0;        }
		operator u16()    const { return m_Type == Type::Unsigned ? u16(m_Unsigned) : 0;        }
		operator u8()     const { return m_Type == Type::Unsigned ? u8(m_Unsigned)  : 0;        }
		operator i64()    const { return m_Type == Type::Integer  ? i64(m_Integer)  : 0;        }
		operator i32()    const { return m_Type == Type::Integer  ? i32(m_Integer)  : 0;        }
		operator i16()    const { return m_Type == Type::Integer  ? i16(m_Integer)  : 0;        }
		operator i8()     const { return m_Type == Type::Integer  ? i8(m_Integer)   : 0;        }
		operator String() const { return m_Type == Type::String   ? m_String        : String(); }

		/**
		 * Set the json as a bool
		 * @param[in] val	Value
		 */
		void SetBool(b8 val);
		/**
		* Set the json as a floating point number
		* @param[in] val	Value
		*/
		void SetFloat(f64 val);
		/**
		* Set the json as an integer number
		* @param[in] val	Value
		*/
		void SetInteger(i64 val);
		/**
		* Set the json as an unsigned number
		* @param[in] val	Value
		*/
		void SetUnsigned(u64 val);
		/**
		* Set the json as a string
		* @param[in] val	Value
		*/
		void SetString(const AnsiChar* val);
		/**
		* Set the json as a string
		* @param[in] val	Value
		*/
		void SetString(const WideChar* val);
		/**
		* Set the json as a string
		* @param[in] val	Value
		*/
		void SetString(const AnsiString& val);
		/**
		* Set the json as a string
		* @param[in] val	Value
		*/
		void SetString(const WideString& val);
		/**
		* Set the json as an object
		*/
		void SetObject();
		/**
		* Set the json as an array
		*/
		void SetArray();

		/**
		 * Get the boolean value of the object
		 * @return	Boolean
		 */
		b8 GetBool() const { return m_Type == Type::Bool ? m_Bool : false; }
		/**
		* Get the floating point value of the object
		* @return	Floating point value
		*/
		f64 GetFloat() const { return m_Type == Type::Float ? m_Float : 0.0; }
		/**
		* Get the integer value of the object
		* @return	Integer
		*/
		i64 GetInt() const { return m_Type == Type::Integer ? m_Integer : 0; }
		/**
		* Get the integer value of the object
		* @return	Number
		*/
		u64 GetUsigned() const { return m_Type == Type::Unsigned ? m_Unsigned : 0; }
		/**
		* Get the string value of the object
		* @return	String
		*/
		String GetString() const { return m_Type == Type::String ? m_String : String(); }

		/**
		 * Check if this 'object' is a null object
		 * @return	'Object' is a null object
		 */
		b8 IsNull() const { return m_Type == Type::Null; }
		/**
		 * Check if this 'object' is an object
		 * @return	'Object' is an object
		 */
		b8 IsObject() const { return m_Type == Type::Object; }
		/**
		 * Check if this 'object' is an array
		 * @return	'Object' is an array
		 */
		b8 IsArray() const { return m_Type == Type::Array; }
		/**
		 * Check if this 'object' is a string
		 * @return	'Object' is a string
		 */
		b8 IsString() const { return m_Type == Type::String; }
		/**
		 * Check if this 'object' is a number
		 * @return	'Object' is a number
		 */
		b8 IsNumber() const { return m_Type == Type::Float || m_Type == Type::Integer || m_Type == Type::Unsigned; }
		/**
		 * Check if this 'object' is a number
		 * @return	'Object' is a number
		 */
		b8 IsFloat() const { return m_Type == Type::Float; }
		/**
		 * Check if this 'object' is a number
		 * @return	'Object' is a number
		 */
		b8 IsInteger() const { return m_Type == Type::Integer; }
		/**
		 * Check if this 'object' is a number
		 * @return	'Object' is a number
		 */
		b8 IsUnsigned() const { return m_Type == Type::Unsigned; }
		/**
		 * Check if this 'object' is a bool
		 * @return	'Object' is a bool
		 */
		b8 IsBool() const { return m_Type == Type::Bool; }
		/**
		* Get if this json object's type
		* @return	Json type
		*/
		Type GetType() const { return m_Type; }

		/**
		 * Get the size of the json array
		 * @return	Json array size
		 */
		u32 ArraySize() const { return m_Type == Type::Array ? u32(m_Array.Size()) : 0; }
		/**
		 * Push a json object to the array
		 */
		void ArrayPush(const Json& json);

	private:

		/**
		 * Cleanup the object (string, array, object)
		 */
		void Cleanup();

		Type m_Type;	/**< Json type */
		union
		{
			b8 m_Bool;
			f64 m_Float;
			i64 m_Integer;
			u64 m_Unsigned;
			String m_String;
			DynArray<Json> m_Array;
			HashMap<String, Json> m_SubObjects;
		};
	};

}
#pragma warning(pop)

// Should avaid circular include issues
#include "JsonParser.h"