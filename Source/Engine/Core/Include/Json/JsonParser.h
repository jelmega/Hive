// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// JsonParser.h: Json parser
#pragma once
#include "Json.h"
#include "FileSystem/FileSystem.h"
#include "Containers/Stack.h"

namespace Hv {
	
	namespace JsonParser
	{
		/**
		 * Parse a json file
		 * @param[in] filepath	File path
		 * @return				Json object
		 */
		HIVE_API Json ParseFile(const String& filepath);
		/**
		* Parse a json file
		* @param[in] file	File
		* @return			Json object
		*/
		HIVE_API Json ParseFile(FileSystem::File& file);
		/**
		* Parse json from memory
		* @param[in] data	Data
		* @return			Json object
		*/
		HIVE_API Json ParseFromMemoryA(const DynArray<u8>& data);

		namespace Detail
		{
			struct JsonParseData
			{
				Json json;
				Json* pCurJson;
				DynArray<String> keys;	/**< Keys to current object */
				DynArray<u32> indices;
				String curKey;
				u8 indentCount;
				b8 inArray; 
			};

			HIVE_API void InterpretLine(const String& line, JsonParseData& data);
			HIVE_API void InterpretValue(String& value, JsonParseData& data);

			HIVE_API Json* GetCurrentJson(JsonParseData& data);
		}
	};

}
