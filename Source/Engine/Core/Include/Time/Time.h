// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Time.h: Time include file
#pragma once

#include "DateTime.h"
#include "TimeDuration.h"
#include "Timer.h"
