// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// TimeDuration.h: Time duration
#pragma once
#include "Core/CoreTypes.h"
#include "Containers/ContainerUtils.h"

namespace Hv {
	
	class TimeDuration
	{
	public:
		/**
		 * Create a null time duration
		 */
		TimeDuration();
		/**
		 * Create a time duration from ticks
		 * @param[in] ticks	Ticks
		 */
		TimeDuration(u64 ticks);
		/**
		 * Create a time duration from another time duration
		 * @param[in] dur	TimeDuration
		 */
		TimeDuration(const TimeDuration& dur);
		/**
		 * Move a time duration into this time duration
		 * @param[in] dur	TimeDuration
		 */
		TimeDuration(TimeDuration&& dur) noexcept;

		TimeDuration operator+(const TimeDuration& dur) const;
		TimeDuration operator-(const TimeDuration& dur) const;
		TimeDuration operator*(f64 mod) const;
		TimeDuration operator/(f64 mod) const;

		TimeDuration& operator=(const TimeDuration& dur);
		TimeDuration& operator=(TimeDuration&& dur) noexcept;

		TimeDuration& operator+=(const TimeDuration& dur);
		TimeDuration& operator-=(const TimeDuration& dur);
		TimeDuration& operator*=(f64 mod);
		TimeDuration& operator/=(f64 mod);

		/**
		 * Get the time duration as microseconds
		 * @return	TimeDuration in microseconds
		 */
		f64 AsMicroSeconds() const;
		/**
		 * Get the time duration as miliseconds
		 * @return	TimeDuration in miliseconds
		 */
		f64 AsMiliSeconds() const;
		/**
		 * Get the time duration as seconds
		 * @return	TimeDuration in seconds
		 */
		f64 AsSeconds() const;
		/**
		 * Get the time duration as minutes
		 * @return	TimeDuration in minutes
		 */
		f64 AsMinutes() const;
		/**
		 * Get the time duration as hours
		 * @return	TimeDuration in hours
		 */
		f64 AsHours() const;


	private:
		u64 m_Ticks;
		u64 m_Frequency;
	};

}

#include "Inline/TimeDuration.inl"

HV_DECLARE_CONTAINER_MEMCPY(Hv::TimeDuration)
