// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DateTimeValues.h: Date and time value structure
#pragma once
#include "Core/CoreTypes.h"

namespace Hv {

	struct DateTimeValues
	{
		u16 year;
		u8 month;
		u8 dayOfWeek;
		u8 day;
		u8 hour;
		u8 minute;
		u8 second;
		u16 millisecond;
	};
}
