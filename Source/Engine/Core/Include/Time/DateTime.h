// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DateTime.h: Date and time info
#pragma once
#include "Core/CoreHeaders.h"
#include "String/String.h"
#include "DateTimeValues.h"

namespace Hv {
	
	class DateTime
	{
	public:
		/**
		* Create an empty datetime
		*/
		DateTime();
		/**
		* Create datetime from values
		* @param[in] values	Values
		*/
		DateTime(const DateTimeValues& values);
		/**
		* Create datetime from values
		* @param[in] dateTime	Date time
		*/
		DateTime(const DateTime& dateTime);
		/**
		* Create datetime from values
		* @param[in] dateTime	Date time
		*/
		DateTime(DateTime&& dateTime) noexcept;

		DateTime operator=(const DateTime& dateTime);
		DateTime operator=(DateTime&& dateTime) noexcept;

		b8 operator==(const DateTime& dateTime) const;
		b8 operator!=(const DateTime& dateTime) const;

		b8 operator<(const DateTime& dateTime) const;
		b8 operator<=(const DateTime& dateTime) const;
		b8 operator>(const DateTime& dateTime) const;
		b8 operator>=(const DateTime& dateTime) const;

		/**
		* Get the year from datetime
		* @return Year
		*/
		u16 GetYear() const;
		/**
		* Get the year from datetime
		* @return Year
		*/
		u8 GetMonth() const;
		/**
		* Get the year from datetime
		* @return Year
		*/
		u8 GetDayOfYear() const;
		/**
		* Get the year from datetime
		* @return Year
		*/
		u8 GetDay() const;
		/**
		* Get the year from datetime
		* @return Year
		*/
		u8 GetHour() const;
		/**
		* Get the year from datetime
		* @return Year
		*/
		u8 GetMinute() const;
		/**
		* Get the year from datetime
		* @return Year
		*/
		u8 GetSecond() const;
		/**
		* Get the year from datetime
		* @return Year
		*/
		u16 GetMillisecond() const;

		/**
		* Get the datetime as a formatted string
		* @return	Formatted string
		* @note	Format: YYYY-MM-DD hht:mm:ss with t being AM or PM
		* @note	function with user specified format will be made in the future
		*/
		String GetDefaultFromattedString();

		/**
		* Get the current date time
		* @return	Current date time
		*/
		static DateTime Now();

	private:
		DateTimeValues m_Values;
	};

}

#include "Inline/DateTime.inl"

HV_DECLARE_CONTAINER_MEMCPY(Hv::DateTime);
