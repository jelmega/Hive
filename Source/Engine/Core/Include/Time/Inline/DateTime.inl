// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DateTime.h: Date and time info
#pragma once
#include "Time/DateTime.h"
#include "HAL/HalTime.h"

namespace Hv {
	
	HV_INL DateTime::DateTime()
	{
		Memory::Clear(&m_Values, sizeof(DateTimeValues));
	}

	HV_INL DateTime::DateTime(const DateTimeValues& values)
	{
		Memory::Copy(&m_Values, &values, sizeof(DateTimeValues));
	}

	HV_INL DateTime::DateTime(const DateTime& dateTime)
	{
		Memory::Copy(&m_Values, &dateTime.m_Values, sizeof(DateTimeValues));
	}

	HV_INL DateTime::DateTime(DateTime&& dateTime) noexcept
	{
		Memory::Copy(&m_Values, &dateTime.m_Values, sizeof(DateTimeValues));
	}

	HV_INL DateTime DateTime::operator=(const DateTime& dateTime)
	{
		Memory::Copy(&m_Values, &dateTime.m_Values, sizeof(DateTimeValues));
		return *this;
	}

	HV_INL DateTime DateTime::operator=(DateTime&& dateTime) noexcept
	{
		Memory::Copy(&m_Values, &dateTime.m_Values, sizeof(DateTimeValues));
		return *this;
	}

	HV_INL b8 DateTime::operator==(const DateTime& dateTime) const
	{
		return Memory::Compare(&m_Values, &dateTime.m_Values, sizeof(DateTimeValues)) == 0;
	}

	HV_INL b8 DateTime::operator!=(const DateTime& dateTime) const
	{
		return Memory::Compare(&m_Values, &dateTime.m_Values, sizeof(DateTimeValues)) == 0;
	}

	HV_INL b8 DateTime::operator<(const DateTime& dateTime) const
	{
		if (m_Values.year < dateTime.m_Values.year)
			return true;
		if (m_Values.month < dateTime.m_Values.month)
			return true;
		if (m_Values.day < dateTime.m_Values.day)
			return true;
		if (m_Values.hour < dateTime.m_Values.hour)
			return true;
		if (m_Values.minute < dateTime.m_Values.minute)
			return true;
		if (m_Values.second < dateTime.m_Values.second)
			return true;
		if (m_Values.millisecond < dateTime.m_Values.millisecond)
			return true;

		return false;
	}

	HV_INL b8 DateTime::operator<=(const DateTime& dateTime) const
	{
		if (m_Values.year > dateTime.m_Values.year)
			return false;
		if (m_Values.month > dateTime.m_Values.month)
			return false;
		if (m_Values.day > dateTime.m_Values.day)
			return false;
		if (m_Values.hour > dateTime.m_Values.hour)
			return false;
		if (m_Values.minute > dateTime.m_Values.minute)
			return false;
		if (m_Values.second > dateTime.m_Values.second)
			return false;
		if (m_Values.millisecond > dateTime.m_Values.millisecond)
			return false;

		return true;
	}

	HV_INL b8 DateTime::operator>(const DateTime& dateTime) const
	{
		if (m_Values.year > dateTime.m_Values.year)
			return true;
		if (m_Values.month > dateTime.m_Values.month)
			return true;
		if (m_Values.day > dateTime.m_Values.day)
			return true;
		if (m_Values.hour > dateTime.m_Values.hour)
			return true;
		if (m_Values.minute > dateTime.m_Values.minute)
			return true;
		if (m_Values.second > dateTime.m_Values.second)
			return true;
		if (m_Values.millisecond > dateTime.m_Values.millisecond)
			return true;

		return false;
	}

	HV_INL b8 DateTime::operator>=(const DateTime& dateTime) const
	{
		if (m_Values.year < dateTime.m_Values.year)
			return false;
		if (m_Values.month < dateTime.m_Values.month)
			return false;
		if (m_Values.day < dateTime.m_Values.day)
			return false;
		if (m_Values.hour < dateTime.m_Values.hour)
			return false;
		if (m_Values.minute < dateTime.m_Values.minute)
			return false;
		if (m_Values.second < dateTime.m_Values.second)
			return false;
		if (m_Values.millisecond < dateTime.m_Values.millisecond)
			return false;

		return true;
	}

	HV_FORCE_INL u16 DateTime::GetYear() const
	{
		return m_Values.year;
	}

	HV_FORCE_INL u8 DateTime::GetMonth() const
	{
		return m_Values.month;
	}

	HV_FORCE_INL u8 DateTime::GetDayOfYear() const
	{
		return m_Values.dayOfWeek;
	}

	HV_FORCE_INL u8 DateTime::GetDay() const
	{
		return m_Values.day;
	}

	HV_FORCE_INL u8 DateTime::GetHour() const
	{
		return m_Values.hour;
	}

	HV_FORCE_INL u8 DateTime::GetMinute() const
	{
		return m_Values.minute;
	}

	HV_FORCE_INL u8 DateTime::GetSecond() const
	{
		return m_Values.second;
	}

	HV_FORCE_INL u16 DateTime::GetMillisecond() const
	{
		return m_Values.millisecond;
	}

	HV_INL String DateTime::GetDefaultFromattedString()
	{
		String str;
		u8 hour = m_Values.hour;
		const AnsiChar* dayPostfix = "AM";
		if (hour > 12)
		{
			hour -= 12;
			dayPostfix = "PM";
		}
		if (hour == 0)
			hour = 12;
		str.FormatF(String("%u-%02u-%02u %02u%s-%02u-%02u"), m_Values.year, m_Values.month, m_Values.day, hour, dayPostfix, m_Values.minute, m_Values.second);
		return str;
	}

	HV_INL DateTime DateTime::Now()
	{
		return DateTime(HAL::Time::GetCurrentDataTime());
	}

}
