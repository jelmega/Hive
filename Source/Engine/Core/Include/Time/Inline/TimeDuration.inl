// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// TimeDuration.inl: Time duration
#pragma once
#include "Time/TimeDuration.h"
#include "HAL/HalTime.h"

namespace Hv {
	
	HV_INL TimeDuration::TimeDuration()
		: m_Ticks(0)
		, m_Frequency(HAL::Time::GetTickFrequency())
	{
	}

	HV_INL TimeDuration::TimeDuration(u64 ticks)
		: m_Ticks(ticks)
		, m_Frequency(HAL::Time::GetTickFrequency())
	{
	}

	HV_INL TimeDuration::TimeDuration(const TimeDuration& dur)
		: m_Ticks(dur.m_Ticks)
		, m_Frequency(dur.m_Frequency)
	{
	}

	HV_INL TimeDuration::TimeDuration(TimeDuration&& dur) noexcept
		: m_Ticks(dur.m_Ticks)
		, m_Frequency(dur.m_Frequency)
	{
	}

	HV_INL TimeDuration TimeDuration::operator+(const TimeDuration& dur) const
	{
		return TimeDuration(m_Ticks + dur.m_Ticks);
	}

	HV_INL TimeDuration TimeDuration::operator-(const TimeDuration& dur) const
	{
		return TimeDuration(m_Ticks - dur.m_Ticks);
	}

	HV_INL TimeDuration TimeDuration::operator*(f64 mod) const
	{
		return TimeDuration(u64(m_Ticks * mod));
	}

	HV_INL TimeDuration TimeDuration::operator/(f64 mod) const
	{
		return TimeDuration(u64(m_Ticks / mod));
	}

	HV_INL TimeDuration& TimeDuration::operator=(const TimeDuration& dur)
	{
		m_Ticks = dur.m_Ticks;
		m_Frequency = dur.m_Frequency;
		return *this;
	}

	HV_INL TimeDuration& TimeDuration::operator=(TimeDuration&& dur) noexcept
	{
		m_Ticks = dur.m_Ticks;
		m_Frequency = dur.m_Frequency;
		return *this;
	}

	HV_INL TimeDuration& TimeDuration::operator+=(const TimeDuration& dur)
	{
		m_Ticks += dur.m_Ticks;
		return *this;
	}

	HV_INL TimeDuration& TimeDuration::operator-=(const TimeDuration& dur)
	{
		m_Ticks -= dur.m_Ticks;
		return *this;
	}

	HV_INL TimeDuration& TimeDuration::operator*=(f64 mod)
	{
		m_Ticks = u64(m_Ticks * mod);
		return *this;
	}

	HV_INL TimeDuration& TimeDuration::operator/=(f64 mod)
	{
		m_Ticks = u64(m_Ticks / mod);
		return *this;
	}

	HV_INL f64 TimeDuration::AsMicroSeconds() const
	{
		return (f64(m_Ticks) / f64(m_Frequency)) * 1'000'000;
	}

	HV_INL f64 TimeDuration::AsMiliSeconds() const
	{
		return (f64(m_Ticks) / f64(m_Frequency)) * 1'000;
	}

	HV_INL f64 TimeDuration::AsSeconds() const
	{
		return f64(m_Ticks) / f64(m_Frequency);
	}

	HV_INL f64 TimeDuration::AsMinutes() const
	{
		return (f64(m_Ticks) / f64(m_Frequency)) / 60.0;
	}

	HV_INL f64 TimeDuration::AsHours() const
	{
		return (f64(m_Ticks) / f64(m_Frequency)) / 3'600.0;
	}

}