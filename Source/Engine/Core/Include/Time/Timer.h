// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Timer.h: Time duration
#pragma once
#include "TimeDuration.h"

namespace Hv {
	
	/**
	 * Timer
	 */
	class HIVE_API Timer
	{
	public:
		/**
		 * Create a new timer
		 * @param[in] start	It the timer needs to start on creation
		 */
		Timer(b8 start = false);
		/**
		 * Create a timer from another timer
		 * @param[in] timer	Timer
		 */
		Timer(const Timer& timer);
		/**
		 * move a timer into this timer
		 * @param[in] timer	Timer
		 */
		Timer(Timer&& timer) noexcept;
		~Timer();

		Timer& operator=(const Timer& timer);
		Timer& operator=(Timer&& timer) noexcept;

		/**
		 * Start the timer
		 */
		void Start();
		/**
		 * Reset the timer (and keep the timer running)
		 * @return	TimeDuration at the time of resetting
		 */
		TimeDuration Reset();
		/**
		 * Stop the timer (and reset timer)
		 * @return	TimeDuration at the time of stopping
		 */
		TimeDuration Stop();
		/**
		 * Pause the timer
		 * @return	TimeDuration at the time of pauseing
		 */
		TimeDuration Pause();
		/**
		 * Get the current duration that has passed
		 * @return TimeDuration that has passed since the start of the timer
		 */
		TimeDuration GetCurrentDuration() const;

	private:
		u64 m_StartTicks;
		u64 m_PauseTicks;
		u64 m_PauseDiff;
		b8 m_Running;
		b8 m_Paused;
	};

}
