// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// SystemConsole.h: System console
#pragma once
#include "Core/CoreHeaders.h"
#include "String/String.h"
#include "HAL/HalConsole.h"

namespace Hv::System {
	
	enum class ConsoleColor : u8
	{
		ForeBlack = 0x00,
		ForeDarkBlue = 0x01,
		ForeDarkGreen = 0x02,
		ForeDarkCyan = 0x03,
		ForeDarkRed = 0x04,
		ForeDarkMagenta = 0x05,
		ForeDarkYellow = 0x06,
		ForeGrey = 0x07,
		ForeIntensity = 0x08,
		ForeLightGrey = 0x08,
		ForeBlue = 0x09,
		ForeGreen = 0x0A,
		ForeCyan = 0x0B,
		ForeRed = 0x0C,
		ForeMagenta = 0x0D,
		ForeYellow = 0x0E,
		ForeWhite = 0x0F,
		BackBlack = 0x00,
		BackDarkBlue = 0x10,
		BackDarkGreen = 0x20,
		BackDarkCyan = 0x30,
		BackDarkRed = 0x40,
		BackDarkMagenta = 0x50,
		BackDarkYellow = 0x60,
		BackGrey = 0x70,
		BackIntensity = 0x80,
		BackLightGrey = 0x80,
		BackBlue = 0x90,
		BackGreen = 0xA0,
		BackCyan = 0xB0,
		BackRed = 0xC0,
		BackMagenta = 0xD0,
		BackYellow = 0xE0,
		BackWhite = 0xF0,
	};
	HV_ENABLE_ENUM_FLAG_OPERATORS(ConsoleColor);

	class HIVE_API Console
	{
	public:
		/**
		* Create a console
		*/
		Console();
		~Console();

		/**
		* Write a string to the console
		* @param[in] str	String to write
		*/
		void Write(const String& str);
		/**
		* Write a string to the console and add a new line
		* @param[in] str	String to write
		*/
		void WriteLine(const String& str);

		/**
		* Read a line from the console
		* @return	Line
		*/
		String ReadLine();

		/**
		* Set the color of the console text
		* @param[in] color	Color
		*/
		void SetConsoleColor(ConsoleColor color);
		/**
		* Reset the color of the console text
		*/
		void ResetConsoleColor();

	private:
		HV_MAKE_NON_COPYABLE(Console);

		System::ConsoleHandle m_Handle;							/**< Console handle */

		static Threading::CriticalSection m_CriticalSection;	/**< Critical section */
	};

	HIVE_API Console& GetConsole();

}

#define g_Console Hv::System::GetConsole()