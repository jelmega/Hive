// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HalSystem.h: HAL system
#pragma once
#include "HAL/HalSystem.h"
#include "String/String.h"

namespace Hv::System {
	
	/**
	 * Check if a debugger is attached
	 * @return	True if a debugger is attached, false otherwise
	 */
	b8 IsDebuggerAttached();

	/**
	 * Log a message to the debuggers output
	 * @param[in] text	Text to log
	 */
	void LogDebugger(const AnsiString& text);

	/**
	 * Log a message to the debuggers output
	 * @param[in] text	Text to log
	 */
	void LogDebugger(const WideString& text);

	/**
	 * Update the system
	 * @return	If the system was updated successfully, false otherwise
	 */
	b8 TickSystem();

}

#include "Inline/CoreSystem.inl"
