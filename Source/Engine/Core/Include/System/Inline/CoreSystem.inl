// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HalSystem.h: HAL system
#pragma once
#include "System/CoreSystem.h"

namespace Hv::System {
	
	HV_FORCE_INL b8 System::IsDebuggerAttached()
	{
		return HAL::System::IsDebuggerAttached();
	}

	HV_FORCE_INL void System::LogDebugger(const AnsiString& text)
	{
		HAL::System::LogDebugger(text.CStr());
	}

	HV_FORCE_INL void System::LogDebugger(const WideString& text)
	{
		HAL::System::LogDebugger(text.CStr());
	}

	HV_FORCE_INL b8 TickSystem()
	{
		return HAL::System::TickSystem();
	}

}
