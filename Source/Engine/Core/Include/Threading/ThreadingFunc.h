// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ThreadingFunc.h: Threading global functions
#pragma once
#include "HAL/HalThreading.h"
#include "Logging/Logger.h"

HV_DECLARE_LOG_CATEGORY_EXTERN(Threading);

namespace Hv::Threading {

	/**
	* Get the id of the current thread
	* @return	Id of the current thread
	*/
	HIVE_API HV_FORCE_INL u32 GetCurrentThreadId() { return HAL::Threading::GetCurrentThreadId(); }

	/**
	* Suspend execution on the current thread for a time-interval
	* @param[in] ms		Milliseconds to sleep
	*/
	HIVE_API HV_FORCE_INL void Sleep(u32 ms) { HAL::Threading::Sleep(ms); }
	/**
	 * Get the amount of logical cores in the system
	 * @return	Number of logical cores
	 */
	HIVE_API HV_FORCE_INL u32 GetLogicalCoreCount() { return HAL::Threading::GetLogicalCoreCount(); }

}