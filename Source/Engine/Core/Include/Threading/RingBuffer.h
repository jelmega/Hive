// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// RingBuffer.h: Ring buffer
#pragma once
#include "Core/CoreHeaders.h"
#include "Atomic.h"
#include "Containers/Array.h"

namespace Hv::Threading {

#define HV_RINGBUFFER_ANY_THREAD 0xFFFF'FFFF

	template<typename T, sizeT Slots>
	class RingBuffer
	{
	public:
		/**
		 * Create a ringbuffer
		 * @param[in] writerId	Id of the writing thread, or HV_RINGBUFFER_ANY_THREAD
		 * @param[in] readerId Id of the reading thread, or HV_RINGBUFFER_ANY_THREAD
		 */
		RingBuffer(u32 writerId, u32 readerId);
		~RingBuffer();

		/**
		 * Write a value to the ring buffer
		 * @param[in] val	Value to write
		 * @return	True if the value was written to the ringbuffer, false otherwise
		 */
		b8 Write(const T& val);

		/**
		 * Read a value from the ring buffer
		 * @param[out] val	Value to read to
		 * @return		True if the value was read from the ringbuffer, false otherwise
		 */
		b8 Read(T& val);

	private:
		Array<T, Slots> m_Array;			/**< Internal array */
		const u32 m_WriterThreadId;			/**< Writer thread id */
		const u32 m_ReaderThreadId;			/**< Reader thread id */
		Atomic<sizeT> m_WriteLoc;			/**< Write location */
		Atomic<sizeT> m_ReadLoc;			/**< Read location */
		CriticalSection m_ReadCritSect;		/**< Read critical section (for HV_RINGBUFFER_ANY_THREAD) */
		CriticalSection m_WriteCritSect;	/**< Write critical section (for HV_RINGBUFFER_ANY_THREAD) */
	};
}

#include "Inline/RingBuffer.inl"
