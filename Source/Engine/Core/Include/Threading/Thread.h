// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Thread.h: Thread
#pragma once
#include "Core/CoreHeaders.h"
#include "HAL/HalThreading.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv::Threading {
	
	class HIVE_API Thread
	{
	public:
		/**
		 * Create a thread object
		 * @param[in] debugName		Debug name
		 */
		Thread(const WideChar* debugName = L"");
		~Thread();

		Thread& operator=(Thread&& thread);

		/**
		 * Create the thread and start executing a callback
		 * @param[in] delegate		Delegate
		 * @return					True if the thread was created successfully, false otherwise
		 */
		b8 Create(Delegate<b8()> delegate);

		/**
		 * Destroy the thread
		 * @return	True if the thread was destroyed successfully, false otherwise
		 */
		b8 Destroy();

		/**
		 * Terminate the thread
		 * @return	True if the thread was terminated correctly, false otherwise
		 */
		b8 Terminate();

		/**
		* Wait for the thread to finish
		*/
		void Wait();

		/**
		 * Get the thread's id
		 * @return	Thread id
		 */
		u32 GetId() const { return m_Id; }

	private:
		HV_MAKE_NON_COPYABLE(Thread);

		ThreadHandle m_Handle;			/**< Thread handle */
		u32 m_Id;						/**< Thread id */
		const WideChar* m_Name;			/**< Debug name */
		Delegate<b8()> m_Delegate;		/**< Thread callback */
	};

}

#pragma warning(pop)