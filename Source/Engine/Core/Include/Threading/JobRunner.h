// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// JobRunner.h: Job runner
#pragma once
#include "Core/CoreHeaders.h"

namespace Hv::Threading {

	class JobSystem;
	
	class JobRunner
	{
	public:
		/**
		 * Create a job system
		 * @param[in] pJobSystem	Job system
		 */
		JobRunner(JobSystem* pJobSystem);
		~JobRunner();

		/**
		 * Run the job runner
		 * @return			True if the runner ran successfully, false otherwise
		 */
		b8 Run();

		/**
		 * Terminate the job runner
		 */
		void Terminate();

	private:
		JobSystem* m_pJobSystem;	/**< Job system */
		b8 m_Running;				/**< If the runner is running */
	};

}