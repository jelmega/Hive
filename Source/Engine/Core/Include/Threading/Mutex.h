// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Mutex.h: mutex
#pragma once
#include "Core/CoreHeaders.h"
#include "HAL/HalThreading.h"

namespace Hv::Threading {
	
	/**
	 * Mutex
	 * @note	Mutexed can be shared between processes, which can causes locking to be slower, use critical section when sharing between processes is not needed
	 */
	class HIVE_API Mutex
	{
	public:
		/**
		* Create a thread object
		* @param[in] name	Name
		*/
		Mutex(const WideChar* name);
		~Mutex();

		/**
		 * Lock the mutex
		 * @note	This function will block the app until the mutex has been aquired
		 */
		void Lock();
		/**
		* Try to lock the mutex
		* @param[in] timeout	Time out
		* @return				True if the mutex could be locked, false otherwise
		*/
		b8 TryLock(u32 timeout = 1);
		/**
		 * Unlock the mutex
		 */
		void Unlock();

	private:
		HV_MAKE_NON_COPYABLE(Mutex);

		MutexHandle m_Handle;	/**< Mutex handle */
		const WideChar* m_Name;	/**< Name */
	};

}
