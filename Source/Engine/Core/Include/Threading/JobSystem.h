// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// JobSystem.h: Job system
#pragma once
#include "Core/CoreHeaders.h"
#include "IJob.h"
#include "Containers/DynArray.h"
#include "Thread.h"
#include "Containers/Queue.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv::Threading {
	
	class JobRunner;

	class HIVE_API JobSystem
	{
	public:
		JobSystem();
		~JobSystem();

		/**
		 * Create the job system
		 * @param[in] numThreads	Number of threads to use
		 * @return					True if the job system was initialized successfully, false otherwise
		 */
		b8 Init(u8 numThreads);
		/**
		 * Shut down the job system
		 * @return	True if the job system was initialized successfully, false otherwise
		 */
		b8 Shutdown();

		/**
		 * Add a job to job system
		 * @param[in] pJob			Job
		 * @note					Additional data can be added to the derived job
		 */
		void AddJob(IJob* pJob);

		/**
		 * Tick the job system and wait if the frame cannot yet contine
		 */
		void Tick();

		/**
		 * Request a job from the job system
		 * @return	Job info, when pJob is a nullptr, no job is available
		 */
		IJob* RequestJob();

	private:
		CriticalSection m_CritSec;		/**< Critical section */

		DynArray<Thread> m_Threads;		/**< Job threads */
		DynArray<JobRunner*> m_Runners;	/**< Job runners */

		// Single frame jobs
		Queue<IJob*> m_HighPriory;
		Queue<IJob*> m_MediumPriory;
		Queue<IJob*> m_LowPriory;
	};

	HIVE_API JobSystem& GetJobSystem();

}

#define g_JobSystem Hv::Threading::GetJobSystem()

#pragma warning(pop)