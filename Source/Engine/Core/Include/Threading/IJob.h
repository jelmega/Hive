// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// IJob.h: Job interface
#pragma once
#include "Core/CoreHeaders.h"

namespace Hv::Threading {
	
	class HIVE_API IJob
	{
	public:
		enum class Priority : u8
		{
			High,	/**< Jobs have priority and will be finished the same frame */
			Medium,	/**< Jobs will be guarenteed to be executed in 3 frames */
			Low		/**< Jobs will be guarenteed to be executed in 30 frames */
		};

	public:
		/**
		 * Create a job
		 * @param[in] priority				Job priority
		 * @param[in] destroyOnCompletion	If the job needs to be destroyed when it's completed
		 */
		IJob(Priority priority, b8 destroyOnCompletion = false);
		virtual ~IJob();

		/**
		 * Execute the job
		 * @return	True if the job finished successfully, false otherwise
		 */
		virtual b8 Execute() = 0;

		/**
		 * Wait until the job is finised
		 */
		void AwaitCompleted();
		/**
		 * Get the number of frames left before the job needs to be executed
		 * @return	Frames left
		 */
		u8 GetFramesLeft() const { return m_FramesLeft; }
		/**
		 * Tick the amount of frames left
		 */
		void TickFramesLeft() { if (m_FramesLeft) --m_FramesLeft; }
		/**
		 * Get the job priority
		 * @return	Job priority
		 */
		Priority GetJobPriority() const { return m_Priority; }
		/**
		* [INTERNAL] Set the job as completed
		*/
		void CompleteJob() { m_Completed = true; }
		/**
		* Check if the job is completed
		* @return	True if the job is completed, false otherwise
		*/
		b8 IsJobCompleted() const { return m_Completed; }
		/**
		* Check if the job nneds to be deleted when on completion
		* @return	True if the job needs to be deleted on completion, false otherwise
		*/
		b8 NeedsDeleteOnCompletion() { return m_DestroyOnCompletion; }

	private:
		HV_MAKE_NON_COPYABLE(IJob);
		friend class JobSystem;

		const Priority m_Priority;		/**< Job priority */
		const b8 m_DestroyOnCompletion;	/**< If the job need to be completed when finished */
		u8 m_FramesLeft;				/**< Frames left until jobs need to be completed */
		b8 m_Completed;					/**< If the job has been completed */

		static constexpr u8 m_MedPriorityFrames = 3;
		static constexpr u8 m_LowPriorityFrames = 30;
	};

}
