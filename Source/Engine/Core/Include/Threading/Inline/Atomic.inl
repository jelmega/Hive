// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Atomic.h: Atomic
#pragma once
#include "Threading/Atomic.h"
#include "HAL/HalThreading.h"

namespace Hv::Threading {
	
	template <typename T>
	Atomic<T>::Atomic()
	{
	}

	template <typename T>
	Atomic<T>::Atomic(T val)
	{
		Store(val);
	}

	template <typename T>
	Atomic<T>::~Atomic()
	{
	}

	template <typename T>
	T Atomic<T>::operator++(int)
	{
		return HAL::Threading::AtomicIncrement(m_Val);
	}

	template <typename T>
	T Atomic<T>::operator++()
	{
		T oldVal = m_Val;
		HAL::Threading::AtomicIncrement(m_Val);
		return oldVal;
	}

	template <typename T>
	T Atomic<T>::operator--(int)
	{
		return HAL::Threading::AtomicDecrement(m_Val);
	}

	template <typename T>
	T Atomic<T>::operator=(T val)
	{
		return Store(val);
	}

	template <typename T>
	T Atomic<T>::operator--()
	{
		T oldVal = m_Val;
		HAL::Threading::AtomicDecrement(m_Val);
		return oldVal;
	}

	template <typename T>
	T Atomic<T>::operator+=(T val)
	{
		return HAL::Threading::AtomicAddFetch(m_Val, val);
	}

	template <typename T>
	T Atomic<T>::operator-=(T val)
	{
		return HAL::Threading::AtomicSubFetch(m_Val, val);
	}

	template <typename T>
	Atomic<T>::operator T()
	{
		return Load;
	}

	template <typename T>
	T Atomic<T>::Store(T val)
	{
		return HAL::Threading::AtomicLoad(val);
	}

	template <typename T>
	T Atomic<T>::Load()
	{
		return HAL::Threading::AtomicLoad(m_Val);
	}


	////////////////////////////////////////////////////////////////////////////////
	// Specializations															  //
	////////////////////////////////////////////////////////////////////////////////
	// Disable b8 operators
	template<>
	HV_FORCE_INL b8 Atomic<b8>::operator++() { return m_Val; }
	template<>
	HV_FORCE_INL b8 Atomic<b8>::operator++(int) { return m_Val; }
	template<>
	HV_FORCE_INL b8 Atomic<b8>::operator--() { return m_Val; }
	template<>
	HV_FORCE_INL b8 Atomic<b8>::operator--(int) { return m_Val; }
	template<>
	HV_FORCE_INL b8 Atomic<b8>::operator+=(b8 val) { return m_Val; }
	template<>
	HV_FORCE_INL b8 Atomic<b8>::operator-=(b8 val) { return m_Val; }

}
