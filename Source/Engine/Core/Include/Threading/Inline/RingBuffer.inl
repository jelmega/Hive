// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// RingBuffer.h: Ring buffer
#pragma once
#include "Threading/RingBuffer.h"
#include "Threading/ThreadingFunc.h"

namespace Hv::Threading {

	template <typename T, sizeT Slots>
	RingBuffer<T, Slots>::RingBuffer(u32 writerId, u32 readerId)
		: m_WriterThreadId(writerId)
		, m_ReaderThreadId(readerId)
		, m_WriteLoc(0)
		, m_ReadLoc(0)
		, m_WriteCritSect(5) // 5 spins, should be enough for small pieces of data
		, m_ReadCritSect(5) // 5 spins, should be enough for small pieces of data
	{
	}

	template <typename T, sizeT Slots>
	RingBuffer<T, Slots>::~RingBuffer()
	{
	}

	template <typename T, sizeT Slots>
	b8 RingBuffer<T, Slots>::Write(const T& val)
	{
		if (m_WriterThreadId == HV_RINGBUFFER_ANY_THREAD)
		{
			m_WriteCritSect.Enter();
			if ((m_WriteLoc + 1) % Slots != m_ReadLoc)
			{
				m_Array[m_WriteLoc] = val;
				if (m_WriteLoc == Slots - 1)
					m_WriteLoc = 0;
				else
					++m_WriteLoc;
				m_WriteCritSect.Leave();
				return true;
			}
			m_WriteCritSect.Leave();
		}
		else
		{
			u32 threadId = GetCurrentThreadId();
			if (threadId != m_WriterThreadId)
			{
				g_Logger.LogFormat(LogThreading, LogLevel::Error, "Trying to write to ringbuffer from invalid thread (thread id: %u, expected %u)", threadId, m_WriterThreadId);
				return false;
			}

			if ((m_WriteLoc + 1) % Slots != m_ReadLoc)
			{
				m_Array[m_WriteLoc] = val;
				if (m_WriteLoc == Slots - 1)
					m_WriteLoc = 0;
				else
					++m_WriteLoc;
				return true;
			}
		}
		return false;
	}

	template <typename T, sizeT Slots>
	b8 RingBuffer<T, Slots>::Read(T& val)
	{
		if (m_ReaderThreadId == HV_RINGBUFFER_ANY_THREAD)
		{
			m_ReadCritSect.Enter();
			u32 readLoc = m_ReadLoc;
			if (m_ReadLoc != m_WriteLoc)
			{
				val = m_Array[m_ReadLoc] = val;
				if (m_ReadLoc == Slots - 1)
					m_ReadLoc = 0;
				else
					++m_ReadLoc;
				m_ReadCritSect.Leave();
				return true;
			}
			m_ReadCritSect.Leave();
		}
		else
		{
			u32 threadId = GetCurrentThreadId();
			if (threadId != m_ReaderThreadId)
			{
				g_Logger.LogFormat(LogThreading, LogLevel::Error, "Trying to write to ringbuffer from invalid thread (thread id: %u, expected %u)", threadId, m_WriterThreadId);
				return false;
			}

			if (m_ReadLoc != m_WriteLoc)
			{
				val = m_Array[m_ReadLoc];
				if (m_ReadLoc == Slots - 1)
					m_ReadLoc = 0;
				else
					++m_ReadLoc;
				return true;
			}
		}
		return false;
	}

}