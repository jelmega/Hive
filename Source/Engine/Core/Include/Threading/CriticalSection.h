// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// CriticalSection.h: Critical section
#pragma once
#include "Core/CoreHeaders.h"
#include "HAL/HalThreading.h"

namespace Hv::Threading {

	/**
	 * Critical section
	 * @note	Same function as a mutex, but not sharable across applications
	 */
	class HIVE_API CriticalSection
	{
	public:
		/**
		* Create a critical section
		*/
		CriticalSection();
		/**
		 * Create a critical section with a spin
		 * @param[in] spinCount		Spin count
		 */
		explicit CriticalSection(u32 spinCount);
		~CriticalSection();

		/**
		 * Enter the critical section
		 */
		void Enter();
		/**
		* Try to enter the critical section
		* @return	True if the critical section was entered, false otherwise
		*/
		b8 TryEnter();
		/**
		 * Leave the critical section
		 */
		void Leave();

		/**
		 * Get the critical section's spin count
		 * @return	Spin count
		 */
		u32 GetSpinCount() const { return m_SpinCount; }

	private:
		HV_MAKE_NON_COPYABLE(CriticalSection);

		CriticalSectionHandle m_Handle; /**< Critical section handle */
		u32 m_SpinCount;				/**< Spin count */
	};

}
