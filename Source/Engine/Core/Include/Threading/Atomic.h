// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Atomic.h: Atomic
#pragma once
#include "Core/CoreHeaders.h"

namespace Hv::Threading {
	
	// TODO: Expend functionality
	/**
	 * Atomic variable
	 * @note	Currently, the atomic value is not yet finished, causeing it to miss some functionality
	 */
	template<typename T>
	class Atomic
	{
		HV_STATIC_ASSERT_MSG(sizeof(T) == 1 || sizeof(T) == 2 || sizeof(T) == 4 || sizeof(T) == 8, 
			"Only values of 1, 2, 4 or 8 bytes can be atomic! Use other syncronization primitives instead!");

		HV_STATIC_ASSERT_MSG(Traits::IsFundamentalV<T>, "Currently, only fundamental types are allowed!");
	public:
		Atomic();
		explicit Atomic(T val);
		~Atomic();

		T operator++();
		T operator++(int);
		T operator--();
		T operator--(int);

		T operator=(T val);
		T operator+=(T val);
		T operator-=(T val);

		operator T();

		/**
		* Store a value
		* @param[in] val	Value to store
		* @return			Stored value
		*/
		T Store(T val);
		/**
		 * Load the atomic value
		 * @return	Value
		 */
		T Load();

	private:
		alignas(sizeof(T)) T m_Val;
	};

	using AtomicB8  = Atomic<b8>;
	using AtomicI8  = Atomic<i8>;
	using AtomicI16 = Atomic<i16>;
	using AtomicI32 = Atomic<i32>;
	using AtomicI64 = Atomic<i64>;
	using AtomicU8  = Atomic<u8>;
	using AtomicU16 = Atomic<u16>;
	using AtomicU32 = Atomic<u32>;
	using AtomicU64 = Atomic<u64>;
}
