// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Threading.h: Threading main header
#pragma once
#include "Thread.h"
#include "Mutex.h"
#include "CriticalSection.h"
#include "ThreadingFunc.h"
#include "RingBuffer.h"

#include "IJob.h"
#include "JobSystem.h"
