// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Rect.h: Rectangle
#pragma once
#include "Core/CoreHeaders.h"
#include "Containers/ContainerUtils.h"

namespace Hv::Math {

	template<typename T>
	struct Rect
	{
#pragma warning(push)
#pragma warning(disable: 4201) // nonstandard extension used : nameless struct/union
		union
		{
			struct
			{
				T left, top, right, bottom;
			};
			struct
			{
				Vec2<T> topLeft, bottomRight;
			};
		};
#pragma warning(pop)

		/**
		* Create a rect
		*/
		Rect();
		/**
		* Create a rect from values
		* @param[in] left	Rect left
		* @param[in] top	Rect top
		* @param[in] right	Rect right
		* @param[in] bottom	Rect bottom
		*/
		template<typename T0, typename T1, typename T2, typename T3>
		Rect(T0 left, T1 top, T2 right, T3 bottom);
		/**
		* Create a rect from 2 vectors
		* @param[in] topLeft		Rect top left
		* @param[in] bottomRight	Rect bottom right
		*/
		template<typename T0, typename T1>
		Rect(const Vec2<T0>& topLeft, const Vec2<T>& bottomRight);
		/**
		* Create a rect from another rect
		* @param[in] rc	Rect
		*/
		template<typename T0>
		Rect(const Rect<T0>& rc);
		/**
		* Create a rect from another rect
		* @param[in] rc	Rect
		*/
		Rect(const Rect& rc);
		/**
		* Move a rect into this rect
		* @param[in] rc	Rect
		*/
		Rect(Rect&& rc) noexcept;

		template<typename T0>
		Rect& operator=(const Rect<T0>& rc);
		Rect& operator=(const Rect& rc);
		Rect& operator=(Rect&& rc) noexcept;

	};

}

HV_DECLARE_CONTAINER_MEMCPY_TEMPLATE(HV_TARGS(typename T), HV_TTYPE(Hv::Math::Rect, T));
