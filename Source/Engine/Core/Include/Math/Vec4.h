// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Vec4.h: 4D Vector
#pragma once
#include "Core/CoreHeaders.h"
#include "Containers/ContainerUtils.h"

namespace Hv::Math {
	
	template<typename T>
	struct Vec3;

	template<typename T>
	struct Vec2;

	template<typename T>
	struct Mat4;

	template<typename T>
	struct Vec4
	{
#pragma warning(push)
#pragma warning(disable: 4201) // nonstandard extension used : nameless struct/union
		union
		{
			T data[4];
			struct { T x, y, z, w; };
		};
#pragma warning(pop)

		/**
		 * Create a default vec4
		 */
		Vec4();
		/**
		 * Create a vec4 with a value
		 * @tparam V		Arithmatic type
		 * @param[in] val	Value
		 */
		template<typename V>
		Vec4(V val);
		/**
		 * Create a vec4
		 * @tparam X	Arithmatic type
		 * @tparam Y	Arithmatic type
		 * @tparam Z	Arithmatic type
		 * @tparam W	Arithmatic type
		 * @param[in] x X value
		 * @param[in] y Y value
		 * @param[in] z Z value
		 * @param[in] w W value
		 */
		template<typename X, typename Y, typename Z, typename W>
		Vec4(X x, Y y, Z z, W w);
		/**
		 * Create a vec4 from another vec4
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vec4
		 */
		template<typename V>
		Vec4(const Vec4<V>& v);
		/**
		 * Create a vec4 from a vec3
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vec3
		 */
		template<typename V>
		Vec4(const Vec3<V>& v);
		/**
		 * Create a vec4 from a vec3 and a value
		 * @tparam V		Arithmatic type
		 * @tparam W		Arithmatic type
		 * @param[in] xyz	XYZ vector
		 * @param[in] w		W value
		 */
		template<typename V, typename W>
		Vec4(const Vec3<V>& xyz, W w);
		/**
		 * Create a vec4 from a vec3 and a value
		 * @tparam X		Arithmatic type
		 * @tparam V		Arithmatic type
		 * @param[in] x		X value
		 * @param[in] yzw	YZW vector
		 */
		template<typename X, typename V>
		Vec4(X x, const Vec3<V>& yzw);
		/**
		 * Create a vec4 from a vec2
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vec2
		 */
		template<typename V>
		Vec4(const Vec2<V>& v);
		/**
		 * Create a vec4 from a vec2 and 2 values
		 * @tparam V		Arithmatic type
		 * @tparam Z		Arithmatic type
		 * @tparam W		Arithmatic type
		 * @param[in] xy	XY vector
		 * @param[in] z		Z value
		 * @param[in] w		W value
		 */
		template<typename V, typename Z, typename W>
		Vec4(const Vec2<V>& xy, Z z, W w);
		/**
		 * Create a vec4 from a vec2 and 2 values
		 * @tparam X		Arithmatic type
		 * @tparam V		Arithmatic type
		 * @tparam W		Arithmatic type
		 * @param[in] x		X value
		 * @param[in] yz	YZ vector
		 * @param[in] w		W value
		 */
		template<typename X, typename V, typename W>
		Vec4(X x, const Vec2<V>& yz, W w);
		/**
		 * Create a vec4 from a vec2 and 2 values
		 * @tparam X		Arithmatic type
		 * @tparam Y		Arithmatic type
		 * @tparam V		Arithmatic type
		 * @param[in] x		X value
		 * @param[in] y		Y value
		 * @param[in] zw	ZW vector
		 */
		template<typename X, typename Y, typename V>
		Vec4(X x, Y y, const Vec2<V>& zw);/**
										   * Create a vec4 from 2 vec2s
										   * @tparam V0		Arithmatic type
										   * @tparam V1		Arithmatic type
										   * @param[in] xy	XY vector
										   * @param[in] zw	ZW vector
										   */
		template<typename V0, typename V1>
		Vec4(const Vec2<V0>& xy, const Vec2<V1>& zw);
		/**
		 * Create a vec4 from another vec4
		 * @param[in] v	Vec4
		 */
		Vec4(const Vec4& v);
		/**
		 * Move a vec4 into this vec4
		 * @param[in] v	Vec4
		 */
		Vec4(Vec4&& v) noexcept;

		T operator[](sizeT index) const;

		Vec4 operator+() const;
		Vec4 operator-() const;
		Vec4 operator~() const;

		template<typename V>
		Vec4 operator+(V v) const;
		template<typename V>
		Vec4 operator+(const Vec4<V>& v) const;
		template<typename V>
		Vec4 operator-(V v) const;
		template<typename V>
		Vec4 operator-(const Vec4<V>& v) const;
		template<typename V>
		Vec4 operator*(V v) const;
		template<typename V>
		Vec4 operator*(const Vec4<V>& v) const;
		template<typename V>
		Vec4 operator/(V v) const;
		template<typename V>
		Vec4 operator/(const Vec4<V>& v) const;
		template<typename V>
		Vec4 operator|(V v) const;
		template<typename V>
		Vec4 operator|(const Vec4<V>& v) const;
		template<typename V>
		Vec4 operator&(V v) const;
		template<typename V>
		Vec4 operator&(const Vec4<V>& v) const;
		template<typename V>
		Vec4 operator^(V v) const;
		template<typename V>
		Vec4 operator^(const Vec4<V>& v) const;
		template<typename V>
		Vec4 operator<<(V v) const;
		template<typename V>
		Vec4 operator<<(const Vec4<V>& v) const;
		template<typename V>
		Vec4 operator>>(V v) const;
		template<typename V>
		Vec4 operator>>(const Vec4<V>& v) const;

		template<typename V>
		Vec4 operator*(const Mat4<V>& m) const;

		template<typename V>
		Vec4& operator=(const Vec4<V>& v);
		Vec4& operator=(const Vec4& v);
		Vec4& operator=(Vec4&& v) noexcept;

		template<typename V>
		Vec4& operator+=(V v);
		template<typename V>
		Vec4& operator+=(const Vec4<V>& v);
		template<typename V>
		Vec4& operator-=(V v);
		template<typename V>
		Vec4& operator-=(const Vec4<V>& v);
		template<typename V>
		Vec4& operator*=(V v);
		template<typename V>
		Vec4& operator*=(const Vec4<V>& v);
		template<typename V>
		Vec4& operator/=(V v);
		template<typename V>
		Vec4& operator/=(const Vec4<V>& v);
		template<typename V>
		Vec4& operator|=(V v);
		template<typename V>
		Vec4& operator|=(const Vec4<V>& v);
		template<typename V>
		Vec4& operator&=(V v);
		template<typename V>
		Vec4& operator&=(const Vec4<V>& v);
		template<typename V>
		Vec4& operator^=(V v);
		template<typename V>
		Vec4& operator^=(const Vec4<V>& v);
		template<typename V>
		Vec4& operator<<=(V v);
		template<typename V>
		Vec4& operator<<=(const Vec4<V>& v);
		template<typename V>
		Vec4& operator>>=(V v);
		template<typename V>
		Vec4& operator>>=(const Vec4<V>& v);

		template<typename V>
		Vec4& operator*=(const Mat4<V>& m);

		template<typename V>
		b8 operator==(const Vec4<V>& v) const;
		template<typename V>
		b8 operator!=(const Vec4<V>& v) const;
		template<typename V>
		b8 operator<(const Vec4<V>& v) const;
		template<typename V>
		b8 operator<=(const Vec4<V>& v) const;
		template<typename V>
		b8 operator>(const Vec4<V>& v) const;
		template<typename V>
		b8 operator>=(const Vec4<V>& v) const;

		/**
		 * Get the largest component of the vector
		 * @return Largest component of the vector
		 */
		T MaxComponent() const;
		/**
		 * Get the smallest component of the vector
		 * @return Smallest component of the vector
		 */
		T MinComponent() const;

		/**
		 * Get the dot product of 2 vectors
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vector
		 * @return		Dot prduct
		 */
		template<typename V>
		T Dot(const Vec4<V>& v) const;
		/**
		 * Get the length of the vector
		 * @return	Length of the vector
		 */
		T Length() const;
		/**
		 * Get the square of the vector
		 * @return	Square length of the vector
		 */
		T SqLength() const;
		/**
		 * Get the distance between 2 vectors
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vector
		 * @return		Distance betweem 2 vectors
		 */
		template<typename V>
		T Distance(const Vec4<V>& v) const;
		/**
		 * Get the square distance between 2 vectors
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vector
		 * @return		Square distance betweem 2 vectors
		 */
		template<typename V>
		T SqDistance(const Vec4<V>& v) const;
		/**
		 * Get a normalized copy of a vector
		 * @return	Normalized vector
		 */
		Vec4 Normalized() const;
		/**
		 * Normalize the vector
		 * @return	Reference to the vector
		 */
		Vec4& Normalize();
		/**
		 * Lerp between 2 vectors
		 * @tparam V0		Arithmatic type
		 * @tparam V1		Arithmatic type
		 * @param[in] v		Vector
		 * @param[in] lf	Lerp factor
		 * @return			Lerped vector
		 */
		template<typename V0, typename V1>
		Vec4 Lerp(const Vec4<V0>& v, V1 lf) const;
		/**
		 * Lerp between 2 vectors with a clamped lerp factor
		 * @tparam V0		Arithmatic type
		 * @tparam V1		Arithmatic type
		 * @param[in] v		Vector
		 * @param[in] lf	Lerp factor
		 * @return			Lerped vector
		 */
		template<typename V0, typename V1>
		Vec4 LerpClamped(const Vec4<V0>& v, V1 lf) const;
		/**
		 * Get a vector with the largest components of 2 vectors
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vector
		 * @return		Vector with the largest components of 2 vectors
		 */
		template<typename V>
		Vec4 Max(const Vec4<V>& v) const;
		/**
		 * Get a vector with the smallest components of 2 vectors
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vector
		 * @return		Vector with the smallest components of 2 vectors
		 */
		template<typename V>
		Vec4 Min(const Vec4<V>& v) const;
		/**
		 * Get a copy of the vector with a clamped length
		 * @tparam V0		Arithmatic type
		 * @tparam V1		Arithmatic type
		 * @param[in] min	Minimum length
		 * @param[in] max	Maximum length
		 * @return			Clamped vector
		 */
		template<typename V0, typename V1>
		Vec4 ClampedLength(V0 min, V1 max) const;
		/**
		 * Clamp the length of the vector
		 * @tparam V0		Arithmatic type
		 * @tparam V1		Arithmatic type
		 * @param[in] min	Minimum length
		 * @param[in] max	Maximum length
		 * @return			Reference to the vector
		 */
		template<typename V0, typename V1>
		Vec4& ClampLength(V0 min, V1 max);
		/**
		 * Get a copy of the vector with clamoed axes
		 * @tparam V0		Arithmatic type
		 * @tparam V1		Arithmatic type
		 * @param[in] min	Minimum value
		 * @param[in] max	Maximum value
		 * @return			Clamped vector
		 */
		template<typename V0, typename V1>
		Vec4 ClampedAxes(V0 min, V1 max) const;
		/**
		 * Get a copy of the vector with clamoed axes
		 * @tparam V0		Arithmatic type
		 * @tparam V1		Arithmatic type
		 * @param[in] min	Minimum value
		 * @param[in] max	Maximum value
		 * @return			Reference to the vector
		 */
		template<typename V0, typename V1>
		Vec4& ClampAxes(V0 min, V1 max);


		/**
		 * Check if 2 vectors are equal to eachother
		 * @tparam V	Arithmatic type
		 * @param[in] v Vector
		 * @return		True if vectors are equal to eachother, false otherwise
		 */
		template<typename V>
		b8 Equals(const Vec4<V>& v) const;
		/**
		 * Check if 2 vectors are equal to eachother
		 * @tparam V			Arithmatic type
		 * @tparam E			Arithmatic type
		 * @param[in] v			Vector
		 * @param[in] epsilon	Epsilon
		 * @return				True if vectors are equal to eachother, false otherwise
		 */
		template<typename V, typename E>
		b8 Equals(const Vec4<V>& v, E epsilon) const;
		/**
		 * Check if the vector is shorter than another vector
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vector
		 * @return		True if the vector is shorter than the other vector, false otherwise
		 */
		template<typename V>
		b8 IsShorter(const Vec4<V>& v);
		/**
		 * Check if the vector is longer than another vector
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vector
		 * @return		True if the vector is longer than the other vector, false otherwise
		 */
		template<typename V>
		b8 IsLonger(const Vec4<V>& v);
		/**
		 * Check if all components are zero
		 * @return	True if all components are zero, false otherwise
		 */
		b8 IsZero() const;
		/**
		 * Check if all components are nearly zero
		 * @return	True if all components are nearly zero, false otherwise
		 */
		b8 IsNearlyZero() const;
		/**
		 * Check wether the vector is uniform (if all components are equals)
		 * @tparam V			Arithmatic type
		 * @param[in] tolerance	Tolerance
		 * @return				True if the vector is uniform, false otherwise
		 */
		template<typename V>
		b8 IsUniform(V tolerance) const;
		/**
		 * Check wether the vector is uniform (if all components are equals)
		 * @return	True if the vector is uniform, false otherwise
		 */
		b8 IsUniform() const;
		/**
		 * Check wether the vector is normalized
		 * @return	True if the vector is normalized, false otherwise
		 */
		b8 IsNormalized() const;
		/**
		 * Check wether the vector is unit vector with a tolerance
		 * @tparam V	Arithmatic type
		 * @return		True if the vector is a unit vector, false otherwise
		 */
		template <typename V>
		b8 IsUnit(V sqTolerance);
		/**
		 * Get a swizzled vector (vector with made with a different order of components, every component can be used more than once)
		 * @param[in] xSwizzle	X index
		 * @param[in] ySwizzle	Y index
		 * @param[in] zSwizzle	Z index
		 * @param[in] wSwizzle	W index
		 * @return	Swizzle vector
		 */
		Vec4 Swizzle(u8 xSwizzle, u8 ySwizzle, u8 zSwizzle, u8 wSwizzle);
		/**
		 * Get a swizzled vector (vector with made with a different order of components, every component can be used more than once)
		 * @param[in] xSwizzle	X index
		 * @param[in] ySwizzle	Y index
		 * @param[in] zSwizzle	Z index
		 * @return	Swizzle vector
		 */
		Vec3<T> Swizzle(u8 xSwizzle, u8 ySwizzle, u8 zSwizzle);
		/**
		 * Get a swizzled vector (vector with made with a different order of components, every component can be used more than once)
		 * @param[in] xSwizzle	X index
		 * @param[in] ySwizzle	Y index
		 * @return	Swizzle vector
		 */
		Vec2<T> Swizzle(u8 xSwizzle, u8 ySwizzle);

		/**
		 * Zero constant
		 */
		const static Vec4 Zero;
		/**S
		 * One constant
		 */
		const static Vec4 One;
		/**
		 * X-axis constant
		 */
		const static Vec4 AxisX;
		/**
		 * Y-axis constant
		 */
		const static Vec4 AxisY;
		/**
		 * Z-axis constant
		 */
		const static Vec4 AxisZ;
		/**
		 * W-axis constant
		 */
		const static Vec4 AxisW;

	};

	template<typename T, typename V>
	Vec4<T> operator*(V val, const Vec4<T>& v);

}

HV_DECLARE_CONTAINER_MEMCPY_TEMPLATE(HV_TARGS(typename T), HV_TTYPE(Hv::Math::Vec4, T))