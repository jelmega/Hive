// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Vec2.h: 2D Vector
#pragma once
#include "Core/CoreHeaders.h"
#include "Containers/ContainerUtils.h"

namespace Hv::Math {
	
	template<typename T>
	struct Vec4;

	template<typename T>
	struct Vec3;

	template<typename T>
	struct Mat2;

	template<typename T>
	struct Vec2
	{
#pragma warning(push)
#pragma warning(disable: 4201) // nonstandard extension used : nameless struct/union
		union
		{
			T data[2];
			struct { T x, y; };
		};
#pragma warning(pop)

		/**
		 * Create a default Vec2
		 */
		Vec2();
		/**
		 * Create a Vec2 with a value
		 * @tparam V		Arithmatic type
		 * @param[in] val	Value
		 */
		template<typename V>
		Vec2(V val);
		/**
		 * Create a Vec2
		 * @tparam X	Arithmatic type
		 * @tparam Y	Arithmatic type
		 * @param[in] x X value
		 * @param[in] y Y value
		 */
		template<typename X, typename Y>
		Vec2(X x, Y y);
		/**
		 * Create a Vec2 from another Vec2
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vec2
		 */
		template<typename V>
		Vec2(const Vec4<V>& v);
		/**
		 * Create a Vec2 from a Vec4
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vec2
		 */
		template<typename V>
		Vec2(const Vec3<V>& v);
		/**
		 * Create a Vec2 from a Vec3
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vec2
		 */
		template<typename V>
		Vec2(const Vec2<V>& v);
		/**
		 * Create a Vec2 from another Vec2
		 * @param[in] v	Vec2
		 */
		Vec2(const Vec2& v);
		/**
		 * Move a Vec2 into this Vec2
		 * @param[in] v	Vec2
		 */
		Vec2(Vec2&& v) noexcept;

		T operator[](sizeT index) const;

		Vec2 operator+() const;
		Vec2 operator-() const;
		Vec2 operator~() const;

		template<typename V>
		Vec2 operator+(V v) const;
		template<typename V>
		Vec2 operator+(const Vec2<V>& v) const;
		template<typename V>
		Vec2 operator-(V v) const;
		template<typename V>
		Vec2 operator-(const Vec2<V>& v) const;
		template<typename V>
		Vec2 operator*(V v) const;
		template<typename V>
		Vec2 operator*(const Vec2<V>& v) const;
		template<typename V>
		Vec2 operator/(V v) const;
		template<typename V>
		Vec2 operator/(const Vec2<V>& v) const;
		template<typename V>
		Vec2 operator|(V v) const;
		template<typename V>
		Vec2 operator|(const Vec2<V>& v) const;
		template<typename V>
		Vec2 operator&(V v) const;
		template<typename V>
		Vec2 operator&(const Vec2<V>& v) const;
		template<typename V>
		Vec2 operator^(V v) const;
		template<typename V>
		Vec2 operator^(const Vec2<V>& v) const;
		template<typename V>
		Vec2 operator<<(V v) const;
		template<typename V>
		Vec2 operator<<(const Vec2<V>& v) const;
		template<typename V>
		Vec2 operator>>(V v) const;
		template<typename V>
		Vec2 operator>>(const Vec2<V>& v) const;

		Vec2 operator*(const Mat2<T>& m) const;

		template<typename V>
		Vec2& operator=(const Vec2<V>& v);
		Vec2& operator=(const Vec2& v);
		Vec2& operator=(Vec2&& v) noexcept;

		template<typename V>
		Vec2& operator+=(V v);
		template<typename V>
		Vec2& operator+=(const Vec2<V>& v);
		template<typename V>
		Vec2& operator-=(V v);
		template<typename V>
		Vec2& operator-=(const Vec2<V>& v);
		template<typename V>
		Vec2& operator*=(V v);
		template<typename V>
		Vec2& operator*=(const Vec2<V>& v);
		template<typename V>
		Vec2& operator/=(V v);
		template<typename V>
		Vec2& operator/=(const Vec2<V>& v);
		template<typename V>
		Vec2& operator|=(V v);
		template<typename V>
		Vec2& operator|=(const Vec2<V>& v);
		template<typename V>
		Vec2& operator&=(V v);
		template<typename V>
		Vec2& operator&=(const Vec2<V>& v);
		template<typename V>
		Vec2& operator^=(V v);
		template<typename V>
		Vec2& operator^=(const Vec2<V>& v);
		template<typename V>
		Vec2& operator<<=(V v);
		template<typename V>
		Vec2& operator<<=(const Vec2<V>& v);
		template<typename V>
		Vec2& operator>>=(V v);
		template<typename V>
		Vec2& operator>>=(const Vec2<V>& v);

		Vec2& operator*=(const Mat2<T>& m);

		template<typename V>
		b8 operator==(const Vec2<V>& v) const;
		template<typename V>
		b8 operator!=(const Vec2<V>& v) const;
		template<typename V>
		b8 operator<(const Vec2<V>& v) const;
		template<typename V>
		b8 operator<=(const Vec2<V>& v) const;
		template<typename V>
		b8 operator>(const Vec2<V>& v) const;
		template<typename V>
		b8 operator>=(const Vec2<V>& v) const;

		/**
		 * Get the largest component of the vector
		 * @return Largest component of the vector
		 */
		T MaxComponent() const;
		/**
		 * Get the smallest component of the vector
		 * @return Smallest component of the vector
		 */
		T MinComponent() const;

		/**
		 * Get the dot product of 2 vectors
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vector
		 * @return		Dot prduct
		 */
		template<typename V>
		T Dot(const Vec2<V>& v) const;
		/**
		 * Get the crpss product of 2 vectors
		 * @tparam V		Arithmatic type
		 * @param[in] v	Vector
		 * @return		Cross prduct
		 */
		template<typename V>
		T Cross(const Vec2<V>& v) const;
		/**
		 * Get the length of the vector
		 * @return	Length of the vector
		 */
		T Length() const;
		/**
		 * Get the square of the vector
		 * @return	Square length of the vector
		 */
		T SqLength() const;
		/**
		 * Get the distance between 2 vectors
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vector
		 * @return		Distance betweem 2 vectors
		 */
		template<typename V>
		T Distance(const Vec2<V>& v) const;
		/**
		 * Get the square distance between 2 vectors
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vector
		 * @return		Square distance betweem 2 vectors
		 */
		template<typename V>
		T SqDistance(const Vec2<V>& v) const;
		/**
		 * Get a normalized copy of a vector
		 * @return	Normalized vector
		 */
		Vec2 Normalized() const;
		/**
		 * Normalize the vector
		 * @return	Reference to the vector
		 */
		Vec2& Normalize();
		/**
		 * Lerp between 2 vectors
		 * @tparam V0		Arithmatic type
		 * @tparam V1		Arithmatic type
		 * @param[in] v		Vector
		 * @param[in] lf	Lerp factor
		 * @return			Lerped vector
		 */
		template<typename V0, typename V1>
		Vec2 Lerp(const Vec2<V0>& v, V1 lf) const;
		/**
		 * Lerp between 2 vectors with a clamped lerp factor
		 * @tparam V0		Arithmatic type
		 * @tparam V1		Arithmatic type
		 * @param[in] v		Vector
		 * @param[in] lf	Lerp factor
		 * @return			Lerped vector
		 */
		template<typename V0, typename V1>
		Vec2 LerpClamped(const Vec2<V0>& v, V1 lf) const;
		/**
		 * Get a vector with the largest components of 2 vectors
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vector
		 * @return		Vector with the largest components of 2 vectors
		 */
		template<typename V>
		Vec2 Max(const Vec2<V>& v) const;
		/**
		 * Get a vector with the smallest components of 2 vectors
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vector
		 * @return		Vector with the smallest components of 2 vectors
		 */
		template<typename V>
		Vec2 Min(const Vec2<V>& v) const;
		/**
		 * Get a copy of the vector with a clamped length
		 * @tparam V0		Arithmatic type
		 * @tparam V1		Arithmatic type
		 * @param[in] min	Minimum length
		 * @param[in] max	Maximum length
		 * @return			Clamped vector
		 */
		template<typename V0, typename V1>
		Vec2 ClampedLength(V0 min, V1 max) const;
		/**
		 * Clamp the length of the vector
		 * @tparam V0		Arithmatic type
		 * @tparam V1		Arithmatic type
		 * @param[in] min	Minimum length
		 * @param[in] max	Maximum length
		 * @return			Reference to the vector
		 */
		template<typename V0, typename V1>
		Vec2& ClampLength(V0 min, V1 max);
		/**
		 * Get a copy of the vector with clamoed axes
		 * @tparam V0		Arithmatic type
		 * @tparam V1		Arithmatic type
		 * @param[in] min	Minimum value
		 * @param[in] max	Maximum value
		 * @return			Clamped vector
		 */
		template<typename V0, typename V1>
		Vec2 ClampedAxes(V0 min, V1 max) const;
		/**
		 * Get a copy of the vector with clamoed axes
		 * @tparam V0		Arithmatic type
		 * @tparam V1		Arithmatic type
		 * @param[in] min	Minimum value
		 * @param[in] max	Maximum value
		 * @return			Reference to the vector
		 */
		template<typename V0, typename V1>
		Vec2& ClampAxes(V0 min, V1 max);
		/**
		 * Get a rotated copy of the vector
		 * @tparam V		Arithmatic type
		 * @param[in] angle	Angle
		 * @return			Rotated vector
		 */
		template<typename V>
		Vec2 Rotated(V angle) const;
		/**
		 * Rotate the vector
		 * @tparam V		Arithmatic type
		 * @param[in] angle	Angle
		 * @return			Rotated vector
		 */
		template<typename V>
		Vec2& Rotate(V angle);
		/**
		 * Get the angle of the vector
		 * @return	Angle
		 */
		T Angle() const;
		/**
		 * Get the angle between 2 vectors
		 * @tparam V		Arithmatic type
		 * @param[in] v	Vector
		 * @return		Angle
		 */
		template<typename V>
		T Angle(const Vec2<V>& v) const;

		/**
		 * Check if 2 vectors are equal to eachother
		 * @tparam V	Arithmatic type
		 * @param[in] v Vector
		 * @return		True if vectors are equal to eachother, false otherwise
		 */
		template<typename V>
		b8 Equals(const Vec2<V>& v) const;
		/**
		 * Check if 2 vectors are equal to eachother
		 * @tparam V			Arithmatic type
		 * @tparam E			Arithmatic type
		 * @param[in] v			Vector
		 * @param[in] epsilon	Epsilon
		 * @return				True if vectors are equal to eachother, false otherwise
		 */
		template<typename V, typename E>
		b8 Equals(const Vec2<V>& v, E epsilon) const;
		/**
		 * Check if the vector is shorter than another vector
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vector
		 * @return		True if the vector is shorter than the other vector, false otherwise
		 */
		template<typename V>
		b8 IsShorter(const Vec2<V>& v);
		/**
		 * Check if the vector is longer than another vector
		 * @tparam V	Arithmatic type
		 * @param[in] v	Vector
		 * @return		True if the vector is longer than the other vector, false otherwise
		 */
		template<typename V>
		b8 IsLonger(const Vec2<V>& v);
		/**
		 * Check if all components are zero
		 * @return	True if all components are zero, false otherwise
		 */
		b8 IsZero() const;
		/**
		 * Check if all components are nearly zero
		 * @return	True if all components are nearly zero, false otherwise
		 */
		b8 IsNearlyZero() const;
		/**
		 * Check wether the vector is uniform (if all components are equals)
		 * @tparam V			Arithmatic type
		 * @param[in] tolerance	Tolerance
		 * @return				True if the vector is uniform, false otherwise
		 */
		template<typename V>
		b8 IsUniform(V tolerance) const;
		/**
		 * Check wether the vector is uniform (if all components are equals)
		 * @return	True if the vector is uniform, false otherwise
		 */
		b8 IsUniform() const;
		/**
		 * Check wether the vector is normalized
		 * @return	True if the vector is normalized, false otherwise
		 */
		b8 IsNormalized() const;
		/**
		 * Check wether the vector is unit vector with a tolerance
		 * @tparam V	Arithmatic type
		 * @return		True if the vector is a unit vector, false otherwise
		 */
		template <typename V>
		b8 IsUnit(V sqTolerance);
		/**
		 * Get a swizzled vector (vector with made with a different order of components, every component can be used more than once)
		 * @param[in] xSwizzle	X index
		 * @param[in] ySwizzle	Y index
		 * @param[in] zSwizzle	Z index
		 * @param[in] wSwizzle	W index
		 * @return	Swizzle vector
		 */
		Vec4<T> Swizzle(u8 xSwizzle, u8 ySwizzle, u8 zSwizzle, u8 wSwizzle);
		/**
		 * Get a swizzled vector (vector with made with a different order of components, every component can be used more than once)
		 * @param[in] xSwizzle	X index
		 * @param[in] ySwizzle	Y index
		 * @param[in] zSwizzle	Z index
		 * @return	Swizzle vector
		 */
		Vec3<T> Swizzle(u8 xSwizzle, u8 ySwizzle, u8 zSwizzle);
		/**
		 * Get a swizzled vector (vector with made with a different order of components, every component can be used more than once)
		 * @param[in] xSwizzle	X index
		 * @param[in] ySwizzle	Y index
		 * @return	Swizzle vector
		 */
		Vec2 Swizzle(u8 xSwizzle, u8 ySwizzle);

		/**
		 * Zero constant
		 */
		const static Vec2 Zero;
		/**S
		 * One constant
		 */
		const static Vec2 One;
		/**
		 * X-axis constant
		 */
		const static Vec2 AxisX;
		/**
		 * Y-axis constant
		 */
		const static Vec2 AxisY;

	};

	template<typename T, typename V>
	Vec2<T> operator*(V val, const Vec2<T>& v);

}

HV_DECLARE_CONTAINER_MEMCPY_TEMPLATE(HV_TARGS(typename T), HV_TTYPE(Hv::Math::Vec2, T))
