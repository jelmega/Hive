// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Mat3.h: 3x3 matrix
#pragma once
#include "Core/CoreHeaders.h"
#include "Containers/ContainerUtils.h"

namespace Hv::Math {

	template<typename T>
	struct Vec3;

	template<typename T>
	struct Vec2;

	template<typename T>
	struct Mat3
	{
#pragma warning(push)
#pragma warning(disable: 4201) // nonstandard extension used : nameless struct/union
		union
		{
			T data[9];
			T data2D[3][3];
			Vec3<T> rows[3];
			struct
			{
				Vec3<T> r0;
				Vec3<T> r1;
				Vec3<T> r2;
			};
			struct
			{
				T m00, m01, m02;
				T m10, m11, m12;
				T m20, m21, m22;
			};
		};
#pragma warning(pop)
		/**
		 * Create a default Mat3
		 */
		Mat3();
		/**
		 * Create a matrix with a value in its diagonal
		 * @tparam V		Arithametic type
		 * @param[in] diag	Diagonal value
		 */
		template<typename V>
		Mat3(V diag);
		/**
		 * @tparam V00		Arithametic type
		 * @tparam V01		Arithametic type
		 * @tparam V02		Arithametic type
		 * @tparam V10		Arithametic type
		 * @tparam V11		Arithametic type
		 * @tparam V12		Arithametic type
		 * @tparam V20		Arithametic type
		 * @tparam V21		Arithametic type
		 * @tparam V22		Arithametic type
		 * @param[in] v00	Value
		 * @param[in] v01	Value
		 * @param[in] v02	Value
		 * @param[in] v10	Value
		 * @param[in] v11	Value
		 * @param[in] v12	Value
		 * @param[in] v20	Value
		 * @param[in] v21	Value
		 * @param[in] v22	Value
		 */
		template<typename V00, typename V01, typename V02, typename V10, typename V11, typename V12, typename V20, typename V21, typename V22>
		Mat3(V00 v00, V01 v01, V02 v02, V10 v10, V11 v11, V12 v12, V20 v20, V21 v21, V22 v22);
		/**
		 * @tparam R0	Arithametic type
		 * @tparam R1	Arithametic type
		 * @tparam R2	Arithametic type
		 * @param[in] r0	Row 0
		 * @param[in] r1	Row 1
		 * @param[in] r2	Row 2
		 */
		template<typename R0, typename R1, typename R2>
		Mat3(const Vec3<R0>& r0, const Vec3<R1>& r1, const Vec3<R2>& r2);
		/**
		 * Create a matrix from aonther matrix
		 * @tparam V	Arithmatic type
		 * @param[in] m	Matrix
		 */
		template<typename V>
		Mat3(const Mat3<V>& m);
		/**
		 * Create a matrix from aonther matrix
		 * @param[in] m	Matrix
		 */
		Mat3(const Mat3& m);
		/**
		 * Move a matrix into this matrix
		 * @param[in] m	Matrix
		 */
		Mat3(Mat3&& m) noexcept;

		Mat3 operator+() const;
		Mat3 operator-() const;

		template<typename V>
		Mat3 operator+(const Mat3<V>& m) const;
		template<typename V>
		Mat3 operator-(const Mat3<V>& m) const;
		template<typename V>
		Mat3 operator*(const Mat3<V>& m) const;
		template<typename V>
		Mat3 operator*(V v) const;
		template<typename V>
		Mat3 operator/(V v) const;

		template<typename V>
		Mat3& operator=(const Mat3<V>& m);
		Mat3& operator=(const Mat3& m);
		template<typename V>
		Mat3& operator=(Mat3&& m) noexcept;

		template<typename V>
		Mat3& operator+=(const Mat3<V>& m);
		template<typename V>
		Mat3& operator-=(const Mat3<V>& m);
		template<typename V>
		Mat3& operator*=(const Mat3<V>& m);
		template<typename V>
		Mat3& operator*=(V v);
		template<typename V>
		Mat3& operator/=(V v);

		template<typename V>
		b8 operator==(const Mat3<V>& m) const;
		template<typename V>
		b8 operator!=(const Mat3<V>& m) const;

		/**
		 * Get the determinant of the matrix
		 * @return	Determinant
		 */
		T Determinant() const;
		/**
		 * Get a transposed copy of the matrix
		 * @return	Transposed matrix
		 */
		Mat3 Transposed() const;
		/**
		 * transpose the matrix
		 * @return	Reference to the matrix
		 */
		Mat3& Transpose();
		/**
		 * Get the cofactor of the matrix
		 * @return	Cofactor
		 */
		Mat3 Cofactor() const;
		/**
		 * Convert the matrix to its cofactor
		 * @return	Reference to the matrix
		 */
		Mat3& ToCofactor();
		/**
		 * Get the adjugate of the matrix
		 * @return	Adjugate
		 */
		Mat3 Adjugate() const;
		/**
		 * Convert the matrix to its adjugate
		 * @return	Reference to the matrix
		 */
		Mat3& ToAdjugate();
		/**
		 * Get the inverse of the matrix
		 * @return	Inverse
		 */
		Mat3 Inverse() const;
		/**
		 * Convert the matrix to its inverse
		 * @return	Reference to the matrix
		 */
		Mat3& ToInverse();
		/**
		 * Get a row from the matrix
		 * @param[in] row	Row index
		 * @return			Row
		 */
		Vec3<T> Row(u8 row) const;
		/**
		 * Set a row from the matrix
		 * @tparam V			Arithmatic type
		 * @param[in] row	Row index
		 * @param[in] v		Vector
		 */
		template<typename V>
		void Row(u8 row, const Vec3<V>& v);
		/**
		 * Get a row from the matrix
		 * @param[in] column	Column index
		 * @return			Column
		 */
		Vec3<T> Column(u8 column) const;
		/**
		 * Set a row from the matrix
		 * @tparam V			Arithmatic type
		 * @param[in] column	Column index
		 * @param[in] v		Vector
		 */
		template<typename V>
		void Column(u8 column, const Vec3<V>& v);
		/**
		 * Get the diagonal from the matrix
		 * @return	Diagonal
		 */
		Vec3<T> Diagonal() const;
		/**
		 * Set the diagonal from the matrix
		 * @tparam V			Arithmatic type
		 * @param[in] m		Vector
		*/
		template<typename V>
		void Diagonal(const Vec3<V>& m);

		/**
		 * Check if 2 matrices are equal to eachother
		 * @tparam V	Arithmatic type
		 * @param[in] m	Matrix
		 * @return		True if matrices are equal to eachother, false otherwise
		 */
		template<typename V>
		b8 Equals(const Mat3<V>& m) const;
		/**
		 * Check if 2 matrices are equal to eachother
		 * @tparam V			Arithmatic type
		 * @tparam E			Arithmatic type
		 * @param[in] m			Matrix
		 * @param[in] epsilon	Epsilon
		 * @return				True if matrices are equal to eachother, false otherwise
		 */
		template<typename V, typename E>
		b8 Equals(const Mat3<V>& m, E epsilon) const;
		/**
		 * Check if the matrix is an identity matrix
		 * @return	True if the matrix is an identity matrix
		 */
		b8 IsIdentity() const;

		/**
		 * Create a rotation matrix from a quaternion
		 * @tparam V	Arithmatic type
		 * @param[in] q	Quaternion
		 * @return		Rotation matrix
		 */
		template<typename V>
		static Mat3 CreateRotation(const Quat<V>& q);
		/**
		 * Create a scale matrix from a vec3
		 * @tparam V	Arithmatic type
		 * @param[in] v	Scale
		 * @return		Scale matrix
		 */
		template<typename V>
		static Mat3 CreateScale(const Vec3<V>& v);
		/**
		 * Create a scale matrix from a vec3
		 * @tparam X	Arithmatic type
		 * @tparam Y	Arithmatic type
		 * @tparam Z	Arithmatic type
		 * @param[in] x	X scale
		 * @param[in] y	Y scale
		 * @param[in] z	Z scale
		 * @return		Scale matrix
		 */
		template<typename X, typename Y, typename Z>
		static Mat3 CreateScale(X x, Y y, Z z);

		/**
		 * Create a 2d rotation matrix from a quaternion
		 * @tparam V		Arithmatic type
		 * @param[in] angle	Angle
		 * @return			Rotation matrix
		 */
		template<typename V>
		static Mat3 CreateRotation2D(V angle);
		/**
		 * Create a scale matrix from a vec3
		 * @tparam V	Arithmatic type
		 * @param[in] v	Scale
		 * @return		Scale matrix
		 */
		template<typename V>
		static Mat3 CreateScale2D(const Vec2<V>& v);
		/**
		 * Create a scale matrix from a vec3
		 * @tparam X	Arithmatic type
		 * @tparam Y	Arithmatic type
		 * @param[in] x	X scale
		 * @param[in] y	Y scale
		 * @return		Scale matrix
		 */
		template<typename X, typename Y>
		static Mat3 CreateScale2D(X x, Y y);
		/**
		 * Create a translation matrix from a vector
		 * @tparam V	Arithmatic type
		 * @param[in] v	translation
		 * @return		Rotation matrix
		 */
		template<typename V>
		static Mat3 CreateTranslation2D(const Vec2<V>& v);
		/**
		 * Create a translation matrix from a vector
		 * @tparam X	Arithmatic type
		 * @tparam Y	Arithmatic type
		 * @param[in] x	X scale
		 * @param[in] y	Y scale
		 * @return		Rotation matrix
		 */
		template<typename X, typename Y>
		static Mat3 CreateTranslation2D(X x, Y y);

		/**
		 * Identity matrix
		 */
		const static Mat3 Identity;
	};

	template<typename T, typename V>
	Mat3<T> operator*(V v, const Mat3<T>& m);

}

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename T), HV_TTYPE(Hv::Math::Mat3, T))