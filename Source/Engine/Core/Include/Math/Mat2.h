// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Mat2.h: 2x2 matrix
#pragma once
#include "Core/CoreHeaders.h"
#include "Containers/ContainerUtils.h"

namespace Hv::Math {

	template<typename T>
	struct Vec2;

	template<typename T>
	struct Mat2
	{
#pragma warning(push)
#pragma warning(disable: 4201) // nonstandard extension used : nameless struct/union
		union
		{
			T data[4];
			T data2D[2][2];
			Vec2<T> rows[2];
			struct
			{
				Vec2<T> r0;
				Vec2<T> r1;
			};
			struct
			{
				T m00, m01;
				T m10, m11;
			};
		};
#pragma warning(pop)
		/**
		 * Create a default Mat2
		 */
		Mat2();
		/**
		 * Create a matrix with a value in its diagonal
		 * @tparam V		Arithametic type
		 * @param[in] diag	Diagonal value
		 */
		template<typename V>
		Mat2(V diag);
		/**
		 * @tparam V00		Arithametic type
		 * @tparam V01		Arithametic type
		 * @tparam V10		Arithametic type
		 * @tparam V11		Arithametic type
		 * @param[in] v00	Value
		 * @param[in] v01	Value
		 * @param[in] v10	Value
		 * @param[in] v11	Value
		 */
		template<typename V00, typename V01, typename V10, typename V11>
		Mat2(V00 v00, V01 v01, V10 v10, V11 v11);
		/**
		 * @tparam R0	Arithametic type
		 * @tparam R1	Arithametic type
		 * @param[in] r0	Row 0
		 * @param[in] r1	Row 1
		 */
		template<typename R0, typename R1>
		Mat2(const Vec2<R0>& r0, const Vec2<R1>& r1);
		/**
		 * Create a matrix from aonther matrix
		 * @tparam V	Arithmatic type
		 * @param[in] m	Matrix
		 */
		template<typename V>
		Mat2(const Mat2<V>& m);
		/**
		 * Create a matrix from aonther matrix
		 * @param[in] m	Matrix
		 */
		Mat2(const Mat2& m);
		/**
		 * Move a matrix into this matrix
		 * @param[in] m	Matrix
		 */
		Mat2(Mat2&& m) noexcept;

		Mat2 operator+() const;
		Mat2 operator-() const;

		template<typename V>
		Mat2 operator+(const Mat2<V>& m) const;
		template<typename V>
		Mat2 operator-(const Mat2<V>& m) const;
		template<typename V>
		Mat2 operator*(const Mat2<V>& m) const;
		template<typename V>
		Mat2 operator*(V v) const;
		template<typename V>
		Mat2 operator/(V v) const;

		template<typename V>
		Mat2& operator=(const Mat2<V>& m);
		Mat2& operator=(const Mat2& m);
		template<typename V>
		Mat2& operator=(Mat2&& m) noexcept;

		template<typename V>
		Mat2& operator+=(const Mat2<V>& m);
		template<typename V>
		Mat2& operator-=(const Mat2<V>& m);
		template<typename V>
		Mat2& operator*=(const Mat2<V>& m);
		template<typename V>
		Mat2& operator*=(V v);
		template<typename V>
		Mat2& operator/=(V v);

		template<typename V>
		b8 operator==(const Mat2<V>& m) const;
		template<typename V>
		b8 operator!=(const Mat2<V>& m) const;

		/**
		 * Get the determinant of the matrix
		 * @return	Determinant
		 */
		T Determinant() const;
		/**
		 * Get a transposed copy of the matrix
		 * @return	Transposed matrix
		 */
		Mat2 Transposed() const;
		/**
		 * transpose the matrix
		 * @return	Reference to the matrix
		 */
		Mat2& Transpose();
		/**
		 * Get the cofactor of the matrix
		 * @return	Cofactor
		 */
		Mat2 Cofactor() const;
		/**
		 * Convert the matrix to its cofactor
		 * @return	Reference to the matrix
		 */
		Mat2& ToCofactor();
		/**
		 * Get the adjugate of the matrix
		 * @return	Adjugate
		 */
		Mat2 Adjugate() const;
		/**
		 * Convert the matrix to its adjugate
		 * @return	Reference to the matrix
		 */
		Mat2& ToAdjugate();
		/**
		 * Get the inverse of the matrix
		 * @return	Inverse
		 */
		Mat2 Inverse() const;
		/**
		 * Convert the matrix to its inverse
		 * @return	Reference to the matrix
		 */
		Mat2& ToInverse();
		/**
		 * Get a row from the matrix
		 * @param[in] row	Row index
		 * @return			Row
		 */
		Vec2<T> Row(u8 row) const;
		/**
		 * Set a row from the matrix
		 * @tparam V			Arithmatic type
		 * @param[in] row	Row index
		 * @param[in] v		Vector
		 */
		template<typename V>
		void Row(u8 row, const Vec2<V>& v);
		/**
		 * Get a row from the matrix
		 * @param[in] column	Column index
		 * @return			Column
		 */
		Vec2<T> Column(u8 column) const;
		/**
		 * Set a row from the matrix
		 * @tparam V			Arithmatic type
		 * @param[in] column	Column index
		 * @param[in] v		Vector
		 */
		template<typename V>
		void Column(u8 column, const Vec2<V>& v);
		/**
		 * Get the diagonal from the matrix
		 * @return	Diagonal
		 */
		Vec2<T> Diagonal() const;
		/**
		 * Set the diagonal from the matrix
		 * @tparam V			Arithmatic type
		 * @param[in] v		Vector
		 */
		template<typename V>
		void Diagonal(const Vec2<V>& m);  // Resharper -> NOLINT

										  /**
										   * Check if 2 matrices are equal to eachother
										   * @tparam V	Arithmatic type
										   * @param[in] m	Matrix
										   * @return		True if matrices are equal to eachother, false otherwise
										   */
		template<typename V>
		b8 Equals(const Mat2<V>& m) const;
		/**
		 * Check if 2 matrices are equal to eachother
		 * @tparam V			Arithmatic type
		 * @tparam E			Arithmatic type
		 * @param[in] v			Matrix
		 * @param[in] epsilon	Epsilon
		 * @return				True if matrices are equal to eachother, false otherwise
		 */
		template<typename V, typename E>
		b8 Equals(const Mat2<V>& v, E epsilon) const;
		/**
		 * Check if the matrix is an identity matrix
		 * @return	True if the matrix is an identity matrix
		 */
		b8 IsIdentity() const;

		/**
		 * Create a 2d rotation matrix from a quaternion
		 * @tparam V		Arithmatic type
		 * @param[in] angle	Angle
		 * @return			Rotation matrix
		 */
		template<typename V>
		static Mat2 CreateRotation2D(V angle);
		/**
		 * Create a scale matrix from a vec3
		 * @tparam V	Arithmatic type
		 * @param[in] v	Scale
		 * @return		Scale matrix
		 */
		template<typename V>
		static Mat2 CreateScale2D(const Vec2<V>& v);
		/**
		 * Create a scale matrix from a vec3
		 * @tparam X	Arithmatic type
		 * @tparam Y	Arithmatic type
		 * @param[in] x	X scale
		 * @param[in] y	Y scale
		 * @return		Scale matrix
		 */
		template<typename X, typename Y>
		static Mat2 CreateScale2D(X x, Y y);

		/**
		 * Identity matrix
		 */
		const static Mat2 Identity;
	};

	template<typename T, typename V>
	Mat2<T> operator*(V v, const Mat2<T>& m);

}

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename T), HV_TTYPE(Hv::Math::Mat2, T))