// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// MathFunc.inl: General math functions
#pragma once
#include "Math/MathFunc.h"
#include "Math/MathConstants.h"
#include "Containers/Array.h"

namespace Hv::Math {
	
	template <typename T>
	HV_FORCE_INL T Min(T a, T b)
	{
		return a < b ? a : b;
	}

	template <typename T>
	HV_FORCE_INL T Min(T a, T b, T c)
	{
		return a < b ? (a < c ? a : c) : (b < c ? b : c);
	}

	template <typename T>
	HV_FORCE_INL T Min(T a, T b, T c, T d)
	{
		return Min(Min(a, b), Min(c, d));
	}

	template <typename T0, typename ... T1>
	HV_INL T0 Min(T0 a, T0 b, T0 c, T0 d, T1... values)
	{
		constexpr u8 numElements = sizeof...(T1) + 1;
		HV_STATIC_ASSERT_MSG(numElements > 0, "Max: function has too many arguments!");
		if (numElements == 1)
			return a;

		Array<T0, numElements + 3> arr({ b, c, d, values... });
		T0 t = a;
		for (sizeT i = 1; i < numElements - 1; ++i)
		{
			t = t < arr[i] ? t : arr[i];
		}
		return t;
	}

	template<typename T>
	HV_FORCE_INL T Max(T a, T b)
	{
		return a > b ? a : b;
	}

	template <typename T>
	HV_FORCE_INL T Max(T a, T b, T c)
	{
		return a > b ? (a > c ? a : c) : (b > c ? b : c);
	}

	template <typename T>
	HV_FORCE_INL T Max(T a, T b, T c, T d)
	{
		return Max(Max(a, b), Max(c, d));
	}

	template <typename T0, typename ... T1>
	HV_INL T0 Max(T0 a, T0 b, T0 c, T0 d, T1... values)
	{
		constexpr u8 numElements = sizeof...(T1) + 1;
		HV_STATIC_ASSERT_MSG(numElements > 0, "Max: function has too many arguments!");
		if (numElements == 1)
			return a;

		Array<T0, numElements + 3> arr({ b, c, d, values... });
		T0 t = a;
		for (sizeT i = 1; i < numElements - 1; ++i)
		{
			t = t > arr[i] ? t : arr[i];
		}
		return t;
	}

	template <typename T>
	HV_FORCE_INL T Abs(T t)
	{
		return t < 0 ? -t : t;
	}

	template <typename T>
	HV_FORCE_INL T Select(T t, T lt, T ge)
	{
		return t < 0 ? lt : ge;
	}

	template <typename T>
	HV_FORCE_INL T Select(T t, T comp, T lt, T ge)
	{
		return t < comp ? lt : ge;
	}

	template <typename T>
	HV_FORCE_INL T Sign(T t)
	{
		return t == 0 ? 0 : (t < 0 ? -1 : 1);
	}

	template <typename T, typename V0, typename V1>
	HV_FORCE_INL T Clamp(T t, V0 min, V1 max)
	{
		if (t < min)
			return T(min);
		if (t > max)
			return T(max);
		return t;
	}

	template <typename T>
	HV_FORCE_INL T ClampUnit(T t)
	{
		if (t < T(0))
			return T(0);
		if (t > T(1))
			return T(1);
		return t;
	}

	template <typename T>
	HV_FORCE_INL T Lerp(T t, T a, T b)
	{
		return  a + t * (b - a);
	}

	template <typename T>
	HV_FORCE_INL T LerpClamped(T t, T a, T b)
	{
		t = ClampUnit(t);
		return a + t * (b - a);
	}

	template <typename T>
	HV_FORCE_INL T Sqrt(T t)
	{
		return T(sqrt(t));
	}

	template <typename T>
	HV_FORCE_INL std::conditional_t<sizeof(T) < 8, i32, i64> Round(T t)
	{
		using RoundT = std::conditional_t<sizeof(T) < 8, u32, u64>;
		if constexpr (std::is_floating_point<T>::value)
			return RoundT(t + T(.5));
		else
			return t;
	}

	template <typename T>
	HV_FORCE_INL std::conditional_t<sizeof(T) < 8, i32, i64> Ceil(T t)
	{
		using RoundT = std::conditional_t<sizeof(T) < 8, u32, u64>;
		if constexpr (std::is_floating_point<T>::value)
		{
			RoundT tmp = RoundT(t);
			return Equals(t, tmp) ? tmp : tmp + 1;
		}
		else
			return t;
	}

	template <typename T>
	HV_FORCE_INL std::conditional_t<sizeof(T) < 8, i32, i64> Floor(T t)
	{
		using RoundT = std::conditional_t<sizeof(T) < 8, u32, u64>;
		if constexpr (std::is_floating_point<T>::value)
			return RoundT(t);
		else
			return t;
	}

	template <typename T>
	HV_FORCE_INL T Mod(T t, T n)
	{
		using RoundT = std::conditional_t<sizeof(T) < 8, u32, u64>;
		if constexpr (std::is_floating_point<T>::value)
			return t < 0 || t > n ? t - n * RoundT(t / n) : t;
		else
			return t < 0 || t > n ? t % n : t;

	}

	template <typename T>
	T PosMod(T t, T n)
	{
		t = Mod(t, n);
		return t < 0 ? t + n : t;
	}

	template <typename T>
	T NegPos(T t, T n)
	{
		t = Mod(t, n);
		return t > 0 ? t - n : t;
	}

	template <typename T>
	T Pow(T t, T n)
	{
		return pow(t, n);
	}

	template <typename T>
	T Sin(T t)
	{
		return sin(t);
	}

	template <typename T>
	T Cos(T t)
	{
		return cos(t);
	}

	template <typename T>
	T Tan(T t)
	{
		return tan(t);
	}

	template <typename T>
	T ArcSin(T t)
	{
		return asin(t);
	}

	template <typename T>
	T ArcCos(T t)
	{
		return acos(t);
	}

	template <typename T>
	T ArcTan(T t)
	{
		return atan(t);
	}

	template <typename T>
	T ArcTan(T y, T x)
	{
		return atan2(y, x);
	}

	template <typename T>
	T ToRadians(T t)
	{
		return t * g_Deg2Rad<T>;
	}

	template <typename T>
	T ToDegrees(T t)
	{
		return t * g_Rad2Deg<T>;
	}

	template <typename T>
	T WrapRadians(T t)
	{
		t = Mod(t, g_2Pi<T>);
		return t < 0 ? t + g_2Pi<T> : t;
	}

	template <typename T>
	T WrapDegrees(T t)
	{
		t = Mod(t, T(360));
		return t < 0 ? t + T(360) : t;
	}

	template<typename V0, typename V1>
	b8 Equals(V0 a, V1 b)
	{
		return Math::Abs(a - b) <= g_Epsilon<typename MostAccuarateType<V0, V1>::type>;
	}

	template<typename V0, typename V1, typename E>
	b8 Equals(V0 a, V1 b, E epsilon)
	{
		return (a - b) <= epsilon;
	}

	template <typename T>
	b8 IsZero(T t)
	{
		return t == 0;
	}

	template <typename T>
	b8 IsNearlyZero(T t)
	{
		return t > -g_Epsilon<T> && t < g_Epsilon<T>;
	}

	template <typename T>
	b8 IsPowerOf2(T t)
	{
		return !(t & (t - 1));
	}

	template <typename T>
	b8 IsEven(T t)
	{
		return !(t & 1);
	}

	template <typename T>
	b8 IsOdd(T t)
	{
		return t & 1;
	}

	template <typename T>
	b8 IsInfinity(T t)
	{
		return t == g_Infinity<T> || t == -g_Infinity<T>;
	}

	template <typename T>
	b8 IsNaN(T t)
	{
		// NaN != NaN
		// ReSharper disable CppIdenticalOperandsInBinaryExpression
		return t != t;
		// ReSharper restore CppIdenticalOperandsInBinaryExpression
	}

	template <typename T>
	b8 IsFinite(T t)
	{
		return !(IsNaN(t) || IsInfinity(t));
	}

}
