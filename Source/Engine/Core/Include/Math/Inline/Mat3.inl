// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Mat3.inl: 3x3 matrix
#pragma once
#include "Math/Mat2.h"

namespace Hv::Math {

	template <typename T>
	HV_INL Mat3<T>::Mat3()
		: m00(T(0)), m01(T(0)), m02(T(0))
		, m10(T(0)), m11(T(0)), m12(T(0))
		, m20(T(0)), m21(T(0)), m22(T(0))
	{
	}

	template <typename T>
	template <typename V>
	HV_INL Mat3<T>::Mat3(V diag)
		: m00(T(diag)), m01(T(0)), m02(T(0))
		, m10(T(0)), m11(T(diag)), m12(T(0))
		, m20(T(0)), m21(T(0)), m22(T(diag))
	{
	}

	template <typename T>
	template <typename V00, typename V01, typename V02, typename V10, typename V11, typename V12, typename V20, typename V21, typename V22>
	HV_INL Mat3<T>::Mat3(V00 v00, V01 v01, V02 v02, V10 v10, V11 v11, V12 v12, V20 v20, V21 v21, V22 v22)
		: m00(T(v00)), m01(T(v01)), m02(T(v02))
		, m10(T(v10)), m11(T(v11)), m12(T(v12))
		, m20(T(v20)), m21(T(v21)), m22(T(v22))
	{
	}

	template <typename T>
	template <typename R0, typename R1, typename R2>
	HV_INL Mat3<T>::Mat3(const Vec3<R0>& r0, const Vec3<R1>& r1, const Vec3<R2>& r2)
		: r0(r0)
		, r1(r1)
		, r2(r2)
	{
	}

	template <typename T>
	template <typename V>
	HV_INL Mat3<T>::Mat3(const Mat3<V>& m)
		: r0(m.r0)
		, r1(m.r1)
		, r2(m.r2)
	{
	}

	template <typename T>
	HV_INL Mat3<T>::Mat3(const Mat3& m)
		: r0(m.r0)
		, r1(m.r1)
		, r2(m.r2)
	{
	}

	template <typename T>
	HV_INL Mat3<T>::Mat3(Mat3&& m) noexcept
		: r0(m.r0)
		, r1(m.r1)
		, r2(m.r2)
	{
	}

	template <typename T>
	HV_FORCE_INL Mat3<T> Mat3<T>::operator+() const
	{
		return *this;
	}

	template <typename T>
	HV_INL Mat3<T> Mat3<T>::operator-() const
	{
		return Mat3(-r0, -r1, -r2);
	}

	template <typename T>
	template <typename V>
	HV_INL Mat3<T> Mat3<T>::operator+(const Mat3<V>& m) const
	{
		return Mat3(r0 + m.r0, r1 + m.r1, r2 + m.r2);
	}

	template <typename T>
	template <typename V>
	HV_INL Mat3<T> Mat3<T>::operator-(const Mat3<V>& m) const
	{
		return Mat3(r0 - m.r0, r1 - m.r1, r2 - m.r2);
	}

	template <typename T>
	template <typename V>
	HV_INL Mat3<T> Mat3<T>::operator*(const Mat3<V>& m) const
	{
		Vec3<T> c0(m.m00, m.m10, m.m20);
		Vec3<T> c1(m.m01, m.m11, m.m21);
		Vec3<T> c2(m.m02, m.m12, m.m22);

		return Mat3(c0.Dot(r0), c1.Dot(r0), c2.Dot(r0),
			c0.Dot(r1), c1.Dot(r1), c2.Dot(r1),
			c0.Dot(r2), c1.Dot(r2), c2.Dot(r2));
	}

	template <typename T>
	template <typename V>
	HV_INL Mat3<T> Mat3<T>::operator*(V v) const
	{
		return Mat3(r0 * v, r1 * v, r2 * v);
	}

	template <typename T>
	template <typename V>
	Mat3<T> Mat3<T>::operator/(V v) const
	{
		return Mat3(r0 / v, r1 / v, r2 / v);
	}

	template <typename T>
	template <typename V>
	HV_INL Mat3<T>& Mat3<T>::operator=(const Mat3<V>& m)
	{
		r0 = m.r0;
		r1 = m.r1;
		r2 = m.r2;
		return *this;
	}

	template <typename T>
	HV_INL Mat3<T>& Mat3<T>::operator=(const Mat3& m)
	{
		HV_ASSERT_MSG(this != &m, "Self assignment!");
		r0 = m.r0;
		r1 = m.r1;
		r2 = m.r2;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL Mat3<T>& Mat3<T>::operator=(Mat3&& m) noexcept
	{
		HV_ASSERT_MSG(this != &m, "Self assignment!");
		r0 = m.r0;
		r1 = m.r1;
		r2 = m.r2;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL Mat3<T>& Mat3<T>::operator+=(const Mat3<V>& m)
	{
		r0 += m.r0;
		r1 += m.r1;
		r2 += m.r2;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL Mat3<T>& Mat3<T>::operator-=(const Mat3<V>& m)
	{
		r0 -= m.r0;
		r1 -= m.r1;
		r2 -= m.r2;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL Mat3<T>& Mat3<T>::operator*=(const Mat3<V>& m)
	{
		Vec3<T> c0(m.m00, m.m10, m.m20);
		Vec3<T> c1(m.m01, m.m11, m.m21);
		Vec3<T> c2(m.m02, m.m12, m.m22);

		Vec3<T> tr0(c0.Dot(r0), c1.Dot(r0), c2.Dot(r0));
		Vec3<T> tr1(c0.Dot(r1), c1.Dot(r1), c2.Dot(r1));
		Vec3<T> tr2(c0.Dot(r2), c1.Dot(r2), c2.Dot(r2));
		r0 = tr0;
		r1 = tr1;
		r2 = tr2;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL Mat3<T>& Mat3<T>::operator*=(V v)
	{
		r0 *= v;
		r1 *= v;
		r2 *= v;
		return *this;
	}

	template <typename T>
	template <typename V>
	Mat3<T>& Mat3<T>::operator/=(V v)
	{
		r0 /= v;
		r1 /= v;
		r2 /= v;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL b8 Mat3<T>::operator==(const Mat3<V>& m) const
	{
		return r0 == m.r0 && r1 == m.r1 && r2 == m.r2;
	}

	template <typename T>
	template <typename V>
	HV_INL b8 Mat3<T>::operator!=(const Mat3<V>& m) const
	{
		return r0 != m.r0 || r1 != m.r1 || r2 != m.r2;
	}

	template <typename T>
	HV_INL T Mat3<T>::Determinant() const
	{
		return m00 * (m11 * m22 - m12 * m21) - m01 * (m10 * m22 - m12 * m20) + m02 * (m10 * m21 - m11 * m20);
	}

	template <typename T>
	HV_INL Mat3<T> Mat3<T>::Transposed() const
	{
		return Mat3(m00, m10, m20,
			m01, m11, m21,
			m02, m12, m22);
	}

	template <typename T>
	HV_INL Mat3<T>& Mat3<T>::Transpose()
	{
		Hv::Swap(m01, m10);
		Hv::Swap(m02, m20);
		Hv::Swap(m12, m21);
		return *this;
	}

	template <typename T>
	HV_INL Mat3<T> Mat3<T>::Cofactor() const
	{
		return Mat3((m11 * m22 - m12 * m21),
			-(m10 * m22 - m12 * m20),
			(m10 * m21 - m11 * m20),
			-(m01 * m22 - m02 * m21),
			(m00 * m22 - m02 * m20),
			-(m00 * m21 - m01 * m20),
			(m01 * m12 - m02 * m11),
			-(m00 * m12 - m02 * m10),
			(m00 * m11 - m01 * m10));
	}

	template <typename T>
	HV_INL Mat3<T>& Mat3<T>::ToCofactor()
	{
		T t00 = (m11 * m22 - m12 * m21);
		T t01 = -(m10 * m22 - m12 * m20);
		T t02 = (m10 * m21 - m11 * m20);
		T t10 = -(m01 * m22 - m02 * m21);
		T t11 = (m00 * m22 - m02 * m20);
		T t12 = -(m00 * m21 - m01 * m20);
		T t20 = (m01 * m12 - m02 * m11);
		T t21 = -(m00 * m12 - m02 * m10);
		T t22 = (m00 * m11 - m01 * m10);
		m00 = t00;
		m01 = t01;
		m02 = t02;
		m10 = t10;
		m11 = t11;
		m12 = t12;
		m20 = t20;
		m21 = t21;
		m22 = t22;
		return *this;
	}

	template <typename T>
	HV_INL Mat3<T> Mat3<T>::Adjugate() const //Transposed cofactor
	{
		return Mat3((m11 * m22 - m12 * m21),
			-(m01 * m22 - m02 * m21),
			(m01 * m12 - m02 * m11),
			-(m10 * m22 - m12 * m20),
			(m00 * m22 - m02 * m20),
			-(m00 * m12 - m02 * m10),
			(m10 * m21 - m11 * m20),
			-(m00 * m21 - m01 * m20),
			(m00 * m11 - m01 * m10));
	}

	template <typename T>
	HV_INL Mat3<T>& Mat3<T>::ToAdjugate() //Transposed cofactor
	{
		T t00 = (m11 * m22 - m12 * m21);
		T t01 = -(m01 * m22 - m02 * m21);
		T t02 = (m01 * m12 - m02 * m11);
		T t10 = -(m10 * m22 - m12 * m20);
		T t11 = (m00 * m22 - m02 * m20);
		T t12 = -(m00 * m12 - m02 * m10);
		T t20 = (m10 * m21 - m11 * m20);
		T t21 = -(m00 * m21 - m01 * m20);
		T t22 = (m00 * m11 - m01 * m10);
		m00 = t00;
		m01 = t01;
		m02 = t02;
		m10 = t10;
		m11 = t11;
		m12 = t12;
		m20 = t20;
		m21 = t21;
		m22 = t22;
		return *this;
	}

	template <typename T>
	HV_INL Mat3<T> Mat3<T>::Inverse() const
	{
		return 1 / Determinant() * Adjugate();
	}

	template <typename T>
	HV_INL Mat3<T>& Mat3<T>::ToInverse()
	{
		ToAdjugate();
		*this *= 1 / Determinant();
		return *this;
	}

	template <typename T>
	HV_FORCE_INL Vec3<T> Mat3<T>::Row(u8 row) const
	{
		HV_ASSERT_MSG(row < 3, "Row out of range!");
		return rows[row];
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL void Mat3<T>::Row(u8 row, const Vec3<V>& v)
	{
		HV_ASSERT_MSG(row < 3, "Row out of range!");
		rows[row] = v;
	}

	template <typename T>
	HV_INL Vec3<T> Mat3<T>::Column(u8 column) const
	{
		HV_ASSERT_MSG(column < 3, "Column out of range!");
		switch (column)
		{
		default:
		case 0:
			return Vec3<T>(m00, m10, m20);
		case 1:
			return Vec3<T>(m01, m11, m21);
		case 2:
			return Vec3<T>(m02, m12, m22);
		}
	}

	template <typename T>
	template <typename V>
	HV_INL void Mat3<T>::Column(u8 column, const Vec3<V>& v)
	{
		HV_ASSERT_MSG(column < 3, "Column out of range!");
		switch (column)
		{
		default:
		case 0:
			m00 = v.x;
			m10 = v.y;
			m20 = v.z;
			break;
		case 1:
			m01 = v.x;
			m11 = v.y;
			m21 = v.z;
			break;
		case 2:
			m02 = v.x;
			m12 = v.y;
			m22 = v.z;
			break;
		}
	}

	template <typename T>
	HV_FORCE_INL Vec3<T> Mat3<T>::Diagonal() const
	{
		return Vec3<T>(m00, m11, m22);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL void Mat3<T>::Diagonal(const Vec3<V>& v)
	{
		m00 = v.x;
		m11 = v.y;
		m22 = v.z;
	}

	template <typename T>
	template <typename V>
	HV_INL b8 Mat3<T>::Equals(const Mat3<V>& m) const
	{
		return r0.Equals(m.r0) && r1.Equals(m.r1) && r2.Equals(m.r2);
	}

	template <typename T>
	template <typename V, typename E>
	HV_INL b8 Mat3<T>::Equals(const Mat3<V>& m, E epsilon) const
	{
		return r0.Equals(m.r0, epsilon) && r1.Equals(m.r1, epsilon) && r2.Equals(m.r2, epsilon);
	}

	template <typename T>
	HV_INL b8 Mat3<T>::IsIdentity() const
	{
		return *this == Identity;
	}

	template <typename T>
	template <typename V>
	Mat3<T> Mat3<T>::CreateRotation(const Quat<V>& q)
	{
		Quat<T> tq = q.Normalized();
		T xx = tq.x * tq.x;
		T yy = tq.y * tq.y;
		T zz = tq.z * tq.z;
		T ww = tq.w * tq.w;
		T xy = tq.x * tq.y;
		T xz = tq.x * tq.z;
		T yz = tq.y * tq.z;
		T xw = tq.x * tq.w;
		T yw = tq.y * tq.w;
		T zw = tq.z * tq.w;

		return Mat3(1 - 2 * (yy + zz), 2 * (xy - zw), 2 * (xz + yw),
			2 * (xy + zw), 1 - 2 * (xx + zz), 2 * (yz - xw),
			2 * (xz - yw), 2 * (yz + xw), 1 - 2 * (xx + yy));
	}

	template <typename T>
	template <typename V>
	Mat3<T> Mat3<T>::CreateScale(const Vec3<V>& v)
	{
		return Mat3(v.x, 0, 0,
			0, v.y, 0,
			0, 0, v.z);
	}

	template <typename T>
	template <typename X, typename Y, typename Z>
	Mat3<T> Mat3<T>::CreateScale(X x, Y y, Z z)
	{
		return Mat3(x, 0, 0,
			0, y, 0,
			0, 0, z);
	}

	template <typename T>
	template <typename V>
	Mat3<T> Mat3<T>::CreateRotation2D(V angle)
	{
		T s = Math::Sin(T(angle));
		T c = Math::Cos(T(angle));

		return Mat3(c, -s, 0,
			s, c, 0,
			0, 0, 1);
	}

	template <typename T>
	template <typename V>
	Mat3<T> Mat3<T>::CreateScale2D(const Vec2<V>& v)
	{
		return Mat3(v.x, 0, 0,
			0, v.y, 0,
			0, 0, 1);
	}

	template <typename T>
	template <typename X, typename Y>
	Mat3<T> Mat3<T>::CreateScale2D(X x, Y y)
	{
		return Mat3(x, 0, 0,
			0, y, 0,
			0, 0, 1);
	}

	template <typename T>
	template <typename V>
	Mat3<T> Mat3<T>::CreateTranslation2D(const Vec2<V>& v)
	{
		return Mat3(1, 0, v.x,
			0, 1, v.y,
			0, 0, 1);
	}

	template <typename T>
	template <typename X, typename Y>
	Mat3<T> Mat3<T>::CreateTranslation2D(X x, Y y)
	{
		return Mat3(1, 0, x,
			0, 1, y,
			0, 0, 1);
	}

	template<typename T>
	const Mat3<T> Mat3<T>::Identity = Mat3<T>(1);

	template <typename T, typename V>
	HV_INL Mat3<T> operator*(V v, const Mat3<T>& m)
	{
		return Mat3<T>(m.r0 * v, m.r1 * v, m.r2 * v);
	}

}