// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Rect.h: Rectangle
#pragma once
#include "Math/Rect.h"

namespace Hv::Math {
	
	template <typename T>
	Rect<T>::Rect()
		: left(0)
		, top(0)
		, right(0)
		, bottom(0)
	{
	}

	template <typename T>
	template <typename T0, typename T1, typename T2, typename T3>
	Rect<T>::Rect(T0 left, T1 top, T2 right, T3 bottom)
		: left(T(left))
		, top(T(top))
		, right(T(right))
		, bottom(T(bottom))
	{
	}

	template <typename T>
	template <typename T0, typename T1>
	Rect<T>::Rect(const Vec2<T0>& topLeft, const Vec2<T>& bottomRight)
		: topLeft(topLeft)
		, bottomRight(bottomRight)
	{
	}

	template <typename T>
	template <typename T0>
	Rect<T>::Rect(const Rect<T0>& rc)
		: left(T(left))
		, top(T(top))
		, right(T(right))
		, bottom(T(bottom))
	{
	}

	template <typename T>
	Rect<T>::Rect(const Rect& rc)
		: left(left)
		, top(top)
		, right(right)
		, bottom(bottom)
	{
	}

	template <typename T>
	Rect<T>::Rect(Rect&& rc) noexcept
		: left(left)
		, top(top)
		, right(right)
		, bottom(bottom)
	{
	}

	template <typename T>
	template <typename T0>
	Rect<T>& Rect<T>::operator=(const Rect<T0>& rc)
	{
		left = T(rc.left);
		top = T(rc.top);
		right = T(rc.right);
		bottom = T(rc.bottom);
		return *this;
	}

	template <typename T>
	Rect<T>& Rect<T>::operator=(const Rect& rc)
	{
		left = rc.left;
		top = rc.top;
		right = rc.right;
		bottom = rc.bottom;
		return *this;
	}

	template <typename T>
	Rect<T>& Rect<T>::operator=(Rect&& rc) noexcept
	{
		left = rc.left;
		top = rc.top;
		right = rc.right;
		bottom = rc.bottom;
		return *this;
	}

}
