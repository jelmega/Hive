// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Vec2.inl: 2D Vector
#pragma once
#include "Math/Vec2.h"
#include "Math/MathConstants.h"
#include "Math/MathFunc.h"
#include "Vec3.h"
#include "Vec4.h"

namespace Hv::Math {

	template <typename T>
	HV_FORCE_INL Vec2<T>::Vec2()
		: x(T(0))
		, y(T(0))
	{
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>::Vec2(V val)
		: x(T(val))
		, y(T(val))
	{
	}

	template <typename T>
	template <typename X, typename Y>
	HV_FORCE_INL Vec2<T>::Vec2(X x, Y y)
		: x(T(x))
		, y(T(y))
	{
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>::Vec2(const Vec4<V>& v)
		: x(T(v.x))
		, y(T(v.y))
	{
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>::Vec2(const Vec3<V>& v)
		: x(T(v.x))
		, y(T(v.y))
	{
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>::Vec2(const Vec2<V>& v)
		: x(T(v.x))
		, y(T(v.y))
	{
	}

	template <typename T>
	HV_FORCE_INL Vec2<T>::Vec2(const Vec2& v)
		: x(v.x)
		, y(v.y)
	{
	}

	template <typename T>
	HV_FORCE_INL Vec2<T>::Vec2(Vec2&& v) noexcept
		: x(v.x)
		, y(v.y)
	{
	}

	template <typename T>
	HV_FORCE_INL T Vec2<T>::operator[](sizeT index) const
	{
		HV_ASSERT_MSG(index < 2, "Index out of range!");
		return data[index];
	}

	template <typename T>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator+() const
	{
		return *this;
	}

	template <typename T>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator-() const
	{
		return Vec2(-x, -y);
	}

	template <typename T>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator~() const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator~ only valid with integral types!");
		return Vec2(~x, ~y);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator+(V v) const
	{
		return Vec2(x + v, y + v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator+(const Vec2<V>& v) const
	{
		return Vec2(x + v.x, y + v.y);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator-(V v) const
	{
		return Vec2(x - v, y - v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator-(const Vec2<V>& v) const
	{
		return Vec2(x - v.x, y - v.y);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator*(V v) const
	{
		return Vec2(x * v, y * v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator*(const Vec2<V>& v) const
	{
		return Vec2(x * v.x, y * v.y);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator/(V v) const
	{
		return Vec2(x / v, y / v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator/(const Vec2<V>& v) const
	{
		return Vec2(x / v.x, y / v.y);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator|(V v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec2(x | v, y | v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator|(const Vec2<V>& v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec2(x | v.x, y | v.y);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator&(V v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec2(x & v, y & v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator&(const Vec2<V>& v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec2(x & v.x, y & v.y);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator^(V v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec2(x ^ v, y ^ v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator^(const Vec2<V>& v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec2(x ^ v.x, y ^ v.y);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator<<(V v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec2(x << v, y << v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator<<(const Vec2<V>& v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec2(x << v.x, y << v.y);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator>>(V v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec2(x >> v, y >> v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T> Vec2<T>::operator>>(const Vec2<V>& v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec2(x >> v.x, y >> v.y);
	}

	template <typename T>
	Vec2<T> Vec2<T>::operator*(const Mat2<T>& m) const
	{
		return Vec2(x * m.m00 + y * m.m10,
			x * m.m01 + y * m.m11);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator=(const Vec2<V>& v)
	{
		x = T(v.x);
		y = T(v.y);
		return *this;
	}

	template <typename T>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator=(const Vec2& v)
	{
		HV_ASSERT_MSG(this != &v, "Self assignment!");
		x = v.x;
		y = v.y;
		return *this;
	}

	template <typename T>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator=(Vec2&& v) noexcept
	{
		HV_ASSERT_MSG(this != &v, "Self assignment!");
		x = v.x;
		y = v.y;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator+=(V v)
	{
		x += T(v);
		y += T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator+=(const Vec2<V>& v)
	{
		x += T(v.x);
		y += T(v.y);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator-=(V v)
	{
		x -= T(v);
		y -= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator-=(const Vec2<V>& v)
	{
		x -= T(v.x);
		y -= T(v.y);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator*=(V v)
	{
		x *= T(v);
		y *= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator*=(const Vec2<V>& v)
	{
		x *= T(v.x);
		y *= T(v.y);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator/=(V v)
	{
		x /= T(v);
		y /= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator/=(const Vec2<V>& v)
	{
		x /= T(v.x);
		y /= T(v.y);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator|=(V v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x |= T(v);
		y |= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator|=(const Vec2<V>& v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x |= T(v.x);
		y |= T(v.y);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator&=(V v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x &= T(v);
		y &= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator&=(const Vec2<V>& v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x &= T(v.x);
		y &= T(v.y);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator^=(V v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x ^= T(v);
		y ^= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator^=(const Vec2<V>& v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x ^= T(v.x);
		y ^= T(v.y);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator<<=(V v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x <<= T(v);
		y <<= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator<<=(const Vec2<V>& v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x <<= T(v.x);
		y <<= T(v.y);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator>>=(V v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x >>= T(v);
		y >>= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec2<T>& Vec2<T>::operator>>=(const Vec2<V>& v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x >>= T(v.x);
		y >>= T(v.y);
		return *this;
	}

	template <typename T>
	Vec2<T>& Vec2<T>::operator*=(const Mat2<T>& m)
	{
		T tx = x * m.m00 + y * m.m10;
		T ty = x * m.m01 + y * m.m11;
		x = tx;
		y = ty;
		return *this;
	}

	template <typename T>
	template<typename V>
	HV_FORCE_INL b8 Vec2<T>::operator==(const Vec2<V>& v) const
	{
		return x == v.x && y == v.y;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec2<T>::operator!=(const Vec2<V>& v) const
	{
		return x != v.x || y != v.y;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec2<T>::operator<(const Vec2<V>& v) const
	{
		return x < v.x && y < v.y;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec2<T>::operator<=(const Vec2<V>& v) const
	{
		return x <= v.x && y <= v.y;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec2<T>::operator>(const Vec2<V>& v) const
	{
		return x > v.x && y > v.y;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec2<T>::operator>=(const Vec2<V>& v) const
	{
		return x >= v.x && y >= v.y;
	}

	template <typename T>
	HV_FORCE_INL T Vec2<T>::MaxComponent() const
	{
		return Math::Max(x, y);
	}

	template <typename T>
	HV_FORCE_INL T Vec2<T>::MinComponent() const
	{
		return Math::Min(x, y);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL T Vec2<T>::Dot(const Vec2<V>& v) const
	{
		return x * v.x + y * v.y;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL T Vec2<T>::Cross(const Vec2<V>& v) const
	{
		return x * v.y - y * v.x;
	}

	template <typename T>
	HV_FORCE_INL T Vec2<T>::Length() const
	{
		return Math::Sqrt(x * x + y * y);
	}

	template <typename T>
	HV_FORCE_INL T Vec2<T>::SqLength() const
	{
		return x * x + y * y;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL T Vec2<T>::Distance(const Vec2<V>& v) const
	{
		T tx = x - v.x;
		T ty = y - v.y;
		return Math::Sqrt(tx * tx + ty * ty);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL T Vec2<T>::SqDistance(const Vec2<V>& v) const
	{
		T tx = x - v.x;
		T ty = y - v.y;
		return tx * tx + ty * ty;
	}

	template <typename T>
	HV_INL Vec2<T> Vec2<T>::Normalized() const
	{
		T len = SqLength();
		if (len < g_Epsilon<T>)
			return *this;
		return *this * T(1) / Math::Sqrt(len);
	}

	template <typename T>
	HV_INL Vec2<T>& Vec2<T>::Normalize()
	{
		T len = SqLength();
		if (len < g_Epsilon<T>)
			return *this;
		*this *= T(1) / Math::Sqrt(len);
		return *this;
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_FORCE_INL Vec2<T> Vec2<T>::Lerp(const Vec2<V0>& v, V1 lf) const
	{
		return *this + lf * (v - *this);
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_FORCE_INL Vec2<T> Vec2<T>::LerpClamped(const Vec2<V0>& v, V1 lf) const
	{
		lf = ClampUnit(lf);
		return *this + lf * (v - *this);
	}

	template <typename T>
	template <typename V>
	HV_INL Vec2<T> Vec2<T>::Max(const Vec2<V>& v) const
	{
		return Vec2(Math::Max(x, v.x), Math::Max(y, v.y));
	}

	template <typename T>
	template <typename V>
	HV_INL Vec2<T> Vec2<T>::Min(const Vec2<V>& v) const
	{
		return Vec2(Math::Min(x, v.x), Math::Min(y, v.y));
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_INL Vec2<T> Vec2<T>::ClampedLength(V0 min, V1 max) const
	{
		T len = SqLength();
		if (len < min * min)
			return *this * min / Math::Sqrt(len);
		if (len > max * max)
			return *this * max / Math::Sqrt(len);
		return *this;
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_INL Vec2<T>& Vec2<T>::ClampLength(V0 min, V1 max)
	{
		T len = SqLength();
		if (len < min * min)
			*this *= min / Math::Sqrt(len);
		else if (len > max * max)
			*this *= max / Math::Sqrt(len);
		return *this;
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_INL Vec2<T> Vec2<T>::ClampedAxes(V0 min, V1 max) const
	{
		return Vec2(Math::Clamp(x, T(min), T(max)), Math::Clamp(y, T(min), T(max)));
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_INL Vec2<T>& Vec2<T>::ClampAxes(V0 min, V1 max)
	{
		x = Math::Clamp(x, T(min), T(max));
		y = Math::Clamp(y, T(min), T(max));
		return *this;
	}

	template <typename T>
	template <typename V>
	Vec2<T> Vec2<T>::Rotated(V angle) const
	{
		T s = Math::Sin(angle);
		T c = Math::Cos(angle);

		return Vec2(x * c - y * s, x * s + y * c);
	}

	template <typename T>
	template <typename V>
	Vec2<T>& Vec2<T>::Rotate(V angle)
	{
		T s = Math::Sin(angle);
		T c = Math::Cos(angle);

		T tx = x * c - y * s;
		T ty = x * s + y * c;
		x = tx;
		y = ty;
		return *this;
	}

	template <typename T>
	T Vec2<T>::Angle() const
	{
		return Math::ArcTan(y, x);
	}

	template <typename T>
	template <typename V>
	T Vec2<T>::Angle(const Vec2<V>& v) const
	{
		T angle = Math::ArcCos(Dot(v));
		if (Math::Sign(Cross(v)) == -1)
			angle = g_2Pi<T> -angle;
		return angle;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec2<T>::Equals(const Vec2<V>& v) const
	{
		return Math::Equals(x, v.x) && Math::Equals(y, v.y);
	}

	template <typename T>
	template <typename V, typename E>
	HV_FORCE_INL b8 Vec2<T>::Equals(const Vec2<V>& v, E epsilon) const
	{
		return Math::Equals(x, v.x, epsilon) && Math::Equals(y, v.y, epsilon);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec2<T>::IsShorter(const Vec2<V>& v)
	{
		return SqLength() < v.SqLength();
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec2<T>::IsLonger(const Vec2<V>& v)
	{
		return SqLength() > v.SqLength();
	}

	template <typename T>
	HV_FORCE_INL b8 Vec2<T>::IsZero() const
	{
		return x == 0 && y == 0;
	}

	template <typename T>
	HV_FORCE_INL b8 Vec2<T>::IsNearlyZero() const
	{
		return Math::Abs(x) < g_Epsilon<T> && Math::Abs(y) < g_Epsilon<T>;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec2<T>::IsUniform(V tolerance) const
	{
		return Math::Equals(x, y, T(tolerance));
	}

	template <typename T>
	HV_FORCE_INL b8 Vec2<T>::IsUniform() const
	{
		return Math::Equals(x, y);
	}

	template <typename T>
	HV_FORCE_INL b8 Vec2<T>::IsNormalized() const
	{
		return Math::Equals(SqLength(), T(1));
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec2<T>::IsUnit(V sqTolerance)
	{
		return Math::Equals(SqLength(), T(1), T(sqTolerance));
	}

	template <typename T>
	Vec4<T> Vec2<T>::Swizzle(u8 xSwizzle, u8 ySwizzle, u8 zSwizzle, u8 wSwizzle)
	{
		HV_ASSERT_MSG(xSwizzle < 2, "xSwizzle out of range!");
		HV_ASSERT_MSG(ySwizzle < 2, "ySwizzle out of range!");
		HV_ASSERT_MSG(zSwizzle < 2, "zSwizzle out of range!");
		HV_ASSERT_MSG(wSwizzle < 2, "wSwizzle out of range!");
		return Vec2(data[xSwizzle], data[ySwizzle], data[zSwizzle], data[wSwizzle]);
	}

	template <typename T>
	Vec3<T> Vec2<T>::Swizzle(u8 xSwizzle, u8 ySwizzle, u8 zSwizzle)
	{
		HV_ASSERT_MSG(xSwizzle < 2, "xSwizzle out of range!");
		HV_ASSERT_MSG(ySwizzle < 2, "ySwizzle out of range!");
		HV_ASSERT_MSG(zSwizzle < 2, "zSwizzle out of range!");
		return Vec2(data[xSwizzle], data[ySwizzle], data[zSwizzle]);
	}

	template <typename T>
	HV_INL HV_FORCE_INL Vec2<T> Vec2<T>::Swizzle(u8 xSwizzle, u8 ySwizzle)
	{
		HV_ASSERT_MSG(xSwizzle < 2, "xSwizzle out of range!");
		HV_ASSERT_MSG(ySwizzle < 2, "ySwizzle out of range!");
		return Vec2(data[xSwizzle], data[ySwizzle]);
	}

	template <typename T, typename V>
	Vec2<T> operator*(V val, const Vec2<T>& v)
	{
		return Vec2<T>(v.x * val, v.y * val);
	}

	template<typename T>
	const Vec2<T> Vec2<T>::Zero = Vec2<T>(0);
	template<typename T>
	const Vec2<T> Vec2<T>::One = Vec2<T>(1);
	template<typename T>
	const Vec2<T> Vec2<T>::AxisX = Vec2<T>(1, 0);
	template<typename T>
	const Vec2<T> Vec2<T>::AxisY = Vec2<T>(0, 1);
	
}