// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Vec4.inl: 4D Vector
#pragma once

#include "Math/Vec4.h"
#include "Math/MathConstants.h"
#include "Math/MathFunc.h"
#include "Math/Vec3.h"
#include "Math/Vec2.h"
#include "Math/Mat4.h"

namespace Hv::Math {
	
	template <typename T>
	HV_FORCE_INL Vec4<T>::Vec4()
		: x(T(0))
		, y(T(0))
		, z(T(0))
		, w(T(0))
	{
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>::Vec4(V val)
		: x(T(val))
		, y(T(val))
		, z(T(val))
		, w(T(val))
	{
	}

	template <typename T>
	template <typename X, typename Y, typename Z, typename W>
	HV_FORCE_INL Vec4<T>::Vec4(X x, Y y, Z z, W w)
		: x(T(x))
		, y(T(y))
		, z(T(z))
		, w(T(w))
	{
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>::Vec4(const Vec4<V>& v)
		: x(T(v.x))
		, y(T(v.y))
		, z(T(v.z))
		, w(T(v.w))
	{
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>::Vec4(const Vec3<V>& v)
		: x(T(v.x))
		, y(T(v.y))
		, z(T(v.z))
		, w(T(0))
	{
	}

	template <typename T>
	template <typename V, typename W>
	HV_FORCE_INL Vec4<T>::Vec4(const Vec3<V>& xyz, W w)
		: x(T(xyz.x))
		, y(T(xyz.y))
		, z(T(xyz.z))
		, w(T(w))
	{
	}

	template <typename T>
	template <typename X, typename V>
	HV_FORCE_INL Vec4<T>::Vec4(X x, const Vec3<V>& yzw)
		: x(T(x))
		, y(T(yzw.x))
		, z(T(yzw.y))
		, w(T(yzw.z))
	{
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>::Vec4(const Vec2<V>& v)
		: x(T(v.x))
		, y(T(v.y))
		, z(T(0))
		, w(T(0))
	{
	}

	template <typename T>
	template <typename V, typename Z, typename W>
	HV_FORCE_INL Vec4<T>::Vec4(const Vec2<V>& xy, Z z, W w)
		: x(T(xy.x))
		, y(T(xy.y))
		, z(T(z))
		, w(T(w))
	{
	}

	template <typename T>
	template <typename X, typename V, typename W>
	HV_FORCE_INL Vec4<T>::Vec4(X x, const Vec2<V>& yz, W w)
		: x(T(x))
		, y(T(yz.x))
		, z(T(yz.y))
		, w(T(w))
	{
	}

	template <typename T>
	template <typename X, typename Y, typename V>
	HV_FORCE_INL Vec4<T>::Vec4(X x, Y y, const Vec2<V>& zw)
		: x(T(x))
		, y(T(y))
		, z(T(zw.x))
		, w(T(zw.y))
	{
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_FORCE_INL Vec4<T>::Vec4(const Vec2<V0>& xy, const Vec2<V1>& zw)
		: x(T(xy.x))
		, y(T(xy.y))
		, z(T(zw.x))
		, w(T(zw.y))
	{
	}

	template <typename T>
	HV_FORCE_INL Vec4<T>::Vec4(const Vec4& v)
		: x(v.x)
		, y(v.y)
		, z(v.z)
		, w(v.w)
	{
	}

	template <typename T>
	HV_FORCE_INL Vec4<T>::Vec4(Vec4&& v) noexcept
		: x(v.x)
		, y(v.y)
		, z(v.z)
		, w(v.w)
	{
	}

	template <typename T>
	HV_FORCE_INL T Vec4<T>::operator[](sizeT index) const
	{
		HV_ASSERT_MSG(index < 4, "Index out of range!");
		return data[index];
	}

	template <typename T>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator+() const
	{
		return *this;
	}

	template <typename T>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator-() const
	{
		return Vec4(-x, -y, -z, -w);
	}

	template <typename T>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator~() const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator~ only valid with integral types!");
		return Vec4(~x, ~y, ~z, ~w);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator+(V v) const
	{
		return Vec4(x + v, y + v, z + v, w + v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator+(const Vec4<V>& v) const
	{
		return Vec4(x + v.x, y + v.y, z + v.z, w + v.w);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator-(V v) const
	{
		return Vec4(x - v, y - v, z - v, w - v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator-(const Vec4<V>& v) const
	{
		return Vec4(x - v.x, y - v.y, z - v.z, w - v.w);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator*(V v) const
	{
		return Vec4(x * v, y * v, z * v, w * v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator*(const Vec4<V>& v) const
	{
		return Vec4(x * v.x, y * v.y, z * v.z, w * v.w);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator/(V v) const
	{
		return Vec4(x / v, y / v, z / v, w / v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator/(const Vec4<V>& v) const
	{
		return Vec4(x / v.x, y / v.y, z / v.z, w / v.w);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator|(V v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec4(x | v, y | v, z | v, w | v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator|(const Vec4<V>& v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec4(x | v.x, y | v.y, z | v.z, w | v.w);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator&(V v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec4(x & v, y & v, z & v, w & v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator&(const Vec4<V>& v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec4(x & v.x, y & v.y, z & v.z, w & v.w);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator^(V v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec4(x ^ v, y ^ v, z ^ v, w ^ v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator^(const Vec4<V>& v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec4(x ^ v.x, y ^ v.y, z ^ v.z, w ^ v.w);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator<<(V v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec4(x << v, y << v, z << v, w << v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator<<(const Vec4<V>& v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec4(x << v.x, y << v.y, z << v.z, w << v.w);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator>>(V v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec4(x >> v, y >> v, z >> v, w >> v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T> Vec4<T>::operator>>(const Vec4<V>& v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec4(x >> v.x, y >> v.y, z >> v.z, w >> v.w);
	}

	template <typename T>
	template <typename V>
	Vec4<T> Vec4<T>::operator*(const Mat4<V>& m) const
	{
		return Vec4(x * m.m00 + y * m.m10 + z * m.m20 + w * m.m30,
			x * m.m01 + y * m.m11 + z * m.m21 + w * m.m31,
			x * m.m02 + y * m.m12 + z * m.m22 + w * m.m32,
			x * m.m03 + y * m.m13 + z * m.m23 + w * m.m33);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator=(const Vec4<V>& v)
	{
		x = T(v.x);
		y = T(v.y);
		z = T(v.z);
		w = T(v.w);
		return *this;
	}

	template <typename T>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator=(const Vec4& v)
	{
		HV_ASSERT_MSG(this != &v, "Self assignment!");
		x = v.x;
		y = v.y;
		z = v.z;
		w = v.w;
		return *this;
	}

	template <typename T>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator=(Vec4&& v) noexcept
	{
		HV_ASSERT_MSG(this != &v, "Self assignment!");
		x = v.x;
		y = v.y;
		z = v.z;
		w = v.w;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator+=(V v)
	{
		x += T(v);
		y += T(v);
		z += T(v);
		w += T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator+=(const Vec4<V>& v)
	{
		x += T(v.x);
		y += T(v.y);
		z += T(v.z);
		w += T(v.w);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator-=(V v)
	{
		x -= T(v);
		y -= T(v);
		z -= T(v);
		w -= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator-=(const Vec4<V>& v)
	{
		x -= T(v.x);
		y -= T(v.y);
		z -= T(v.z);
		w -= T(v.w);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator*=(V v)
	{
		x *= T(v);
		y *= T(v);
		z *= T(v);
		w *= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator*=(const Vec4<V>& v)
	{
		x *= T(v.x);
		y *= T(v.y);
		z *= T(v.z);
		w *= T(v.w);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator/=(V v)
	{
		x /= T(v);
		y /= T(v);
		z /= T(v);
		w /= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator/=(const Vec4<V>& v)
	{
		x /= T(v.x);
		y /= T(v.y);
		z /= T(v.z);
		w /= T(v.w);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator|=(V v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x |= T(v);
		y |= T(v);
		z |= T(v);
		w |= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator|=(const Vec4<V>& v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x |= T(v.x);
		y |= T(v.y);
		z |= T(v.z);
		w |= T(v.w);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator&=(V v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x &= T(v);
		y &= T(v);
		z &= T(v);
		w &= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator&=(const Vec4<V>& v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x &= T(v.x);
		y &= T(v.y);
		z &= T(v.z);
		w &= T(v.w);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator^=(V v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x ^= T(v);
		y ^= T(v);
		z ^= T(v);
		w ^= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator^=(const Vec4<V>& v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x ^= T(v.x);
		y ^= T(v.y);
		z ^= T(v.z);
		w ^= T(v.w);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator<<=(V v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x <<= T(v);
		y <<= T(v);
		z <<= T(v);
		w <<= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator<<=(const Vec4<V>& v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x <<= T(v.x);
		y <<= T(v.y);
		z <<= T(v.z);
		w <<= T(v.w);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator>>=(V v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x >>= T(v);
		y >>= T(v);
		z >>= T(v);
		w >>= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec4<T>& Vec4<T>::operator>>=(const Vec4<V>& v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x >>= T(v.x);
		y >>= T(v.y);
		z >>= T(v.z);
		w >>= T(v.w);
		return *this;
	}

	template <typename T>
	template <typename V>
	Vec4<T>& Vec4<T>::operator*=(const Mat4<V>& m)
	{
		T tx = x * m.m00 + y * m.m10 + z * m.m20 + w * m.m30;
		T ty = x * m.m01 + y * m.m11 + z * m.m21 + w * m.m31;
		T tz = x * m.m02 + y * m.m12 + z * m.m22 + w * m.m32;
		T tw = x * m.m03 + y * m.m13 + z * m.m23 + w * m.m33;
		x = tx;
		y = ty;
		z = tz;
		w = tw;
		return *this;
	}

	template <typename T>
	template<typename V>
	HV_FORCE_INL b8 Vec4<T>::operator==(const Vec4<V>& v) const
	{
		return x == v.x && y == v.y && z == v.z && w == v.w;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec4<T>::operator!=(const Vec4<V>& v) const
	{
		return x != v.x || y != v.y || z != v.z || w != v.w;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec4<T>::operator<(const Vec4<V>& v) const
	{
		return x < v.x && y < v.y && z < v.z && w < v.w;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec4<T>::operator<=(const Vec4<V>& v) const
	{
		return x <= v.x && y <= v.y && z <= v.z && w <= v.w;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec4<T>::operator>(const Vec4<V>& v) const
	{
		return x > v.x && y > v.y && z > v.z && w > v.w;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec4<T>::operator>=(const Vec4<V>& v) const
	{
		return x >= v.x && y >= v.y && z >= v.z && w >= v.w;
	}

	template <typename T>
	HV_FORCE_INL T Vec4<T>::MaxComponent() const
	{
		return Math::Max(x, y, z, w);
	}

	template <typename T>
	HV_FORCE_INL T Vec4<T>::MinComponent() const
	{
		return Math::Min(x, y, z, w);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL T Vec4<T>::Dot(const Vec4<V>& v) const
	{
		return x * v.x + y * v.y + z * v.z + w * v.w;
	}

	template <typename T>
	HV_FORCE_INL T Vec4<T>::Length() const
	{
		return Math::Sqrt(x * x + y * y + z * z + w * w);
	}

	template <typename T>
	HV_FORCE_INL T Vec4<T>::SqLength() const
	{
		return x * x + y * y + z * z + w * w;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL T Vec4<T>::Distance(const Vec4<V>& v) const
	{
		T tx = x - v.x;
		T ty = y - v.y;
		T tz = z - v.z;
		T tw = w - v.w;
		return Math::Sqrt(tx * tx + ty * ty + tz * tz + tw * tw);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL T Vec4<T>::SqDistance(const Vec4<V>& v) const
	{
		T tx = x - v.x;
		T ty = y - v.y;
		T tz = z - v.z;
		T tw = w - v.w;
		return tx * tx + ty * ty + tz * tz + tw * tw;
	}

	template <typename T>
	HV_INL Vec4<T> Vec4<T>::Normalized() const
	{
		T len = SqLength();
		if (len < g_Epsilon<T>)
			return *this;
		return *this * T(1) / Math::Sqrt(len);
	}

	template <typename T>
	HV_INL Vec4<T>& Vec4<T>::Normalize()
	{
		T len = SqLength();
		if (len < g_Epsilon<T>)
			return *this;
		*this *= T(1) / Math::Sqrt(len);
		return *this;
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_FORCE_INL Vec4<T> Vec4<T>::Lerp(const Vec4<V0>& v, V1 lf) const
	{
		return *this + lf * (v - *this);
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_FORCE_INL Vec4<T> Vec4<T>::LerpClamped(const Vec4<V0>& v, V1 lf) const
	{
		lf = ClampUnit(lf);
		return *this + lf * (v - *this);
	}

	template <typename T>
	template <typename V>
	HV_INL Vec4<T> Vec4<T>::Max(const Vec4<V>& v) const
	{
		return Vec4(Math::Max(x, v.x), Math::Max(y, v.y), Math::Max(z, v.z), Math::Max(w, v.w));
	}

	template <typename T>
	template <typename V>
	HV_INL Vec4<T> Vec4<T>::Min(const Vec4<V>& v) const
	{
		return Vec4(Math::Min(x, v.x), Math::Min(y, v.y), Math::Min(z, v.z), Math::Min(w, v.w));
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_INL Vec4<T> Vec4<T>::ClampedLength(V0 min, V1 max) const
	{
		T len = SqLength();
		if (len < min * min)
			return *this * min / Math::Sqrt(len);
		if (len > max * max)
			return *this * max / Math::Sqrt(len);
		return *this;
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_INL Vec4<T>& Vec4<T>::ClampLength(V0 min, V1 max)
	{
		T len = SqLength();
		if (len < min * min)
			*this *= min / Math::Sqrt(len);
		else if (len > max * max)
			*this *= max / Math::Sqrt(len);
		return *this;
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_INL Vec4<T> Vec4<T>::ClampedAxes(V0 min, V1 max) const
	{
		return Vec4(Math::Clamp(x, T(min), T(max)), Math::Clamp(y, T(min), T(max)), Math::Clamp(z, T(min), T(max)), Math::Clamp(w, T(min), T(max)));
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_INL Vec4<T>& Vec4<T>::ClampAxes(V0 min, V1 max)
	{
		x = Math::Clamp(x, T(min), T(max));
		y = Math::Clamp(y, T(min), T(max));
		z = Math::Clamp(z, T(min), T(max));
		w = Math::Clamp(w, T(min), T(max));
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec4<T>::Equals(const Vec4<V>& v) const
	{
		return Math::Equals(x, v.x) && Math::Equals(y, v.y) && Math::Equals(z, v.z) && Math::Equals(w, v.w);
	}

	template <typename T>
	template <typename V, typename E>
	HV_FORCE_INL b8 Vec4<T>::Equals(const Vec4<V>& v, E epsilon) const
	{
		return Math::Equals(x, v.x, epsilon) && Math::Equals(y, v.y, epsilon) && Math::Equals(z, v.z, epsilon) && Math::Equals(w, v.w, epsilon);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec4<T>::IsShorter(const Vec4<V>& v)
	{
		return SqLength() < v.SqLength();
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec4<T>::IsLonger(const Vec4<V>& v)
	{
		return SqLength() > v.SqLength();
	}

	template <typename T>
	HV_FORCE_INL b8 Vec4<T>::IsZero() const
	{
		return x == 0 && y == 0 && z == 0 && w == 0;
	}

	template <typename T>
	HV_FORCE_INL b8 Vec4<T>::IsNearlyZero() const
	{
		return Math::Abs(x) < g_Epsilon<T> && Math::Abs(y) < g_Epsilon<T> && Math::Abs(z) < g_Epsilon<T> && Math::Abs(w) < g_Epsilon<T>;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec4<T>::IsUniform(V tolerance) const
	{
		return Math::Equals(x, y, T(tolerance)) && Math::Equals(y, z, T(tolerance)) && Math::Equals(z, w, T(tolerance));
	}

	template <typename T>
	HV_FORCE_INL b8 Vec4<T>::IsUniform() const
	{
		return Math::Equals(x, y) && Math::Equals(y, z) && Math::Equals(z, w);
	}

	template <typename T>
	HV_FORCE_INL b8 Vec4<T>::IsNormalized() const
	{
		return Math::Equals(SqLength(), T(1));
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec4<T>::IsUnit(V sqTolerance)
	{
		return Math::Equals(SqLength(), T(1), T(sqTolerance));
	}

	template <typename T>
	HV_INL Vec4<T> Vec4<T>::Swizzle(u8 xSwizzle, u8 ySwizzle, u8 zSwizzle, u8 wSwizzle)
	{
		HV_ASSERT_MSG(xSwizzle < 4, "xSwizzle out of range!");
		HV_ASSERT_MSG(ySwizzle < 4, "ySwizzle out of range!");
		HV_ASSERT_MSG(zSwizzle < 4, "zSwizzle out of range!");
		HV_ASSERT_MSG(wSwizzle < 4, "wSwizzle out of range!");
		return Vec4(data[xSwizzle], data[ySwizzle], data[zSwizzle], data[wSwizzle]);
	}

	template <typename T>
	HV_INL Vec3<T> Vec4<T>::Swizzle(u8 xSwizzle, u8 ySwizzle, u8 zSwizzle)
	{
		HV_ASSERT_MSG(xSwizzle < 4, "xSwizzle out of range!");
		HV_ASSERT_MSG(ySwizzle < 4, "ySwizzle out of range!");
		HV_ASSERT_MSG(zSwizzle < 4, "zSwizzle out of range!");
		return Vec3<T>(data[xSwizzle], data[ySwizzle], data[zSwizzle]);
	}

	template <typename T>
	HV_INL Vec2<T> Vec4<T>::Swizzle(u8 xSwizzle, u8 ySwizzle)
	{
		HV_ASSERT_MSG(xSwizzle < 4, "xSwizzle out of range!");
		HV_ASSERT_MSG(ySwizzle < 4, "ySwizzle out of range!");
		return Vec2<T>(data[xSwizzle], data[ySwizzle]);
	}

	template <typename T, typename V>
	HV_FORCE_INL Vec4<T> operator*(V val, const Vec4<T>& v)
	{
		return Vec4<T>(v.x * val, v.y * val, v.z * val, v.w * val);
	}

	template<typename T>
	const Vec4<T> Vec4<T>::Zero = Vec4<T>(0);
	template<typename T>
	const Vec4<T> Vec4<T>::One = Vec4<T>(1);
	template<typename T>
	const Vec4<T> Vec4<T>::AxisX = Vec4<T>(1, 0, 0, 0);
	template<typename T>
	const Vec4<T> Vec4<T>::AxisY = Vec4<T>(0, 1, 0, 0);
	template<typename T>
	const Vec4<T> Vec4<T>::AxisZ = Vec4<T>(0, 0, 1, 0);
	template<typename T>
	const Vec4<T> Vec4<T>::AxisW = Vec4<T>(0, 0, 0, 1);

}