// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Quat.h: Quaternion
#pragma once
#include "Math/Quat.h" 

namespace Hv::Math {
	
	template <typename T>
	HV_FORCE_INL Quat<T>::Quat()
		: x(T(0))
		, y(T(0))
		, z(T(0))
		, w(T(0))
	{
	}

	template <typename T>
	template <typename X, typename Y, typename Z, typename W>
	HV_FORCE_INL Quat<T>::Quat(X x, Y y, Z z, W w)
		: x(T(x))
		, y(T(y))
		, z(T(z))
		, w(T(w))
	{
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Quat<T>::Quat(const Quat<V>& q)
		: x(T(q.x))
		, y(T(q.y))
		, z(T(q.z))
		, w(T(q.w))
	{
	}

	template <typename T>
	HV_FORCE_INL Quat<T>::Quat(const Quat& q)
		: x(q.x)
		, y(q.y)
		, z(q.z)
		, w(q.w)
	{
	}

	template <typename T>
	HV_FORCE_INL Quat<T>::Quat(Quat&& q) noexcept
		: x(q.x)
		, y(q.y)
		, z(q.z)
		, w(q.w)
	{
	}

	template <typename T>
	HV_FORCE_INL Quat<T> Quat<T>::operator+() const
	{
		return *this;
	}

	template <typename T>
	HV_FORCE_INL Quat<T> Quat<T>::operator-() const
	{
		return Quat(-x, -y, -z, -w);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Quat<T> Quat<T>::operator+(const Quat<V>& q) const
	{
		return Quat(x + q.x, y + q.y, z + q.z, w + q.w);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Quat<T> Quat<T>::operator-(const Quat<V>& q) const
	{
		return Quat(x - q.x, y - q.y, z - q.z, w - q.w);
	}

	template <typename T>
	template <typename V>
	HV_INL Quat<T> Quat<T>::operator*(const Quat<V>& q) const
	{
		return Quat(w * q.x + x * q.w + y * q.z - z * q.y,
			w * q.y + y * q.w + z * q.x - x * q.z,
			w * q.z + z * q.w + x * q.y - y * q.x,
			w * q.w - x * q.x - y * q.y - z * q.z);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Quat<T> Quat<T>::operator*(V v) const
	{
		return Quat(x * v, y * v, z * v, w * v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Quat<T> Quat<T>::operator/(V v) const
	{
		return Quat(x / v, y / v, z / v, w / v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Quat<T>& Quat<T>::operator=(const Quat<V>& q)
	{
		x = T(q.x);
		y = T(q.y);
		z = T(q.z);
		w = T(q.w);
		return *this;
	}

	template <typename T>
	HV_FORCE_INL Quat<T>& Quat<T>::operator=(const Quat& q)
	{
		HV_ASSERT_MSG(this != &q, "Self assignment!");
		x = q.x;
		y = q.y;
		z = q.z;
		w = q.w;
		return *this;
	}

	template <typename T>
	HV_FORCE_INL Quat<T>& Quat<T>::operator=(Quat&& q) noexcept
	{
		HV_ASSERT_MSG(this != &q, "Self assignment!");
		x = q.x;
		y = q.y;
		z = q.z;
		w = q.w;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Quat<T>& Quat<T>::operator+=(const Quat<V>& q)
	{
		x += T(q.x);
		y += T(q.y);
		z += T(q.z);
		w += T(q.w);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Quat<T>& Quat<T>::operator-=(const Quat<V>& q)
	{
		x -= T(q.x);
		y -= T(q.y);
		z -= T(q.z);
		w -= T(q.w);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL Quat<T>& Quat<T>::operator*=(const Quat<V>& q)
	{
		T tx = w * q.x + x * q.w + y * q.z - z * q.y;
		T ty = w * q.y + y * q.w + z * q.x - x * q.z;
		T tz = w * q.z + z * q.w + x * q.y - y * q.x;
		T tw = w * q.w - x * q.x - y * q.y - z * q.z;
		x = tx;
		y = ty;
		z = tz;
		w = tw;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Quat<T>& Quat<T>::operator*=(V v)
	{
		x *= T(v);
		y *= T(v);
		z *= T(v);
		w *= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Quat<T>& Quat<T>::operator/=(V v)
	{
		x /= T(v);
		y /= T(v);
		z /= T(v);
		w /= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Quat<T>::operator==(const Quat<V>& q) const
	{
		return x == q.x && y == q.y && z == q.z && w == q.w;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Quat<T>::operator!=(const Quat<V>& q) const
	{
		return x != q.x || y != q.y || z != q.z && w != q.w;
	}

	template <typename T>
	HV_FORCE_INL T Quat<T>::Norm() const
	{
		return Math::Sqrt(x * x + y * y + z * z + w * w);
	}

	template <typename T>
	HV_FORCE_INL T Quat<T>::SqNorm() const
	{
		return x * x + y * y + z * z + w * w;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL T Quat<T>::Dot(const Quat<V>& q)
	{
		return x * T(q.x) + y * T(q.y) + z * T(q.z) + w * T(q.w);
	}

	template <typename T>
	HV_INL Quat<T> Quat<T>::Normalized() const
	{
		T norm = SqNorm();
		if (norm < g_Epsilon<T>)
			return *this;
		T invSqrt = 1 / Math::Sqrt(norm);
		return Quat(x * invSqrt, y * invSqrt, z * invSqrt, w * invSqrt);
	}

	template <typename T>
	HV_INL Quat<T>& Quat<T>::Normalize()
	{
		T norm = SqNorm();
		if (norm < g_Epsilon<T>)
			return *this;
		T invSqrt = 1 / Math::Sqrt(norm);
		x *= invSqrt;
		y *= invSqrt;
		z *= invSqrt;
		w *= invSqrt;
		return *this;
	}

	template <typename T>
	HV_FORCE_INL Quat<T> Quat<T>::Conjugate() const
	{
		return Quat(-x, -y, -z, w);
	}

	template <typename T>
	HV_FORCE_INL Quat<T>& Quat<T>::ToConjugate()
	{
		x = -x;
		y = -y;
		z = -z;
		return *this;
	}

	template <typename T>
	HV_INL Quat<T> Quat<T>::Inverse() const
	{
		T norm = SqNorm();
		if (norm < g_Epsilon<T>)
			return *this;
		T invSqrt = 1 / Math::Sqrt(norm);
		return Quat(-x * invSqrt, -y * invSqrt, -z * invSqrt, w * invSqrt);
	}

	template <typename T>
	HV_INL Quat<T>& Quat<T>::ToInverse()
	{
		T norm = SqNorm();
		if (norm < g_Epsilon<T>)
			return *this;
		T invSqrt = 1 / Math::Sqrt(norm);
		x = -x * invSqrt;
		y = -y * invSqrt;
		z = -z * invSqrt;
		w = w * invSqrt;
		return *this;
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_INL Quat<T> Quat<T>::Lerp(const Quat<V0>& q, V1 lf)
	{
		return *this + lf * (q - *this);
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_INL Quat<T> Quat<T>::LerpClamped(const Quat<V0>& q, V1 lf)
	{
		lf = Math::ClampUnit(lf);
		return *this + lf * (q - *this);
	}

	// http://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/slerp/
	template <typename T>
	template <typename V0, typename V1>
	HV_INL Quat<T> Quat<T>::Slerp(const Quat<V0>& q, V1 lf)
	{
		Quat q0 = Normalized();
		Quat q1 = q.Normalized();

		if (lf == 0.f) return q0;
		if (lf == 1.f) return q1;

		T cosHalfTheta = q0.Dot(q1);

		if (cosHalfTheta < 0)
		{
			q1 = -q1;
			cosHalfTheta = -cosHalfTheta;
		}

		if (cosHalfTheta >= 1.f)
		{
			return q0;
		}

		T halfTheta = Math::ArcCos(cosHalfTheta);
		T sinHalfTheta = Math::Sqrt(T(1) - cosHalfTheta * cosHalfTheta);

		if (Math::Abs(sinHalfTheta) < g_Epsilon<T>)
		{
			return q0.Lerp(q1, .5);
		}

		lf = Math::Sin(lf * halfTheta) / sinHalfTheta;
		return q0.Lerp(q1, lf);
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_INL Quat<T> Quat<T>::SlerpClamped(const Quat<V0>& q, V1 lf)
	{
		Quat q0 = Normalized();
		Quat q1 = q.Normalized();
		lf = Math::ClampUnit(lf);

		if (lf == 0.f) return q0;
		if (lf == 1.f) return q1;

		T cosHalfTheta = q0.Dot(q1);

		if (cosHalfTheta < 0)
		{
			q1 = -q1;
			cosHalfTheta = -cosHalfTheta;
		}

		if (cosHalfTheta >= 1.f)
		{
			return q0;
		}

		T halfTheta = Math::ArcCos(cosHalfTheta);
		T sinHalfTheta = Math::Sqrt(T(1) - cosHalfTheta * cosHalfTheta);

		if (Math::Abs(sinHalfTheta) < g_Epsilon<T>)
		{
			return q0.Lerp(q1, .5);
		}

		lf = Math::Sin(lf * halfTheta) / sinHalfTheta;
		return q0.Lerp(q1, lf);
	}

	template <typename T>
	HV_FORCE_INL T Quat<T>::Angle() const
	{
		return T(2) * Math::ArcCos(w);
	}

	template <typename T>
	template <typename V>
	HV_INL T Quat<T>::Angle(const Quat<V>& q) const
	{
		return Math::WrapRadians(T(2) * (Math::ArcCos(w) - Math::ArcCos(q.w)));
	}

	template <typename T>
	HV_INL Vec3<T> Quat<T>::Euler() const
	{
		T r00 = 2 * (x * y + w * z);
		T r01 = w * w + x * x - y * y - z * z;
		T r10 = -2 * (x * z - w * y);
		T r20 = 2 * (y * z + w * x);
		T r21 = w * w - x * x - y * y + z * z;

		return Vec3<T>(Math::ArcTan(r20, r21),
			Math::ArcSin(r10),
			Math::ArcTan(r00, r01));
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_INL void Quat<T>::GetAxisAngle(const Vec3<V0>& axis, V1& angle) const
	{
		angle = T(2) * Math::ArcCos(w);
		T s = Math::Sqrt(T(1) - w * w);
		if (s < g_Epsilon<T>)
		{
			axis.x = x;
			axis.y = y;
			axis.z = z;
		}
		else
		{
			axis.x = x / s;
			axis.y = y / s;
			axis.z = z / s;
		}
	}

	template <typename T>
	HV_FORCE_INL T Quat<T>::Real() const
	{
		return w;
	}

	template <typename T>
	HV_FORCE_INL Vec3<T> Quat<T>::Imaginary() const
	{
		return Vec3<T>(x, y, z);
	}

	template <typename T>
	template <typename V>
	HV_INL b8 Quat<T>::Equals(const Quat<V>& q)
	{
		return Math::Equals(x, q.x) && Math::Equals(y, q.y) && Math::Equals(z, q.z) && Math::Equals(w, q.w);
	}

	template <typename T>
	template <typename V, typename E>
	HV_INL b8 Quat<T>::Equals(const Quat<V>& q, E epsilon)
	{
		return Math::Equals(x, q.x, epsilon) && Math::Equals(y, q.y, epsilon) && Math::Equals(z, q.z, epsilon) && Math::Equals(w, q.w, epsilon);
	}

	template <typename T>
	template <typename X, typename Y, typename Z>
	HV_INL Quat<T> Quat<T>::CreateEuler(X pitch, Y yaw, Z roll)
	{
		pitch /= 2;
		yaw /= 2;
		roll /= 2;
		T cx = Math::Cos(pitch);
		T sx = Math::Sin(pitch);
		T cy = Math::Cos(yaw);
		T sy = Math::Sin(yaw);
		T cz = Math::Cos(roll);
		T sz = Math::Sin(roll);

		return Quat(sx * cy * cz - cx * sy * sz,
			cx * sy * cz + sx * cy * sz,
			cx * cy * sz - sx * sy * cz,
			cx * cy * cz + sx * sy * sz);
	}

	template <typename T>
	template <typename V>
	HV_INL Quat<T> Quat<T>::CreateEuler(const Vec3<V>& v)
	{
		T pitch = T(v.x) / 2;
		T yaw = T(v.y) / 2;
		T roll = T(v.z) / 2;
		T cx = Math::Cos(pitch);
		T sx = Math::Sin(pitch);
		T cy = Math::Cos(yaw);
		T sy = Math::Sin(yaw);
		T cz = Math::Cos(roll);
		T sz = Math::Sin(roll);

		return Quat(sx * cy * cz - cx * sy * sz,
			cx * sy * cz + sx * cy * sz,
			cx * cy * sz - sx * sy * cz,
			cx * cy * cz + sx * sy * sz);
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_INL Quat<T> Quat<T>::CreateAxisAngle(const Vec3<V0>& axis, V1 angle)
	{
		T halfAngle = T(angle) / 2;
		T c = Math::Cos(halfAngle);
		T s = Math::Sin(halfAngle);
		return Quat(s * axis.x,
			s * axis.y,
			s * axis.z,
			c);
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_INL Quat<T> Quat<T>::CreateLookRotation(const Vec3<V0>& forward, const Vec3<V1>& up)
	{
		T dot = forward.Dot(forward, Vec3<T>::Forward);
		if (dot <= -1)
			return Quat(up.x, up.y, up.z, g_Pi<T>).Normalize();
		if (dot >= 1)
			return Quat::Identity;

		T angle = Math::ArcCos(dot);
		Vec3<T> axis = Vec3<T>::Forward.Cross(forward);
		axis.Normalize();
		return CreateAxisAngle(axis, angle);
	}

	// http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
	template <typename T>
	template <typename V>
	HV_INL Quat<T> Quat<T>::CreateFromMatrix(const Mat4<V>& m)
	{
		T w = Math::Sqrt(1 + m.m00 + m.m11 + m.m22) / 2;
		T w4 = w * 4;
		return Quat((m.m21 - m.m12) / w4,
			(m.m02 - m.m20) / w4,
			(m.m10 - m.m01) / w4,
			w);
	}

	template <typename T, typename V>
	Quat<T> operator*(V val, const Quat<T>& q)
	{
		return Quat<T>(q.x * val, q.y * val, q.z * val, q.w * val);
	}

	template<typename T>
	const Quat<T> Quat<T>::Identity = Quat<T>(0, 0, 0, 1);

}