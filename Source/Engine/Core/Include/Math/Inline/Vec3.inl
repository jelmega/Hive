// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Vec3.inl: 3D Vector
#pragma once
#include "Math/Vec3.h"
#include "Math/MathConstants.h"
#include "Math/MathFunc.h"
#include "Math/Vec4.h"
#include "Math/Vec2.h"
#include "Math/Mat3.h"

namespace Hv::Math {
	
	template <typename T>
	HV_FORCE_INL Vec3<T>::Vec3()
		: x(T(0))
		, y(T(0))
		, z(T(0))
	{
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>::Vec3(V val)
		: x(T(val))
		, y(T(val))
		, z(T(val))
	{
	}

	template <typename T>
	template <typename X, typename Y, typename Z>
	HV_FORCE_INL Vec3<T>::Vec3(X x, Y y, Z z)
		: x(T(x))
		, y(T(y))
		, z(T(z))
	{
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>::Vec3(const Vec4<V>& v)
		: x(T(v.x))
		, y(T(v.y))
		, z(T(v.z))
	{
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>::Vec3(const Vec3<V>& v)
		: x(T(v.x))
		, y(T(v.y))
		, z(T(v.z))
	{
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>::Vec3(const Vec2<V>& v)
		: x(T(v.x))
		, y(T(v.y))
		, z(T(0))
	{
	}

	template <typename T>
	template <typename V, typename Z>
	HV_FORCE_INL Vec3<T>::Vec3(const Vec2<V>& v, Z z)
		: x(T(v.x))
		, y(T(v.y))
		, z(T(z))
	{
	}

	template <typename T>
	template <typename X, typename V>
	HV_FORCE_INL Vec3<T>::Vec3(X x, const Vec2<V>& v)
		: x(T(x))
		, y(T(v.x))
		, z(T(v.y))
	{
	}

	template <typename T>
	HV_FORCE_INL Vec3<T>::Vec3(const Vec3& v)
		: x(v.x)
		, y(v.y)
		, z(v.z)
	{
	}

	template <typename T>
	HV_FORCE_INL Vec3<T>::Vec3(Vec3&& v) noexcept
		: x(v.x)
		, y(v.y)
		, z(v.z)
	{
	}

	template <typename T>
	HV_FORCE_INL T Vec3<T>::operator[](sizeT index) const
	{
		HV_ASSERT_MSG(index < 3, "Index out of range!");
		return data[index];
	}

	template <typename T>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator+() const
	{
		return *this;
	}

	template <typename T>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator-() const
	{
		return Vec3(-x, -y, -z);
	}

	template <typename T>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator~() const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator~ only valid with integral types!");
		return Vec3(~x, ~y, ~z);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator+(V v) const
	{
		return Vec3(x + v, y + v, z + v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator+(const Vec3<V>& v) const
	{
		return Vec3(x + v.x, y + v.y, z + v.z);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator-(V v) const
	{
		return Vec3(x - v, y - v, z - v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator-(const Vec3<V>& v) const
	{
		return Vec3(x - v.x, y - v.y, z - v.z);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator*(V v) const
	{
		return Vec3(x * v, y * v, z * v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator*(const Vec3<V>& v) const
	{
		return Vec3(x * v.x, y * v.y, z * v.z);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator/(V v) const
	{
		return Vec3(x / v, y / v, z / v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator/(const Vec3<V>& v) const
	{
		return Vec3(x / v.x, y / v.y, z / v.z);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator|(V v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec3(x | v, y | v, z | v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator|(const Vec3<V>& v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec3(x | v.x, y | v.y, z | v.z);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator&(V v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec3(x & v, y & v, z & v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator&(const Vec3<V>& v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec3(x & v.x, y & v.y, z & v.z);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator^(V v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec3(x ^ v, y ^ v, z ^ v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator^(const Vec3<V>& v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec3(x ^ v.x, y ^ v.y, z ^ v.z);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator<<(V v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec3(x << v, y << v, z << v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator<<(const Vec3<V>& v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec3(x << v.x, y << v.y, z << v.z);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator>>(V v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec3(x >> v, y >> v, z >> v);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::operator>>(const Vec3<V>& v) const
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		return Vec3(x >> v.x, y >> v.y, z >> v.z);
	}

	template <typename T>
	template <typename V>
	Vec3<T> Vec3<T>::operator*(const Mat3<V>& m) const
	{
		return Vec3(x * m.m00 + y * m.m10 + z * m.m20,
			x * m.m01 + y * m.m11 + z * m.m21,
			x * m.m02 + y * m.m12 + z * m.m22);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator=(const Vec3<V>& v)
	{
		x = T(v.x);
		y = T(v.y);
		z = T(v.z);
		return *this;
	}

	template <typename T>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator=(const Vec3& v)
	{
		HV_ASSERT_MSG(this != &v, "Self assignment!");
		x = v.x;
		y = v.y;
		z = v.z;
		return *this;
	}

	template <typename T>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator=(Vec3&& v) noexcept
	{
		HV_ASSERT_MSG(this != &v, "Self assignment!");
		x = v.x;
		y = v.y;
		z = v.z;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator+=(V v)
	{
		x += T(v);
		y += T(v);
		z += T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator+=(const Vec3<V>& v)
	{
		x += T(v.x);
		y += T(v.y);
		z += T(v.z);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator-=(V v)
	{
		x -= T(v);
		y -= T(v);
		z -= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator-=(const Vec3<V>& v)
	{
		x -= T(v.x);
		y -= T(v.y);
		z -= T(v.z);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator*=(V v)
	{
		x *= T(v);
		y *= T(v);
		z *= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator*=(const Vec3<V>& v)
	{
		x *= T(v.x);
		y *= T(v.y);
		z *= T(v.z);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator/=(V v)
	{
		x /= T(v);
		y /= T(v);
		z /= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator/=(const Vec3<V>& v)
	{
		x /= T(v.x);
		y /= T(v.y);
		z /= T(v.z);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator|=(V v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x |= T(v);
		y |= T(v);
		z |= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator|=(const Vec3<V>& v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x |= T(v.x);
		y |= T(v.y);
		z |= T(v.z);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator&=(V v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x &= T(v);
		y &= T(v);
		z &= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator&=(const Vec3<V>& v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x &= T(v.x);
		y &= T(v.y);
		z &= T(v.z);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator^=(V v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x ^= T(v);
		y ^= T(v);
		z ^= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator^=(const Vec3<V>& v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x ^= T(v.x);
		y ^= T(v.y);
		z ^= T(v.z);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator<<=(V v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x <<= T(v);
		y <<= T(v);
		z <<= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator<<=(const Vec3<V>& v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x <<= T(v.x);
		y <<= T(v.y);
		z <<= T(v.z);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator>>=(V v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x >>= T(v);
		y >>= T(v);
		z >>= T(v);
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T>& Vec3<T>::operator>>=(const Vec3<V>& v)
	{
		HV_STATIC_ASSERT_MSG(std::is_integral<T>::value, "operator| only valid with integral types!");
		x >>= T(v.x);
		y >>= T(v.y);
		z >>= T(v.z);
		return *this;
	}

	template <typename T>
	template <typename V>
	Vec3<T>& Vec3<T>::operator*=(const Mat3<T>& m)
	{
		T tx = x * m.m00 + y * m.m10 + z * m.m20;
		T ty = x * m.m01 + y * m.m11 + z * m.m21;
		T tz = x * m.m02 + y * m.m12 + z * m.m22;
		x = tx;
		y = ty;
		z = tz;
		return *this;
	}

	template <typename T>
	template<typename V>
	HV_FORCE_INL b8 Vec3<T>::operator==(const Vec3<V>& v) const
	{
		return x == v.x && y == v.y && z == v.z;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec3<T>::operator!=(const Vec3<V>& v) const
	{
		return x != v.x || y != v.y || z != v.z;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec3<T>::operator<(const Vec3<V>& v) const
	{
		return x < v.x && y < v.y && z < v.z;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec3<T>::operator<=(const Vec3<V>& v) const
	{
		return x <= v.x && y <= v.y && z <= v.z;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec3<T>::operator>(const Vec3<V>& v) const
	{
		return x > v.x && y > v.y && z > v.z;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec3<T>::operator>=(const Vec3<V>& v) const
	{
		return x >= v.x && y >= v.y && z >= v.z;
	}

	template <typename T>
	HV_FORCE_INL T Vec3<T>::MaxComponent() const
	{
		return Math::Max(x, y, z);
	}

	template <typename T>
	HV_FORCE_INL T Vec3<T>::MinComponent() const
	{
		return Math::Min(x, y, z);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL T Vec3<T>::Dot(const Vec3<V>& v) const
	{
		return x * v.x + y * v.y + z * v.z;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Vec3<T> Vec3<T>::Cross(const Vec3<V>& v) const
	{
		return Vec3(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
	}

	template <typename T>
	HV_FORCE_INL T Vec3<T>::Length() const
	{
		return Math::Sqrt(x * x + y * y + z * z);
	}

	template <typename T>
	HV_FORCE_INL T Vec3<T>::SqLength() const
	{
		return x * x + y * y + z * z;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL T Vec3<T>::Distance(const Vec3<V>& v) const
	{
		T tx = x - v.x;
		T ty = y - v.y;
		T tz = z - v.z;
		return Math::Sqrt(tx * tx + ty * ty + tz * tz);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL T Vec3<T>::SqDistance(const Vec3<V>& v) const
	{
		T tx = x - v.x;
		T ty = y - v.y;
		T tz = z - v.z;
		return tx * tx + ty * ty + tz * tz;
	}

	template <typename T>
	HV_INL Vec3<T> Vec3<T>::Normalized() const
	{
		T len = SqLength();
		if (len < g_Epsilon<T>)
			return *this;
		return *this * T(1) / Math::Sqrt(len);
	}

	template <typename T>
	HV_INL Vec3<T>& Vec3<T>::Normalize()
	{
		T len = SqLength();
		if (len < g_Epsilon<T>)
			return *this;
		*this *= T(1) / Math::Sqrt(len);
		return *this;
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_FORCE_INL Vec3<T> Vec3<T>::Lerp(const Vec3<V0>& v, V1 lf) const
	{
		return *this + lf * (v - *this);
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_FORCE_INL Vec3<T> Vec3<T>::LerpClamped(const Vec3<V0>& v, V1 lf) const
	{
		lf = ClampUnit(lf);
		return *this + lf * (v - *this);
	}

	template <typename T>
	template <typename V>
	HV_INL Vec3<T> Vec3<T>::Max(const Vec3<V>& v) const
	{
		return Vec3(Math::Max(x, v.x), Math::Max(y, v.y), Math::Max(z, v.z));
	}

	template <typename T>
	template <typename V>
	HV_INL Vec3<T> Vec3<T>::Min(const Vec3<V>& v) const
	{
		return Vec3(Math::Min(x, v.x), Math::Min(y, v.y), Math::Min(z, v.z));
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_INL Vec3<T> Vec3<T>::ClampedLength(V0 min, V1 max)  const
	{
		T len = SqLength();
		if (len < min * min)
			return *this * min / Math::Sqrt(len);
		if (len > max * max)
			return *this * max / Math::Sqrt(len);
		return *this;
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_INL Vec3<T>& Vec3<T>::ClampLength(V0 min, V1 max)
	{
		T len = SqLength();
		if (len < min * min)
			*this *= min / Math::Sqrt(len);
		else if (len > max * max)
			*this *= max / Math::Sqrt(len);
		return *this;
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_INL Vec3<T> Vec3<T>::ClampedAxes(V0 min, V1 max)  const
	{
		return Vec3(Math::Clamp(x, T(min), T(max)), Math::Clamp(y, T(min), T(max)), Math::Clamp(z, T(min), T(max)));
	}

	template <typename T>
	template <typename V0, typename V1>
	HV_INL Vec3<T>& Vec3<T>::ClampAxes(V0 min, V1 max)
	{
		x = Math::Clamp(x, T(min), T(max));
		y = Math::Clamp(y, T(min), T(max));
		z = Math::Clamp(z, T(min), T(max));
		return *this;
	}

	template <typename T>
	template <typename V>
	Vec3<T> Vec3<T>::Rotated(const Quat<V>& q) const
	{
		Vec3 t = 2 * q.xyz.Cross(*this);
		return *this + q.w * t * q.xyz.Cross(t);
	}

	template <typename T>
	template <typename V>
	Vec3<T> Vec3<T>::Rotate(const Quat<V>& q)
	{
		Vec3 t = 2 * q.xyz.Cross(*this);
		*this += q.w * t * q.xyz.Cross(t);
		return *this;
	}

	template <typename T>
	Quat<T> Vec3<T>::Rotation() const
	{
		T dot = z; // Dot ((0, 0, 1))
		if (dot < -1 + g_Epsilon<T> || dot > 1 - g_Epsilon<T>)
			return Quat<T>::Identity;
		Quat<T> q;
		q.xyz = Cross(Vec3::Forward);
		q.w = Math::Sqrt(SqLength()) + z;
		return q;
	}

	template <typename T>
	template <typename V>
	Quat<T> Vec3<T>::Rotation(const Vec3<V>& v)
	{
		T dot = Dot(v);
		if (dot < -1 + g_Epsilon<T> || dot > 1 - g_Epsilon<T>)
			return Quat<T>::Identity;
		Quat<T> q;
		q.xyz = Cross(v);
		q.w = Math::Sqrt(SqLength() * v.SqLength()) + Dot(v);
		return q;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec3<T>::Equals(const Vec3<V>& v) const
	{
		return Math::Equals(x, v.x) && Math::Equals(y, v.y) && Math::Equals(z, v.z);
	}

	template <typename T>
	template <typename V, typename E>
	HV_FORCE_INL b8 Vec3<T>::Equals(const Vec3<V>& v, E epsilon) const
	{
		return Math::Equals(x, v.x, epsilon) && Math::Equals(y, v.y, epsilon) && Math::Equals(z, v.z, epsilon);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec3<T>::IsShorter(const Vec3<V>& v)
	{
		return SqLength() < v.SqLength();
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec3<T>::IsLonger(const Vec3<V>& v)
	{
		return SqLength() > v.SqLength();
	}

	template <typename T>
	HV_FORCE_INL b8 Vec3<T>::IsZero() const
	{
		return x == 0 && y == 0 && z == 0;
	}

	template <typename T>
	HV_FORCE_INL b8 Vec3<T>::IsNearlyZero() const
	{
		return Math::Abs(x) < g_Epsilon<T> && Math::Abs(y) < g_Epsilon<T> && Math::Abs(z) < g_Epsilon<T>;
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec3<T>::IsUniform(V tolerance) const
	{
		return Math::Equals(x, y, T(tolerance)) && Math::Equals(y, z, T(tolerance));
	}

	template <typename T>
	HV_FORCE_INL b8 Vec3<T>::IsUniform() const
	{
		return Math::Equals(x, y) && Math::Equals(y, z);
	}

	template <typename T>
	HV_FORCE_INL b8 Vec3<T>::IsNormalized() const
	{
		return Math::Equals(SqLength(), T(1));
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL b8 Vec3<T>::IsUnit(V sqTolerance)
	{
		return Math::Equals(SqLength(), T(1), T(sqTolerance));
	}

	template <typename T>
	Vec4<T> Vec3<T>::Swizzle(u8 xSwizzle, u8 ySwizzle, u8 zSwizzle, u8 wSwizzle)
	{
		HV_ASSERT_MSG(xSwizzle < 3, "xSwizzle out of range!");
		HV_ASSERT_MSG(ySwizzle < 3, "ySwizzle out of range!");
		HV_ASSERT_MSG(zSwizzle < 3, "zSwizzle out of range!");
		HV_ASSERT_MSG(wSwizzle < 3, "wSwizzle out of range!");
		return Vec3(data[xSwizzle], data[ySwizzle], data[zSwizzle], data[wSwizzle]);
	}

	template <typename T>
	HV_INL Vec3<T> Vec3<T>::Swizzle(u8 xSwizzle, u8 ySwizzle, u8 zSwizzle)
	{
		HV_ASSERT_MSG(xSwizzle < 3, "xSwizzle out of range!");
		HV_ASSERT_MSG(ySwizzle < 3, "ySwizzle out of range!");
		HV_ASSERT_MSG(zSwizzle < 3, "zSwizzle out of range!");
		return Vec3(data[xSwizzle], data[ySwizzle], data[zSwizzle]);
	}

	template <typename T>
	Vec2<T> Vec3<T>::Swizzle(u8 xSwizzle, u8 ySwizzle)
	{
		HV_ASSERT_MSG(xSwizzle < 3, "xSwizzle out of range!");
		HV_ASSERT_MSG(ySwizzle < 3, "ySwizzle out of range!");
		return Vec2<T>(data[xSwizzle], data[ySwizzle]);
	}

	template<typename T, typename V>
	HV_FORCE_INL Vec3<T> operator*(V val, const Vec3<T>& v)
	{
		return Vec3<T>(v.x * val, v.y * val, v.z * val);
	}

	template <typename T, typename V>
	Quat<T> operator*(const Vec3<T>& v, const Quat<V>& q)
	{
		return Quat<T>(v.x * q.w + v.y * q.z - v.z * q.y,
			v.y * q.w + v.z * q.x - v.x * q.z,
			v.z * q.w + v.x * q.y - v.y * q.x,
			v.x * q.x - v.y * q.y - v.z * q.z);
	}

	template <typename T, typename V>
	Vec3<T> operator*(const Quat<V>& q, const Vec3<T>& v)
	{
		return Vec3<T>(q.w * v.x + q.y * v.z - q.z * v.y,
			q.w * v.y + q.z * v.x - q.x * v.z,
			q.w * v.z + q.x * v.y - q.y * v.x);
	}

	template<typename T>
	const Vec3<T> Vec3<T>::Zero = Vec3<T>(0);
	template<typename T>
	const Vec3<T> Vec3<T>::One = Vec3<T>(1);
	template<typename T>
	const Vec3<T> Vec3<T>::AxisX = Vec3<T>(1, 0, 0);
	template<typename T>
	const Vec3<T> Vec3<T>::AxisY = Vec3<T>(0, 1, 0);
	template<typename T>
	const Vec3<T> Vec3<T>::AxisZ = Vec3<T>(0, 0, 1);

	template<typename T>
	const Vec3<T> Vec3<T>::Left = Vec3<T>(1, 0, 0);
	template<typename T>
	const Vec3<T> Vec3<T>::Right = Vec3<T>(-1, 0, 0);
	template<typename T>
	const Vec3<T> Vec3<T>::Up = Vec3<T>(0, 1, 0);
	template<typename T>
	const Vec3<T> Vec3<T>::Down = Vec3<T>(0, -1, 0);
	template<typename T>
	const Vec3<T> Vec3<T>::Forward = Vec3<T>(0, 0, 1);
	template<typename T>
	const Vec3<T> Vec3<T>::Backward = Vec3<T>(0, 0, -1);

}