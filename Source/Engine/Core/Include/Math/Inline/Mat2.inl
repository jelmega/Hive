// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Mat2.inl: 2x2 matrix
#pragma once
#include "Math/Mat2.h"

namespace Hv::Math {
	
	template <typename T>
	HV_INL Mat2<T>::Mat2()
		: m00(T(0)), m01(T(0))
		, m10(T(0)), m11(T(0))
	{
	}

	template <typename T>
	template <typename V>
	HV_INL Mat2<T>::Mat2(V diag)
		: m00(T(diag)), m01(T(0))
		, m10(T(0)), m11(T(diag))
	{
	}

	template <typename T>
	template <typename V00, typename V01, typename V10, typename V11>
	HV_INL Mat2<T>::Mat2(V00 v00, V01 v01, V10 v10, V11 v11)
		: m00(T(v00)), m01(T(v01))
		, m10(T(v10)), m11(T(v11))
	{
	}

	template <typename T>
	template <typename R0, typename R1>
	HV_INL Mat2<T>::Mat2(const Vec2<R0>& r0, const Vec2<R1>& r1)
		: r0(r0)
		, r1(r1)
	{
	}

	template <typename T>
	template <typename V>
	HV_INL Mat2<T>::Mat2(const Mat2<V>& m)
		: r0(m.r0)
		, r1(m.r1)
	{
	}

	template <typename T>
	HV_INL Mat2<T>::Mat2(const Mat2& m)
		: r0(m.r0)
		, r1(m.r1)
	{
	}

	template <typename T>
	HV_INL Mat2<T>::Mat2(Mat2&& m) noexcept
		: r0(m.r0)
		, r1(m.r1)
	{
	}

	template <typename T>
	HV_FORCE_INL Mat2<T> Mat2<T>::operator+() const
	{
		return *this;
	}

	template <typename T>
	HV_INL Mat2<T> Mat2<T>::operator-() const
	{
		return Mat2(-r0, -r1);
	}

	template <typename T>
	template <typename V>
	HV_INL Mat2<T> Mat2<T>::operator+(const Mat2<V>& m) const
	{
		return Mat2(r0 + m.r0, r1 + m.r1);
	}

	template <typename T>
	template <typename V>
	HV_INL Mat2<T> Mat2<T>::operator-(const Mat2<V>& m) const
	{
		return Mat2(r0 - m.r0, r1 - m.r1);
	}

	template <typename T>
	template <typename V>
	HV_INL Mat2<T> Mat2<T>::operator*(const Mat2<V>& m) const
	{
		Vec2<T> c0(m.m00, m.m10);
		Vec2<T> c1(m.m01, m.m11);

		return Mat2(c0.Dot(r0), c1.Dot(r0),
			c0.Dot(r1), c1.Dot(r1));
	}

	template <typename T>
	template <typename V>
	HV_INL Mat2<T> Mat2<T>::operator*(V v) const
	{
		return Mat2(r0 * v, r1 * v);
	}

	template <typename T>
	template <typename V>
	Mat2<T> Mat2<T>::operator/(V v) const
	{
		return Mat2(r0 / v, r1 / v);
	}

	template <typename T>
	template <typename V>
	HV_INL Mat2<T>& Mat2<T>::operator=(const Mat2<V>& m)
	{
		r0 = m.r0;
		r1 = m.r1;
		return *this;
	}

	template <typename T>
	HV_INL Mat2<T>& Mat2<T>::operator=(const Mat2& m)
	{
		HV_ASSERT_MSG(this != &m, "Self assignment!");
		r0 = m.r0;
		r1 = m.r1;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL Mat2<T>& Mat2<T>::operator=(Mat2&& m) noexcept
	{
		HV_ASSERT_MSG(this != &m, "Self assignment!");
		r0 = m.r0;
		r1 = m.r1;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL Mat2<T>& Mat2<T>::operator+=(const Mat2<V>& m)
	{
		r0 += m.r0;
		r1 += m.r1;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL Mat2<T>& Mat2<T>::operator-=(const Mat2<V>& m)
	{
		r0 -= m.r0;
		r1 -= m.r1;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL Mat2<T>& Mat2<T>::operator*=(const Mat2<V>& m)
	{
		Vec2<T> c0(m.m00, m.m10);
		Vec2<T> c1(m.m01, m.m11);

		Vec2<T> tr0(c0.Dot(r0), c1.Dot(r0));
		Vec2<T> tr1(c0.Dot(r1), c1.Dot(r1));
		r0 = tr0;
		r1 = tr1;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL Mat2<T>& Mat2<T>::operator*=(V v)
	{
		r0 *= v;
		r1 *= v;
		return *this;
	}

	template <typename T>
	template <typename V>
	Mat2<T>& Mat2<T>::operator/=(V v)
	{
		r0 /= v;
		r1 /= v;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL b8 Mat2<T>::operator==(const Mat2<V>& m) const
	{
		return r0 == m.r0 && r1 == m.r1;
	}

	template <typename T>
	template <typename V>
	HV_INL b8 Mat2<T>::operator!=(const Mat2<V>& m) const
	{
		return r0 != m.r0 || r1 != m.r1;
	}

	template <typename T>
	HV_INL T Mat2<T>::Determinant() const
	{
		return m00 * m11 - m01 * m10;
	}

	template <typename T>
	HV_INL Mat2<T> Mat2<T>::Transposed() const
	{
		return Mat2(m00, m10,
			m01, m11);
	}

	template <typename T>
	HV_INL Mat2<T>& Mat2<T>::Transpose()
	{
		Hv::Swap(m01, m10);
		return *this;
	}

	template <typename T>
	HV_INL Mat2<T> Mat2<T>::Cofactor() const
	{
		return Mat2(m11, -m10, -m01, m00);
	}

	template <typename T>
	HV_INL Mat2<T>& Mat2<T>::ToCofactor()
	{
		Hv::Swap(m00, m11);
		Hv::Swap(m01, m10);
		m01 = -m01;
		m10 = -m10;
		return *this;
	}

	template <typename T>
	HV_INL Mat2<T> Mat2<T>::Adjugate() const //Transposed cofactor
	{
		return Mat2(m11, -m01, -m10, m00);
	}

	template <typename T>
	HV_INL Mat2<T>& Mat2<T>::ToAdjugate() //Transposed cofactor
	{
		Hv::Swap(m00, m11);
		m01 = -m01;
		m10 = -m10;
		return *this;
	}

	template <typename T>
	HV_INL Mat2<T> Mat2<T>::Inverse() const
	{
		return 1 / Determinant() * Adjugate();
	}

	template <typename T>
	HV_INL Mat2<T>& Mat2<T>::ToInverse()
	{
		ToAdjugate();
		*this *= 1 / Determinant();
		return *this;
	}

	template <typename T>
	HV_FORCE_INL Vec2<T> Mat2<T>::Row(u8 row) const
	{
		HV_ASSERT_MSG(row < 2, "Row out of range!");
		return rows[row];
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL void Mat2<T>::Row(u8 row, const Vec2<V>& v)
	{
		HV_ASSERT_MSG(row < 2, "Row out of range!");
		rows[row] = v;
	}

	template <typename T>
	HV_INL Vec2<T> Mat2<T>::Column(u8 column) const
	{
		HV_ASSERT_MSG(column < 2, "Column out of range!");
		switch (column)
		{
		default:
		case 0:
			return Vec2<T>(m00, m10);
		case 1:
			return Vec2<T>(m01, m11);
		}
	}

	template <typename T>
	template <typename V>
	HV_INL void Mat2<T>::Column(u8 column, const Vec2<V>& v)
	{
		HV_ASSERT_MSG(column < 2, "Column out of range!");
		switch (column)
		{
		default:
		case 0:
			m00 = v.x;
			m10 = v.y;
			break;
		case 1:
			m01 = v.x;
			m11 = v.y;
			break;
		}
	}

	template <typename T>
	HV_FORCE_INL Vec2<T> Mat2<T>::Diagonal() const
	{
		return Vec2<T>(m00, m11);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL void Mat2<T>::Diagonal(const Vec2<V>& v)
	{
		m00 = v.x;
		m11 = v.y;
	}

	template <typename T>
	template <typename V>
	HV_INL b8 Mat2<T>::Equals(const Mat2<V>& m) const
	{
		return r0.Equals(m.r0) && r1.Equals(m.r1);
	}

	template <typename T>
	template <typename V, typename E>
	HV_INL b8 Mat2<T>::Equals(const Mat2<V>& m, E epsilon) const
	{
		return r0.Equals(m.r0, epsilon) && r1.Equals(m.r1, epsilon);
	}

	template <typename T>
	HV_INL b8 Mat2<T>::IsIdentity() const
	{
		return *this == Identity;
	}

	template <typename T>
	template <typename V>
	Mat2<T> Mat2<T>::CreateRotation2D(V angle)
	{
		T s = Math::Sin(T(angle));
		T c = Math::Cos(T(angle));

		return Mat2(c, -s,
			s, c);
	}

	template <typename T>
	template <typename V>
	Mat2<T> Mat2<T>::CreateScale2D(const Vec2<V>& v)
	{
		return Mat2(v.x, 0,
			0, v.y);
	}

	template <typename T>
	template <typename X, typename Y>
	Mat2<T> Mat2<T>::CreateScale2D(X x, Y y)
	{
		return Mat2(x, 0,
			0, y);
	}

	template<typename T>
	const Mat2<T> Mat2<T>::Identity = Mat2<T>(1);

	template <typename T, typename V>
	HV_INL Mat2<T> operator*(V v, const Mat2<T>& m)
	{
		return Mat2<T>(m.r0 * v, m.r1 * v);
	}

}