// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Mat4.inl: 4x4 matrix
#pragma once
#include "Math/Mat4.h"

namespace Hv::Math {

#define DETERMINANT_3D(m00, m01, m02, m10, m11, m12, m20, m21, m22)\
	(m00 * (m11 * m22 - m12 * m21) - m01 * (m10 * m22 - m12 * m20) + m02 * (m10 * m21 - m11 * m20))

	template <typename T>
	HV_INL Mat4<T>::Mat4()
		: m00(T(0)), m01(T(0)), m02(T(0)), m03(T(0))
		, m10(T(0)), m11(T(0)), m12(T(0)), m13(T(0))
		, m20(T(0)), m21(T(0)), m22(T(0)), m23(T(0))
		, m30(T(0)), m31(T(0)), m32(T(0)), m33(T(0))
	{
	}

	template <typename T>
	template <typename V>
	HV_INL Mat4<T>::Mat4(V diag)
		: m00(T(diag)), m01(T(0)), m02(T(0)), m03(T(0))
		, m10(T(0)), m11(T(diag)), m12(T(0)), m13(T(0))
		, m20(T(0)), m21(T(0)), m22(T(diag)), m23(T(0))
		, m30(T(0)), m31(T(0)), m32(T(0)), m33(T(diag))
	{
	}

	template <typename T>
	template <typename V00, typename V01, typename V02, typename V03, typename V10, typename V11, typename V12, typename
		V13, typename V20, typename V21, typename V22, typename V23, typename V30, typename V31, typename V32, typename V33>
		HV_INL Mat4<T>::Mat4(V00 v00, V01 v01, V02 v02, V03 v03, V10 v10, V11 v11, V12 v12, V13 v13, V20 v20, V21 v21, V22 v22,
			V23 v23, V30 v30, V31 v31, V32 v32, V33 v33)
		: m00(T(v00)), m01(T(v01)), m02(T(v02)), m03(T(v03))
		, m10(T(v10)), m11(T(v11)), m12(T(v12)), m13(T(v13))
		, m20(T(v20)), m21(T(v21)), m22(T(v22)), m23(T(v23))
		, m30(T(v30)), m31(T(v31)), m32(T(v32)), m33(T(v33))
	{
	}

	template <typename T>
	template <typename R0, typename R1, typename R2, typename R3>
	HV_INL Mat4<T>::Mat4(const Vec4<R0>& r0, const Vec4<R1>& r1, const Vec4<R2>& r2, const Vec4<R3>& r3)
		: r0(r0)
		, r1(r1)
		, r2(r2)
		, r3(r3)
	{
	}

	template <typename T>
	template <typename V>
	HV_INL Mat4<T>::Mat4(const Mat4<V>& m)
		: r0(m.r0)
		, r1(m.r1)
		, r2(m.r2)
		, r3(m.r3)
	{
	}

	template <typename T>
	HV_INL Mat4<T>::Mat4(const Mat4& m)
		: r0(m.r0)
		, r1(m.r1)
		, r2(m.r2)
		, r3(m.r3)
	{
	}

	template <typename T>
	HV_INL Mat4<T>::Mat4(Mat4&& m) noexcept
		: r0(m.r0)
		, r1(m.r1)
		, r2(m.r2)
		, r3(m.r3)
	{
	}

	template <typename T>
	HV_FORCE_INL Mat4<T> Mat4<T>::operator+() const
	{
		return *this;
	}

	template <typename T>
	HV_INL Mat4<T> Mat4<T>::operator-() const
	{
		return Mat4(-r0, -r1, -r2, -r3);
	}

	template <typename T>
	template <typename V>
	HV_INL Mat4<T> Mat4<T>::operator+(const Mat4<V>& m) const
	{
		return Mat4(r0 + m.r0, r1 + m.r1, r2 + m.r2, r3 + m.r3);
	}

	template <typename T>
	template <typename V>
	HV_INL Mat4<T> Mat4<T>::operator-(const Mat4<V>& m) const
	{
		return Mat4(r0 - m.r0, r1 - m.r1, r2 - m.r2, r3 - m.r3);
	}

	template <typename T>
	template <typename V>
	HV_INL Mat4<T> Mat4<T>::operator*(const Mat4<V>& m) const
	{
		Vec4<T> c0(m.m00, m.m10, m.m20, m.m30);
		Vec4<T> c1(m.m01, m.m11, m.m21, m.m31);
		Vec4<T> c2(m.m02, m.m12, m.m22, m.m32);
		Vec4<T> c3(m.m03, m.m13, m.m23, m.m33);

		return Mat4(c0.Dot(r0), c1.Dot(r0), c2.Dot(r0), c3.Dot(r0),
			c0.Dot(r1), c1.Dot(r1), c2.Dot(r1), c3.Dot(r1),
			c0.Dot(r2), c1.Dot(r2), c2.Dot(r2), c3.Dot(r2),
			c0.Dot(r3), c1.Dot(r3), c2.Dot(r3), c3.Dot(r3));
	}

	template <typename T>
	template <typename V>
	HV_INL Mat4<T> Mat4<T>::operator*(V v) const
	{
		return Mat4(r0 * v, r1 * v, r2 * v, r3 * v);
	}

	template <typename T>
	template <typename V>
	Mat4<T> Mat4<T>::operator/(V v) const
	{
		return Mat4(r0 / v, r1 / v, r2 / v, r3 / v);
	}

	template <typename T>
	template <typename V>
	HV_INL Mat4<T>& Mat4<T>::operator=(const Mat4<V>& m)
	{
		r0 = m.r0;
		r1 = m.r1;
		r2 = m.r2;
		r3 = m.r3;
		return *this;
	}

	template <typename T>
	HV_INL Mat4<T>& Mat4<T>::operator=(const Mat4& m)
	{
		HV_ASSERT_MSG(this != &m, "Self assignment!");
		r0 = m.r0;
		r1 = m.r1;
		r2 = m.r2;
		r3 = m.r3;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL Mat4<T>& Mat4<T>::operator=(Mat4&& m) noexcept
	{
		HV_ASSERT_MSG(this != &m, "Self assignment!");
		r0 = m.r0;
		r1 = m.r1;
		r2 = m.r2;
		r3 = m.r3;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL Mat4<T>& Mat4<T>::operator+=(const Mat4<V>& m)
	{
		r0 += m.r0;
		r1 += m.r1;
		r2 += m.r2;
		r3 += m.r3;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL Mat4<T>& Mat4<T>::operator-=(const Mat4<V>& m)
	{
		r0 -= m.r0;
		r1 -= m.r1;
		r2 -= m.r2;
		r3 -= m.r3;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL Mat4<T>& Mat4<T>::operator*=(const Mat4<V>& m)
	{
		Vec4<T> c0(m.m00, m.m10, m.m20, m.m30);
		Vec4<T> c1(m.m01, m.m11, m.m21, m.m31);
		Vec4<T> c2(m.m02, m.m12, m.m22, m.m32);
		Vec4<T> c3(m.m03, m.m13, m.m23, m.m33);

		Vec4<T> tr0(c0.Dot(r0), c1.Dot(r0), c2.Dot(r0), c3.Dot(r0));
		Vec4<T> tr1(c0.Dot(r1), c1.Dot(r1), c2.Dot(r1), c3.Dot(r1));
		Vec4<T> tr2(c0.Dot(r2), c1.Dot(r2), c2.Dot(r2), c3.Dot(r2));
		Vec4<T> tr3(c0.Dot(r3), c1.Dot(r3), c2.Dot(r3), c3.Dot(r3));
		r0 = tr0;
		r1 = tr1;
		r2 = tr2;
		r3 = tr3;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL Mat4<T>& Mat4<T>::operator*=(V v)
	{
		r0 *= v;
		r1 *= v;
		r2 *= v;
		r3 *= v;
		return *this;
	}

	template <typename T>
	template <typename V>
	Mat4<T>& Mat4<T>::operator/=(V v)
	{
		r0 /= v;
		r1 /= v;
		r2 /= v;
		r3 /= v;
		return *this;
	}

	template <typename T>
	template <typename V>
	HV_INL b8 Mat4<T>::operator==(const Mat4<V>& m) const
	{
		return r0 == m.r0 && r1 == m.r1 && r2 == m.r2 && r3 == m.r3;
	}

	template <typename T>
	template <typename V>
	HV_INL b8 Mat4<T>::operator!=(const Mat4<V>& m) const
	{
		return r0 != m.r0 || r1 != m.r1 || r2 != m.r2 || r3 != m.r3;
	}

	template <typename T>
	HV_INL T Mat4<T>::Determinant() const
	{
		return	m00 * DETERMINANT_3D(m11, m12, m13, m21, m22, m23, m31, m32, m33) -
			m01 * DETERMINANT_3D(m10, m12, m13, m20, m22, m23, m30, m32, m33) +
			m02 * DETERMINANT_3D(m10, m11, m13, m20, m21, m23, m30, m31, m33) -
			m03 * DETERMINANT_3D(m10, m11, m12, m20, m21, m22, m30, m31, m32);
	}

	template <typename T>
	HV_INL Mat4<T> Mat4<T>::Transposed() const
	{
		return Mat4(m00, m10, m20, m30,
			m01, m11, m21, m31,
			m02, m12, m22, m32,
			m03, m13, m23, m33);
	}

	template <typename T>
	HV_INL Mat4<T>& Mat4<T>::Transpose()
	{
		Hv::Swap(m01, m10);
		Hv::Swap(m02, m20);
		Hv::Swap(m03, m30);
		Hv::Swap(m12, m21);
		Hv::Swap(m13, m31);
		Hv::Swap(m23, m32);
		return *this;
	}

	template <typename T>
	HV_INL Mat4<T> Mat4<T>::Cofactor() const
	{
		return Mat4(DETERMINANT_3D(m11, m12, m13, m21, m22, m23, m31, m32, m33),
			-DETERMINANT_3D(m10, m12, m13, m20, m22, m23, m30, m32, m33),
			DETERMINANT_3D(m10, m11, m13, m20, m21, m23, m30, m31, m33),
			-DETERMINANT_3D(m10, m11, m12, m20, m21, m22, m30, m31, m32),
			-DETERMINANT_3D(m01, m02, m03, m21, m22, m23, m31, m32, m33),
			DETERMINANT_3D(m00, m02, m03, m20, m22, m23, m30, m32, m33),
			-DETERMINANT_3D(m00, m01, m03, m20, m21, m23, m30, m31, m33),
			DETERMINANT_3D(m00, m01, m02, m20, m21, m22, m30, m31, m32),
			DETERMINANT_3D(m01, m02, m03, m11, m12, m13, m31, m32, m33),
			-DETERMINANT_3D(m00, m02, m03, m10, m12, m13, m30, m32, m33),
			DETERMINANT_3D(m00, m01, m03, m10, m11, m13, m30, m31, m33),
			-DETERMINANT_3D(m00, m01, m02, m10, m11, m12, m30, m31, m32),
			-DETERMINANT_3D(m01, m02, m03, m11, m12, m13, m21, m22, m23),
			DETERMINANT_3D(m00, m02, m03, m10, m12, m13, m20, m22, m23),
			-DETERMINANT_3D(m00, m01, m03, m10, m11, m13, m20, m21, m23),
			DETERMINANT_3D(m00, m01, m02, m10, m11, m12, m20, m21, m22));
	}

	template <typename T>
	HV_INL Mat4<T>& Mat4<T>::ToCofactor()
	{
		T t00 = DETERMINANT_3D(m11, m12, m13, m21, m22, m23, m31, m32, m33);
		T t01 = -DETERMINANT_3D(m10, m12, m13, m20, m22, m23, m30, m32, m33);
		T t02 = DETERMINANT_3D(m10, m11, m13, m20, m21, m23, m30, m31, m33);
		T t03 = -DETERMINANT_3D(m10, m11, m12, m20, m21, m22, m30, m31, m32);
		T t10 = -DETERMINANT_3D(m01, m02, m03, m21, m22, m23, m31, m32, m33);
		T t11 = DETERMINANT_3D(m00, m02, m03, m20, m22, m23, m30, m32, m33);
		T t12 = -DETERMINANT_3D(m00, m01, m03, m20, m21, m23, m30, m31, m33);
		T t13 = DETERMINANT_3D(m00, m01, m02, m20, m21, m22, m30, m31, m32);
		T t20 = DETERMINANT_3D(m01, m02, m03, m11, m12, m13, m31, m32, m33);
		T t21 = -DETERMINANT_3D(m00, m02, m03, m10, m12, m13, m30, m32, m33);
		T t22 = DETERMINANT_3D(m00, m01, m03, m10, m11, m13, m30, m31, m33);
		T t23 = -DETERMINANT_3D(m00, m01, m02, m10, m11, m12, m30, m31, m32);
		T t30 = -DETERMINANT_3D(m01, m02, m03, m11, m12, m13, m21, m22, m23);
		T t31 = DETERMINANT_3D(m00, m02, m03, m10, m12, m13, m20, m22, m23);
		T t32 = -DETERMINANT_3D(m00, m01, m03, m10, m11, m13, m20, m21, m23);
		T t33 = DETERMINANT_3D(m00, m01, m02, m10, m11, m12, m20, m21, m22);
		m00 = t00;
		m01 = t01;
		m02 = t02;
		m03 = t03;
		m10 = t10;
		m11 = t11;
		m12 = t12;
		m13 = t13;
		m20 = t20;
		m21 = t21;
		m22 = t22;
		m23 = t23;
		m30 = t30;
		m31 = t31;
		m32 = t32;
		m33 = t33;
		return *this;
	}

	template <typename T>
	HV_INL Mat4<T> Mat4<T>::Adjugate() const //Transposed cofactor
	{
		return Mat4(DETERMINANT_3D(m11, m12, m13, m21, m22, m23, m31, m32, m33),
			-DETERMINANT_3D(m01, m02, m03, m21, m22, m23, m31, m32, m33),
			DETERMINANT_3D(m01, m02, m03, m11, m12, m13, m31, m32, m33),
			-DETERMINANT_3D(m01, m02, m03, m11, m12, m13, m21, m22, m23),
			-DETERMINANT_3D(m10, m12, m13, m20, m22, m23, m30, m32, m33),
			DETERMINANT_3D(m00, m02, m03, m20, m22, m23, m30, m32, m33),
			-DETERMINANT_3D(m00, m02, m03, m10, m12, m13, m30, m32, m33),
			DETERMINANT_3D(m00, m02, m03, m10, m12, m13, m20, m22, m23),
			DETERMINANT_3D(m10, m11, m13, m20, m21, m23, m30, m31, m33),
			-DETERMINANT_3D(m00, m01, m03, m20, m21, m23, m30, m31, m33),
			DETERMINANT_3D(m00, m01, m03, m10, m11, m13, m30, m31, m33),
			-DETERMINANT_3D(m00, m01, m03, m10, m11, m13, m20, m21, m23),
			-DETERMINANT_3D(m10, m11, m12, m20, m21, m22, m30, m31, m32),
			DETERMINANT_3D(m00, m01, m02, m20, m21, m22, m30, m31, m32),
			-DETERMINANT_3D(m00, m01, m02, m10, m11, m12, m30, m31, m32),
			DETERMINANT_3D(m00, m01, m02, m10, m11, m12, m20, m21, m22));
	}

	template <typename T>
	HV_INL Mat4<T>& Mat4<T>::ToAdjugate() //Transposed cofactor
	{
		T t00 = DETERMINANT_3D(m11, m12, m13, m21, m22, m23, m31, m32, m33);
		T t01 = -DETERMINANT_3D(m01, m02, m03, m21, m22, m23, m31, m32, m33);
		T t02 = DETERMINANT_3D(m01, m02, m03, m11, m12, m13, m31, m32, m33);
		T t03 = -DETERMINANT_3D(m01, m02, m03, m11, m12, m13, m21, m22, m23);
		T t10 = -DETERMINANT_3D(m10, m12, m13, m20, m22, m23, m30, m32, m33);
		T t11 = DETERMINANT_3D(m00, m02, m03, m20, m22, m23, m30, m32, m33);
		T t12 = -DETERMINANT_3D(m00, m02, m03, m10, m12, m13, m30, m32, m33);
		T t13 = DETERMINANT_3D(m00, m02, m03, m10, m12, m13, m20, m22, m23);
		T t20 = DETERMINANT_3D(m10, m11, m13, m20, m21, m23, m30, m31, m33);
		T t21 = -DETERMINANT_3D(m00, m01, m03, m20, m21, m23, m30, m31, m33);
		T t22 = DETERMINANT_3D(m00, m01, m03, m10, m11, m13, m30, m31, m33);
		T t23 = -DETERMINANT_3D(m00, m01, m03, m10, m11, m13, m20, m21, m23);
		T t30 = -DETERMINANT_3D(m10, m11, m12, m20, m21, m22, m30, m31, m32);
		T t31 = DETERMINANT_3D(m00, m01, m02, m20, m21, m22, m30, m31, m32);
		T t32 = -DETERMINANT_3D(m00, m01, m02, m10, m11, m12, m30, m31, m32);
		T t33 = DETERMINANT_3D(m00, m01, m02, m10, m11, m12, m20, m21, m22);
		m00 = t00;
		m01 = t01;
		m02 = t02;
		m03 = t03;
		m10 = t10;
		m11 = t11;
		m12 = t12;
		m13 = t13;
		m20 = t20;
		m21 = t21;
		m22 = t22;
		m23 = t23;
		m30 = t30;
		m31 = t31;
		m32 = t32;
		m33 = t33;
		return *this;
	}

	template <typename T>
	HV_INL Mat4<T> Mat4<T>::Inverse() const
	{
		T det = Determinant();
		if (Math::IsNearlyZero(det))
			return *this;
		return Adjugate() / det;
	}

	template <typename T>
	HV_INL Mat4<T>& Mat4<T>::ToInverse()
	{
		T det = Determinant();
		if (Math::IsNearlyZero(det))
			return *this;
		ToAdjugate();
		*this /= det;
		return *this;
	}

	template <typename T>
	HV_FORCE_INL Vec4<T> Mat4<T>::Row(u8 row) const
	{
		HV_ASSERT_MSG(row < 4, "Row out of range!");
		return rows[row];
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL void Mat4<T>::Row(u8 row, const Vec4<V>& v)
	{
		HV_ASSERT_MSG(row < 4, "Row out of range!");
		rows[row] = v;
	}

	template <typename T>
	HV_INL Vec4<T> Mat4<T>::Column(u8 column) const
	{
		HV_ASSERT_MSG(column < 4, "Column out of range!");
		switch (column)
		{
		default:
		case 0:
			return Vec4<T>(m00, m10, m20, m30);
		case 1:
			return Vec4<T>(m01, m11, m21, m31);
		case 2:
			return Vec4<T>(m02, m12, m22, m32);
		case 3:
			return Vec4<T>(m03, m13, m23, m33);
		}
	}

	template <typename T>
	template <typename V>
	HV_INL void Mat4<T>::Column(u8 column, const Vec4<V>& v)
	{
		HV_ASSERT_MSG(column < 4, "Column out of range!");
		switch (column)
		{
		default:
		case 0:
			m00 = v.x;
			m10 = v.y;
			m20 = v.z;
			m30 = v.w;
			break;
		case 1:
			m01 = v.x;
			m11 = v.y;
			m21 = v.z;
			m31 = v.w;
			break;
		case 2:
			m02 = v.x;
			m12 = v.y;
			m22 = v.z;
			m32 = v.w;
			break;
		case 3:
			m03 = v.x;
			m13 = v.y;
			m23 = v.z;
			m33 = v.w;
			break;
		}
	}

	template <typename T>
	HV_FORCE_INL Vec4<T> Mat4<T>::Diagonal() const
	{
		return Vec4<T>(m00, m11, m22, m33);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL void Mat4<T>::Diagonal(const Vec4<V>& v)
	{
		m00 = v.x;
		m11 = v.y;
		m22 = v.z;
		m33 = v.w;
	}

	template <typename T>
	template <typename V>
	HV_INL b8 Mat4<T>::Equals(const Mat4<V>& m) const
	{
		return r0.Equals(m.r0) && r1.Equals(m.r1) && r2.Equals(m.r2) && r3.Equals(m.r3);
	}

	template <typename T>
	template <typename V, typename E>
	HV_INL b8 Mat4<T>::Equals(const Mat4<V>& m, E epsilon) const
	{
		return r0.Equals(m.r0, epsilon) && r1.Equals(m.r1, epsilon) && r2.Equals(m.r2, epsilon) && r3.Equals(m.r3, epsilon);
	}

	template <typename T>
	HV_INL b8 Mat4<T>::IsIdentity() const
	{
		return *this == Identity;
	}

	template <typename T>
	template <typename V>
	HV_INL Mat4<T> Mat4<T>::CreateRotation(const Quat<V>& q)
	{
		Quat<T> tq = q.Normalized();
		T xx = tq.x * tq.x;
		T yy = tq.y * tq.y;
		T zz = tq.z * tq.z;
		T xy = tq.x * tq.y;
		T xz = tq.x * tq.z;
		T yz = tq.y * tq.z;
		T xw = tq.x * tq.w;
		T yw = tq.y * tq.w;
		T zw = tq.z * tq.w;

		return Mat4(1 - 2 * (yy + zz), 2 * (xy - zw)    , 2 * (xz + yw)    , 0,
					2 * (xy + zw)    , 1 - 2 * (xx + zz), 2 * (yz - xw)    , 0,
					2 * (xz - yw)    , 2 * (yz + xw)    , 1 - 2 * (xx + yy), 0,
					0                , 0                , 0                , 1);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Mat4<T> Mat4<T>::CreateScale(const Vec3<V>& v)
	{
		return Mat4(v.x, 0  , 0  , 0,
					0  , v.y, 0  , 0,
					0  , 0  , v.z, 0,
					0  , 0  , 0  , 1);
	}

	template <typename T>
	template <typename X, typename Y, typename Z>
	HV_FORCE_INL Mat4<T> Mat4<T>::CreateScale(X x, Y y, Z z)
	{
		return Mat4(x, 0, 0, 0,
					0, y, 0, 0,
					0, 0, z, 0,
					0, 0, 0, 1);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Mat4<T> Mat4<T>::CreateTranslation(const Vec3<V>& v)
	{
		return Mat4(1  , 0  , 0  , 0,
					0  , 1  , 0  , 0,
					0  , 0  , 1  , 0,
					v.x, v.y, v.z, 1);
	}

	template <typename T>
	template <typename X, typename Y, typename Z>
	HV_FORCE_INL Mat4<T> Mat4<T>::CreateTranslation(X x, Y y, Z z)
	{
		return Mat4(1, 0, 0, 0,
					0, 1, 0, 0,
					0, 0, 1, 0,
					x, y, z, 1);
	}

	template <typename T>
	template <typename V0, typename V1, typename V2>
	Mat4<T> Mat4<T>::CreateTransform(Vec3<V0>& scale, Quat<V1>& rotation, Vec3<V2>& translation)
	{
		Quat<T> tq = rotation.Normalized();
		T xx = tq.x * tq.x;
		T yy = tq.y * tq.y;
		T zz = tq.z * tq.z;
		T xy = tq.x * tq.y;
		T xz = tq.x * tq.z;
		T yz = tq.y * tq.z;
		T xw = tq.x * tq.w;
		T yw = tq.y * tq.w;
		T zw = tq.z * tq.w;

		return Mat4(scale.x * Vec4<T>(1 - 2 * (yy + zz), 2 * (xy - zw), 2 * (xz + yw), 0),
					scale.y * Vec4<T>(2 * (xy + zw), 1 - 2 * (xx + zz), 2 * (yz - xw), 0),
					scale.z * Vec4<T>(2 * (xz - yw), 2 * (yz + xw), 1 - 2 * (xx + yy), 0),
					Vec4<T>(translation, 1));
	}

	template <typename T>
	template <typename V>
	HV_INL Mat4<T> Mat4<T>::CreateRotation2D(V angle)
	{
		T s = Math::Sin(T(angle));
		T c = Math::Cos(T(angle));

		return Mat4(c, -s, 0, 0,
					s,  c, 0, 0,
					0,  0, 1, 0,
					0,  0, 0, 1);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Mat4<T> Mat4<T>::CreateScale2D(const Vec2<V>& v)
	{
		return Mat4(v.x, 0, 0, 0,
					0, v.y, 0, 0,
					0, 0, 1, 0,
					0, 0, 0, 1);
	}

	template <typename T>
	template <typename X, typename Y>
	HV_FORCE_INL Mat4<T> Mat4<T>::CreateScale2D(X x, Y y)
	{
		return Mat4(x, 0, 0, 0,
					0, y, 0, 0,
					0, 0, 1, 0,
					0, 0, 0, 1);
	}

	template <typename T>
	template <typename V>
	HV_FORCE_INL Mat4<T> Mat4<T>::CreateTranslation2D(const Vec2<V>& v)
	{
		return Mat4(1, 0, 0, 0,
					0, 1, 0, 0,
					0, 0, 1, 0,
					v.y, v.x, 0, 1);
	}

	template <typename T>
	template <typename X, typename Y>
	HV_FORCE_INL Mat4<T> Mat4<T>::CreateTranslation2D(X x, Y y)
	{
		return Mat4(1, 0, 0, 0,
					0, 1, 0, 0,
					0, 0, 1, 0,
					x, y, 0, 1);
	}

	template <typename T>
	template <typename V0, typename V1, typename V2>
	Mat4<T> Mat4<T>::CreateTransform2D(Vec2<V0>& scale, V1 rotation, Vec2<V2>& translation)
	{

		T s = Math::Sin(T(rotation));
		T c = Math::Cos(T(rotation)); 

		return Mat4(scale.x * Vec4<T>(c, -s, 0, 0),
					scale.y * Vec4<T>(s,  c, 0, 0),
					scale.z * Vec4<T>(0,  0, 1, 0),
					Vec4<T>(translation,  0, 1));
	}

	template <typename T>
	template <typename V0, typename V1, typename V2>
	Mat4<T> Mat4<T>::CreateLookat(const Vec3<V0>& eyePos, const Vec3<V1>& target, const Vec3<V2>& up)
	{
		Vec3<T> forward = (target - eyePos).Normalized();
		Vec3<T> right = up.Cross(forward).Normalized();
		Vec3<T> lookUp = forward.Cross(right);


		return Mat4(right.x           , lookUp.x           , forward.x           , 0,
					right.y           , lookUp.y           , forward.y           , 0,
					right.z           , lookUp.z           , forward.z           , 0,
					-right.Dot(eyePos), -lookUp.Dot(eyePos), -forward.Dot(eyePos), 1);
	}

	template <typename T>
	template <typename V0, typename V1, typename V2, typename V3, typename V4, typename V5>
	HV_INL Mat4<T> Mat4<T>::CreateOrthograpic(V0 left, V1 top, V2 right, V3 bottom, V4 znear, V5 zfar)
	{
		T width = T(right - left);
		T height = T(top - bottom);
		T depth = T(zfar - znear);

		return Mat4(2 / T(width), 0, 0, 0,
					0, 2 / height, 0, 0,
					0, 0, 1 / depth, 0,
					-T(right + left) / width, -T(top + bottom) / height, T(zfar + znear) / depth, 1);
	}

	template <typename T>
	template <typename V0, typename V1, typename V2, typename V3>
	HV_INL Mat4<T> Mat4<T>::CreateOrthograpic(V0 width, V1 height, V2 znear, V3 zfar)
	{
		T depth = T(zfar - znear);

		return Mat4(2 / T(width), 0            , 0                      , 0,
					0           , 2 / T(height), 0                      , 0,
					0           , 0            , 1 / T(depth)           , 0,
					0           , 0            , T(zfar + znear) / depth, 1);
	}

	template <typename T>
	template <typename V0, typename V1, typename V2, typename V3>
	HV_INL Mat4<T> Mat4<T>::CreatePerspective(V0 fov, V1 aspect, V2 znear, V3 zfar)
	{
		T tanHalfFov = Math::Tan(T(fov) / 2);
		T depth = T(zfar - znear);

		return Mat4(1 / (aspect * tanHalfFov), 0             , 0                        , 0,
					0                        , 1 / tanHalfFov, 0                        , 0,
					0                        , 0             , (zfar + znear) / depth   , 1,
					0                        , 0             , -2 * zfar * znear / depth, 0);
	}

	template<typename T>
	const Mat4<T> Mat4<T>::Identity = Mat4<T>(1);

	// Y is upside down for vulkan
	template<typename T>
	const Mat4<T> Mat4<T>::VulkanProjectionCorrection = Mat4<T>(1.f, 0.f, 0.f, 0.f,
		0.f, -1.f, 0.f, 0.f,
		0.f, 0.f, 1.f, 0.f,
		0.f, 0.f, 0.f, 1.f);
	// Z range is (-1,1), instead of (0,1) like other APIs, and OpenGL is right-handed, not left handed
	template<typename T>
	const Mat4<T> Mat4<T>::OpenGLProjectionCorrection = Mat4<T>(1.f, 0.f, 0.f, 0.f,
		0.f, 1.f, 0.f, 0.f,
		0.f, 0.f, -2.f, 0.f,
		0.f, 0.f, 2.f, 1.f);

	template <typename T, typename V>
	HV_INL Mat4<T> operator*(V v, const Mat4<T>& m)
	{
		return Mat4<T>(m.r0 * v, m.r1 * v, m.r2 * v, m.r3 * v);
	}

#undef DETERMINANT_3D

}