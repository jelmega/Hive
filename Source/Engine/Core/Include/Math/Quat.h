// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Quat.h: Quaternion
#pragma once
#include "Core/CoreHeaders.h"
#include "Containers/ContainerUtils.h"

namespace Hv::Math {
	
	template<typename T>
	struct Vec3;
	
	template<typename T>
	struct Quat
	{
#pragma warning(push)
#pragma warning(disable: 4201) // nonstandard extension used : nameless struct/union
		union
		{
			T data[4];
			struct
			{
				union
				{
					struct { T x, y, z; };
					Vec3<T> xyz;
				};
				
				T w;
			};
		};
#pragma warning(pop)

		/**
		 * Create a default quaternion
		 */
		Quat();
		/**
		 * Creata quaternion from values
		 * @tparam X	Arithmatic type
		 * @tparam Y	Arithmatic type
		 * @tparam Z	Arithmatic type
		 * @tparam W	Arithmatic type
		 * @param[in] x	X value
		 * @param[in] y	Y value
		 * @param[in] z	Z value
		 * @param[in] w	W value
		 */
		template<typename X, typename Y, typename Z, typename W>
		Quat(X x, Y y, Z z, W w);
		/**
		 * Create a quaternion from another quaternion
		 * @tparam V	Arithmatic type
		 * @param[in] q	Quaternion
		 */
		template<typename V>
		Quat(const Quat<V>& q);
		/**
		 * Create a quaternion from another quaternion
		 * @tparam V	Arithmatic type
		 * @param[in] q	Quaternion
		 */
		Quat(const Quat& q);
		/**
		 * Create a quaternion from another quaternion
		 * @tparam V	Arithmatic type
		 * @param[in] q	Quaternion
		 */
		Quat(Quat&& q) noexcept;

		Quat operator+() const;
		Quat operator-() const;

		template<typename V>
		Quat operator+(const Quat<V>& q) const;
		template<typename V>
		Quat operator-(const Quat<V>& q) const;
		template<typename V>
		Quat operator*(const Quat<V>& q) const;
		template<typename V>
		Quat operator*(V v) const;
		template<typename V>
		Quat operator/(V v) const;

		template<typename V>
		Quat& operator=(const Quat<V>& q);
		Quat& operator=(const Quat& q);
		Quat& operator=(Quat&& q) noexcept;

		template<typename V>
		Quat& operator+=(const Quat<V>& q);
		template<typename V>
		Quat& operator-=(const Quat<V>& q);
		template<typename V>
		Quat& operator*=(const Quat<V>& q);
		template<typename V>
		Quat& operator*=(V v);
		template<typename V>
		Quat& operator/=(V v);

		template<typename V>
		b8 operator==(const Quat<V>& q) const;
		template<typename V>
		b8 operator!=(const Quat<V>& q) const;

		/**
		 * Get the norm of the quaternion
		 * @return	Norm
		 */
		T Norm() const;
		/**
		 * Get the square norm of the quaternion
		 * @return	Square orm
		 */
		T SqNorm() const;
		/**
		 * Get the dot product of 2 quaternion
		 * @tparam V	Arithmatic type
		 * @param[in] q	Quaternion
		 * @return		Dot product
		 */
		template<typename V>
		T Dot(const Quat<V>& q);
		/**
		 * Get a normalized copy of the quaternion
		 * @return	Normalized quaternion
		 */
		Quat Normalized() const;
		/**
		 * Normalize the quaternion
		 * @return	Reference to the quaternion
		 */
		Quat& Normalize();

		/**
		 * Get the conjugate of the quaternion
		 * @return	Conjugate
		 */
		Quat Conjugate() const;
		/**
		 * Convert the quaternion into its conjugate
		 * @return	Reference to the quaternion
		 */
		Quat& ToConjugate();
		/**
		 * Get the inverse of the quaternion
		 * @return	Inverse
		 */
		Quat Inverse() const;
		/**
		 * Convert the quaternion into its inverse
		 * @return	Reference to the quaternion
		 */
		Quat& ToInverse();

		/**
		* Lerp between 2 quaternions
		* @tparam V0		Arithmatic type
		* @tparam V1		Arithmatic type
		* @param[in] q		Quaternion
		* @param[in] lf	Lerp factor
		*/
		template<typename V0, typename V1>
		Quat Lerp(const Quat<V0>& q, V1 lf);
		/**
		* Lerp between 2 quaternions with a clamped lerp factor
		* @tparam V0		Arithmatic type
		* @tparam V1		Arithmatic type
		* @param[in] q		Quaternion
		* @param[in] lf	Lerp factor
		*/
		template<typename V0, typename V1>
		Quat LerpClamped(const Quat<V0>& q, V1 lf);
		/**
		* Spherically interpolate between 2 quaternions
		* @tparam V0		Arithmatic type
		* @tparam V1		Arithmatic type
		* @param[in] q		Quaternion
		* @param[in] lf	Lerp factor
		*/
		template<typename V0, typename V1>
		Quat Slerp(const Quat<V0>& q, V1 lf);
		/**
		* Spherically interpolate between 2 quaternions with a clamped lerp factor
		* @tparam V0		Arithmatic type
		* @tparam V1		Arithmatic type
		* @param[in] q		Quaternion
		* @param[in] lf	Lerp factor
		*/
		template<typename V0, typename V1>
		Quat SlerpClamped(const Quat<V0>& q, V1 lf);

		/**
		* Get the current angle of the quaternion
		* @return	Angle
		*/
		T Angle() const;
		/**
		* Get the angle between 2 quaternions
		* @tparam V	Arithmatic type
		* @param[in] q	Quaternion
		* @return	Angle between quaternions
		*/
		template<typename V>
		T Angle(const Quat<V>& q) const;
		/**
		* Get the angle of the quaternion as euler angles
		* @return	Euler angles
		*/
		Vec3<T> Euler() const;
		/**
		* Get the angle between 2 quaternions
		* @tparam V0		Arithmatic type
		* @tparam V1		Arithmatic type
		* @param[out] axis	Rotation axis
		* @param[out] angle	Angle
		*/
		template<typename V0, typename V1>
		void GetAxisAngle(const Vec3<V0>& axis, V1& angle) const;
		/**
		* Get the real part of the quaternion (w)
		* @return Real part
		*/
		T Real() const;
		/**
		* Get the imaginary part of the quaternion (x, y, z)
		* @return Imaginary part
		*/
		Vec3<T> Imaginary() const;

		/**
		 * Check if 2 quaternions are equal to eachother
		 * @tparam V	Arithmatic type
		 * @param[in] q	Quaternion
		 */
		template<typename V>
		b8 Equals(const Quat<V>& q);
		/**
		 * Check if 2 quaternions are equal to eachother
		 * @tparam V			Arithmatic type
		 * @tparam E			Arithmatic type
		 * @param[in] q			Quaternion
		 * @param[in] epsilon	Epsilon
		 */
		template<typename V, typename E>
		b8 Equals(const Quat<V>& q, E epsilon);

		/**
		 * [STATIC] Create a quaternion from euler angles
		 * @tparam X		Arithmatic type
		 * @tparam Y		Arithmatic type
		 * @tparam Z		Arithmatic type
		 * @param[in] pitch	Pitch (X-axis)
		 * @param[in] yaw	Yaw (Y-axis)
		 * @param[in] roll	Roll (Z-axis)
		 */
		template<typename X, typename Y, typename Z>
		static Quat CreateEuler(X pitch, Y yaw, Z roll);
		/**
		* [STATIC] Create a quaternion from euler angles
		* @tparam V		Arithmatic type
		* @param[in] v	Vector with euler angles
		* @return		Quaternion
		*/
		template<typename V>
		static Quat CreateEuler(const Vec3<V>& v);
		/**
		 * [STATIC] Create a quaternion from an axis and an angle
		 * @tparam V0		Arithmatic type
		 * @tparam V1		Arithmatic type
		 * @param[in] axis	Rotation axis
		 * @param[in] angle	Angle
		 * @return		Quaternion
		 */
		template<typename V0, typename V1>
		static Quat CreateAxisAngle(const Vec3<V0>& axis, V1 angle);
		/**
		 * [STATIC] Create a quaternion looking at a point
		 * @tparam V0			Arithmatic type
		 * @tparam V1			Arithmatic type
		 * @param[in] forward	Forward vector
		 * @param[in] up		Up factor
		 * @return		Quaternion
		 */
		template<typename V0, typename V1>
		static Quat CreateLookRotation(const Vec3<V0>& forward, const Vec3<V1>& up);

		/**
		 * Create a quaternion from a rotation matrix
		 * @tparam V	Arithmatic type
		 * @param[in] m	Rotation matrix
		 * @return		Quaternion
		 */
		template<typename V>
		static Quat CreateFromMatrix(const Mat4<V>& m);

		const static Quat Identity;

	};

	template<typename T, typename V>
	Quat<T> operator*(V val, const Quat<T>& q);

}

HV_DECLARE_CONTAINER_MEMCPY_TEMPLATE(HV_TARGS(typename T), HV_TTYPE(Hv::Math::Quat, T));
