// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// MathFunc.h: General math functions
#pragma once
#include "Core/CoreHeaders.h"

namespace Hv::Math {
	
	/**
	 * Get the most acurate type out of 2 types
	 * @tparam T0	Arithmatic type
	 * @tparam T1	Arithmatic type
	 */
	template<typename T0, typename T1>
	struct MostAccuarateType
	{
		HV_STATIC_ASSERT_MSG(Traits::IsArithmaticV<T0>, "T0 must to be an arithmatic type");
		HV_STATIC_ASSERT_MSG(Traits::IsArithmaticV<T1>, "T1 must to be an arithmatic type");
		/**
		 * Most acurate type
		 */
		using type = typename Traits::ConditionalT<
			(
			(
				(Traits::IsFloatingPointV<T0> && Traits::IsFloatingPointV<T1>) && sizeof(T0) >= sizeof(T1)) ||
				(Traits::IsFloatingPointV<T0> && !Traits::IsFloatingPointV<T1>) ||
				(sizeof(T0) >= sizeof(T1))
			)
			, T0, T1>;
	};

	/**
	 * Get the maximum value of 2 values
	 * @tparam T	Arithmatic type
	 * @param[in] a	Value
	 * @param[in] b	Value
	 * @return		Maximum value
	 */
	template<typename T>
	T Min(T a, T b);
	/**
	 * Get the maximum value of 3 values
	 * @tparam T	Arithmatic type
	 * @param[in] a	Value
	 * @param[in] b	Value
	 * @param[in] c	Value
	 * @return		Maximum value
	 */
	template<typename T>
	T Min(T a, T b, T c);
	/**
	 * Get the maximum value of 3 values
	 * @tparam T	Arithmatic type
	 * @param[in] a	Value
	 * @param[in] b	Value
	 * @param[in] c	Value
	 * @param[in] d	Value
	 * @return		Maximum value
	 */
	template<typename T>
	T Min(T a, T b, T c, T d);
	/**
	 * Get the maximum value of from a veriable amount of values
	 * @tparam T0			Type of first element (arithmatic) (also used as return type)
	 * @tparam T1			Arithmatic types (all types need to be implicitly convertable to T0)
	 * @param[in] a			1st value
	 * @param[in] b			2nd value
	 * @param[in] c			3rd value
	 * @param[in] d			4th value
	 * @param[in] values	Other values
	 * @return				Maximum value
	 */
	template<typename T0, typename... T1>
	T0 Min(T0 a, T0 b, T0 c, T0 d, T1... values);

	/**
	 * Get the maximum value of 2 values
	 * @tparam T	Arithmatic type
	 * @param[in] a	Value
	 * @param[in] b	Value
	 * @return		Maximum value
	 */
	template<typename T>
	T Max(T a, T b);
	/**
	 * Get the maximum value of 3 values
	 * @tparam T	Arithmatic type
	 * @param[in] a	Value
	 * @param[in] b	Value
	 * @param[in] c	Value
	 * @return		Maximum value
	 */
	template<typename T>
	T Max(T a, T b, T c);
	/**
	 * Get the maximum value of 3 values
	 * @tparam T	Arithmatic type
	 * @param[in] a	Value
	 * @param[in] b	Value
	 * @param[in] c	Value
	 * @param[in] d	Value
	 * @return		Maximum value
	 */
	template<typename T>
	T Max(T a, T b, T c, T d);
	/**
	 * Get the maximum value of from a veriable amount of values
	 * @tparam T0			Type of first element (arithmatic) (also used as return type)
	 * @tparam T1			Arithmatic types (all types need to be implicitly convertable to T0)
	 * @param[in] a			1st value
	 * @param[in] b			2nd value
	 * @param[in] c			3rd value
	 * @param[in] d			4th value
	 * @param[in] values	Other values
	 * @return				Maximum value
	 */
	template<typename T0, typename... T1>
	T0 Max(T0 a, T0 b, T0 c, T0 d, T1... values);

	/**
	 * Get the absolute value of a value
	 * @tparam T	Arithmatic type
	 * @param[in] t	Value
	 * @return		Absolute value
	 */
	template<typename T>
	T Abs(T t);
	/**
	 * Select 'lt' or 'ge' based on the value of 't'
	 * @tparam T		Arithmatic type
	 * @param[in] t		Value to compare with
	 * @param[in] lt	Result value if 't' is less then 0
	 * @param[in] ge	Result value it 't' is greater or equal to 0
	 * @return			Result value
	 */
	template<typename T>
	T Select(T t, T lt, T ge);
	/**
	 * Select 'lt' or 'ge' based on the relationship between 't' and 'comp'
	 * @tparam T		Arithmatic type
	 * @param[in] t		Value to compare with
	 * @param[in] comp	Value to compare to
	 * @param[in] lt	Result value if 't' is less then 'comp'
	 * @param[in] ge	Result value it 't' is greater or equal to 'comp'
	 * @return			Result value
	 */
	template<typename T>
	T Select(T t, T comp, T lt, T ge);
	/**
	 * Get the sign of a value
	 * @tparam T	Arithmatic type
	 * @param[in] t	Value
	 * @return		Sign of the value
	 */
	template<typename T>
	T Sign(T t);
	/**
	 * Clamp a value between 2 values
	 * @tparam T		Arithmatic type
	 * @tparam V0	Arithmatic type
	 * @tparam V1	Arithmatic type
	 * @param[in] t		Value to clamp
	 * @param[in] min	Minimum value
	 * @param[in] max	Maximum value
	 * @return			Clamped value
	 */
	template<typename T, typename V0, typename V1>
	T Clamp(T t, V0 min, V1 max);
	/**
	 * Clamp a value between 0 and 1
	 * @tparam T	Arithmatic type
	 * @param[in] t	Value to clamp
	 * @return		Clamped value
	 */
	template<typename T>
	T ClampUnit(T t);
	/**
	 * Lerp between 2 values
	 * @tparam T	Arithmatic type
	 * @param[in] t	Lerp factor
	 * @param[in] a	1st value
	 * @param[in] b	2nd value
	 * @return		Lerped value
	 */
	template<typename T>
	T Lerp(T t, T a, T b);
	/**
	 * Lerp between 2 values with a clamped lerp factor
	 * @tparam T	Arithmatic type
	 * @param[in] t	Lerp factor
	 * @param[in] a	1st value
	 * @param[in] b	2nd value
	 * @return		Lerped value
	 */
	template<typename T>
	T LerpClamped(T t, T a, T b);
	/**
	 * Get the square root of a value
	 * @tparam T	Arithmatic type
	 * @param[in] t	Value
	 * @return		Square root
	 */
	template<typename T>
	T Sqrt(T t);
	/**
	 * Round a value
	 * @tparam T	Floating point type
	 * @param[in] t	Value to round
	 * @return		Rounded value
	 */
	template<typename T>
	std::conditional_t<sizeof(T) < 8, i32, i64> Round(T t);
	/**
	 * Ceil a value
	 * @tparam T	Floating point type
	 * @param[in] t	Value to round
	 * @return		Ceiled value
	 */
	template<typename T>
	std::conditional_t<sizeof(T) < 8, i32, i64> Ceil(T t);
	/**
	 * Floor a value
	 * @tparam T	Floating point type
	 * @param[in] t	Value to round
	 * @return		Floored value
	 */
	template<typename T>
	std::conditional_t<sizeof(T) < 8, i32, i64> Floor(T t);
	/**
	 * Calculate modulus of a value
	 * @tparam T	Arithmatic type
	 * @param[in] t	Dividend
	 * @param[in] n Divisor
	 * @return		Remainder
	 */
	template<typename T>
	T Mod(T t, T n);
	/**
	 * Calculate the positive modulus of a value
	 * @tparam T	Arithmatic type
	 * @param[in] t	Dividend
	 * @param[in] n Divisor
	 * @return		Remainder
	 */
	template<typename T>
	T PosMod(T t, T n);
	/**
	 * Calculate the negative modulus of a value
	 * @tparam T	Arithmatic type
	 * @param[in] t	Dividend
	 * @param[in] n Divisor
	 * @return		Remainder
	 */
	template<typename T>
	T NegPos(T t, T n);
	/***
	 * Raise a value to a power
	 * @tparam T	Arithmatic type
	 * @param[in] t	Value
	 * @param[in] n Power
	 * @return		Value raised to a power
	 */
	template<typename T>
	T Pow(T t, T n);

	/**
	 * Get the sine of a value
	 * @tparam T	Arithmatic type
	 * @param[in] t	Value
	 * @return		Sinus of the value
	 */
	template<typename T>
	T Sin(T t);
	/**
	 * Get the cosine of a value
	 * @tparam T	Arithmatic type
	 * @param[in] t	Value
	 * @return		Cosinus of the value
	 */
	template<typename T>
	T Cos(T t);
	/**
	 * Get the tangent of a value
	 * @tparam T	Arithmatic type
	 * @param[in] t	Value
	 * @return		Tangent of the value
	 */
	template<typename T>
	T Tan(T t);
	/**
	 * Get the arcsine of a value
	 * @tparam T Arithmatic type
	 * @param[in] t	Value
	 * @return		Arcsine of the value
	 */
	template<typename T>
	T ArcSin(T t);
	/**
	 * Get the arccosine of a value
	 * @tparam T	Arithmatic type
	 * @param[in] t	Value
	 * @return		Arccosine of the value
	 */
	template<typename T>
	T ArcCos(T t);
	/**
	 * Get the arctangent of a value
	 * @tparam T	Arithmatic type
	 * @param[in] t	Value
	 * @return		Arctangent of the value
	 */
	template<typename T>
	T ArcTan(T t);
	/**
	 * Get the arctangent of a value
	 * @tparam T	Arithmatic type
	 * @param[in] x	X value
	 * @param[in] y Y value
	 * @return		Arctangent of the value
	 */
	template<typename T>
	T ArcTan(T x, T y);

	/**
	 * Convert degrees to radians
	 * @tparam T	Arithmatic type
	 * @param[in] t Angle in degrees
	 * @return		Angle in radians
	 */
	template<typename T>
	T ToRadians(T t);
	/**
	 * Convert degrees to radians
	 * @tparam T	Arithmatic type
	 * @param[in] t Angle in radians
	 * @return		Angle in degrees
	 */
	template<typename T>
	T ToDegrees(T t);
	/**
	 * Wrap an angle in radians
	 * @tparam T	Arithmatic type
	 * @param[in] t Angle in radians
	 * @return		wrapped angle in radians
	 */
	template<typename T>
	T WrapRadians(T t);
	/**
	 * Wrap an angle in degrees
	 * @tparam T	Arithmatic type
	 * @param[in] t Angle in degrees
	 * @return		wrapped angle in degrees
	 */
	template<typename T>
	T WrapDegrees(T t);
	/**
	 * Compare 2 values
	 * @tparam V0	Arithmatic type
	 * @tparam V1	Arithmatic type
	 * @param[in] a	1st value
	 * @param[in] b 2nd value
	 * @return		True if values are equals, false otherwise
	 */
	template<typename V0, typename V1>
	b8 Equals(V0 a, V1 b);
	/**
	 * Compare 2 values
	 * @tparam V0	Arithmatic type
	 * @tparam V1	Arithmatic type
	 * @tparam E	Arithmatic type
	 * @param[in] a			1st value
	 * @param[in] b			2nd value
	 * @param[in] epsilon	Epsilon
	 * @return				True if values are equals, false otherwise
	 */
	template<typename V0, typename V1, typename E>
	b8 Equals(V0 a, V1 b, E epsilon);
	/**
	 * Check if a value is zero
	 * @tparam T	Arithmatic type
	 * @param[in] t	Value
	 * @return		True if value is zero, false otherwise
	 */
	template<typename T>
	b8 IsZero(T t);
	/**
	 * Check if a value is nearly zero
	 * @tparam T	Arithmatic type
	 * @param[in] t	Value
	 * @return		True if value is nearly zero, false otherwise
	 */
	template<typename T>
	b8 IsNearlyZero(T t);
	/**
	 * Check if a value is a power of 2
	 * @tparam T	Arithmatic type
	 * @param[in] t	Value
	 * @return		True if value is a power of 2, false otherwise
	 */
	template<typename T>
	b8 IsPowerOf2(T t);
	/**
	 * Check if a value is even
	 * @tparam T	Arithmatic type
	 * @param[in] t	Value
	 * @return		True if value is even, false otherwise
	 */
	template<typename T>
	b8 IsEven(T t);
	/**
	 * Check if a value is even
	 * @tparam T	Arithmatic type
	 * @param[in] t	Value
	 * @return		True if value is even, false otherwise
	 */
	template<typename T>
	b8 IsOdd(T t);

	/**
	 * Check if a value is infinity
	 * @tparam T	Arithmatic type
	 * @param[in] t	Value
	 * @return		True if value is infinity, false otherwise
	 */
	template<typename T>
	b8 IsInfinity(T t);
	/**
	 * Check if a value is NaN
	 * @tparam T	Arithmatic type
	 * @param[in] t	Value
	 * @return		True if value is NaN, false otherwise
	 */
	template<typename T>
	b8 IsNaN(T t);
	/**
	 * Check if a value is finite
	 * @tparam T	Arithmatic type
	 * @param[in] t	Value
	 * @return		True if value is finite, false otherwise
	 */
	template<typename T>
	b8 IsFinite(T t);

}

#include "Inline/MathFunc.inl"