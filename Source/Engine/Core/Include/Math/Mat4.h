// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Mat4.h: 4x4 matrix
#pragma once
#include "Core/CoreHeaders.h"
#include "Containers/ContainerUtils.h"

namespace Hv::Math {

	template<typename T>
	struct Vec4;

	template<typename T>
	struct Vec3;

	template<typename T>
	struct Vec2;

	template<typename T>
	struct Quat;

	template<typename T>
	struct Mat4
	{
#pragma warning(push)
#pragma warning(disable: 4201) // nonstandard extension used : nameless struct/union
		union
		{
			T data[16];
			T data2D[4][4];
			Vec4<T> rows[4];
			struct
			{
				Vec4<T> r0;
				Vec4<T> r1;
				Vec4<T> r2;
				Vec4<T> r3;
			};
			struct
			{
				T m00, m01, m02, m03;
				T m10, m11, m12, m13;
				T m20, m21, m22, m23;
				T m30, m31, m32, m33;
			};
		};
#pragma warning(pop)
		/**
		 * Create a default Mat4
		 */
		Mat4();
		/**
		 * Create a matrix with a value in its diagonal
		 * @tparam V		Arithametic type
		 * @param[in] diag	Diagonal value
		 */
		template<typename V>
		Mat4(V diag);
		/**
		 * @tparam V00		Arithametic type
		 * @tparam V01		Arithametic type
		 * @tparam V02		Arithametic type
		 * @tparam V03		Arithametic type
		 * @tparam V10		Arithametic type
		 * @tparam V11		Arithametic type
		 * @tparam V12		Arithametic type
		 * @tparam V13		Arithametic type
		 * @tparam V20		Arithametic type
		 * @tparam V21		Arithametic type
		 * @tparam V22		Arithametic type
		 * @tparam V23		Arithametic type
		 * @tparam V30		Arithametic type
		 * @tparam V31		Arithametic type
		 * @tparam V32		Arithametic type
		 * @tparam V33		Arithametic type
		 * @param[in] v00	Value
		 * @param[in] v01	Value
		 * @param[in] v02	Value
		 * @param[in] v03	Value
		 * @param[in] v10	Value
		 * @param[in] v11	Value
		 * @param[in] v12	Value
		 * @param[in] v13	Value
		 * @param[in] v20	Value
		 * @param[in] v21	Value
		 * @param[in] v22	Value
		 * @param[in] v23	Value
		 * @param[in] v30	Value
		 * @param[in] v31	Value
		 * @param[in] v32	Value
		 * @param[in] v33	Value
		 */
		template<typename V00, typename V01, typename V02, typename V03, typename V10, typename V11, typename V12, typename V13, typename V20, typename V21, typename V22, typename V23, typename V30, typename V31, typename V32, typename V33>
		Mat4(V00 v00, V01 v01, V02 v02, V03 v03, V10 v10, V11 v11, V12 v12, V13 v13, V20 v20, V21 v21, V22 v22, V23 v23, V30 v30, V31 v31, V32 v32, V33 v33);
		/**
		* @tparam R0		Arithametic type
		* @tparam R1		Arithametic type
		* @tparam R2		Arithametic type
		* @tparam R3		Arithametic type
		* @param[in] r0		Row 0
		* @param[in] r1		Row 1
		* @param[in] r2		Row 2
		* @param[in] r3		Row 3
		*/
		template<typename R0, typename R1, typename R2, typename R3>
		Mat4(const Vec4<R0>& r0, const Vec4<R1>& r1, const Vec4<R2>& r2, const Vec4<R3>& r3);
		/**
		 * Create a matrix from another matrix
		 * @tparam V	Arithmatic type
		 * @param[in] m	Matrix
		 */
		template<typename V>
		Mat4(const Mat4<V>& m);
		/**
		 * Create a matrix from aonther matrix
		 * @param[in] m	Matrix
		 */
		Mat4(const Mat4& m);
		/**
		* Move a matrix into this matrix
		* @param[in] m	Matrix
		*/
		Mat4(Mat4&& m) noexcept;

		Mat4 operator+() const;
		Mat4 operator-() const;

		template<typename V>
		Mat4 operator+(const Mat4<V>& m) const;
		template<typename V>
		Mat4 operator-(const Mat4<V>& m) const;
		template<typename V>
		Mat4 operator*(const Mat4<V>& m) const;
		template<typename V>
		Mat4 operator*(V v) const;
		template<typename V>
		Mat4 operator/(V v) const;

		template<typename V>
		Mat4& operator=(const Mat4<V>& m);
		Mat4& operator=(const Mat4& m);
		template<typename V>
		Mat4& operator=(Mat4&& m) noexcept;

		template<typename V>
		Mat4& operator+=(const Mat4<V>& m);
		template<typename V>
		Mat4& operator-=(const Mat4<V>& m);
		template<typename V>
		Mat4& operator*=(const Mat4<V>& m);
		template<typename V>
		Mat4& operator*=(V v);
		template<typename V>
		Mat4& operator/=(V v);

		template<typename V>
		b8 operator==(const Mat4<V>& m) const;
		template<typename V>
		b8 operator!=(const Mat4<V>& m) const;

		/**
		 * Get the determinant of the matrix
		 * @return	Determinant
		 */
		T Determinant() const;
		/**
		 * Get a transposed copy of the matrix
		 * @return	Transposed matrix
		 */
		Mat4 Transposed() const;
		/**
		 * transpose the matrix
		 * @return	Reference to the matrix
		 */
		Mat4& Transpose();
		/**
		 * Get the cofactor of the matrix
		 * @return	Cofactor
		 */
		Mat4 Cofactor() const;
		/**
		 * Convert the matrix to its cofactor
		 * @return	Reference to the matrix
		 */
		Mat4& ToCofactor();
		/**
		 * Get the adjugate of the matrix
		 * @return	Adjugate
		 */
		Mat4 Adjugate() const;
		/**
		 * Convert the matrix to its adjugate
		 * @return	Reference to the matrix
		 */
		Mat4& ToAdjugate();
		/**
		 * Get the inverse of the matrix
		 * @return	Inverse
		 */
		Mat4 Inverse() const;
		/**
		 * Convert the matrix to its inverse
		 * @return	Reference to the matrix
		 */
		Mat4& ToInverse();
		/**
		 * Get a row from the matrix
		 * @param[in] row	Row index
		 * @return			Row
		 */
		Vec4<T> Row(u8 row) const;
		/**
		 * Set a row from the matrix
		 * @tparam V			Arithmatic type
		 * @param[in] row	Row index
		 * @param[in] v		Vector
		 */
		template<typename V>
		void Row(u8 row, const Vec4<V>& v);
		/**
		 * Get a row from the matrix
		 * @param[in] column	Column index
		 * @return			Column
		 */
		Vec4<T> Column(u8 column) const;
		/**
		 * Set a row from the matrix
		 * @tparam V			Arithmatic type
		 * @param[in] column	Column index
		 * @param[in] v		Vector
		 */
		template<typename V>
		void Column(u8 column, const Vec4<V>& v);
		/**
		 * Get the diagonal from the matrix
		 * @return	Diagonal
		 */
		Vec4<T> Diagonal() const;
		/**
		 * Set the diagonal from the matrix
		 * @tparam V			Arithmatic type
		 * @param[in] m		Vector
		 */
		template<typename V>
		void Diagonal(const Vec4<V>& m);

		/**
		 * Check if 2 matrices are equal to eachother
		 * @tparam V	Arithmatic type
		 * @param[in] m	Matrix
		 * @return		True if matrices are equal to eachother, false otherwise
		 */
		template<typename V>
		b8 Equals(const Mat4<V>& m) const;
		/**
		 * Check if 2 matrices are equal to eachother
		 * @tparam V			Arithmatic type
		 * @tparam E			Arithmatic type
		 * @param[in] v			Matrix
		 * @param[in] epsilon	Epsilon
		 * @return				True if matrices are equal to eachother, false otherwise
		 */
		template<typename V, typename E>
		b8 Equals(const Mat4<V>& v, E epsilon) const;
		/**
		 * Check if the matrix is an identity matrix
		 * @return	True if the matrix is an identity matrix
		 */
		b8 IsIdentity() const;

		/**
		 * Create a rotation matrix from a quaternion
		 * @tparam V	Arithmatic type
		 * @param[in] q	Quaternion
		 * @return		Rotation matrix
		 */
		template<typename V>
		static Mat4 CreateRotation(const Quat<V>& q);
		/**
		 * Create a scale matrix from a vec3
		 * @tparam V	Arithmatic type
		 * @param[in] v	Scale
		 * @return		Scale matrix
		 */
		template<typename V>
		static Mat4 CreateScale(const Vec3<V>& v);
		/**
		 * Create a scale matrix from a vec3
		 * @tparam X	Arithmatic type
		 * @tparam Y	Arithmatic type
		 * @tparam Z	Arithmatic type
		 * @param[in] x	X scale
		 * @param[in] y	Y scale
		 * @param[in] z	Z scale
		 * @return		Scale matrix
		 */
		template<typename X, typename Y, typename Z>
		static Mat4 CreateScale(X x, Y y, Z z);
		/**
		 * Create a translation matrix from a vector
		 * @tparam V	Arithmatic type
		 * @param[in] v	translation
		 * @return		Rotation matrix
		 */
		template<typename V>
		static Mat4 CreateTranslation(const Vec3<V>& v);
		/**
		 * Create a translation matrix from a vector
		 * @tparam X	Arithmatic type
		 * @tparam Y	Arithmatic type
		 * @tparam Z	Arithmatic type
		 * @param[in] x	X scale
		 * @param[in] y	Y scale
		 * @param[in] z	Z scale
		 * @return		Rotation matrix
		 */
		template<typename X, typename Y, typename Z>
		static Mat4 CreateTranslation(X x, Y y, Z z);
		/**
		* Create a transform matrix
		* @tparam V0				Arithmatic type
		* @tparam V1				Arithmatic type
		* @tparam V2				Arithmatic type
		* @param[in] scale			Scale
		* @param[in] rotation		Rotation
		* @param[in] translation	Translation
		*/
		template<typename V0, typename V1, typename V2>
		static Mat4 CreateTransform(Vec3<V0>& scale, Quat<V1>& rotation, Vec3<V2>& translation);

		/**
		 * Create a 2d rotation matrix from a quaternion
		 * @tparam V		Arithmatic type
		 * @param[in] angle	Angle
		 * @return			Rotation matrix
		 */
		template<typename V>
		static Mat4 CreateRotation2D(V angle);
		/**
		 * Create a 2d scale matrix from a vec3
		 * @tparam V	Arithmatic type
		 * @param[in] v	Scale
		 * @return		Scale matrix
		 */
		template<typename V>
		static Mat4 CreateScale2D(const Vec2<V>& v);
		/**
		 * Create a 2d scale matrix from a vec3
		 * @tparam X	Arithmatic type
		 * @tparam Y	Arithmatic type
		 * @param[in] x	X scale
		 * @param[in] y	Y scale
		 * @return		Scale matrix
		 */
		template<typename X, typename Y>
		static Mat4 CreateScale2D(X x, Y y);
		/**
		 * Create a 2d translation matrix from a vector
		 * @tparam V	Arithmatic type
		 * @param[in] v	translation
		 * @return		Rotation matrix
		 */
		template<typename V>
		static Mat4 CreateTranslation2D(const Vec2<V>& v);
		/**
		 * Create a 2d translation matrix from a vector
		 * @tparam X	Arithmatic type
		 * @tparam Y	Arithmatic type
		 * @param[in] x	X scale
		 * @param[in] y	Y scale
		 * @return		Rotation matrix
		 */
		template<typename X, typename Y>
		static Mat4 CreateTranslation2D(X x, Y y);
		/**
		* Create a transform matrix
		* @tparam V0				Arithmatic type
		* @tparam V1				Arithmatic type
		* @tparam V2				Arithmatic type
		* @param[in] scale			Scale
		* @param[in] rotation		Rotation
		* @param[in] translation	Translation
		*/
		template<typename V0, typename V1, typename V2>
		static Mat4 CreateTransform2D(Vec2<V0>& scale, V1 rotation, Vec2<V2>& translation);

		/**
		 * Create an perspecitve matrix
		 * @tparam V0			Arithmatic type
		 * @tparam V1			Arithmatic type
		 * @tparam V2			Arithmatic type
		 * @param[in] eyePos	Eye position
		 * @param[in] target	Look target
		 * @param[in] up		Up direction
		 */
		template<typename V0, typename V1, typename V2>
		static Mat4 CreateLookat(const Vec3<V0>& eyePos, const Vec3<V1>& target, const Vec3<V2>& up);

		/**
		 * Create an orthographic matrix (offcenter)
		 * @tparam V0			Arithmatic type
		 * @tparam V1			Arithmatic type
		 * @tparam V2			Arithmatic type
		 * @tparam V3			Arithmatic type
		 * @tparam V4			Arithmatic type
		 * @tparam V5			Arithmatic type
		 * @param[in] left		Left
		 * @param[in] top		Top
		 * @param[in] right		Right
		 * @param[in] bottom	Bottom
		 * @param[in] znear		Z near
		 * @param[in] zfar		Z far
		 */
		template<typename V0, typename V1, typename V2, typename V3, typename V4, typename V5>
		static Mat4 CreateOrthograpic(V0 left, V1 top, V2 right, V3 bottom, V4 znear, V5 zfar);
		/**
		 * Create an orthographic matrix
		 * @tparam V0			Arithmatic type
		 * @tparam V1			Arithmatic type
		 * @tparam V2			Arithmatic type
		 * @tparam V3			Arithmatic type
		 * @param[in] width		Width
		 * @param[in] height	Height
		 * @param[in] znear		Z near
		 * @param[in] zfar		Z far
		 */
		template<typename V0, typename V1, typename V2, typename V3>
		static Mat4 CreateOrthograpic(V0 width, V1 height, V2 znear, V3 zfar);
		/**
		 * Create an perspecitve matrix
		 * @tparam V0			Arithmatic type
		 * @tparam V1			Arithmatic type
		 * @tparam V2			Arithmatic type
		 * @tparam V3			Arithmatic type
		 * @param[in] fov		Field of view
		 * @param[in] aspect	Aspect ration
		 * @param[in] znear		Z near
		 * @param[in] zfar		Z far
		 */
		template<typename V0, typename V1, typename V2, typename V3>
		static Mat4 CreatePerspective(V0 fov, V1 aspect, V2 znear, V3 zfar);

		/**
		 * Identity matrix
		 */
		const static Mat4 Identity;
		/**
		 * Vulkan projection correction matrix
		 */
		const static Mat4 VulkanProjectionCorrection;
		/**
		 * OpenGL projection correction matrix
		 */
		const static Mat4 OpenGLProjectionCorrection;
	};

	template<typename T, typename V>
	Mat4<T> operator*(V v, const Mat4<T>& m);
}

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename T), HV_TTYPE(Hv::Math::Mat4, T))