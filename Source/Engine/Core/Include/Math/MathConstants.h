// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// MathConstants.h: Math constants
#pragma once
#include "Core/CoreTypes.h"
#include <limits>

namespace Hv::Math {
	
	namespace Detail {

		constexpr f64 g_Pi			= 3.1415926535897932;
		constexpr f64 g_SqrtPi		= 1.7724538509055160;
		constexpr f64 g_LogPi		= 0.4971498726941338;
		constexpr f64 g_Log2Pi		= 0.7981798683581150;
		constexpr f64 g_LnPi		= 1.1447298858494001;
		constexpr f64 g_Ln2Pi		= 1.8378770664093454;
		constexpr f64 g_E			= 2.7182818284590452;
		constexpr f64 g_LogE		= 0.4342944819032518;
		constexpr f64 g_Epsilon		= 0.0001;
		constexpr f64 g_BigEpsilon	= 0.01;
	}

	template<typename T>
	constexpr T g_Pi = T(Detail::g_Pi);
	template<typename T>
	constexpr T g_2Pi			= T(2.0 * Detail::g_Pi);
	template<typename T>
	constexpr T g_1DivPi		= T(1.0 / Detail::g_Pi);
	template<typename T>
	constexpr T g_1Div2Pi		= T(.5 / Detail::g_Pi);
	template<typename T>
	constexpr T g_PiDiv2		= T(.5 * Detail::g_Pi);
	template<typename T>
	constexpr T g_PiDiv4		= T(.25 * Detail::g_Pi);
	template<typename T>
	constexpr T g_LogPi			= T(Detail::g_LogPi);
	template<typename T>
	constexpr T g_Log2Pi		= T(Detail::g_Log2Pi);
	template<typename T>
	constexpr T g_LnPi			= T(Detail::g_LnPi);
	template<typename T>
	constexpr T g_Ln2Pi			= T(Detail::g_Ln2Pi);
	template<typename T>
	constexpr T g_E				= T(Detail::g_E);
	template<typename T>
	constexpr T g_Epsilon		= T(Detail::g_Epsilon);
	template<typename T>
	constexpr T g_BigEpsilon	= T(Detail::g_BigEpsilon);
	template<typename T>
	constexpr T g_Deg2Rad		= T(Detail::g_Pi / 180.0);
	template<typename T>
	constexpr T g_Rad2Deg		= T(180.0 / Detail::g_Pi);
	template<typename T>
	constexpr T g_Infinity		= std::numeric_limits<T>::infinity();

	template<typename T>
	constexpr T g_Max			= (std::numeric_limits<T>::max)();
	template<typename T>
	constexpr T g_Min			= (std::numeric_limits<T>::min)();		/**< integrals: -g_Max, floating point: smallest pos value > 0 */
	template<typename T>
	constexpr T g_Lowest		= (std::numeric_limits<T>::lowest)();	/**< -g_Max */

}
