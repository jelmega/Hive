// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Math.h: Math main header
#pragma once

#include "MathConstants.h"

#include "MathFunc.h"

#include "Vec4.h"
#include "Inline/Vec4.inl"

#include "Vec3.h"
#include "Inline/Vec3.inl"

#include "Vec2.h"
#include "Inline/Vec2.inl"

#include "Mat4.h"
#include "Inline/Mat4.inl"

#include "Mat3.h"
#include "Inline/Mat3.inl"

#include "Mat2.h"
#include "Inline/Mat2.inl"

#include "Quat.h"
#include "Inline/Quat.inl"

#include "Rect.h"
#include "Inline/Rect.inl"

////////////////////////////////////////////////////////////////////////////////
//	Defines
////////////////////////////////////////////////////////////////////////////////
#define HM_XSWIZZLE 0
#define HM_YSWIZZLE 1
#define HM_ZSWIZZLE 2
#define HM_WSWIZZLE 3

namespace Hm = Hv::Math;

////////////////////////////////////////////////////////////////////////////////
//	type aliasses
////////////////////////////////////////////////////////////////////////////////

//	Vec4
using i8v4  = Hm::Vec4<i8>;
using i16v4 = Hm::Vec4<i16>;
using i32v4 = Hm::Vec4<i32>;
using i64v4 = Hm::Vec4<i64>;
using u8v4  = Hm::Vec4<u8>;
using u16v4 = Hm::Vec4<u16>;
using u32v4 = Hm::Vec4<u32>;
using u64v4 = Hm::Vec4<u64>;
using f32v4 = Hm::Vec4<f32>;
using f64v4 = Hm::Vec4<f64>;

//	Vec3
using i8v3  = Hm::Vec3<i8>;
using i16v3 = Hm::Vec3<i16>;
using i32v3 = Hm::Vec3<i32>;
using i64v3 = Hm::Vec3<i64>;
using u8v3  = Hm::Vec3<u8>;
using u16v3 = Hm::Vec3<u16>;
using u32v3 = Hm::Vec3<u32>;
using u64v3 = Hm::Vec3<u64>;
using f32v3 = Hm::Vec3<f32>;
using f64v3 = Hm::Vec3<f64>;

//	Vec2
using i8v2  = Hm::Vec2<i8>;
using i16v2 = Hm::Vec2<i16>;
using i32v2 = Hm::Vec2<i32>;
using i64v2 = Hm::Vec2<i64>;
using u8v2  = Hm::Vec2<u8>;
using u16v2 = Hm::Vec2<u16>;
using u32v2 = Hm::Vec2<u32>;
using u64v2 = Hm::Vec2<u64>;
using f32v2 = Hm::Vec2<f32>;
using f64v2 = Hm::Vec2<f64>;

//	Mat4
using i8m4  = Hm::Mat4<i8>;
using i16m4 = Hm::Mat4<i16>;
using i32m4 = Hm::Mat4<i32>;
using i64m4 = Hm::Mat4<i64>;
using u8m4  = Hm::Mat4<u8>;
using u16m4 = Hm::Mat4<u16>;
using u32m4 = Hm::Mat4<u32>;
using u64m4 = Hm::Mat4<u64>;
using f32m4 = Hm::Mat4<f32>;
using f64m4 = Hm::Mat4<f64>;

//	Mat3
using i8m3  = Hm::Mat3<i8>;
using i16m3 = Hm::Mat3<i16>;
using i32m3 = Hm::Mat3<i32>;
using i64m3 = Hm::Mat3<i64>;
using u8m3  = Hm::Mat3<u8>;
using u16m3 = Hm::Mat3<u16>;
using u32m3 = Hm::Mat3<u32>;
using u64m3 = Hm::Mat3<u64>;
using f32m3 = Hm::Mat3<f32>;
using f64m3 = Hm::Mat3<f64>;

//	Mat2
using i8m2  = Hm::Mat2<i8>;
using i16m2 = Hm::Mat2<i16>;
using i32m2 = Hm::Mat2<i32>;
using i64m2 = Hm::Mat2<i64>;
using u8m2  = Hm::Mat2<u8>;
using u16m2 = Hm::Mat2<u16>;
using u32m2 = Hm::Mat2<u32>;
using u64m2 = Hm::Mat2<u64>;
using f32m2 = Hm::Mat2<f32>;
using f64m2 = Hm::Mat2<f64>;

//	Quat
using f32q = Hm::Quat<f32>;
using f64q = Hm::Quat<f64>;

//	Rect
using i8rect  = Hm::Rect<i8>;
using i16rect = Hm::Rect<i16>;
using i32rect = Hm::Rect<i32>;
using i64rect = Hm::Rect<i64>;
using u8rect  = Hm::Rect<u8>;
using u16rect = Hm::Rect<u16>;
using u32rect = Hm::Rect<u32>;
using u64rect = Hm::Rect<u64>;
using f32rect = Hm::Rect<f32>;
using f64rect = Hm::Rect<f64>;

////////////////////////////////////////////////////////////////////////////////
//	Other
////////////////////////////////////////////////////////////////////////////////
// Could be removed if the vulkan shaders can be corrected automatically
namespace Hv::Math {

	const f32m4 VulkanCorrection = f32m4(1,  0, 0, 0, 
										 0, -1, 0, 0, 
										 0,  0, 1, 0, 
										 0,  0, 0, 1);

}