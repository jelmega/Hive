// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Adler.h: Adler Checksum
#pragma once
#include "Core/CoreHeaders.h"
#include "Containers/DynArray.h"
#include "Containers/Array.h"

namespace Hv::Checksum {
	
	/**
	* Calculate the adler32 checksum
	* @param[in] pData	Data
	* @param[in] size	Data size in bytes
	* @param[in] adler	Previous adler32 checksum
	* @return			Adler32 checksum
	*/
	HIVE_API u32 Adler32(u8* pData, u64 size, u32 adler = 0);

}