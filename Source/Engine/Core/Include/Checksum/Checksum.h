// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Checksum.h: Checksum main header
#pragma once

#include "CRC.h"
#include "Adler.h"
