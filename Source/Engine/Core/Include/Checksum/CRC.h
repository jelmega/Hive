// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// CRC.h: CRC (Cyclical Redundance Code) Checksum
#pragma once
#include "Core/CoreHeaders.h"
#include "Containers/DynArray.h"
#include "Containers/Array.h"

namespace Hv::Checksum {

	namespace Detail {
		
		Array<u32, 256>& GetCrc32Table();

	}

	/**
	* Calculate a 32-bit CRC checksum for some data
	* @param[in] pData		Raw data
	* @param[in] size		Size of data in bytes
	* @param[in] crc		Initial value
	* @return				Checksum
	*/
	HIVE_API u32 UpdateCRC32(const u8* pData, u64 size, u32 crc = 0xFFFF'FFFF);
	/**
	 * Calculate a 32-bit CRC checksum for some data
	 * @param[in] pData		Raw data
	 * @param[in] size		Size of data in bytes
	 * @return				Checksum
	 */
	HIVE_API u32 CRC32(const u8* pData, u64 size);
	/**
	 * Calculate a 32-bit CRC checksum for some data
	 * @param[in] data	Raw data
	 * @return			Checksum
	 */
	HIVE_API u32 CRC32(const DynArray<u8>& data);

}
