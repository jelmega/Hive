// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Logger.inl: Logger
#pragma once
#include "Logging/Logger.h"

namespace Hv {
	
	template <typename ... Args>
	void Logger::LogFormat(LogLevel level, const String& format, Args&&... args)
	{
		String str;
		str.FormatF(format, Forward<Args>(args)...);
		Log(m_DefaultCategory, level, str);
	}

	template <typename ... Args>
	void Logger::LogFormat(const LogCategory& cat, LogLevel level, const String& format, Args&&... args)
	{
		String str;
		str.FormatF(format, Forward<Args>(args)...);
		Log(cat, level, str);
	}

}
