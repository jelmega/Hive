// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Logger.h: Logger
#pragma once
#include "Core/CoreHeaders.h"
#include "String/String.h"
#include "FileSystem/FileSystem.h"

namespace Hv {
	
	enum class LogLevel : u8
	{
		Never	= 0,	/**< Never log */
		Fatal	= 0,	/**< Fatal errors */
		Error	= 1,	/**< Error */
		Warning	= 2,	/**< Warning */
		Verbose	= 3,	/**< Verbose */
		Info	= 4,	/**< Info */
		Detail	= 5,	/**< Detailed info */
		All		= 0xFF,	/**< All log levels */
	};

	struct HIVE_API LogCategory
	{
		const char* categoryName;	/**< Category name */
		LogLevel categoryLevel;		/**< Log levels to log for */
		u8 categoryNameLength;		/**< Cached length of the category name */

		LogCategory(const char* name, LogLevel minLogLevel);
	};

	class HIVE_API Logger
	{
	public:
		Logger();
		~Logger();

		/**
		 * Log a number of new lines
		 * @param[in] nr	Number of new lines to print
		 */
		void NewLine(u8 nr = 1);

		/**
		 * Log a string
		 * @param[in] level	Log level
		 * @param[in] str	String to log
		 * @param[in] cat	Log category
		 */
		void Log(const LogCategory& cat, LogLevel level, const String& str);
		/**
		 * Log verbose
		 * @param[in] str	String to log
		 */
		void LogVerbose(const String& str);
		/**
		 * Log verbose
		 * @param[in] str	String to log
		 * @param[in] cat	Log category
		 */
		void LogVerbose(const LogCategory& cat, const String& str);
		/**
		 * Log info
		 * @param[in] str	String to log
		 */
		void LogInfo(const String& str);
		/**
		 * Log info
		 * @param[in] str	String to log
		 * @param[in] cat	Log category
		 */
		void LogInfo(const LogCategory& cat, const String& str);
		/**
		 * Log detailed info
		 * @param[in] str	String to log
		 */
		void LogDetail(const String& str);
		/**
		 * Log detailed info
		 * @param[in] str	String to log
		 * @param[in] cat	Log category
		 */
		void LogDetail(const LogCategory& cat, const String& str);
		/**
		 * Log a warning
		 * @param[in] str	String to log
		 */
		void LogWarning(const String& str);
		/**
		 * Log a warning
		 * @param[in] str	String to log
		 * @param[in] cat	Log category
		 */
		void LogWarning(const LogCategory& cat, const String& str);
		/**
		 * Log an error
		 * @param[in] str	String to log
		 */
		void LogError(const String& str);
		/**
		 * Log an error
		 * @param[in] str	String to log
		 * @param[in] cat	Log category
		 */
		void LogError(const LogCategory& cat, const String& str);
		/**
		 * Log a fatal error
		 * @param[in] str	String to log
		 */
		void LogFatal(const String& str);
		/**
		 * Log a fatal error
		 * @param[in] str	String to log
		 * @param[in] cat	Log category
		 */
		void LogFatal(const LogCategory& cat, const String& str);
		/**
		 * Format and log a string
		 * @tparam Args		Argument types
		 * @param[in] level		Log level
		 * @param[in] format	Format
		 * @param[in] args		Arguments
		 */
		template<typename... Args>
		void LogFormat(LogLevel level, const String& format, Args&&... args);
		/**
		 * Format and log a string
		 * @tparam Args		Argument types
		 * @param[in] level		Log level
		 * @param[in] format	Format
		 * @param[in] args		Arguments
		 * @param[in] cat		Log category
		 */
		template<typename... Args>
		void LogFormat(const LogCategory& cat, LogLevel level, const String& format, Args&&... args);

		/**
		 * Set if the logger needs to log to the console
		 * @param[in] enable	If logging to the console is enabled
		 */
		void SetLogToConsole(b8 enable);
		/**
		 * Set if the logger needs to log to a file
		 * @param[in] enable	If logging to a file is enabled
		 */
		void SetLogToFile(b8 enable);

	private:
		/**
		 * Get the string/tag that represents the log level
		 * @param[in] level	Log level
		 * @return			Log level tag
		 */
		const String LevelToString(LogLevel level) const;
		/**
		 * Set the console color for the specific log level
		 * @param[in] level	Log level
		 */
		void SetLevelConsoleColor(LogLevel level);

		FileSystem::File m_File;								/**< File to log to */
		b8 m_LogToConsole;										/**< If the logger needs to log to the console */
		b8 m_LogToFile;											/**< If the logger needs to log to the file */
		LogCategory m_DefaultCategory;							/**< Default log category */

		static Threading::CriticalSection m_CriticalSection;	/**< Critical section */

		static constexpr u8 m_MaxCategoryNameLen = 10;			/**< Maximum length of a log category name, names longer than this, will be truncated */
	};

	HIVE_API Logger& GetLogger();
}

#include "Inline/Logger.inl"

#define g_Logger Hv::GetLogger()

/**
 * Define a log category
 * @param[in] name	Name of the log category
 */
#define HV_DECLARE_LOG_CATEGORY_EXTERN(name) HIVE_API Hv::LogCategory& Log##name();

 /**
  * Define a log category
  * @param[in] name	Name of the log category
  */
#define HV_DECLARE_LOG_CATEGORY(name, minLevel) HIVE_API Hv::LogCategory& Log##name()\
	{\
		static Hv::LogCategory category(#name, minLevel);\
		return category;\
	}

/**
* Log a warning when a condition failed
* @param[in] cond	Condition
* @param[in] text	Text to log
*/
#define HV_LOG_WARNING_ON_FAIL(cond, text)\
	if (!cond)\
	{\
		g_Logger.LogWarning(text);\
	}

/**
* Log an error when a condition failed
* @param[in] cond	Condition
* @param[in] text	Text to log
*/
#define HV_LOG_ERROR_ON_FAIL(cond, text)\
	if (!cond)\
	{\
		g_Logger.LogError(text);\
	}

/**
* Log info when a condition is true
* @param[in] cond	Condition
* @param[in] text	Text to log
*/
#define HV_LOG_INFO_ON_COND(cond, text)\
	if (cond)\
	{\
		g_Logger.LogInfo(text);\
	}