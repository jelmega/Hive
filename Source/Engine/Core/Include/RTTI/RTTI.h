// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// RTTI.h: RTTI data and defines
#pragma once
#include "Core/CoreHeaders.h"

namespace Hv {

	enum class RTTIStructFlags
	{
		None		= 0x0000'0000,	/**< No struct flags */
		Component	= 0x0000'0001,	/**< ECS component */
		Event		= 0x0000'0002,	/**< ECS event */
	};
	HV_ENABLE_ENUM_FLAG_OPERATORS(RTTIStructFlags);

	// Combine taken from boost
	constexpr u64 RTTIHashCombine(u64 hash0, u64 hash1)
	{
		constexpr u64 m = 0xc6a4a7935bd1e995ull;
		constexpr i32 r = 47;

		hash1 *= m;
		hash1 ^= hash1 >> r;
		hash1 *= m;

		hash0 ^= hash1;
		hash0 *= m;

		// Completely arbitrary number, to prevent 0's
		// from hashing to 0.
		return hash0 + 0xe6546b64;
	}
	
	template<typename T>
	class RTTI
	{
	public:
		static constexpr const AnsiChar* Name = "";
		static constexpr const AnsiChar* FullName = "";
		static constexpr const AnsiChar* Namespace = "";
		static constexpr u64 Hash = 0;
		static constexpr b8 IsClass = true;
		static constexpr RTTIStructFlags StructFlags = RTTIStructFlags::None;
	};

}

// Generate RTTI info for this class
#define HV_CLASS(...)

// Generate RTTI info for this struct
#define HV_STRUCT(...)

// Generate RTTI info for this property
#define HV_PROPERTY(...)

// Required in file to be able to generated RTTI info (use as '#include HV_INCLUDE_RTTI_MODULE(name)')
#define HV_INCLUDE_RTTI_MODULE(moduleName)	HV_STRINGIFY(../Generated/##moduleName##.module.generated.h)