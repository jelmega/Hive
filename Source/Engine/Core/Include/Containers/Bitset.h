// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DynArray.h: Dynamic array
#pragma once
#include "DynArray.h"

namespace Hv {
	
	class Bitset
	{
	private:
		using BaseType = DynArray<u8, Container::GrowthFixedSize<2>>;
	public:
		Bitset();
		~Bitset();

		b8 operator[](sizeT idx);

		/**
		 * Resize the bitset
		 * @param[in] size	Size of the bitset
		 */
		void Resize(sizeT size);

		/**
		 * Set a bit in the bitset
		 * @param[in] idx	Bit to set
		 */
		void Set(sizeT idx);
		/**
		* Set a bit in the bitset
		* @param[in] idx	Bit to set
		* @param[in] val	Bit to set
		*/
		void Set(sizeT idx, b8 val);
		/**
		* Unset a bit in the bitset
		* @param[in] idx	Bit to set
		*/
		void Unset(sizeT idx);

		/**
		* Get a bit in the bitset
		* @param[in] idx	Bit to get
		* @return			Value of bit or false if index is out of range
		*/
		b8 Get(sizeT idx);

		/**
		 * Check if no bits are set
		 * @return	True if no bits are set, false otherwise
		 */
		b8 None() const;
		/**
		* Check if any bits are set
		* @return	True if any bits are set, false otherwise
		*/
		b8 Any() const;
		/**
		* Check if all bits are set
		* @return	True if all bits are set, false otherwise
		*/
		b8 All() const;
		/**
		 * Clear the bitset
		 */
		void Clear();
		/**
		 * Check if a bitset fits into this bitset (if all set bits in the other are set in this bitset)
		 * @param[in] bitset	Other bitset
		 * @return				True if the bitset fits, false otherwise
		 */
		b8 Fits(const Bitset& bitset);

	private:
		BaseType m_Container;
		sizeT m_Size;
	};

}

#include "Inline/BitSet.inl"
