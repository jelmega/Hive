// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Array.h: Fixed size array
#pragma once
#include "Core/CoreHeaders.h"
#include "ContainerUtils.h"

namespace Hv {

	/**
	 * Fixed size array
	 * @note Alignment is not guaranteed
	 * @tparam T	Element type
	 * @tparam ArrSize	Array size
	 */
	template<typename T, sizeT ArrSize>
	class Array
	{
	public:
		/**
		 * Create an array
		 */
		Array();
		/**
		 * Create an array initialized to a value
		 * @param[in] value	Value
		 */
		Array(const T& value);
		/**
		 * Create an array from a array
		 * @tparam S		Array size
		 * @param[in] arr	Array
		 */
		template<sizeT S>
		Array(T(&arr)[S]);
		/**
		 * Create an array from a array
		 * @param[in] arr	Array
		 * @param[in] size	Array size
		 */
		Array(const T* arr, sizeT size);
		/**
		 * Create an Array from iterators
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 */
		Array(const T* itFirst, const T* itLast);
		/**
		 * Create an DynArray from iterators
		 * @tparam Iterator		Forward iterator
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 */
		template<typename Iterator>
		Array(const Iterator& itFirst, const Iterator& itLast);
		/**
		 * Create an array from an initializer list
		 * @param[in] il	Initializer list
		 */
		Array(std::initializer_list<T> il);
		/**
		 * Create an array from another array
		 * @tparam S			Size of other array
		 * @param[in] arr	Other array
		 */
		template<sizeT S>
		Array(const Array<T, S>& arr);
		/**
		 * Create an array from another array
		 * @param[in] arr	Other array
		 */
		Array(const Array& arr);
		/**
		 * Move an array into the array
		 * @param[in] arr	Array to move
		 */
		Array(Array&& arr) noexcept;

		template<sizeT N>
		Array& operator=(T(&arr)[N]);
		Array& operator=(std::initializer_list<T> il);
		template<sizeT S>
		Array& operator=(const Array<T, S>& arr);
		Array& operator=(const Array& arr);
		Array& operator=(Array&& arr) noexcept;

		T& operator[](sizeT index);
		const T& operator[](sizeT index) const;

		/**
		 * Fill the array with a value
		 * @param[in] value	Value to fill the array with
		 */
		void Fill(const T& value);
		/**
		 * Assign a range of values to an array
		 * @param[in] arr	Array
		 * @param[in] size	Array size
		 */
		void Assign(const T* arr, sizeT size);
		/**
		 * Assign a range of values to an array
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 */
		void Assign(const T* itFirst, const T* itLast);
		/**
		 * Assign a range of values to an array
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 */
		template<typename InputIterator>
		void Assign(const InputIterator& itFirst, const InputIterator& itLast);
		/**
		 * Swap the content of 2 arrays
		 * @param[in] arr	Array to swap content with
		 */
		void Swap(Array& arr);

		/**
		 * Get the size of the array
		 * @return	Size of the array
		 */
		static constexpr sizeT Size();

		/**
		 * Get a pointer to the data
		 * @return	Pointer to the data
		 */
		T* Data();
		/**
		 * Get a pointer to the data
		 * @return	Pointer to the data
		 */
		const T* Data() const;

		/**
		 * Return an iterator to the front/begin of the array
		 * @return	Iterator to the front/begin of the array
		 */
		T* Front();
		/**
		 * Return an iterator to the front/begin of the array
		 * @return	Iterator to the front/begin of the array
		 */
		const T* Front() const;
		/**
		 * Return an iterator to the last element of the array
		 * @return	Iterator to the last element of the array
		 */
		T* Last();
		/**
		 * Return an iterator to the last element of the array
		 * @return	Iterator to the last element of the array
		 */
		const T* Last() const;
		/**
		 * Return an iterator to the back/end of the array
		 * @return	Iterator to the back/end of the array
		 */
		T* Back();
		/**
		 * Return an iterator to the back/end of the array
		 * @return	Iterator to the back/end of the array
		 */
		const T* Back() const;

		/**
		 * Return an iterator to the begin of the array
		 * @note	Function meant to be used for range-based for-loops, use Front instead
		 * @return	Iterator to the begin of the array
		 */
		T* begin();
		/**
		 * Return an iterator to the begin of the array
		 * @note	Function meant to be used for range-based for-loops, use Front instead
		 * @return	Iterator to the begin of the array
		 */
		const T* begin() const;
		/**
		 * Return an iterator to the end of the array
		 * @note	Function meant to be used for range-based for-loops, use Back instead
		 * @return	Iterator to the end of the array
		 */
		T* end();
		/**
		 * Return an iterator to the end of the array
		 * @note	Function meant to be used for range-based for-loops, use Back instead
		 * @return	Iterator to the end of the array
		 */
		const T* end() const;

	private:
		template<typename, sizeT>
		friend class Array;

		T m_Data[ArrSize]; /**< Internal array */
	};
}

#include "Inline/Array.inl"

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename T, sizeT S), HV_TTYPE(Hv::Array, T, S))