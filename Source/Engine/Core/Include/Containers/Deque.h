// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Deque.h: Double ended queue
#pragma once
#include "Core/CoreHeaders.h"
#include "ContainerUtils.h"
#include "Memory/Memory.h"

namespace Hv {

	/**
	 * Double Ended Queue
	 * @note	The containers uses multiple ways to copy data internally, using the copy-assignment operator as default,
	*			but MemCpy and the copy-constructor can also be used. These values can be set using their relative declares:
	*			HV_DECLARE_CONTAINER_MEMCPY and HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR and their respective template variants.
	 * @note	The growthPolicy should always return a capacity, which is a multiple of SubArraySize
	 * @tparam T			Element type
	 * @tparam SubArraySize	Size of the internal sub arrays
	 * @tparam GrowthPolicy	Growth policy
	 */
	template<typename T, sizeT SubArraySize = 8, Container::GrowthPolicyFunc GrowthPolicy = Container::GrowthDoubleValue<SubArraySize>>
	class Deque
	{
		HV_STATIC_ASSERT_MSG(SubArraySize > 0, "Deque: SubArraySize needs to be larger than 0");

	public:
		class Iterator
		{
		public:
			/**
			 * Create an empty iterator
			 */
			Iterator();
			/**
			 * Create an iterator from another iterator
			 * @param[in] it	Other iterator
			 */
			Iterator(const Iterator& it);
			/**
			 * Move an iterator into this iterator
			 * @param[in] it	Other iterator
			 */
			Iterator(Iterator&& it) noexcept;

			T& operator*();
			const T& operator*() const;
			T * operator->();
			const T * operator->() const;

			Iterator& operator++();
			Iterator operator++(int);
			Iterator& operator--();
			Iterator operator--(int);
			Iterator operator+(sizeT count);
			Iterator operator-(sizeT count);

			Iterator& operator=(const Iterator& it);
			Iterator& operator=(Iterator&& it) noexcept;
			Iterator& operator+=(sizeT count);
			Iterator& operator-=(sizeT count);

			b8 operator==(const Iterator& it) const;
			b8 operator!=(const Iterator& it) const;

		private:
			friend class Deque<T, SubArraySize, GrowthPolicy>;
			/**
			 * Create an iterator from data and indices
			 * @param[in] pData		Pointer to data
			 * @param[in] index		Index in array
			 * @param[in] subIndex	Index in subarray
			 */
			Iterator(T* * pData, sizeT index, sizeT subIndex);

			T* * m_Data;
			sizeT m_Index;
			sizeT m_SubIndex;
		};
	public:
		/**
		 * Create an empty Deque
		 */
		Deque();
		/**
		 * Create an empty Deque
		 * @param[in] pAlloc	Allocator
		 */
		Deque(Memory::IAllocator * pAlloc);
		/**
		 * Create an Deque with a capacity
		 * @param[in] capacity	Capacity
		 * @param[in] pAlloc	Allocator
		 */
		Deque(sizeT capacity, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create an Deque of a certain size, filled with a value
		 * @param[in] count		Amount of values
		 * @param[in] value		Value
		 * @param[in] pAlloc	Allocator
		 */
		Deque(sizeT count, const T& value, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create an Deque from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] pAlloc	Allocator
		 */
		template<sizeT N>
		Deque(T(&arr)[N], Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a Deque from an arrya
		 * @param[in] arr		Array
		 * @param[in] count		Array size
		 * @param[in] pAlloc	Allocator
		 */
		Deque(const T * arr, sizeT count, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a Deque from iterators
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @param[in] pAlloc	Allocator
		 */
		Deque(const T * itFirst, const T * itLast, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a Deque from iterators
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 * @param[in] pAlloc		Allocator
		 */
		template<typename InputIterator>
		Deque(const InputIterator& itFirst, const InputIterator& itLast, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a Deque from an initializer list
		 * @tparam Iterator	Forward iterator
		 * @param[in] il		Initializer list
		 * @param[in] pAlloc	Allocator
		 */
		Deque(std::initializer_list<T> il, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a Deque from another Deque
		 * @tparam OtherPolicy			Growth policy of the other Deque
		 * @tparam OtherSubArraySize	Sub array size of the other Deque
		 * @param[in] deque				Other Deque
		 */
		template<Container::GrowthPolicyFunc OtherPolicy, sizeT OtherSubArraySize>
		Deque(const Deque<T, OtherPolicy, OtherSubArraySize>& deque);
		/**
		 * Create a Deque from another Deque
		 * @param[in] deque	Other Deque
		 */
		Deque(const Deque& deque);
		/**
		 * Create a Deque from another Deque
		 * @tparam OtherPolicy			Growth policy of the other Deque
		 * @tparam OtherSubArraySize	Sub array size of the other Deque
		 * @param[in] deque				Other Deque
		 * @param[in] pAlloc			Allocator
		 */
		template<Container::GrowthPolicyFunc OtherPolicy, sizeT OtherSubArraySize>
		Deque(const Deque<T, OtherPolicy, OtherSubArraySize>& deque, Memory::IAllocator * pAlloc);
		/**
		 * Create a Deque from another Deque
		 * @param[in] deque		Other Deque
		 * @param[in] pAlloc	Allocator
		 */
		Deque(const Deque& deque, Memory::IAllocator * pAlloc);
		/**
		 * Create a Deque from another Deque
		 * @tparam OtherPolicy	Growth policy of the other Deque
		 * @param[in] deque		Other Deque
		 */
		template<Container::GrowthPolicyFunc OtherPolicy>
		Deque(Deque<T, SubArraySize, OtherPolicy>&& deque);
		/**
		 * Move a Deque into this Deque
		 * @param[in] deque	Other Deque
		 */
		Deque(Deque&& deque) noexcept;
		~Deque();

		template<sizeT N>
		Deque& operator=(T(&arr)[N]);
		Deque& operator=(std::initializer_list<T> il);
		template<Container::GrowthPolicyFunc OtherPolicy, sizeT OtherSubArraySize>
		Deque& operator=(const Deque<T, OtherSubArraySize, OtherPolicy>& deque);
		Deque& operator=(const Deque& deque);
		template<Container::GrowthPolicyFunc OtherPolicy>
		Deque& operator=(Deque<T, SubArraySize, OtherPolicy>&& deque);
		Deque& operator=(Deque&& deque) noexcept;

		T& operator[](sizeT index);
		const T& operator[](sizeT index) const;

		/**
		 * Peek the front value
		 * @return Front value
		 */
		T& PeekFront();
		/**
		 * Peek the front value
		 * @return Front value
		 */
		const T& PeekFront() const;
		/**
		* Peek the front value
		* @return Front value
		*/
		T& PeekBack();
		/**
		* Peek the front value
		* @return Front value
		*/
		const T& PeekBack() const;

		/**
		 * Fill the Deque with a value for 'count' indices
		 * @param[in] val	Value
		 * @param[in] count	Amount of values
		 */
		void Fill(const T& val, sizeT count);
		/**
		 * Assign an array to the Deque
		 * @tparam N		Array size
		 * @param[in] arr	Array
		 */
		template<sizeT N>
		void Assign(T(&arr)[N]);
		/**
		 * Assign an array to the Deque
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 */
		void Assign(T * arr, sizeT count);
		/**
		 * Assign a range of values to the Deque
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		void Assign(T * itFirst, T * itLast);
		/**
		 * Assign a range of values to the Deque
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to first element
		 * @param[in] itLast		Iterator to the element after the last element
		 */
		template<typename InputIterator>
		void Assign(const InputIterator& itFirst, const InputIterator& itLast);
		/**
		 * Asign an initializer list to the Deque
		 * @param[in] il	Initializer list
		 */
		void Assign(std::initializer_list<T> il);

		/**
		 * Resize the Deque
		 * @param[in] size	New size
		 */
		void Resize(sizeT size);
		/**
		 * Resize the Deque
		 * @param[in] size	New size
		 * @param[in] val	Value to add if size is bigger than the current size
		 */
		void Resize(sizeT size, const T& val);
		/**
		 * Reserve space in the Deque
		 * @param[in] size	Min size to reserve
		 */
		void Reserve(sizeT size);
		/**
		 * Reduce the capacity to fit its size
		 */
		void ShrinkToFit();
		/**
		 * Clear the vectors contents
		 * @param[in] resize	If the vector needs to be resized during clearing (data deallocated)
		 */
		void Clear(bool resize = false);

		/**
		 * Add a value to the back of the Deque
		 * @param[in] val	Value to add
		 */
		void PushBack(const T& val);
		/**
		 * Move a value to the back of the Deque
		 * @param[in] val	Value to add
		 */
		void PushBack(T&& val);
		/**
		 * Remove the last value from the Deque
		 * @return	Last value
		 */
		void PopBack();
		/**
		 * Add a value to the first of the Deque
		 * @param[in] val	Value to add
		 */
		void PushFront(const T& val);
		/**
		 * Move a value to the first of the Deque
		 * @param[in] val	Value to add
		 */
		void PushFront(T&& val);
		/**
		 * Remove the first value from the Deque
		 * @return	First value
		 */
		void PopFront();

		/**
		 * Insert a value into the Deque
		 * @param[in] index	Index of location in Deque
		 * @param[in] val	Value to insert
		 */
		void Insert(sizeT index, const T& val);
		/**
		 * Insert a value into the Deque
		 * @param[in] it	Iterator to location in Deque
		 * @param[in] val	Value to insert
		 */
		void Insert(const Iterator& it, const T& val);
		/**
		 * Insert a value into the Deque by moving it
		 * @param[in] index	Index of location in Deque
		 * @param[in] val	Value to insert
		 */
		void Insert(sizeT index, T&& val);
		/**
		 * Insert a value into the Deque by moving it
		 * @param[in] it	Iterator to location in Deque
		 * @param[in] val	Value to insert
		 */
		void Insert(const Iterator& it, T&& val);
		/**
		 * Insert an amount of values into the Deque
		 * @param[in] index	Index of location in Deque
		 * @param[in] count	Amount of values to insert
		 * @param[in] val	Value to insert
		 */
		void Insert(sizeT index, sizeT count, const T& val);
		/**
		 * Insert an amount of values into the Deque
		 * @param[in] it	Iterator to location in Deque
		 * @param[in] count	Amount of values to insert
		 * @param[in] val	Value to insert
		 */
		void Insert(const Iterator& it, sizeT count, const T& val);
		/**
		 * Insert an array into the Deque
		 * @tparam N			Array size
		 * @param[in] index	Index of location in Deque
		 * @param[in] arr	Array
		 */
		template<sizeT N>
		void Insert(sizeT index, T(&arr)[N]);
		/**
		 * Insert an array into the Deque
		 * @tparam N			Array size
		 * @param[in] it	Iterator to location in Deque
		 * @param[in] arr	Array
		 */
		template<sizeT N>
		void Insert(const Iterator& it, T(&arr)[N]);
		/**
		 * Insert an array into the Deque
		 * @param[in] index	Index of location in Deque
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 */
		void Insert(sizeT index, T * arr, sizeT count);
		/**
		 * Insert an array into the Deque
		 * @param[in] it	Iterator to location in Deque
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 */
		void Insert(const Iterator& it, T * arr, sizeT count);
		/**
		 * Insert a range of values into the Deque
		 * @param[in] index		Index of location in Deque
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		void Insert(sizeT index, const T * itFirst, const T * itLast);
		/**
		 * Insert a range of values into the Deque
		 * @param[in] it		Iterator to location in Deque
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		void Insert(const Iterator& it, const T * itFirst, const T * itLast);
		/**
		 * Insert a range of values into the Deque
		 * @tparam InputIterator	Forward Iterator
		 * @param[in] index			Index of location in Deque
		 * @param[in] itFirst		Iterator to first element
		 * @param[in] itLast		Iterator to the element after the last element
		 */
		template<typename InputIterator>
		void Insert(sizeT index, const InputIterator& itFirst, const InputIterator& itLast);
		/**
		 * Insert a range of values into the Deque
		 * @tparam InputIterator	Forward Iterator
		 * @param[in] it			Iterator to location in Deque
		 * @param[in] itFirst		Iterator to first element
		 * @param[in] itLast		Iterator to the element after the last element
		 */
		template<typename InputIterator>
		void Insert(const Iterator& it, const InputIterator& itFirst, const InputIterator& itLast);
		/**
		 * Insert a range of values into the Deque
		 * @param[in] index	Index of location in Deque
		 * @param[in] il	Initializer list
		 */
		void Insert(sizeT index, std::initializer_list<T> il);
		/**
		 * Insert a range of values into the Deque
		 * @param[in] it	Iterator to location in Deque
		 * @param[in] il	Initializer list
		 */
		void Insert(const Iterator& it, std::initializer_list<T> il);

		/**
		 * Erase a value
		 * @param[in] index	Index of location in Deque
		 */
		void Erase(sizeT index);
		/**
		 * Erase a value
		 * @param[in] it	Iterator to location in Deque
		 */
		void Erase(const Iterator& it);
		/**
		 * Erase an amount of value
		 * @param[in] index	Index of location in Deque
		 * @param[in] count	Amount of values to erase
		 */
		void Erase(sizeT index, sizeT count);
		/**
		 * Erase an amount of value
		 * @param[in] it	Iterator to location in Deque
		 * @param[in] count	Amount of values to erase
		 */
		void Erase(const Iterator& it, sizeT count);
		/**
		 * Erase a range of values
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	Iterator to element after last element
		 */
		void Erase(const Iterator& itFirst, const Iterator& itLast);

		/**
		 * Create and insert a value
		 * @tparam Args		Constructor argument types
		 * @param[in] index	Index of location in Deque
		 * @param[in] args	Constructor arguments
		 */
		template<typename... Args>
		void Emplace(sizeT index, Args&&... args);
		/**
		 * Create and insert a value
		 * @tparam Args		Constructor argument types
		 * @param[in] it	Iterator to location in Deque
		 * @param[in] args	Constructor arguments
		 */
		template<typename... Args>
		void Emplace(const Iterator& it, Args&&... args);
		/**
		 * Create and add a value to the back of the Deque
		 * @tparam Args		Constructor argument types
		 * @param[in] args	Constructor arguments
		 */
		template<typename... Args>
		void EmplaceBack(Args&&... args);
		/**
		 * Create and add a value to the back of the Deque
		 * @tparam Args		Constructor argument types
		 * @param[in] args	Constructor arguments
		 */
		template<typename... Args>
		void EmplaceFront(Args&&... args);

		/**
		 * Swap the content of 2 Deques
		 * @param[in] deque	Deque to swap content with
		 */
		void Swap(Deque& deque);

		/**
		 * Check if the Deque is empty
		 * @return	If the Deque is empty
		 */
		b8 IsEmpty() const;
		/**
		 * Get the size of the Deque
		 * @return	Size of the Deque
		 */
		sizeT Size() const;
		/**
		 * Get the size of the Deque
		 * @return	Capacity of the Deque
		 */
		sizeT Capacity() const;
		/**
		 * Get a pointer to the data
		 * @return	Pointer to the data
		 */
		T* * Data();
		/**
		 * Get a pointer to the allocator
		 * @return	Pointer to the allocator
		 */
		Memory::IAllocator * GetAllocator();

		/**
		 * Return an iterator to the front/begin of the array
		 * @return	Iterator to the front/begin of the array
		 */
		Iterator Front() const;
		/**
		 * Return an iterator to the last element of the array
		 * @return	Iterator to the last element of the array
		 */
		Iterator Last() const;
		/**
		 * Return an iterator to the back/end of the array
		 * @return	Iterator to the back/end of the array
		 */
		Iterator Back() const;

		/**
		 * Return an iterator to the begin of the array
		 * @note	Function meant to be used for range-based for-loops, use Front instead
		 * @return	Iterator to the begin of the array
		 */
		Iterator begin() const;
		/**
		 * Return an iterator to the end of the array
		 * @note	Function meant to be used for range-based for-loops, use Back instead
		 * @return	Iterator to the end of the array
		 */
		Iterator end() const;

	private:

		/**
		 * Copy a deque to this deque
		 * @tparam OtherPolicy			Growth policy of the other Deque
		 * @tparam OtherSubArraySize	Sub array size of the other Deque
		 * @param[in] deque				Deque to copy
		 */
		template<Container::GrowthPolicyFunc OtherPolicy, sizeT OtherSubArraySize>
		void Copy(const Deque& deque);
		/**
		 * Copy a deque to this deque
		 * @param[in] deque	Deque to copy
		 */
		void Copy(const Deque& deque);

		/**
		 * Shift data right inside of the Deque
		 * @param[in] from			Index to move data from
		 * @param[in] count			Amount of positions to shift data
		 * @param[in] rightSideData	True if data on the right needs to be shifted, falseo if data on the left needs to be shifted
		 */
		void ShiftLeft(sizeT from, sizeT count, b8 rightSideData);
		/**
		 * Shift data right inside of the Deque
		 * @param[in] from			Index to move data from
		 * @param[in] count			Amount of positions to shift data
		 * @param[in] rightSideData	True if data on the right needs to be shifted, falseo if data on the left needs to be shifted
		 */
		void ShiftRight(sizeT from, sizeT count, b8 rightSideData);

		T** m_Data;															/**< Data  */
		sizeT m_Front;														/**< Front index  */
		sizeT m_UsedSubArrs;												/**< Amount of used sub arrays */
		sizeT m_Size;														/**< Size  */
		sizeT m_Capacity;													/**< Capacity  */
		Memory::IAllocator * m_pAlloc;										/**< Pointer to allocator  */
		HV_NATVIS_CODE(static const sizeT m_SubArraySize = SubArraySize);	/**< natvis data for Visual Studio */
	};

}

#include "Inline/Deque.inl"

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename T, sizeT S, Hv::Container::GrowthPolicyFunc G), HV_TTYPE(Hv::Deque, T, S, G))
