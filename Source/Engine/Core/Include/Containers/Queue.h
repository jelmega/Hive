// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Queue.h: Queue
#pragma once
#include "Deque.h"

namespace Hv {

	/**
	 * Queue
	 * @note	The containers uses multiple ways to copy data internally, using the copy-assignment operator as default,
	 *			but MemCpy and the copy-constructor can also be used. These values can be set using their relative declares:
	 *			HV_DECLARE_CONTAINER_MEMCPY and HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR and their respective template variants.
	 * @note	The growthPolicy should always return a capacity, which is a multiple of SubArraySize
	 * @note A queue is essentially a wrapper around a deque, designed to fit a FIFO style workflow
	 * @tparam T			Element type
	 * @tparam SubArraySize	Size of the internal sub arrays
	 * @tparam GrowthPolicy	Growth policy
	 */
	template<typename T, sizeT SubArraySize = 8, Container::GrowthPolicyFunc GrowthPolicy = Container::GrowthDoubleValue<SubArraySize>>
	class Queue
	{
	private:
		HV_STATIC_ASSERT_MSG(SubArraySize > 0, "Queue: SubArraySize needs to be larger than 0");
		
		using BaseType = Deque<T, SubArraySize, GrowthPolicy>;
		using Iterator = typename BaseType::Iterator;

	public:
		/**
		 * Create an empty Queue
		 */
		Queue();
		/**
		 * Create an empty Queue
		 * @param[in] pAlloc	Allocator
		 */
		Queue(Memory::IAllocator* pAlloc);
		/**
		 * Create a queue from another queue
		 * @param[in] queue	Other Queue
		 */
		Queue(const Queue& queue);
		/**
		 * Create a Stack from another Stack
		 * @param[in] queue		Other Queue
		 * @param[in] pAlloc	Allocator
		 */
		Queue(const Queue& queue, Memory::IAllocator* pAlloc);
		/**
		 * Move a queue into this queue
		 * @param[in] queue	Other Queue
		 */
		Queue(Queue&& queue) noexcept;
		~Queue();

		Queue& operator=(const Queue& queue);
		Queue& operator=(Queue&& queue) noexcept;

		/**
		* Peek the top value
		* @return Top value
		*/
		T& Peek();
		/**
		* Peek the top value
		* @return Top value
		*/
		const T& Peek() const;

		/**
		 * Reserve space in the Queue
		 * @param[in] size	Min size to reserve
		 */
		void Reserve(sizeT size);
		/**
		 * Clear the vectors contents
		 * @param[in] resize	If the vector needs to be resized during clearing (data deallocated)
		 */
		void Clear(bool resize = false);

		/**
		 * Add a value to the back of the Queue
		 * @param[in] val	Value to add
		 */
		void Push(const T& val);
		/**
		 * Move a value to the back of the Queue
		 * @param[in] val	Value to add
		 */
		void Push(T&& val);
		/**
		 * Remove the last value from the Queue
		 */
		void Pop();

		/**
		 * Create and add a value to the back of the Queue
		 * @tparam Args		Constructor argument types
		 * @param[in] args	Constructor arguments
		 */
		template<typename... Args>
		void Emplace(Args&&... args);

		/**
		 * Swap the content of 2 Deques
		 * @param[in] queue	Queue to swap content with
		 */
		void Swap(Queue& queue);

		/**
		 * Check if the Queue is empty
		 * @return	If the Queue is empty
		 */
		b8 IsEmpty() const;
		/**
		 * Get the size of the Queue
		 * @return	Size of the Queue
		 */
		sizeT Size() const;
		/**
		 * Get the size of the Queue
		 * @return	Capacity of the Queue
		 */
		sizeT Capacity() const;
		/**
		 * Get a pointer to the allocator
		 * @return	Pointer to the allocator
		 */
		Memory::IAllocator* GetAllocator();
		/**
		 * Get the internal container of the stack
		 * @return	Reference to the internal container of the stack
		 */
		Deque<T, SubArraySize, GrowthPolicy>& GetInternalContainer();
		/**
		 * Get the internal container of the stack
		 * @return	Internal container of the stack
		 */
		const Deque<T, SubArraySize, GrowthPolicy>& GetInternalContainer() const;

		/**
		 * Return an iterator to the front/begin of the array
		 * @return	Iterator to the front/begin of the array
		 */
		Iterator Front();
		/**
		 * Return an iterator to the front/begin of the array
		 * @return	Iterator to the front/begin of the array
		 */
		const Iterator Front() const;
		/**
		 * Return an iterator to the back/end of the array
		 * @return	Iterator to the back/end of the array
		 */
		Iterator Back();
		/**
		 * Return an iterator to the back/end of the array
		 * @return	Iterator to the back/end of the array
		 */
		const Iterator Back() const;

		/**
		* Return an iterator to the front/begin of the array
		* @return	Iterator to the front/begin of the array
		*/
		Iterator begin();
		/**
		* Return an iterator to the front/begin of the array
		* @return	Iterator to the front/begin of the array
		*/
		const Iterator begin() const;
		/**
		* Return an iterator to the back/end of the array
		* @return	Iterator to the back/end of the array
		*/
		Iterator end();
		/**
		* Return an iterator to the back/end of the array
		* @return	Iterator to the back/end of the array
		*/
		const Iterator end() const;

	private:

		BaseType m_Container;	/**< Internal container */
	};

}

#include "Inline/Queue.inl"

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename T, sizeT S, Hv::Container::GrowthPolicyFunc G), HV_TTYPE(Hv::Queue, T, S, G))
