// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// List.inl: Doubly linked list
#pragma once
#include "Containers/List.h"

namespace Hv {
	
	template <typename T>
	List<T>::Node::Node()
		: pPrev(nullptr)
		, pNext(nullptr)
	{
	}

	template <typename T>
	List<T>::Node::Node(const T& val, Node* pPrev, Node* pNext)
		: pPrev(pPrev)
		, pNext(pNext)
	{
		if constexpr (Container::UseCopyConstructor<T>)
			new (&value) T(val);
		else
			value = val;
	}

	template <typename T>
	List<T>::Node::Node(T&& val, Node* pPrev, Node* pNext)
		: pPrev(pPrev)
		, pNext(pNext)
	{
		if constexpr (Container::UseCopyConstructor<T>)
			new (&value) T(Forward<T>(val));
		else
			value = Forward<T>(val);
	}

	template <typename T>
	List<T>::Node::~Node()
	{
		value.~T();
	}

	template <typename T>
	List<T>::Iterator::Iterator()
		: m_pNode(nullptr)
	{
	}

	template <typename T>
	List<T>::Iterator::Iterator(const Iterator& it)
		: m_pNode(it.m_pNode)
	{
	}

	template <typename T>
	List<T>::Iterator::Iterator(Iterator&& it) noexcept
		: m_pNode(it.m_pNode)
	{
	}

	template <typename T>
	T& List<T>::Iterator::operator*()
	{
		HV_ASSERT_MSG(m_pNode, "Can't retrieve value!");
		return m_pNode->value;
	}

	template <typename T>
	const T& List<T>::Iterator::operator*() const
	{
		HV_ASSERT_MSG(m_pNode, "Can't retrieve value!");
		return m_pNode->value;
	}

	template <typename T>
	T* List<T>::Iterator::operator->()
	{
		HV_ASSERT_MSG(m_pNode, "Can't retrieve value!");
		return &m_pNode->value;
	}

	template <typename T>
	const T* List<T>::Iterator::operator->() const
	{
		HV_ASSERT_MSG(m_pNode, "Can't retrieve value!");
		return &m_pNode->value;
	}

	template <typename T>
	typename List<T>::Iterator& List<T>::Iterator::operator++()
	{
		HV_ASSERT_MSG(m_pNode, "Can't increment iterator!");
		m_pNode = m_pNode->pNext;
		return *this;
	}

	template <typename T>
	typename List<T>::Iterator List<T>::Iterator::operator++(int)
	{
		HV_ASSERT_MSG(m_pNode, "Can't increment iterator!");
		Iterator it(*this);
		m_pNode = m_pNode->pNext;
		return it;
	}

	template <typename T>
	typename List<T>::Iterator& List<T>::Iterator::operator--()
	{
		HV_ASSERT_MSG(m_pNode, "Can't decrement iterator!");
		m_pNode = m_pNode->pPrev;
		return *this;
	}

	template <typename T>
	typename List<T>::Iterator List<T>::Iterator::operator--(int)
	{
		HV_ASSERT_MSG(m_pNode, "Can't decrement iterator!");
		Iterator it(*this);
		m_pNode = m_pNode->pPrev;
		return it;
	}

	template <typename T>
	typename List<T>::Iterator List<T>::Iterator::operator+(sizeT count)
	{
		HV_ASSERT_MSG(m_pNode, "Can't increment iterator!");
		Iterator it(*this);
		for (sizeT i = 0; i < count; ++i)
		{
			it.m_pNode = it.m_pNode->pNext;
			HV_ASSERT(it.m_pNode);
		}
		return it;
	}

	template <typename T>
	typename List<T>::Iterator List<T>::Iterator::operator-(sizeT count)
	{
		HV_ASSERT_MSG(m_pNode, "Can't decrement iterator!");
		Iterator it(*this);
		for (sizeT i = 0; i < count; ++i)
		{
			it.m_pNode = it.m_pNode->pPrev;
			HV_ASSERT(it.m_pNode);
		}
		return it;
	}

	template <typename T>
	typename List<T>::Iterator& List<T>::Iterator::operator=(const Iterator& it)
	{
		m_pNode = it.m_pNode;
		return *this;
	}

	template <typename T>
	typename List<T>::Iterator& List<T>::Iterator::operator=(Iterator&& it) noexcept
	{
		m_pNode = it.m_pNode;
		return *this;
	}

	template <typename T>
	typename List<T>::Iterator& List<T>::Iterator::operator+=(sizeT count)
	{
		HV_ASSERT_MSG(m_pNode, "Can't increment iterator!");
		for (sizeT i = 0; i < count; ++i)
		{
			m_pNode = m_pNode->pNext;
			HV_ASSERT(m_pNode);
		}
		return *this;
	}

	template <typename T>
	typename List<T>::Iterator& List<T>::Iterator::operator-=(sizeT count)
	{
		HV_ASSERT_MSG(m_pNode, "Can't decrement iterator!");
		for (sizeT i = 0; i < count; ++i)
		{
			m_pNode = m_pNode->pPrev;
			HV_ASSERT(m_pNode);
		}
		return *this;
	}

	template <typename T>
	b8 List<T>::Iterator::operator==(const Iterator& it) const
	{
		return m_pNode == it.m_pNode;
	}

	template <typename T>
	b8 List<T>::Iterator::operator!=(const Iterator& it) const
	{
		return m_pNode != it.m_pNode;
	}

	template <typename T>
	List<T>::Iterator::Iterator(Node* pNode)
		: m_pNode(pNode)
	{
	}

	template <typename T>
	HV_INL List<T>::List()
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(g_pAllocator)
	{
	}

	template <typename T>
	HV_INL List<T>::List(Memory::IAllocator* pAlloc)
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(pAlloc)
	{
	}

	template <typename T>
	HV_INL List<T>::List(sizeT count, const T& value, Memory::IAllocator* pAlloc)
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Fill(value, count);
	}

	template <typename T>
	template <sizeT N>
	HV_INL List<T>::List(T(& arr)[N], Memory::IAllocator* pAlloc)
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(pAlloc)
	{
		Assign(arr, N);
	}

	template <typename T>
	HV_INL List<T>::List(const T* arr, sizeT count, Memory::IAllocator* pAlloc)
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Assign(arr, count);
	}

	template <typename T>
	HV_INL List<T>::List(const T* itFirst, const T* itLast, Memory::IAllocator* pAlloc)
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		Assign(itFirst, sizeT(itLast - itFirst));
	}

	template <typename T>
	template <typename InputIterator>
	HV_INL List<T>::List(const InputIterator& itFirst, const InputIterator& itLast, Memory::IAllocator* pAlloc)
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		Assign(itFirst, itLast);
	}

	template <typename T>
	HV_INL List<T>::List(std::initializer_list<T> il, Memory::IAllocator* pAlloc)
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(il.size())
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Assign(il.begin(), il.size());
	}

	template <typename T>
	HV_INL List<T>::List(const List& list)
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(list.m_pAlloc)
	{
		Copy(list);
	}

	template <typename T>
	HV_INL List<T>::List(const List& list, Memory::IAllocator* pAlloc)
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(pAlloc)
	{
		Copy(list);
	}

	template <typename T>
	HV_INL List<T>::List(List&& list) noexcept
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(list.m_pAlloc)
	{
		Hv::Swap(m_pHead, list.m_pHead);
		Hv::Swap(m_pTail, list.m_pTail);
		Hv::Swap(m_Size, list.m_Size);
	}

	template <typename T>
	HV_INL List<T>::~List()
	{
		Clear();
	}

	template <typename T>
	template <sizeT N>
	HV_INL List<T>& List<T>::operator=(T(& arr)[N])
	{
		Clear();
		Assign(arr, N);
		return *this;
	}

	template <typename T>
	HV_INL List<T>& List<T>::operator=(std::initializer_list<T> il)
	{
		Clear();
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Assign(il.begin(), il.size());
		return *this;
	}

	template <typename T>
	HV_INL List<T>& List<T>::operator=(const List& list)
	{
		Clear();
		Copy(list);
		return *this;
	}

	template <typename T>
	HV_INL List<T>& List<T>::operator=(List&& list) noexcept
	{
		if (m_pAlloc == list.m_pAlloc)
		{
			Hv::Swap(m_pHead, list.m_pHead);
			Hv::Swap(m_pTail, list.m_pTail);
			Hv::Swap(m_Size, list.m_Size);
		}
		else
		{
			Copy(list);
		}
		list.Clear();
		return *this;
	}

	template <typename T>
	HV_FORCE_INL T& List<T>::operator[](sizeT index)
	{
		HV_ASSERT_MSG(index < m_Size, "Index out of range!");
		return GetNodeAtIndex(index)->value;
	}

	template <typename T>
	HV_FORCE_INL const T& List<T>::operator[](sizeT index) const
	{
		HV_ASSERT_MSG(index < m_Size, "Index out of range!");
		return GetNodeAtIndex(index)->value;
	}

	template <typename T>
	void List<T>::Fill(const T& val, sizeT count)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Clear();
		m_Size = count;
		Node* pPrev = nullptr;
		for (sizeT i = 0; i < m_Size; ++i)
		{
			Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			new (pNode) Node(val, pPrev);
			if (pPrev)
				pPrev->pNext = pNode;
			else
				m_pHead = pNode;
			pPrev = pNode;
		}
		m_pTail = pPrev;
	}

	template <typename T>
	template <sizeT N>
	HV_INL void List<T>::Assign(T(& arr)[N])
	{
		Assign(arr, N);
	}

	template <typename T>
	HV_INL void List<T>::Assign(T* arr, sizeT count)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Clear();
		m_Size = count;
		Node* pPrev = nullptr;
		for (sizeT i = 0; i < m_Size; ++i)
		{
			Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			new (pNode) Node(arr[i], pPrev);
			if (pPrev)
				pPrev->pNext = pNode;
			else
				m_pHead = pNode;
			pPrev = pNode;
		}
		m_pTail = pPrev;
	}

	template <typename T>
	HV_INL void List<T>::Assign(T* itFirst, T* itLast)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		Assign(itFirst, sizeT(itLast - itFirst));
	}

	template <typename T>
	template <typename InputIterator>
	HV_INL void List<T>::Assign(const InputIterator& itFirst, const InputIterator& itLast)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		Clear();
		m_Size = 0;
		Node* pPrev = nullptr;
		for (const T* it = itFirst; it != itLast; ++it)
		{
			Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			new (pNode) Node(*it, pPrev);
			if (pPrev)
				pPrev->pNext = pNode;
			else
				m_pHead = pNode;
			pPrev = pNode;
			++m_Size;
		}
		m_pTail = pPrev;
	}

	template <typename T>
	HV_INL void List<T>::Assign(std::initializer_list<T> il)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Assign(il.begin(), il.size());
	}

	template <typename T>
	HV_INL void List<T>::Resize(sizeT size)
	{
		if (size == 0)
			Clear();
		else if (size < m_Size)
		{
			Node* pNode = GetNodeAtIndex(size - 1);
			Node* pNext = pNode->pNext;
			pNode->pNext = nullptr;
			m_pTail = pNode;
			pNode = pNext;
			while (pNode)
			{
				pNext = pNode->pNext;
				pNode->~Node();
				m_pAlloc->Free(pNode);
				pNode = pNext;
			}
			m_Size = size;
		}
	}

	template <typename T>
	HV_INL void List<T>::Resize(sizeT size, const T& val)
	{
		if (size == 0)
			Clear();
		else if (size < m_Size)
		{
			Node* pNode = GetNodeAtIndex(size - 1);
			Node* pNext = pNode->pNext;
			pNode->pNext = nullptr;
			m_pTail = pNode;
			pNode = pNext;
			while (pNode)
			{
				pNext = pNode->pNext;
				pNode->~Node();
				m_pAlloc->Free(pNode);
				pNode = pNext;
			}
			
		}
		else
		{
			Node* pPrev = m_pTail;
			for (sizeT i = 0, count = size - m_Size; i < count; ++i)
			{
				Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
				new (pNode) Node(val, pPrev);
				if (pPrev)
					pPrev->pNext = pNode;
				else
					m_pHead = pNode;
				pPrev = pNode;
			}
			m_pTail = pPrev;
		}
		m_Size = size;
	}

	template <typename T>
	HV_INL void List<T>::Clear()
	{
		Node* pNode = m_pHead;
		while (pNode)
		{
			Node* pNext = pNode->pNext;
			pNode->~Node();
			m_pAlloc->Free(pNode);
			pNode = pNext;
		}
		m_Size = 0;
		m_pHead = m_pTail = nullptr;
	}

	template <typename T>
	HV_INL void List<T>::PushBack(const T& val)
	{
		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(val, m_pTail);
		if (m_pTail)
			m_pTail->pNext = pNode;
		else
			m_pHead = pNode;
		m_pTail = pNode;
		++m_Size;
	}

	template <typename T>
	HV_INL void List<T>::PushBack(T&& val)
	{
		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(Forward<T>(val), m_pTail);
		if (m_pTail)
			m_pTail->pNext = pNode;
		else
			m_pHead = pNode;
		m_pTail = pNode;
		++m_Size;
	}

	template <typename T>
	HV_INL void List<T>::PopBack()
	{
		if (m_pTail)
		{
			Node* pNode = m_pTail;

			if (m_pTail == m_pHead)
				m_pHead = m_pTail = nullptr;
			else
			{
				m_pTail = pNode->pPrev;
				m_pTail->pNext = nullptr;
			}

			pNode->~Node();
			m_pAlloc->Free(pNode);
			--m_Size;
		}
	}

	template <typename T>
	HV_INL void List<T>::PushFront(const T& val)
	{
		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(val, nullptr, m_pHead);
		if (m_pHead)
			m_pHead->pPrev = pNode;
		else
			m_pTail = pNode;
		m_pHead = pNode;
		++m_Size;
	}

	template <typename T>
	HV_INL void List<T>::PushFront(T&& val)
	{
		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(Forward<T>(val), nullptr, m_pHead);
		if (m_pHead)
			m_pHead->pPrev = pNode;
		else
			m_pTail = pNode;
		m_pHead = pNode;
		++m_Size;
	}

	template <typename T>
	HV_INL void List<T>::PopFront()
	{
		if (m_pHead)
		{
			Node* pNode = m_pHead;

			if (m_pHead == m_pTail)
				m_pTail = m_pTail = nullptr;
			else
			{
				m_pHead = pNode->pPrev;
				m_pHead->pNext = nullptr;
			}

			pNode->~Node();
			m_pAlloc->Free(pNode);
			--m_Size;
		}
	}

	template <typename T>
	HV_INL void List<T>::Insert(const Iterator& it, const T& val)
	{
		Node* pNext = it.m_pNode;
		Node* pPrev = pNext ? pNext->pPrev : m_pTail;
		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(val, pPrev, pNext);
		if (pPrev)
			pPrev->pNext = pNode;
		else
			m_pHead = pNode;
		if (pNext)
			pNext->pPrev = pNode;
		else
			m_pTail = pNode;
		++m_Size;
	}

	template <typename T>
	HV_INL void List<T>::Insert(const Iterator& it, T&& val)
	{
		Node* pNext = it.m_pNode;
		Node* pPrev = pNext ? pNext->pPrev : m_pTail;
		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(Forward<T>(val), pPrev, pNext);
		if (pPrev)
			pPrev->pNext = pNode;
		else
			m_pHead = pNode;
		if (pNext)
			pNext->pPrev = pNode;
		else
			m_pTail = pNode;
		++m_Size;
	}

	template <typename T>
	HV_INL void List<T>::Insert(const Iterator& it, sizeT count, const T& val)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Node* pNext = it.m_pNode;
		Node* pPrev = pNext ? pNext->pPrev : m_pTail;
		Node* pNode = nullptr;
		for (sizeT i = 0; i < count; ++i)
		{
			pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			new (pNode) Node(val, pPrev, pNext);
			if (pPrev)
				pPrev->pNext = pNode;
			else
				m_pHead = pNode;
			pPrev = pNode;
		}
		if (pNext)
			pNext->pPrev = pNode;
		else
			m_pTail = pNode;
		m_Size += count;
	}

	template <typename T>
	template <sizeT N>
	HV_INL void List<T>::Insert(const Iterator& it, T(& arr)[N])
	{
		Insert(it, arr, N);
	}

	template <typename T>
	HV_INL void List<T>::Insert(const Iterator& it, T* arr, sizeT count)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Node* pNext = it.m_pNode;
		Node* pPrev = pNext ? pNext->pPrev : m_pTail;
		Node* pNode = nullptr;
		for (sizeT i = 0; i < count; ++i)
		{
			pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			new (pNode) Node(arr[i], pPrev, pNext);
			if (pPrev)
				pPrev->pNext = pNode;
			else
				m_pHead = pNode;
			pPrev = pNode;
		}
		if (pNext)
			pNext->pPrev = pNode;
		else
			m_pTail = pNode;
		m_Size += count;
	}

	template <typename T>
	HV_INL void List<T>::Insert(const Iterator& it, const T* itFirst, const T* itLast)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		Insert(it, itFirst, sizeT(itLast - itFirst));
	}

	template <typename T>
	template <typename InputIterator>
	HV_INL void List<T>::Insert(const Iterator& it, const InputIterator& itFirst, const InputIterator& itLast)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		Node* pNext = it.m_pNode;
		Node* pPrev = pNext ? pNext->pPrev : m_pTail;
		Node* pNode = nullptr;
		for (Iterator itData = itFirst; itData != itLast; ++itData)
		{
			pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			new (pNode) Node(*itData, pPrev, pNext);
			if (pPrev)
				pPrev->pNext = pNode;
			else
				m_pHead = pNode;
			pPrev = pNode;
			++m_Size;
		}
		if (pNext)
			pNext->pPrev = pNode;
		else
			m_pTail = pNode;
	}

	template <typename T>
	HV_INL void List<T>::Insert(const Iterator& it, std::initializer_list<T> il)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Insert(il.begin(), il.size());
	}

	template <typename T>
	HV_INL void List<T>::Erase(const Iterator& it)
	{
		HV_ASSERT_MSG(it.m_pNode, "Iterator out of range!");
		Node* pNode = it.m_pNode;
		Node* pPrev = pNode->pPrev;
		Node* pNext = pNode->pNext;

		pNode->~Node();
		m_pAlloc->Free(pNode);

		if (pPrev)
			pPrev->pNext = pNext;
		else
			m_pHead = pNext;
		if (pNext)
			pNext->pPrev = pPrev;
		else
			m_pTail = pPrev;
		--m_Size;
	}

	template <typename T>
	HV_INL void List<T>::Erase(const Iterator& it, sizeT count)
	{
		HV_ASSERT_MSG(it.m_pNode, "Iterator out of range!");
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		HV_ASSERT_MSG(count <= m_Size, "Count is too big, reaching past SList's size!");
		Node* pNode = it.m_pNode;
		Node* pPrev = pNode->pPrev;
		Node* pNext = nullptr;

		for (sizeT i = 0; i < count; ++i)
		{
			HV_ASSERT_MSG(pNode, "Count is too big, reaching past SList's size!");
			pNext = pNode->pNext;
			pNode->~Node();
			m_pAlloc->Free(pNode);
			pNode = pNext;
		}

		if (pPrev)
			pPrev->pNext = pNext;
		else
			m_pHead = pNext;
		if (pNext)
			pNext->pPrev = pPrev;
		else
			m_pTail = pPrev;
		m_Size -= count;
	}

	template <typename T>
	HV_INL void List<T>::Erase(const Iterator& itFirst, const Iterator& itLast)
	{
		HV_ASSERT_MSG(itFirst.m_pNode, "First iterator out of range!");
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");

		Node* pNode = itFirst.m_pNode;
		Node* pPrev = pNode->pPrev;
		Node* pNext = nullptr;

		while (pNode != itLast.m_pNode)
		{
			HV_ASSERT_MSG(pNode, "ItLast out of range!");
			pNext = pNode->pNext;
			pNode->~Node();
			m_pAlloc->Free(pNode);
			pNode = pNext;
			--m_Size;
		}

		if (pPrev)
			pPrev->pNext = pNext;
		else
			m_pHead = pNext;
		if (pNext)
			pNext->pPrev = pPrev;
		else
			m_pTail = pPrev;
	}

	template <typename T>
	template <typename ... Args>
	HV_INL void List<T>::Emplace(const Iterator& it, Args&&... args)
	{
		Node* pNext = it.m_pNode;
		Node* pPrev = pNext ? pNext->pPrev : m_pTail;
		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(Forward<T>(T(args...)), pPrev, pNext);
		if (pPrev)
			pPrev->pNext = pNode;
		else
			m_pHead = pNode;
		if (pNext)
			pNext->pPrev = pNode;
		else
			m_pTail = pNode;
		++m_Size;
	}

	template <typename T>
	template <typename ... Args>
	HV_INL void List<T>::EmplaceBack(Args&&... args)
	{
		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(Forward<T>(T(args...)), m_pTail);
		if (m_pTail)
			m_pTail->pNext = pNode;
		else
			m_pHead = pNode;
		m_pTail = pNode;
		++m_Size;
	}

	template <typename T>
	template <typename ... Args>
	HV_INL void List<T>::EmplaceFront(Args&&... args)
	{
		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(Forward<T>(T(args...)), nullptr, m_pHead);
		if (m_pHead)
			m_pHead->pPrev = pNode;
		else
			m_pTail = pNode;
		m_pHead = pNode;
		++m_Size;
	}

	template <typename T>
	HV_INL void List<T>::Swap(List& list)
	{
		if (m_pAlloc == list.m_pAlloc)
		{
			Hv::Swap(m_pHead, list.m_pHead);
			Hv::Swap(m_pTail, list.m_pTail);
			Hv::Swap(m_Size, list.m_Size);
		}
		else
		{
			List tmp(Move(*this), m_pAlloc);
			*this = Move(list);
			list = Move(tmp);
		}
	}

	template <typename T>
	HV_INL typename List<T>::Node* List<T>::GetNodeAtIndex(sizeT index)
	{
		HV_ASSERT_MSG(index < m_Size, "Index out of range!");
		sizeT half = m_Size / 2;
		if (index <= half)
		{
			Node* pNode = m_pHead;
			for (sizeT i = 0; i < index; ++i)
			{
				pNode = pNode->pNext;
			}
			return pNode;
		}
		else
		{
			Node* pNode = m_pTail;
			for (sizeT i = 0, diff = m_Size - index - 1; i < diff; ++i)
			{
				pNode = pNode->pPrev;
			}
			return pNode;
		}
	}

	template <typename T>
	HV_FORCE_INL b8 List<T>::IsEmpty() const
	{
		return m_Size == 0;
	}

	template <typename T>
	HV_FORCE_INL sizeT List<T>::Size() const
	{
		return m_Size;
	}

	template <typename T>
	HV_FORCE_INL Memory::IAllocator* List<T>::GetAllocator()
	{
		return m_pAlloc;
	}

	template <typename T>
	HV_FORCE_INL typename List<T>::Iterator List<T>::Front() const
	{
		return Iterator(m_pHead);
	}

	template <typename T>
	HV_FORCE_INL typename List<T>::Iterator List<T>::Last() const
	{
		return Iterator(m_pTail);
	}

	template <typename T>
	HV_FORCE_INL typename List<T>::Iterator List<T>::Back() const
	{
		return Iterator(nullptr);
	}

	template <typename T>
	HV_FORCE_INL typename List<T>::Iterator List<T>::begin() const
	{
		return Iterator(m_pHead);
	}

	template <typename T>
	HV_FORCE_INL typename List<T>::Iterator List<T>::end() const
	{
		return Iterator(nullptr);
	}

	template <typename T>
	void List<T>::Copy(const List& list)
	{
		Clear();
		if (!m_pAlloc)
			m_pAlloc = list.m_pAlloc;
		m_Size = list.m_Size;
		Node* pPrev = nullptr, *pOther = list.m_pHead;
		for (sizeT i = 0; i < m_Size; ++i)
		{
			Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			new (pNode) Node(pOther->value, pPrev);
			if (pPrev)
				pPrev->pNext = pNode;
			else
				m_pHead = pNode;
			pPrev = pNode;
			pOther = pOther->pNext;
		}
		m_pTail = pPrev;
		m_Size = list.m_Size;
	}

}
