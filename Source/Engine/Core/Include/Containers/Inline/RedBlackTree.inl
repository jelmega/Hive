// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// RedBlackTree.inl: Red-black tree
#pragma once
#include "Containers/RedBlackTree.h"

namespace Hv {
	
#define GET_SUCCESSOR(node, successor)\
	successor = node->pRight;\
	if (successor)\
	{\
		while(successor->pLeft)\
		{\
			successor = successor->pLeft;\
		}\
	}

#define GET_GRANDPARENT(node, grandparent)\
	if (node->pParent)\
		grandparent = node->pParent->pParent;\
	else\
		grandparent = nullptr;

#define GET_SIBLING(node, sibling)\
	if (node->pParent)\
	{\
		if (node == node->pParent->pRight)\
			sibling = node->pParent->pRight;\
		else\
			sibling = node->pParent->pLeft;\
	}\
	else\
		sibling = nullptr;\

#define GET_UNCLE(node, uncle)\
	if (node->pParent)\
		GET_SIBLING(node->pParent, uncle)\
	else\
		uncle = nullptr;

#define IS_BLACK(node) (!node || node->color == Color::Black)
#define IS_RED(node) (node && node->color == Color::Red)


	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Node::Node()
		: pParent(nullptr)
		, pLeft(nullptr)
		, pRight(nullptr)
		, color(Color::Black)
	{
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Node::Node(const T& val)
		: pParent(nullptr)
		, pLeft(nullptr)
		, pRight(nullptr)
		, color(Color::Black)
	{
		if constexpr (Container::UseCopyConstructor<T>)
			new (&value) T(val);
		else
			value = val;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Node::Node(T&& val)
		: pParent(nullptr)
		, pLeft(nullptr)
		, pRight(nullptr)
		, color(Color::Black)
	{
		if constexpr (Container::UseCopyConstructor<T>)
			new (&value) T(Forward<T>(val));
		else
			value = Forward<T>(val);
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Node::Node(const Node& node)
		: pParent(node.pParent)
		, pLeft(node.pLeft)
		, pRight(node.pRight)
		, color(node.color)
	{
		if constexpr (Container::UseCopyConstructor<T>)
			new (&value) T(node.value);
		else
			value = node.value;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Node::Node(Node&& node) noexcept
		: pParent(node.pParent)
		, pLeft(node.pLeft)
		, pRight(node.pRight)
		, color(node.color)
	{
		if constexpr (Container::UseCopyConstructor<T>)
			new (&value) T(Forward<T>(node.value));
		else
			value = Forward<T>(node.value);
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Node::~Node()
	{
		value.~T();
	}

	////////////////////////////////////////////////////////////////////////////////

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::Iterator()
		: m_pNode(nullptr)
	{
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::Iterator(const Iterator& it)
		: m_pNode(it.m_pNode)
	{
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::Iterator(Iterator&& it) noexcept
		: m_pNode(it.m_pNode)
	{
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator& RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::operator++()
	{
		HV_ASSERT_MSG(m_pNode, "Can't increment iterator!");
		Node* pNode = m_pNode, *pPrev = nullptr;
		while (true)
		{
			if (!pPrev && pNode->pRight != pPrev)
			{
				if (pNode != m_pNode)
				{
					if (pNode->pLeft && pNode->pLeft != pPrev)
					{
						pPrev = pNode;
						pNode = pNode->pLeft;
						continue;
					}
					break;
				}
				if (pNode->pRight)
				{
					pPrev = pNode;
					pNode = pNode->pRight;
					continue;
				}
			}

			if (pNode->pParent)
			{
				pPrev = pNode;
				pNode = pNode->pParent;
				continue;
			}
			m_pNode = nullptr;
			break;
		}
		return *this;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::operator++(int)
	{
		Iterator it(*this);
		++it;
		return it;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator& RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::operator--()
	{
		HV_ASSERT_MSG(m_pNode, "Can't decrement iterator!");
		Node *pNode = m_pNode, *pPrev = nullptr;
		while (true)
		{
			if (!pPrev || pNode->pLeft != pPrev)
			{
				if (pNode != m_pNode)
				{
					if (pNode->pRight && pNode->pRight != pPrev)
					{
						pPrev = pNode;
						pNode = pNode->pRight;
						continue;
					}
					break;
				}
				if (pNode->pLeft)
				{
					pPrev = pNode;
					pNode = pNode->pLeft;
					continue;
				}
			}
			if (pNode->pParent)
			{
				pPrev = pNode;
				pNode = pNode->pParent;
				continue;
			}
			pNode = nullptr;
			break;
		}
		m_pNode = pNode;
		return *this;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::operator--(int)
	{
		Iterator it(*this);
		--it;
		return it;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::operator+(sizeT count)
	{
		Iterator it(*this);
		for (sizeT i = 0; i < count; ++i)
			++it;
		return *this;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::operator-(sizeT count)
	{
		Iterator it(*this);
		for (sizeT i = 0; i < count; ++i)
			--it;
		return *this;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator& RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::operator=(const Iterator& it)
	{
		m_pNode = it.m_pNode;
		return *this;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator& RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::operator=(Iterator&& it) noexcept
	{
		m_pNode = it.m_pNode;
		return *this;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator& RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::operator+=(sizeT count)
	{
		for (sizeT i = 0; i < count; ++i)
			++*this;
		return *this;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator& RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::operator-=(sizeT count)
	{
		for (sizeT i = 0; i < count; ++i)
			--*this;
		return *this;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL typename std::conditional<MutableIterator, T&, const T&>::type RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::operator*()
	{
		return m_pNode->value;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL const T& RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::operator*() const
	{
		return m_pNode->value;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL typename std::conditional<MutableIterator, T*, const T*>::type RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::operator->()
	{
		return &m_pNode->value;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL const T* RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::operator->() const
	{
		return &m_pNode->value;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL b8 RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::operator==(const Iterator& it) const
	{
		return m_pNode == it.m_pNode;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL b8 RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::operator!=(const Iterator& it) const
	{
		return m_pNode != it.m_pNode;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator::Iterator(Node* pNode)
		: m_pNode(pNode)
	{
	}

	////////////////////////////////////////////////////////////////////////////////

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::RedBlackTree(Memory::IAllocator* pAlloc)
		: m_pRoot(nullptr)
		, m_Size(0)
		, m_pAlloc(pAlloc)
	{
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::RedBlackTree(const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_pRoot(nullptr)
		, m_Size(0)
		, m_Comp(comp)
		, m_pAlloc(pAlloc)
	{
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::RedBlackTree(const ExtractKey& extract, Memory::IAllocator* pAlloc)
		: m_pRoot(nullptr)
		, m_Size(0)
		, m_Extract(extract)
		, m_pAlloc(pAlloc)
	{
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::RedBlackTree(const CompLess& comp, const ExtractKey& extract, Memory::IAllocator* pAlloc)
		: m_pRoot(nullptr)
		, m_Size(0)
		, m_pAlloc(pAlloc)
	{
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::RedBlackTree(const RedBlackTree& rbt)
		: m_pRoot(nullptr)
		, m_Size(0)
		, m_pAlloc(rbt.m_pAlloc)
	{
		Copy(rbt);
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::RedBlackTree(const RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>& rbt, Memory::IAllocator* pAlloc)
		: m_pRoot(nullptr)
		, m_Size(0)
		, m_pAlloc(pAlloc)
	{
		Copy(rbt);
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::RedBlackTree(RedBlackTree&& rbt) noexcept
		: m_pRoot(nullptr)
		, m_Size(0)
		, m_Comp(rbt.m_Comp)
		, m_Extract(rbt.m_Extract)
		, m_pAlloc(rbt.m_pAlloc)
	{
		Hv::Swap(m_pRoot, rbt.m_pRoot);
		Hv::Swap(m_Size, rbt.m_Size);
		Hv::Swap(m_Comp, rbt.m_Comp);
		Hv::Swap(m_Extract, rbt.m_Extract);
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::~RedBlackTree()
	{
		Clear();
	}
	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>& RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::operator=(const RedBlackTree& rbt)
	{
		Clear();
		if (!m_pAlloc)
			m_pAlloc = rbt.m_pAlloc;

		Copy(rbt);
		return *this;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>& RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::operator=(RedBlackTree&& rbt) noexcept
	{
		Clear();
		m_pAlloc = rbt.m_pAlloc;

		Hv::Swap(m_pRoot, rbt.m_pRoot);
		Hv::Swap(m_Size, rbt.m_Size);
		Hv::Swap(m_Comp, rbt.m_Comp);
		Hv::Swap(m_Extract, rbt.m_Extract);

		return *this;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL Pair<typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator, b8> RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Insert(const T& val)
	{
		return Insert(Iterator(m_pRoot), val);
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL Pair<typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator, b8> RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Insert(T&& val)
	{
		return Insert(Iterator(m_pRoot), Forward<T>(val));
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL Pair<typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator, b8> RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Insert(const Iterator& hint, const T& val)
	{
		if (!m_pRoot)
		{
			Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			new (pNode) Node(val);
			m_pRoot = pNode;
			return Pair<Iterator, b8>(Iterator(pNode), true);
		}

		Node* pTemp = hint.m_pNode, *pPrev = nullptr;
		while (true)
		{
			if (m_Comp(m_Extract(pTemp->value), m_Extract(val)))
			{
				if (pTemp->pLeft == pPrev)
					return Pair<Iterator, b8>(Iterator(), false);
				if (pTemp->pLeft)
				{
					pTemp = pTemp->pLeft;
					continue;
				}
				Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
				new (pNode) Node(val);
				pTemp->pLeft = pNode;
				pNode->pParent = pTemp;
				++m_Size;
				FixInsert(pNode);
				return Pair<Iterator, b8>(Iterator(pNode), true);
			}
			if (!UniqueKey || m_Comp(m_Extract(val), m_Extract(pTemp->value)))
			{
				if (pTemp->pRight == pPrev)
					return Pair<Iterator, b8>(Iterator(), false);
				if (pTemp->pRight)
				{
					pTemp = pTemp->pRight;
					continue;
				}
				Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
				new (pNode) Node(val);
				pTemp->pRight = pNode;
				pNode->pParent = pTemp;
				++m_Size;
				FixInsert(pNode);
				return Pair<Iterator, b8>(Iterator(pNode), true);
			}
			return Pair<Iterator, b8>(Iterator(pTemp), false);
		}
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL Pair<typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator, b8> RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Insert(const Iterator& hint, T&& val)
	{
		if (!m_pRoot)
		{
			Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			new (pNode) Node(Forward<T>(val));
			m_pRoot = pNode;
			return Pair<Iterator, b8>(Iterator(pNode), true);
		}

		Node* pTemp = hint.m_pNode, *pPrev = nullptr;
		while (true)
		{
			if (m_Comp(m_Extract(pTemp->value), m_Extract(val)))
			{
				if (pTemp->pLeft == pPrev)
					return Pair<Iterator, b8>(Iterator(), false);
				if (pTemp->pLeft)
				{
					pTemp = pTemp->pLeft;
					continue;
				}
				Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
				new (pNode) Node(Forward<T>(val));
				pTemp->pLeft = pNode;
				pNode->pParent = pTemp;
				++m_Size;
				FixInsert(pNode);
				return Pair<Iterator, b8>(Iterator(pNode), true);
			}
			if (!UniqueKey || m_Comp(m_Extract(val), m_Extract(pTemp->value)))
			{
				if (pTemp->pRight == pPrev)
					return Pair<Iterator, b8>(Iterator(), false);
				if (pTemp->pRight)
				{
					pTemp = pTemp->pRight;
					continue;
				}
				Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
				new (pNode) Node(Forward<T>(val));
				pTemp->pRight = pNode;
				pNode->pParent = pTemp;
				++m_Size;
				FixInsert(pNode);
				return Pair<Iterator, b8>(Iterator(pNode), true);
			}
			return Pair<Iterator, b8>(Iterator(pTemp), false);
		}
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	template <typename ... Args>
	HV_INL Pair<typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator, b8> RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Emplace(Args&&... args)
	{
		T val(args...);
		return Insert(Iterator(m_pRoot), Forward<T>(val));
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	template <typename ... Args>
	HV_INL Pair<typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator, b8> RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::EmplaceHint(const Iterator& hint, Args&&... args)
	{
		T val(args...);
		return Insert(hint, Forward<T>(val));
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Erase(const T& val)
	{
		return Erase(Find(val));
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	template <typename Key>
	HV_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::EraseKey(const Key& key)
	{
		return Erase(FindKey(key));
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Erase(const Iterator& it)
	{
		if (m_Size > 0)
		{
			Iterator itTemp = it;
			++itTemp;
			Node* pNode = it.m_pNode;
			if (!pNode)
				return;
			Node* pSuccessor;
			GET_SUCCESSOR(pNode, pSuccessor)
				if (pNode != pSuccessor)
					IterSwap(pNode, pSuccessor);

			if (pSuccessor != m_pRoot)
				FixErase(pSuccessor);
			else
				m_pRoot = nullptr;

			pSuccessor->~Node();
			m_pAlloc->Free(pSuccessor);

			return itTemp;
		}
		return Iterator();
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL void RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Clear()
	{
		if (m_pRoot)
		{
			Node* pNode = m_pRoot, *pPrev = nullptr;

			while (pNode)
			{
				if (pNode->pRight)
				{
					pPrev = pNode;
					pNode = pNode->pRight;
					continue;
				}
				if (pNode->pLeft)
				{
					pPrev = pNode;
					pNode = pNode->pLeft;
					continue;
				}

				if (pPrev)
				{
					if (pNode == pPrev->pLeft)
						pPrev->pLeft = nullptr;
					else
						pPrev->pRight = nullptr;
				}
				pNode->~Node();
				m_pAlloc->Free(pNode);
				pNode = pPrev;
			}
			m_Size = 0;
			m_pRoot = nullptr;
		}
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Find(const T& val) const
	{
		if (!m_pRoot)
			return Iterator();

		Node* pNode = m_pRoot;
		while (true)
		{
			if (m_Comp(m_Extract(pNode->value), m_Extract(val)))
			{
				pNode = pNode->pLeft;
			}
			if (m_Comp(m_Extract(val), m_Extract(pNode->value)))
			{
				pNode = pNode->pLeft;
			}
			break;
		}
		return Iterator(pNode);
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	template <typename Key>
	HV_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::FindKey(const Key& key) const
	{
		if (!m_pRoot)
			return Iterator();

		Node* pNode = m_pRoot;
		while (true)
		{
			if (m_Comp(m_Extract(pNode->value), key))
			{
				pNode = pNode->pLeft;
			}
			if (m_Comp(key, m_Extract(pNode->value)))
			{
				pNode = pNode->pLeft;
			}
			break;
		}
		return Iterator(pNode);
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	template <typename U, typename Pred>
	HV_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::FindAs(const U& val, const Pred& pred) const
	{
		if (!m_pRoot)
			return Iterator();

		Node* pNode = m_pRoot, *pPrev = nullptr;
		b8 needSearch = false;
		while (true)
		{
			if (needSearch)
			{
				if (pred(pNode->value, val))
					return Iterator(pNode);
			}
			if (!pPrev || pNode->pRight != pPrev)
			{
				if (pNode->pLeft && pNode->pLeft == pPrev)
				{
					pPrev = pNode;
					pNode = pNode->pLeft;
					needSearch = true;
					continue;
				}
				if (pNode->pLeft && pNode->pLeft == pPrev)
				{
					pPrev = pNode;
					pNode = pNode->pLeft;
					needSearch = true;
					continue;
				}
			}
			if (pNode->pParent)
			{
				pPrev = pNode;
				pNode = pNode->pParent;
				needSearch = false;
				continue;
			}
			break;
		}
		return Iterator();
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	sizeT RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Count(const T& val) const
	{
		Iterator it = Find(val);
		if (!it.m_pNode)
			return 0;

		sizeT count = 1;
		++it;
		while (true)
		{
			if (m_Comp(m_Extract(val), m_Extract(*it)))
				break;

			++it;
			++count;
		}
		return count;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	template <typename Key>
	sizeT RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::CountKey(const Key& key) const
	{
		Iterator it = FindKey(key);
		if (!it.m_pNode)
			return 0;

		sizeT count = 1;
		++it;
		while (true)
		{
			if (m_Comp(key, m_Extract(*it)))
				break;

			++it;
			++count;
		}
		return count;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::LowerBound(const T& val) const
	{
		if (!m_pRoot)
			return Iterator();

		Node* pNode = m_pRoot;
		while (pNode)
		{
			if (pNode->pRight)
			{
				if (m_Comp(m_Extract(val), m_Extract(pNode->pRight->value)))
				{
					pNode = pNode->pRight;
					continue;
				}
			}
			if (pNode->pLeft)
			{
				if (m_Comp(m_Extract(pNode->pLeft->value), m_Extract(val)))
				{
					break;
				}
				pNode = pNode->pLeft;
				continue;
			}
			pNode = nullptr;
			break;
		}
		return Iterator(pNode);
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	template <typename Key>
	HV_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::LowerBoundKey(const Key& key) const
	{
		if (!m_pRoot)
			return Iterator();

		Node* pNode = m_pRoot;
		while (pNode)
		{
			if (pNode->pRight)
			{
				if (m_Comp(key, m_Extract(pNode->pRight->value)))
				{
					pNode = pNode->pRight;
					continue;
				}
			}
			if (pNode->pLeft)
			{
				if (m_Comp(m_Extract(pNode->pLeft->value), key))
				{
					break;
				}
				pNode = pNode->pLeft;
				continue;
			}
			pNode = nullptr;
			break;
		}
		return Iterator(pNode);
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::UpperBound(const T& val) const
	{
		if (!m_pRoot)
			return Iterator();

		Node* pNode = m_pRoot;
		while (pNode)
		{
			if (pNode->pLeft)
			{
				if (m_Comp(m_Extract(pNode->pRight->value), m_Extract(val)))
				{
					pNode = pNode->pLeft;
					continue;
				}
			}
			if (pNode->pRight)
			{
				if (m_Comp(m_Extract(val), m_Extract(pNode->pRight->value)))
				{
					break;
				}
				pNode = pNode->pRight;
				continue;
			}
			pNode = nullptr;
			break;
		}
		return Iterator(pNode);
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	template <typename Key>
	HV_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::UpperBoundKey(const Key& key) const
	{
		if (!m_pRoot)
			return Iterator();

		Node* pNode = m_pRoot;
		while (pNode)
		{
			if (pNode->pLeft)
			{
				if (m_Comp(m_Extract(pNode->pRight->value), key))
				{
					pNode = pNode->pLeft;
					continue;
				}
			}
			if (pNode->pRight)
			{
				if (m_Comp(key, m_Extract(pNode->pRight->value)))
				{
					break;
				}
				pNode = pNode->pRight;
				continue;
			}
			pNode = nullptr;
			break;
		}
		return Iterator(pNode);
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	void RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Swap(RedBlackTree& rbt)
	{
		if (m_pAlloc == rbt.m_pAlloc)
		{
			Hv::Swap(m_pRoot, rbt.m_pRoot);
			Hv::Swap(m_Size, rbt.m_Size);
			Hv::Swap(m_Comp, rbt.m_Comp);
			Hv::Swap(m_Extract, rbt.m_Extract);
		}
		else
		{
			RedBlackTree tmp(Move(*this), m_pAlloc);
			*this = Move(rbt);
			rbt = Move(tmp);
		}
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL b8 RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::IsEmpty() const
	{
		return m_Size == 0;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL sizeT RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Size() const
	{
		return m_Size;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL Memory::IAllocator* RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::GetAllocator() const
	{
		return m_pAlloc;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Front() const
	{
		if (!m_pRoot)
			return Iterator();

		Node* pNode = m_pRoot;
		while (pNode->pLeft)
		{
			pNode = pNode->pLeft;
		}
		return Iterator(pNode);
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Last() const
	{
		if (!m_pRoot)
			return Iterator();

		Node* pNode = m_pRoot;
		while (pNode->pRight)
		{
			pNode = pNode->pRight;
		}
		return Iterator(pNode);
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Back() const
	{
		return Iterator();
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::begin() const
	{
		if (!m_pRoot)
			return Iterator();

		Node* pNode = m_pRoot;
		while (pNode->pLeft)
		{
			pNode = pNode->pLeft;
		}
		return Iterator(pNode);
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	HV_FORCE_INL typename RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Iterator RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::end() const
	{
		return Iterator();
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	void RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::Copy(const RedBlackTree& rbt)
	{
		if (!m_pAlloc)
			m_pAlloc = rbt.m_pAlloc;
		m_Size = rbt.m_Size;
		m_Comp = rbt.m_Comp;
		m_Extract = rbt.m_Extract;

		if (rbt.m_pRoot)
		{
			Node* pOrig = rbt.m_pRoot;

			m_pRoot = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			new (m_pRoot) Node(*pOrig);

			Node* pNode = m_pRoot;
			while (true)
			{
				if (pOrig->pLeft && !pNode->pLeft)
				{
					pOrig = pOrig->pLeft;
					Node* pNew = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
					new (pNew) Node(*pOrig);
					pNew->pParent = pNode;
					pNode->pLeft = pNew;
					pNode = pNew;
					continue;
				}
				if (pOrig->pRight && !pNode->pRight)
				{
					pOrig = pOrig->pRight;
					Node* pNew = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
					new (pNew) Node(*pOrig);
					pNew->pParent = pNode;
					pNode->pRight = pNew;
					pNode = pNew;
					continue;
				}
				if (pOrig->pParent && pNode->pParent)
				{
					pOrig = pOrig->pParent;
					pNode = pNode->pParent;
					continue;
				}
				break;
			}
		}
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	void RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::RotateLeft(Node* pNode)
	{
		//	    B             A
		//	   / \           / \
		//	  A   c  ---->  a   B
		//	 / \               / \
		//	a   b             b   c
		//
		// B == pNode

		HV_ASSERT_MSG(pNode->pLeft, "Couldn't rotate node left!");
		Node* pA = pNode->pLeft;
		Node* pb = pA->pRight;
		Node* pRoot = pNode->pParent;

		pA->pRight = pNode;
		pNode->pParent = pA;
		if (pb)
			pb->pParent = pNode;
		pNode->pLeft = pb;

		if (pRoot)
		{
			if (pRoot->pLeft == pNode)
				pRoot->pLeft = pA;
			else
				pRoot->pRight = pA;
		}
		pA->pParent = pRoot;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	void RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::RotateRight(Node* pNode)
	{
		//	  A               B    
		//	 / \             / \   
		//	a   B   ---->   A   c
		//	   / \         / \   
		//	  b   c       a   b 
		//
		//	A == pNode

		HV_ASSERT_MSG(pNode->pRight, "Can't rotate node right!");
		Node* pB = pNode->pRight;
		Node* pb = pB->pLeft;
		Node* pRoot = pNode->pParent;

		pB->pLeft = pNode;
		pNode->pParent = pB;
		if (pb)
			pb->pParent = pNode;
		pNode->pRight = pb;

		if (pRoot)
		{
			if (pRoot->pLeft == pNode)
				pRoot->pLeft = pB;
			else
				pRoot->pRight = pB;
		}
		pB->pParent = pRoot;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	void RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::FixInsert(Node* pNode)
	{
		Node* pParent = pNode->pParent;
		// pNode is the root of the tree
		if (!pNode->pParent)
		{
			pNode->color = Color::Black;
			return;
		}

		// Parent is black, nothing to fix
		if (pParent->color == Color::Black)
			return;

		Node* pGrandParent = pParent->pParent;
		Node* pUncle;
		GET_SIBLING(pParent, pUncle)
			// Uncle is red, recolor and fix with grandparent
			if IS_RED(pUncle)
			{
				pParent->color = Color::Black;
				pUncle->color = Color::Black;
				pGrandParent->color = Color::Red;
				FixInsert(pGrandParent);
				return;
			}

		// pNode is the right child of parent and parent is the left child of grand parent
		//
		//	  G
		//	 /
		//	P
		//	 \
				//	  N
		if (pGrandParent->pLeft && pNode == pGrandParent->pLeft->pRight)
		{
			RotateLeft(pParent);
			pNode = pNode->pLeft;
		}
		// pNode is the left child of parent and parent is the right child of grand parent
		//
		//	G
		//	 \
		//	  P
		//	 /
		//	N
		else if (pGrandParent->pRight && pNode == pGrandParent->pRight->pLeft)
		{
			RotateRight(pParent);
			pNode = pNode->pRight;
		}

		pParent = pNode->pParent;
		pGrandParent = pParent->pParent;

		// in the case of
		//
		//	    G      G
		//	   /        \
		//	  P    or    P
		//	 /            \
		//	N              N
		//
		// Rotate around the grandparent and recolor the nodes
		if (pNode == pParent->pLeft)
			RotateRight(pGrandParent);
		else
			RotateLeft(pGrandParent);
		pParent->color = Color::Black;
		pGrandParent->color = Color::Red;
	}

	template <typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	void RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>::FixErase(Node* pNode)
	{
		// pNode has max 1 child
		Node* pChild = !pNode->pLeft ? pNode->pRight : pNode->pLeft;

		Node* pParent = pNode->pParent;

		// pNode is red, replace with child
		if (pNode->color == Color::Red)
		{
			if (pChild)
				pChild->pParent = pParent;
			if (pNode == pParent->pLeft)
				pParent->pLeft = pChild;
			else
				pParent->pRight = pChild;
			return;
		}

		// pNode is black and pChild is red, replace node with pChild and recolor pChild
		if IS_RED(pChild)
		{
			pChild->color = Color::Black;
			pChild->pParent = pParent;
			if (pNode == pParent->pLeft)
				pParent->pLeft = pChild;
			else
				pParent->pRight = pChild;
			return;
		}

		// pNode is root, return
		if (!pParent)
			return;

		Node* pSibling;
		GET_SIBLING(pNode, pSibling);
		HV_ASSERT(pSibling);
		// Sibling is red
		if (pSibling->color == Color::Red)
		{
			pParent->color = Color::Red;
			pSibling->color = Color::Black;
			if (pNode == pParent->pLeft)
				RotateLeft(pParent);
			else
				RotateRight(pParent);
		}

		// pParent, pSibling and pSibling's leafs are black
		if (pParent->color == Color::Black && pSibling->color == Color::Black && IS_BLACK(pSibling->pLeft) && IS_BLACK(pSibling->pRight))
		{
			pSibling->color = Color::Red;
			FixErase(pParent);
			return;
		}

		// pParent is red and pSibling and pSibling's leafs are black
		if (pParent->color == Color::Red && pSibling->color == Color::Black && IS_BLACK(pSibling->pLeft) && IS_BLACK(pSibling->pRight))
		{
			pSibling->color = Color::Red;
			pParent->color = Color::Black;
			return;
		}

		// pSibling is black
		if (pSibling->color == Color::Black)
		{
			if (pNode == pParent->pLeft && IS_BLACK(pSibling->pRight) && IS_RED(pSibling->pLeft))
			{
				pSibling->color = Color::Red;
				pSibling->pLeft = Color::Black;
				RotateRight(pSibling);
			}
			else if (pNode == pParent->pRight && IS_BLACK(pSibling->pLeft) && IS_RED(pSibling->pRight))
			{
				pSibling->color = Color::Red;
				pSibling->pRight->color = Color::Black;
				RotateLeft(pSibling);
			}
		}

		// pSibling is black and outer child is red, rotate and recolor
		pSibling->color = pParent->color;
		pParent->color = Color::Black;

		if (pNode == pParent->pLeft)
		{
			pSibling->pRight->color = Color::Black;
			RotateLeft(pParent);
		}
		else
		{
			pSibling->pLeft->color = Color::Black;
			RotateLeft(pParent);
		}
	}

#undef IS_RED
#undef IS_BLACK
#undef GET_UNCLE
#undef GET_SIBLING
#undef GET_GRANDPARENT
#undef GET_SUCCESSOR

}
