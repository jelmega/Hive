// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HashMap.inl: Hash/unordered map
#pragma once
#include "Containers/HashMap.h"

namespace Hv {

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMap()
		: m_HashTable(g_pAllocator)
	{
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMap(Memory::IAllocator* pAlloc)
		: m_HashTable(pAlloc)
	{
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMap(const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc)
		: m_HashTable(comp, hash, pAlloc)
	{
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <sizeT N>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMap(Pair<Key, Value>(&arr)[N], Memory::IAllocator* pAlloc)
		: m_HashTable(pAlloc)
	{
		Reserve(N);
		for (sizeT i = 0; i < N; ++i)
			m_HashTable.Insert(arr[i]);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <sizeT N>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMap(Pair<Key, Value>(&arr)[N], const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc)
		: m_HashTable(comp, hash, pAlloc)
	{
		Reserve(N);
		for (sizeT i = 0; i < N; ++i)
			m_HashTable.Insert(arr[i]);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMap(const Pair<Key, Value>* arr, sizeT count, Memory::IAllocator* pAlloc)
		: m_HashTable(pAlloc)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Reserve(count);
		for (sizeT i = 0; i < count; ++i)
			m_HashTable.Insert(arr[i]);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMap(const Pair<Key, Value>* arr, sizeT count, const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc)
		: m_HashTable(comp, hash, pAlloc)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Reserve(count);
		for (sizeT i = 0; i < count; ++i)
			m_HashTable.Insert(arr[i]);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMap(const Pair<Key, Value>* itFirst, const Pair<Key, Value>* itLast, Memory::IAllocator* pAlloc)
		: m_HashTable(pAlloc)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		sizeT count = sizeT(itLast - itFirst);
		Reserve(count);
		const Pair<Key, Value>* it = itFirst;
		for (sizeT i = 0; i < count; ++i, ++it)
			m_HashTable.Insert(*it);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMap(const Pair<Key, Value>* itFirst, const Pair<Key, Value>* itLast, const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc)
		: m_HashTable(comp, hash, pAlloc)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		sizeT count = sizeT(itLast - itFirst);
		Reserve(count);
		const Pair<Key, Value>* it = itFirst;
		for (sizeT i = 0; i < count; ++i, ++it)
			m_HashTable.Insert(*it);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <typename InputIterator>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMap(const InputIterator& itFirst, const InputIterator& itLast, Memory::IAllocator* pAlloc)
		: m_HashTable(pAlloc)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		sizeT count = 0;
		for (InputIterator it = itFirst; itFirst != itLast; ++it)
			++count;
		Reserve(count);
		InputIterator it = itFirst;
		for (sizeT i = 0; i < count; ++i, ++it)
			m_HashTable.Insert(*it);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <typename InputIterator>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMap(const InputIterator& itFirst, const InputIterator& itLast, const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc)
		: m_HashTable(comp, hash, pAlloc)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		sizeT count = 0;
		for (InputIterator it = itFirst; itFirst != itLast; ++it)
			++count;
		Reserve(count);
		InputIterator it = itFirst;
		for (sizeT i = 0; i < count; ++i, ++it)
			m_HashTable.Insert(*it);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMap(std::initializer_list<Pair<Key, Value>> il, Memory::IAllocator* pAlloc)
		: m_HashTable(pAlloc)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Reserve(il.size());
		const Pair<Key, Value>* it = il.begin();
		for (sizeT i = 0; i < il.size(); ++i, ++it)
			m_HashTable.Insert(*it);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMap(std::initializer_list<Pair<Key, Value>> il, const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc)
		: m_HashTable(comp, hash, pAlloc)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Reserve(il.size());
		const Pair<Key, Value>* it = il.begin();
		for (sizeT i = 0; i < il.size(); ++i, ++it)
			m_HashTable.Insert(*it);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMap(const HashMap& map)
		: m_HashTable(map.m_HashTable)
	{
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMap(const HashMap& map, Memory::IAllocator* pAlloc)
		: m_HashTable(map.m_HashTable, pAlloc)
	{
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMap(HashMap&& map) noexcept
		: m_HashTable(Forward<BaseType>(map.m_HashTable))
	{
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::~HashMap()
	{
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <sizeT N>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>& HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::operator=(Pair<Key, Value>(&arr)[N])
	{
		m_HashTable.Clear();
		Reserve(N);
		for (sizeT i = 0; i < N; ++i)
			m_HashTable.Insert(arr[i]);
		return *this;
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>& HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::operator=(std::initializer_list<Pair<Key, Value>> il)
	{
		Clear();
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Reserve(il.size());
		const Pair<Key, Value>* it = il.begin();
		for (sizeT i = 0; i < il.size(); ++i, ++it)
			m_HashTable.Insert(*it);
		return *this;
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>& HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::operator=(const HashMap& map)
	{
		m_HashTable = map.m_HashTable;
		return *this;
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>& HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::operator=(HashMap&& map) noexcept
	{
		m_HashTable = Forward<BaseType>(map.m_HashTable);
		return *this;
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	Pair<typename HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator, b8> HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Insert(const Pair<Key, Value>& val)
	{
		return m_HashTable.Insert(val);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	Pair<typename HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator, b8> HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Insert(const Key& key, const Value& val)
	{
		return m_HashTable.Insert(Forward<KeyValuePair>(KeyValuePair(key, val)));
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	Pair<typename HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator, b8> HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Insert(Pair<Key, Value>&& val)
	{
		return m_HashTable.Insert(Forward<KeyValuePair>(val));
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	Pair<typename HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator, b8> HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Insert(Key&& key, Value&& val)
	{
		return m_HashTable.Insert(Forward<KeyValuePair>(KeyValuePair(Forward<Key>(key), Forward<Value>(val))));
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <sizeT N>
	void HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Insert(Pair<Key, Value>(&arr)[N])
	{
		Reserve(m_HashTable.Size() + N);
		for (sizeT i = 0; i < N; ++i)
			m_HashTable.Insert(arr[i]);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	void HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Insert(const Pair<Key, Value>* arr, sizeT count)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Reserve(m_HashTable.Size() + count);
		for (sizeT i = 0; i < count; ++i)
			m_HashTable.Insert(arr[i]);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	void HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Insert(const Pair<Key, Value>* itFirst, const Pair<Key, Value>* itLast)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		sizeT count = sizeT(itLast - itFirst);
		Reserve(m_HashTable.Size() + count);
		const Pair<Key, Value>* it = itFirst;
		for (sizeT i = 0; i < count; ++i, ++it)
			m_HashTable.Insert(*it);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <typename InputIterator>
	void HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Insert(const InputIterator& itFirst, const InputIterator& itLast)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		sizeT count = 0;
		for (InputIterator it = itFirst; itFirst != itLast; ++it)
			++count;
		Reserve(m_HashTable.Size() + count);
		InputIterator it = itFirst;
		for (sizeT i = 0; i < count; ++i, ++it)
			m_HashTable.Insert(*it);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <typename InputIterator>
	void HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Insert(std::initializer_list<Pair<Key, Value>> il)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Reserve(m_HashTable.Size() + il.size());
		const Pair<Key, Value>* it = il.begin();
		for (sizeT i = 0; i < il.size(); ++i, ++it)
			m_HashTable.Insert(*it);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <typename ... Args>
	Pair<typename HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator, b8> HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Emplace(const Key& key, Args&&... args)
	{
		return m_HashTable(Forward<KeyValuePair>(KeyValuePair(key, Forward<Value>(Value(args...)))));
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	sizeT HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Erase(const Key& key)
	{
		Iterator it = m_HashTable.FindKey(key);
		if (it == m_HashTable.Back())
			return 0;
		m_HashTable.Erase(it);
		return 1;
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	typename HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Erase(const Iterator& it)
	{
		return m_HashTable.Erase(it);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	typename HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Erase(const Iterator& itFirst, const Iterator& itLast)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		for (Iterator it = itFirst; it != itLast; ++it)
			m_HashTable.Erase(it);
		return itLast;
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	void HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Clear()
	{
		m_HashTable.Clear();
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	typename HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Find(const Key& key) const
	{
		return m_HashTable.FindKey(key);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <typename U, typename Pred>
	typename HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::FindAs(const U& val, const Pred& pred) const
	{
		return m_HashTable.FindAs(val, pred);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	sizeT HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Count(const Key& key) const
	{
		return m_HashTable.CountKey(key);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	void HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Rehash(sizeT buckets)
	{
		m_HashTable.Rehash(buckets);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	void HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Reserve(sizeT size)
	{
		m_HashTable.Reserve(size);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	void HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Swap(HashMap& map)
	{
		m_HashTable.Swap(map.m_HashTable);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	sizeT HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::BucketCount() const
	{
		return m_HashTable.BucketCount();
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	sizeT HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::BucketSize(sizeT bucket) const
	{
		return m_HashTable.BucketSize(bucket);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	sizeT HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Bucket(const Key& key) const
	{
		return m_HashTable.BucketKey(key);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	sizeT HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::LoadFactor() const
	{
		return m_HashTable.LoadFactor();
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	void HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::MaxLoadFactor(sizeT maxLoadFactor)
	{
		m_HashTable.MaxLoadFactor(maxLoadFactor);
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	sizeT HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::MaxLoadFactor() const
	{
		return m_HashTable.MaxLoadFactor();
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	b8 HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::IsEmpty() const
	{
		return m_HashTable.IsEmpty();
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	sizeT HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Size() const
	{
		return m_HashTable.Size();
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	Memory::IAllocator* HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::GetAllocator() const
	{
		return m_HashTable.GetAllocator();
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	typename HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Front() const
	{
		return m_HashTable.Front();
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	typename HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Last() const
	{
		return m_HashTable.Last();
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	typename HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Back() const
	{
		return m_HashTable.Back();
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	typename HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::begin() const
	{
		return m_HashTable.begin();
	}

	template <typename Key, typename Value, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	typename HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator HashMap<Key, Value, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::end() const
	{
		return m_HashTable.end();
	}

}