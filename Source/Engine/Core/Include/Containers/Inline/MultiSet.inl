// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// MultiSet.h: Ordered multi-set
#pragma once

#include "Containers/MultiSet.h"

namespace Hv {

	template <typename T, typename CompLess>
	HV_FORCE_INL MultiSet<T, CompLess>::MultiSet()
		: m_Tree(g_pAllocator)
	{
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL MultiSet<T, CompLess>::MultiSet(Memory::IAllocator* pAlloc)
		: m_Tree(pAlloc)
	{
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL MultiSet<T, CompLess>::MultiSet(const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_Tree(comp, pAlloc)
	{
	}

	template <typename T, typename CompLess>
	template <sizeT N>
	HV_INL MultiSet<T, CompLess>::MultiSet(T(&arr)[N], Memory::IAllocator* pAlloc)
		: m_Tree(pAlloc)
	{
		for (sizeT i = 0; i < N; ++i)
			m_Tree.Insert(arr[i]);
	}

	template <typename T, typename CompLess>
	template <sizeT N>
	HV_INL MultiSet<T, CompLess>::MultiSet(T(&arr)[N], const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_Tree(comp, pAlloc)
	{
		for (sizeT i = 0; i < N; ++i)
			m_Tree.Insert(arr[i]);
	}

	template <typename T, typename CompLess>
	HV_INL MultiSet<T, CompLess>::MultiSet(const T* arr, sizeT count, Memory::IAllocator* pAlloc)
		: m_Tree(pAlloc)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		for (sizeT i = 0; i < count; ++i)
			m_Tree.Insert(arr[i]);
	}

	template <typename T, typename CompLess>
	HV_INL MultiSet<T, CompLess>::MultiSet(const T* arr, sizeT count, const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_Tree(comp, pAlloc)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		for (sizeT i = 0; i < count; ++i)
			m_Tree.Insert(arr[i]);
	}

	template <typename T, typename CompLess>
	HV_INL MultiSet<T, CompLess>::MultiSet(const T* itFirst, const T* itLast, Memory::IAllocator* pAlloc)
		: m_Tree(pAlloc)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		for (const T* it = itFirst; it != itLast; ++it)
			m_Tree.Insert(*it);
	}

	template <typename T, typename CompLess>
	HV_INL MultiSet<T, CompLess>::MultiSet(const T* itFirst, const T* itLast, const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_Tree(comp, pAlloc)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		for (const T* it = itFirst; it != itLast; ++it)
			m_Tree.Insert(*it);
	}

	template <typename T, typename CompLess>
	template <typename InputIterator>
	HV_INL MultiSet<T, CompLess>::MultiSet(const InputIterator& itFirst, const InputIterator& itLast, Memory::IAllocator* pAlloc)
		: m_Tree(pAlloc)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		for (Iterator it = itFirst; it != itLast; ++it)
			m_Tree.Insert(*it);
	}

	template <typename T, typename CompLess>
	template <typename InputIterator>
	HV_INL MultiSet<T, CompLess>::MultiSet(const InputIterator& itFirst, const InputIterator& itLast, const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_Tree(comp, pAlloc)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		for (Iterator it = itFirst; it != itLast; ++it)
			m_Tree.Insert(*it);
	}

	template <typename T, typename CompLess>
	HV_INL MultiSet<T, CompLess>::MultiSet(std::initializer_list<T> il, Memory::IAllocator* pAlloc)
		: m_Tree(pAlloc)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		for (const T* it = il.begin(); it != il.end(); ++it)
			m_Tree.Insert(*it);
	}

	template <typename T, typename CompLess>
	HV_INL MultiSet<T, CompLess>::MultiSet(std::initializer_list<T> il, const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_Tree(comp, pAlloc)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		for (const T* it = il.begin(); it != il.end(); ++it)
			m_Tree.Insert(*it);
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL MultiSet<T, CompLess>::MultiSet(const MultiSet& multiset)
		: m_Tree(multiset.m_Tree)
	{
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL MultiSet<T, CompLess>::MultiSet(const MultiSet& multiset, Memory::IAllocator* pAlloc)
		: m_Tree(multiset.m_Tree, pAlloc)
	{
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL MultiSet<T, CompLess>::MultiSet(MultiSet&& multiset) noexcept
		: m_Tree(Forward<BaseType>(multiset.m_Tree))
	{
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL MultiSet<T, CompLess>::~MultiSet()
	{
	}

	template <typename T, typename CompLess>
	template <sizeT N>
	HV_INL MultiSet<T, CompLess>& MultiSet<T, CompLess>::operator=(T(&arr)[N])
	{
		m_Tree.Clear();
		for (sizeT i = 0; i < N; ++i)
			m_Tree.Insert(arr[i]);
		return *this;
	}

	template <typename T, typename CompLess>
	HV_INL MultiSet<T, CompLess>& MultiSet<T, CompLess>::operator=(std::initializer_list<T> il)
	{
		m_Tree.Clear();
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		for (const T* it = il.begin(); it != il.end(); ++it)
			m_Tree.Insert(*it);
		return *this;
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL MultiSet<T, CompLess>& MultiSet<T, CompLess>::operator=(const MultiSet& multiset)
	{
		m_Tree = multiset.m_Tree;
		return *this;
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL MultiSet<T, CompLess>& MultiSet<T, CompLess>::operator=(MultiSet&& multiset) noexcept
	{
		m_Tree = Forward<BaseType>(multiset.m_Tree);
		return *this;
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL typename MultiSet<T, CompLess>::Iterator MultiSet<T, CompLess>::Insert(const T& val)
	{
		return m_Tree.Insert(val).first;
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL typename MultiSet<T, CompLess>::Iterator MultiSet<T, CompLess>::Insert(T&& val)
	{
		return m_Tree.Insert(Forward<T>(val)).first;
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL typename MultiSet<T, CompLess>::Iterator MultiSet<T, CompLess>::Insert(const Iterator& hint, const T& val)
	{
		return m_Tree.Insert(hint, val).first;
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL typename MultiSet<T, CompLess>::Iterator MultiSet<T, CompLess>::Insert(const Iterator& hint, T&& val)
	{
		return m_Tree.Insert(hint, Forward<T>(val)).first;
	}

	template <typename T, typename CompLess>
	template <sizeT N>
	HV_FORCE_INL void MultiSet<T, CompLess>::Insert(T(&arr)[N])
	{
		for (sizeT i = 0; i < N; ++i)
			m_Tree.Insert(arr[i]);
	}

	template <typename T, typename CompLess>
	HV_INL void MultiSet<T, CompLess>::Insert(const T* arr, sizeT count)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		for (sizeT i = 0; i < count; ++i)
			m_Tree.Insert(arr[i]);
	}

	template <typename T, typename CompLess>
	HV_INL void MultiSet<T, CompLess>::Insert(const T* itFirst, const T* itLast)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		for (const T* it = itFirst; it != itLast; ++it)
			m_Tree.Insert(*it);
	}

	template <typename T, typename CompLess>
	template <typename InputIterator>
	HV_INL void MultiSet<T, CompLess>::Insert(const InputIterator& itFirst, const InputIterator& itLast)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		for (Iterator it = itFirst; it != itLast; ++it)
			m_Tree.Insert(*it);
	}

	template <typename T, typename CompLess>
	template <typename InputIterator>
	HV_INL void MultiSet<T, CompLess>::Insert(std::initializer_list<T> il)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		for (const T* it = il.begin(); it != il.end(); ++it)
			m_Tree.Insert(*it);
	}

	template <typename T, typename CompLess>
	template <typename ... Args>
	HV_FORCE_INL typename MultiSet<T, CompLess>::Iterator MultiSet<T, CompLess>::Emplace(Args&&... args)
	{
		return m_Tree.Emplace(Forward<Args...>(args...)).first;
	}

	template <typename T, typename CompLess>
	template <typename ... Args>
	HV_FORCE_INL typename MultiSet<T, CompLess>::Iterator MultiSet<T, CompLess>::EmplaceHint(const Iterator& hint, Args&&... args)
	{
		return m_Tree.EmplaceHint(hint, Forward<Args...>(args...)).first;
	}

	template <typename T, typename CompLess>
	HV_INL sizeT MultiSet<T, CompLess>::Erase(const T& val)
	{
		sizeT count = 0;
		while (true)
		{
			Iterator it = m_Tree.Find(val);
			if (it != m_Tree.Back())
			{
				return count;
			}
			m_Tree.Erase(it);
			++count;
		}
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL typename MultiSet<T, CompLess>::Iterator MultiSet<T, CompLess>::Erase(const Iterator& it)
	{
		return m_Tree.Erase(it);
	}

	template <typename T, typename CompLess>
	HV_INL typename MultiSet<T, CompLess>::Iterator MultiSet<T, CompLess>::Erase(const Iterator& itFirst, const Iterator& itLast)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		for (Iterator it = itFirst; it != itLast; ++it)
			m_Tree.Erase(it);
		return itLast;
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL void MultiSet<T, CompLess>::Clear()
	{
		m_Tree.Clear();
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL typename MultiSet<T, CompLess>::Iterator MultiSet<T, CompLess>::Find(const T& val) const
	{
		return m_Tree.Find(val);
	}

	template <typename T, typename CompLess>
	template <typename U, typename Pred>
	HV_FORCE_INL typename MultiSet<T, CompLess>::Iterator MultiSet<T, CompLess>::FindAs(const U& val, const Pred& pred) const
	{
		return m_Tree.FindAs(val, pred);
	}

	template <typename T, typename CompLess>
	sizeT MultiSet<T, CompLess>::Count(const T& val) const
	{
		return m_Tree.Count(val);
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL typename MultiSet<T, CompLess>::Iterator MultiSet<T, CompLess>::LowerBound(const T& val) const
	{
		return m_Tree.LowerBound(val);
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL typename MultiSet<T, CompLess>::Iterator MultiSet<T, CompLess>::UpperBound(const T& val) const
	{
		return m_Tree.UpperBound(val);
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL Pair<typename MultiSet<T, CompLess>::Iterator, typename MultiSet<T, CompLess>::Iterator> MultiSet<T, CompLess>::EqualRange(const T& val) const
	{
		return Pair<Iterator, Iterator>(m_Tree.LowerBound(val), m_Tree.UpperBound(val));
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL void MultiSet<T, CompLess>::Swap(MultiSet& multiset)
	{
		m_Tree.Swap(multiset.m_Tree);
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL b8 MultiSet<T, CompLess>::IsEmpty() const
	{
		return m_Tree.IsEmpty();
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL sizeT MultiSet<T, CompLess>::Size() const
	{
		return m_Tree.Size();
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL Memory::IAllocator* MultiSet<T, CompLess>::GetAllocator() const
	{
		return m_Tree.GetAllocator();
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL typename MultiSet<T, CompLess>::Iterator MultiSet<T, CompLess>::Front() const
	{
		return m_Tree.Front();
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL typename MultiSet<T, CompLess>::Iterator MultiSet<T, CompLess>::Last() const
	{
		return m_Tree.Last();
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL typename MultiSet<T, CompLess>::Iterator MultiSet<T, CompLess>::Back() const
	{
		return m_Tree.Back();
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL typename MultiSet<T, CompLess>::Iterator MultiSet<T, CompLess>::begin() const
	{
		return m_Tree.begin();
	}

	template <typename T, typename CompLess>
	HV_FORCE_INL typename MultiSet<T, CompLess>::Iterator MultiSet<T, CompLess>::end() const
	{
		return m_Tree.end();
	}
	
}