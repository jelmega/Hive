// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Map.inl: Ordered multi map
#pragma once
#include "Containers/MultiMap.h"

namespace Hv {

	template <typename Key, typename Value, typename CompLess>
	MultiMap<Key, Value, CompLess>::MultiMap()
		: m_Tree(g_pAllocator)
	{
	}

	template <typename Key, typename Value, typename CompLess>
	MultiMap<Key, Value, CompLess>::MultiMap(Memory::IAllocator* pAlloc)
		: m_Tree(pAlloc)
	{
	}

	template <typename Key, typename Value, typename CompLess>
	MultiMap<Key, Value, CompLess>::MultiMap(const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_Tree(comp, pAlloc)
	{
	}

	template <typename Key, typename Value, typename CompLess>
	template <sizeT N>
	MultiMap<Key, Value, CompLess>::MultiMap(Pair<Key, Value>(&arr)[N], Memory::IAllocator* pAlloc)
		: m_Tree(pAlloc)
	{
		for (sizeT i = 0; i < N; ++i)
			m_Tree.Insert(arr[i]);
	}

	template <typename Key, typename Value, typename CompLess>
	template <sizeT N>
	MultiMap<Key, Value, CompLess>::MultiMap(Pair<Key, Value>(&arr)[N], const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_Tree(comp, pAlloc)
	{
		for (sizeT i = 0; i < N; ++i)
			m_Tree.Insert(arr[i]);
	}

	template <typename Key, typename Value, typename CompLess>
	MultiMap<Key, Value, CompLess>::MultiMap(const Pair<Key, Value>* arr, sizeT count, Memory::IAllocator* pAlloc)
		: m_Tree(pAlloc)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		for (sizeT i = 0; i < count; ++i)
			m_Tree.Insert(arr[i]);
	}

	template <typename Key, typename Value, typename CompLess>
	MultiMap<Key, Value, CompLess>::MultiMap(const Pair<Key, Value>* arr, sizeT count, const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_Tree(comp, pAlloc)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		for (sizeT i = 0; i < count; ++i)
			m_Tree.Insert(arr[i]);
	}

	template <typename Key, typename Value, typename CompLess>
	MultiMap<Key, Value, CompLess>::MultiMap(const Pair<Key, Value>* itFirst, const Pair<Key, Value>* itLast, Memory::IAllocator* pAlloc)
		: m_Tree(pAlloc)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		for (const Pair<Key, Value>* it = itFirst; it != itLast; ++it)
			m_Tree.Insert(*it);
	}

	template <typename Key, typename Value, typename CompLess>
	MultiMap<Key, Value, CompLess>::MultiMap(const Pair<Key, Value>* itFirst, const Pair<Key, Value>* itLast, const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_Tree(comp, pAlloc)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		for (const Pair<Key, Value>* it = itFirst; it != itLast; ++it)
			m_Tree.Insert(*it);
	}

	template <typename Key, typename Value, typename CompLess>
	template <typename InputIterator>
	MultiMap<Key, Value, CompLess>::MultiMap(const InputIterator& itFirst, const InputIterator& itLast, Memory::IAllocator* pAlloc)
		: m_Tree(pAlloc)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		for (Iterator it = itFirst; it != itLast; ++it)
			m_Tree.Insert(*it);
	}

	template <typename Key, typename Value, typename CompLess>
	template <typename InputIterator>
	MultiMap<Key, Value, CompLess>::MultiMap(const InputIterator& itFirst, const InputIterator& itLast, const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_Tree(comp, pAlloc)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		for (Iterator it = itFirst; it != itLast; ++it)
			m_Tree.Insert(*it);
	}

	template <typename Key, typename Value, typename CompLess>
	MultiMap<Key, Value, CompLess>::MultiMap(std::initializer_list<Pair<Key, Value>> il, Memory::IAllocator* pAlloc)
		: m_Tree(pAlloc)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		for (const Pair<Key, Value>* it = il.begin(); it != il.end(); ++it)
			m_Tree.Insert(*it);
	}

	template <typename Key, typename Value, typename CompLess>
	MultiMap<Key, Value, CompLess>::MultiMap(std::initializer_list<Pair<Key, Value>> il, const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_Tree(comp, pAlloc)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		for (const Pair<Key, Value>* it = il.begin(); it != il.end(); ++it)
			m_Tree.Insert(*it);
	}

	template <typename Key, typename Value, typename CompLess>
	MultiMap<Key, Value, CompLess>::MultiMap(const MultiMap& multimap)
		: m_Tree(multimap.m_Tree)
	{
	}

	template <typename Key, typename Value, typename CompLess>
	MultiMap<Key, Value, CompLess>::MultiMap(const MultiMap& multimap, Memory::IAllocator* pAlloc)
		: m_Tree(multimap.m_Tree, pAlloc)
	{
	}

	template <typename Key, typename Value, typename CompLess>
	MultiMap<Key, Value, CompLess>::MultiMap(MultiMap&& multimap) noexcept
		: m_Tree(Forward<BaseType>(multimap.m_Tree))
	{
	}

	template <typename Key, typename Value, typename CompLess>
	MultiMap<Key, Value, CompLess>::~MultiMap()
	{
	}

	template <typename Key, typename Value, typename CompLess>
	template <sizeT N>
	MultiMap<Key, Value, CompLess>& MultiMap<Key, Value, CompLess>::operator=(Pair<Key, Value>(&arr)[N])
	{
		m_Tree.Clear();
		for (sizeT i = 0; i < N; ++i)
			m_Tree.Insert(arr[i]);
		return *this;
	}

	template <typename Key, typename Value, typename CompLess>
	MultiMap<Key, Value, CompLess>& MultiMap<Key, Value, CompLess>::operator=(std::initializer_list<Pair<Key, Value>> il)
	{
		m_Tree.Clear();
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		for (const Pair<Key, Value>* it = il.begin(); it != il.end(); ++it)
			m_Tree.Insert(*it);
		return *this;
	}

	template <typename Key, typename Value, typename CompLess>
	MultiMap<Key, Value, CompLess>& MultiMap<Key, Value, CompLess>::operator=(const MultiMap& multimap)
	{
		m_Tree = multimap.m_Tree;
		return *this;
	}

	template <typename Key, typename Value, typename CompLess>
	MultiMap<Key, Value, CompLess>& MultiMap<Key, Value, CompLess>::operator=(MultiMap&& multimap) noexcept
	{
		m_Tree = Forward<BaseType>(multimap.m_Tree);
		return *this;
	}

	template <typename Key, typename Value, typename CompLess>
	Pair<typename MultiMap<Key, Value, CompLess>::Iterator, b8> MultiMap<Key, Value, CompLess>::Insert(const Pair<Key, Value>& pair)
	{
		return m_Tree.Insert(pair);
	}

	template <typename Key, typename Value, typename CompLess>
	Pair<typename MultiMap<Key, Value, CompLess>::Iterator, b8> MultiMap<Key, Value, CompLess>::Insert(const Key& key, const Value& value)
	{
		return m_Tree.Insert(Forward<KeyValuePair>(Pair<Key, Value>(key, value)));
	}

	template <typename Key, typename Value, typename CompLess>
	Pair<typename MultiMap<Key, Value, CompLess>::Iterator, b8> MultiMap<Key, Value, CompLess>::Insert(Pair<Key, Value>&& pair)
	{
		return m_Tree.Insert(Forward<KeyValuePair>(pair));
	}

	template <typename Key, typename Value, typename CompLess>
	Pair<typename MultiMap<Key, Value, CompLess>::Iterator, b8> MultiMap<Key, Value, CompLess>::Insert(Key&& key, Value&& value)
	{
		return m_Tree.Insert(Forward<KeyValuePair>(KeyValuePair(Forward<Key>(key), Forward<Value>(value))));
	}

	template <typename Key, typename Value, typename CompLess>
	Pair<typename MultiMap<Key, Value, CompLess>::Iterator, b8> MultiMap<Key, Value, CompLess>::Insert(const Iterator& hint, const Pair<Key, Value>& pair)
	{
		return m_Tree.Insert(hint, pair);
	}

	template <typename Key, typename Value, typename CompLess>
	Pair<typename MultiMap<Key, Value, CompLess>::Iterator, b8> MultiMap<Key, Value, CompLess>::Insert(const Iterator& hint, const Key& key, const Value& value)
	{
		return m_Tree.Insert(hint, Forward<KeyValuePair>(Pair<Key, Value>(key, value)));
	}

	template <typename Key, typename Value, typename CompLess>
	Pair<typename MultiMap<Key, Value, CompLess>::Iterator, b8> MultiMap<Key, Value, CompLess>::Insert(const Iterator& hint, Pair<Key, Value>&& pair)
	{
		return m_Tree.Insert(hint, Forward<KeyValuePair>(pair));
	}

	template <typename Key, typename Value, typename CompLess>
	Pair<typename MultiMap<Key, Value, CompLess>::Iterator, b8> MultiMap<Key, Value, CompLess>::Insert(const Iterator& hint, Key&& key, Value&& value)
	{
		return m_Tree.Insert(hint, Forward<KeyValuePair>(KeyValuePair(Forward<Key>(key), Forward<Value>(value))));
	}

	template <typename Key, typename Value, typename CompLess>
	template <sizeT N>
	void MultiMap<Key, Value, CompLess>::Insert(Pair<Key, Value>(&arr)[N])
	{
		for (sizeT i = 0; i < N; ++i)
			m_Tree.Insert(arr[i]);
	}

	template <typename Key, typename Value, typename CompLess>
	void MultiMap<Key, Value, CompLess>::Insert(const Pair<Key, Value>* arr, sizeT count)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		for (sizeT i = 0; i < count; ++i)
			m_Tree.Insert(arr[i]);
	}

	template <typename Key, typename Value, typename CompLess>
	void MultiMap<Key, Value, CompLess>::Insert(const Pair<Key, Value>* itFirst, const Pair<Key, Value>* itLast)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		for (const Pair<Key, Value>* it = itFirst; it != itLast; ++it)
			m_Tree.Insert(*it);
	}

	template <typename Key, typename Value, typename CompLess>
	template <typename InputIterator>
	void MultiMap<Key, Value, CompLess>::Insert(const InputIterator& itFirst, const InputIterator& itLast)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		for (Iterator it = itFirst; it != itLast; ++it)
			m_Tree.Insert(*it);
	}

	template <typename Key, typename Value, typename CompLess>
	template <typename InputIterator>
	void MultiMap<Key, Value, CompLess>::Insert(std::initializer_list<Pair<Key, Value>> il)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		for (const Pair<Key, Value>* it = il.begin(); it != il.end(); ++it)
			m_Tree.Insert(*it);
	}

	template <typename Key, typename Value, typename CompLess>
	template <typename ... Args>
	Pair<typename MultiMap<Key, Value, CompLess>::Iterator, b8> MultiMap<Key, Value, CompLess>::Emplace(const Key& key, Args&&... args)
	{
		return m_Tree.Insert(Forward<KeyValuePair>(KeyValuePair(key, Forward<Value>(Value(args...)))));
	}

	template <typename Key, typename Value, typename CompLess>
	template <typename ... Args>
	Pair<typename MultiMap<Key, Value, CompLess>::Iterator, b8> MultiMap<Key, Value, CompLess>::EmplaceHint(const Iterator& hint, const Key& key, Args&&... args)
	{
		return m_Tree.Insert(hint, Forward<KeyValuePair>(KeyValuePair(key, Forward<Value>(Value(args...)))));
	}

	template <typename Key, typename Value, typename CompLess>
	sizeT MultiMap<Key, Value, CompLess>::Erase(const Key& key)
	{
		sizeT count = 0;
		while (true)
		{
			Iterator it = m_Tree.FindKey(key);
			if (it != m_Tree.Back())
			{
				return count;
			}
			m_Tree.Erase(it);
			++count;
		}
	}

	template <typename Key, typename Value, typename CompLess>
	typename MultiMap<Key, Value, CompLess>::Iterator MultiMap<Key, Value, CompLess>::Erase(const Iterator& it)
	{
		return m_Tree.Erase(it);
	}

	template <typename Key, typename Value, typename CompLess>
	typename MultiMap<Key, Value, CompLess>::Iterator MultiMap<Key, Value, CompLess>::Erase(const Iterator& itFirst, const Iterator& itLast)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		for (Iterator it = itFirst; it != itLast; ++it)
			m_Tree.Erase(it);
		return itLast;
	}

	template <typename Key, typename Value, typename CompLess>
	void MultiMap<Key, Value, CompLess>::Clear()
	{
		m_Tree.Clear();
	}

	template <typename Key, typename Value, typename CompLess>
	typename MultiMap<Key, Value, CompLess>::Iterator MultiMap<Key, Value, CompLess>::Find(const Key& key) const
	{
		return m_Tree.FindKey(key);
	}

	template <typename Key, typename Value, typename CompLess>
	template <typename U, typename Pred>
	typename MultiMap<Key, Value, CompLess>::Iterator MultiMap<Key, Value, CompLess>::FindAs(const U& val, const Pred& pred) const
	{
		return m_Tree.FindAs(val, pred);
	}

	template <typename Key, typename Value, typename CompLess>
	sizeT MultiMap<Key, Value, CompLess>::Count(const Key& key) const
	{
		return m_Tree.Count(key);
	}

	template <typename Key, typename Value, typename CompLess>
	typename MultiMap<Key, Value, CompLess>::Iterator MultiMap<Key, Value, CompLess>::LowerBound(const Key& key) const
	{
		return m_Tree.LowerBoundKey(key);
	}

	template <typename Key, typename Value, typename CompLess>
	typename MultiMap<Key, Value, CompLess>::Iterator MultiMap<Key, Value, CompLess>::UpperBound(const Key& key) const
	{
		return m_Tree.UpperBoundKey(key);
	}

	template <typename Key, typename Value, typename CompLess>
	Pair<typename MultiMap<Key, Value, CompLess>::Iterator, typename MultiMap<Key, Value, CompLess>::Iterator> MultiMap<Key, Value, CompLess>::EqualRange(const Key& key) const
	{
		return Pair<Iterator, Iterator>(m_Tree.LowerBoundKey(key), m_Tree.UpperBoundKey(key));
	}

	template <typename Key, typename Value, typename CompLess>
	void MultiMap<Key, Value, CompLess>::Swap(MultiMap& multimap)
	{
		m_Tree.Swap(multimap.m_Tree);
	}

	template <typename Key, typename Value, typename CompLess>
	b8 MultiMap<Key, Value, CompLess>::IsEmpty() const
	{
		return m_Tree.IsEmpty();
	}

	template <typename Key, typename Value, typename CompLess>
	sizeT MultiMap<Key, Value, CompLess>::Size() const
	{
		return m_Tree.Size();
	}

	template <typename Key, typename Value, typename CompLess>
	Memory::IAllocator* MultiMap<Key, Value, CompLess>::GetAllocator() const
	{
		return m_Tree.GetAllocator();
	}

	template <typename Key, typename Value, typename CompLess>
	typename MultiMap<Key, Value, CompLess>::Iterator MultiMap<Key, Value, CompLess>::Front() const
	{
		return m_Tree.Front();
	}

	template <typename Key, typename Value, typename CompLess>
	typename MultiMap<Key, Value, CompLess>::Iterator MultiMap<Key, Value, CompLess>::Last() const
	{
		return m_Tree.Last();
	}

	template <typename Key, typename Value, typename CompLess>
	typename MultiMap<Key, Value, CompLess>::Iterator MultiMap<Key, Value, CompLess>::Back() const
	{
		return m_Tree.Back();
	}

	template <typename Key, typename Value, typename CompLess>
	typename MultiMap<Key, Value, CompLess>::Iterator MultiMap<Key, Value, CompLess>::begin() const
	{
		return m_Tree.begin();
	}

	template <typename Key, typename Value, typename CompLess>
	typename MultiMap<Key, Value, CompLess>::Iterator MultiMap<Key, Value, CompLess>::end() const
	{
		return m_Tree.end();
	}
	
}