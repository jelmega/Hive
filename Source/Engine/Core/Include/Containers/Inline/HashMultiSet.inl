// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HashMultiSet.inl: Hash/unordered multi-set
#pragma once
#include "Containers/HashMultiSet.h"

namespace Hv {
	
	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMultiSet()
		: m_HashTable(g_pAllocator)
	{
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMultiSet(Memory::IAllocator* pAlloc)
		: m_HashTable(pAlloc)
	{
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMultiSet(const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc)
		: m_HashTable(comp, hash, pAlloc)
	{
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <sizeT N>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMultiSet(T(&arr)[N], Memory::IAllocator* pAlloc)
		: m_HashTable(pAlloc)
	{
		Reserve(N);
		for (sizeT i = 0; i < N; ++i)
			m_HashTable.Insert(arr[i]);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <sizeT N>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMultiSet(T(&arr)[N], const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc)
		: m_HashTable(comp, hash, pAlloc)
	{
		Reserve(N);
		for (sizeT i = 0; i < N; ++i)
			m_HashTable.Insert(arr[i]);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMultiSet(const T* arr, sizeT count, Memory::IAllocator* pAlloc)
		: m_HashTable(pAlloc)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Reserve(count);
		for (sizeT i = 0; i < count; ++i)
			m_HashTable.Insert(arr[i]);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMultiSet(const T* arr, sizeT count, const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc)
		: m_HashTable(comp, hash, pAlloc)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Reserve(count);
		for (sizeT i = 0; i < count; ++i)
			m_HashTable.Insert(arr[i]);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMultiSet(const T* itFirst, const T* itLast, Memory::IAllocator* pAlloc)
		: m_HashTable(pAlloc)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		sizeT count = sizeT(itLast - itFirst);
		Reserve(count);
		const T* it = itFirst;
		for (sizeT i = 0; i < count; ++i, ++it)
			m_HashTable.Insert(*it);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMultiSet(const T* itFirst, const T* itLast, const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc)
		: m_HashTable(comp, hash, pAlloc)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		sizeT count = sizeT(itLast - itFirst);
		Reserve(count);
		const T* it = itFirst;
		for (sizeT i = 0; i < count; ++i, ++it)
			m_HashTable.Insert(*it);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <typename InputIterator>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMultiSet(const InputIterator& itFirst, const InputIterator& itLast, Memory::IAllocator* pAlloc)
		: m_HashTable(pAlloc)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		sizeT count = 0;
		for (InputIterator it = itFirst; itFirst != itLast; ++it)
			++count;
		Reserve(count);
		InputIterator it = itFirst;
		for (sizeT i = 0; i < count; ++i, ++it)
			m_HashTable.Insert(*it);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <typename InputIterator>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMultiSet(const InputIterator& itFirst, const InputIterator& itLast, const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc)
		: m_HashTable(comp, hash, pAlloc)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		sizeT count = 0;
		for (InputIterator it = itFirst; itFirst != itLast; ++it)
			++count;
		Reserve(count);
		InputIterator it = itFirst;
		for (sizeT i = 0; i < count; ++i, ++it)
			m_HashTable.Insert(*it);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMultiSet(std::initializer_list<T> il, Memory::IAllocator* pAlloc)
		: m_HashTable(pAlloc)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Reserve(il.size());
		const T* it = il.begin();
		for (sizeT i = 0; i < il.size(); ++i, ++it)
			m_HashTable.Insert(*it);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMultiSet(std::initializer_list<T> il, const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc)
		: m_HashTable(comp, hash, pAlloc)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Reserve(il.size());
		const T* it = il.begin();
		for (sizeT i = 0; i < il.size(); ++i, ++it)
			m_HashTable.Insert(*it);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMultiSet(const HashMultiSet& multiset)
		: m_HashTable(multiset.m_HashTable)
	{
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMultiSet(const HashMultiSet& multiset, Memory::IAllocator* pAlloc)
		: m_HashTable(multiset.m_HashTable, pAlloc)
	{
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::HashMultiSet(HashMultiSet&& multiset) noexcept
		: m_HashTable(Forward<BaseType>(multiset.m_HashTable))
	{
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::~HashMultiSet()
	{
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <sizeT N>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>& HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::operator=(T(&arr)[N])
	{
		m_HashTable.Clear();
		Reserve(N);
		for (sizeT i = 0; i < N; ++i)
			m_HashTable.Insert(arr[i]);
		return *this;
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>& HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::operator=(std::initializer_list<T> il)
	{
		Clear();
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Reserve(il.size());
		const T* it = il.begin();
		for (sizeT i = 0; i < il.size(); ++i, ++it)
			m_HashTable.Insert(*it);
		return *this;
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>& HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::operator=(const HashMultiSet& multiset)
	{
		m_HashTable = multiset.m_HashTable;
		return *this;
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>& HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::operator=(HashMultiSet&& multiset) noexcept
	{
		m_HashTable = Forward<BaseType>(multiset.m_HashTable);
		return *this;
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	Pair<typename HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator, b8> HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Insert(const T& val)
	{
		return m_HashTable.Insert(val);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	Pair<typename HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator, b8> HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Insert(T&& val)
	{
		return m_HashTable.Insert(Forward<T>(val));
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <sizeT N>
	void HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Insert(T(&arr)[N])
	{
		Reserve(m_HashTable.Size() + N);
		for (sizeT i = 0; i < N; ++i)
			m_HashTable.Insert(arr[i]);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	void HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Insert(const T* arr, sizeT count)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Reserve(m_HashTable.Size() + count);
		for (sizeT i = 0; i < count; ++i)
			m_HashTable.Insert(arr[i]);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	void HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Insert(const T* itFirst, const T* itLast)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		sizeT count = sizeT(itLast - itFirst);
		Reserve(m_HashTable.Size() + count);
		const T* it = itFirst;
		for (sizeT i = 0; i < count; ++i, ++it)
			m_HashTable.Insert(*it);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <typename InputIterator>
	void HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Insert(const InputIterator& itFirst, const InputIterator& itLast)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		sizeT count = 0;
		for (InputIterator it = itFirst; itFirst != itLast; ++it)
			++count;
		Reserve(m_HashTable.Size() + count);
		InputIterator it = itFirst;
		for (sizeT i = 0; i < count; ++i, ++it)
			m_HashTable.Insert(*it);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <typename InputIterator>
	void HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Insert(std::initializer_list<T> il)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Reserve(m_HashTable.Size() + il.size());
		const T* it = il.begin();
		for (sizeT i = 0; i < il.size(); ++i, ++it)
			m_HashTable.Insert(*it);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <typename ... Args>
	Pair<typename HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator, b8> HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Emplace(Args&&... args)
	{
		return m_HashTable.Emplace(args...);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	sizeT HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Erase(const T& val)
	{
		sizeT count = 0;
		while (true)
		{
			Iterator it = m_HashTable.Find(val);
			if (it != m_HashTable.Back())
			{
				return count;
			}
			m_HashTable.Erase(it);
			++count;
		}
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	typename HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Erase(const Iterator& it)
	{
		return m_HashTable.Erase(it);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	typename HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Erase(const Iterator& itFirst, const Iterator& itLast)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		for (Iterator it = itFirst; it != itLast; ++it)
			m_HashTable.Erase(it);
		return itLast;
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	void HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Clear()
	{
		m_HashTable.Clear();
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	typename HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Find(const T& val) const
	{
		return m_HashTable.Find(val);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <typename U, typename Pred>
	typename HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::FindAs(const U& val, const Pred& pred) const
	{
		return m_HashTable.FindAs(val, pred);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	sizeT HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Count(const T& val) const
	{
		return m_HashTable.Count(val);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	void HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Rehash(sizeT buckets)
	{
		m_HashTable.Rehash(buckets);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	void HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Reserve(sizeT size)
	{
		m_HashTable.Reserve(size);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	void HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Swap(HashMultiSet& multiset)
	{
		m_HashTable.Swap(multiset.m_HashTable);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	sizeT HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::BucketCount() const
	{
		return m_HashTable.BucketCount();
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	sizeT HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::BucketSize(sizeT bucket) const
	{
		return m_HashTable.BucketSize(bucket);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	sizeT HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Bucket(const T& val) const
	{
		return m_HashTable.Bucket(val);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	sizeT HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::LoadFactor() const
	{
		return m_HashTable.LoadFactor();
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	void HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::MaxLoadFactor(sizeT maxLoadFactor)
	{
		m_HashTable.MaxLoadFactor(maxLoadFactor);
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	sizeT HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::MaxLoadFactor() const
	{
		return m_HashTable.MaxLoadFactor();
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	b8 HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::IsEmpty() const
	{
		return m_HashTable.IsEmpty();
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	sizeT HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Size() const
	{
		return m_HashTable.Size();
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	Memory::IAllocator* HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::GetAllocator() const
	{
		return m_HashTable.GetAllocator();
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	typename HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Front() const
	{
		return m_HashTable.Front();
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	typename HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Last() const
	{
		return m_HashTable.Last();
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	typename HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Back() const
	{
		return m_HashTable.Back();
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	typename HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::begin() const
	{
		return m_HashTable.begin();
	}

	template <typename T, typename CompEqual, typename Hash, b8 CacheHash, Container::GrowthPolicyFunc BucketGrowthPolicy>
	typename HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::Iterator HashMultiSet<T, CompEqual, Hash, CacheHash, BucketGrowthPolicy>::end() const
	{
		return m_HashTable.end();
	}

}