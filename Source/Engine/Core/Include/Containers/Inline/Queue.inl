// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Queue.inl: Queue
#pragma once
#include "Containers/Queue.h"

namespace Hv {
	
	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Queue<T,  SubArraySize, GrowthPolicy>::Queue()
		: m_Container()
	{
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Queue<T,  SubArraySize, GrowthPolicy>::Queue(Memory::IAllocator* pAlloc)
		: m_Container(pAlloc)
	{
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Queue<T, SubArraySize, GrowthPolicy>::Queue(const Queue& queue)
		: m_Container(queue)
	{
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Queue<T, SubArraySize, GrowthPolicy>::Queue(const Queue& queue, Memory::IAllocator* pAlloc)
		: m_Container(queue, pAlloc)
	{
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Queue<T, SubArraySize, GrowthPolicy>::Queue(Queue&& queue) noexcept
		: m_Container(Forward<BaseType>(queue))
	{
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Queue<T,  SubArraySize, GrowthPolicy>::~Queue()
	{
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Queue<T,  SubArraySize, GrowthPolicy>& Queue<T,  SubArraySize, GrowthPolicy>::operator=(const Queue& queue)
	{
		m_Container = queue.m_Container;
		return *this;
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Queue<T,  SubArraySize, GrowthPolicy>& Queue<T,  SubArraySize, GrowthPolicy>::operator=(Queue&& queue) noexcept
	{
		m_Container = Forward<BaseType>(queue.m_Container);
		return *this;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	T& Queue<T, SubArraySize, GrowthPolicy>::Peek()
	{
		HV_ASSERT(m_Container.Size());
		return *m_Container.Front();
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	const T& Queue<T, SubArraySize, GrowthPolicy>::Peek() const
	{
		HV_ASSERT(m_Container.Size());
		return *m_Container.Front();
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Queue<T,  SubArraySize, GrowthPolicy>::Reserve(sizeT size)
	{
		m_Container.Reserve(size);
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Queue<T,  SubArraySize, GrowthPolicy>::Clear(bool resize)
	{
		m_Container.Clear(resize);
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Queue<T,  SubArraySize, GrowthPolicy>::Push(const T& val)
	{
		m_Container.PushBack(val);
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Queue<T,  SubArraySize, GrowthPolicy>::Push(T&& val)
	{
		m_Container.PushBack(Forward<T>(val));
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Queue<T,  SubArraySize, GrowthPolicy>::Pop()
	{
		m_Container.PopFront();
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename ... Args>
	void Queue<T,  SubArraySize, GrowthPolicy>::Emplace(Args&&... args)
	{
		m_Container.EmplaceBack(args...);
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Queue<T,  SubArraySize, GrowthPolicy>::Swap(Queue& queue)
	{
		m_Container.Swap(queue.m_Container);
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	b8 Queue<T,  SubArraySize, GrowthPolicy>::IsEmpty() const
	{
		return m_Container.IsEmpty();
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT Queue<T,  SubArraySize, GrowthPolicy>::Size() const
	{
		return m_Container.Size();
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT Queue<T,  SubArraySize, GrowthPolicy>::Capacity() const
	{
		return m_Container.Capacity();
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Memory::IAllocator* Queue<T,  SubArraySize, GrowthPolicy>::GetAllocator()
	{
		return m_Container.GetAllocator();
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>& Queue<T,  SubArraySize, GrowthPolicy>::GetInternalContainer()
	{
		return m_Container;
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	const Deque<T, SubArraySize, GrowthPolicy>& Queue<T,  SubArraySize, GrowthPolicy>::GetInternalContainer() const
	{
		return m_Container;
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Queue<T, SubArraySize, GrowthPolicy>::Iterator Queue<T,  SubArraySize, GrowthPolicy>::Front()
	{
		return m_Container.Front();
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	const typename Queue<T, SubArraySize, GrowthPolicy>::Iterator Queue<T,  SubArraySize, GrowthPolicy>::Front() const
	{
		return m_Container.Front();
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Queue<T, SubArraySize, GrowthPolicy>::Iterator Queue<T,  SubArraySize, GrowthPolicy>::Back()
	{
		return m_Container.Last();
	}

	template <typename T,  sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	const typename Queue<T, SubArraySize, GrowthPolicy>::Iterator Queue<T,  SubArraySize, GrowthPolicy>::Back() const
	{
		return m_Container.Last();
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Queue<T, SubArraySize, GrowthPolicy>::Iterator Queue<T, SubArraySize, GrowthPolicy>::begin()
	{
		return m_Container.Front();
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	const typename Queue<T, SubArraySize, GrowthPolicy>::Iterator Queue<T, SubArraySize, GrowthPolicy>::begin() const
	{
		return m_Container.Front();
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Queue<T, SubArraySize, GrowthPolicy>::Iterator Queue<T, SubArraySize, GrowthPolicy>::end()
	{
		return m_Container.Back();
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	const typename Queue<T, SubArraySize, GrowthPolicy>::Iterator Queue<T, SubArraySize, GrowthPolicy>::end() const
	{
		return m_Container.Back();
	}

}
