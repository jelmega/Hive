// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Deque.inl: Double ended queue
#pragma once
#include "Containers/Deque.h"

namespace Hv {

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>::Iterator::Iterator()
		: m_Data(nullptr)
		, m_Index(0)
		, m_SubIndex(0)
	{
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>::Iterator::Iterator(const Iterator& it)
		: m_Data(it.m_Data)
		, m_Index(it.m_Index)
		, m_SubIndex(it.m_SubIndex)
	{
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>::Iterator::Iterator(Iterator&& it) noexcept
		: m_Data(it.m_Data)
		, m_Index(it.m_Index)
		, m_SubIndex(it.m_SubIndex)
	{
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	T& Deque<T, SubArraySize, GrowthPolicy>::Iterator::operator*()
	{
		return m_Data[m_Index][m_SubIndex];
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	const T& Deque<T, SubArraySize, GrowthPolicy>::Iterator::operator*() const
	{
		return m_Data[m_Index][m_SubIndex];
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	T* Deque<T, SubArraySize, GrowthPolicy>::Iterator::operator->()
	{
		return &m_Data[m_Index][m_SubIndex];
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	const T* Deque<T, SubArraySize, GrowthPolicy>::Iterator::operator->() const
	{
		return &m_Data[m_Index][m_SubIndex];
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Deque<T, SubArraySize, GrowthPolicy>::Iterator& Deque<T, SubArraySize, GrowthPolicy>::Iterator::operator++()
	{
		++m_SubIndex;
		if (m_SubIndex == SubArraySize)
		{
			m_SubIndex = 0;
			++m_Index;
		}
		return *this;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Deque<T, SubArraySize, GrowthPolicy>::Iterator Deque<T, SubArraySize, GrowthPolicy>::Iterator::operator++(int)
	{
		Iterator it(*this);
		++m_SubIndex;
		if (m_SubIndex == SubArraySize)
		{
			m_SubIndex = 0;
			++m_Index;
		}
		return it;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Deque<T, SubArraySize, GrowthPolicy>::Iterator& Deque<T, SubArraySize, GrowthPolicy>::Iterator::operator--()
	{
		--m_SubIndex;
		if (m_SubIndex == 0)
		{
			m_SubIndex = SubArraySize - 1;
			HV_ASSERT_MSG(m_Index > 0, "Can't decrement iterator!");
			--m_Index;
		}
		return *this;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Deque<T, SubArraySize, GrowthPolicy>::Iterator Deque<T, SubArraySize, GrowthPolicy>::Iterator::operator--(int)
	{
		Iterator it(*this);
		--m_SubIndex;
		if (m_SubIndex == 0)
		{
			m_SubIndex = SubArraySize - 1;
			HV_ASSERT_MSG(m_Index > 0, "Can't decrement iterator!");
			--m_Index;
		}
		return it;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Deque<T, SubArraySize, GrowthPolicy>::Iterator Deque<T, SubArraySize, GrowthPolicy>::Iterator::operator+(sizeT count)
	{
		sizeT idxInc = count / SubArraySize;
		sizeT mod = count - idxInc * SubArraySize;

		Iterator it(*this);
		it.m_Index += idxInc;
		it.m_SubIndex += mod;
		if (it.m_SubIndex >= SubArraySize)
		{
			it.m_SubIndex -= SubArraySize;
			++it.m_Index;
		}
		return it;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Deque<T, SubArraySize, GrowthPolicy>::Iterator Deque<T, SubArraySize, GrowthPolicy>::Iterator::operator-(sizeT count)
	{
		HV_ASSERT_MSG(m_Index * SubArraySize + m_SubIndex >= count, "Can't decrement iterator!");
		sizeT idxInc = count / SubArraySize;
		sizeT mod = count - idxInc * SubArraySize;

		Iterator it(*this);
		it.m_Index -= idxInc;
		if (it.m_SubIndex < mod)
		{
			it.m_SubIndex = SubArraySize + it.m_SubIndex - mod;
			--it.m_Index;
		}
		else
			it.m_SubIndex -= mod;
		return it;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Deque<T, SubArraySize, GrowthPolicy>::Iterator& Deque<T, SubArraySize, GrowthPolicy>::Iterator::operator=(const Iterator& it)
	{
		m_Data = it.m_Data;
		m_Index = it.m_Index;
		m_SubIndex = it.m_SubIndex;
		return *this;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Deque<T, SubArraySize, GrowthPolicy>::Iterator& Deque<T, SubArraySize, GrowthPolicy>::Iterator::operator=(Iterator&& it) noexcept
	{
		m_Data = it.m_Data;
		m_Index = it.m_Index;
		m_SubIndex = it.m_SubIndex;
		return *this;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Deque<T, SubArraySize, GrowthPolicy>::Iterator& Deque<T, SubArraySize, GrowthPolicy>::Iterator::operator+=(sizeT count)
	{
		sizeT idxInc = count / SubArraySize;
		sizeT mod = count - idxInc * SubArraySize;

		m_Index += idxInc;
		m_SubIndex += mod;
		if (m_SubIndex >= SubArraySize)
		{
			m_SubIndex -= SubArraySize;
			++m_Index;
		}
		return *this;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Deque<T, SubArraySize, GrowthPolicy>::Iterator& Deque<T, SubArraySize, GrowthPolicy>::Iterator::operator-=(sizeT count)
	{
		sizeT idxInc = count / SubArraySize;
		sizeT mod = count - idxInc * SubArraySize;

		HV_ASSERT_MSG(m_Index > idxInc, "Can't decrement iterator!");
		m_Index -= idxInc;
		if (m_SubIndex < mod)
		{
			m_SubIndex = SubArraySize + m_SubIndex - mod;
			HV_ASSERT_MSG(m_Index > 0, "Can't decrement iterator!");
			--m_Index;
		}
		else
			m_SubIndex -= mod;
		return *this;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	b8 Deque<T, SubArraySize, GrowthPolicy>::Iterator::operator==(const Iterator& it) const
	{
		return m_Data == it.m_Data && m_Index == it.m_Index && m_SubIndex == it.m_SubIndex;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	b8 Deque<T, SubArraySize, GrowthPolicy>::Iterator::operator!=(const Iterator& it) const
	{
		return m_Data != it.m_Data || m_Index != it.m_Index || m_SubIndex != it.m_SubIndex;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>::Iterator::Iterator(T** pData, sizeT index, sizeT subIndex)
		: m_Data(pData)
		, m_Index(index)
		, m_SubIndex(subIndex)
	{
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>::Deque()
		: m_Data(nullptr)
		, m_Front(0)
		, m_UsedSubArrs(0)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(g_pAllocator)
	{
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>::Deque(Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Front(0)
		, m_UsedSubArrs(0)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>::Deque(sizeT capacity, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Front(0)
		, m_UsedSubArrs(0)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(capacity > 0, "Capacity to reserve needs to be bigger than 0!");
		HV_ASSERT_MSG(m_Capacity % SubArraySize == 0, "GrowthPolicy needs to return a multiple of SubArraySize");
		Reserve(capacity);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>::Deque(sizeT count, const T& value, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Front(0)
		, m_UsedSubArrs(0)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		HV_ASSERT_MSG(m_Capacity % SubArraySize == 0, "GrowthPolicy needs to return a multiple of SubArraySize");
		Fill(value, count);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	Deque<T, SubArraySize, GrowthPolicy>::Deque(T(&arr)[N], Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Front(0)
		, m_UsedSubArrs(0)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(m_Capacity % SubArraySize == 0, "GrowthPolicy needs to return a multiple of SubArraySize");
		Assign(arr, N);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>::Deque(const T* arr, sizeT count, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Front(0)
		, m_UsedSubArrs(0)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		HV_ASSERT_MSG(m_Capacity % SubArraySize == 0, "GrowthPolicy needs to return a multiple of SubArraySize");
		Assign(arr, count);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>::Deque(const T* itFirst, const T* itLast, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Front(0)
		, m_UsedSubArrs(0)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		Assign(itFirst, sizeT(itLast - itFirst));
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename InputIterator>
	Deque<T, SubArraySize, GrowthPolicy>::Deque(const InputIterator& itFirst, const InputIterator& itLast, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Front(0)
		, m_UsedSubArrs(0)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		Assign(itFirst, itLast);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>::Deque(std::initializer_list<T> il, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Front(0)
		, m_UsedSubArrs(0)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Assign(il.begin(), il.end());
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc OtherPolicy, sizeT OtherSubArraySize>
	Deque<T, SubArraySize, GrowthPolicy>::Deque(const Deque<T, OtherPolicy, OtherSubArraySize>& deque)
		: m_Data(nullptr)
		, m_Front(0)
		, m_UsedSubArrs(0)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(deque.m_pAlloc)
	{
		Copy(deque);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>::Deque(const Deque& deque)
		: m_Data(nullptr)
		, m_Front(0)
		, m_UsedSubArrs(0)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(deque.m_pAlloc)
	{
		Copy(deque);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc OtherPolicy, sizeT OtherSubArraySize>
	Deque<T, SubArraySize, GrowthPolicy>::Deque(const Deque<T, OtherPolicy, OtherSubArraySize>& deque,
		Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Front(0)
		, m_UsedSubArrs(0)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(deque.m_pAlloc)
	{
		Copy(deque);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>::Deque(const Deque& deque, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Front(0)
		, m_UsedSubArrs(0)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(deque.m_pAlloc)
	{
		Copy(deque);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc OtherPolicy>
	Deque<T, SubArraySize, GrowthPolicy>::Deque(Deque<T, SubArraySize, OtherPolicy>&& deque)
		: m_Data(nullptr)
		, m_Front(0)
		, m_UsedSubArrs(0)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(deque.m_pAlloc)
	{
		Hv::Swap(m_Data, deque.m_Data);
		Hv::Swap(m_Front, deque.m_Front);
		Hv::Swap(m_UsedSubArrs, deque.m_UsedSubArrs);
		Hv::Swap(m_Size, deque.m_Size);
		Hv::Swap(m_Capacity, deque.m_Capacity);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>::Deque(Deque&& deque) noexcept
		: m_Data(nullptr)
		, m_Front(0)
		, m_UsedSubArrs(0)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(deque.m_pAlloc)
	{
		Hv::Swap(m_Data, deque.m_Data);
		Hv::Swap(m_Front, deque.m_Front);
		Hv::Swap(m_UsedSubArrs, deque.m_UsedSubArrs);
		Hv::Swap(m_Size, deque.m_Size);
		Hv::Swap(m_Capacity, deque.m_Capacity);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>::~Deque()
	{
		Clear(true);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	Deque<T, SubArraySize, GrowthPolicy>& Deque<T, SubArraySize, GrowthPolicy>::operator=(T(&arr)[N])
	{
		Assign(arr, N);
		return *this;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>& Deque<T, SubArraySize, GrowthPolicy>::operator=(std::initializer_list<T> il)
	{
		Assign(il.begin(), il.size());
		return *this;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc OtherPolicy, sizeT OtherSubArraySize>
	Deque<T, SubArraySize, GrowthPolicy>& Deque<T, SubArraySize, GrowthPolicy>::operator=(const Deque<T, OtherSubArraySize, OtherPolicy>& deque)
	{
		Copy(deque);
		return *this;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>& Deque<T, SubArraySize, GrowthPolicy>::operator=(const Deque& deque)
	{
		Copy(deque);
		return *this;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc OtherPolicy>
	Deque<T, SubArraySize, GrowthPolicy>& Deque<T, SubArraySize, GrowthPolicy>::operator=(Deque<T, SubArraySize, OtherPolicy>&& deque)
	{
		if (m_pAlloc == deque.m_pAlloc)
		{
			Hv::Swap(m_Data, deque.m_Data);
			Hv::Swap(m_Front, deque.m_Front);
			Hv::Swap(m_UsedSubArrs, deque.m_UsedSubArrs);
			Hv::Swap(m_Size, deque.m_Size);
			Hv::Swap(m_Capacity, deque.m_Capacity);
		}
		else
		{
			Copy(deque);
		}
		return *this;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Deque<T, SubArraySize, GrowthPolicy>& Deque<T, SubArraySize, GrowthPolicy>::operator=(Deque&& deque) noexcept
	{
		if (m_pAlloc == deque.m_pAlloc)
		{
			Hv::Swap(m_Data, deque.m_Data);
			Hv::Swap(m_Front, deque.m_Front);
			Hv::Swap(m_UsedSubArrs, deque.m_UsedSubArrs);
			Hv::Swap(m_Size, deque.m_Size);
			Hv::Swap(m_Capacity, deque.m_Capacity);
		}
		else
		{
			Copy(deque);
		}
		return *this;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	T& Deque<T, SubArraySize, GrowthPolicy>::operator[](sizeT index)
	{
		index += m_Front;
		sizeT arrIdx = index / SubArraySize;
		sizeT subArrIdx = index - arrIdx * SubArraySize;
		return m_Data[arrIdx][subArrIdx];
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	const T& Deque<T, SubArraySize, GrowthPolicy>::operator[](sizeT index) const
	{
		index += m_Front;
		sizeT arrIdx = index / SubArraySize;
		sizeT subArrIdx = index - arrIdx * SubArraySize;
		return m_Data[arrIdx][subArrIdx];
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	T& Deque<T, SubArraySize, GrowthPolicy>::PeekFront()
	{
		HV_ASSERT(m_Size);
		return *Front();
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	const T& Deque<T, SubArraySize, GrowthPolicy>::PeekFront() const
	{
		HV_ASSERT(m_Size);
		return *Front();
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	T& Deque<T, SubArraySize, GrowthPolicy>::PeekBack()
	{
		HV_ASSERT(m_Size);
		return *Last();
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	const T& Deque<T, SubArraySize, GrowthPolicy>::PeekBack() const
	{
		HV_ASSERT(m_Size);
		return *Last();
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Fill(const T& val, sizeT count)
	{
		Clear(false);
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Reserve(count);
		m_Size = count;
		m_UsedSubArrs = (m_Size - 1) / SubArraySize + 1;
		m_Front = 0;
		for (sizeT i = 0; i < m_UsedSubArrs; ++i)
		{
			sizeT icount = count > SubArraySize ? SubArraySize : count;
			T* subArr = m_Data[i];

			if constexpr (Container::UseCopyConstructor<T>)
			{
				for (sizeT j = 0; j < icount; ++j)
					new (subArr + j) T(val);
			}
			else
			{
				for (sizeT j = 0; j < icount; ++j)
					subArr[j] = val;
			}

			count -= SubArraySize;
		}
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	void Deque<T, SubArraySize, GrowthPolicy>::Assign(T(&arr)[N])
	{
		Assign(arr, N);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Assign(T* arr, sizeT count)
	{
		Clear(false);
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Reserve(count);
		m_Size = count;
		m_UsedSubArrs = (m_Size - 1) / SubArraySize + 1;
		m_Front = 0;
		const T* it = arr;
		for (sizeT i = 0; i < m_UsedSubArrs; ++i)
		{
			sizeT icount = count > SubArraySize ? SubArraySize : count;
			T* subArr = m_Data[i];

			if constexpr (Container::UseMemCpy<T>)
			{
				Memory::Copy(subArr, it, icount * sizeof(T));
				it += icount;
			}
			else if constexpr (Container::UseCopyConstructor<T>)
			{
				for (sizeT j = 0; j < icount; ++j, ++it)
					new (subArr + j) T(*it);
			}
			else
			{
				for (sizeT j = 0; j < icount; ++j, ++it)
					subArr[j] = *it;
			}

			count -= SubArraySize;
		}
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Assign(T* itFirst, T* itLast)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		Assign(itFirst, sizeT(itLast - itFirst));
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename InputIterator>
	void Deque<T, SubArraySize, GrowthPolicy>::Assign(const InputIterator& itFirst, const InputIterator& itLast)
	{
		Clear(true);
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		for (InputIterator it = itFirst; it != itLast; ++it)
			++m_Size;
		sizeT count = m_Size;
		Reserve(m_Size);
		m_UsedSubArrs = (m_Size - 1) / SubArraySize + 1;
		m_Front = 0;
		Iterator it = itFirst;
		for (sizeT i = 0; i < m_UsedSubArrs; ++i)
		{
			sizeT icount = count > SubArraySize ? SubArraySize : count;
			T* subArr = m_Data[i];

			if constexpr (Container::UseCopyConstructor<T>)
			{
				for (sizeT j = 0; j < icount; ++j, ++it)
					new (subArr + j) T(*it);
			}
			else
			{
				for (sizeT j = 0; j < icount; ++j, ++it)
					subArr[j] = *it;
			}

			count -= SubArraySize;
		}
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Assign(std::initializer_list<T> il)
	{
		Assign(il.begin(), il.size());
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Resize(sizeT size)
	{
		if (size > m_Size)
			Reserve(size);
		sizeT origSize = m_Size;
		sizeT origUsedSubArrs = m_UsedSubArrs;
		m_Size = size;
		sizeT idx = m_Front + m_Size;
		m_UsedSubArrs = m_Size == 0 ? 0 : (idx - 1) / SubArraySize + 1;

		for (sizeT i = origUsedSubArrs; i >= m_UsedSubArrs; ++i)
			Memory::Clear(m_Data[i], SubArraySize * sizeof(T));

		origSize = (origSize - m_Front) % SubArraySize;
		Memory::Clear(m_Data[m_UsedSubArrs - 1] + origSize, SubArraySize - origSize);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Resize(sizeT size, const T& val)
	{
		if (size > m_Size)
		{
			Reserve(size);
			sizeT idx = m_Front + m_Size;
			sizeT endIdx = (m_Front + size) % SubArraySize;
			sizeT usedSubArrs = (size - 1) / SubArraySize + 1;

			sizeT j = idx % SubArraySize;
			for (sizeT i = m_UsedSubArrs - 1; i < usedSubArrs; ++i)
			{
				T* subArr = m_Data[i];
				if constexpr (Container::UseCopyConstructor<T>)
				{
					for (sizeT count = i == usedSubArrs - 1 ? endIdx : SubArraySize; j < count; ++j)
						new (subArr + j) T(val);
				}
				else
				{
					for (sizeT count = i == usedSubArrs - 1 ? endIdx : SubArraySize; j < count; ++j)
						subArr[j] = val;
				}
				j = 0;
			}
			m_Size = size;
			m_UsedSubArrs = usedSubArrs;
		}
		else if (size < m_Size)
		{
			sizeT origSize = m_Size;
			sizeT origUsedSubArrs = m_UsedSubArrs;
			m_Size = size;
			sizeT idx = m_Front + m_Size;
			m_UsedSubArrs = m_Size == 0 ? 0 : (idx - 1) / SubArraySize + 1;

			for (sizeT i = origUsedSubArrs; i >= m_UsedSubArrs; ++i)
				Memory::Clear(m_Data[i], SubArraySize * sizeof(T));

			origSize = (origSize - m_Front) % SubArraySize;
			Memory::Clear(m_Data[m_UsedSubArrs - 1] + origSize, SubArraySize - origSize);
		}
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Reserve(sizeT size)
	{
		if (size > m_Capacity)
		{
			sizeT cap = GrowthPolicy(size);
			HV_ASSERT_MSG(cap % SubArraySize == 0, "GrowthPolicy needs to return a multiple of SubArraySize");
			T** old = m_Data;
			sizeT numSubArrs = cap / SubArraySize;
			m_Data = (T**)m_pAlloc->Allocate(numSubArrs * sizeof(T*), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			if (old)
			{
				sizeT prevSubArrs = m_Capacity / SubArraySize;
				Memory::Copy(m_Data, old, prevSubArrs * sizeof(T*));
				for (sizeT i = prevSubArrs; i < numSubArrs; ++i)
				{
					m_Data[i] = (T*)m_pAlloc->Allocate(SubArraySize * sizeof(T), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
				}
				m_pAlloc->Free(old);
			}
			else
			{
				for (sizeT i = 0; i < numSubArrs; ++i)
					m_Data[i] = (T*)m_pAlloc->Allocate(SubArraySize * sizeof(T), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			}
			m_Capacity = cap;
		}
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::ShrinkToFit()
	{
		if (m_Size == 0)
		{
			Clear(true);
		}
		else
		{
			sizeT cap = m_UsedSubArrs * SubArraySize;

			if (cap < m_Capacity)
			{
				T** old = m_Data;
				m_Data = (T**)m_pAlloc->Allocate(cap * sizeof(T*), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
				sizeT neededSubArrs = (m_Size - 1) / SubArraySize + 1;
				Memory::Copy(m_Data, old, neededSubArrs * sizeof(T));
				sizeT totalSubArrs = m_Capacity / SubArraySize;
				for (sizeT i = neededSubArrs; i < totalSubArrs; ++i)
					m_pAlloc->Free(old[i]);
			}
		}
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Clear(bool resize)
	{
		if (m_Capacity > 0)
		{
			sizeT j = m_Front;
			sizeT lastIdx = (m_Front + m_Size) % SubArraySize;
			sizeT numSubArrs = (m_Capacity - 1) / SubArraySize + 1;
			for (sizeT i = 0; i < numSubArrs; ++i)
			{
				T* subArr = m_Data[i];
				if (i < m_UsedSubArrs)
				{
					for (sizeT count = i == m_UsedSubArrs - 1 ? lastIdx : SubArraySize; j < count; ++j)
					{
						subArr[j].~T();
					}
					j = 0;
				}
				if (resize)
					m_pAlloc->Free(subArr);
			}
			if (resize)
			{
				m_pAlloc->Free(m_Data);
				m_Data = nullptr;
				m_Capacity = 0;
			}
			m_Size = m_UsedSubArrs = 0;
		}
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::PushBack(const T& val)
	{
		sizeT idx = m_Front + m_Size;
		Reserve(m_Size + 1);
		sizeT mod = idx % SubArraySize;

		if constexpr (Container::UseCopyConstructor<T>)
			new (&m_Data[idx / SubArraySize][mod]) T(val);
		else
			m_Data[idx / SubArraySize][mod] = val;

		++m_Size;
		if (mod == 0)
			++m_UsedSubArrs;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::PushBack(T&& val)
	{
		sizeT idx = m_Front + m_Size;
		Reserve(m_Size + 1);
		sizeT mod = idx % SubArraySize;

		if constexpr (Container::UseCopyConstructor<T>)
			new (&m_Data[idx / SubArraySize][mod]) T(Forward<T>(val));
		else
			m_Data[idx / SubArraySize][mod] = Forward<T>(val);

		++m_Size;
		if (mod == 0)
			++m_UsedSubArrs;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::PopBack()
	{
		if (m_Size > 0)
		{
			--m_Size;
			sizeT idx = m_Front + m_Size;
			sizeT mod = idx % SubArraySize;
			m_Data[idx / SubArraySize][mod].~T();

			if (mod == SubArraySize - 1)
				--m_UsedSubArrs;
		}

	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::PushFront(const T& val)
	{
		sizeT end = m_Front + m_Size;

		if (m_Front == 0)
		{
			Reserve(end + SubArraySize + 1);
			m_Front = SubArraySize - 1;
			sizeT numSubArrs = m_Capacity / SubArraySize - 1;
			m_pAlloc->Free(m_Data[numSubArrs]);
			if (numSubArrs > 0)
				Memory::Move(m_Data + 1, m_Data, numSubArrs * sizeof(T*));
			m_Data[0] = (T*)m_pAlloc->Allocate(SubArraySize * sizeof(T), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			++m_UsedSubArrs;
		}
		else
			--m_Front;

		if constexpr (Container::UseCopyConstructor<T>)
			new (&m_Data[0][m_Front]) T(val);
		else
			m_Data[0][m_Front] = val;

		++m_Size;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::PushFront(T&& val)
	{
		sizeT end = m_Front + m_Size;
		Reserve(end + 1);

		if (m_Front == 0)
		{
			Reserve(end + SubArraySize + 1);
			m_Front = SubArraySize - 1;
			sizeT numSubArrs = m_Capacity / SubArraySize - 1;
			m_pAlloc->Free(m_Data[numSubArrs]);
			if (numSubArrs > 0)
				Memory::Move(m_Data + 1, m_Data, numSubArrs * sizeof(T*));
			m_Data[0] = (T*)m_pAlloc->Allocate(SubArraySize * sizeof(T), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			++m_UsedSubArrs;
		}
		else
			--m_Front;

		if constexpr (Container::UseCopyConstructor<T>)
			new (&m_Data[0][m_Front]) T(Forward<T>(val));
		else
			m_Data[0][m_Front] = Forward<T>(val);

		++m_Size;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::PopFront()
	{
		if (m_Size > 0)
		{
			m_Data[0][m_Front].~T();
			if (m_Front == SubArraySize - 1)
			{
				m_Front = 0;
				sizeT numSubArrs = m_Capacity / SubArraySize - 1;
				m_pAlloc->Free(m_Data[0]);
				if (numSubArrs > 0)
					Memory::Move(m_Data, m_Data + 1, numSubArrs * sizeof(T*));
				m_Data[numSubArrs] = (T*)m_pAlloc->Allocate(SubArraySize * sizeof(T), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
				--m_UsedSubArrs;
			}
			else
				++m_Front;
			--m_Size;
		}
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Insert(sizeT index, const T& val)
	{
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		sizeT halfSize = m_Size / 2;
		sizeT idx = m_Front + index;
		if (index >= halfSize)
		{
			ShiftRight(idx, 1, true);
		}
		else
		{
			ShiftLeft(idx, 1, false);
			idx = m_Front + index;
		}
		if constexpr (Container::UseCopyConstructor<T>)
			new (&m_Data[idx / SubArraySize][idx % SubArraySize]) T(val);
		else
			m_Data[idx / SubArraySize][idx % SubArraySize] = val;
		++m_Size;
		m_UsedSubArrs = (m_Front + m_Size - 1) / SubArraySize + 1;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Insert(const Iterator& it, const T& val)
	{
		sizeT index = it.m_Index * SubArraySize + it.m_SubIndex - m_Front;
		Insert(index, val);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Insert(sizeT index, T&& val)
	{
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		sizeT halfSize = m_Size / 2;
		sizeT idx = m_Front + index;
		if (index >= halfSize)
		{
			ShiftRight(idx, 1, true);
		}
		else
		{
			ShiftLeft(idx, 1, false);
			idx = m_Front + index;
		}
		if constexpr (Container::UseCopyConstructor<T>)
			new (&m_Data[idx / SubArraySize][idx % SubArraySize]) T(Forward<T>(val));
		else
			m_Data[idx / SubArraySize][idx % SubArraySize] = Forward<T>(val);
		++m_Size;
		m_UsedSubArrs = (m_Front + m_Size - 1) / SubArraySize + 1;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Insert(const Iterator& it, T&& val)
	{
		sizeT index = it.m_Index * SubArraySize + it.m_SubIndex - m_Front;
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		Insert(index, Forward<T>(val));
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Insert(sizeT index, sizeT count, const T& val)
	{
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		sizeT halfSize = m_Size / 2;
		sizeT idx = m_Front + index;
		sizeT size = count;
		if (index >= halfSize)
		{
			ShiftRight(idx, size, true);
		}
		else
		{
			ShiftLeft(idx, size, false);
			idx = m_Front + index;
		}
		sizeT startIdx = idx / SubArraySize;
		sizeT endIdx = (idx + size) / SubArraySize + 1;
		sizeT j = idx % SubArraySize;
		sizeT icount = size > j ? SubArraySize - j : size;
		for (sizeT i = startIdx; i < endIdx; ++i)
		{
			T* subArr = m_Data[i];

			if constexpr (Container::UseCopyConstructor<T>)
			{
				for (; j < icount; ++j)
					new (subArr + j) T(*val);
			}
			else
			{
				for (; j < icount; ++j)
					subArr[j] = val;
			}

			j = 0;
			size -= icount;
			icount = size > SubArraySize ? SubArraySize : size;
		}
		m_Size += count;
		m_UsedSubArrs = (m_Front + m_Size - 1) / SubArraySize + 1;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Insert(const Iterator& it, sizeT count, const T& val)
	{
		sizeT index = it.m_Index * SubArraySize + it.m_SubIndex - m_Front;
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Insert(index, count, val);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	void Deque<T, SubArraySize, GrowthPolicy>::Insert(sizeT index, T(&arr)[N])
	{
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		Insert(index, arr, N);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	void Deque<T, SubArraySize, GrowthPolicy>::Insert(const Iterator& it, T(&arr)[N])
	{
		sizeT index = it.m_Index * SubArraySize + it.m_SubIndex - m_Front;
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		Insert(index, arr, N);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Insert(sizeT index, T* arr, sizeT count)
	{
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		sizeT halfSize = m_Size / 2;
		sizeT idx = m_Front + index;
		sizeT size = count;
		if (index >= halfSize)
		{
			ShiftRight(idx, size, true);
		}
		else
		{
			ShiftLeft(idx, size, false);
			idx = m_Front + index;
		}
		const T* itData = arr;
		sizeT startIdx = idx / SubArraySize;
		sizeT endIdx = (idx + size - 1) / SubArraySize;
		sizeT j = idx % SubArraySize;
		sizeT icount = size > SubArraySize - j ? SubArraySize - j : size;
		for (sizeT i = 0; i <= endIdx; ++i)
		{
			T* subArr = m_Data[startIdx + i];

			if constexpr (Container::UseMemCpy<T>)
			{
				Memory::Copy(subArr + j, itData, icount * sizeof(T));
				itData += icount;
			}
			else if constexpr (Container::UseCopyConstructor<T>)
			{
				for (; j < icount; ++j, ++itData)
					new (subArr + j) T(*itData);
			}
			else
			{
				for (; j < icount; ++j, ++itData)
					subArr[j] = *itData;
			}

			j = 0;
			size -= icount;
			icount = size > SubArraySize ? SubArraySize : size;
		}
		m_Size += count;
		m_UsedSubArrs = (m_Front + m_Size - 1) / SubArraySize + 1;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Insert(const Iterator& it, T* arr, sizeT count)
	{
		sizeT index = it.m_Index * SubArraySize + it.m_SubIndex - m_Front;
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Insert(index, arr, count);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Insert(sizeT index, const T* itFirst, const T* itLast)
	{
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last iterator can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		Insert(index, itFirst, sizeT(itLast - itFirst));
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Insert(const Iterator& it, const T* itFirst, const T* itLast)
	{
		sizeT index = it.m_Index * SubArraySize + it.m_SubIndex - m_Front;
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last iterator can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		Insert(index, itFirst, sizeT(itLast - itFirst));
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename InputIterator>
	void Deque<T, SubArraySize, GrowthPolicy>::Insert(sizeT index, const InputIterator& itFirst, const InputIterator& itLast)
	{
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		sizeT count = 0;
		for (Iterator itData = itFirst; itData != itLast; ++itData)
			++count;
		sizeT halfSize = m_Size / 2;
		sizeT idx = m_Front + index;
		sizeT size = count;
		if (index >= halfSize)
		{
			ShiftRight(idx, size, true);
		}
		else
		{
			ShiftLeft(idx, size, false);
			idx = m_Front + index;
		}
		InputIterator itData = itFirst;
		sizeT startIdx = idx / SubArraySize;
		sizeT endIdx = (idx + size - 1) / SubArraySize;
		sizeT j = idx % SubArraySize;
		sizeT icount = size > SubArraySize - j ? SubArraySize - j : size;
		for (sizeT i = 0; i <= endIdx; ++i)
		{
			T* subArr = m_Data[startIdx + i];

			if constexpr (Container::UseCopyConstructor<T>)
			{
				for (; j < icount; ++j, ++itData)
					new (subArr + j) T(*itData);
			}
			else
			{
				for (; j < icount; ++j, ++itData)
					subArr[j] = *itData;
			}

			j = 0;
			size -= icount;
			icount = size > SubArraySize ? SubArraySize : size;
		}
		m_Size += count;
		m_UsedSubArrs = (m_Front + m_Size - 1) / SubArraySize + 1;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename InputIterator>
	void Deque<T, SubArraySize, GrowthPolicy>::Insert(const Iterator& it, const InputIterator& itFirst, const InputIterator& itLast)
	{
		sizeT index = it.m_Index * SubArraySize + it.m_SubIndex - m_Front;
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last iterator can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		Insert(index, itFirst, itLast);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Insert(sizeT index, std::initializer_list<T> il)
	{
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Insert(index, il.begin(), il.size());
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Insert(const Iterator& it, std::initializer_list<T> il)
	{
		sizeT index = it.m_Index * SubArraySize + it.m_SubIndex - m_Front;
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Insert(index, il.begin(), il.size());
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Erase(sizeT index)
	{
		HV_ASSERT_MSG(index < m_Size, "Index out of range!");
		sizeT idx = m_Front + index;
		m_Data[idx / SubArraySize][idx % SubArraySize].~T();
		sizeT halfSize = m_Size / 2;
		if (index >= halfSize)
		{
			ShiftLeft(index, 1, true);
		}
		else
		{
			ShiftRight(index, 1, false);
		}
		--m_Size;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Erase(const Iterator& it)
	{
		sizeT index = it.m_Index * SubArraySize + it.m_SubIndex;
		HV_ASSERT_MSG(index < m_Size, "Iterator out of range!");
		Erase(index);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Erase(sizeT index, sizeT count)
	{
		HV_ASSERT_MSG(index < m_Size, "Index out of range!");
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		HV_ASSERT_MSG(index + count <= m_Size, "Count is too big, reaching past DynArray's size!");
		sizeT idx = m_Front + index;

		sizeT size = count;
		sizeT startIdx = idx / SubArraySize;
		sizeT endIdx = (idx + size - 1) / SubArraySize;
		sizeT j = idx % SubArraySize;
		sizeT icount = size > SubArraySize - j ? SubArraySize - j : size;
		for (sizeT i = 0; i <= endIdx; ++i)
		{
			T* subArr = m_Data[startIdx + i];

			for (; j < icount; ++j)
				subArr[j].~T();

			j = 0;
			size -= icount;
			icount = size > SubArraySize ? SubArraySize : size;
		}

		sizeT halfSize = m_Size / 2;
		if (index >= halfSize)
		{
			ShiftLeft(index + count - 1, count, true);
		}
		else
		{
			ShiftRight(index, count, false);
		}
		m_Size -= count;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Erase(const Iterator& it, sizeT count)
	{
		sizeT idx = it.m_Index * SubArraySize + it.m_SubIndex;
		sizeT index = idx - m_Front;
		HV_ASSERT_MSG(index < m_Size, "Index out of range!");
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		HV_ASSERT_MSG(index + count <= m_Size, "Count is too big, reaching past DynArray's size!");
		Erase(index, count);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Erase(const Iterator& itFirst, const Iterator& itLast)
	{
		sizeT idx = itFirst.m_Index * SubArraySize + itFirst.m_SubIndex;
		sizeT idxLast = itLast.m_Index * SubArraySize + itLast.m_SubIndex;
		sizeT count = idxLast - idx;
		sizeT index = idx - m_Front;
		HV_ASSERT_MSG(index < m_Size, "Index out of range!");
		HV_ASSERT_MSG(index + count <= m_Size, "Count is too big, reaching past DynArray's size!");

		sizeT size = count;
		sizeT startIdx = idx / SubArraySize;
		sizeT endIdx = (idx + size - 1) / SubArraySize;
		sizeT j = idx % SubArraySize;
		sizeT icount = size > SubArraySize - j ? SubArraySize - j : size;
		for (sizeT i = 0; i <= endIdx; ++i)
		{
			T* subArr = m_Data[startIdx + i];

			for (; j < icount; ++j)
				subArr[j].~T();

			j = 0;
			size -= icount;
			icount = size > SubArraySize ? SubArraySize : size;
		}

		sizeT halfSize = m_Size / 2;
		if (index >= halfSize)
		{
			ShiftLeft(index + count - 1, count, true);
		}
		else
		{
			ShiftRight(index, count, false);
		}
		m_Size -= count;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename ... Args>
	void Deque<T, SubArraySize, GrowthPolicy>::Emplace(sizeT index, Args&&... args)
	{
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		sizeT halfSize = m_Size / 2;
		sizeT idx = m_Front + index;
		if (index >= halfSize)
		{
			ShiftRight(idx, 1, true);
		}
		else
		{
			ShiftLeft(idx, 1, false);
			idx = m_Front + index;
		}
		new (&m_Data[idx / SubArraySize][idx % SubArraySize]) T(args...);
		++m_Size;
		m_UsedSubArrs = (m_Front + m_Size - 1) / SubArraySize + 1;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename ... Args>
	void Deque<T, SubArraySize, GrowthPolicy>::Emplace(const Iterator& it, Args&&... args)
	{
		sizeT index = it.m_Index * SubArraySize + it.m_SubIndex - m_Front;
		HV_ASSERT_MSG(index - m_Front <= m_Size, "Index out of range!");
		Emplace(index, Forward<Args...>(args...));
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename ... Args>
	void Deque<T, SubArraySize, GrowthPolicy>::EmplaceBack(Args&&... args)
	{
		sizeT idx = m_Front + m_Size;
		Reserve(m_Size + 1);
		sizeT mod = idx % SubArraySize;

		new (&m_Data[idx / SubArraySize][mod]) T(args...);

		++m_Size;
		if (mod == 0)
			++m_UsedSubArrs;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename ... Args>
	void Deque<T, SubArraySize, GrowthPolicy>::EmplaceFront(Args&&... args)
	{
		sizeT end = m_Front + m_Size;

		if (m_Front == 0)
		{
			Reserve(end + SubArraySize + 1);
			m_Front = SubArraySize - 1;
			sizeT numSubArrs = m_Capacity / SubArraySize - 1;
			m_pAlloc->Free(m_Data[numSubArrs]);
			if (numSubArrs > 0)
				Memory::Move(m_Data + 1, m_Data, numSubArrs * sizeof(T*));
			m_Data[0] = (T*)m_pAlloc->Allocate(SubArraySize * sizeof(T), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			++m_UsedSubArrs;
		}
		else
			--m_Front;

		new (&m_Data[0][m_Front]) T(args...);

		++m_Size;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Swap(Deque& deque)
	{
		if (m_pAlloc == deque.m_pAlloc)
		{
			Hv::Swap(m_Data, deque.m_Data);
			Hv::Swap(m_Front, deque.m_Front);
			Hv::Swap(m_UsedSubArrs, deque.m_UsedSubArrs);
			Hv::Swap(m_Size, deque.m_Size);
			Hv::Swap(m_Capacity, deque.m_Capacity);
		}
		else
		{
			Deque tmp(Move(*this), m_pAlloc);
			*this = Move(deque);
			deque = Move(tmp);
		}
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	b8 Deque<T, SubArraySize, GrowthPolicy>::IsEmpty() const
	{
		return m_Size == 0;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT Deque<T, SubArraySize, GrowthPolicy>::Size() const
	{
		return m_Size;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	sizeT Deque<T, SubArraySize, GrowthPolicy>::Capacity() const
	{
		return m_Capacity;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	T** Deque<T, SubArraySize, GrowthPolicy>::Data()
	{
		return m_Data;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	Memory::IAllocator* Deque<T, SubArraySize, GrowthPolicy>::GetAllocator()
	{
		return m_pAlloc;
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Deque<T, SubArraySize, GrowthPolicy>::Iterator Deque<T, SubArraySize, GrowthPolicy>::Front() const
	{
		return Iterator(m_Data, 0, m_Front);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Deque<T, SubArraySize, GrowthPolicy>::Iterator Deque<T, SubArraySize, GrowthPolicy>::Last() const
	{
		sizeT size = m_Front + m_Size - 1;
		return Iterator(m_Data, size / SubArraySize, size % SubArraySize);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Deque<T, SubArraySize, GrowthPolicy>::Iterator Deque<T, SubArraySize, GrowthPolicy>::Back() const
	{
		sizeT size = m_Front + m_Size;
		return Iterator(m_Data, size / SubArraySize, size % SubArraySize);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Deque<T, SubArraySize, GrowthPolicy>::Iterator Deque<T, SubArraySize, GrowthPolicy>::begin() const
	{
		return Iterator(m_Data, 0, m_Front);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	typename Deque<T, SubArraySize, GrowthPolicy>::Iterator Deque<T, SubArraySize, GrowthPolicy>::end() const
	{
		sizeT size = m_Front + m_Size;
		return Iterator(m_Data, size / SubArraySize, size % SubArraySize);
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc OtherPolicy, sizeT OtherSubArraySize>
	void Deque<T, SubArraySize, GrowthPolicy>::Copy(const Deque& deque)
	{
		Clear(false);
		Reserve(deque.m_Size);
		m_Size = deque.m_Size;
		m_UsedSubArrs = (deque.m_Size - 1) / SubArraySize + 1;
		sizeT count = m_Size;
		typename Deque<T, OtherPolicy, OtherSubArraySize>::Iterator it = deque.Front();
		for (sizeT i = 0; i < m_UsedSubArrs; ++i)
		{
			sizeT icount = count > SubArraySize ? SubArraySize : count;
			T* subArr = m_Data[i];

			if constexpr (Container::UseCopyConstructor<T>)
			{
				for (sizeT j = 0; j < icount; ++j, ++it)
					new (subArr + j) T(*it);
			}
			else
			{
				for (sizeT j = 0; j < icount; ++j, ++it)
					subArr[j] = *it;
			}

			count -= SubArraySize;
		}
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::Copy(const Deque& deque)
	{
		Clear(false);
		if (!m_pAlloc)
			m_pAlloc = deque.m_pAlloc;
		Reserve(deque.m_Size);
		m_Size = deque.m_Size;
		m_UsedSubArrs = (deque.m_Size - 1) / SubArraySize + 1;
		sizeT count = m_Size;
		Iterator it = deque.Front();
		for (sizeT i = 0; i < m_UsedSubArrs; ++i)
		{
			sizeT icount = count > SubArraySize ? SubArraySize : count;
			T* subArr = m_Data[i];

			if constexpr (Container::UseCopyConstructor<T>)
			{
				for (sizeT j = 0; j < icount; ++j, ++it)
					new (subArr + j) T(*it);
			}
			else
			{
				for (sizeT j = 0; j < icount; ++j, ++it)
					subArr[j] = *it;
			}

			count -= SubArraySize;
		}
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::ShiftLeft(sizeT from, sizeT count, b8 rightSideData)
	{
		if (rightSideData)
		{
			if (from >= m_Size - 1)
				return;

			sizeT movePos = count % SubArraySize;
			sizeT hIdx0 = (m_Front + from + 1) % SubArraySize;
			sizeT vIdx0 = (m_Front + from + 1) / SubArraySize;
			sizeT hIdx1 = (m_Front + from - count + 1) % SubArraySize;
			sizeT vIdx1 = (m_Front + from - count + 1) / SubArraySize;
			sizeT vMovePos = vIdx1 - vIdx0;
			sizeT end = (m_Front + m_Size - count - 1) / SubArraySize;
			sizeT loops = vIdx0 - end;

			sizeT size = hIdx0 > hIdx1 ? SubArraySize - hIdx0 : SubArraySize - hIdx1;
			if (size > 0)
				Memory::Move(&m_Data[vIdx1][hIdx1], &m_Data[vIdx0][hIdx0], size * sizeof(T));
			for (sizeT i = 0; i < loops; ++i)
			{
				sizeT idx = vIdx1 + loops - i;
				if (movePos < SubArraySize)
					Memory::Move(&m_Data[idx + vMovePos - 1][movePos], &m_Data[idx][0], (SubArraySize - movePos) * sizeof(T));
				if (movePos > 0)
					Memory::Move(&m_Data[idx + vMovePos][0], &m_Data[idx][SubArraySize - movePos], movePos * sizeof(T));
			}
		}
		else
		{
			if (count < m_Front)
				return;
			sizeT neededSize = count < from ? count : count - m_Front;
			if (neededSize > 0)
			{
				sizeT size = ((m_Size - 1) / SubArraySize + 1) * SubArraySize;
				Reserve(size + neededSize);
				neededSize = (neededSize - 1) / SubArraySize + 1;
				m_Front += neededSize * SubArraySize;

				sizeT totalSubArrs = m_Capacity / SubArraySize;
				T** temp = (T**)m_pAlloc->Allocate(sizeof(T), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
				Memory::Copy(temp, m_Data, totalSubArrs * sizeof(T*));
				sizeT diff = totalSubArrs - neededSize;
				Memory::Copy(m_Data + neededSize, temp, diff * sizeof(T*));
				Memory::Copy(m_Data, temp + diff, neededSize * sizeof(T*));
				m_pAlloc->Free(temp);
			}
			if (from == 0 && m_Front > count)
			{
				m_Front -= count;
				return;
			}

			sizeT movePos = count % SubArraySize;
			sizeT hIdx0 = (m_Front + from) % SubArraySize;
			sizeT vIdx0 = (m_Front + from) / SubArraySize;
			sizeT hIdx1 = (m_Front + from - count + 1) % SubArraySize;
			sizeT vIdx1 = (m_Front + from - count + 1) / SubArraySize;
			sizeT vMovePos = vIdx0 - vIdx1;
			sizeT loops = vIdx1;

			for (sizeT i = 0; i < loops; ++i)
			{
				if (movePos < SubArraySize)
					Memory::Move(&m_Data[i + vMovePos][0], &m_Data[i][movePos], (SubArraySize - movePos) * sizeof(T));
				if (movePos > 0)
					Memory::Move(&m_Data[i + vMovePos][movePos], &m_Data[i + 1][0], movePos * sizeof(T));
			}
			sizeT size = hIdx0 < hIdx1 ? hIdx0 : hIdx1;
			if (size > 0)
				Memory::Move(&m_Data[vIdx1][0], &m_Data[vIdx0][movePos], size * sizeof(T));

			m_Front -= count;
		}
	}

	template <typename T, sizeT SubArraySize, Container::GrowthPolicyFunc GrowthPolicy>
	void Deque<T, SubArraySize, GrowthPolicy>::ShiftRight(sizeT from, sizeT count, b8 rightSideData)
	{
		if (rightSideData)
		{
			Reserve(m_Front + m_Size + count);
			if (from == m_Size)
				return;
			sizeT movePos = count % SubArraySize;
			sizeT hIdx0 = (m_Front + from) % SubArraySize;
			sizeT vIdx0 = (m_Front + from) / SubArraySize;
			sizeT hIdx1 = (m_Front + from + count) % SubArraySize;
			sizeT vIdx1 = (m_Front + from + count) / SubArraySize;
			sizeT vMovePos = vIdx1 - vIdx0;
			sizeT end = (m_Front + m_Size + count - 1) / SubArraySize;
			sizeT loops = end - vIdx0;

			for (sizeT i = 0; i < loops; ++i)
			{
				sizeT idx = vIdx1 + loops - i;
				if (movePos < SubArraySize)
					Memory::Move(&m_Data[idx + vMovePos][movePos], &m_Data[idx][0], (SubArraySize - movePos) * sizeof(T));
				if (movePos > 0)
					Memory::Move(&m_Data[idx + vMovePos][0], &m_Data[idx - 1][movePos], movePos * sizeof(T));
			}
			sizeT size = hIdx0 > hIdx1 ? SubArraySize - hIdx0 : SubArraySize - hIdx1;
			if (size > 0)
				Memory::Move(&m_Data[vIdx1][hIdx1], &m_Data[vIdx0][hIdx0], size * sizeof(T));
		}
		else
		{
			if (from == 0)
			{
				m_Front += count;
				return;
			}

			sizeT movePos = count % SubArraySize;
			sizeT hIdx0 = (m_Front + from - 1) % SubArraySize;
			sizeT vIdx0 = (m_Front + from - 1) / SubArraySize;
			sizeT hIdx1 = (m_Front + from + count - 1) % SubArraySize;
			sizeT vIdx1 = (m_Front + from + count - 1) / SubArraySize;
			sizeT vMovePos = vIdx1 - vIdx0;
			sizeT loops = vIdx0;

			sizeT size = (hIdx0 < hIdx1 ? hIdx0 : hIdx1) + 1;
			if (size > 0)
				Memory::Move(&m_Data[vIdx1][movePos], &m_Data[vIdx0][0], size * sizeof(T));
			for (sizeT i = 0; i < loops; ++i)
			{
				if (movePos < SubArraySize)
					Memory::Move(&m_Data[i + vMovePos + 1][0], &m_Data[i][SubArraySize - movePos], (SubArraySize - movePos) * sizeof(T));
				if (movePos > 0)
					Memory::Move(&m_Data[i + vMovePos][movePos], &m_Data[i][0], movePos * sizeof(T));
			}

			m_Front += count;
		}
	}
	
}