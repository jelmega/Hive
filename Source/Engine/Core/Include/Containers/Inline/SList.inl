// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// SList.inl: Singly linked list
#pragma once
#include "Containers/SList.h"

namespace Hv {
	
	template <typename T>
	HV_FORCE_INL SList<T>::Node::Node()
		: pNext(nullptr)
	{
	}

	template <typename T>
	HV_FORCE_INL SList<T>::Node::Node(const T& val, Node* pNext)
		: pNext(pNext)
	{
		if constexpr (Container::UseCopyConstructor<T>)
			new (&value) T(val);
		else
			value = val;
	}

	template <typename T>
	HV_FORCE_INL SList<T>::Node::Node(T&& val, Node* pNext)
		: pNext(pNext)
	{
		if constexpr (Container::UseCopyConstructor<T>)
			new (&value) T(Forward<T>(val));
		else
			value = FORWARD<T>(val);
	}

	template <typename T>
	HV_FORCE_INL SList<T>::Node::~Node()
	{
		value.~T();
	}

	template <typename T>
	HV_FORCE_INL SList<T>::Iterator::Iterator()
		: m_pNode(nullptr)
	{
	}

	template <typename T>
	HV_FORCE_INL SList<T>::Iterator::Iterator(const Iterator& it)
		: m_pNode(it.m_pNode)
	{
	}

	template <typename T>
	HV_FORCE_INL SList<T>::Iterator::Iterator(Iterator&& it) noexcept
		: m_pNode(it.m_pNode)
	{
	}

	template <typename T>
	HV_FORCE_INL T& SList<T>::Iterator::operator*()
	{
		HV_ASSERT_MSG(m_pNode, "Can't retrieve value!");
		return m_pNode->value;
	}

	template <typename T>
	HV_FORCE_INL const T& SList<T>::Iterator::operator*() const
	{
		HV_ASSERT_MSG(m_pNode, "Can't retrieve value!");
		return m_pNode->value;
	}

	template <typename T>
	HV_FORCE_INL T* SList<T>::Iterator::operator->()
	{
		HV_ASSERT_MSG(m_pNode, "Can't retrieve value!");
		return &m_pNode->value;
	}

	template <typename T>
	HV_FORCE_INL const T* SList<T>::Iterator::operator->() const
	{
		HV_ASSERT_MSG(m_pNode, "Can't retrieve value!");
		return &m_pNode->value;
	}

	template <typename T>
	HV_FORCE_INL typename SList<T>::Iterator& SList<T>::Iterator::operator++()
	{
		HV_ASSERT_MSG(m_pNode, "Can't increment iterator!");
		m_pNode = m_pNode->pNext;
		return *this;
	}

	template <typename T>
	HV_FORCE_INL typename SList<T>::Iterator SList<T>::Iterator::operator++(int)
	{
		HV_ASSERT_MSG(m_pNode, "Can't increment iterator!");
		Iterator it(*this);
		m_pNode = m_pNode->pNext;
		return *this;
	}

	template <typename T>
	HV_FORCE_INL typename SList<T>::Iterator SList<T>::Iterator::operator+(sizeT count)
	{
		HV_ASSERT_MSG(m_pNode, "Can't increment iterator!");
		Iterator it(m_pNode);
		for (sizeT i = 0; i < count; ++i)
		{
			it.m_pNode = it.m_pNode->pNext;
			HV_ASSERT(it.m_pNode);
		}
		return it;
	}

	template <typename T>
	HV_FORCE_INL typename SList<T>::Iterator& SList<T>::Iterator::operator=(const Iterator& it)
	{
		m_pNode = it.m_pNode;
		return *this;
	}

	template <typename T>
	HV_FORCE_INL typename SList<T>::Iterator& SList<T>::Iterator::operator=(Iterator&& it) noexcept
	{
		m_pNode = it.m_pNode;
		return *this;
	}

	template <typename T>
	HV_FORCE_INL typename SList<T>::Iterator& SList<T>::Iterator::operator+=(sizeT count)
	{
		for (sizeT i = 0; i < count; ++i)
			m_pNode = m_pNode->pNext;
		return *this;
	}

	template <typename T>
	HV_FORCE_INL b8 SList<T>::Iterator::operator==(const Iterator& it) const
	{
		return m_pNode == it.m_pNode;
	}

	template <typename T>
	HV_FORCE_INL b8 SList<T>::Iterator::operator!=(const Iterator& it) const
	{
		return m_pNode != it.m_pNode;
	}

	template <typename T>
	HV_FORCE_INL SList<T>::Iterator::Iterator(Node* pNode)
		:m_pNode(pNode)
	{
	}

	////////////////////////////////////////////////////////////////////////////////

	template <typename T>
	HV_INL SList<T>::SList()
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(g_pAllocator)
	{
	}

	template <typename T>
	HV_INL SList<T>::SList(Memory::IAllocator* pAlloc)
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(pAlloc)
	{
	}

	template <typename T>
	HV_INL SList<T>::SList(sizeT count, const T& value, Memory::IAllocator* pAlloc)
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Fill(value, count);
	}

	template <typename T>
	template <sizeT N>
	HV_INL SList<T>::SList(T(&arr)[N], Memory::IAllocator* pAlloc)
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(pAlloc)
	{
		Assign(arr, N);
	}

	template <typename T>
	HV_INL SList<T>::SList(const T* arr, sizeT count, Memory::IAllocator* pAlloc)
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Assign(arr, count);
	}

	template <typename T>
	HV_INL SList<T>::SList(const T* itFirst, const T* itLast, Memory::IAllocator* pAlloc)
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		Assign(itFirst, sizeT(itLast - itFirst));
	}

	template <typename T>
	template <typename InputIterator>
	HV_INL SList<T>::SList(const InputIterator& itFirst, const InputIterator& itLast, Memory::IAllocator* pAlloc)
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		Assign(itFirst, itLast);
	}

	template <typename T>
	HV_INL SList<T>::SList(std::initializer_list<T> il, Memory::IAllocator* pAlloc)
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Assign(il.begin(), il.size());
	}

	template <typename T>
	HV_INL SList<T>::SList(const SList& list)
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(list.m_pAlloc)
	{
		Copy(list);
	}

	template <typename T>
	HV_INL SList<T>::SList(const SList& list, Memory::IAllocator* pAlloc)
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(0)
		, m_pAlloc(pAlloc)
	{
		Copy(list);
	}

	template <typename T>
	HV_INL SList<T>::SList(SList&& list) noexcept
		: m_pHead(nullptr)
		, m_pTail(nullptr)
		, m_Size(list.m_Size)
		, m_pAlloc(list.m_pAlloc)
	{
		Hv::Swap(m_pHead, list.m_pHead);
		Hv::Swap(m_pTail, list.m_pTail);
		Hv::Swap(m_Size, list.m_Size);
	}

	template <typename T>
	HV_INL SList<T>::~SList()
	{
		Clear();
	}

	template <typename T>
	template <sizeT N>
	SList<T>& SList<T>::operator=(T(&arr)[N])
	{
		Assign(arr, N);
		return *this;
	}

	template <typename T>
	HV_INL SList<T>& SList<T>::operator=(std::initializer_list<T> il)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Assign(il.begin(), il.size());
		return *this;
	}
	
	template <typename T>
	HV_INL SList<T>& SList<T>::operator=(const SList& list)
	{
		Copy(list);
		return *this;
	}

	template <typename T>
	HV_INL SList<T>& SList<T>::operator=(SList&& list) noexcept
	{
		if (m_pAlloc == list.m_pAlloc)
		{
			Hv::Swap(m_pHead, list.m_pHead);
			Hv::Swap(m_pTail, list.m_pTail);
			Hv::Swap(m_Size, list.m_Size);
		}
		else
		{
			Copy(list);
		}
		list.Clear();
		return *this;
	}

	template <typename T>
	HV_FORCE_INL T& SList<T>::operator[](sizeT index)
	{
		HV_ASSERT_MSG(index < m_Size, "Index out of range!");
		return GetNodeAtIndex(index)->value;
	}

	template <typename T>
	HV_FORCE_INL const T& SList<T>::operator[](sizeT index) const
	{
		HV_ASSERT_MSG(index < m_Size, "Index out of range!");
		return GetNodeAtIndex(index)->value;
	}

	template <typename T>
	HV_INL void SList<T>::Fill(const T& val, sizeT count)
	{
		Clear();
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		m_Size = count;
		Node *pPrev = &m_pHead, *pNode = nullptr;
		for (sizeT i = 0; i < m_Size; ++i)
		{
			pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			new (pNode) T(val);
			pPrev->pNext = pNode;
			pPrev = pNode;
		}
		m_pTail = pNode;
	}

	template <typename T>
	template <sizeT N>
	HV_INL void SList<T>::Assign(T(&arr)[N])
	{
		Assign(arr, N);
	}

	template <typename T>
	HV_INL void SList<T>::Assign(T* arr, sizeT count)
	{
		Clear();
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		m_Size = count;
		Node *pPrev = &m_pHead, *pNode = nullptr;
		for (sizeT i = 0; i < m_Size; ++i)
		{
			pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			new (pNode) T(arr[i]);
			pPrev->pNext = pNode;
			pPrev = pNode;
		}
		m_pTail = pNode;
	}

	template <typename T>
	HV_INL void SList<T>::Assign(const T* itFirst, const T* itLast)
	{
		Clear();
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		Assign(itFirst, sizeT(itLast - itFirst));
	}

	template <typename T>
	template <typename InputIterator>
	HV_INL void SList<T>::Assign(const InputIterator& itFirst, const InputIterator& itLast)
	{
		Clear();
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		for (InputIterator it = itFirst; it != itLast; ++it)
			++m_Size;
		Node *pPrev = &m_pHead, *pNode = nullptr;
		const T* it = itFirst;
		for (sizeT i = 0; i < m_Size; ++i, ++it)
		{
			pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			new (pNode) T(*it);
			pPrev->pNext = pNode;
			pPrev = pNode;
		}
		m_pTail = pNode;
	}

	template <typename T>
	HV_INL void SList<T>::Assign(std::initializer_list<T> il)
	{
		Assign(il.begin(), il.size());
	}

	template <typename T>
	HV_INL void SList<T>::Resize(sizeT size)
	{
		if (size == 0)
		{
			Clear();
			m_Size = 0;
		}
		else if (size < m_Size)
		{
			Node* pNode = GetNodeAtIndex(size - 1);
			Node* pNext = pNode->pNext;
			pNode->pNext = nullptr;
			m_pTail = pNode;
			pNode = pNext;
			while (pNode)
			{
				pNext = pNode->pNext;
				pNode->~Node();
				m_pAlloc->Free(pNode);
				pNode = pNext;
			}
			m_Size = size;
		}

	}

	template <typename T>
	HV_INL void SList<T>::Resize(sizeT size, const T& val)
	{
		if (size == 0)
			Clear();
		else if (size < m_Size)
		{
			Node* pNode = GetNodeAtIndex(size - 1);
			Node* pNext = pNode->pNext;
			pNode->pNext = nullptr;
			m_pTail = pNode;
			pNode = pNext;
			while (pNode)
			{
				pNext = pNode->pNext;
				pNode->~Node();
				m_pAlloc->Free(pNode);
				pNode = pNext;
			}
		}
		else if (size > m_Size)
		{
			Node* pPrev = m_pTail, *pNode = nullptr;
			for (sizeT i = 0, end = size - m_Size; i < end; ++i)
			{
				pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
				new (pNode) Node(val);
				pPrev->pNext = pNode;
				pPrev = pNode;
			}
			m_pTail = pNode;
		}
		m_Size = size;
	}

	template <typename T>
	HV_INL void SList<T>::Clear()
	{
		Node* pNode = m_pHead;
		while (pNode)
		{
			Node* pNext = pNode->pNext;
			pNode->~Node();
			m_pAlloc->Free(pNode);
			pNode = pNext;
		}
		m_Size = 0;
		m_pHead = m_pTail = nullptr;
	}

	template <typename T>
	void SList<T>::PushBack(const T& val)
	{
		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(val);
		m_pTail->pNext = pNode;
		m_pTail = pNode;
		++m_Size;
	}

	template <typename T>
	HV_INL void SList<T>::PushBack(T&& val)
	{
		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(Forward<T>(val));
		m_pTail->pNext = pNode;
		m_pTail = pNode;
		++m_Size;
	}

	template <typename T>
	HV_INL void SList<T>::PopBack()
	{
		if (m_Size == 1)
		{
			Node* pNode = m_pTail;
			m_pHead = m_pTail = nullptr;
			pNode->~Node();
			m_pAlloc->Free(pNode);
			--m_Size;
		}
		else if (m_Size > 1)
		{
			Node* pNode = GetNodeAtIndex(m_Size - 2);
			m_pTail = pNode;
			pNode = pNode->pNext;
			m_pTail->pNext = nullptr;
			pNode->~Node();
			m_pAlloc->Free(pNode);
			--m_Size;
		}
	}

	template <typename T>
	HV_INL void SList<T>::PushFront(const T& val)
	{
		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(val, m_pHead);
		m_pHead = pNode;
		++m_Size;
	}

	template <typename T>
	HV_INL void SList<T>::PushFront(T&& val)
	{
		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(Forward<T>(val), m_pHead);
		m_pHead = pNode;
		++m_Size;
	}

	template <typename T>
	HV_INL void SList<T>::PopFront()
	{
		if (m_Size == 1)
		{
			Node* pNode = m_pTail;
			m_pHead = m_pTail = nullptr;
			pNode->~Node();
			m_pAlloc->Free(pNode);
			--m_Size;
		}
		else if (m_Size > 1)
		{
			Node* pNode = m_pHead;
			m_pHead = pNode->pNext;
			pNode->~Node();
			m_pAlloc->Free(pNode);
			--m_Size;
		}
	}

	template <typename T>
	HV_INL void SList<T>::InsertAfter(const Iterator& it, const T& val)
	{
		HV_ASSERT_MSG(it.m_pNode, "Iterator out of range!");
		Node* pPrev = it.m_pNode;
		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(val);
		pNode->pNext = pPrev->pNext;
		pPrev->pNext = pNode;
		if (pPrev == m_pTail)
			m_pTail = pNode;
		++m_Size;
	}

	template <typename T>
	HV_INL void SList<T>::InsertAfter(const Iterator& it, T&& val)
	{
		HV_ASSERT_MSG(it.m_pNode, "Iterator out of range!");
		Node* pPrev = it.m_pNode;
		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(Forward<T>(val));
		pNode->pNext = pPrev->pNext;
		pPrev->pNext = pNode;
		if (pPrev == m_pTail)
			m_pTail = pNode;
		++m_Size;
	}

	template <typename T>
	HV_INL void SList<T>::InsertAfter(const Iterator& it, sizeT count, const T& val)
	{
		HV_ASSERT_MSG(it.m_pNode, "Iterator out of range!");
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Node* pPrev = it.m_pNode;
		Node* pNext = pPrev->pNext;
		Node* pNode = nullptr;
		for (sizeT i = 0; i < count; ++i)
		{
			pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			new (pNode) T(val);
			pPrev->pNext = pNode;
			pPrev = pNode;
		}
		pPrev->pNext = pNext;
		if (it.m_pNode == m_pTail)
			m_pTail = pNode;
		m_Size += count;
	}

	template <typename T>
	template <sizeT N>
	HV_INL void SList<T>::InsertAfter(const Iterator& it, T(&arr)[N])
	{
		HV_ASSERT_MSG(it.m_pNode, "Iterator out of range!");
		InsertAfter(it, arr, N);
	}

	template <typename T>
	HV_INL void SList<T>::InsertAfter(const Iterator& it, T* arr, sizeT count)
	{
		HV_ASSERT_MSG(it.m_pNode, "Iterator out of range!");
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Node* pPrev = it.m_pNode;
		Node* pNext = pPrev->pNext;
		Node* pNode = nullptr;
		for (sizeT i = 0; i < count; ++i)
		{
			pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			new (pNode) T(arr[i]);
			pPrev->pNext = pNode;
			pPrev = pNode;
		}
		pPrev->pNext = pNext;
		if (it.m_pNode == m_pTail)
			m_pTail = pNode;
		m_Size += count;
	}

	template <typename T>
	HV_INL void SList<T>::InsertAfter(const Iterator& it, const T* itFirst, const T* itLast)
	{
		HV_ASSERT_MSG(it.m_pNode, "Iterator out of range!");
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last iterator can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		InsertAfter(it, itFirst, sizeT(itLast - itFirst));
	}

	template <typename T>
	template <typename InputIterator>
	HV_INL void SList<T>::InsertAfter(const Iterator& it, const InputIterator& itFirst, const InputIterator& itLast)
	{
		HV_ASSERT_MSG(it.m_pNode, "Iterator out of range!");
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		Node* pPrev = it.m_pNode;
		Node* pNext = pPrev->pNext;
		Node* pNode = nullptr;
		for (Iterator itData = itFirst; itData != itLast; ++itLast)
		{
			pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			new (pNode) T(*itData);
			pPrev->pNext = pNode;
			pPrev = pNode;
			++m_Size;
		}
		pPrev->pNext = pNext;
		if (it.m_pNode == m_pTail)
			m_pTail = pNode;
	}

	template <typename T>
	HV_INL void SList<T>::InsertAfter(const Iterator& it, std::initializer_list<T> il)
	{
		HV_ASSERT_MSG(it.m_pNode, "Iterator out of range!");
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		InsertAfter(il.begin(), il.size());
	}

	template <typename T>
	HV_INL void SList<T>::EraseAfter(const Iterator& it)
	{
		if (m_Size == 0)
			return;

		HV_ASSERT_MSG(it.m_pNode, "Iterator out of range!");

		Node* pPrev = it.m_pNode;
		if (!pPrev->pNext)
			return;
		Node* pNode = pPrev->pNext;
		pPrev->pNext = pNode->pNext;
		if (pNode == m_pTail)
			m_pTail = pPrev;

		pNode->~Node();
		m_pAlloc->Free(pNode);

		--m_Size;
	}

	template <typename T>
	HV_INL void SList<T>::EraseAfter(const Iterator& it, sizeT count)
	{
		if (m_Size == 0)
			return;

		HV_ASSERT_MSG(it.m_pNode, "Iterator out of range!");
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		HV_ASSERT_MSG(count <= m_Size, "Count is too big, reaching past SList's size!");

		Node* pPrev = it.m_pNode;
		Node* pNode = pPrev->pNext;
		for (sizeT i = 0; i < count; ++i)
		{
			HV_ASSERT_MSG(pNode, "Count is too big, reaching past SList's size!");
			Node* pNext = pNode->pNext;
			pNode->~Node();
			m_pAlloc->Free(pNode);
			pNode = pNext;
		}
		pPrev->pNext = pNode;
		if (!pNode)
			m_pTail = pPrev;
		m_Size -= count;
	}

	template <typename T>
	HV_INL void SList<T>::EraseAfter(const Iterator& itFirst, const Iterator& itLast)
	{

		HV_ASSERT_MSG(itFirst.m_pNode, "First iterator out of range!");
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");

		Node* pPrev = itFirst.m_pNode;
		Node* pNode = pPrev->pNext;
		while (pNode != itLast.m_pNode)
		{
			HV_ASSERT_MSG(pNode, "ItLast out of range!");
			Node* pNext = pNode->pNext;
			pNode->~Node();
			m_pAlloc->Free(pNode);
			pNode = pNext;
			--m_Size;
		}
		pPrev->pNext = pNode;
		if (!pNode)
			m_pTail = pPrev;
	}

	template <typename T>
	template <typename ... Args>
	HV_INL void SList<T>::EmplaceAfter(const Iterator& it, Args&&... args)
	{
		HV_ASSERT_MSG(it.m_pNode, "Iterator out of range!");
		Node* pPrev = it.m_pNode;
		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(Forward<T>(T(args...)));
		pNode->pNext = pPrev->pNext;
		pPrev->pNext = pNode;
		if (pPrev == m_pTail)
			m_pTail = pNode;
		++m_Size;
	}

	template <typename T>
	template <typename ... Args>
	HV_INL void SList<T>::EmplaceBack(Args&&... args)
	{
		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(Forward<T>(T(args...)));
		m_pTail->pNext = pNode;
		m_pTail = pNode;
		++m_Size;
	}

	template <typename T>
	template <typename ... Args>
	HV_INL void SList<T>::EmplaceFront(Args&&... args)
	{
		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(Forward<T>(T(args...)), m_pHead);
		m_pHead = pNode;
		++m_Size;
	}

	template <typename T>
	HV_INL void SList<T>::Swap(SList& list)
	{
		if (m_pAlloc == list.m_pAlloc)
		{
			Hv::Swap(m_pHead, list.m_pHead);
			Hv::Swap(m_pTail, list.m_pTail);
			Hv::Swap(m_Size, list.m_Size);
		}
		else
		{
			SList tmp(Move(*this), m_pAlloc);
			*this = Move(list);
			list = Move(tmp);
		}
	}

	template <typename T>
	HV_FORCE_INL typename SList<T>::Node* SList<T>::GetNodeAtIndex(sizeT index)
	{
		HV_ASSERT_MSG(index < m_Size, "Index out of range!");
		Node* pNode = m_pHead;
		for (sizeT i = 0; i < index; ++i)
		{
			pNode = pNode->pNext;
		}
		return pNode;
	}

	template <typename T>
	HV_FORCE_INL b8 SList<T>::IsEmpty() const
	{
		return m_Size == 0;
	}

	template <typename T>
	HV_FORCE_INL sizeT SList<T>::Size() const
	{
		return m_Size;
	}

	template <typename T>
	HV_FORCE_INL Memory::IAllocator* SList<T>::GetAllocator()
	{
		return m_pAlloc;
	}

	template <typename T>
	HV_FORCE_INL typename SList<T>::Iterator SList<T>::BeforeFront() const
	{
		return Iterator(&m_pHead);
	}

	template <typename T>
	HV_FORCE_INL typename SList<T>::Iterator SList<T>::Front() const
	{
		return Iterator(m_pHead);
	}

	template <typename T>
	HV_FORCE_INL typename SList<T>::Iterator SList<T>::Last() const
	{
		return Iterator(m_pTail);
	}

	template <typename T>
	HV_FORCE_INL typename SList<T>::Iterator SList<T>::Back() const
	{
		return Iterator(nullptr);
	}

	template <typename T>
	HV_FORCE_INL typename SList<T>::Iterator SList<T>::begin() const
	{
		return Iterator(m_pHead);
	}

	template <typename T>
	HV_FORCE_INL typename SList<T>::Iterator SList<T>::end() const
	{
		return Iterator(nullptr);
	}

	template <typename T>
	void SList<T>::Copy(const SList& list)
	{
		Clear();
		if (!m_pAlloc)
			m_pAlloc = list.m_pAlloc;
		m_Size = list.m_Size;
		Node *pPrev = &m_pHead, *pNode = &m_pHead;
		const Node* pOther = list.m_pHead;
		for (sizeT i = 0; i < m_Size; ++i, pOther = pOther->pNext)
		{
			pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			new (pNode) T(pOther->value);
			pPrev->pNext = pNode;
			pPrev = pNode;
		}
		m_pTail = pNode;
	}

}
