// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Flags.inl: Flags Wrapper
#pragma once
#include "Containers/Flags.h"

namespace Hv {
	
	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>::Flags()
		: m_Flags(0)
	{
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>::Flags(Enum flags)
		: m_Flags(Value(flags))
	{
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>::Flags(Value flags)
		: m_Flags(flags)
	{
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>::Flags(const Flags& flags)
		: m_Flags(flags.m_Flags)
	{
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>::Flags(Flags&& flags) noexcept
		: m_Flags(flags.m_Flags)
	{
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum> Flags<Value, Enum>::operator~() const
	{
		return Flags(~m_Flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum> Flags<Value, Enum>::operator+(const Flags& flags) const
	{
		return Flags(m_Flags | flags.m_Flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum> Flags<Value, Enum>::operator+(Enum flags) const
	{
		return Flags(m_Flags | Value(flags));
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum> Flags<Value, Enum>::operator+(Value flags) const
	{
		return Flags(m_Flags | flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum> Flags<Value, Enum>::operator-(const Flags& flags) const
	{
		return Flags(m_Flags & flags.m_Flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum> Flags<Value, Enum>::operator-(Enum flags) const
	{
		return Flags(m_Flags & Value(flags));
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum> Flags<Value, Enum>::operator-(Value flags) const
	{
		return Flags(m_Flags & ~flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum> Flags<Value, Enum>::operator&(const Flags& flags) const
	{
		return Flags(m_Flags & flags.m_Flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum> Flags<Value, Enum>::operator&(Enum flags) const
	{
		return Flags(m_Flags & Value(flags));
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum> Flags<Value, Enum>::operator&(Value flags) const
	{
		return Flags(m_Flags & flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum> Flags<Value, Enum>::operator|(const Flags& flags) const
	{
		return Flags(m_Flags | flags.m_Flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum> Flags<Value, Enum>::operator|(Enum flags) const
	{
		return Flags(m_Flags | Value(flags));
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum> Flags<Value, Enum>::operator|(Value flags) const
	{
		return Flags(m_Flags | flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum> Flags<Value, Enum>::operator^(const Flags& flags) const
	{
		return Flags(m_Flags ^ flags.m_Flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum> Flags<Value, Enum>::operator^(Enum flags) const
	{
		return Flags(m_Flags ^ Value(flags));
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum> Flags<Value, Enum>::operator^(Value flags) const
	{
		return Flags(m_Flags ^ flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator=(Enum flags)
	{
		m_Flags = Value(flags);
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator=(Value flags)
	{
		m_Flags = flags;
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator=(const Flags& flags)
	{
		m_Flags = flags.m_Flags;
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator=(Flags&& flags) noexcept
	{
		m_Flags = flags.m_Flags;
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator+=(const Flags& flags)
	{
		m_Flags |= flags.m_Flags;
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator+=(Enum flags)
	{
		m_Flags |= Value(flags);
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator+=(Value flags)
	{
		m_Flags |= flags;
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator-=(const Flags& flags)
	{
		m_Flags &= ~flags.m_Flags;
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator-=(Enum flags)
	{
		m_Flags &= ~Value(flags);
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator-=(Value flags)
	{
		m_Flags &= ~flags;
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator&=(const Flags& flags)
	{
		m_Flags &= flags.m_Flags;
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator&=(Enum flags)
	{
		m_Flags &= Value(flags);
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator&=(Value flags)
	{
		m_Flags &= flags;
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator|=(const Flags& flags)
	{
		m_Flags |= flags.m_Flags;
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator|=(Enum flags)
	{
		m_Flags |= Value(flags);
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator|=(Value flags)
	{
		m_Flags |= flags;
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator^=(const Flags& flags)
	{
		m_Flags ^= flags.m_Flags;
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator^=(Enum flags)
	{
		m_Flags ^= Value(flags);
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>& Flags<Value, Enum>::operator^=(Value flags)
	{
		m_Flags ^= flags;
		return *this;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL b8 Flags<Value, Enum>::operator==(const Flags& flags) const
	{
		return m_Flags == flags.m_Flags;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL b8 Flags<Value, Enum>::operator==(Enum flags) const
	{
		return m_Flags == Value(flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL b8 Flags<Value, Enum>::operator==(Value flags) const
	{
		return m_Flags == flags;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL b8 Flags<Value, Enum>::operator!=(const Flags& flags) const
	{
		return m_Flags != flags;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL b8 Flags<Value, Enum>::operator!=(Enum flags) const
	{
		return m_Flags != Value(flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL b8 Flags<Value, Enum>::operator!=(Value flags) const
	{
		return m_Flags != flags;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>::operator b8() const
	{
		return m_Flags;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>::operator u8() const
	{
		return u8(m_Flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>::operator u16() const
	{
		return u16(m_Flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>::operator u32() const
	{
		return u32(m_Flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL Flags<Value, Enum>::operator u64() const
	{
		return u64(m_Flags);
	}

	template <typename Value, typename Enum>
	Flags<Value, Enum>::operator Enum() const
	{
		return Enum(m_Flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL void Flags<Value, Enum>::Set(const Flags& flags)
	{
		m_Flags |= flags.m_Flags;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL void Flags<Value, Enum>::Set(Enum flags)
	{
		m_Flags |= Value(flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL void Flags<Value, Enum>::Set(Value flags)
	{
		m_Flags |= flags;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL void Flags<Value, Enum>::Set(const Flags& flags, b8 set)
	{
		if (set)
			m_Flags |= flags.m_Flags;
		else
			m_Flags &= ~flags.m_Flags;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL void Flags<Value, Enum>::Set(Enum flags, b8 set)
	{
		if (set)
			m_Flags |= Value(flags);
		else
			m_Flags &= ~Value(flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL void Flags<Value, Enum>::Set(Value flags, b8 set)
	{
		if (set)
			m_Flags |= flags;
		else
			m_Flags &= ~flags;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL void Flags<Value, Enum>::Unset(const Flags& flags)
	{
		m_Flags &= ~flags.m_Flags;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL void Flags<Value, Enum>::Unset(Enum flags)
	{
		m_Flags &= ~Value(flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL void Flags<Value, Enum>::Unset(Value flags)
	{
		m_Flags &= ~flags;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL b8 Flags<Value, Enum>::IsSet(const Flags& flags) const
	{
		return (m_Flags & flags.m_Flags) == flags.m_Flags;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL b8 Flags<Value, Enum>::IsSet(Enum flags) const
	{
		return (m_Flags & Value(flags)) == Value(flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL b8 Flags<Value, Enum>::IsSet(Value flags) const
	{
		return (m_Flags & flags) == flags;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL b8 Flags<Value, Enum>::IsNotSet(const Flags& flags) const
	{
		return !(m_Flags & flags.m_Flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL b8 Flags<Value, Enum>::IsNotSet(Enum flags) const
	{
		return !(m_Flags & Value(flags));
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL b8 Flags<Value, Enum>::IsNotSet(Value flags) const
	{
		return !(m_Flags & flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL b8 Flags<Value, Enum>::IsAnySet(const Flags& flags) const
	{
		return m_Flags & flags.m_Flags;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL b8 Flags<Value, Enum>::IsAnySet(Enum flags) const
	{
		return m_Flags & Value(flags);
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL b8 Flags<Value, Enum>::IsAnySet(Value flags) const
	{
		return m_Flags & flags;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL b8 Flags<Value, Enum>::None() const
	{
		return !m_Flags;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL b8 Flags<Value, Enum>::Any() const
	{
		return m_Flags;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL b8 Flags<Value, Enum>::All() const
	{
		return ~m_Flags == 0;
	}

	template <typename Value, typename Enum>
	HV_FORCE_INL void Flags<Value, Enum>::Swap(Flags& flags)
	{
		Hv::Swap(m_Flags, flags.m_Flags);
	}

}