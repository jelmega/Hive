// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HashTable.inl: Hash table
#pragma once
#include "Containers/HashTable.h"

namespace Hv {
	
	namespace Container {

		template <typename T, b8 Cache>
		HV_FORCE_INL HashNode<T, Cache>::HashNode()
			: pNext(nullptr)
		{
		}

		template <typename T, b8 Cache>
		HV_FORCE_INL HashNode<T, Cache>::HashNode(const T& val, HashNode* pNext)
			: pNext(pNext)
		{
			if constexpr (UseCopyConstructor<T>)
				new (&value) T(val);
			else
				value = val;
		}

		template <typename T, b8 Cache>
		HV_FORCE_INL HashNode<T, Cache>::HashNode(T&& val, HashNode* pNext)
			: pNext(pNext)
		{
			if constexpr (UseCopyConstructor<T>)
				new (&value) T(Forward<T>(val));
			else
				value = Forward<T>(val);
		}

		template <typename T, b8 Cache>
		HV_FORCE_INL HashNode<T, Cache>::HashNode(const T& val, hashT hash, HashNode* pNext)
			: pNext(pNext)
		{
			HV_UNREFERENCED_PARAM(hash);
			if constexpr (UseCopyConstructor<T>)
				new (&value) T(val);
			else
				value = val;
		}

		template <typename T, b8 Cache>
		HV_FORCE_INL HashNode<T, Cache>::HashNode(T&& val, hashT hash, HashNode* pNext)
			: pNext(pNext)
		{
			HV_UNREFERENCED_PARAM(hash);
			if constexpr (UseCopyConstructor<T>)
				new (&value) T(Forward<T>(val));
			else
				value = Forward<T>(val);
		}

		template <typename T, b8 Cache>
		HV_FORCE_INL HashNode<T, Cache>::HashNode(const HashNode& node)
			: pNext(node.pNext)
		{
			if constexpr (UseCopyConstructor<T>)
				new (&value) T(node.value);
			else
				value = node.value;
		}

		template <typename T, b8 Cache>
		HV_FORCE_INL HashNode<T, Cache>::HashNode(HashNode&& node) noexcept
		{
			if constexpr (UseCopyConstructor<T>)
				new (&value) T(Forward<T>(node.value));
			else
				value = Forward<T>(node.value);
		}

		template <typename T, b8 Cache>
		HV_FORCE_INL HashNode<T, Cache>::~HashNode()
		{
			value.~T();
		}

		template <typename T, b8 Cache>
		HV_FORCE_INL hashT HashNode<T, Cache>::GetHash() const
		{
			return 0;
		}

		template <typename T, b8 Cache>
		HV_FORCE_INL void HashNode<T, Cache>::SetHash(hashT hash)
		{
			HV_UNREFERENCED_PARAM(hash);
		}

		template <typename T>
		HV_FORCE_INL HashNode<T, true>::HashNode()
			: hash(0)
			, pNext(nullptr)
		{
		}

		template <typename T>
		HV_FORCE_INL HashNode<T, true>::HashNode(const T& val, HashNode* pNext)
			: hash(0)
			, pNext(pNext)
		{
			if constexpr (UseCopyConstructor<T>)
				new(&value) T(val);
			else
				value = val;
		}

		template <typename T>
		HV_FORCE_INL HashNode<T, true>::HashNode(T&& val, HashNode* pNext)
			: hash(0)
			, pNext(pNext)
		{
			if constexpr (UseCopyConstructor<T>)
				new(&value) T(Forward<T>(val));
			else
				value = Forward<T>(val);
		}

		template <typename T>
		HV_FORCE_INL HashNode<T, true>::HashNode(const T& val, hashT hash, HashNode* pNext)
			: hash(hash)
			, pNext(pNext)
		{
			if (UseCopyConstructor<T>)
				new(&value) T(val);
			else
				value = val;
		}

		template <typename T>
		HV_FORCE_INL HashNode<T, true>::HashNode(T&& val, hashT hash, HashNode* pNext)
			: hash(hash)
			, pNext(pNext)
		{
			if constexpr (UseCopyConstructor<T>)
				new(&value) T(Forward<T>(val));
			else
				value = Forward<T>(val);
		}

		template <typename T>
		HV_FORCE_INL HashNode<T, true>::HashNode(const HashNode& node)
			: hash(node.hash)
			, pNext(node.pNext)
		{
			if constexpr (UseCopyConstructor<T>)
				new(&value) T(node.value);
			else
				value = node.value;
		}

		template <typename T>
		HV_FORCE_INL HashNode<T, true>::HashNode(HashNode&& node) noexcept
			: hash(node.hash)
			, pNext(node.pNext)
		{
			if constexpr (UseCopyConstructor<T>)
				new (&value) T(Forward<T>(node.value));
			else
				value = Forward<T>(node.value);
		}

		template <typename T>
		HV_FORCE_INL HashNode<T, true>::~HashNode()
		{
			value.~T();
		}

		template <typename T>
		HV_FORCE_INL hashT HashNode<T, true>::GetHash() const
		{
			return hash;
		}

		template <typename T>
		HV_FORCE_INL void HashNode<T, true>::SetHash(hashT hash)
		{
			this->hash = hash;
		}

	}

	////////////////////////////////////////////////////////////////////////////////

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator::Iterator()
		: m_ppBucket(nullptr)
		, m_pNode(nullptr)
		, m_BucketIndex(0)
		, m_BucketCount(0)
	{
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator::Iterator(const Iterator& it)
		: m_ppBucket(it.m_ppBucket)
		, m_pNode(it.m_pNode)
		, m_BucketIndex(it.m_BucketIndex)
		, m_BucketCount(it.m_BucketCount)
	{
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator::Iterator(Iterator&& it) noexcept
		: m_ppBucket(it.m_ppBucket)
		, m_pNode(it.m_pNode)
		, m_BucketIndex(it.m_BucketIndex)
		, m_BucketCount(it.m_BucketCount)
	{
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator::~Iterator()
	{
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_INL typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator& HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator::operator++()
	{
		HV_ASSERT_MSG(m_pNode, "Can't increment iterator!");

		if (m_pNode->pNext)
		{
			m_pNode = m_pNode->pNext;
		}
		else
		{
			++m_BucketIndex;
			++m_ppBucket;

			while (!*m_ppBucket && m_BucketIndex < m_BucketCount)
			{
				++m_BucketIndex;
				++m_ppBucket;
			}

			if (m_BucketIndex == m_BucketCount)
			{
				m_ppBucket = nullptr;
				m_pNode = nullptr;
			}
			else
				m_pNode = *m_ppBucket;
		}
		return *this;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator::operator++(int)
	{
		Iterator it(*this);
		++*this;
		return it;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator::operator+(sizeT count)
	{
		Iterator it(*this);
		for (sizeT i = 0; i < count; ++i)
			++it;
		return it;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator& HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator::operator=(const Iterator& it)
	{
		m_ppBucket = it.m_ppBucket;
		m_pNode = it.m_pNode;
		m_BucketIndex = it.m_BucketIndex;
		m_BucketCount = it.m_BucketCount;
		return *this;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator& HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator::operator=(Iterator&& it) noexcept
	{
		m_ppBucket = it.m_ppBucket;
		m_pNode = it.m_pNode;
		m_BucketIndex = it.m_BucketIndex;
		m_BucketCount = it.m_BucketCount;
		return *this;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator& HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator::operator+=(sizeT count)
	{
		for (sizeT i = 0; i < count; ++i)
			++*this;
		return *this;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL typename std::conditional<MutableIterator, T&, const T&>::type HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator::operator*()
	{
		return m_pNode->value;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL const T& HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator::operator*() const
	{
		return m_pNode->value;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL typename std::conditional<MutableIterator, T*, const T*>::type HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator::operator->()
	{
		return &m_pNode->value;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL const T* HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator::operator->() const
	{
		return &m_pNode->value;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL b8 HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator::operator==(const Iterator& it) const
	{
		// Just check bucket and node, should be enough
		return m_ppBucket == it.m_ppBucket && m_pNode == it.m_pNode;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL b8 HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator::operator!=(const Iterator& it) const
	{
		// Just check bucket and node, should be enough
		return m_ppBucket != it.m_ppBucket || m_pNode != it.m_pNode;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator::Iterator(Node** pBucket, Node* pNode, sizeT bucketIndex, sizeT bucketCount)
		: m_ppBucket(pBucket)
		, m_pNode(pNode)
		, m_BucketIndex(bucketIndex)
		, m_BucketCount(bucketCount)
	{
	}

	////////////////////////////////////////////////////////////////////////////////

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::HashTable(Memory::IAllocator* pAlloc)
		: m_ppBuckets(nullptr)
		, m_BucketCount(0)
		, m_Size(0)
		, m_MaxLoadFactor(m_DefaultMaxLoadFactor)
		, m_pAlloc(pAlloc)
	{
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::HashTable(const KeyCompare& compare, Memory::IAllocator* pAlloc)
		: m_ppBuckets(nullptr)
		, m_BucketCount(0)
		, m_Size(0)
		, m_MaxLoadFactor(m_DefaultMaxLoadFactor)
		, m_Comp(compare)
		, m_pAlloc(pAlloc)
	{
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::HashTable(const Hash& hash, Memory::IAllocator* pAlloc)
		: m_ppBuckets(nullptr)
		, m_BucketCount(0)
		, m_Size(0)
		, m_MaxLoadFactor(m_DefaultMaxLoadFactor)
		, m_Hash(hash)
		, m_pAlloc(pAlloc)
	{
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::HashTable(const KeyCompare& compare, const Hash& hash, Memory::IAllocator* pAlloc)
		: m_ppBuckets(nullptr)
		, m_BucketCount(0)
		, m_Size(0)
		, m_MaxLoadFactor(m_DefaultMaxLoadFactor)
		, m_Comp(compare)
		, m_Hash(hash)
		, m_pAlloc(pAlloc)
	{
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::HashTable(const ExtractKey& extract, Memory::IAllocator* pAlloc)
		: m_ppBuckets(nullptr)
		, m_BucketCount(0)
		, m_Size(0)
		, m_MaxLoadFactor(m_DefaultMaxLoadFactor)
		, m_Extract(extract)
		, m_pAlloc(pAlloc)
	{
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::HashTable(const KeyCompare& compare, const Hash& hash, const ExtractKey& extract, Memory::IAllocator* pAlloc)
		: m_ppBuckets(nullptr)
		, m_BucketCount(0)
		, m_Size(0)
		, m_MaxLoadFactor(m_DefaultMaxLoadFactor)
		, m_Comp(compare)
		, m_Hash(hash)
		, m_Extract(extract)
		, m_pAlloc(pAlloc)
	{
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_INL HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::HashTable(const HashTable& ht)
		: m_ppBuckets(nullptr)
		, m_BucketCount(ht.m_BucketCount)
		, m_Size(ht.m_Size)
		, m_MaxLoadFactor(ht.m_MaxLoadFactor)
		, m_Comp(ht.m_Comp)
		, m_Hash(ht.m_Hash)
		, m_Extract(ht.m_Extract)
		, m_pAlloc(ht.m_pAlloc)
	{
		Copy(ht);
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_INL HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::HashTable(const HashTable& ht, Memory::IAllocator* pAlloc)
		: m_ppBuckets(nullptr)
		, m_BucketCount(0)
		, m_Size(0)
		, m_MaxLoadFactor(0)
		, m_pAlloc(pAlloc)
	{
		Copy(ht);
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_INL HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::HashTable(HashTable&& ht) noexcept
		: m_ppBuckets(nullptr)
		, m_BucketCount(ht.m_BucketCount)
		, m_Size(ht.m_Size)
		, m_MaxLoadFactor(ht.m_MaxLoadFactor)
		, m_Comp(ht.m_Comp)
		, m_Hash(ht.m_Hash)
		, m_Extract(ht.m_Extract)
		, m_pAlloc(ht.m_pAlloc)
	{
		Hv::Swap(m_ppBuckets, ht.m_ppBuckets);
		Hv::Swap(m_BucketCount, ht.m_BucketCount);
		Hv::Swap(m_Size, ht.m_Size);
		Hv::Swap(m_MaxLoadFactor, ht.m_MaxLoadFactor);
		Hv::Swap(m_Comp, ht.m_Comp);
		Hv::Swap(m_Hash, ht.m_Hash);
		Hv::Swap(m_Extract, ht.m_Extract);
		Hv::Swap(m_pAlloc, ht.m_pAlloc);
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
		HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::~HashTable()
	{
		Clear(true);
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_INL HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>& HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::operator=(const HashTable& ht)
	{
		Clear();
		Copy(ht);
		return *this;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8
		MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>& HashTable<T,
	KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::operator=(
		HashTable&& ht) noexcept
	{
		Clear();
		if (m_pAlloc == ht.m_pAlloc)
		{
			Hv::Swap(m_ppBuckets, ht.m_ppBuckets);
			Hv::Swap(m_BucketCount, ht.m_BucketCount);
			Hv::Swap(m_Size, ht.m_Size);
			Hv::Swap(m_MaxLoadFactor, ht.m_MaxLoadFactor);
			Hv::Swap(m_Comp, ht.m_Comp);
			Hv::Swap(m_Hash, ht.m_Hash);
			Hv::Swap(m_Extract, ht.m_Extract);
			Hv::Swap(m_pAlloc, ht.m_pAlloc);
		}
		else
		{
			Copy(ht);
		}
		return *this;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_INL Pair<typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator, b8> HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Insert(const T& val)
	{
		if (!m_ppBuckets)
			Reserve(1);

		hashT hash = m_Hash(m_Extract(val));
		sizeT index = sizeT(hash % m_BucketCount);

		if (UniqueKey && m_ppBuckets[index])
		{
			Node* pNode = m_ppBuckets[index];
			while (pNode)
			{
				if (m_Comp(m_Extract(pNode->value), m_Extract(val)))
					return Pair<Iterator, b8>(Iterator(m_ppBuckets, pNode, index, m_BucketCount), false);
				pNode = pNode->pNext;
			}
		}

		f32 loadFactor = f32(m_Size + 1) / f32(m_BucketCount);
		if (loadFactor > m_MaxLoadFactor)
			Reserve(m_Size + 1);

		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(val, hash);
		pNode->pNext = m_ppBuckets[index];
		m_ppBuckets[index] = pNode;
		++m_Size;
		return Pair<Iterator, b8>(Iterator(m_ppBuckets, pNode, index, m_BucketCount), true);
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_INL Pair<typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator, b8> HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Insert(T&& val)
	{
		if (!m_ppBuckets)
			Reserve(1);

		hashT hash = m_Hash(m_Extract(val));
		sizeT index = sizeT(hash % m_BucketCount);

		if (UniqueKey && m_ppBuckets[index])
		{
			Node* pNode = m_ppBuckets[index];
			while (pNode)
			{
				if (m_Comp(m_Extract(pNode->value), m_Extract(val)))
				{
					return Pair<Iterator, b8>(Iterator(m_ppBuckets, pNode, index, m_BucketCount), false);
				}
				pNode = pNode->pNext;
			}
		}

		f32 loadFactor = f32(m_Size + 1) / f32(m_BucketCount);
		if (loadFactor > m_MaxLoadFactor)
		{
			Reserve(m_Size + 1);
			index = sizeT(hash % m_BucketCount);
		}

		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(Forward<T>(val), hash);
		pNode->pNext = m_ppBuckets[index];
		m_ppBuckets[index] = pNode;
		++m_Size;
		return Pair<Iterator, b8>(Iterator(m_ppBuckets, pNode, index, m_BucketCount), true);
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <typename ... Args>
	HV_INL Pair<typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator, b8> HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Emplace(Args&&... args)
	{
		if (!m_ppBuckets)
			Reserve(1);

		T val(args...);
		hashT hash = m_Hash(m_Extract(val));
		sizeT index = sizeT(hash % m_BucketCount);

		if (UniqueKey && m_ppBuckets[index])
		{
			Node* pNode = m_ppBuckets[index];
			while (pNode)
			{
				if (m_Comp(m_Extract(pNode->value), m_Extract(val)))
				{
					val.~T();
					return Pair<Iterator, b8>(Iterator(m_ppBuckets, pNode, index, m_BucketCount), false);
				}
				pNode = pNode->pNext;
			}
		}

		sizeT loadFactor = f32(m_Size + 1) / f32(m_BucketCount);
		if (loadFactor > m_MaxLoadFactor)
			Reserve(m_Size + 1);

		Node* pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		new (pNode) Node(Forward<T>(val), hash);
		pNode->pNext = m_ppBuckets[index];
		m_ppBuckets[index] = pNode;
		++m_Size;
		return Pair<Iterator, b8>(Iterator(m_ppBuckets, pNode, index, m_BucketCount), true);
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_INL typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Erase(const T& val)
	{
		hashT hash = m_Hash(m_Extract(val));
		sizeT index = sizeT(hash % m_BucketCount);

		if (m_ppBuckets[index])
		{
			Node* pNode = m_ppBuckets[index], *pPrev = nullptr;
			while (pNode)
			{
				if (m_Comp(m_Extract(pNode->value), m_Extract(val)))
				{
					Iterator it(m_ppBuckets, pNode, index, m_BucketCount);
					++it;

					if (pPrev)
						pPrev->pNext = pNode->pNext;
					else
						m_ppBuckets[index] = pNode->pNext;

					pNode->~Node();
					m_pAlloc->Free(pNode);
					--m_Size;
					return it;
				}
				pPrev = pNode;
				pNode = pNode->pNext;
			}
		}
		return Iterator();
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <typename Key>
	HV_INL typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::EraseKey(const Key& key)
	{
		hashT hash = m_Hash(key);
		sizeT index = sizeT(hash % m_BucketCount);

		if (m_ppBuckets[index])
		{
			Node* pNode = m_ppBuckets[index], *pPrev = nullptr;
			while (pNode)
			{
				if (m_Comp(m_Extract(pNode->value), key))
				{
					Iterator it(m_ppBuckets, pNode, index, m_BucketCount);
					++it;

					if (pPrev)
						pPrev->pNext = pNode->pNext;
					else
						m_ppBuckets[index] = pNode->pNext;

					pNode->~Node();
					m_pAlloc->Free(pNode);
					--m_Size;
					return it;
				}
				pPrev = pNode;
				pNode = pNode->pNext;
			}
		}
		return Iterator();
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_INL typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Erase(const Iterator& it)
	{
		HV_ASSERT_MSG(it.m_ppBucket == m_ppBuckets && it.m_pNode && it.m_BucketCount == m_BucketCount, "Iterator out of range!");

		if (m_ppBuckets[it.m_BucketIndex])
		{
			Node* pNode = m_ppBuckets[it.m_BucketIndex], *pPrev = nullptr;
			while (pNode)
			{
				if (pNode == it.m_pNode)
				{
					Iterator itTemp(it);
					++itTemp;

					if (pPrev)
						pPrev->pNext = pNode->pNext;
					else
						m_ppBuckets[it.m_BucketIndex] = pNode->pNext;

					pNode->~Node();
					m_pAlloc->Free(pNode);
					--m_Size;
					return itTemp;
				}
				pPrev = pNode;
				pNode = pNode->pNext;
			}
		}
		return Iterator();
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_INL void HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Clear(b8 resize)
	{
		if (m_ppBuckets)
		{
			for (sizeT i = 0; i < m_BucketCount; ++i)
			{
				if (m_ppBuckets[i])
				{
					Node* pNode = m_ppBuckets[i];
					while (pNode)
					{
						Node* pNext = pNode->pNext;
						pNode->~Node();
						m_pAlloc->Free(pNode);
						pNode = pNext;
					}
				}
			}
			m_Size = 0;

			if (resize)
			{
				m_pAlloc->Free(m_ppBuckets);
				m_ppBuckets = nullptr;
				m_BucketCount = 0;
			}
		}
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_INL typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Find(const T& val) const
	{
		if (!m_ppBuckets)
			return Iterator();

		hashT hash = m_Hash(m_Extract(val));
		sizeT index = sizeT(hash % m_BucketCount);

		if (m_ppBuckets[index])
		{
			Node* pNode = m_ppBuckets[index];
			while (pNode)
			{
				if (m_Comp(m_Extract(pNode->value), m_Extract(val)))
					return Iterator(m_ppBuckets, pNode, index, m_BucketCount);
				pNode = pNode->pNext;
			}
		}
		return Iterator();
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <typename Key>
	HV_INL typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::FindKey(const Key& key) const
	{
		if (!m_ppBuckets)
			return Iterator();

		hashT hash = m_Hash(key);
		sizeT index = sizeT(hash % m_BucketCount);

		if (m_ppBuckets[index])
		{
			Node* pNode = m_ppBuckets[index];
			while (pNode)
			{
				if (m_Comp(m_Extract(pNode->value), key))
					return Iterator(m_ppBuckets, pNode, index, m_BucketCount);
				pNode = pNode->pNext;
			}
		}
		return Iterator();
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <typename U, typename Pred>
	HV_INL typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::FindAs(const U& val, const Pred& pred) const
	{
		if (!m_ppBuckets)
			return Iterator();
		for (sizeT i = 0; i < m_BucketCount; ++i)
		{
			if (m_ppBuckets[i])
			{
				Node* pNode = m_ppBuckets[i];
				while (pNode)
				{
					if (pred(pNode->value, val))
						return Iterator(m_ppBuckets, pNode, i, m_BucketCount);
					pNode = pNode->pNext;
				}
			}
		}
		return Iterator();
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_INL sizeT HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Count(const T& val) const
	{
		if (!m_ppBuckets)
			return 0;

		hashT hash = m_Hash(m_Extract(val));
		sizeT index = sizeT(hash % m_BucketCount);

		sizeT count = 0;
		if (m_ppBuckets[index])
		{
			Node* pNode = m_ppBuckets[index];
			while (pNode)
			{
				if (m_Comp(m_Extract(pNode->value), m_Extract(val)))
					++count;
				pNode = pNode->pNext;
			}
		}
		return count;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <typename Key>
	HV_INL sizeT HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::CountKey(const Key& key) const
	{
		if (!m_ppBuckets)
			return 0;

		hashT hash = m_Hash(key);
		sizeT index = sizeT(hash % m_BucketCount);

		sizeT count = 0;
		if (m_ppBuckets[index])
		{
			Node* pNode = m_ppBuckets[index];
			while (pNode)
			{
				if (m_Comp(m_Extract(pNode->value), key))
					++count;
				pNode = pNode->pNext;
			}
		}
		return count;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_INL void HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Rehash(sizeT buckets)
	{
		Node** ppBuckets = (Node**)m_pAlloc->Allocate(buckets * sizeof(Node*), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
		for (sizeT i = 0; i < m_BucketCount; ++i)
		{
			if (m_ppBuckets[i])
			{
				Node* pNode = m_ppBuckets[i];
				while (pNode)
				{
					hashT hash;
					if constexpr (CacheHash)
						hash = pNode->GetHash();
					else
						hash = m_Hash(m_Extract(pNode->value));
					sizeT index = sizeT(hash % buckets);

					Node* pNext = pNode->pNext;

					pNode->pNext = ppBuckets[index];
					ppBuckets[index] = pNode;

					pNode = pNext;
				}
			}
		}
		if (m_ppBuckets)
			m_pAlloc->Free(m_ppBuckets);
		m_ppBuckets = ppBuckets;
		m_BucketCount = buckets;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL void HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Reserve(sizeT size)
	{
		f32 loadFactor;
		if (m_BucketCount == 0)
			loadFactor = m_MaxLoadFactor + 0.0001f;
		else
			loadFactor = f32(size) / f32(m_BucketCount);
		if (loadFactor > m_MaxLoadFactor)
		{
			sizeT temp = sizeT(loadFactor + 0.99f) + 1;
			Rehash(BucketGrowthPolicy(temp));
		}
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_INL void HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Swap(HashTable& ht)
	{
		if (m_pAlloc == ht.m_pAlloc)
		{
			Hv::Swap(m_ppBuckets, ht.m_ppBuckets);
			Hv::Swap(m_BucketCount, ht.m_BucketCount);
			Hv::Swap(m_Size, ht.m_Size);
			Hv::Swap(m_MaxLoadFactor, ht.m_MaxLoadFactor);
			Hv::Swap(m_Comp, ht.m_Comp);
			Hv::Swap(m_Hash, ht.m_Hash);
			Hv::Swap(m_Extract, ht.m_Extract);
			Hv::Swap(m_pAlloc, ht.m_pAlloc);
		}
		else
		{
			HashTable tmp(Move(*this), m_pAlloc);
			*this = Move(ht);
			ht = Move(tmp);
		}
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL sizeT HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::BucketCount() const
	{
		return m_BucketCount;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_INL sizeT HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::BucketSize(sizeT bucket) const
	{
		HV_ASSERT_MSG(bucket < m_BucketCount, "Bucket out of range!");

		if (!m_ppBuckets[bucket])
			return 0;

		sizeT count = 0;
		Node* pNode = m_ppBuckets[bucket];
		while (pNode)
		{
			++count;
			pNode = pNode->pNext;
		}
		return count;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL sizeT HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Bucket(const T& val) const
	{
		hashT hash = m_Hash(m_Extract(val));
		return sizeT(hash % m_BucketCount);
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	template <typename Key>
	HV_FORCE_INL sizeT HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::BucketKey(const Key& key) const
	{
		hashT hash = m_Hash(key);
		return sizeT(hash % m_BucketCount);
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL sizeT HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::LoadFactor() const
	{
		return f32(m_Size) / f32(m_BucketCount);
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL void HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::MaxLoadFactor(sizeT maxLoadFactor)
	{
		m_MaxLoadFactor = maxLoadFactor;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL sizeT HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::MaxLoadFactor() const
	{
		return m_MaxLoadFactor;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL b8 HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::IsEmpty() const
	{
		return m_Size == 0;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL sizeT HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Size() const
	{
		return m_Size;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL Memory::IAllocator* HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::GetAllocator() const
	{
		return m_pAlloc;
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_INL typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Front() const
	{
		if (m_ppBuckets)
		{
			for (sizeT i = 0; i < m_BucketCount; ++i)
			{
				if (m_ppBuckets[i])
				{
					return Iterator(m_ppBuckets, m_ppBuckets[i], i, m_BucketCount);
				}
			}
		}
		return Iterator();
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_INL typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Last() const
	{
		if (m_ppBuckets)
		{
			for (sizeT i = 0; i < m_BucketCount; ++i)
			{
				sizeT index = m_BucketCount - i;
				if (m_ppBuckets[index])
				{
					Node* pNode = m_ppBuckets[index];
					while (pNode->pNext)
					{
						pNode = pNode->pNext;
					}
					return Iterator(m_ppBuckets, pNode, index, m_BucketCount);
				}
			}
		}
		return Iterator();
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Back() const
	{
		return Iterator();
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_INL typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::begin() const
	{
		if (m_ppBuckets)
		{
			for (sizeT i = 0; i < m_BucketCount; ++i)
			{
				if (m_ppBuckets[i])
				{
					return Iterator(m_ppBuckets + i, m_ppBuckets[i], i, m_BucketCount);
				}
			}
		}
		return Iterator();
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	HV_FORCE_INL typename HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Iterator HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::end() const
	{
		return Iterator();
	}

	template <typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8
		MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	void HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>::Copy(
		const HashTable& ht)
	{
		if (!m_pAlloc)
			m_pAlloc = ht.m_pAlloc;
		m_BucketCount = ht.m_BucketCount;
		m_Size = ht.m_Size;
		m_MaxLoadFactor = ht.m_MaxLoadFactor;
		m_Comp = ht.m_Comp;
		m_Hash = ht.m_Hash;
		m_Extract = ht.m_Extract;

		if (m_Size > 0)
		{
			m_ppBuckets = (Node**)m_pAlloc->Allocate(m_BucketCount * sizeof(Node*), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));

			for (sizeT i = 0; i < m_BucketCount; ++i)
			{
				Node* pOrig = ht.m_ppBuckets[i];
				Node* pNode = nullptr, *pPrev = nullptr;
				while (pOrig)
				{
					pNode = (Node*)m_pAlloc->Allocate(sizeof(Node), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
					new (pNode) Node(*pOrig);
					if (pPrev)
						pPrev->pNext = pNode;
					else
						m_ppBuckets[i] = pNode;
					pPrev = pNode;
					pOrig = pOrig->pNext;
				}
				if (pNode)
					pNode->pNext = nullptr;
				else
					m_ppBuckets[i] = nullptr;
			}
		}
	}

}
