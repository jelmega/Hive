// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DynArray.inl: Dynamic array
#pragma once
#include "Containers/DynArray.h"

namespace Hv {
	
	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL DynArray<T, GrowthPolicy>::DynArray()
		: m_Data(nullptr)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(g_pAllocator)
	{
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL DynArray<T, GrowthPolicy>::DynArray(Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL DynArray<T, GrowthPolicy>::DynArray(sizeT size, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(size > 0, "Size to reserve needs to be bigger than 0!");
		Resize(size);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL DynArray<T, GrowthPolicy>::DynArray(sizeT count, const T& value, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Fill(value, count);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	HV_INL DynArray<T, GrowthPolicy>::DynArray(T(&arr)[N], Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		Assign(arr, N);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL DynArray<T, GrowthPolicy>::DynArray(const T* arr, sizeT count, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Assign(arr, count);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL DynArray<T, GrowthPolicy>::DynArray(const T* itFirst, const T* itLast, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		Assign(itFirst, sizeT(itLast - itFirst));
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename Iterator>
	HV_INL DynArray<T, GrowthPolicy>::DynArray(const Iterator& itFirst, const Iterator& itLast, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		Assign(itFirst, itLast);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL DynArray<T, GrowthPolicy>::DynArray(std::initializer_list<T> il, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Assign(il.begin(), il.size());
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc OtherPolicy>
	HV_INL DynArray<T, GrowthPolicy>::DynArray(const DynArray<T, OtherPolicy>& dynArr)
		: m_Data(nullptr)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(g_pAllocator)
	{
		Assign(dynArr.m_Data, dynArr.m_Size);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL DynArray<T, GrowthPolicy>::DynArray(const DynArray& dynArr)
		: m_Data(nullptr)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(g_pAllocator)
	{
		if (dynArr.m_Size > 0)
			Assign(dynArr.m_Data, dynArr.m_Size);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc OtherPolicy>
	HV_INL DynArray<T, GrowthPolicy>::DynArray(const DynArray<T, OtherPolicy>& dynArr, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(pAlloc)
	{
		if (dynArr.m_Size > 0)
			Assign(dynArr.m_Data, dynArr.m_Size);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL DynArray<T, GrowthPolicy>::DynArray(const DynArray& dynArr, Memory::IAllocator* pAlloc)
		: m_Data(nullptr)
		, m_Size(dynArr.m_Size)
		, m_Capacity(dynArr.m_Capacity)
		, m_pAlloc(pAlloc)
	{
		if (dynArr.m_Size > 0)
			Assign(dynArr.m_Data, dynArr.m_Size);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc OtherPolicy>
	HV_INL DynArray<T, GrowthPolicy>::DynArray(DynArray<T, OtherPolicy>&& dynArr)
		: m_Data(nullptr)
		, m_Size(0)
		, m_Capacity(0)
		, m_pAlloc(dynArr.m_pAlloc)
	{
		Hv::Swap(m_Data, dynArr.m_Data);
		Hv::Swap(m_Size, dynArr.m_Size);
		Hv::Swap(m_Capacity, dynArr.m_Capacity);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL DynArray<T, GrowthPolicy>::DynArray(DynArray&& dynArr) noexcept
		: m_Data(nullptr)
		, m_Size(dynArr.m_Size)
		, m_Capacity(dynArr.m_Capacity)
		, m_pAlloc(dynArr.m_pAlloc)
	{
		Hv::Swap(m_Data, dynArr.m_Data);
		Hv::Swap(m_Size, dynArr.m_Size);
		Hv::Swap(m_Capacity, dynArr.m_Capacity);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL DynArray<T, GrowthPolicy>::~DynArray()
	{
		Clear(true);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	HV_INL DynArray<T, GrowthPolicy>& DynArray<T, GrowthPolicy>::operator=(T(&arr)[N])
	{
		Assign(arr, N);
		return *this;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL DynArray<T, GrowthPolicy>& DynArray<T, GrowthPolicy>::operator=(std::initializer_list<T> il)
	{
		Assign(il.begin(), il.size());
		return *this;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc OtherPolicy>
	HV_INL DynArray<T, GrowthPolicy>& DynArray<T, GrowthPolicy>::operator=(const DynArray<T, OtherPolicy>& dynArr)
	{	
		if (dynArr.m_Size == 0)
			return *this;
		Assign(dynArr.m_Data, dynArr.m_Size);
		return *this;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL DynArray<T, GrowthPolicy>& DynArray<T, GrowthPolicy>::operator=(const DynArray& dynArr)
	{
		if (!m_pAlloc)
			m_pAlloc = dynArr.m_pAlloc;
		if (dynArr.m_Size == 0)
			return *this;
		HV_ASSERT(this != &dynArr);
		Assign(dynArr.m_Data, dynArr.m_Size);
		return *this;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc OtherPolicy>
	HV_FORCE_INL DynArray<T, GrowthPolicy>& DynArray<T, GrowthPolicy>::operator=(DynArray<T, OtherPolicy>&& dynArr)
	{
		if (!m_pAlloc)
			m_pAlloc = dynArr.m_pAlloc;
		if (m_pAlloc == dynArr.m_pAlloc)
		{
			Hv::Swap(m_Data, dynArr.m_Data);
			Hv::Swap(m_Size, dynArr.m_Size);
			Hv::Swap(m_Capacity, dynArr.m_Capacity);
		}
		else
		{
			if (dynArr.m_Size == 0)
				return *this;
			Assign(dynArr.m_Data, dynArr.m_Size);
		}
		return *this;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL DynArray<T, GrowthPolicy>& DynArray<T, GrowthPolicy>::operator=(DynArray&& dynArr) noexcept
	{
		HV_ASSERT(this != &dynArr);
		if (m_pAlloc == dynArr.m_pAlloc)
		{
			Hv::Swap(m_Data, dynArr.m_Data);
			Hv::Swap(m_Size, dynArr.m_Size);
			Hv::Swap(m_Capacity, dynArr.m_Capacity);
		}
		else
		{
			m_pAlloc = dynArr.m_pAlloc;
			if (dynArr.m_Size == 0)
				return *this;
			Assign(dynArr.m_Data, dynArr.m_Size);
		}
		return *this;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL T& DynArray<T, GrowthPolicy>::operator[](sizeT index)
	{
		HV_ASSERT_MSG(index < m_Size, "Index out of range!");
		return m_Data[index];
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL const T& DynArray<T, GrowthPolicy>::operator[](sizeT index) const
	{
		HV_ASSERT_MSG(index < m_Size, "Index out of range!");
		return m_Data[index];
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Fill(const T& val, sizeT count)
	{
		Clear(false);
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		m_Size = count;
		Reserve(m_Size);
		if constexpr (Container::UseCopyConstructor<T>)
		{
			for (sizeT i = 0; i < m_Size; ++i)
				new (m_Data + i) T(val);
		}
		else
		{
			for (sizeT i = 0; i < m_Size; ++i)
				m_Data[i] = val;
		}
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	HV_INL void DynArray<T, GrowthPolicy>::Assign(const T(&arr)[N])
	{
		Assign(arr, N);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Assign(const T* arr, sizeT count)
	{
		Clear(false);
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		m_Size = count;
		Reserve(count);
		if constexpr (Container::UseMemCpy<T>)
		{
			Memory::Copy(m_Data, arr, m_Size * sizeof(T));
		}
		else if constexpr (Container::UseCopyConstructor<T>)
		{
			for (sizeT i = 0; i < m_Size; ++i)
				new (m_Data + i) T(arr[i]);
		}
		else
		{
			for (sizeT i = 0; i < m_Size; ++i)
				m_Data[i] = arr[i];
		}
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Assign(const T* itFirst, const T* itLast)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last iterator can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		Assign(itFirst, sizeT(itLast - itFirst));
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename Iterator>
	HV_INL void DynArray<T, GrowthPolicy>::Assign(const Iterator& itFirst, const Iterator& itLast)
	{
		Clear(false);
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		m_Size = 0;
		for (Iterator it = itFirst; it != itLast; ++it)
			++m_Size;
		Reserve(m_Size);
		if constexpr (Container::UseMemCpy<T>)
		{
			Memory::Copy(m_Data, itFirst, m_Size * sizeof(T));
		}
		else if constexpr (Container::UseCopyConstructor<T>)
		{
			Iterator it = itFirst;
			for (sizeT i = 0; i < m_Size; ++i, ++it)
				new (m_Data + i) T(*it);
		}
		else
		{
			Iterator it = itFirst;
			for (sizeT i = 0; i < m_Size; ++i, ++it)
				m_Data[i] = *it;
		}
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Assign(std::initializer_list<T> il)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Assign(il.begin(), il.end());
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL void DynArray<T, GrowthPolicy>::Resize(sizeT size)
	{
		if (size > m_Size)
			Reserve(size);
		m_Size = size;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Resize(sizeT size, const T& val)
	{
		if (size > m_Size)
		{
			Reserve(size);
			if constexpr (Container::UseCopyConstructor<T>)
			{
				for (sizeT i = 0; i < size; ++i)
					new (m_Data + m_Size + i) T(val);
			}
			else
			{
				for (sizeT i = 0; i < size; ++i)
					m_Data[m_Size + i] = val;
			}
		}

		m_Size = size;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Reserve(sizeT size)
	{
		if (size > m_Capacity)
		{
			T* old = m_Data;
			m_Capacity = GrowthPolicy(size);
			m_Data = (T*)m_pAlloc->Allocate(m_Capacity * sizeof(T), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			if (old)
			{
				if (m_Size > 0)
					Memory::Copy(m_Data, old, m_Size * sizeof(T));
				m_pAlloc->Free(old);
			}
		}
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::ShrinkToFit()
	{
		if (m_Size == 0)
		{
			Clear(true);
		}
		else
		{
			T* old = m_Data;
			m_Capacity = m_Size;
			m_Data = (T*)m_pAlloc->Allocate(m_Capacity * sizeof(T), 8, HV_ALLOC_CONTEXT(Memory::AllocCategory::Container));
			Memory::Copy(m_Data, old, m_Size * sizeof(T));
			m_pAlloc->Free(old);
		}
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Clear(bool resize)
	{
		if (resize && m_Data)
		{
			m_Capacity = 0;
			for (sizeT i = 0; i < m_Size; ++i)
				m_Data[i].~T();
			m_pAlloc->Free(m_Data);
			m_Data = nullptr;
		}
		else if (m_Data && m_Size)
		{
			Memory::Clear(m_Data, m_Size * sizeof(T));
		}
		m_Size = 0;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Push(const T& val)
	{
		Reserve(m_Size + 1);
		if constexpr (Container::UseCopyConstructor<T>)
			new (m_Data + m_Size) T(val);
		else
			m_Data[m_Size] = val;
		++m_Size;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Push(T&& val)
	{
		Reserve(m_Size + 1);
		if constexpr (Container::UseCopyConstructor<T>)
			new (m_Data + m_Size) T(Forward<T>(val));
		else
			m_Data[m_Size] = Forward<T>(val);
		++m_Size;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL void DynArray<T, GrowthPolicy>::Pop()
	{
		if (m_Size > 0)
		{
			--m_Size;
			m_Data[m_Size].~T();
		}
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <Container::GrowthPolicyFunc G>
	void DynArray<T, GrowthPolicy>::Append(DynArray<T, G> dynArr)
	{
		if (dynArr.m_Size == 0)
			return;

		Reserve(m_Size + dynArr.m_Size);
		
		if constexpr (Container::UseMemCpy<T>)
		{
			Memory::Copy(m_Data + m_Size, dynArr.m_Data, dynArr.m_Size * sizeof(T));
		}
		else if constexpr (Container::UseCopyConstructor<T>)
		{
			for (sizeT i = 0; i < dynArr.m_Size; ++i)
				new (m_Data + m_Size + i) T(dynArr.m_Data[i]);
		}
		else
		{
			for (sizeT i = 0; i < dynArr.m_Size; ++i)
				m_Data[m_Size + i] = dynArr.m_Data[i];
		}

		m_Size += dynArr.m_Size;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Insert(sizeT index, const T& val)
	{
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		Reserve(m_Size + 1);
		if (index != m_Size)
			Memory::Move(m_Data + index + 1, m_Data + index, (m_Size - index) * sizeof(T));

		if constexpr (Container::UseCopyConstructor<T>)
			new (m_Data + index) T(val);
		else
			m_Data[index] = val;

		++m_Size;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Insert(const T* it, const T& val)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Size, "Iterator out of range!");
		Insert(sizeT(it - m_Data), val);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Insert(sizeT index, T&& val)
	{
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		Reserve(m_Size + 1);
		if (index != m_Size)
			Memory::Move(m_Data + index + 1, m_Data + index, (m_Size - index) * sizeof(T));

		if constexpr (Container::UseCopyConstructor<T>)
			new (m_Data + index) T(Forward<T>(val));
		else
			m_Data[index] = Forward<T>(val);

		++m_Size;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Insert(const T* it, T&& val)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Size, "Iterator out of range!");
		Insert(sizeT(it - m_Data), Forward<T>(val));
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Insert(sizeT index, sizeT count, const T& val)
	{
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Reserve(m_Size + count);
		if (index != m_Size)
			Memory::Move(m_Data + index + count, m_Data + index, (m_Size - index) * sizeof(T));
		if constexpr (Container::UseCopyConstructor<T>)
		{
			for (sizeT i = 0; i < count; ++i)
				new (m_Data + index + i) T(val);
		}
		else
		{
			for (sizeT i = 0; i < count; ++i)
				m_Data[index + i] = val;
		}
		m_Size += count;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Insert(const T* it, sizeT count, const T& val)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Size, "Iterator out of range!");
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Insert(sizeT(it - m_Data), count, val);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	HV_INL void DynArray<T, GrowthPolicy>::Insert(sizeT index, T(&arr)[N])
	{
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		Insert(index, arr, N);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <sizeT N>
	HV_INL void DynArray<T, GrowthPolicy>::Insert(const T* it, T(&arr)[N])
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Size, "Iterator out of range!");
		Insert(sizeT(it - m_Data), arr, N);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Insert(sizeT index, T* arr, sizeT count)
	{
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Reserve(m_Size + count);
		if (index != m_Size)
			Memory::Move(m_Data + index + count, m_Data + index, (m_Size - index) * sizeof(T));
		if constexpr (Container::UseCopyConstructor<T>)
		{
			for (sizeT i = 0; i < count; ++i)
				new (m_Data + index + i) T(arr[i]);
		}
		else
		{
			for (sizeT i = 0; i < count; ++i)
				m_Data[index + i] = arr[i];
		}
		m_Size += count;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Insert(const T* it, T* arr, sizeT count)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Size, "Iterator out of range!");
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Insert(sizeT(it - m_Data), arr, count);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Insert(sizeT index, const T* itFirst, const T* itLast)
	{
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last iterator can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		Insert(index, itFirst, sizeT(itLast - itFirst));
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Insert(const T* it, const T* itFirst, const T* itLast)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Size, "Iterator out of range!");
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last iterator can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		Insert(sizeT(it - m_Data), itFirst, sizeT(itLast - itFirst));
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename Iterator>
	HV_INL void DynArray<T, GrowthPolicy>::Insert(sizeT index, const Iterator& itFirst, const Iterator& itLast)
	{
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		sizeT count = 0;
		for (Iterator itData = itFirst; itData != itLast; ++itData)
			++count;
		Reserve(m_Size + count);
		if (index != m_Size)
			Memory::Move(m_Data + index + count, m_Data + index, (m_Size - index) * sizeof(T));
		if constexpr (Container::UseCopyConstructor<T>)
		{
			Iterator itData = itFirst;
			for (sizeT i = 0; i < count; ++i, ++itData)
				new (m_Data + index + i) T(*itData);
		}
		else
		{
			Iterator itData = itFirst;
			for (sizeT i = 0; i < count; ++i, ++itData)
				m_Data[index + i] = *itData;
		}
		m_Size += count;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename Iterator>
	HV_INL void DynArray<T, GrowthPolicy>::Insert(const T* it, const Iterator& itFirst, const Iterator& itLast)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Size, "Iterator out of range!");
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		Insert(sizeT(it - m_Data), itFirst, itLast);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Insert(sizeT index, std::initializer_list<T> il)
	{
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Insert(index, il.begin(), il.size());
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Insert(const T* it, std::initializer_list<T> il)
	{
		HV_ASSERT_MSG(it >= m_Data && it <= m_Data + m_Size, "Iterator out of range!");
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		Insert(sizeT(it - m_Data), il.begin(), il.size());
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Erase(sizeT index)
	{
		HV_ASSERT_MSG(index < m_Size, "Index out of range!");
		m_Data[index].~T();
		if (index != m_Size - 1)
			Memory::Move(m_Data + index, m_Data + index + 1, (m_Size - index) * sizeof(T));
		--m_Size;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Erase(const T* it)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Size, "Iterator out of range!");
		Erase(sizeT(it - m_Data));
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Erase(sizeT index, sizeT count)
	{
		HV_ASSERT_MSG(index < m_Size, "Index out of range!");
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		HV_ASSERT_MSG(index + count < m_Size, "Count is too big, reaching past DynArray's size!");
		for (sizeT i = index, end = index + count; i < end; ++i)
			m_Data[i].~T();
		if (index + count < m_Size - 1)
			Memory::Move(m_Data + index, m_Data + index + count, (m_Size - index) * sizeof(T));
		m_Size -= count;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Erase(const T* it, sizeT count)
	{
		HV_ASSERT_MSG(it >= m_Data && it < m_Data + m_Size, "Iterator out of range!");
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Erase(sizeT(it - m_Data), count);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Erase(const T* itFirst, const T* itLast)
	{
		HV_ASSERT_MSG(itFirst >= m_Data && itFirst < m_Data + m_Size, "First iterator out of range!");
		HV_ASSERT_MSG(itLast >= m_Data && itLast <= m_Data + m_Size, "Last iterator out of range!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		Erase(sizeT(itFirst - m_Data), sizeT(itLast - itFirst));
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename ... Args>
	HV_INL void DynArray<T, GrowthPolicy>::Emplace(sizeT index, Args&&... args)
	{
		HV_ASSERT_MSG(index <= m_Size, "Index out of range!");
		Reserve(m_Size + 1);
		if (index != m_Size)
			Memory::Move(m_Data + index + 1, m_Data + index, (m_Size - index) * sizeof(T));
		new (m_Data + index) T(args...);
		++m_Size;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename ... Args>
	HV_INL void DynArray<T, GrowthPolicy>::Emplace(const T* it, Args&&... args)
	{
		Emplace(sizeT(it - m_Data), Forward<Args...>(args...));
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename ... Args>
	HV_INL void DynArray<T, GrowthPolicy>::EmplaceBack(Args&&... args)
	{
		Reserve(m_Size + 1);
		new (m_Data + m_Size) T(args...);
		++m_Size;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	T* DynArray<T, GrowthPolicy>::Find(const T& value)
	{
		T* it = m_Data;
		for (sizeT i = 0; i < m_Size; ++i)
		{
			if (*it == value)
				return it;
			++it;
		}
		return it;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	const T* DynArray<T, GrowthPolicy>::Find(const T& value) const
	{
		const T* it = m_Data;
		for (sizeT i = 0; i < m_Size; ++i)
		{
			if (*it == value)
				return it;
			++it;
		}
		return it;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_INL void DynArray<T, GrowthPolicy>::Swap(DynArray& dynArr)
	{
		if (m_pAlloc == dynArr.m_pAlloc)
		{
			Hv::Swap(m_Data, dynArr.m_Data);
			Hv::Swap(m_Size, dynArr.m_Size);
			Hv::Swap(m_Capacity, dynArr.m_Capacity);
		}
		else
		{
			if (m_Capacity < dynArr.m_Size)
				Reserve(dynArr.m_Size);
			else if (dynArr.m_Capacity < m_Size)
				dynArr.Reserve(m_Size);

			Hv::Swap(m_Size, dynArr.m_Size);
			Hv::Swap(m_Capacity, dynArr.m_Capacity);

			if (m_Size >= dynArr.m_Size)
				Hv::SwapRanges(m_Data, m_Data + m_Size, dynArr.m_Data);
			else
				Hv::SwapRanges(m_Data, m_Data + dynArr.m_Size, dynArr.m_Data);
		}
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL b8 DynArray<T, GrowthPolicy>::IsEmpty() const
	{
		return m_Size == 0;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL sizeT DynArray<T, GrowthPolicy>::Size() const
	{
		return m_Size;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL sizeT DynArray<T, GrowthPolicy>::Capacity() const
	{
		return m_Capacity;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL T* DynArray<T, GrowthPolicy>::Data()
	{
		return m_Data;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	const T* DynArray<T, GrowthPolicy>::Data() const
	{
		return m_Data;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL Memory::IAllocator* DynArray<T, GrowthPolicy>::GetAllocator()
	{
		return m_pAlloc;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL T* DynArray<T, GrowthPolicy>::Front()
	{
		return m_Data;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL const T* DynArray<T, GrowthPolicy>::Front() const
	{
		return m_Data;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL T* DynArray<T, GrowthPolicy>::Last()
	{
		return m_Data + m_Size - 1;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL const T* DynArray<T, GrowthPolicy>::Last() const
	{
		return m_Data + m_Size - 1;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL T* DynArray<T, GrowthPolicy>::Back()
	{
		return m_Data + m_Size;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL const T* DynArray<T, GrowthPolicy>::Back() const
	{
		return m_Data + m_Size;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL T* DynArray<T, GrowthPolicy>::begin()
	{
		return m_Data;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL const T* DynArray<T, GrowthPolicy>::begin() const
	{
		return m_Data;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL T* DynArray<T, GrowthPolicy>::end()
	{
		return m_Data + m_Size;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL const T* DynArray<T, GrowthPolicy>::end() const
	{
		return m_Data + m_Size;
	}

}
