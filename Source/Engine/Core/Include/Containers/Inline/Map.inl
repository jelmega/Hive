// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Map.inl: Ordered map
#pragma once
#include "Containers/Map.h"

namespace Hv {
	
	template <typename Key, typename Value, typename CompLess>
	Map<Key, Value, CompLess>::Map()
		: m_Tree(g_pAllocator)
	{
	}

	template <typename Key, typename Value, typename CompLess>
	Map<Key, Value, CompLess>::Map(Memory::IAllocator* pAlloc)
		: m_Tree(pAlloc)
	{
	}

	template <typename Key, typename Value, typename CompLess>
	Map<Key, Value, CompLess>::Map(const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_Tree(comp, pAlloc)
	{
	}

	template <typename Key, typename Value, typename CompLess>
	template <sizeT N>
	Map<Key, Value, CompLess>::Map(Pair<Key, Value>(&arr)[N], Memory::IAllocator* pAlloc)
		: m_Tree(pAlloc)
	{
		for (sizeT i = 0; i < N; ++i)
			m_Tree.Insert(arr[i]);
	}

	template <typename Key, typename Value, typename CompLess>
	template <sizeT N>
	Map<Key, Value, CompLess>::Map(Pair<Key, Value>(&arr)[N], const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_Tree(comp, pAlloc)
	{
		for (sizeT i = 0; i < N; ++i)
			m_Tree.Insert(arr[i]);
	}

	template <typename Key, typename Value, typename CompLess>
	Map<Key, Value, CompLess>::Map(const Pair<Key, Value>* arr, sizeT count, Memory::IAllocator* pAlloc)
		: m_Tree(pAlloc)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		for (sizeT i = 0; i < count; ++i)
			m_Tree.Insert(arr[i]);
	}

	template <typename Key, typename Value, typename CompLess>
	Map<Key, Value, CompLess>::Map(const Pair<Key, Value>* arr, sizeT count, const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_Tree(comp, pAlloc)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		for (sizeT i = 0; i < count; ++i)
			m_Tree.Insert(arr[i]);
	}

	template <typename Key, typename Value, typename CompLess>
	Map<Key, Value, CompLess>::Map(const Pair<Key, Value>* itFirst, const Pair<Key, Value>* itLast, Memory::IAllocator* pAlloc)
		: m_Tree(pAlloc)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		for (const KeyValuePair* it = itFirst; it != itLast; ++it)
			m_Tree.Insert(*it);
	}

	template <typename Key, typename Value, typename CompLess>
	Map<Key, Value, CompLess>::Map(const Pair<Key, Value>* itFirst, const Pair<Key, Value>* itLast, const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_Tree(comp, pAlloc)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		for (const KeyValuePair* it = itFirst; it != itLast; ++it)
			m_Tree.Insert(*it);
	}

	template <typename Key, typename Value, typename CompLess>
	template <typename InputIterator>
	Map<Key, Value, CompLess>::Map(const InputIterator& itFirst, const InputIterator& itLast, Memory::IAllocator* pAlloc)
		: m_Tree(pAlloc)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		for (Iterator it = itFirst; it != itLast; ++it)
			m_Tree.Insert(*it);
	}

	template <typename Key, typename Value, typename CompLess>
	template <typename InputIterator>
	Map<Key, Value, CompLess>::Map(const InputIterator& itFirst, const InputIterator& itLast, const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_Tree(comp, pAlloc)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		for (Iterator it = itFirst; it != itLast; ++it)
			m_Tree.Insert(*it);
	}

	template <typename Key, typename Value, typename CompLess>
	Map<Key, Value, CompLess>::Map(std::initializer_list<Pair<Key, Value>> il, Memory::IAllocator* pAlloc)
		: m_Tree(pAlloc)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		for (const KeyValuePair* it = il.begin(); it != il.end(); ++it)
			m_Tree.Insert(*it);
	}

	template <typename Key, typename Value, typename CompLess>
	Map<Key, Value, CompLess>::Map(std::initializer_list<Pair<Key, Value>> il, const CompLess& comp, Memory::IAllocator* pAlloc)
		: m_Tree(comp, pAlloc)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		for (const KeyValuePair* it = il.begin(); it != il.end(); ++it)
			m_Tree.Insert(*it);
	}

	template <typename Key, typename Value, typename CompLess>
	Map<Key, Value, CompLess>::Map(const Map& map)
		: m_Tree(map.m_Tree)
	{
	}

	template <typename Key, typename Value, typename CompLess>
	Map<Key, Value, CompLess>::Map(const Map& map, Memory::IAllocator* pAlloc)
		: m_Tree(map.m_Tree, pAlloc)
	{
	}

	template <typename Key, typename Value, typename CompLess>
	Map<Key, Value, CompLess>::Map(Map&& map) noexcept
		: m_Tree(Forwaard<BaseType>(map.m_Tree))
	{
	}

	template <typename Key, typename Value, typename CompLess>
	Map<Key, Value, CompLess>::~Map()
	{
	}

	template <typename Key, typename Value, typename CompLess>
	template <sizeT N>
	Map<Key, Value, CompLess>& Map<Key, Value, CompLess>::operator=(Pair<Key, Value>(&arr)[N])
	{
		m_Tree.Clear();
		for (sizeT i = 0; i < N; ++i)
			m_Tree.Insert(arr[i]);
		return *this;
	}

	template <typename Key, typename Value, typename CompLess>
	Map<Key, Value, CompLess>& Map<Key, Value, CompLess>::operator=(std::initializer_list<Pair<Key, Value>> il)
	{
		m_Tree.Clear();
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		for (const KeyValuePair* it = il.begin(); it != il.end(); ++it)
			m_Tree.Insert(*it);
		return *this;
	}

	template <typename Key, typename Value, typename CompLess>
	Map<Key, Value, CompLess>& Map<Key, Value, CompLess>::operator=(const Map& map)
	{
		m_Tree = map.m_Tree;
		return *this;
	}

	template <typename Key, typename Value, typename CompLess>
	Map<Key, Value, CompLess>& Map<Key, Value, CompLess>::operator=(Map&& map) noexcept
	{
		m_Tree = Forward<>(map.m_Tree);
		return *this;
	}

	template <typename Key, typename Value, typename CompLess>
	Value& Map<Key, Value, CompLess>::operator[](const Key& key)
	{
		Iterator it = m_Tree.FindKey(key);
		HV_ASSERT_MSG(it != m_Tree.Back(), "Can't find key!");
		return (*it).second;
	}

	template <typename Key, typename Value, typename CompLess>
	const Value& Map<Key, Value, CompLess>::operator[](const Key& key) const
	{
		Iterator it = m_Tree.FindKey(key);
		HV_ASSERT_MSG(it != m_Tree.Back(), "Can't find key!");
		return (*it).second;
	}

	template <typename Key, typename Value, typename CompLess>
	Pair<typename Map<Key, Value, CompLess>::Iterator, b8> Map<Key, Value, CompLess>::Insert(const Pair<Key, Value>& pair)
	{
		return m_Tree.Insert(pair);
	}

	template <typename Key, typename Value, typename CompLess>
	Pair<typename Map<Key, Value, CompLess>::Iterator, b8> Map<Key, Value, CompLess>::Insert(const Key& key, const Value& value)
	{
		return m_Tree.Insert(Forward<KeyValuePair>(KeyValuePair(key, value)));
	}

	template <typename Key, typename Value, typename CompLess>
	Pair<typename Map<Key, Value, CompLess>::Iterator, b8> Map<Key, Value, CompLess>::Insert(Pair<Key, Value>&& pair)
	{
		return m_Tree.Insert(Forward<KeyValuePair>(pair));
	}

	template <typename Key, typename Value, typename CompLess>
	Pair<typename Map<Key, Value, CompLess>::Iterator, b8> Map<Key, Value, CompLess>::Insert(Key&& key, Value&& value)
	{
		return m_Tree.Insert(Forward<KeyValuePair>(KeyValuePair(Forward<Key>(key), Forward<Value>(value))));
	}

	template <typename Key, typename Value, typename CompLess>
	Pair<typename Map<Key, Value, CompLess>::Iterator, b8> Map<Key, Value, CompLess>::Insert(const Iterator& hint, const Pair<Key, Value>& pair)
	{
		return m_Tree.Insert(hint, pair);
	}

	template <typename Key, typename Value, typename CompLess>
	Pair<typename Map<Key, Value, CompLess>::Iterator, b8> Map<Key, Value, CompLess>::Insert(const Iterator& hint, const Key& key, const Value& value)
	{
		return m_Tree.Insert(hint, Forward<KeyValuePair>(KeyValuePair(key, value)));
	}

	template <typename Key, typename Value, typename CompLess>
	Pair<typename Map<Key, Value, CompLess>::Iterator, b8> Map<Key, Value, CompLess>::Insert(const Iterator& hint, Pair<Key, Value>&& pair)
	{
		return m_Tree.Insert(hint, Forward<KeyValuePair>(pair));
	}

	template <typename Key, typename Value, typename CompLess>
	Pair<typename Map<Key, Value, CompLess>::Iterator, b8> Map<Key, Value, CompLess>::Insert(const Iterator& hint, Key&& key, Value&& value)
	{
		return m_Tree.Insert(hint, Forward<KeyValuePair>(KeyValuePair(Forward<Key>(key), Forward<Value>(value))));
	}

	template <typename Key, typename Value, typename CompLess>
	template <sizeT N>
	void Map<Key, Value, CompLess>::Insert(KeyValuePair(&arr)[N])
	{
		for (sizeT i = 0; i < N; ++i)
			m_Tree.Insert(arr[i]);
	}

	template <typename Key, typename Value, typename CompLess>
	void Map<Key, Value, CompLess>::Insert(const Pair<Key, Value>* arr, sizeT count)
	{
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		for (sizeT i = 0; i < count; ++i)
			m_Tree.Insert(arr[i]);
	}

	template <typename Key, typename Value, typename CompLess>
	void Map<Key, Value, CompLess>::Insert(const Pair<Key, Value>* itFirst, const Pair<Key, Value>* itLast)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last oteratpr can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		for (const KeyValuePair* it = itFirst; it != itLast; ++it)
			m_Tree.Insert(*it);
	}

	template <typename Key, typename Value, typename CompLess>
	template <typename InputIterator>
	void Map<Key, Value, CompLess>::Insert(const InputIterator& itFirst, const InputIterator& itLast)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		for (Iterator it = itFirst; it != itLast; ++it)
			m_Tree.Insert(*it);
	}

	template <typename Key, typename Value, typename CompLess>
	template <typename InputIterator>
	void Map<Key, Value, CompLess>::Insert(std::initializer_list<Pair<Key, Value>> il)
	{
		HV_ASSERT_MSG(il.size() > 0, "Initializer list needs to contain at least 1 element!");
		for (const KeyValuePair* it = il.begin(); it != il.end(); ++it)
			m_Tree.Insert(*it);
	}

	template <typename Key, typename Value, typename CompLess>
	template <typename ... Args>
	Pair<typename Map<Key, Value, CompLess>::Iterator, b8> Map<Key, Value, CompLess>::Emplace(const Key& key, Args&&... args)
	{
		return m_Tree.Insert(Forward<KeyValuePair>(KeyValuePair(key, Forward<Value>(Value(args...)))));

	}

	template <typename Key, typename Value, typename CompLess>
	template <typename ... Args>
	Pair<typename Map<Key, Value, CompLess>::Iterator, b8> Map<Key, Value, CompLess>::EmplaceHint(const Iterator& hint, const Key& key, Args&&... args)
	{
		return m_Tree.Insert(Forward<KeyValuePair>(KeyValuePair(hint, key, Forward<Value>(Value(args...)))));
	}

	template <typename Key, typename Value, typename CompLess>
	sizeT Map<Key, Value, CompLess>::Erase(const Key& key)
	{
		Iterator it = m_Tree.EraseKey(key);
		if (it == m_Tree.Back())
			return 0;
		return 1;
	}

	template <typename Key, typename Value, typename CompLess>
	typename Map<Key, Value, CompLess>::Iterator Map<Key, Value, CompLess>::Erase(const Iterator& it)
	{
		return m_Tree.Erase(it);
	}

	template <typename Key, typename Value, typename CompLess>
	typename Map<Key, Value, CompLess>::Iterator Map<Key, Value, CompLess>::Erase(const Iterator& itFirst, const Iterator& itLast)
	{
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		for (Iterator it = itFirst; it != itLast; ++it)
			m_Tree.Erase(it);
		return itLast;
	}

	template <typename Key, typename Value, typename CompLess>
	void Map<Key, Value, CompLess>::Clear()
	{
		m_Tree.Clear();
	}

	template <typename Key, typename Value, typename CompLess>
	typename Map<Key, Value, CompLess>::Iterator Map<Key, Value, CompLess>::Find(const Key& key) const
	{
		return m_Tree.FindKey(key);
	}

	template <typename Key, typename Value, typename CompLess>
	template <typename U, typename Pred>
	typename Map<Key, Value, CompLess>::Iterator Map<Key, Value, CompLess>::FindAs(const U& val, const Pred& pred) const
	{
		return m_Tree.FindAs(val, pred);
	}

	template <typename Key, typename Value, typename CompLess>
	sizeT Map<Key, Value, CompLess>::Count(const Key& key) const
	{
		return m_Tree.CountKey(key);
	}

	template <typename Key, typename Value, typename CompLess>
	typename Map<Key, Value, CompLess>::Iterator Map<Key, Value, CompLess>::LowerBound(const Key& key) const
	{
		return m_Tree.LowerBoundKey(key);
	}

	template <typename Key, typename Value, typename CompLess>
	typename Map<Key, Value, CompLess>::Iterator Map<Key, Value, CompLess>::UpperBound(const Key& key) const
	{
		return m_Tree.UpperBoundKey(key);
	}

	template <typename Key, typename Value, typename CompLess>
	Pair<typename Map<Key, Value, CompLess>::Iterator, typename Map<Key, Value, CompLess>::Iterator> Map<Key, Value, CompLess>::EqualRange(const Key& key) const
	{
		return Pair<Iterator, Iterator>(m_Tree.LowerBoundKey(key), m_Tree.UpperBoundKey(key));
	}

	template <typename Key, typename Value, typename CompLess>
	void Map<Key, Value, CompLess>::Swap(Map& map)
	{
		m_Tree.Swap(map.m_Tree);
	}

	template <typename Key, typename Value, typename CompLess>
	b8 Map<Key, Value, CompLess>::IsEmpty() const
	{
		return m_Tree.IsEmpty();
	}

	template <typename Key, typename Value, typename CompLess>
	sizeT Map<Key, Value, CompLess>::Size() const
	{
		return m_Tree.Size();
	}

	template <typename Key, typename Value, typename CompLess>
	Memory::IAllocator* Map<Key, Value, CompLess>::GetAllocator() const
	{
		return m_Tree.GetAllocator();
	}

	template <typename Key, typename Value, typename CompLess>
	typename Map<Key, Value, CompLess>::Iterator Map<Key, Value, CompLess>::Front() const
	{
		return m_Tree.Front();
	}

	template <typename Key, typename Value, typename CompLess>
	typename Map<Key, Value, CompLess>::Iterator Map<Key, Value, CompLess>::Last() const
	{
		return m_Tree.Last();
	}

	template <typename Key, typename Value, typename CompLess>
	typename Map<Key, Value, CompLess>::Iterator Map<Key, Value, CompLess>::Back() const
	{
		return m_Tree.Back();
	}

	template <typename Key, typename Value, typename CompLess>
	typename Map<Key, Value, CompLess>::Iterator Map<Key, Value, CompLess>::begin() const
	{
		return m_Tree.begin();
	}

	template <typename Key, typename Value, typename CompLess>
	typename Map<Key, Value, CompLess>::Iterator Map<Key, Value, CompLess>::end() const
	{
		return m_Tree.end();
	}

}