// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// IntrusiveList.inl: Intrusive list
#pragma once
#include "Containers/IntrusiveList.h"

namespace Hv {

	HV_FORCE_INL IntrusiveListNode::IntrusiveListNode()
		: m_pPrev(nullptr)
		, m_pNext(nullptr)
	{
	}

	HV_FORCE_INL IntrusiveListNode::~IntrusiveListNode()
	{
	}

	////////////////////////////////////////////////////////////////////////////////

	template <typename Node>
	HV_FORCE_INL IntrusiveList<Node>::Iterator::Iterator()
		: m_pNode(nullptr)
	{
	}

	template <typename Node>
	HV_FORCE_INL IntrusiveList<Node>::Iterator::Iterator(IntrusiveListNode* pNode)
		: m_pNode(pNode)
	{
	}

	template <typename Node>
	HV_FORCE_INL IntrusiveList<Node>::Iterator::Iterator(const Iterator& it)
		: m_pNode(it.m_pNode)
	{
	}

	template <typename Node>
	HV_FORCE_INL IntrusiveList<Node>::Iterator::Iterator(Iterator&& it) noexcept
		: m_pNode(it.m_pNode)
	{
	}

	template <typename Node>
	HV_FORCE_INL typename IntrusiveList<Node>::Iterator& IntrusiveList<Node>::Iterator::operator++()
	{
		HV_ASSERT_MSG(m_pNode, "Can't increment iterator!");
		m_pNode = m_pNode->m_pNext;
		return *this;
	}

	template <typename Node>
	HV_FORCE_INL typename IntrusiveList<Node>::Iterator IntrusiveList<Node>::Iterator::operator++(int)
	{
		Iterator(it);
		++*this;
		return it;
	}

	template <typename Node>
	HV_FORCE_INL typename IntrusiveList<Node>::Iterator& IntrusiveList<Node>::Iterator::operator--()
	{
		HV_ASSERT_MSG(m_pNode, "Can't increment iterator!");
		m_pNode = m_pNode->m_pPrev;
		return *this;
	}

	template <typename Node>
	HV_FORCE_INL typename IntrusiveList<Node>::Iterator IntrusiveList<Node>::Iterator::operator--(int)
	{
		Iterator(it);
		--*this;
		return it;
	}

	template <typename Node>
	HV_FORCE_INL typename IntrusiveList<Node>::Iterator IntrusiveList<Node>::Iterator::operator+(sizeT count) const
	{
		Iterator it(*this);
		for (sizeT i = 0; i < count; ++i)
			++it;
		return it;
	}

	template <typename Node>
	HV_FORCE_INL typename IntrusiveList<Node>::Iterator IntrusiveList<Node>::Iterator::operator-(sizeT count) const
	{
		Iterator it(*this);
		for (sizeT i = 0; i < count; ++i)
			--it;
		return it;
	}

	template <typename Node>
	HV_FORCE_INL typename IntrusiveList<Node>::Iterator& IntrusiveList<Node>::Iterator::operator=(const Iterator& it)
	{
		m_pNode = it.m_pNode;
		return *this;
	}

	template <typename Node>
	HV_FORCE_INL typename IntrusiveList<Node>::Iterator& IntrusiveList<Node>::Iterator::operator=(Iterator&& it) noexcept
	{
		m_pNode = it.m_pNode;
		return *this;
	}

	template <typename Node>
	HV_FORCE_INL typename IntrusiveList<Node>::Iterator& IntrusiveList<Node>::Iterator::operator+=(sizeT count)
	{
		for (sizeT i = 0; i < count; ++i)
			++*this;
		return *this;
	}

	template <typename Node>
	HV_FORCE_INL typename IntrusiveList<Node>::Iterator& IntrusiveList<Node>::Iterator::operator-=(sizeT count)
	{
		for (sizeT i = 0; i < count; ++i)
			--*this;
		return *this;
	}

	template <typename Node>
	HV_FORCE_INL Node& IntrusiveList<Node>::Iterator::operator*()
	{
		return *m_pNode;
	}

	template <typename Node>
	HV_FORCE_INL const Node& IntrusiveList<Node>::Iterator::operator*() const
	{
		return *m_pNode;
	}

	template <typename Node>
	HV_FORCE_INL Node* IntrusiveList<Node>::Iterator::operator->()
	{
		return m_pNode;
	}

	template <typename Node>
	HV_FORCE_INL const Node* IntrusiveList<Node>::Iterator::operator->() const
	{
		return m_pNode;
	}

	template <typename Node>
	HV_FORCE_INL b8 IntrusiveList<Node>::Iterator::operator==(const Iterator& it) const
	{
		return m_pNode == it.m_pNode;
	}

	template <typename Node>
	HV_FORCE_INL b8 IntrusiveList<Node>::Iterator::operator!=(const Iterator& it) const
	{
		return m_pNode != it.m_pNode;
	}

	template <typename Node>
	HV_FORCE_INL IntrusiveList<Node>::IntrusiveList()
		: m_pHead(nullptr)
		, m_pTail(nullptr)
	{
	}

	template <typename Node>
	HV_FORCE_INL IntrusiveList<Node>::IntrusiveList(IntrusiveList&& list) noexcept
		: m_pHead(list.m_pHead)
		, m_pTail(list.m_pTail)
	{
		list.m_pHead = nullptr;
		list.m_pTail = nullptr;
	}

	template <typename Node>
	HV_FORCE_INL IntrusiveList<Node>::~IntrusiveList()
	{
		Clear();
	}

	template <typename Node>
	HV_FORCE_INL IntrusiveList<Node>& IntrusiveList<Node>::operator=(IntrusiveList&& list) noexcept
	{
		m_pHead = list.m_pHead;
		list.m_pHead = nullptr;
		m_pTail = list.m_pTail;
		list.m_pTail = nullptr;
		return *this;
	}

	template <typename Node>
	HV_FORCE_INL void IntrusiveList<Node>::PushBack(Node& node)
	{
		node.m_pPrev = m_pTail;
		m_pTail->m_pNext = &node;
		m_pTail = &node;
	}

	template <typename Node>
	HV_FORCE_INL void IntrusiveList<Node>::PopBack()
	{
		if (m_pTail)
		{
			Node* pPrev = m_pTail->m_pPrev;
			m_pTail->m_pNext = nullptr;
			pPrev->m_pNext = nullptr;
			m_pTail = pPrev;
		}
	}

	template <typename Node>
	HV_FORCE_INL void IntrusiveList<Node>::PushFront(Node& node)
	{
		node.m_pNext = m_pHead;
		m_pHead->m_pPrev = &node;
		m_pHead = &node;
	}

	template <typename Node>
	HV_FORCE_INL void IntrusiveList<Node>::PopFront()
	{
		if (m_pHead)
		{
			Node* pNext = m_pHead->m_pNext;
			m_pHead->m_pPrev = nullptr;
			pNext->m_pPrev = nullptr;
			m_pHead = pNext;
		}
	}

	template <typename Node>
	HV_INL void IntrusiveList<Node>::Insert(Iterator& it, Node& node)
	{
		Node* pPrev;
		if (it.m_pNode)
			pPrev = it.m_pNode->m_pPrev;
		else
			pPrev = m_pTail;

		node.m_pPrev = pPrev;
		node.m_pNext = it.m_pNode;

		if (pPrev)
			pPrev->m_pNext = &node;
		else
			m_pHead = &node;

		if (it.m_pNode)
			it.m_pNode->m_pPrev = &node;
		else
			m_pTail = &node;
	}

	template <typename Node>
	HV_INL void IntrusiveList<Node>::Remove(Iterator& it)
	{
		HV_ASSERT_MSG(it.m_pNode, "Iterator out of range!");

		Node* pPrev = it.m_pNode->m_pPrev;
		Node* pNext = it.m_pNode->m_pNext;

		if (pPrev)
			pPrev->m_pNext = pNext;
		else
			m_pHead = pNext;
		if (pNext)
			pNext->m_pPrev = pPrev;
		else
			m_pTail = pPrev;

		it.m_pNode->m_pPrev = it.m_pNode->m_pNext = nullptr;
	}

	template <typename Node>
	HV_INL void IntrusiveList<Node>::Remove(Iterator& it, sizeT count)
	{
		HV_ASSERT_MSG(it.m_pNode, "Iterator out of range!");
		HV_ASSERT_MSG(count > 0, "Count needs to be bigger than 0!");
		Node* pPrev = it.m_pNode.m_pPrev;
		Node* pNode = it.m_pNode;
		for (sizeT i = 0; i < count; ++i)
		{
			HV_ASSERT(pNode);
			Node* pNext = pNode->m_pNext;
			pNode->m_pPrev = pNode->m_pNext = nullptr;
			pNode = pNext;
		}
		if (pPrev)
			pPrev->pNext = pNode;
		else
			m_pHead = pNode;
		if (pNode)
			pNode->m_pPrev = pPrev;
		else
			m_pTail = pPrev;
	}

	template <typename Node>
	HV_INL void IntrusiveList<Node>::Remove(Iterator& itFirst, Iterator& itLast)
	{
		HV_ASSERT_MSG(itFirst.m_pNode, "Iterator out of range!");
		HV_ASSERT_MSG(itFirst != itLast, "Iterators can't match!");
		Node* pPrev = itFirst.m_pNode.m_pPrev;
		Node* pNode = itFirst.m_pNode;
		while (pNode != itLast.m_pNode)
		{
			HV_ASSERT(pNode);
			Node* pNext = pNode->m_pNext;
			pNode->m_pPrev = pNode->m_pNext = nullptr;
			pNode = pNext;
		}
		if (pPrev)
			pPrev->pNext = pNode;
		else
			m_pHead = pNode;
		if (pNode)
			pNode->m_pPrev = pPrev;
		else
			m_pTail = pPrev;
	}

	template <typename Node>
	HV_INL void IntrusiveList<Node>::Clear()
	{
		Node* pNode = m_pHead;
		while (pNode)
		{
			Node* pNext = pNode->m_pNext;
			pNode->m_pNext = pNode->m_pPrev = nullptr;
			pNode = pNext;
		}
		m_pHead = m_pTail = nullptr;
	}

	template <typename Node>
	HV_FORCE_INL void IntrusiveList<Node>::Swap(IntrusiveList& list)
	{
		Hv::Swap(m_pHead, list.m_pHead);
		Hv::Swap(m_pTail, list.m_pTail);
	}

	template <typename Node>
	HV_FORCE_INL b8 IntrusiveList<Node>::IsEmpty() const
	{
		return !m_pHead;
	}

	template <typename Node>
	HV_INL sizeT IntrusiveList<Node>::Size() const
	{
		sizeT count = 0;
		Node* pNode = m_pHead;
		while (pNode)
		{
			++count;
			pNode = pNode->m_pNext;
		}
		return count;
	}

	template <typename Node>
	HV_FORCE_INL typename IntrusiveList<Node>::Iterator IntrusiveList<Node>::Front() const
	{
		return Iterator(m_pHead);
	}

	template <typename Node>
	HV_FORCE_INL typename IntrusiveList<Node>::Iterator IntrusiveList<Node>::Last() const
	{
		return Iterator(m_pTail);
	}

	template <typename Node>
	HV_FORCE_INL typename IntrusiveList<Node>::Iterator IntrusiveList<Node>::Back() const
	{
		return Iterator();
	}

	template <typename Node>
	HV_FORCE_INL typename IntrusiveList<Node>::Iterator IntrusiveList<Node>::begin() const
	{
		return Iterator(m_pHead);
	}

	template <typename Node>
	HV_FORCE_INL typename IntrusiveList<Node>::Iterator IntrusiveList<Node>::end() const
	{
		return Iterator();
	}
	
}
