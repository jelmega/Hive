// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Bitset.inl: Bit set
#pragma once
#include "Containers/Bitset.h"

namespace Hv {

	HV_INL Bitset::Bitset()
		: m_Size(0)
	{
	}

	HV_INL Bitset::~Bitset()
	{
	}

	HV_INL b8 Bitset::operator[](sizeT idx)
	{
		if (idx >= m_Size)
			return false;
		sizeT arrPos = idx >> 3;
		sizeT upos = idx & 7;

		u8 val = m_Container[arrPos];
		return val & (sizeT(1) << (7 - upos));
	}

	HV_INL void Bitset::Resize(sizeT size)
	{
		m_Container.Resize(size / 8);
		m_Size = size;
	}

	HV_INL void Bitset::Set(sizeT idx)
	{
		if (idx >= m_Size)
		{
			m_Container.Resize((idx / 8) + 1);
			m_Size = idx + 1;
		}
		sizeT arrPos = idx >> 3;
		sizeT upos = idx & 7;

		u8& val = m_Container[arrPos];
		val |= 1 << (7 - upos);
	}

	HV_INL void Bitset::Set(sizeT idx, b8 val)
	{
		if (idx >= m_Size)
		{
			m_Container.Resize((idx / 8) + 1);
			m_Size = idx + 1;
		}
		sizeT arrPos = idx >> 3;
		sizeT upos = idx & 7;

		u8 bit = 1 << (7 - upos);
		u8& tmp = m_Container[arrPos];
		if (val)
			tmp |= bit;
		else
			tmp &= ~bit;
	}

	HV_INL void Bitset::Unset(sizeT idx)
	{
		if (idx >= m_Size)
			return;
		sizeT arrPos = idx >> 3;
		sizeT upos = idx & 7;

		u8 bit = 1 << (7 - upos);
		u8& val = m_Container[arrPos];
		val &= ~bit;
	}

	HV_INL b8 Bitset::Get(sizeT idx)
	{
		if (idx >= m_Size)
			return false;

		sizeT arrPos = idx >> 3;
		sizeT upos = idx & 7;

		u8 val = m_Container[arrPos];
		return val & (sizeT(1) << (7 - upos));
	}

	HV_INL b8 Bitset::None() const
	{
		for (sizeT i = 0; i < m_Container.Size(); ++i)
		{
			if (m_Container[i] != 0)
				return false;
		}
		return true;
	}

	HV_INL b8 Bitset::Any() const
	{
		for (sizeT i = 0; i < m_Container.Size(); ++i)
		{
			if (m_Container[i] != 0)
				return true;
		}
		return false;
	}

	HV_INL b8 Bitset::All() const
	{
		sizeT end = m_Container.Size() - 1;
		for (sizeT i = 0; i <= end; ++i)
		{
			if (m_Container[i] != 0xFF)
				return false;
		}
		sizeT numBits = m_Size - (end << 3);
		if (numBits)
		{
			u8 check = 0xFF << (7 - numBits);
			if (m_Container[end] != check)
				return false;
		}
		return true;
	}

	HV_INL void Bitset::Clear()
	{
		m_Container.Clear();
	}

	HV_INL b8 Bitset::Fits(const Bitset& bitset)
	{
		if (m_Size >= bitset.m_Size)
		{
			for (sizeT i = 0; i < bitset.m_Container.Size(); ++i)
			{
				u8 comp = bitset.m_Container[i];
				if ((m_Container[i] & comp) != comp)
					return false;
			}
		}
		else
		{
			sizeT i = 0;
			for (; i < m_Container.Size(); ++i)
			{
				u8 comp = bitset.m_Container[i];
				if ((m_Container[i] & comp) != comp)
					return false;
			}
			for (; i < bitset.m_Container.Size(); ++i)
			{
				if (bitset.m_Container[i] != 0)
					return false;
			}
		}
		return true;
	}
}
