// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Stack.inl: Stack
#pragma once
#include "Containers/Stack.h"

namespace Hv {

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL Stack<T,  GrowthPolicy>::Stack()
		: m_Container()
	{
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL Stack<T,  GrowthPolicy>::Stack(Memory::IAllocator* pAlloc)
		: m_Container(pAlloc)
	{
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL Stack<T,  GrowthPolicy>::Stack(sizeT capacity, Memory::IAllocator* pAlloc)
		: m_Container(capacity, pAlloc)
	{
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL Stack<T,  GrowthPolicy>::Stack(const Stack& stack)
		: m_Container(stack.m_Container)
	{
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL Stack<T,  GrowthPolicy>::Stack(const Stack& stack, Memory::IAllocator* pAlloc)
		: m_Container(stack.m_Container, pAlloc)
	{
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL Stack<T,  GrowthPolicy>::Stack(Stack&& stack) noexcept
		: m_Container(Forward<BaseType>(stack.m_Container))
	{
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL Stack<T,  GrowthPolicy>::~Stack()
	{
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL Stack<T,  GrowthPolicy>& Stack<T,  GrowthPolicy>::operator=(const Stack& stack)
	{
		m_Container = stack.m_Container;
		return *this;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL Stack<T,  GrowthPolicy>& Stack<T,  GrowthPolicy>::operator=(Stack&& stack) noexcept
	{
		m_Container = Forward<BaseType>(stack.m_Container);
		return *this;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL void Stack<T,  GrowthPolicy>::Reserve(sizeT size)
	{
		m_Container.Reserve(size);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL void Stack<T,  GrowthPolicy>::ShrinkToFit()
	{
		m_Container.ShrinkToFit();
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL void Stack<T,  GrowthPolicy>::Clear(bool resize)
	{
		m_Container.Clear(resize);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL void Stack<T,  GrowthPolicy>::Push(const T& val)
	{
		m_Container.PushBack(val);
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL void Stack<T,  GrowthPolicy>::Push(T&& val)
	{
		m_Container.PushBack(Forward<T>(val));
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	template <typename ... Args>
	void Stack<T,  GrowthPolicy>::Emplace(Args&&... args)
	{
		m_Container.EmplaceBack(Forward<Args...>(args...));
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL void Stack<T,  GrowthPolicy>::Pop()
	{
		m_Container.Pop();
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	T& Stack<T,  GrowthPolicy>::Top()
	{
		HV_ASSERT_MSG(m_Container.Size() > 0, "Stack is empty!");
		return m_Container[m_Container.Size() - 1];
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	const T& Stack<T,  GrowthPolicy>::Top() const
	{
		HV_ASSERT_MSG(m_Container.Size() > 0, "Stack is empty!");
		return m_Container[m_Container.Size() - 1];
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL b8 Stack<T,  GrowthPolicy>::IsEmpty() const
	{
		return m_Container.IsEmpty();
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL sizeT Stack<T,  GrowthPolicy>::Size() const
	{
		return m_Container.Size();
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL sizeT Stack<T,  GrowthPolicy>::Capacity() const
	{
		return m_Container.Capacity();
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL Memory::IAllocator* Stack<T,  GrowthPolicy>::GetAllocator()
	{
		return m_Container.GetAllocator();
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL DynArray<T,  GrowthPolicy>& Stack<T,  GrowthPolicy>::GetInternalContainer()
	{
		return m_Container;
	}

	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	HV_FORCE_INL const DynArray<T,  GrowthPolicy>& Stack<T,  GrowthPolicy>::GetInternalContainer() const
	{
		return m_Container;
	}


	template <typename T, Container::GrowthPolicyFunc GrowthPolicy>
	void Stack<T,  GrowthPolicy>::Swap(Stack& stack)
	{
		m_Container.Swap(stack.m_Container);
	}
	
}
