// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Array.h: Fixed size array
#pragma once
#include "Containers/Array.h"
#include "Memory/Memory.h"

namespace Hv {

	template <typename T, sizeT ArrSize>
	Array<T, ArrSize>::Array()
		: m_Data{}
	{
	}

	template <typename T, sizeT ArrSize>
	HV_INL Array<T, ArrSize>::Array(const T& value)
		: m_Data{ value }
	{
		Fill(value);
	}

	template <typename T, sizeT ArrSize>
	template <sizeT S>
	HV_INL Array<T, ArrSize>::Array(T(&arr)[S])
		: m_Data{}
	{
		Assign(arr, S);
	}

	template <typename T, sizeT ArrSize>
	HV_INL Array<T, ArrSize>::Array(const T* arr, sizeT size)
		: m_Data{}
	{
		HV_ASSERT_MSG(arr, "arr can't be a nullptr!");
		Assign(arr, size);
	}

	template <typename T, sizeT ArrSize>
	HV_INL Array<T, ArrSize>::Array(const T* itFirst, const T* itLast)
		: m_Data{}
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last iterator can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		Assign(sizeT(itLast - itFirst));
	}

	template <typename T, sizeT ArrSize>
	template <typename Iterator>
	HV_INL Array<T, ArrSize>::Array(const Iterator& itFirst, const Iterator& itLast)
		: m_Data{}
	{
		Assign(itFirst, itLast);
	}

	template <typename T, sizeT ArrSize>
	HV_INL Array<T, ArrSize>::Array(std::initializer_list<T> il)
		: m_Data{}
	{
		Assign((const T*)il.begin(), (sizeT)il.size());
	}

	template <typename T, sizeT ArrSize>
	template <sizeT S>
	HV_INL Array<T, ArrSize>::Array(const Array<T, S>& arr)
		: m_Data{}
	{
		Assign(arr.m_Data, S);
	}

	template <typename T, sizeT ArrSize>
	HV_INL Array<T, ArrSize>::Array(const Array& arr)
		: m_Data{}
	{
		Assign(arr.m_Data, ArrSize);
	}

	template <typename T, sizeT ArrSize>
	HV_INL Array<T, ArrSize>::Array(Array&& arr) noexcept
	{
		Assign(arr.m_Data, ArrSize);
	}

	template <typename T, sizeT ArrSize>
	template <sizeT N>
	HV_INL Array<T, ArrSize>& Array<T, ArrSize>::operator=(T(&arr)[N])
	{
		Assign(arr, N);
		return *this;
	}

	template <typename T, sizeT ArrSize>
	HV_INL Array<T, ArrSize>& Array<T, ArrSize>::operator=(std::initializer_list<T> il)
	{
		Assign(il.begin(), il.size());
		return *this;
	}

	template <typename T, sizeT ArrSize>
	template <sizeT S>
	HV_INL Array<T, ArrSize>& Array<T, ArrSize>::operator=(const Array<T, S>& arr)
	{
		Assign(arr.m_Data, S);
		return *this;
	}

	template <typename T, sizeT ArrSize>
	HV_INL Array<T, ArrSize>& Array<T, ArrSize>::operator=(const Array& arr)
	{
		HV_ASSERT(this != &arr);
		Assign(arr.m_Data, ArrSize);
		return *this;
	}

	template <typename T, sizeT ArrSize>
	HV_INL Array<T, ArrSize>& Array<T, ArrSize>::operator=(Array&& arr) noexcept
	{
		HV_ASSERT(this != &arr);
		Assign(m_Data, ArrSize);
		return *this;
	}

	template <typename T, sizeT ArrSize>
	HV_FORCE_INL T& Array<T, ArrSize>::operator[](sizeT index)
	{
		HV_ASSERT_MSG(index < ArrSize, "Index out of range!");
		return m_Data[index];
	}

	template <typename T, sizeT ArrSize>
	HV_FORCE_INL const T& Array<T, ArrSize>::operator[](sizeT index) const
	{
		HV_ASSERT_MSG(index < ArrSize, "Index out of range!");
		return m_Data[index];
	}

	template <typename T, sizeT ArrSize>
	HV_INL void Array<T, ArrSize>::Fill(const T& value)
	{
		if (Container::UseCopyConstructor<T>)
		{
			for (sizeT i = 0; i < ArrSize; ++i)
				new (m_Data + i) T(value);
		}
		else
		{
			for (sizeT i = 0; i < ArrSize; ++i)
				m_Data[i] = value;
		}
	}

	template <typename T, sizeT ArrSize>
	HV_FORCE_INL void Array<T, ArrSize>::Assign(const T* arr, sizeT size)
	{
		HV_ASSERT_MSG(arr, "arr can't be a nullptr!");
		size = size < ArrSize ? size : ArrSize;
		if (Container::UseMemCpy<T>)
		{
			Memory::Copy(m_Data, arr, size * sizeof(T));
		}
		else if (Container::UseCopyConstructor<T>)
		{
			const T* it = arr;
			for (sizeT i = 0; i < size; ++i, ++it)
				new (m_Data + i) T(*it);
		}
		else
		{
			const T* it = arr;
			for (sizeT i = 0; i < size; ++i, ++it)
				m_Data[i] = *it;
		}
	}

	template <typename T, sizeT ArrSize>
	HV_INL void Array<T, ArrSize>::Assign(const T* itFirst, const T* itLast)
	{
		HV_ASSERT_MSG(itFirst, "First iterator can't be a nullptr!");
		HV_ASSERT_MSG(itLast, "Last iterator can't be a nullptr!");
		HV_ASSERT_MSG(itFirst < itLast, "First iterator needs to come before last iterator!");
		sizeT size = sizeT(itLast - itFirst);
		Assign(itFirst, size);
	}

	template <typename T, sizeT ArrSize>
	template <typename InputIterator>
	void Array<T, ArrSize>::Assign(const InputIterator& itFirst, const InputIterator& itLast)
	{
		const T* it = itFirst;
		sizeT size = 0;
		if (Container::UseCopyConstructor<T>)
		{
			for (sizeT i = 0; i < ArrSize && itFirst + i < itLast; ++i, ++size, ++it)
				new (m_Data + i) T(*it);
		}
		else
		{
			for (sizeT i = 0; i < ArrSize && itFirst + i < itLast; ++i, ++size, ++it)
				m_Data[i] = *it;
		}
	}

	template <typename T, sizeT ArrSize>
	HV_FORCE_INL void Array<T, ArrSize>::Swap(Array& arr)
	{
		Hv::Swap(m_Data, arr.m_Data);
	}

	template <typename T, sizeT ArrSize>
	constexpr sizeT Array<T, ArrSize>::Size()
	{
		return ArrSize;
	}

	template <typename T, sizeT ArrSize>
	HV_FORCE_INL T* Array<T, ArrSize>::Data()
	{
		return m_Data;
	}

	template <typename T, sizeT ArrSize>
	const T* Array<T, ArrSize>::Data() const
	{
		return m_Data;
	}

	template <typename T, sizeT ArrSize>
	HV_FORCE_INL T* Array<T, ArrSize>::Front()
	{
		return m_Data;
	}

	template <typename T, sizeT ArrSize>
	HV_FORCE_INL const T* Array<T, ArrSize>::Front() const
	{
		return m_Data;
	}

	template <typename T, sizeT ArrSize>
	HV_FORCE_INL T* Array<T, ArrSize>::Last()
	{
		return m_Data + ArrSize - 1;
	}

	template <typename T, sizeT ArrSize>
	HV_FORCE_INL const T* Array<T, ArrSize>::Last() const
	{
		return m_Data + ArrSize - 1;
	}

	template <typename T, sizeT ArrSize>
	HV_FORCE_INL T* Array<T, ArrSize>::Back()
	{
		return m_Data + ArrSize;
	}

	template <typename T, sizeT ArrSize>
	HV_FORCE_INL const T* Array<T, ArrSize>::Back() const
	{
		return m_Data + ArrSize;
	}

	template <typename T, sizeT ArrSize>
	HV_FORCE_INL T* Array<T, ArrSize>::begin()
	{
		return m_Data;
	}

	template <typename T, sizeT ArrSize>
	HV_FORCE_INL const T* Array<T, ArrSize>::begin() const
	{
		return m_Data;
	}

	template <typename T, sizeT ArrSize>
	HV_FORCE_INL T* Array<T, ArrSize>::end()
	{
		return m_Data + ArrSize;
	}

	template <typename T, sizeT ArrSize>
	HV_FORCE_INL const T* Array<T, ArrSize>::end() const
	{
		return m_Data + ArrSize;
	}

}
