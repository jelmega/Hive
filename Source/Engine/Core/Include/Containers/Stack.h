// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Stack.h: Stack
#pragma once
#include "DynArray.h"

namespace Hv {
	
	/**
	 * Stack
	 * @note	The containers uses multiple ways to copy data internally, using the copy-assignment operator as default,
	 *			but MemCpy and the copy-constructor can also be used. These values can be set using their relative declares:
	 *			HV_DECLARE_CONTAINER_MEMCPY and HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR and their respective template variants.
	 * @note A stack is essentially a wrapper around a dynamic array, designed to fit a LIFO style workflow
	 * @tparam T			Element type
	 * @tparam GrowthPolicy	Growth policy
	 */
	template<typename T, Container::GrowthPolicyFunc GrowthPolicy = Container::GrowthDouble>
	class Stack
	{
	private:
		using BaseType = DynArray<T, GrowthPolicy>;

	public:
		/**
		 * Create an empty Stack
		 */
		Stack();
		/**
		 * Create an empty Stack
		 * @param[in] pAlloc	Allocator
		 */
		Stack(Memory::IAllocator* pAlloc);
		/**
		 * Create an Stack with a capacity
		 * @param[in] capacity	Capacity
		 * @param[in] pAlloc	Allocator
		 */
		Stack(sizeT capacity, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a Stack from another Stack
		 * @param[in] stack	Other DynArr
		 */
		Stack(const Stack& stack);
		/**
		 * Create a Stack from another Stack
		 * @param[in] stack	Other DynArr
		 * @param[in] pAlloc	Allocator
		 */
		Stack(const Stack& stack, Memory::IAllocator* pAlloc);
		/**
		 * Move a Stack into this Stack
		 * @param[in] stack	Other DynArr
		 */
		Stack(Stack&& stack) noexcept;
		~Stack();

		Stack& operator=(const Stack& stack);
		Stack& operator=(Stack&& stack) noexcept;

		/**
		 * Reserve space in the Stack
		 * @param[in] size	Min size to reserve
		 */
		void Reserve(sizeT size);
		/**
		 * Reduce the capacity to fit its size
		 */
		void ShrinkToFit();
		/**
		 * Clear the vectors contents
		 * @param[in] resize	If the vector needs to be resized during clearing (data deallocated)
		 */
		void Clear(bool resize = false);

		/**
		 * Add a value to the back of the Stack
		 * @param[in] val	Value to add
		 */
		void Push(const T& val);
		/**
		 * Move a value to the back of the Stack
		 * @param[in] val	Value to add
		 */
		void Push(T&& val);
		/**
		 * Create and add a value to the back of the DynArray
		 * @tparam Args		Constructor argument types
		 * @param[in] args	Constructor arguments
		 */
		template<typename... Args>
		void Emplace(Args&&... args);
		/**
		 * Remove the last value from the Stack
		 */
		void Pop();

		/**
		 * Get the top element in the stack
		 * @return	Top element in the stack
		 */
		T& Top();
		/**
		 * Get the top element in the stack
		 * @return	Top element in the stack
		 */
		const T& Top() const;

		/**
		 * Check if the Stack is empty
		 * @return	If the Stack is empty
		 */
		b8 IsEmpty() const;
		/**
		 * Get the size of the Stack
		 * @return	Size of the Stack
		 */
		sizeT Size() const;
		/**
		 * Get the size of the Stack
		 * @return	Capacity of the Stack
		 */
		sizeT Capacity() const;
		/**
		 * Get a pointer to the allocator
		 * @return	Pointer to the allocator
		 */
		Memory::IAllocator* GetAllocator();
		/**
		 * Get the internal container of the stack
		 * @return	Reference to the internal container of the stack
		 */
		DynArray<T, GrowthPolicy>& GetInternalContainer();
		/**
		 * Get the internal container of the stack
		 * @return	Internal container of the stack
		 */
		const DynArray<T, GrowthPolicy>& GetInternalContainer() const;

		/**
		 * Swap the content of 2 Stacks
		 * @param[in] stack	Stack to swap content with
		 */
		void Swap(Stack& stack);

	private:
		
		BaseType m_Container;	/**< Internal container */
	};

}

#include "Inline/Stack.inl"

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename T, Hv::Container::GrowthPolicyFunc G), HV_TTYPE(Hv::Stack, T, G))