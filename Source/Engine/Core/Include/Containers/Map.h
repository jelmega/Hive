// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Map.h: Ordered map
#pragma once
#include "RedBlackTree.h"

namespace Hv {
	/**
	 * Map (sorted)
	 * @note	The containers uses multiple ways to copy data internally, using the copy-assignment operator as default,
	 *			but MemCpy and the copy-constructor can also be used. These values can be map using their relative declares:
	 *			HV_DECLARE_CONTAINER_MEMCPY and HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR and their respective template variants.
	 * @note	A map can contain only 1 instance of a key
	 * @tparam Key				Key type
	 * @tparam Value			Value type
	 * @tparam CompLess			Comparison predicate
	 */
	template<typename Key, typename Value, typename CompLess = Container::CompareLess<Key>>
	class Map
	{
	private:
		using BaseType = RedBlackTree<Pair<Key, Value>, CompLess, Container::UseFirst<Key, Value>, true, true>;
		using KeyValuePair = Pair<Key, Value>;
	public:
		using Iterator = typename BaseType::Iterator;

	public:
		/**
		 * Create a map
		 */
		Map();
		/**
		 * Create a map
		 * @param[in] pAlloc	Allocator
		 */
		Map(Memory::IAllocator* pAlloc);
		/**
		 * Create a map
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		Map(const CompLess& comp, Memory::IAllocator* pAlloc);
		/**
		 * Create a map from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] pAlloc	Allocator
		 */
		template<sizeT N>
		Map(Pair<Key, Value>(&arr)[N], Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a map  from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		template<sizeT N>
		Map(Pair<Key, Value>(&arr)[N], const CompLess& comp, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a map  from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] pAlloc	Allocator
		 */
		Map(const Pair<Key, Value>* arr, sizeT count, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a map from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		Map(const Pair<Key, Value>* arr, sizeT count, const CompLess& comp, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a map from iterators
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @param[in] pAlloc	Allocator
		 */
		Map(const Pair<Key, Value>* itFirst, const Pair<Key, Value>* itLast, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a map from iterators
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		Map(const Pair<Key, Value>* itFirst, const Pair<Key, Value>* itLast, const CompLess& comp, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a map from iterators
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 * @param[in] pAlloc		Allocator
		 */
		template<typename InputIterator>
		Map(const InputIterator& itFirst, const InputIterator& itLast, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a map from iterators
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 * @param[in] comp			Comparison predicate
		 * @param[in] pAlloc		Allocator
		 */
		template<typename InputIterator>
		Map(const InputIterator& itFirst, const InputIterator& itLast, const CompLess& comp, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a map from an initializer list
		 * @param[in] il		Initializer list
		 * @param[in] pAlloc	Allocator
		 */
		Map(std::initializer_list<Pair<Key, Value>> il, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a map from an initializer list
		 * @param[in] il		Initializer list
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		Map(std::initializer_list<Pair<Key, Value>> il, const CompLess& comp, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a map from another map
		 * @param[in] map		map
		 */
		Map(const Map& map);
		/**
		 * Create a map from another map
		 * @param[in] map		map
		 * @param[in] pAlloc	Allocator
		 */
		Map(const Map& map, Memory::IAllocator* pAlloc);
		/**
		 * Move a map into this map
		 * @param[in] map		map
		 */
		Map(Map&& map) noexcept;
		~Map();

		template<sizeT N>
		Map& operator=(Pair<Key, Value>(&arr)[N]);
		Map& operator=(std::initializer_list<Pair<Key, Value>> il);
		Map& operator=(const Map& map);
		Map& operator=(Map&& map) noexcept;

		Value& operator[](const Key& key);
		const Value& operator[](const Key& key) const;

		/**
		 * Insert a pair into the map
		 * @param[in] pair	Value pair
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const Pair<Key, Value>& pair);
		/**
		 * Insert a key and value into the map
		 * @param[in] key	Key
		 * @param[in] value	Value
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const Key& key, const Value& value);
		/**
		 * Move a pair into the map
		 * @param[in] pair	Value pair
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(Pair<Key, Value>&& pair);
		/**
		 * Move a key and value into the map
		 * @param[in] key	Key
		 * @param[in] value	Value
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(Key&& key, Value&& value);
		/**
		 * Insert a pair into the map, with a hint
		 * @param[in] pair	Value pair
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const Iterator& hint, const Pair<Key, Value>& pair);
		/**
		 * Insert a key and value into the map, with a hint
		 * @param[in] key	Key
		 * @param[in] value	Value
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const Iterator& hint, const Key& key, const Value& value);
		/**
		 * Move a pair into the map, with a hint
		 * @param[in] pair	Value pair
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const Iterator& hint, Pair<Key, Value>&& pair);
		/**
		 * Move a key and value into the map, with a hint
		 * @param[in] key	Key
		 * @param[in] value	Value
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const Iterator& hint, Key&& key, Value&& value);
		/**
		 * Insert an array into the map
		 * @tparam N		Array size
		 * @param[in] arr	Array
		 */
		template<sizeT N>
		void Insert(Pair<Key, Value>(&arr)[N]);
		/**
		 * Insert an array into the map
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 */
		void Insert(const Pair<Key, Value>* arr, sizeT count);
		/**
		 * Insert a range of value into the map
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		void Insert(const Pair<Key, Value>* itFirst, const Pair<Key, Value>* itLast);
		/**
		 * Insert a range of value into the map
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 */
		template<typename InputIterator>
		void Insert(const InputIterator& itFirst, const InputIterator& itLast);
		/**
		 * Insert an initializer list into the map
		 * @param[in] il	Initializer list
		 */
		template<typename InputIterator>
		void Insert(std::initializer_list<Pair<Key, Value>> il);

		/**
		 * Create and insert a value into the map, with a provided key
		 * @tparam Args		Constructor argument types
		 * @param[in] key	Key
		 * @param[in] args	Constructor arguments for Value
		 * @return			Pair with info about the emplacement
		 *					-first is the iterator to the emplaced value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		template<typename... Args>
		Pair<Iterator, b8> Emplace(const Key& key, Args&&... args);
		/**
		 * Create and insert a value into the map, with a hint, with a provided key
		 * @tparam Args		Constructor argument types
		 * @param[in] key	Key
		 * @param[in] args	Constructor arguments for Value
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Pair with info about the emplacement
		 *					-first is the iterator to the emplaced value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		template<typename... Args>
		Pair<Iterator, b8> EmplaceHint(const Iterator& hint, const Key& key, Args&&... args);

		/**
		 * Erase a value from the map
		 * @param[in] key	Key to erase
		 * @return			Amount of values removed, either 1 or 0
		 */
		sizeT Erase(const Key& key);
		/**
		 * Erase an iterator from the map
		 * @param[in] it	Iterator to erase
		 * @return			Iterator after the removed element
		 */
		Iterator Erase(const Iterator& it);
		/**
		 * Erase a range of values
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @return				Iterator after the last removed element
		 */
		Iterator Erase(const Iterator& itFirst, const Iterator& itLast);

		/**
		 * Clear the map
		 */
		void Clear();

		/**
		 * Find a value in the map
		 * @param[in] key	Key
		 */
		Iterator Find(const Key& key) const;
		/**
		 * Find the first occurance where the predicate returns true
		 * @tparam U			Value type
		 * @tparam Pred		Predicate type
		 * @param[in] val	Value to compare width
		 * @param[in] pred	Compare less predicate (1st param: value, 2nd param: value of type U)
		 */
		template<typename U, typename Pred>
		Iterator FindAs(const U& val, const Pred& pred) const;

		/**
		* Get the amount of entries that have a certain key
		* @param[in] key	Key to count
		* @return			Amount of entries of 'key'
		*/
		sizeT Count(const Key& key) const;

		/**
		 * Find the first occurance which isn't smaller than a value
		 * @param[in] key	Key
		 */
		Iterator LowerBound(const Key& key)  const;
		/**
		 * Find the last occurance which isn't larger than a value
		 * @param[in] key	Key
		 */
		Iterator UpperBound(const Key& key)  const;
		/**
		 * Find the first occurance which isn't smaller and last occurance which isn't larger than a value
		 * @param[in] key	Key
		 */
		Pair<Iterator, Iterator> EqualRange(const Key& key)  const;

		/**
		 * Swap the content of 2 maps
		 * @param[in] map	Map to swap content with
		 */
		void Swap(Map& map);

		/**
		 * Check if the map is empty
		 * @return	If the map is empty
		 */
		b8 IsEmpty() const;
		/**
		 * Get the size of the map
		 * @return	Size of the map
		 */
		sizeT Size() const;
		/**
		 * Get a pointer to the allocator
		 * @return	Pointer to the allocator
		 */
		Memory::IAllocator* GetAllocator()  const;

		/**
		 * Return an iterator to the front/begin of the map
		 * @return	Iterator to the front/begin of the map
		 */
		Iterator Front() const;
		/**
		 * Return an iterator to the last element of the map
		 * @return	Iterator to the last element of the map
		 */
		Iterator Last() const;
		/**
		 * Return an iterator to the back/end of the map
		 * @return	Iterator to the back/end of the map
		 */
		Iterator Back() const;

		/**
		 * Return an iterator to the begin of the map
		 * @note	Function meant to be used for range-based for-loops, use Front instead
		 * @return	Iterator to the begin of the map
		 */
		Iterator begin() const;
		/**
		 * Return an iterator to the end of the map
		 * @note	Function meant to be used for range-based for-loops, use Back instead
		 * @return	Iterator to the end of the map
		 */
		Iterator end() const;

	private:

		BaseType m_Tree;	/**< Internal tree */

	};


}

#include "Inline/Map.inl"

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename K, typename V, typename C), HV_TTYPE(Hv::Map, K, V, C))
