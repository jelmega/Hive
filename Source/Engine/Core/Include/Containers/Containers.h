// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Container.h: General container include header
#pragma once

// 'Sequential'
#include "Array.h"
#include "DynArray.h"
#include "Stack.h"
#include "Deque.h"
#include "Queue.h"

// Linked lists
#include "List.h"
#include "SList.h"

// Ordered containers
#include "RedBlackTree.h"
#include "Set.h"
#include "MultiSet.h"
#include "Map.h"
#include "MultiMap.h"

// Hash/unordered containers
#include "HashTable.h"
#include "HashSet.h"
#include "HashMultiSet.h"
#include "HashMap.h"
#include "HashMultiMap.h"

// Other
#include "Flags.h"
#include "Bitset.h"