// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Flags.h: Flags Wrapper
#pragma once
#include "Core/CoreHeaders.h"
#include "Containers/ContainerUtils.h"

namespace Hv {

	template<typename Value, typename Enum>
	class Flags
	{
		HV_STATIC_ASSERT_MSG(Traits::IsUnsignedV<Value>, "Storage type needs to be an unsigned interger");
		HV_STATIC_ASSERT_MSG(Traits::IsEnumV<Enum>, "Enum type needs to be an enum");
	public:
		/**
		* Create flags set to 0
		*/
		Flags();
		/**
		* Create flags from enum flags
		* @param[in] flags	Enum flags
		*/
		Flags(Enum flags);
		/**
		* Create flags from a value
		* @param[in] flags	Value
		*/
		Flags(Value flags);
		/**
		* Create flags from other flags
		* @param[in] flags	Flags
		*/
		Flags(const Flags& flags);
		/**
		* Move flags into these flags
		*/
		Flags(Flags&& flags) noexcept;

		Flags operator~() const;

		Flags operator+(const Flags& flags) const;
		Flags operator+(Enum flags) const;
		Flags operator+(Value flags) const;
		Flags operator-(const Flags& flags) const;
		Flags operator-(Enum flags) const;
		Flags operator-(Value flags) const;
		Flags operator&(const Flags& flags) const;
		Flags operator&(Enum flags) const;
		Flags operator&(Value flags) const;
		Flags operator|(const Flags& flags) const;
		Flags operator|(Enum flags) const;
		Flags operator|(Value flags) const;
		Flags operator^(const Flags& flags) const;
		Flags operator^(Enum flags) const;
		Flags operator^(Value flags) const;

		Flags& operator=(Enum flags);
		Flags& operator=(Value flags);
		Flags& operator=(const Flags& flags);
		Flags& operator=(Flags&& flags) noexcept;

		Flags& operator+=(const Flags& flags);
		Flags& operator+=(Enum flags);
		Flags& operator+=(Value flags);
		Flags& operator-=(const Flags& flags);
		Flags& operator-=(Enum flags);
		Flags& operator-=(Value flags);
		Flags& operator&=(const Flags& flags);
		Flags& operator&=(Enum flags);
		Flags& operator&=(Value flags);
		Flags& operator|=(const Flags& flags);
		Flags& operator|=(Enum flags);
		Flags& operator|=(Value flags);
		Flags& operator^=(const Flags& flags);
		Flags& operator^=(Enum flags);
		Flags& operator^=(Value flags);

		b8 operator==(const Flags& flags) const;
		b8 operator==(Enum flags) const;
		b8 operator==(Value flags) const;
		b8 operator!=(const Flags& flags) const;
		b8 operator!=(Enum flags) const;
		b8 operator!=(Value flags) const;

		operator b8() const;
		operator u8() const;
		operator u16() const;
		operator u32() const;
		operator u64() const;
		operator Enum() const;

		/**
		* Set 1 or more flags
		* @param[in] flags	Flags to set
		*/
		void Set(const Flags& flags);
		/**
		* Set 1 or more flags
		* @param[in] flags	Flags to set
		*/
		void Set(Enum flags);
		/**
		* Set 1 or more flags
		* @param[in] flags	Flags to set
		*/
		void Set(Value flags);
		/**
		* Set 1 or more flags to 'set'
		* @param[in] flags	Flags to modify
		* @param[in] set	Value to set flags to
		*/
		void Set(const Flags& flags, b8 set);
		/**
		* Set 1 or more flags to 'set'
		* @param[in] flags	Flags to modify
		* @param[in] set	Value to set flags to
		*/
		void Set(Enum flags, b8 set);
		/**
		* Set 1 or more flags to 'set'
		* @param[in] flags	Flags to modify
		* @param[in] set	Value to set flags to
		*/
		void Set(Value flags, b8 set);
		/**
		* Unset 1 or more flags
		* @param[in] flags	Flags to set
		*/
		void Unset(const Flags& flags);
		/**
		* Unset 1 or more flags
		* @param[in] flags	Flags to set
		*/
		void Unset(Enum flags);
		/**
		* Unset 1 or more flags
		* @param[in] flags	Flags to set
		*/
		void Unset(Value flags);

		/**
		* Check if flags are set
		* @param[in]flags	Flags to check
		* @return			If flags are set
		*/
		b8 IsSet(const Flags& flags) const;
		/**
		* Check if flags are set
		* @param[in]flags	Flags to check
		* @return			If flags are set
		*/
		b8 IsSet(Enum flags) const;
		/**
		* Check if flags are set
		* @param[in]flags	Flags to check
		* @return			If flags are set
		*/
		b8 IsSet(Value flags) const;
		/**
		* Check if flags are not set
		* @param[in]flags	Flags to check
		* @return			If flags are set
		*/
		b8 IsNotSet(const Flags& flags) const;
		/**
		* Check if flags are not set
		* @param[in]flags	Flags to check
		* @return			If flags are set
		*/
		b8 IsNotSet(Enum flags) const;
		/**
		* Check if flags are not set
		* @param[in]flags	Flags to check
		* @return			If flags are set
		*/
		b8 IsNotSet(Value flags) const;
		/**
		* Check if any flags are set
		* @param[in]flags	Flags to check
		* @return			If any flags are set
		*/
		b8 IsAnySet(const Flags& flags) const;
		/**
		* Check if any flags are set
		* @param[in]flags	Flags to check
		* @return			If any flags are set
		*/
		b8 IsAnySet(Enum flags) const;
		/**
		* Check if any flags are set
		* @param[in]flags	Flags to check
		* @return			If any flags are set
		*/
		b8 IsAnySet(Value flags) const;

		/**
		* Check if no flags are set
		* @return	If no flags are set
		*/
		b8 None() const;
		/**
		* Check if any flags are set
		* @return	If any flags are set
		*/
		b8 Any() const;
		/**
		* Check if all flags are set
		* @return	If all flags are set
		*/
		b8 All() const;

		void Swap(Flags& flags);

	private:
		Value m_Flags;

	};

}

#include "Inline/Flags.inl"

HV_DECLARE_CONTAINER_MEMCPY_TEMPLATE(HV_TARGS(typename V, typename E), HV_TTYPE(Hv::Flags, V, E))