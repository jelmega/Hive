// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// IntrusiveList.h: Intrusive list
#pragma once
#include "Core/CoreHeaders.h"
#include "Containers/ContainerUtils.h"

namespace Hv {

	/**
	* Intrusive list node
	* @note	Used as base for any class that needs to be a node in an intrusive list
	*/
	class IntrusiveListNode
	{
	public:
		/**
		* Create a instrusive list node
		*/
		IntrusiveListNode();
		virtual ~IntrusiveListNode();

	private:
		template<typename Node>
		friend class IntrusiveList;

		IntrusiveListNode* m_pPrev;
		IntrusiveListNode* m_pNext;
	};

	/**
	* Instrusive list
	* @tparam Node	Node type, derived from IntrusiveListNode
	*/
	template<typename Node>
	class IntrusiveList
	{
		HV_STATIC_ASSERT((Traits::IsBaseOfV<IntrusiveList, Node>), "Node needs to be derived from IntrusiveListNode!");
	public:

		class Iterator
		{
		public:
			/**
			* Create an empty iterator
			*/
			Iterator();
			/**
			* Create an iterator from a node
			* @param[in] pNode	Node
			*/
			Iterator(IntrusiveListNode* pNode);
			/**
			* Create an iterator from another iterator
			* @param[in] it	Other iterator
			*/
			Iterator(const Iterator& it);
			/**
			* Move an iterator into this iterator
			* @param[in] it	Other iterator
			*/
			Iterator(Iterator&& it) noexcept;

			Iterator& operator++();
			Iterator operator++(int);
			Iterator& operator--();
			Iterator operator--(int);

			Iterator operator+(sizeT count) const;
			Iterator operator-(sizeT count) const;

			Iterator& operator=(const Iterator& it);
			Iterator& operator=(Iterator&& it) noexcept;
			Iterator& operator+=(sizeT count);
			Iterator& operator-=(sizeT count);

			Node& operator*();
			const Node& operator*() const;
			Node* operator->();
			const Node* operator->() const;

			b8 operator==(const Iterator& it) const;
			b8 operator!=(const Iterator& it) const;

		private:
			friend class IntrusiveList<Node>;

			Node* m_pNode;
		};

	public:
		/**
		* Create an empty intrusive list
		*/
		IntrusiveList();
		IntrusiveList(IntrusiveList&& list) noexcept;
		~IntrusiveList();

		IntrusiveList& operator=(IntrusiveList&& list) noexcept;

		/**
		* Add a node to the back of the List
		* @param[in] node	Node to add
		*/
		void PushBack(Node& node);
		/**
		* Remove the last node from the List
		*/
		void PopBack();
		/**
		* Add a node to the front of the List
		* @param[in] node	Node to add
		*/
		void PushFront(Node& node);
		/**
		* Remove the first node from the List
		*/
		void PopFront();

		/**
		* Insert a node into the intrusive list
		* @param[in] it	Position to insert
		* @param[in] node	Node
		*/
		void Insert(Iterator& it, Node& node);

		/**
		* Remove a node from the intrusive list
		* @param[in] it	Iterator to node to remove
		*/
		void Remove(Iterator& it);
		/**
		* Remove an amount of values from the intrusive list
		* @param[in] it	Iterator to first node to remove
		* @param[in] count	Amount of nodes to remove
		*/
		void Remove(Iterator& it, sizeT count);
		/**
		* Remove a range of values from the intrusive list
		* @param[in] itFirst	Iterator to first node to remove
		* @param[in] itLast	Iterator after last node to remove
		*/
		void Remove(Iterator& itFirst, Iterator& itLast);

		/**
		* Clear the instrusive list
		*/
		void Clear();

		/**
		* Swap the content of 2 intrusive lists
		* @param[in] list	List to swap content with
		*/
		void Swap(IntrusiveList& list);

		/**
		* Check if the list is empty
		* @return	If the list is empty
		*/
		b8 IsEmpty() const;
		/**
		* Get the size of the List
		* @return	Size of the List
		*/
		sizeT Size() const;

		/**
		* Return an iterator to the front/begin of the list
		* @return	InputIterator to the front/begin of the list
		*/
		Iterator Front() const;
		/**
		* Return an iterator to the last element of the list
		* @return	InputIterator to the last element of the list
		*/
		Iterator Last() const;
		/**
		* Return an iterator to the back/end of the list
		* @return	InputIterator to the back/end of the list
		*/
		Iterator Back() const;

		/**
		* Return an iterator to the begin of the array
		* @note	Function meant to be used for range-based for-loops, use Front instead
		* @return	InputIterator to the begin of the array
		*/
		Iterator begin() const;
		/**
		* Return an iterator to the end of the array
		* @note	Function meant to be used for range-based for-loops, use Back instead
		* @return	InputIterator to the end of the array
		*/
		Iterator end() const;

	private:
		HV_MAKE_NON_COPYABLE(Hv::IntrusiveList<Node>);

		IntrusiveListNode* m_pHead;
		IntrusiveListNode* m_pTail;
	};

}

#include "Inline/IntrusiveList.inl"

HV_DECLARE_CONTAINER_MEMCPY_TEMPLATE(HV_TARGS(typename Node), HV_TTYPE(Hv::IntrusiveList, Node))