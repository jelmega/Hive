// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DynArray.h: Dynamic array
#pragma once
#include "Core/CoreHeaders.h"
#include "ContainerUtils.h"
#include "Memory/Memory.h"

namespace Hv {
	
	/**
	 * Dynamic array
	 * @note	The containers uses multiple ways to copy data internally, using the copy-assignment operator as default,
	 *			but MemCpy and the copy-constructor can also be used. These values can be set using their relative declares:
	 *			HV_DECLARE_CONTAINER_MEMCPY and HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR and their respective template variants.
	 * @tparam T			Element type
	 * @tparam GrowthPolicy	Growth policy
	 */
	template<typename T, Container::GrowthPolicyFunc GrowthPolicy = Container::GrowthDouble>
	class DynArray
	{
	public:
		/**
		 * Create an empty DynArray
		 */
		DynArray();
		/**
		 * Create an empty DynArray
		 * @param[in] pAlloc	Allocator
		 */
		DynArray(Memory::IAllocator* pAlloc);
		/**
		 * Create an DynArray with a size
		 * @param[in] size		Size
		 * @param[in] pAlloc	Allocator
		 */
		DynArray(sizeT size, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create an DynArray of a certain size, filled with a value
		 * @param[in] count		Amount of values
		 * @param[in] value		Value
		 * @param[in] pAlloc	Allocator
		 */
		DynArray(sizeT count, const T& value, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create an DynArray from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] pAlloc	Allocator
		 */
		template<sizeT N>
		DynArray(T(&arr)[N], Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a DynArray from an array
		 * @param[in] arr		Array
		 * @param[in] count		Array size
		 * @param[in] pAlloc	Allocator
		 */
		DynArray(const T* arr, sizeT count, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a DynArray from iterators
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @param[in] pAlloc	Allocator
		 */
		DynArray(const T* itFirst, const T* itLast, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a DynArray from iterators
		 * @tparam Iterator		Forward iterator
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @param[in] pAlloc	Allocator
		 */
		template<typename Iterator>
		DynArray(const Iterator& itFirst, const Iterator& itLast, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a DynArray from an initializer list
		 * @tparam Iterator		Forward iterator
		 * @param[in] il		Initializer list
		 * @param[in] pAlloc	Allocator
		 */
		DynArray(std::initializer_list<T> il, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a DynArray from another DynArray
		 * @tparam OtherPolicy	Growth policy of the other DynArray
		 * @param[in] dynArr	Other DynArr
		 */
		template<Container::GrowthPolicyFunc OtherPolicy>
		DynArray(const DynArray<T, OtherPolicy>& dynArr);
		/**
		 * Create a DynArray from another DynArray
		 * @param[in] dynArr	Other DynArr
		 */
		DynArray(const DynArray& dynArr);
		/**
		 * Create a DynArray from another DynArray
		 * @tparam OtherPolicy	Growth policy of the other DynArray
		 * @param[in] dynArr	Other DynArr
		 * @param[in] pAlloc	Allocator
		 */
		template<Container::GrowthPolicyFunc OtherPolicy>
		DynArray(const DynArray<T, OtherPolicy>& dynArr, Memory::IAllocator* pAlloc);
		/**
		 * Create a DynArray from another DynArray
		 * @param[in] dynArr	Other DynArr
		 * @param[in] pAlloc	Allocator
		 */
		DynArray(const DynArray& dynArr, Memory::IAllocator* pAlloc);
		/**
		 * Move a DynArray into this DynArray
		 * @tparam OtherPolicy	Growth policy of the other DynArray
		 * @param[in] dynArr	Other DynArr
		 */
		template<Container::GrowthPolicyFunc OtherPolicy>
		DynArray(DynArray<T, OtherPolicy>&& dynArr);
		/**
		 * Move a DynArray into this DynArray
		 * @param[in] dynArr	Other DynArr
		 */
		DynArray(DynArray&& dynArr) noexcept;
		~DynArray();

		template<sizeT N>
		DynArray& operator=(T(&arr)[N]);
		DynArray& operator=(std::initializer_list<T> il);
		template<Container::GrowthPolicyFunc OtherPolicy>
		DynArray& operator=(const DynArray<T, OtherPolicy>& dynArr);
		DynArray& operator=(const DynArray& dynArr);
		template<Container::GrowthPolicyFunc OtherPolicy>
		DynArray& operator=(DynArray<T, OtherPolicy>&& dynArr);
		DynArray& operator=(DynArray&& dynArr) noexcept;

		T& operator[](sizeT index);
		const T& operator[](sizeT index) const;

		/**
		 * Fill the DynArray with a value for 'count' indices
		 * @param[in] val	Value
		 * @param[in] count	Amount of values
		 */
		void Fill(const T& val, sizeT count);
		/**
		 * Assign an array to the DynArray
		 * @tparam N		Array size
		 * @param[in] arr	Array
		 */
		template<sizeT N>
		void Assign(const T(&arr)[N]);
		/**
		 * Assign an array to the DynArray
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 */
		void Assign(const T* arr, sizeT count);
		/**
		 * Assign a range of values to the DynArray
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		void Assign(const T* itFirst, const T* itLast);
		/**
		 * Assign a range of values to the DynArray
		 * @tparam Iterator		Forward iterator
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		template<typename Iterator>
		void Assign(const Iterator& itFirst, const Iterator& itLast);
		/**
		 * Asign an initializer list to the DynArr
		 * @param[in] il	Initializer list
		 */
		void Assign(std::initializer_list<T> il);

		/**
		 * Resize the DynArray
		 * @param[in] size	New size
		 */
		void Resize(sizeT size);
		/**
		 * Resize the DynArray
		 * @param[in] size	New size
		 * @param[in] val	Value to add if size is bigger than the current size
		 */
		void Resize(sizeT size, const T& val);
		/**
		 * Reserve space in the DynArray
		 * @param[in] size	Min size to reserve
		 */
		void Reserve(sizeT size);
		/**
		 * Reduce the capacity to fit its size
		 */
		void ShrinkToFit();
		/**
		 * Clear the vectors contents
		 * @param[in] resize	If the vector needs to be resized during clearing (data deallocated)
		 */
		void Clear(bool resize = false);

		/**
		 * Add a value to the back of the DynArray
		 * @param[in] val	Value to add
		 */
		void Push(const T& val);
		/**
		 * Move a value to the back of the DynArray
		 * @param[in] val	Value to add
		 */
		void Push(T&& val);
		/**
		 * Remove the last value from the DynArray
		 */
		void Pop();

		/**
		 * Append another DynArray to the back
		 * @tparam G			Other growth policy
		 * @param[in] dynArr	DynArray to append
		 */
		template<Container::GrowthPolicyFunc G>
		void Append(DynArray<T, G> dynArr);

		/**
		 * Insert a value into the DynArray 
		 * @param[in] index	Index of location in DynArray
		 * @param[in] val	Value to insert
		 */
		void Insert(sizeT index, const T& val);
		/**
		 * Insert a value into the DynArray
		 * @param[in] it	Iterator to location in DynArray
		 * @param[in] val	Value to insert
		 */
		void Insert(const T* it, const T& val);
		/**
		 * Insert a value into the DynArray by moving it
		 * @param[in] index	Index of location in DynArray
		 * @param[in] val	Value to insert
		 */
		void Insert(sizeT index, T&& val);
		/**
		 * Insert a value into the DynArray by moving it
		 * @param[in] it	Iterator to location in DynArray
		 * @param[in] val	Value to insert
		 */
		void Insert(const T* it, T&& val);
		/**
		 * Insert an amount of values into the DynArray
		 * @param[in] index	Index of location in DynArray
		 * @param[in] count	Amount of values to insert
		 * @param[in] val	Value to insert
		 */
		void Insert(sizeT index, sizeT count, const T& val);
		/**
		 * Insert an amount of values into the DynArray
		 * @param[in] it	Iterator to location in DynArray
		 * @param[in] count	Amount of values to insert
		 * @param[in] val	Value to insert
		 */
		void Insert(const T* it, sizeT count, const T& val);
		/**
		 * Insert an array into the DynArray
		 * @tparam N			Array size
		 * @param[in] index	Index of location in DynArray
		 * @param[in] arr	Array
		 */
		template<sizeT N>
		void Insert(sizeT index, T(&arr)[N]);
		/**
		 * Insert an array into the DynArray
		 * @tparam N			Array size
		 * @param[in] it	Iterator to location in DynArray
		 * @param[in] arr	Array
		 */
		template<sizeT N>
		void Insert(const T* it, T(&arr)[N]);
		/**
		 * Insert an array into the DynArray
		 * @param[in] index	Index of location in DynArray
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 */
		void Insert(sizeT index, T* arr, sizeT count);
		/**
		 * Insert an array into the DynArray
		 * @param[in] it	Iterator to location in DynArray
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 */
		void Insert(const T* it, T* arr, sizeT count);
		/**
		 * Insert a range of values into the DynArray
		 * @param[in] index		Index of location in DynArray
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		void Insert(sizeT index, const T* itFirst, const T* itLast);
		/**
		 * Insert a range of values into the DynArray
		 * @param[in] it		Iterator to location in DynArray
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		void Insert(const T* it, const T* itFirst, const T* itLast);
		/**
		 * Insert a range of values into the DynArray
		 * @tparam Iterator		Forward Iterator
		 * @param[in] index		Index of location in DynArray
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		template<typename Iterator>
		void Insert(sizeT index, const Iterator& itFirst, const Iterator& itLast);
		/**
		 * Insert a range of values into the DynArray
		 * @tparam Iterator		Forward Iterator
		 * @param[in] it		Iterator to location in DynArray
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		template<typename Iterator>
		void Insert(const T* it, const Iterator& itFirst, const Iterator& itLast);
		/**
		 * Insert an initializer list into the DynArray
		 * @param[in] index	Index of location in DynArray
		 * @param[in] il	Initializer list
		 */
		void Insert(sizeT index, std::initializer_list<T> il);
		/**
		 * Insert an initializer list into the DynArray
		 * @param[in] it	Iterator to location in DynArray
		 * @param[in] il	Initializer list
		 */
		void Insert(const T* it, std::initializer_list<T> il);

		/**
		 * Erase a value
		 * @param[in] index	Index of location in DynArray
		 */
		void Erase(sizeT index);
		/**
		 * Erase a value
		 * @param[in] it	Iterator to location in DynArray
		 */
		void Erase(const T* it);
		/**
		 * Erase an amount of value
		 * @param[in] index	Index of location in DynArray
		 * @param[in] count	Amount of values to erase
		 */
		void Erase(sizeT index, sizeT count);
		/**
		 * Erase an amount of value
		 * @param[in] it	Iterator to location in DynArray
		 * @param[in] count	Amount of values to erase
		 */
		void Erase(const T* it, sizeT count);
		/**
		 * Erase a range of values
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	Iterator to element after last element
		 */
		void Erase(const T* itFirst, const T* itLast);

		/**
		 * Create and insert a value
		 * @tparam Args	Constructor argument types
		 * @param[in] index	Index of location in DynArray
		 * @param[in] args	Constructor arguments
		 */
		template<typename... Args>
		void Emplace(sizeT index, Args&&... args);
		/**
		 * Create and insert a value
		 * @tparam Args	Constructor argument types
		 * @param[in] it	Iterator to location in DynArray
		 * @param[in] args	Constructor arguments
		 */
		template<typename... Args>
		void Emplace(const T* it, Args&&... args);
		/**
		 * Create and add a value to the back of the DynArray
		 * @tparam Args	Constructor argument types
		 * @param[in] args	Constructor arguments
		 */
		template<typename... Args>
		void EmplaceBack(Args&&... args);

		/**
		 * Find the first element in the array
		 * @param[in] value		Value to find
		 * @return				Iterator to the found element, or Back if no element was found 
		 */
		T* Find(const T& value);
		/**
		 * Find the first element in the array
		 * @param[in] value		Value to find
		 * @return				Iterator to the found element, or Back if no element was found 
		 */
		const T* Find(const T& value) const;

		/**
		 * Swap the content of 2 DynArrays
		 * @param[in] dynArr	DynArray to swap content with
		 */
		void Swap(DynArray& dynArr);

		/**
		 * Check if the DynArray is empty
		 * @return	If the DynArray is empty
		 */
		b8 IsEmpty() const;
		/**
		 * Get the size of the DynArray
		 * @return	Size of the DynArray
		 */
		sizeT Size() const;
		/**
		 * Get the size of the DynArray
		 * @return	Capacity of the DynArray
		 */
		sizeT Capacity() const;
		/**
		 * Get a pointer to the data
		 * @return	Pointer to the data
		 */
		T* Data();
		/**
		 * Get a pointer to the data
		 * @return	Pointer to the data
		 */
		const T* Data() const;
		/**
		 * Get a pointer to the allocator
		 * @return	Pointer to the allocator
		 */
		Memory::IAllocator* GetAllocator();

		/**
		 * Return an iterator to the front/begin of the array
		 * @return	Iterator to the front/begin of the array
		 */
		T* Front();
		/**
		 * Return an iterator to the front/begin of the array
		 * @return	Iterator to the front/begin of the array
		 */
		const T* Front() const;
		/**
		 * Return an iterator to the last element of the array
		 * @return	Iterator to the last element of the array
		 */
		T* Last();
		/**
		 * Return an iterator to the last element of the array
		 * @return	InputIterator to the last element of the array
		 */
		const T* Last() const;
		/**
		 * Return an iterator to the back/end of the array
		 * @return	Iterator to the back/end of the array
		 */
		T* Back();
		/**
		 * Return an iterator to the back/end of the array
		 * @return	Iterator to the back/end of the array
		 */
		const T* Back() const;

		/**
		 * Return an iterator to the begin of the array
		 * @note	Function meant to be used for range-based for-loops, use Front instead
		 * @return	Iterator to the begin of the array
		 */
		T* begin();
		/**
		 * Return an iterator to the begin of the array
		 * @note	Function meant to be used for range-based for-loops, use Front instead
		 * @return	Iterator to the begin of the array
		 */
		const T* begin() const;
		/**
		 * Return an iterator to the end of the array
		 * @note	Function meant to be used for range-based for-loops, use Back instead
		 * @return	Iterator to the end of the array
		 */
		T* end();
		/**
		 * Return an iterator to the end of the array
		 * @note	Function meant to be used for range-based for-loops, use Back instead
		 * @return	Iterator to the end of the array
		 */
		const T* end() const;

	private:
		template<typename, Container::GrowthPolicyFunc>
		friend class DynArray;

		T* m_Data;						/**< Data */
		sizeT m_Size;					/**< Size */
		sizeT m_Capacity;				/**< Capacity */
		Memory::IAllocator* m_pAlloc;	/**< Pointer to allocator*/
	};

}
#include "Inline/DynArray.inl"

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename T, Hv::Container::GrowthPolicyFunc G), HV_TTYPE(Hv::DynArray, T, G))
