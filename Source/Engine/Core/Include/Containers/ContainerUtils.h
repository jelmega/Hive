// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ContainerUtils.h: Container utilities
#pragma once
#include "Core/CoreHeaders.h"
#include "Core/Pair.h"

namespace Hv::Container {
	
	namespace Detail {

		constexpr sizeT g_Primes[] = {
			2u         , 3u         , 5u         , 7u         , 11u        , 13u        , 17u        , 19u        ,
			23u        , 29u        , 31u        , 37u        , 41u        , 43u        , 47u        , 53u        ,
			59u        , 61u        , 67u        , 71u        , 73u        , 79u        , 83u        , 89u        ,
			97u        , 103u       , 109u       , 113u       , 127u       , 137u       , 139u       , 149u       ,
			157u       , 167u       , 179u       , 193u       , 199u       , 211u       , 227u       , 241u       ,
			257u       , 277u       , 293u       , 313u       , 337u       , 359u       , 383u       , 409u       ,
			439u       , 467u       , 503u       , 541u       , 577u       , 619u       , 661u       , 709u       ,
			761u       , 823u       , 887u       , 953u       , 1031u      , 1109u      , 1193u      , 1289u      ,
			1381u      , 1493u      , 1613u      , 1741u      , 1879u      , 2029u      , 2179u      , 2357u      ,
			2549u      , 2753u      , 2971u      , 3209u      , 3469u      , 3739u      , 4027u      , 4349u      ,
			4703u      , 5087u      , 5503u      , 5953u      , 6427u      , 6949u      , 7517u      , 8123u      ,
			8783u      , 9497u      , 10273u     , 11113u     , 12011u     , 12983u     , 14033u     , 15173u     ,
			16411u     , 17749u     , 19183u     , 20753u     , 22447u     , 24281u     , 26267u     , 28411u     ,
			30727u     , 33223u     , 35933u     , 38873u     , 42043u     , 45481u     , 49201u     , 53201u     ,
			57557u     , 62233u     , 67307u     , 72817u     , 78779u     , 85229u     , 92203u     , 99733u     ,
			107897u    , 116731u    , 126271u    , 136607u    , 147793u    , 159871u    , 172933u    , 187091u    ,
			202409u    , 218971u    , 236897u    , 256279u    , 277261u    , 299951u    , 324503u    , 351061u    ,
			379787u    , 410857u    , 444487u    , 480881u    , 520241u    , 562841u    , 608903u    , 658753u    ,
			712697u    , 771049u    , 834181u    , 902483u    , 976369u    , 1056323u   , 1142821u   , 1236397u   ,
			1337629u   , 1447153u   , 1565659u   , 1693859u   , 1832561u   , 1982627u   , 2144977u   , 2320627u   ,
			2510653u   , 2716249u   , 2938679u   , 3179303u   , 3439651u   , 3721303u   , 4026031u   , 4355707u   ,
			4712381u   , 5098259u   , 5515729u   , 5967347u   , 6456007u   , 6984629u   , 7556579u   , 8175383u   ,
			8844859u   , 9569143u   , 10352717u  , 11200489u  , 12117689u  , 13109983u  , 14183539u  , 15345007u  ,
			16601593u  , 17961079u  , 19431899u  , 21023161u  , 22744717u  , 24607243u  , 26622317u  , 28802401u  ,
			31160981u  , 33712729u  , 36473443u  , 39460231u  , 42691603u  , 46187573u  , 49969847u  , 54061849u  ,
			58488943u  , 63278561u  , 68460391u  , 74066549u  , 80131819u  , 86693767u  , 93793069u  , 101473717u ,
			109783337u , 118773397u , 128499677u , 139022417u , 150406843u , 162723577u , 176048909u , 190465427u ,
			206062531u , 222936881u , 241193053u , 260944219u , 282312799u , 305431229u , 330442829u , 357502601u ,
			386778277u , 418451333u , 452718089u , 489790921u , 529899637u , 573292817u , 620239453u , 671030513u ,
			725980837u , 785430967u , 849749479u , 919334987u , 994618837u , 1076067617u, 1164186217u, 1259520799u,
			1362662261u, 1474249943u, 1594975441u, 1725587117u, 1866894511u, 2019773507u, 2185171673u, 2364114217u,
			2557710269u, 2767159799u, 2993761039u, 3238918481u, 3504151727u, 3791104843u, 4101556399u, 4294967291u
		};

		constexpr sizeT g_NumPrimes = sizeof(g_Primes) / sizeof(g_Primes[0]);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Copy function Helpers													  //
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Structure to specialize the use of memcpy for a container
	 */
	template<typename T>
	struct ContainerDefaultMemCpy
	{
		static constexpr b8 Value = false;
	};

	/**
	 * Specialize the use of memcpy for a container
	 * @param[in] type			Type
	 */
#define HV_DECLARE_CONTAINER_MEMCPY(type)\
	namespace Hv::Container {\
		template<>\
		struct ContainerDefaultMemCpy<type>\
		{\
			static constexpr b8 Value = true;\
		};\
	}

	/**
	 * Specialize the use of memcpy for a container
	 * @param[in] templateArgs	Template arguments, use HV_TEMPLATE_ARGS
	 * @param[in] type			Type, use HV_TEMPLATE_TYPE
	 */
#define HV_DECLARE_CONTAINER_MEMCPY_TEMPLATE(tpargs, type)\
	namespace Hv::Container {\
		template<tpargs>\
		struct ContainerDefaultMemCpy<type>\
		{\
			static constexpr b8 Value = true;\
		};\
	}

	template<typename T>
	struct ContainerDefaultCopyConstructor
	{
		static constexpr b8 Value = false;
	};

	/**
	 * Specialize the use of memcpy for a container
	 * @param[in] type			Type
	 */
#define HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR(type)\
	namespace Hv::Container {\
		template<>\
		struct ContainerDefaultCopyConstructor<type>\
		{\
			static constexpr b8 Value = true;\
		};\
	}

	/**
	 * Specialize the use of memcpy for a container
	 * @param[in] templateArgs	Template arguments, use HV_TEMPLATE_ARGS
	 * @param[in] type			Type, use HV_TEMPLATE_TYPE
	 */
#define HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(tpargs, type)\
	namespace Hv::Container {\
		template<tpargs>\
		struct ContainerDefaultCopyConstructor<type>\
		{\
			static constexpr b8 Value = true;\
		};\
	}

	template<typename T>
	constexpr b8 UseMemCpy = Traits::IsFundamentalV<T> || Traits::IsPointerV<T> ||
							Traits::IsEnumV<T> || ContainerDefaultMemCpy<T>::Value;
	template<typename T>
	constexpr b8 UseCopyConstructor = (!UseMemCpy<T> && !Traits::IsCopyAssignableV<T> && Traits::IsCopyConstructableV<T>) || ContainerDefaultCopyConstructor<T>::Value;

	////////////////////////////////////////////////////////////////////////////////
	// Growth Policies															  //
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Growth policy function pointer
	 * @param[in] size	Min size needed
	 * @return			Capacity (space to allocate)
	 */
	using GrowthPolicyFunc = sizeT(*)(sizeT size);

	/**
	 * Double capacity growth policy
	 * @param[in] size	Min size needed
	 * @return			Capacity (space to allocate)
	 */
	HV_FORCE_INL sizeT GrowthDouble(sizeT size)
	{
		sizeT cap = 1;
		while (cap < size)
		{
			cap <<= 1;
		}
		return cap;
	}
	/**
	 * Double capacity growth policy
	 * @tparam InitialValue	Initial value of capacity
	 * @param[in] size		Min size needed
	 * @return				Capacity (space to allocate)
	 */
	template<sizeT InitialValue>
	HV_FORCE_INL sizeT GrowthDoubleValue(sizeT size)
	{
		sizeT cap = InitialValue;
		while (cap < size)
		{
			cap <<= 1;
		}
		return cap;
	}
	/**
	* Fixed size capacity growth policy
	* @tparam Increment	Incremental size
	* @param[in] size		Min size needed
	* @return				Capacity (space to allocate)
	*/
	template<sizeT Increment>
	HV_FORCE_INL sizeT GrowthFixedSize(sizeT size)
	{
		sizeT cap = Increment;
		while (cap < size)
		{
			cap += Increment;
		}
		return cap;
	}

	HV_FORCE_INL sizeT GrowthPrime(sizeT size)
	{
		sizeT cap = Detail::g_Primes[0];
		sizeT index = 1;
		while (cap < size)
		{
			cap = Detail::g_Primes[index++];
			HV_ASSERT_MSG(index != Detail::g_NumPrimes, "Prime size limit reached!");
		}
		return cap;
	}

	////////////////////////////////////////////////////////////////////////////////
	// Helper Functions															  //
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Key extraction (use self)
	 * @tparam T	Value type
	 * @param[in] t	Value
	 * @return		Value (t)
	 */
	template<typename T>
	struct UseSelf
	{
		HV_FORCE_INL const T& operator()(const T& t) const
		{
			return t;
		}
	};

	/**
	 * Key extraction (use self)
	 * @tparam Key		Key type
	 * @tparam Value	Value type
	 * @param[in] t		Value
	 * @return			Value (t.first)
	 */
	template<typename Key, typename Value>
	struct UseFirst
	{
		HV_FORCE_INL const Key& operator()(const Pair<Key, Value>& pair) const
		{
			return pair.first;
		}
	};

	/**
	 * Compare less
	 * @tparam T		Value type
	 * @param[in] val0	1st value
	 * @param[in] val1	2nd value
	 * @return			True if val0 is less than val1
	 */
	template<typename T>
	struct CompareLess
	{
		HV_FORCE_INL b8 operator()(const T& val0, const T& val1) const
		{
			return val0 < val1;
		}
	};

	/**
	 * Compare equal
	 * @tparam T		Value type
	 * @param[in] val0	1st value
	 * @param[in] val1	2nd value
	 * @return			True if val0 is equal to val1
	 */
	template<typename T>
	struct CompareEqual
	{
		HV_FORCE_INL b8 operator()(const T& val0, const T& val1) const
		{
			return val0 == val1;
		}
	};

}

////////////////////////////////////////////////////////////////////////////////
// Additional container declares for types included in ContainerUtils.h		  //
////////////////////////////////////////////////////////////////////////////////
HV_DECLARE_CONTAINER_MEMCPY_TEMPLATE(HV_TARGS(typename T0, typename T1), HV_TTYPE(Hv::Pair, T0, T1));