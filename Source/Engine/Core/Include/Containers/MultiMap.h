// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Map.h: Ordered multi map
#pragma once
#include "RedBlackTree.h"

namespace Hv {
	
	/**
	 * MultiMap (sorted)
	 * @note	The containers uses multiple ways to copy data internally, using the copy-assignment operator as default,
	 *			but MemCpy and the copy-constructor can also be used. These values can be multimap using their relative declares:
	 *			HV_DECLARE_CONTAINER_MEMCPY and HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR and their respective template variants.
	 * @note	A multimap can contain only 1 instance of a key
	 * @tparam Key				Key type
	 * @tparam Value			Value type
	 * @tparam CompLess			Comparison predicate
	 */
	template<typename Key, typename Value, typename CompLess = Container::CompareLess<Key>>
	class MultiMap
	{
	private:
		using BaseType = RedBlackTree<Pair<Key, Value>, CompLess, Container::UseFirst<Key, Value>, false, true>;
		using KeyValuePair = Pair<Key, Value>;
	public:
		using Iterator = typename BaseType::Iterator;

	public:
		/**
		 * Create a multimap
		 */
		MultiMap();
		/**
		 * Create a multimap
		 * @param[in] pAlloc	Allocator
		 */
		MultiMap(Memory::IAllocator* pAlloc);
		/**
		 * Create a multimap
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		MultiMap(const CompLess& comp, Memory::IAllocator* pAlloc);
		/**
		 * Create a multimap from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] pAlloc	Allocator
		 */
		template<sizeT N>
		MultiMap(Pair<Key, Value>(&arr)[N], Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap  from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		template<sizeT N>
		MultiMap(Pair<Key, Value>(&arr)[N], const CompLess& comp, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap  from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] pAlloc	Allocator
		 */
		MultiMap(const Pair<Key, Value>* arr, sizeT count, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		MultiMap(const Pair<Key, Value>* arr, sizeT count, const CompLess& comp, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap from iterators
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @param[in] pAlloc	Allocator
		 */
		MultiMap(const Pair<Key, Value>* itFirst, const Pair<Key, Value>* itLast, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap from iterators
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		MultiMap(const Pair<Key, Value>* itFirst, const Pair<Key, Value>* itLast, const CompLess& comp, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap from iterators
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 * @param[in] pAlloc		Allocator
		 */
		template<typename InputIterator>
		MultiMap(const InputIterator& itFirst, const InputIterator& itLast, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap from iterators
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 * @param[in] comp			Comparison predicate
		 * @param[in] pAlloc		Allocator
		 */
		template<typename InputIterator>
		MultiMap(const InputIterator& itFirst, const InputIterator& itLast, const CompLess& comp, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap from an initializer list
		 * @param[in] il		Initializer list
		 * @param[in] pAlloc	Allocator
		 */
		MultiMap(std::initializer_list<Pair<Key, Value>> il, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap from an initializer list
		 * @param[in] il		Initializer list
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		MultiMap(std::initializer_list<Pair<Key, Value>> il, const CompLess& comp, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap from another multimap
		 * @param[in] multimap	multimap
		 */
		MultiMap(const MultiMap& multimap);
		/**
		 * Create a multimap from another multimap
		 * @param[in] multimap	multimap
		 * @param[in] pAlloc	Allocator
		 */
		MultiMap(const MultiMap& multimap, Memory::IAllocator* pAlloc);
		/**
		 * Move a multimap into this multimap
		 * @param[in] multimap	multimap
		 */
		MultiMap(MultiMap&& multimap) noexcept;
		~MultiMap();

		template<sizeT N>
		MultiMap& operator=(Pair<Key, Value>(&arr)[N]);
		MultiMap& operator=(std::initializer_list<Pair<Key, Value>> il);
		MultiMap& operator=(const MultiMap& multimap);
		MultiMap& operator=(MultiMap&& multimap) noexcept;

		/**
		 * Insert a pair into the multimap
		 * @param[in] pair	Value pair
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const Pair<Key, Value>& pair);
		/**
		 * Insert a key and value into the multimap
		 * @param[in] key	Key
		 * @param[in] value	Value
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const Key& key, const Value& value);
		/**
		 * Move a pair into the multimap
		 * @param[in] pair	Value pair
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(Pair<Key, Value>&& pair);
		/**
		 * Move a key and value into the multimap
		 * @param[in] key	Key
		 * @param[in] value	Value
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(Key&& key, Value&& value);
		/**
		 * Insert a pair into the multimap, with a hint
		 * @param[in] pair	Value pair
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const Iterator& hint, const Pair<Key, Value>& pair);
		/**
		 * Insert a key and value into the multimap, with a hint
		 * @param[in] key	Key
		 * @param[in] value	Value
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const Iterator& hint, const Key& key, const Value& value);
		/**
		 * Move a pair into the multimap, with a hint
		 * @param[in] pair	Value pair
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const Iterator& hint, Pair<Key, Value>&& pair);
		/**
		 * Move a key and value into the multimap, with a hint
		 * @param[in] key	Key
		 * @param[in] value	Value
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const Iterator& hint, Key&& key, Value&& value);
		/**
		 * Insert an array into the multimap
		 * @tparam N		Array size
		 * @param[in] arr	Array
		 */
		template<sizeT N>
		void Insert(Pair<Key, Value>(&arr)[N]);
		/**
		 * Insert an array into the multimap
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 */
		void Insert(const Pair<Key, Value>* arr, sizeT count);
		/**
		 * Insert a range of value into the multimap
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		void Insert(const Pair<Key, Value>* itFirst, const Pair<Key, Value>* itLast);
		/**
		 * Insert a range of value into the multimap
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 */
		template<typename InputIterator>
		void Insert(const InputIterator& itFirst, const InputIterator& itLast);
		/**
		 * Insert an initializer list into the multimap
		 * @param[in] il	Initializer list
		 */
		template<typename InputIterator>
		void Insert(std::initializer_list<Pair<Key, Value>> il);

		/**
		 * Create and insert a value into the multimap, with a provided key
		 * @tparam Args		Constructor argument types
		 * @param[in] key	Key
		 * @param[in] args	Constructor arguments for Value
		 * @return			Pair with info about the emplacement
		 *					-first is the iterator to the emplaced value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		template<typename... Args>
		Pair<Iterator, b8> Emplace(const Key& key, Args&&... args);
		/**
		 * Create and insert a value into the multimap, with a hint, with a provided key
		 * @tparam Args		Constructor argument types
		 * @param[in] key	Key
		 * @param[in] args	Constructor arguments for Value
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Pair with info about the emplacement
		 *					-first is the iterator to the emplaced value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		template<typename... Args>
		Pair<Iterator, b8> EmplaceHint(const Iterator& hint, const Key& key, Args&&... args);

		/**
		 * Erase a value from the multimap
		 * @param[in] key	Key to erase
		 * @return			Amount of values removed, either 1 or 0
		 */
		sizeT Erase(const Key& key);
		/**
		 * Erase an iterator from the multimap
		 * @param[in] it	Iterator to erase
		 * @return			Iterator after the removed element
		 */
		Iterator Erase(const Iterator& it);
		/**
		 * Erase a range of values
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @return				Iterator after the last removed element
		 */
		Iterator Erase(const Iterator& itFirst, const Iterator& itLast);

		/**
		 * Clear the multimap
		 */
		void Clear();

		/**
		 * Find a value in the multimap
		 * @param[in] key	Key
		 */
		Iterator Find(const Key& key) const;
		/**
		 * Find the first occurance where the predicate returns true
		 * @tparam U			Value type
		 * @tparam Pred		Predicate type
		 * @param[in] val	Value to compare width
		 * @param[in] pred	Compare less predicate (1st param: value, 2nd param: value of type U)
		 */
		template<typename U, typename Pred>
		Iterator FindAs(const U& val, const Pred& pred) const;

		/**
		 * Get the amount of entries that have a certain key
		 * @param[in] key	Key to count
		 * @return			Amount of entries of 'key'
		 */
		sizeT Count(const Key& key) const;

		/**
		 * Find the first occurance which isn't smaller than a value
		 * @param[in] key	Key
		 */
		Iterator LowerBound(const Key& key)  const;
		/**
		 * Find the last occurance which isn't larger than a value
		 * @param[in] key	Key
		 */
		Iterator UpperBound(const Key& key)  const;
		/**
		 * Find the first occurance which isn't smaller and last occurance which isn't larger than a value
		 * @param[in] key	Key
		 */
		Pair<Iterator, Iterator> EqualRange(const Key& key)  const;

		/**
		 * Swap the content of 2 maps
		 * @param[in] multimap	MultiMap to swap content with
		 */
		void Swap(MultiMap& multimap);

		/**
		 * Check if the multimap is empty
		 * @return	If the multimap is empty
		 */
		b8 IsEmpty() const;
		/**
		 * Get the size of the multimap
		 * @return	Size of the multimap
		 */
		sizeT Size() const;
		/**
		 * Get a pointer to the allocator
		 * @return	Pointer to the allocator
		 */
		Memory::IAllocator* GetAllocator()  const;

		/**
		 * Return an iterator to the front/begin of the multimap
		 * @return	Iterator to the front/begin of the multimap
		 */
		Iterator Front() const;
		/**
		 * Return an iterator to the last element of the multimap
		 * @return	Iterator to the last element of the multimap
		 */
		Iterator Last() const;
		/**
		 * Return an iterator to the back/end of the multimap
		 * @return	Iterator to the back/end of the multimap
		 */
		Iterator Back() const;

		/**
		 * Return an iterator to the begin of the multimap
		 * @note	Function meant to be used for range-based for-loops, use Front instead
		 * @return	Iterator to the begin of the multimap
		 */
		Iterator begin() const;
		/**
		 * Return an iterator to the end of the multimap
		 * @note	Function meant to be used for range-based for-loops, use Back instead
		 * @return	Iterator to the end of the multimap
		 */
		Iterator end() const;

	private:

		BaseType m_Tree;	/** Internal tree */

	};

}

#include "Inline/MultiMap.inl"

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename K, typename V, typename C), HV_TTYPE(Hv::MultiMap, K, V, C))
