// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HashTable.h: Hash table
#pragma once
#include "Core/CoreHeaders.h"
#include "ContainerUtils.h"
#include "Memory/Memory.h"
#include "Core/Hash.h"

namespace Hv {
	
	namespace Container {
		/**
		* Hash node
		* @tparam T		Element type
		* @tparam Cache	If the hash value needs to be cached inside the table
		*/
		template<typename T, bool Cache>
		struct HashNode
		{
			/**
			* Create a hash node
			*/
			HashNode();
			/**
			* Create a hash node from a value
			* @param[in] val	Value
			*/
			HashNode(const T& val, HashNode* pNext = nullptr);
			/**
			* Move a value in this hash node
			* @param[in] val	Value
			*/
			HashNode(T&& val, HashNode* pNext = nullptr);
			/**
			* Create a hash node from a value and a hash
			* @param[in] val	Value
			* @param[in] hash	Hash
			*/
			HashNode(const T& val, hashT hash, HashNode* pNext = nullptr);
			/**
			* Move a value in this node, with a hash
			* @param[in] val	Value
			* @param[in] hash	Hash
			*/
			HashNode(T&& val, hashT hash, HashNode* pNext = nullptr);
			/**
			* Create a hash node from another node
			* @param[in] node	Hash node
			*/
			HashNode(const HashNode& node);
			/**
			* Move a hash node into this node
			* @param[in] node	Hash node
			*/
			HashNode(HashNode&& node) noexcept;
			~HashNode();

			/**
			* Get the hash from this node
			* @return Hash
			*/
			hashT GetHash() const;
			/**
			* Set the hash of this node
			* @param[in] hash	Hash
			*/
			void SetHash(hashT hash);

			T value;
			HashNode* pNext;
		};

		template<typename T>
		struct HashNode<T, true>
		{
			HashNode();
			HashNode(const T& val, HashNode* pNext = nullptr);
			HashNode(T&& val, HashNode* pNext = nullptr);
			HashNode(const T& val, hashT hash, HashNode* pNext = nullptr);
			HashNode(T&& val, hashT hash, HashNode* pNext = nullptr);
			HashNode(const HashNode& node);
			HashNode(HashNode&& node) noexcept;
			~HashNode();

			hashT GetHash() const;
			void SetHash(hashT hash);

			T value;
			hashT hash;
			HashNode* pNext;
		};
	}

	/**
	 * HashTable
	 * @note	The containers uses multiple ways to copy data internally, using the copy-assignment operator as default,
	 *			but MemCpy and the copy-constructor can also be used. These values can be map using their relative declares:
	 *			HV_DECLARE_CONTAINER_MEMCPY and HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR and their respective template variants.
	 * @note	This class is not meant to be used by itself, this class is the internal hash table for hashsets and hashmaps
	 * @tparam T				Element type
	 * @tparam KeyCompare		Comparison predicate
	 * @tparam Hash				Hash predicate
	 * @tparam ExtractKey		Predicate used to extract the key from the element
	 * @tparam CacheHash		If the hash value needs to be cached inside the table
	 * @tparam UniqueKey		If a key can only appear once inside the tree
	 * @tparam MutableIterator	If the iterator is mutable or not (normally false for hashsets, but true for hashmaps
	 */
	template<typename T, typename KeyCompare, typename Hash, typename ExtractKey, b8 CacheHash, b8 UniqueKey, b8 MutableIterator, Container::GrowthPolicyFunc BucketGrowthPolicy>
	class HashTable
	{
	private:
		using Node = Container::HashNode<T, CacheHash>;

	public:
		class Iterator
		{
		public:
			/**
			 * Create an empty iterator
			 */
			Iterator();
			/**
			 * Create an iterator from another iterator
			 * @param[in] it	Other iterator
			 */
			Iterator(const Iterator& it);
			/**
			 * Move an iterator into this iterator
			 * @param[in] it	Other iterator
			 */
			Iterator(Iterator&& it) noexcept;
			~Iterator();

			Iterator& operator++();
			Iterator operator++(int);

			Iterator operator+(sizeT count);

			Iterator& operator=(const Iterator& it);
			Iterator& operator=(Iterator&& it) noexcept;
			Iterator& operator+=(sizeT count);

			typename std::conditional<MutableIterator, T&, const T&>::type operator*();
			const T& operator*() const;
			typename std::conditional<MutableIterator, T*, const T*>::type operator->();
			const T* operator->() const;

			b8 operator==(const Iterator& it) const;
			b8 operator!=(const Iterator& it) const;
		private:
			friend class HashTable<T, KeyCompare, Hash, ExtractKey, CacheHash, UniqueKey, MutableIterator, BucketGrowthPolicy>;
			Iterator(Node** pBucket, Node* pNode, sizeT bucketIndex, sizeT bucketCount);

			Node** m_ppBucket;		/**< Bucket */
			Node* m_pNode;			/**< Node */
			sizeT m_BucketIndex;	/***/
			sizeT m_BucketCount;	/**< Bucket count */
		};

	public:

		/**
		 * Create a hashtable
		 * @param[in] pAlloc	Allocator
		 */
		HashTable(Memory::IAllocator* pAlloc);
		/**
		 * Create a hashtable
		 * @param[in] compare	Key compare predicate
		 * @param[in] pAlloc	Allocator
		 */
		HashTable(const KeyCompare& compare, Memory::IAllocator* pAlloc);
		/**
		 * Create a hashtable
		 * @param[in] hash		Hash predicate
		 * @param[in] pAlloc	Allocator
		 */
		HashTable(const Hash& hash, Memory::IAllocator* pAlloc);
		/**
		 * Create a hashtable
		 * @param[in] compare	Key compare predicate
		 * @param[in] hash		Hash predicate
		 * @param[in] pAlloc	Allocator
		 */
		HashTable(const KeyCompare& compare, const Hash& hash, Memory::IAllocator* pAlloc);
		/**
		 * Create a hashtable
		 * @param[in] extract	Key extract predicate
		 * @param[in] pAlloc	Allocator
		 */
		HashTable(const ExtractKey& extract, Memory::IAllocator* pAlloc);
		/**
		 * Create a hashtable
		 * @param[in] compare	Key compare predicate
		 * @param[in] hash		Hash predicate
		 * @param[in] extract	Key extract predicate
		 * @param[in] pAlloc	Allocator
		 */
		HashTable(const KeyCompare& compare, const Hash& hash, const ExtractKey& extract, Memory::IAllocator* pAlloc);

		/**
		* Create a hashtable from another hashtable
		* @param[in] ht	Hashtable
		*/
		HashTable(const HashTable& ht);
		/**
		* Create a hashtable from another hashtable
		* @param[in] ht	Hashtable
		* @param[in] pAlloc	Allocator
		*/
		HashTable(const HashTable& ht, Memory::IAllocator* pAlloc);
		/**
		* Move a hashtable into this hashtable
		* @param[in] ht	Hashtable
		*/
		HashTable(HashTable&& ht) noexcept;
		~HashTable();

		HashTable& operator=(const HashTable& ht);
		HashTable& operator=(HashTable&& ht) noexcept;

		/**
		 * Insert a value into the hashtable
		 * @param[in] val	Value
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const T& val);
		/**
		 * Move a value into the hashtable
		 * @param[in] val	Value
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(T&& val);
		/**
		 * Create and insert a value into the hashtable
		 * @tparam Args		Constructor argument types
		 * @param[in] args	Constructor arguments
		 * @return			Pair with info about the emplacement
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		template<typename... Args>
		Pair<Iterator, b8> Emplace(Args&&... args);

		/**
		 * Erase the first occurance of the value from the hashtable
		 * @param[in] val	Value to remove
		 * @return			Iterator after the removed element
		 */
		Iterator Erase(const T& val);
		/**
		 * Erase the first occurance of the key from the hashtable
		 * @tparam Key		Key type, needs to match the type used for the comparison predicate
		 * @param[in] key	Key to remove
		 * @return			Iterator after the removed element
		 */
		template<typename Key>
		Iterator EraseKey(const Key& key);
		/**
		* Erase an iterator from the hashtable
		* @param[in] it	Iterator to erase
		* @return			Iterator after the removed element
		*/
		Iterator Erase(const Iterator& it);

		/**
		 * Clear the hashtable
		 * @param[in] resize	If the vector needs to be resized during clearing (data deallocated)
		 */
		void Clear(b8 resize = true);

		/**
		* Find a value in the hashtable
		* @param[in] val	Value to find
		* @return			Iterator to found value, if find failed, an iterator to the back gets returned
		*/
		Iterator Find(const T& val) const;
		/**
		* Find the first occurance of a key in the hashtable
		* @tparam Key		Key type, needs to match the type used for the comparison predicate
		* @param[in] key	Key to find
		* @return			Iterator to found value, if find failed, an iterator to the back gets returned
		*/
		template<typename Key>
		Iterator FindKey(const Key& key) const;
		/**
		* Find the first occurance where the predicate returns true
		* @tparam U			Value type
		* @tparam Pred		Predicate type
		* @param[in] val	Value to compare width
		* @param[in] pred	Compare less predicate (1st param: value, 2nd param: value of type U)
		* @return			Iterator to found value, if find failed, an iterator to the back gets returned
		*/
		template<typename U, typename Pred>
		Iterator FindAs(const U& val, const Pred& pred) const;

		/**
		* Get the amount of entries that have a certain value
		* @param[in] val	Value to count
		* @return			Amount of entries of 'value'
		*/
		sizeT Count(const T& val) const;
		/**
		* Get the amount of entries that have a certain value
		* @tparam Key		Key type
		* @param[in] key	Key to count
		* @return			Amount of entries with a key of 'key'
		*/
		template<typename Key>
		sizeT CountKey(const Key& key) const;

		/**
		 * Rehash the hashtable to make data fit in a certain amount of buckets
		 * @note Caching the hash can provide a performance benefit
		 * @param[in] buckets	Amount of buckets to rehash to
		 */
		void Rehash(sizeT buckets);
		/**
		 * Reserve space in the hashtable
		 * @param[in] size	Space to reserve
		 */
		void Reserve(sizeT size);
			
		/**
		 * Swap the content of 2 hashtable
		 * @param[in] ht	Hashtable to swap content with
		 */
		void Swap(HashTable& ht);

		/**
		 * Get the amount of buckets in the hashtable
		 * @return	Amount of buckets in the hashtable
		 */
		sizeT BucketCount() const;
		/**
		 * Get the size of a bucket
		 * @param[in] bucket	Index of the bucket
		 * @return				Size of the bucket
		 */
		sizeT BucketSize(sizeT bucket) const;
		/**
		 * Get the index of the bucket that contains a value
		 * @param[in] val	Value
		 * @return			Index of the bucket that contains the value
		 */
		sizeT Bucket(const T& val) const;
		/**
		 * Get the index of the bucket that contains the key
		 * @return	Index of the bucket that contains the key
		 */
		template<typename Key>
		sizeT BucketKey(const Key& key) const;

		/**
		 * Get the current load factor of the hashtable
		 * @return	Current load factor of the hashtable
		 */
		sizeT LoadFactor() const;
		/**
		 * Set the maximum load factor of the hashtable
		 * @param[in] maxLoadFactor	Maximum load factor
		 */
		void MaxLoadFactor(sizeT maxLoadFactor);
		/**
		 * Get the maximum load factor of the hashtable
		 * @return	Maximum load factor of the hashtable
		 */
		sizeT MaxLoadFactor() const;

		/**
		 * Check if the hashtable is empty
		 * @return	If the hashtable is empty
		 */
		b8 IsEmpty() const;
		/**
		 * Get the size of the hashtable
		 * @return	Size of the hashtable
		 */
		sizeT Size() const;
		/**
		 * Get a pointer to the allocator
		 * @return	Pointer to the allocator
		 */
		Memory::IAllocator* GetAllocator() const;

		/**
		 * Return an iterator to the front/begin of the hashtable
		 * @return	Iterator to the front/begin of the hashtable
		 */
		Iterator Front() const;
		/**
		 * Return an iterator to the last element of the hashtable
		 * @return	Iterator to the last element of the hashtable
		 */
		Iterator Last() const;
		/**
		 * Return an iterator to the back/end of the hashtable
		 * @return	Iterator to the back/end of the hashtable
		 */
		Iterator Back() const;

		/**
		 * Return an iterator to the begin of the hashtable
		 * @note	Function meant to be used for range-based for-loops, use Front instead
		 * @return	Iterator to the begin of the hashtable
		 */
		Iterator begin() const;
		/**
		 * Return an iterator to the end of the hashtable
		 * @note	Function meant to be used for range-based for-loops, use Back instead
		 * @return	Iterator to the end of the hashtable
		 */
		Iterator end() const;

	private:
		friend class Iterator;

		/**
		* Copy a hash table to this hash table
		* @param[in] ht	Hash table to copy
		*/
		void Copy(const HashTable& ht);

		Node** m_ppBuckets;		/**< Buckets */
		sizeT m_BucketCount;	/**< Buckets count */
		sizeT m_Size;			/**< Size */
		f32 m_MaxLoadFactor;	/**< Max load factor */
		KeyCompare m_Comp;		/**< Comparison predicate */
		Hash m_Hash;			/**< Hash predicate */
		ExtractKey m_Extract;	/**< Key extraction predicate */
		Memory::IAllocator* m_pAlloc;	/**< Alloctor */

		
		static constexpr f32 m_DefaultMaxLoadFactor = 1.f;
	};

}

#include "Inline/HashTable.inl"

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename T, typename C, typename H, typename E, b8 CH, b8 U, b8 M, Hv::Container::GrowthPolicyFunc G), HV_TTYPE(Hv::HashTable, T, C, H, E, CH, U, M, G))