// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// SList.h: Singly linked list
#pragma once
#include "Core/CoreHeaders.h"
#include "ContainerUtils.h"
#include "Memory/Memory.h"

namespace Hv {
	
	/**
	 * Singly linked list
	 * @note	The containers uses multiple ways to copy data internally, using the copy-assignment operator as default,
	*			but MemCpy and the copy-constructor can also be used. These values can be set using their relative declares:
	*			HV_DECLARE_CONTAINER_MEMCPY and HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR and their respective template variants.
	 * @tparam T			Element type
	 */
	template<typename T>
	class SList
	{
	private:
		struct Node
		{
			T value;
			Node * pNext;

			/**
			 * Create an empty node
			 */
			Node();
			/**
			 * Create a node
			 * @param[in] val	Value
			 * @param[in] pNext	Next node
			 */
			Node(const T& val, Node * pNext = nullptr);
			/**
			 * Create a node, by moving the value
			 * @param[in] val	Value
			 * @param[in] pNext	Next node
			 */
			Node(T&& val, Node * pNext = nullptr);
			~Node();
		};
	public:
		class Iterator
		{
		public:
			/**
			 * Create an empty iterator
			 */
			Iterator();
			/**
			 * Create an iterator from another iterator
			 * @param[in] it	Other iterator
			 */
			Iterator(const Iterator& it);
			/**
			 * Move an iterator into this iterator
			 * @param[in] it	Other iterator
			 */
			Iterator(Iterator&& it) noexcept;

			T& operator*();
			const T& operator*() const;
			T * operator->();
			const T * operator->() const;

			Iterator& operator++();
			Iterator operator++(int);
			Iterator operator+(sizeT count);

			Iterator& operator=(const Iterator& it);
			Iterator& operator=(Iterator&& it) noexcept;
			Iterator& operator+=(sizeT count);

			b8 operator==(const Iterator& it) const;
			b8 operator!=(const Iterator& it) const;

		private:
			/**
			 *  Create an iterator from a node
			 * @param[in] pNode	Node
			 */
			Iterator(Node * pNode);

			Node * m_pNode;
		};

	public:
		/**
		 * Create an empty SList
		 */
		SList();
		/**
		 * Create an empty SList
		 * @param[in] pAlloc	Memory::IAllocator
		 */
		SList(Memory::IAllocator * pAlloc);
		/**
		 * Create an SList of a certain size, filled with a value
		 * @param[in] count		Amount of values
		 * @param[in] value		Value
		 * @param[in] pAlloc	Allocator
		 */
		SList(sizeT count, const T& value, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create an SList from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] pAlloc	Allocator
		 */
		template<sizeT N>
		SList(T(&arr)[N], Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a SList from an array
		 * @param[in] arr		Array
		 * @param[in] count		Array size
		 * @param[in] pAlloc	Allocator
		 */
		SList(const T * arr, sizeT count, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a SList from iterators
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @param[in] pAlloc	Allocator
		 */
		SList(const T * itFirst, const T * itLast, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a SList from iterators
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 * @param[in] pAlloc		Allocator
		 */
		template<typename InputIterator>
		SList(const InputIterator& itFirst, const InputIterator& itLast, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a SList from an initializer list
		 * @param[in] il		 Initializer list
		 * @param[in] pAlloc	 Allocator
		 */
		SList(std::initializer_list<T> il, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a SList from another SList
		 * @param[in] list	Other SList
		 */
		SList(const SList& list);
		/**
		 * Create a SList from another SList
		 * @param[in] list		Other SList
		 * @param[in] pAlloc	Allocator
		 */
		SList(const SList& list, Memory::IAllocator * pAlloc);
		/**
		 * Move a SList into this SList
		 * @param[in] list	Other SList
		 */
		SList(SList&& list) noexcept;
		~SList();

		template<sizeT N>
		SList& operator=(T(&arr)[N]);
		SList& operator=(std::initializer_list<T> il);
		SList& operator=(const SList& list);
		SList& operator=(SList&& list) noexcept;

		T& operator[](sizeT index);
		const T& operator[](sizeT index) const;

		/**
		 * Fill the SList with a value for 'count' indices
		 * @param[in] val	Value
		 * @param[in] count	Amount of values
		 */
		void Fill(const T& val, sizeT count);
		/**
		 * Assign an array to the SList
		 * @tparam N		Array size
		 * @param[in] arr	Array
		 */
		template<sizeT N>
		void Assign(T(&arr)[N]);
		/**
		 * Assign an array to the SList
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 */
		void Assign(T * arr, sizeT count);
		/**
		 * Assign a range of values to the SList
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		void Assign(const T * itFirst, const T * itLast);
		/**
		 * Assign a range of values to the SList
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to first element
		 * @param[in] itLast		Iterator to the element after the last element
		 */
		template<typename InputIterator>
		void Assign(const InputIterator& itFirst, const InputIterator& itLast);
		/**
		 * Asign an initializer list to the SList
		 * @param[in] il	Initializer list
		 */
		void Assign(std::initializer_list<T> il);

		/**
		 * Resize the SList
		 * @param[in] size	New size
		 * @note			Lists cannot be made larger with resize
		 */
		void Resize(sizeT size);
		/**
		 * Resize the SList
		 * @param[in] size	New size
		 * @param[in] val	Value to add if size is bigger than the current size
		 */
		void Resize(sizeT size, const T& val);
		/**
		 * Clear the vectors contents
		 */
		void Clear();

		/**
		 * Add a value to the back of the SList
		 * @param[in] val	Value to add
		 */
		void PushBack(const T& val);
		/**
		 * Move a value to the back of the SList
		 * @param[in] val	Value to add
		 */
		void PushBack(T&& val);
		/**
		 * Remove the last value from the SList
		 */
		void PopBack();
		/**
		 * Add a value to the front of the SList
		 * @param[in] val	Value to add
		 */
		void PushFront(const T& val);
		/**
		 * Move a value to the front of the SList
		 * @param[in] val	Value to add
		 */
		void PushFront(T&& val);
		/**
		 * Remove the first value from the SList
		 */
		void PopFront();

		/**
		 * Insert a value into the SList after an iterator
		 * @param[in] it	Iterator to location in SList
		 * @param[in] val	Value to insert
		 */
		void InsertAfter(const Iterator& it, const T& val);
		/**
		 * Insert a value into the SList by moving it after an iterator
		 * @param[in] it	Iterator to location in SList
		 * @param[in] val	Value to insert
		 */
		void InsertAfter(const Iterator& it, T&& val);
		/**
		 * Insert an amount of values into the SList after an iterator
		 * @param[in] it	Iterator to location in SList
		 * @param[in] count	Amount of values to insert
		 * @param[in] val	Value to insert
		 */
		void InsertAfter(const Iterator& it, sizeT count, const T& val);
		/**
		 * Insert an array into the SList after an iterator
		 * @tparam N			Array size
		 * @param[in] it	Iterator to location in SList
		 * @param[in] arr	Array
		 */
		template<sizeT N>
		void InsertAfter(const Iterator& it, T(&arr)[N]);
		/**
		 * Insert an array into the SList after an iterator
		 * @param[in] it	Iterator to location in SList
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 */
		void InsertAfter(const Iterator& it, T * arr, sizeT count);
		/**
		 * Insert a range of values into the SList after an iterator
		 * @param[in] it		Iterator to location in SList
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		void InsertAfter(const Iterator& it, const T * itFirst, const T * itLast);
		/**
		 * Insert a range of values into the SList after an iterator
		 * @tparam InputIterator		Forward InputIterator
		 * @param[in] it		Iterator to location in SList
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		template<typename InputIterator>
		void InsertAfter(const Iterator& it, const InputIterator& itFirst, const InputIterator& itLast);
		/**
		 * Insert an initializer list into the SList after an iterator
		 * @param[in] it	Iterator to location in SList
		 * @param[in] il	Initializer list
		 */
		void InsertAfter(const Iterator& it, std::initializer_list<T> il);

		/**
		 * EraseAfter a value
		 * @param[in] it	Iterator to location in SList
		 */
		void EraseAfter(const Iterator& it);
		/**
		 * EraseAfter an amount of values
		 * @param[in] it	Iterator to location in SList
		 * @param[in] count	Amount of values to erase
		 */
		void EraseAfter(const Iterator& it, sizeT count);
		/**
		 * EraseAfter a range of values
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	InputIterator to element after last element
		 */
		void EraseAfter(const Iterator& itFirst, const Iterator& itLast);

		/**
		 * Create and insert a value
		 * @tparam Args		Constructor argument types
		 * @param[in] it	Iterator to location in SList
		 * @param[in] args	Constructor arguments
		 */
		template<typename... Args>
		void EmplaceAfter(const Iterator& it, Args&&... args);
		/**
		 * Create and add a value to the back of the SList
		 * @tparam Args		Constructor argument types
		 * @param[in] args	Constructor arguments
		 */
		template<typename... Args>
		void EmplaceBack(Args&&... args);
		/**
		 * Create and add a value to the back of the SList
		 * @tparam Args		Constructor argument types
		 * @param[in] args	Constructor arguments
		 */
		template<typename... Args>
		void EmplaceFront(Args&&... args);

		/**
		 * Swap the content of 2 DynArrays
		 * @param[in] list	SList to swap content with
		 */
		void Swap(SList& list);

		/**
		 * Get the node at a certain index
		 * @param[in] index	Index of node
		 * @return			Node at index
		 */
		Node * GetNodeAtIndex(sizeT index);

		/**
		 * Check if the SList is empty
		 * @return	If the SList is empty
		 */
		b8 IsEmpty() const;
		/**
		 * Get the size of the SList
		 * @return	Size of the SList
		 */
		sizeT Size() const;
		/**
		 * Get a pointer to the allocator
		 * @return	Pointer to the allocator
		 */
		Memory::IAllocator * GetAllocator();

		/**
		 * Return an iterator before the front/begin of the array
		 * @note	Do not dereference this iterator
		 * @return	InputIterator to the front/begin of the array
		 */
		Iterator BeforeFront() const;
		/**
		 * Return an iterator to the front/begin of the array
		 * @return	InputIterator to the front/begin of the array
		 */
		Iterator Front() const;
		/**
		 * Return an iterator to the last element of the array
		 * @return	InputIterator to the last element of the array
		 */
		Iterator Last() const;
		/**
		 * Return an iterator to the back/end of the array
		 * @return	InputIterator to the back/end of the array
		 */
		Iterator Back() const;

		/**
		 * Return an iterator to the begin of the array
		 * @note	Function meant to be used for range-based for-loops, use Front instead
		 * @return	InputIterator to the begin of the array
		 */
		Iterator begin() const;
		/**
		 * Return an iterator to the end of the array
		 * @note	Function meant to be used for range-based for-loops, use Back instead
		 * @return	InputIterator to the end of the array
		 */
		Iterator end() const;

	private:

		/**
		* Copy a list to this list
		* @param[in] list	List to copy
		*/
		void Copy(const SList& list);

		Node* m_pHead;					/**< Singly linked list head  */
		Node* m_pTail;					/**< Singly linked list tail  */
		sizeT m_Size;					/**< Size  */
		Memory::IAllocator* m_pAlloc;	/**< Pointer to allocator */
	};

}

#include "SList.h"

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename T), HV_TTYPE(Hv::SList, T))
