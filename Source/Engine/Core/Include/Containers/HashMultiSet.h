// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HashMultiSet.h: Hash/unordered multi-set
#pragma once
#include "HashTable.h"

namespace Hv {

	/**
	 * Hashmultiset
	 * @note	The containers uses multiple ways to copy data internally, using the copy-assignment operator as default,
	 *			but MemCpy and the copy-constructor can also be used. These values can be set using their relative declares:
	 *			HV_DECLARE_CONTAINER_MEMCPY and HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR and their respective template variants.
	 * @tparam T					Element type
	 * @tparam CompEqual			Comparison predicate
	 * @tparam Hash					Hash Predicate
	 * @tparam CacheHash			If the hash of the value needs to be cached internally (can speed up rehash)
	 * @tparam BucketGrowthPolicy	Growth policy for buckets
	 */
	template<typename T, typename CompEqual = Container::CompareEqual<T>, typename Hash = HashFNV1a<T>, b8 CacheHash = true, Container::GrowthPolicyFunc BucketGrowthPolicy = Container::GrowthPrime>
	class HashMultiSet
	{
	private:
		using BaseType = HashTable<T, CompEqual, Hash, Container::UseSelf<T>, CacheHash, false, false, BucketGrowthPolicy>;

	public:
		using Iterator = typename BaseType::Iterator;

	public:

		/**
		 * Create a multiset
		 */
		HashMultiSet();
		/**
		 * Create a multiset
		 * @param[in] pAlloc	Allocator
		 */
		HashMultiSet(Memory::IAllocator* pAlloc);
		/**
		 * Create a multiset
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		HashMultiSet(const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc);
		/**
		 * Create a multiset from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] pAlloc	Allocator
		 */
		template<sizeT N>
		HashMultiSet(T(&arr)[N], Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		template<sizeT N>
		HashMultiSet(T(&arr)[N], const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] pAlloc	Allocator
		 */
		HashMultiSet(const T* arr, sizeT count, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from an array
		 * @tparam N			Array size
		 * @param[in] arr	Array
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		HashMultiSet(const T* arr, sizeT count, const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from iterators
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @param[in] pAlloc	Allocator
		 */
		HashMultiSet(const T* itFirst, const T* itLast, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from iterators
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		HashMultiSet(const T* itFirst, const T* itLast, const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from iterators
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 * @param[in] pAlloc		Allocator
		 */
		template<typename InputIterator>
		HashMultiSet(const InputIterator& itFirst, const InputIterator& itLast, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from iterators
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 * @param[in] comp			Comparison predicate
		 * @param[in] pAlloc		Allocator
		 */
		template<typename InputIterator>
		HashMultiSet(const InputIterator& itFirst, const InputIterator& itLast, const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from an initializer list
		 * @param[in] il		Initializer list
		 * @param[in] pAlloc	Allocator
		 */
		HashMultiSet(std::initializer_list<T> il, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from an initializer list
		 * @param[in] il		Initializer list
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		HashMultiSet(std::initializer_list<T> il, const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from another multiset
		 * @param[in] multiset	HashMultiSet
		 */
		HashMultiSet(const HashMultiSet& multiset);
		/**
		 * Create a multiset from another multiset
		 * @param[in] multiset	HashMultiSet
		 * @param[in] pAlloc	Allocator
		 */
		HashMultiSet(const HashMultiSet& multiset, Memory::IAllocator* pAlloc);
		/**
		 * Move a multiset into this multiset
		 * @param[in] multiset	HashMultiSet
		 */
		HashMultiSet(HashMultiSet&& multiset) noexcept;
		~HashMultiSet();

		template<sizeT N>
		HashMultiSet& operator=(T(&arr)[N]);
		HashMultiSet& operator=(std::initializer_list<T> il);
		HashMultiSet& operator=(const HashMultiSet& multiset);
		HashMultiSet& operator=(HashMultiSet&& multiset) noexcept;

		/**
		 * Insert a value into the multiset
		 * @param[in] val	Value
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const T& val);
		/**
		 * Move a value into the multiset
		 * @param[in] val	Value
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(T&& val);
		/**
		 * Insert an array into the multiset
		 * @tparam N		Array size
		 * @param[in] arr	Array
		 */
		template<sizeT N>
		void Insert(T(&arr)[N]);
		/**
		 * Insert an array into the multiset
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 */
		void Insert(const T* arr, sizeT count);
		/**
		 * Insert a range of value into the multiset
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		void Insert(const T* itFirst, const T* itLast);
		/**
		 * Insert a range of value into the multiset
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 */
		template<typename InputIterator>
		void Insert(const InputIterator& itFirst, const InputIterator& itLast);
		/**
		 * Insert an initializer list into the multiset
		 * @param[in] il	Initializer list
		 */
		template<typename InputIterator>
		void Insert(std::initializer_list<T> il);

		/**
		 * Create and insert a value into the multiset
		 * @tparam Args		Constructor argument types
		 * @param[in] args	Constructor arguments
		 * @return			Pair with info about the emplacement
		 *					-first is the iterator to the emplaced value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		template<typename... Args>
		Pair<Iterator, b8> Emplace(Args&&... args);

		/**
		 * Erase a value from the multiset
		 * @param[in] val	Value to erase
		 * @return			Amount of values removed, either 1 or 0
		 */
		sizeT Erase(const T& val);
		/**
		 * Erase an iterator from the multiset
		 * @param[in] it	Iterator to erase
		 * @return			Iterator after the removed element
		 */
		Iterator Erase(const Iterator& it);
		/**
		 * Erase a range of values
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @return				Iterator after the last removed element
		 */
		Iterator Erase(const Iterator& itFirst, const Iterator& itLast);

		/**
		 * Clear the multiset
		 */
		void Clear();

		/**
		 * Find a value in the multiset
		 * @param[in] val	Value to find
		 */
		Iterator Find(const T& val) const;
		/**
		 * Find the first occurance where the predicate returns true
		 * @tparam U		Value type
		 * @tparam Pred		Predicate type
		 * @param[in] val	Value to compare width
		 * @param[in] pred	Compare less predicate (1st param: value, 2nd param: value of type U)
		 */
		template<typename U, typename Pred>
		Iterator FindAs(const U& val, const Pred& pred) const;

		/**
		 * Get the amount of entries that have a certain value
		 * @param[in] val	Value to count
		 * @return			Amount of entries of 'value'
		 */
		sizeT Count(const T& val) const;

		/**
		 * Rehash the hashset to make data fit in a certain amount of buckets
		 * @note Caching the hash can provide a performance benefit
		 * @param[in] buckets	Amount of buckets to rehash to
		 */
		void Rehash(sizeT buckets);
		/**
		 * Reserve space in the hashset
		 * @param[in] size	Space to reserve
		 */
		void Reserve(sizeT size);

		/**
		 * Swap the content of 2 sets
		 * @param[in] multiset	HashMultiSet to swap content with
		 */
		void Swap(HashMultiSet& multiset);

		/**
		 * Get the amount of buckets in the hashset
		 * @return	Amount of buckets in the hashset
		 */
		sizeT BucketCount() const;
		/**
		 * Get the size of a bucket
		 * @param[in] bucket	Index of the bucket
		 * @return				Size of the bucket
		 */
		sizeT BucketSize(sizeT bucket) const;
		/**
		 * Get the index of the bucket that contains a value
		 * @param[in] val	Value
		 * @return			Index of the bucket that contains the value
		 */
		sizeT Bucket(const T& val) const;

		/**
		 * Get the current load factor of the hashset
		 * @return	Current load factor of the hashset
		 */
		sizeT LoadFactor() const;
		/**
		 * Set the maximum load factor of the hashset
		 * @param[in] maxLoadFactor	Maximum load factor
		 */
		void MaxLoadFactor(sizeT maxLoadFactor);
		/**
		 * Get the maximum load factor of the hashset
		 * @return	Maximum load factor of the hashset
		 */
		sizeT MaxLoadFactor() const;

		/**
		 * Check if the multiset is empty
		 * @return	If the multiset is empty
		 */
		b8 IsEmpty() const;
		/**
		 * Get the size of the multiset
		 * @return	Size of the multiset
		*/
		sizeT Size() const;
		/**
		 * Get a pointer to the allocator
		 * @return	Pointer to the allocator
		 */
		Memory::IAllocator* GetAllocator()  const;

		/**
		 * Return an iterator to the front/begin of the multiset
		 * @return	Iterator to the front/begin of the multiset
		 */
		Iterator Front() const;
		/**
		 * Return an iterator to the last element of the multiset
		 * @return	Iterator to the last element of the multiset
		 */
		Iterator Last() const;
		/**
		 * Return an iterator to the back/end of the multiset
		 * @return	Iterator to the back/end of the multiset
		 */
		Iterator Back() const;

		/**
		 * Return an iterator to the begin of the multiset
		 * @note	Function meant to be used for range-based for-loops, use Front instead
		 * @return	Iterator to the begin of the multiset
		 */
		Iterator begin() const;
		/**
		 * Return an iterator to the end of the multiset
		 * @note	Function meant to be used for range-based for-loops, use Back instead
		 * @return	Iterator to the end of the multiset
		 */
		Iterator end() const;

	private:
		BaseType m_HashTable;
	};

}

#include "Inline/HashMultiSet.inl"

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename T, typename C, typename H, b8 CH, Hv::Container::GrowthPolicyFunc G), HV_TTYPE(Hv::HashMultiSet, T, C, H, CH, G))
