// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// MultiSet.h: Ordered multi-set
#pragma once
#include "RedBlackTree.h"

namespace Hv {
	
	/**
	 * MultiSet (sorted)
	 * @note	The containers uses multiple ways to copy data internally, using the copy-assignment operator as default,
	 *			but MemCpy and the copy-constructor can also be used. These values can be multiset using their relative declares:
	 *			HV_DECLARE_CONTAINER_MEMCPY and HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR and their respective template variants.
	 * @note	A multiset can contain only 1 instance of a key
	 * @tparam T				Element type
	 * @tparam CompLess			Comparison predicate
	 */
	template<typename T, typename CompLess = Container::CompareLess<T>>
	class MultiSet
	{
	private:
		using BaseType = RedBlackTree<T, CompLess, Container::UseSelf<T>, false, false>;

	public:
		using Iterator = typename BaseType::Iterator;

	public:
		/**
		 * Create a multiset
		 */
		MultiSet();
		/**
		 * Create a multiset
		 * @param[in] pAlloc	Allocator
		 */
		MultiSet(Memory::IAllocator* pAlloc);
		/**
		 * Create a multiset
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		MultiSet(const CompLess& comp, Memory::IAllocator* pAlloc);
		/**
		 * Create a multiset from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] pAlloc	Allocator
		 */
		template<sizeT N>
		MultiSet(T(&arr)[N], Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		template<sizeT N>
		MultiSet(T(&arr)[N], const CompLess& comp, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] pAlloc	Allocator
		 */
		MultiSet(const T* arr, sizeT count, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		MultiSet(const T* arr, sizeT count, const CompLess& comp, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from iterators
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @param[in] pAlloc	Allocator
		 */
		MultiSet(const T* itFirst, const T* itLast, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from iterators
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		MultiSet(const T* itFirst, const T* itLast, const CompLess& comp, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from iterators
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 * @param[in] pAlloc		Allocator
		 */
		template<typename InputIterator>
		MultiSet(const InputIterator& itFirst, const InputIterator& itLast, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from iterators
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 * @param[in] comp			Comparison predicate
		 * @param[in] pAlloc		Allocator
		 */
		template<typename InputIterator>
		MultiSet(const InputIterator& itFirst, const InputIterator& itLast, const CompLess& comp, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from an initializer list
		 * @param[in] il		Initializer list
		 * @param[in] pAlloc	Allocator
		 */
		MultiSet(std::initializer_list<T> il, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from an initializer list
		 * @param[in] il		Initializer list
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		MultiSet(std::initializer_list<T> il, const CompLess& comp, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multiset from another multiset
		 * @param[in] multiset	Multiset
		 */
		MultiSet(const MultiSet& multiset);
		/**
		 * Create a multiset from another multiset
		 * @param[in] multiset	Multiset
		 * @param[in] pAlloc	Allocator
		 */
		MultiSet(const MultiSet& multiset, Memory::IAllocator* pAlloc);
		/**
		 * Move a multiset into this multiset
		 * @param[in] multiset	Multiset
		 */
		MultiSet(MultiSet&& multiset) noexcept;
		~MultiSet();

		template<sizeT N>
		MultiSet& operator=(T(&arr)[N]);
		MultiSet& operator=(std::initializer_list<T> il);
		MultiSet& operator=(const MultiSet& multiset);
		MultiSet& operator=(MultiSet&& multiset) noexcept;

		/**
		 * Insert a value into the multiset
		 * @param[in] val	Value
		 * @return			Iterator to the inserted value
		 */
		Iterator Insert(const T& val);
		/**
		 * Move a value into the multiset
		 * @param[in] val	Value
		 * @return			Iterator to the inserted value
		 */
		Iterator Insert(T&& val);
		/**
		 * Insert a value into the multiset, with a hint
		 * @param[in] val	Value
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Iterator to the inserted value
		 */
		Iterator Insert(const Iterator& hint, const T& val);
		/**
		 * Move a value into the multiset, with a hint
		 * @param[in] val	Value
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Iterator to the inserted value
		 */
		Iterator Insert(const Iterator& hint, T&& val);
		/**
		 * Insert an array into the multiset
		 * @tparam N		Array size
		 * @param[in] arr	Array
		 */
		template<sizeT N>
		void Insert(T(&arr)[N]);
		/**
		 * Insert an array into the multiset
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 */
		void Insert(const T* arr, sizeT count);
		/**
		 * Insert a range of value into the multiset
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		void Insert(const T* itFirst, const T* itLast);
		/**
		 * Insert a range of value into the multiset
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 */
		template<typename InputIterator>
		void Insert(const InputIterator& itFirst, const InputIterator& itLast);
		/**
		 * Insert an initializer list into the multiset
		 * @param[in] il	Initializer list
		 */
		template<typename InputIterator>
		void Insert(std::initializer_list<T> il);

		/**
		 * Create and insert a value into the multiset
		 * @tparam Args		Constructor argument types
		 * @param[in] args	Constructor arguments
		 * @return			Iterator to the emplaced value
		 */
		template<typename... Args>
		Iterator Emplace(Args&&... args);
		/**
		 * Create and insert a value into the multiset, with a hint
		 * @tparam Args		Constructor argument types
		 * @param[in] args	Constructor arguments
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Iterator to the emplaced value
		 */
		template<typename... Args>
		Iterator EmplaceHint(const Iterator& hint, Args&&... args);

		/**
		 * Erase a value from the multiset
		 * @param[in] val	Value to erase
		 * @return			Amount of erased values
		 */
		sizeT Erase(const T& val);
		/**
		 * Erase an iterator from the multiset
		 * @param[in] it	Iterator to erase
		 * @return			Iterator after the removed element
		 * @return			Amount of values removed
		 */
		Iterator Erase(const Iterator& it);
		/**
		 * Erase a range of values
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @return				Iterator after the last removed element
		 */
		Iterator Erase(const Iterator& itFirst, const Iterator& itLast);

		/**
		 * Clear the multiset
		 */
		void Clear();

		/**
		 * Find a value in the multiset
		 * @param[in] val	Value to find
		 */
		Iterator Find(const T& val) const;
		/**
		 * Find the first occurance where the predicate returns true
		 * @tparam U			Value type
		 * @tparam Pred		Predicate type
		 * @param[in] val	Value to compare width
		 * @param[in] pred	Compare less predicate (1st param: value, 2nd param: value of type U)
		 */
		template<typename U, typename Pred>
		Iterator FindAs(const U& val, const Pred& pred) const;

		/**
		 * Get the amount of entries that have a certain value
		 * @param[in] val	Value to count
		 * @return			Amount of entries of 'value'
		 */
		sizeT Count(const T& val) const;

		/**
		 * Find the first occurance which isn't smaller than a value
		 * @param[in] val	Value
		 */
		Iterator LowerBound(const T& val)  const;
		/**
		 * Find the last occurance which isn't larger than a value
		 * @param[in] val	Value
		 */
		Iterator UpperBound(const T& val)  const;
		/**
		 * Find the first occurance which isn't smaller and last occurance which isn't larger than a value
		 * @param[in] val	Value
		 */
		Pair<Iterator, Iterator> EqualRange(const T& val)  const;

		/**
		 * Swap the content of 2 multisets
		 * @param[in] multiset	MultiSet to swap content with
		 */
		void Swap(MultiSet& multiset);

		/**
		 * Check if the multiset is empty
		 * @return	If the multiset is empty
		 */
		b8 IsEmpty() const;
		/**
		 * Get the size of the multiset
		 * @return	Size of the multiset
		 */
		sizeT Size() const;
		/**
		 * Get a pointer to the allocator
		 * @return	Pointer to the allocator
		 */
		Memory::IAllocator* GetAllocator()  const;

		/**
		 * Return an iterator to the front/begin of the multiset
		 * @return	Iterator to the front/begin of the multiset
		 */
		Iterator Front() const;
		/**
		 * Return an iterator to the last element of the multiset
		 * @return	Iterator to the last element of the multiset
		 */
		Iterator Last() const;
		/**
		 * Return an iterator to the back/end of the multiset
		 * @return	Iterator to the back/end of the multiset
		 */
		Iterator Back() const;

		/**
		 * Return an iterator to the begin of the multiset
		 * @note	Function meant to be used for range-based for-loops, use Front instead
		 * @return	Iterator to the begin of the multiset
		 */
		Iterator begin() const;
		/**
		 * Return an iterator to the end of the multiset
		 * @note	Function meant to be used for range-based for-loops, use Back instead
		 * @return	Iterator to the end of the multiset
		 */
		Iterator end() const;

	private:

		BaseType m_Tree;	/**< Internal tree */

	};

}

#include "Inline/MultiSet.inl"

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename T, typename C), HV_TTYPE(Hv::MultiSet, T, C))