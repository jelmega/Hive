// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// RedBlackTree.h: Red-black tree
#pragma once
#include "Core/CoreHeaders.h"
#include "ContainerUtils.h"
#include "Memory/Memory.h"

namespace Hv {

	/**
	 * Red black tree (used for sorted sets and maps)
	 * @note	The containers uses multiple ways to copy data internally, using the copy-assignment operator as default,
	 *			but MemCpy and the copy-constructor can also be used. These values can be set using their relative declares:
	 *			HV_DECLARE_CONTAINER_MEMCPY and HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR and their respective template variants.
	 * @note	This class is not meant to be used by itself, this class is the internal tree for sets and maps
	 * @tparam T				Element type
	 * @tparam CompLess			Comparison predicate
	 * @tparam ExtractKey		Predicate used to extract the key from the element
	 * @tparam UniqueKey		If a key can only appear once inside the tree
	 * @tparam MutableIterator	If the iterator is mutable or not (normally false for sets, but true for maps
	 */
	template<typename T, typename CompLess, typename ExtractKey, b8 UniqueKey, b8 MutableIterator>
	class RedBlackTree
	{
	private:
		enum class Color : u8
		{
			Black,
			Red
		};

		struct Node
		{
			T value;
			Node* pParent;
			Node* pLeft;
			Node* pRight;
			Color color;

			/**
			 * Create an empty node
			 */
			Node();
			/**
			 * Create a node
			 * @param[in] val	Value
			 */
			Node(const T& val);
			/**
			 * Create a node, by moving the value
			 * @param[in] val	Value
			 */
			Node(T&& val);
			/**
			 * Create a node from another node
			 * @param[in] node	Node
			 */
			Node(const Node& node);
			/**
			 * Move a node into this node
			 * @param[in] node	Node
			 */
			Node(Node&& node) noexcept;
			~Node();
		};

	public:

		class Iterator
		{
		public:
			/**
			 * Create an empty iterator
			 */
			Iterator();
			/**
			 * Create an iterator from another iterator
			 * @param[in] it	Other iterator
			 */
			Iterator(const Iterator& it);
			/**
			 * Move an iterator into this iterator
			 * @param[in] it	Other iterator
			 */
			Iterator(Iterator&& it) noexcept;

			Iterator& operator++();
			Iterator operator++(int);
			Iterator& operator--();
			Iterator operator--(int);

			Iterator operator+(sizeT count);
			Iterator operator-(sizeT count);

			Iterator& operator=(const Iterator& it);
			Iterator& operator=(Iterator&& it) noexcept;
			Iterator& operator+=(sizeT count);
			Iterator& operator-=(sizeT count);

			typename std::conditional<MutableIterator, T&, const T&>::type operator*();
			const T& operator*() const;
			typename std::conditional<MutableIterator, T*, const T*>::type operator->();
			const T* operator->() const;

			b8 operator==(const Iterator& it) const;
			b8 operator!=(const Iterator& it) const;

		private:
			friend class RedBlackTree<T, CompLess, ExtractKey, UniqueKey, MutableIterator>;
			Iterator(Node* pNode);

			Node* m_pNode;
		};

	public:
		/**
		 * Create a red-black tree
		 * @param[in] pAlloc	Allocator
		 */
		RedBlackTree(Memory::IAllocator* pAlloc);
		/**
		 * Create a red-black tree
		 * @param[in] pAlloc	Allocator
		 * @param[in] comp		Comparison predicate
		 */
		RedBlackTree(const CompLess& comp, Memory::IAllocator* pAlloc);
		/**
		 * Create a red-black tree
		 * @param[in] pAlloc	Allocator
		 * @param[in] extract	Key extraction predicate
		 */
		RedBlackTree(const ExtractKey& extract, Memory::IAllocator* pAlloc);
		/**
		 * Create a red-black tree
		 * @param[in] pAlloc	Allocator
		 * @param[in] comp		Comparison predicate
		 * @param[in] extract	Key extraction predicate
		 */
		RedBlackTree(const CompLess& comp, const ExtractKey& extract, Memory::IAllocator* pAlloc);
		/**
		 * Create a red-black tree from another red-black tree
		 * @param[in] rbt		Red-black tree
		 */
		RedBlackTree(const RedBlackTree& rbt);
		/**
		 * Create a red-black tree from another red-black tree
		 * @param[in] rbt		Red-black tree
		 * @param[in] pAlloc	Allocator
		 */
		RedBlackTree(const RedBlackTree& rbt, Memory::IAllocator* pAlloc);
		/**
		 * Move a red-black tree into this red-black tree
		 * @param[in] rbt		Red-black tree
		 */
		RedBlackTree(RedBlackTree&& rbt) noexcept;
		~RedBlackTree();

		RedBlackTree& operator=(const RedBlackTree& rbt);
		RedBlackTree& operator=(RedBlackTree&& rbt) noexcept;

		/**
		 * Insert a value into the red-black tree
		 * @param[in] val	Value
		 */
		Pair<Iterator, b8> Insert(const T& val);
		/**
		 * Move a value into the red-black tree
		 * @param[in] val	Value
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(T&& val);
		/**
		 * Insert a value into the red-black tree, with a hint
		 * @param[in] val	Value
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const Iterator& hint, const T& val);
		/**
		 * Move a value into the red-black tree, with a hint
		 * @param[in] val	Value
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const Iterator& hint, T&& val);

		/**
		 * Create and insert a value into the red-black tree
		 * @tparam Args		Constructor argument types
		 * @param[in] args	Constructor arguments
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		template<typename... Args>
		Pair<Iterator, b8> Emplace(Args&&... args);
		/**
		 * Create and insert a value into the red-black tree, with a hint
		 * @tparam Args		Constructor argument types
		 * @param[in] args	Constructor arguments
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Pair with info about the emplacement
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		template<typename... Args>
		Pair<Iterator, b8> EmplaceHint(const Iterator& hint, Args&&... args);
		/**
		 * Erase the first occurance of a value from the red-black tree
		 * @param[in] val	Value to erase
		 * @return			Iterator after the removed element
		 */
		Iterator Erase(const T& val);
		/**
		 * Erase all values with the key from the red-black tree
		 * @tparam Key		Key type, needs to match the type used for the comparison predicate
		 * @param[in] key	Key to erase
		 * @return			Iterator after the removed element
		 */
		template<typename Key>
		Iterator EraseKey(const Key& key);
		/**
		 * Erase an iterator from the red-black tree
		 * @param[in] it	Iterator to erase
		 * @return			Iterator after the removed element
		 */
		Iterator Erase(const Iterator& it);

		/**
		 * Clear the red-back tree
		 */
		void Clear();

		/**
		 * Find a value in the red-black tree
		 * @param[in] val	Value to find
		 * @return			Iterator to found value, if find failed, an iterator to the back gets returned
		 */
		Iterator Find(const T& val) const;
		/**
		 * Find the first occurance of a key in the red-black tree
		 * @tparam Key		Key type, needs to match the type used for the comparison predicate
		 * @param[in] key	Key to find
		 * @return			Iterator to found value, if find failed, an iterator to the back gets returned
		 */
		template<typename Key>
		Iterator FindKey(const Key& key) const;
		/**
		 * Find the first occurance where the predicate returns true
		 * @tparam U			Value type
		 * @tparam Pred		Predicate type
		 * @param[in] val	Value to compare width
		 * @param[in] pred	Compare less predicate (1st param: value, 2nd param: value of type U)
		 * @return			Iterator to found value, if find failed, an iterator to the back gets returned
		 */
		template<typename U, typename Pred>
		Iterator FindAs(const U& val, const Pred& pred) const;

		/**
		 * Get the amount of entries that have a certain value
		 * @param[in] val	Value to count
		 * @return			Amount of entries of 'value'
		 */
		sizeT Count(const T& val) const;
		/**
		 * Get the amount of entries that have a certain value
		 * @tparam Key		Key type
		 * @param[in] key	Key to count
		 * @return			Amount of entries with a key of 'key'
		 */
		template<typename Key>
		sizeT CountKey(const Key& key) const;

		/**
		 * Find the first occurance which isn't smaller than a value
		 * @param[in] val	Value
		 * @return			Iterator to lowerbound, if failed, an iterator to the back gets returned
		 */
		Iterator LowerBound(const T& val) const;
		/**
		 * Find the first occurance of which the key isn't smaller than another key
		 * @tparam Key		Key type, needs to match the type used for the comparison predicate
		 * @param[in] key	Key
		 * @return			Iterator to lowerbound, if failed, an iterator to the back gets returned
		 */
		template<typename Key>
		Iterator LowerBoundKey(const Key& key) const;
		/**
		 * Find the last occurance which isn't larger than a value
		 * @param[in] val	Value
		 * @return			Iterator to lowerbound, if failed, an iterator to the back gets returned
		 */
		Iterator UpperBound(const T& val) const;
		/**
		 * Find the last occurance of which the key isn't larger than another key
		 * @tparam Key		Key type, needs to match the type used for the comparison predicate
		 * @param[in] key	Key
		 * @return			Iterator to lowerbound, if failed, an iterator to the back gets returned
		 */
		template<typename Key>
		Iterator UpperBoundKey(const Key& key) const;

		/**
		 * Swap the content of 2 Red-black trees
		 * @param[in] rbt	Red-black tree to swap content with
		 */
		void Swap(RedBlackTree& rbt);

		/**
		 * Check if the red-black tree is empty
		 * @return	If the red-black tree is empty
		 */
		b8 IsEmpty() const;
		/**
		 * Get the size of the red-black tree
		 * @return	Size of the red-black tree
		 */
		sizeT Size() const;
		/**
		 * Get a pointer to the allocator
		 * @return	Pointer to the allocator
		 */
		Memory::IAllocator* GetAllocator() const;

		/**
		 * Return an iterator to the front/begin of the red-black tree
		 * @return	Iterator to the front/begin of the red-black tree
		 */
		Iterator Front() const;
		/**
		 * Return an iterator to the last element of the red-black tree
		 * @return	Iterator to the last element of the red-black tree
		 */
		Iterator Last() const;
		/**
		 * Return an iterator to the back/end of the red-black tree
		 * @return	Iterator to the back/end of the red-black tree
		 */
		Iterator Back() const;

		/**
		 * Return an iterator to the begin of the red-black tree
		 * @note	Function meant to be used for range-based for-loops, use Front instead
		 * @return	Iterator to the begin of the red-black tree
		 */
		Iterator begin() const;
		/**
		 * Return an iterator to the end of the red-black tree
		 * @note	Function meant to be used for range-based for-loops, use Back instead
		 * @return	Iterator to the end of the red-black tree
		 */
		Iterator end() const;

	private:
		/**
		 * Copy a red-black tree to this red-black tree
		 * @param[in] rbt	Red-black tree to copy
		 */
		void Copy(const RedBlackTree& rbt);
		/**
		 * Rotate a node left
		 * @param[in] pNode	Node to rotate
		 */
		void RotateLeft(Node* pNode);
		/**
		 * Rotate a node right
		 * @param[in] pNode	Node to rotate
		 */
		void RotateRight(Node* pNode);
		/**
		 * Fix the graph after insertion
		 * @param[in] pNode	Node to fix
		 */
		void FixInsert(Node* pNode);
		/**
		 * Fix the graph after erase
		 * @param[in] pNode	Node to fix
		 */
		void FixErase(Node* pNode);

		Node* m_pRoot;					/**< Root node */
		sizeT m_Size;					/**< Tree size */
		CompLess m_Comp;				/**< Comparison predicate */
		ExtractKey m_Extract;			/**< Key extraction predicate */
		Memory::IAllocator* m_pAlloc;	/**< Allocator */

	};
	
}

#include "Inline/RedBlackTree.inl"

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename T, typename C, typename E, b8 U, b8 M), HV_TTYPE(Hv::RedBlackTree, T, C, E, U, M))