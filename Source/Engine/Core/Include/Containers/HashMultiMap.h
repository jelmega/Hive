// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HashMultiSet.h: Hash/unordered multi-map
#pragma once
#include "HashTable.h"

namespace Hv {

	/**
	 * Hashset
	 * @note	The containers uses multiple ways to copy data internally, using the copy-assignment operator as default,
	 *			but MemCpy and the copy-constructor can also be used. These values can be set using their relative declares:
	 *			HV_DECLARE_CONTAINER_MEMCPY and HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR and their respective template variants.
	 * @note	A set can contain only 1 instance of a key
	 * @tparam Key					Key type
	 * @tparam Value				Value type
	 * @tparam CompEqual			Comparison predicate
	 * @tparam Hash					Hash Predicate
	 * @tparam CacheHash			If the hash of the value needs to be cached internally (can speed up rehash)
	 * @tparam BucketGrowthPolicy	Growth policy for buckets
	 */
	template<typename Key, typename Value, typename CompEqual = Container::CompareEqual<Key>, typename Hash = HashFNV1a<Key>, b8 CacheHash = true, Container::GrowthPolicyFunc BucketGrowthPolicy = Container::GrowthPrime>
	class HashMultiMap
	{
	private:
		using BaseType = HashTable<Pair<Key, Value>, CompEqual, Hash, Container::UseFirst<Key, Value>, CacheHash, false, true, BucketGrowthPolicy>;
		using KeyValuePair = Pair<Key, Value>;
	public:
		using Iterator = typename BaseType::Iterator;

	public:

		/**
		 * Create a multimap
		 */
		HashMultiMap();
		/**
		 * Create a multimap
		 * @param[in] pAlloc	Allocator
		 */
		HashMultiMap(Memory::IAllocator* pAlloc);
		/**
		 * Create a multimap
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		HashMultiMap(const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc);
		/**
		 * Create a multimap from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] pAlloc	Allocator
		 */
		template<sizeT N>
		HashMultiMap(Pair<Key, Value>(&arr)[N], Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		template<sizeT N>
		HashMultiMap(Pair<Key, Value>(&arr)[N], const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] pAlloc	Allocator
		 */
		HashMultiMap(const Pair<Key, Value>* arr, sizeT count, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		HashMultiMap(const Pair<Key, Value>* arr, sizeT count, const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap from iterators
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @param[in] pAlloc	Allocator
		 */
		HashMultiMap(const Pair<Key, Value>* itFirst, const Pair<Key, Value>* itLast, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap from iterators
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		HashMultiMap(const Pair<Key, Value>* itFirst, const Pair<Key, Value>* itLast, const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap from iterators
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 * @param[in] pAlloc		Allocator
		 */
		template<typename InputIterator>
		HashMultiMap(const InputIterator& itFirst, const InputIterator& itLast, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap from iterators
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 * @param[in] comp			Comparison predicate
		 * @param[in] pAlloc		Allocator
		 */
		template<typename InputIterator>
		HashMultiMap(const InputIterator& itFirst, const InputIterator& itLast, const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap from an initializer list
		 * @param[in] il		Initializer list
		 * @param[in] pAlloc	Allocator
		 */
		HashMultiMap(std::initializer_list<Pair<Key, Value>> il, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap from an initializer list
		 * @param[in] il		Initializer list
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		HashMultiMap(std::initializer_list<Pair<Key, Value>> il, const CompEqual& comp, const Hash& hash, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a multimap from another multimap
		 * @param[in] multimap	HashMultiMap
		 */
		HashMultiMap(const HashMultiMap& multimap);
		/**
		 * Create a multimap from another multimap
		 * @param[in] multimap	HashMultiMap
		 * @param[in] pAlloc	Allocator
		 */
		HashMultiMap(const HashMultiMap& multimap, Memory::IAllocator* pAlloc);
		/**
		 * Move a multimap into this multimap
		 * @param[in] multimap	HashMultiMap
		 */
		HashMultiMap(HashMultiMap&& multimap) noexcept;
		~HashMultiMap();

		template<sizeT N>
		HashMultiMap& operator=(Pair<Key, Value>(&arr)[N]);
		HashMultiMap& operator=(std::initializer_list<Pair<Key, Value>> il);
		HashMultiMap& operator=(const HashMultiMap& multimap);
		HashMultiMap& operator=(HashMultiMap&& multimap) noexcept;

		/**
		 * Insert a value into the multimap
		 * @param[in] val	Value
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const Pair<Key, Value>& val);
		/**
		 * Insert a key and value into the multimap
		 * @param[in] key	Key
		 * @param[in] val	Value
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const Key& key, const Value& val);
		/**
		 * Move a value into the multimap
		 * @param[in] val	Value
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(Pair<Key, Value>&& val);
		/**
		 * Move a key and value into the multimap
		 * @param[in] key	Key
		 * @param[in] val	Value
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(Key&& key, Value&& val);
		/**
		 * Insert an array into the multimap
		 * @tparam N		Array size
		 * @param[in] arr	Array
		 */
		template<sizeT N>
		void Insert(Pair<Key, Value>(&arr)[N]);
		/**
		 * Insert an array into the multimap
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 */
		void Insert(const Pair<Key, Value>* arr, sizeT count);
		/**
		 * Insert a range of value into the multimap
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		void Insert(const Pair<Key, Value>* itFirst, const Pair<Key, Value>* itLast);
		/**
		 * Insert a range of value into the multimap
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 */
		template<typename InputIterator>
		void Insert(const InputIterator& itFirst, const InputIterator& itLast);
		/**
		 * Insert an initializer list into the multimap
		 * @param[in] il	Initializer list
		 */
		template<typename InputIterator>
		void Insert(std::initializer_list<Pair<Key, Value>> il);

		/**
		 * Create and insert a value into the multimap, with a provided key
		 * @tparam Args		Constructor argument types
		 * @param[in] key	Key
		 * @param[in] args	Constructor arguments
		 * @return			Pair with info about the emplacement
		 *					-first is the iterator to the emplaced value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		template<typename... Args>
		Pair<Iterator, b8> Emplace(const Key & key, Args&&... args);

		/**
		 * Erase a value from the multimap
		 * @param[in] key	Key to erase
		 * @return			Amount of values removed, either 1 or 0
		 */
		sizeT Erase(const Key& key);
		/**
		 * Erase an iterator from the multimap
		 * @param[in] it	Iterator to erase
		 * @return			Iterator after the removed element
		 */
		Iterator Erase(const Iterator& it);
		/**
		 * Erase a range of values
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @return				Iterator after the last removed element
		 */
		Iterator Erase(const Iterator& itFirst, const Iterator& itLast);

		/**
		 * Clear the multimap
		 */
		void Clear();

		/**
		 * Find a value in the multimap
		 * @param[in] key	Key to find
		 */
		Iterator Find(const Key& key) const;
		/**
		 * Find the first occurance where the predicate returns true
		 * @tparam U			Value type
		 * @tparam Pred		Predicate type
		 * @param[in] val	Value to compare width
		 * @param[in] pred	Compare less predicate (1st param: value, 2nd param: value of type U)
		 */
		template<typename U, typename Pred>
		Iterator FindAs(const U& val, const Pred& pred) const;

		/**
		 * Get the amount of entries that have a certain value
		 * @param[in] key	Key to count
		 * @return			Amount of entries of 'value'
		 */
		sizeT Count(const Key& key) const;

		/**
		 * Rehash the hashmultimap to make data fit in a certain amount of buckets
		 * @note Caching the hash can provide a performance benefit
		 * @param[in] buckets	Amount of buckets to rehash to
		 */
		void Rehash(sizeT buckets);
		/**
		 * Reserve space in the hashmultimap
		 * @param[in] size	Space to reserve
		 */
		void Reserve(sizeT size);

		/**
		 * Swap the content of 2 multimaps
		 * @param[in] multimap	HashMultiMap to swap content with
		 */
		void Swap(HashMultiMap& multimap);

		/**
		 * Get the amount of buckets in the hashmultimap
		 * @return	Amount of buckets in the hashmultimap
		 */
		sizeT BucketCount() const;
		/**
		 * Get the size of a bucket
		 * @param[in] bucket	Index of the bucket
		 * @return				Size of the bucket
		 */
		sizeT BucketSize(sizeT bucket) const;
		/**
		 * Get the index of the bucket that contains a key
		 * @param[in] key	Key
		 * @return			Index of the bucket that contains the key
		 */
		sizeT Bucket(const Key& key) const;

		/**
		 * Get the current load factor of the hashmultimap
		 * @return	Current load factor of the hashmultimap
		 */
		sizeT LoadFactor() const;
		/**
		 * Set the maximum load factor of the hashmultimap
		 * @param[in] maxLoadFactor	Maximum load factor
		 */
		void MaxLoadFactor(sizeT maxLoadFactor);
		/**
		 * Get the maximum load factor of the hashmultimap
		 * @return	Maximum load factor of the hashmultimap
		 */
		sizeT MaxLoadFactor() const;

		/**
		 * Check if the multimap is empty
		 * @return	If the multimap is empty
		 */
		b8 IsEmpty() const;
		/**
		 * Get the size of the multimap
		 * @return	Size of the multimap
		 */
		sizeT Size() const;
		/**
		 * Get a pointer to the allocator
		 * @return	Pointer to the allocator
		 */
		Memory::IAllocator* GetAllocator()  const;

		/**
		 * Return an iterator to the front/begin of the multimap
		 * @return	Iterator to the front/begin of the multimap
		 */
		Iterator Front() const;
		/**
		 * Return an iterator to the last element of the multimap
		 * @return	Iterator to the last element of the multimap
		 */
		Iterator Last() const;
		/**
		 * Return an iterator to the back/end of the multimap
		 * @return	Iterator to the back/end of the multimap
		 */
		Iterator Back() const;

		/**
		 * Return an iterator to the begin of the multimap
		 * @note	Function meant to be used for range-based for-loops, use Front instead
		 * @return	Iterator to the begin of the multimap
		 */
		Iterator begin() const;
		/**
		 * Return an iterator to the end of the multimap
		 * @note	Function meant to be used for range-based for-loops, use Back instead
		 * @return	Iterator to the end of the multimap
		 */
		Iterator end() const;

	private:
		BaseType m_HashTable;
	};

}

#include "Inline/HashMultiMap.inl"

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename K, typename V, typename C, typename H, b8 CH, Hv::Container::GrowthPolicyFunc G), HV_TTYPE(Hv::HashMultiMap, K, V, C, H, CH, G))