// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Set.h: Ordered set
#pragma once
#include "RedBlackTree.h"

namespace Hv {

	/**
	 * Set (sorted)
	 * @note	The containers uses multiple ways to copy data internally, using the copy-assignment operator as default,
	 *			but MemCpy and the copy-constructor can also be used. These values can be set using their relative declares:
	 *			HV_DECLARE_CONTAINER_MEMCPY and HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR and their respective template variants.
	 * @note	A set can contain only 1 instance of a key
	 * @tparam T			Element type
	 * @tparam CompLess		Comparison predicate
	 */
	template<typename T, typename CompLess = Container::CompareLess<T>>
	class Set
	{
	private:
		using BaseType = RedBlackTree<T, CompLess, Container::UseSelf<T>, true, false>;

	public:
		using Iterator = typename BaseType::Iterator;

	public:
		/**
		 * Create a set
		 */
		Set();
		/**
		 * Create a set
		 * @param[in] pAlloc	Allocator
		 */
		Set(Memory::IAllocator * pAlloc);
		/**
		 * Create a set
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		Set(const CompLess& comp, Memory::IAllocator * pAlloc);
		/**
		 * Create a set from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] pAlloc	Allocator
		 */
		template<sizeT N>
		Set(T(&arr)[N], Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a set from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		template<sizeT N>
		Set(T(&arr)[N], const CompLess& comp, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a set from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] pAlloc	Allocator
		 */
		Set(const T * arr, sizeT count, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a set from an array
		 * @tparam N			Array size
		 * @param[in] arr	Array
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		Set(const T * arr, sizeT count, const CompLess& comp, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a set from iterators
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @param[in] pAlloc	Allocator
		 */
		Set(const T * itFirst, const T * itLast, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a set from iterators
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		Set(const T * itFirst, const T * itLast, const CompLess& comp, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a set from iterators
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 * @param[in] pAlloc		Allocator
		 */
		template<typename InputIterator>
		Set(const InputIterator& itFirst, const InputIterator& itLast, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a set from iterators
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 * @param[in] comp			Comparison predicate
		 * @param[in] pAlloc		Allocator
		 */
		template<typename InputIterator>
		Set(const InputIterator& itFirst, const InputIterator& itLast, const CompLess& comp, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a set from an initializer list
		 * @param[in] il		Initializer list
		 * @param[in] pAlloc	Allocator
		 */
		Set(std::initializer_list<T> il, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a set from an initializer list
		 * @param[in] il		Initializer list
		 * @param[in] comp		Comparison predicate
		 * @param[in] pAlloc	Allocator
		 */
		Set(std::initializer_list<T> il, const CompLess& comp, Memory::IAllocator * pAlloc = g_pAllocator);
		/**
		 * Create a set from another set
		 * @param[in] set		Set
		 */
		Set(const Set& set);
		/**
		 * Create a set from another set
		 * @param[in] set		Set
		 * @param[in] pAlloc	Allocator
		 */
		Set(const Set& set, Memory::IAllocator * pAlloc);
		/**
		 * Move a set into this set
		 * @param[in] set		Set
		 */
		Set(Set&& set) noexcept;
		~Set();

		template<sizeT N>
		Set& operator=(T(&arr)[N]);
		Set& operator=(std::initializer_list<T> il);
		Set& operator=(const Set& set);
		Set& operator=(Set&& set) noexcept;

		/**
		 * Insert a value into the set
		 * @param[in] val	Value
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const T& val);
		/**
		 * Move a value into the set
		 * @param[in] val	Value
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(T&& val);
		/**
		 * Insert a value into the set, with a hint
		 * @param[in] val	Value
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const Iterator& hint, const T& val);
		/**
		 * Move a value into the set, with a hint
		 * @param[in] val	Value
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Pair with info about the insertion
		 *					-first is the iterator to the inserted value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		Pair<Iterator, b8> Insert(const Iterator& hint, T&& val);
		/**
		 * Insert an array into the set
		 * @tparam N		Array size
		 * @param[in] arr	Array
		 */
		template<sizeT N>
		void Insert(T(&arr)[N]);
		/**
		 * Insert an array into the set
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 */
		void Insert(const T * arr, sizeT count);
		/**
		 * Insert a range of value into the set
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		void Insert(const T * itFirst, const T * itLast);
		/**
		 * Insert a range of value into the set
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 */
		template<typename InputIterator>
		void Insert(const InputIterator& itFirst, const InputIterator& itLast);
		/**
		 * Insert an initializer list into the set
		 * @param[in] il	Initializer list
		 */
		template<typename InputIterator>
		void Insert(std::initializer_list<T> il);

		/**
		 * Create and insert a value into the set
		 * @tparam Args		Constructor argument types
		 * @param[in] args	Constructor arguments
		 * @return			Pair with info about the emplacement
		 *					-first is the iterator to the emplaced value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		template<typename... Args>
		Pair<Iterator, b8> Emplace(Args&&... args);
		/**
		 * Create and insert a value into the set, with a hint
		 * @tparam Args		Constructor argument types
		 * @param[in] args	Constructor arguments
		 * @param[in] hint	Iterator to node to start searching from
		 * @return			Pair with info about the emplacement
		 *					-first is the iterator to the emplaced value, or if the value already is inserted, to the location of that value
		 *					-second is true if a new element was inserted
		 */
		template<typename... Args>
		Pair<Iterator, b8> EmplaceHint(const Iterator& hint, Args&&... args);

		/**
		 * Erase a value from the set
		 * @param[in] val	Value to erase
		 * @return			Amount of values removed, either 1 or 0
		 */
		sizeT Erase(const T& val);
		/**
		 * Erase an iterator from the set
		 * @param[in] it	Iterator to erase
		 * @return			Iterator after the removed element
		 */
		Iterator Erase(const Iterator& it);
		/**
		 * Erase a range of values
		 * @param[in] itFirst	Iterator to the first element
		 * @param[in] itLast	Iterator to the element after the last element
		 * @return				Iterator after the last removed element
		 */
		Iterator Erase(const Iterator& itFirst, const Iterator& itLast);

		/**
		 * Clear the set
		 */
		void Clear();

		/**
		 * Find a value in the set
		 * @param[in] val	Value to find
		 */
		Iterator Find(const T& val) const;
		/**
		 * Find the first occurance where the predicate returns true
		 * @tparam U			Value type
		 * @tparam Pred		Predicate type
		 * @param[in] val	Value to compare width
		 * @param[in] pred	Compare less predicate (1st param: value, 2nd param: value of type U)
		 */
		template<typename U, typename Pred>
		Iterator FindAs(const U& val, const Pred& pred) const;

		/**
		 * Get the amount of entries that have a certain value
		 * @param[in] val	Value to count
		 * @return			Amount of entries of 'value'
		 */
		sizeT Count(const T& val) const;

		/**
		 * Find the first occurance which isn't smaller than a value
		 * @param[in] val	Value
		 */
		Iterator LowerBound(const T& val)  const;
		/**
		 * Find the last occurance which isn't larger than a value
		 * @param[in] val	Value
		 */
		Iterator UpperBound(const T& val)  const;
		/**
		 * Find the first occurance which isn't smaller and last occurance which isn't larger than a value
		 * @param[in] val	Value
		 */
		Pair<Iterator, Iterator> EqualRange(const T& val)  const;

		/**
		 * Swap the content of 2 sets
		 * @param[in] set	Set to swap content with
		 */
		void Swap(Set& set);

		/**
		 * Check if the set is empty
		 * @return	If the set is empty
		 */
		b8 IsEmpty() const;
		/**
		 * Get the size of the set
		 * @return	Size of the set
		 */
		sizeT Size() const;
		/**
		 * Get a pointer to the allocator
		 * @return	Pointer to the allocator
		 */
		Memory::IAllocator * GetAllocator()  const;

		/**
		 * Return an iterator to the front/begin of the set
		 * @return	Iterator to the front/begin of the set
		 */
		Iterator Front() const;
		/**
		 * Return an iterator to the last element of the set
		 * @return	Iterator to the last element of the set
		 */
		Iterator Last() const;
		/**
		 * Return an iterator to the back/end of the set
		 * @return	Iterator to the back/end of the set
		 */
		Iterator Back() const;

		/**
		 * Return an iterator to the begin of the set
		 * @note	Function meant to be used for range-based for-loops, use Front instead
		 * @return	Iterator to the begin of the set
		 */
		Iterator begin() const;
		/**
		 * Return an iterator to the end of the set
		 * @note	Function meant to be used for range-based for-loops, use Back instead
		 * @return	Iterator to the end of the set
		 */
		Iterator end() const;

	private:

		BaseType m_Tree;	/**< Internal tree */

	};
	
}

#include "Inline/Set.inl"

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename T, typename C), HV_TTYPE(Hv::Set, T, C))