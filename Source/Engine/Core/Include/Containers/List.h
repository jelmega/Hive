// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// List.h: Doubly linked list
#pragma once
#include "Core/CoreHeaders.h"
#include "ContainerUtils.h"
#include "Memory/Memory.h"

namespace Hv {
	
	/**
	 * Singly linked list
	 * @note	The containers uses multiple ways to copy data internally, using the copy-assignment operator as default,
	 *			but MemCpy and the copy-constructor can also be used. These values can be set using their relative declares:
	 *			HV_DECLARE_CONTAINER_MEMCPY and HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR and their respective template variants.
	 * @tparam T			Element type
	 */
	template<typename T>
	class List
	{
	private:
		struct Node
		{
			T value;
			Node* pNext;
			Node* pPrev;

			/**
			 * Create an empty node
			 */
			Node();
			/**
			 * Create a node
			 * @param[in] val	Value
			 * @param[in] pNext	Next node
			 */
			Node(const T& val, Node* pPrev = nullptr, Node* pNext = nullptr);
			/**
			 * Create a node, by moving the value
			 * @param[in] val	Value
			 * @param[in] pNext	Next node
			 */
			Node(T&& val, Node* pPrev = nullptr, Node* pNext = nullptr);
			~Node();
		};
	public:
		class Iterator
		{
		public:
			/**
			 * Create an empty iterator
			 */
			Iterator();
			/**
			 * Create an iterator from another iterator
			 * @param[in] it	Other iterator
			 */
			Iterator(const Iterator& it);
			/**
			 * Move an iterator into this iterator
			 * @param[in] it	Other iterator
			 */
			Iterator(Iterator&& it) noexcept;

			T& operator*();
			const T& operator*() const;
			T* operator->();
			const T* operator->() const;

			Iterator& operator++();
			Iterator operator++(int);
			Iterator& operator--();
			Iterator operator--(int);
			Iterator operator+(sizeT count);
			Iterator operator-(sizeT count);

			Iterator& operator=(const Iterator& it);
			Iterator& operator=(Iterator&& it) noexcept;
			Iterator& operator+=(sizeT count);
			Iterator& operator-=(sizeT count);

			b8 operator==(const Iterator& it) const;
			b8 operator!=(const Iterator& it) const;

		private:
			/**
			 *  Create an iterator from a node
			 * @param[in] pNode	Node
			 */
			Iterator(Node* pNode);

			Node* m_pNode;
		};

	public:
		/**
		 * Create an empty List
		 */
		List();
		/**
		 * Create an empty List
		 * @param[in] pAlloc	Memory::IAllocator
		 */
		List(Memory::IAllocator* pAlloc);
		/**
		 * Create an List of a certain size, filled with a value
		 * @param[in] count		Amount of values
		 * @param[in] value		Value
		 * @param[in] pAlloc	Allocator
		 */
		List(sizeT count, const T& value, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create an List from an array
		 * @tparam N			Array size
		 * @param[in] arr		Array
		 * @param[in] pAlloc	Allocator
		 */
		template<sizeT N>
		List(T(&arr)[N], Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a List from an array
		 * @param[in] arr		Array
		 * @param[in] count		Array size
		 * @param[in] pAlloc	Allocator
		 */
		List(const T* arr, sizeT count, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a List from iterators
		 * @param[in] itFirst	InputIterator to the first element
		 * @param[in] itLast	InputIterator to the element after the last element
		 * @param[in] pAlloc	Allocator
		 */
		List(const T* itFirst, const T* itLast, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a List from iterators
		 * @tparam InputIterator	Forward iterator
		 * @param[in] itFirst		Iterator to the first element
		 * @param[in] itLast		Iterator to the element after the last element
		 * @param[in] pAlloc		Allocator
		 */
		template<typename InputIterator>
		List(const InputIterator& itFirst, const InputIterator& itLast, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a List from an initializer list
		 * @tparam InputIterator	Forward iterator
		 * @param[in] il			Initializer list
		 * @param[in] pAlloc		Allocator
		 */
		List(std::initializer_list<T> il, Memory::IAllocator* pAlloc = g_pAllocator);
		/**
		 * Create a List from another List
		 * @param[in] list	Other List
		 */
		List(const List& list);
		/**
		 * Create a List from another List
		 * @param[in] list		Other List
		 * @param[in] pAlloc	Allocator
		 */
		List(const List& list, Memory::IAllocator* pAlloc);
		/**
		 * Move a List into this List
		 * @param[in] list	Other List
		 */
		List(List&& list) noexcept;
		~List();

		template<sizeT N>
		List& operator=(T(&arr)[N]);
		List& operator=(std::initializer_list<T> il);
		List& operator=(const List& list);
		List& operator=(List&& list) noexcept;

		T& operator[](sizeT index);
		const T& operator[](sizeT index) const;

		/**
		 * Fill the List with a value for 'count' indices
		 * @param[in] val	Value
		 * @param[in] count	Amount of values
		 */
		void Fill(const T& val, sizeT count);
		/**
		 * Assign an array to the List
		 * @tparam N		Array size
		 * @param[in] arr	Array
		 */
		template<sizeT N>
		void Assign(T(&arr)[N]);
		/**
		 * Assign an array to the List
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 */
		void Assign(T* arr, sizeT count);
		/**
		 * Assign a range of values to the List
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		void Assign(T* itFirst, T* itLast);
		/**
		 * Assign a range of values to the List
		 * @tparam InputIterator		Forward iterator
		 * @param[in] itFirst	InputIterator to first element
		 * @param[in] itLast	InputIterator to the element after the last element
		 */
		template<typename InputIterator>
		void Assign(const InputIterator& itFirst, const InputIterator& itLast);
		/**
		 * Asign an initializer list to the List
		 * @param[in] il	Initializer list
		 */
		void Assign(std::initializer_list<T> il);

		/**
		 * Resize the List
		 * @param[in] size	New size
		 * @note			Lists cannot be made larger with resize
		 */
		void Resize(sizeT size);
		/**
		 * Resize the List
		 * @param[in] size	New size
		 * @param[in] val	Value to add if size is bigger than the current size
		 */
		void Resize(sizeT size, const T& val);
		/**
		 * Clear the vectors contents
		 */
		void Clear();

		/**
		 * Add a value to the back of the List
		 * @param[in] val	Value to add
		 */
		void PushBack(const T& val);
		/**
		 * Move a value to the back of the List
		 * @param[in] val	Value to add
		 */
		void PushBack(T&& val);
		/**
		 * Remove the last value from the List
		 */
		void PopBack();
		/**
		 * Add a value to the front of the List
		 * @param[in] val	Value to add
		 */
		void PushFront(const T& val);
		/**
		 * Move a value to the front of the List
		 * @param[in] val	Value to add
		 */
		void PushFront(T&& val);
		/**
		 * Remove the first value from the List
		 */
		void PopFront();

		/**
		 * Insert a value into the List
		 * @param[in] it	Iterator to location in List
		 * @param[in] val	Value to insert
		 */
		void Insert(const Iterator& it, const T& val);
		/**
		 * Insert a value into the List by moving it
		 * @param[in] it	Iterator to location in List
		 * @param[in] val	Value to insert
		 */
		void Insert(const Iterator& it, T&& val);
		/**
		 * Insert an amount of values into the List
		 * @param[in] it	Iterator to location in List
		 * @param[in] count	Amount of values to insert
		 * @param[in] val	Value to insert
		 */
		void Insert(const Iterator& it, sizeT count, const T& val);
		/**
		 * Insert an array into the List
		 * @tparam N		Array size
		 * @param[in] it	Iterator to location in List
		 * @param[in] arr	Array
		 */
		template<sizeT N>
		void Insert(const Iterator& it, T(&arr)[N]);
		/**
		 * Insert an array into the List
		 * @param[in] it	Iterator to location in List
		 * @param[in] arr	Array
		 * @param[in] count	Array size
		 */
		void Insert(const Iterator& it, T* arr, sizeT count);
		/**
		 * Insert a range of values into the List
		 * @param[in] it		Iterator to location in List
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		void Insert(const Iterator& it, const T* itFirst, const T* itLast);
		/**
		 * Insert a range of values into the List
		 * @tparam InputIterator		Forward InputIterator
		 * @param[in] it		Iterator to location in List
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	Iterator to the element after the last element
		 */
		template<typename InputIterator>
		void Insert(const Iterator& it, const InputIterator& itFirst, const InputIterator& itLast);
		/**
		 * Insert an initializer list into the List
		 * @param[in] it	Iterator to location in List
		 * @param[in] il	Initializer list
		 */
		void Insert(const Iterator& it, std::initializer_list<T> il);

		/**
		 * Erase a value
		 * @param[in] it	Iterator to location in List
		 */
		void Erase(const Iterator& it);
		/**
		 * Erase an amount of values
		 * @param[in] it	Iterator to location in List
		 * @param[in] count	Amount of values to erase
		 */
		void Erase(const Iterator& it, sizeT count);
		/**
		 * Erase a range of values
		 * @param[in] itFirst	Iterator to first element
		 * @param[in] itLast	Iterator to element after last element
		 */
		void Erase(const Iterator& itFirst, const Iterator& itLast);

		/**
		 * Create and insert a value
		 * @tparam Args		Constructor argument types
		 * @param[in] it	Iterator to location in List
		 * @param[in] args	Constructor arguments
		 */
		template<typename... Args>
		void Emplace(const Iterator& it, Args&&... args);
		/**
		 * Create and add a value to the back of the List
		 * @tparam Args		Constructor argument types
		 * @param[in] args	Constructor arguments
		 */
		template<typename... Args>
		void EmplaceBack(Args&&... args);
		/**
		 * Create and add a value to the back of the List
		 * @tparam Args		Constructor argument types
		 * @param[in] args	Constructor arguments
		 */
		template<typename... Args>
		void EmplaceFront(Args&&... args);

		/**
		 * Swap the content of 2 lists
		 * @param[in] list	List to swap content with
		 */
		void Swap(List& list);

		/**
		 * Get the node at a certain index
		 * @param[in] index	Index of node
		 * @return			Node at index
		 */
		Node* GetNodeAtIndex(sizeT index);

		/**
		 * Check if the list is empty
		 * @return	If the list is empty
		 */
		b8 IsEmpty() const;
		/**
		 * Get the size of the List
		 * @return	Size of the List
		 */
		sizeT Size() const;
		/**
		 * Get a pointer to the allocator
		 * @return	Pointer to the allocator
		 */
		Memory::IAllocator* GetAllocator();

		/**
		 * Return an iterator to the front/begin of the list
		 * @return	InputIterator to the front/begin of the list
		 */
		Iterator Front() const;
		/**
		 * Return an iterator to the last element of the list
		 * @return	InputIterator to the last element of the list
		 */
		Iterator Last() const;
		/**
		 * Return an iterator to the back/end of the list
		 * @return	InputIterator to the back/end of the list
		 */
		Iterator Back() const;

		/**
		 * Return an iterator to the begin of the array
		 * @note	Function meant to be used for range-based for-loops, use Front instead
		 * @return	InputIterator to the begin of the array
		 */
		Iterator begin() const;
		/**
		 * Return an iterator to the end of the array
		 * @note	Function meant to be used for range-based for-loops, use Back instead
		 * @return	InputIterator to the end of the array
		 */
		Iterator end() const;

	private:

		/**
		 * Copy a list to this list
		 * @param[in] list	List to copy
		 */
		void Copy(const List& list);

		Node* m_pHead;					/**< Linked list head */
		Node* m_pTail;					/**< Linked list tail */
		sizeT m_Size;					/**< Size */
		Memory::IAllocator* m_pAlloc;	/**< Pointer to allocator*/
	};

}

#include "Inline/List.inl"

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename T), HV_TTYPE(Hv::List, T))
