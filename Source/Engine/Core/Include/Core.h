// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Core.h: Includes files to use core
#pragma once

#include "Core/CoreHeaders.h"
#include "HAL/HalSystem.h"

#include "Containers/Containers.h"

#include "String/String.h"

#include "Math/Math.h"

#include "FileSystem/FileSystem.h"

#include "Time/Time.h"

#include "System/SystemConsole.h"
#include "System/CoreSystem.h"

#include "Logging/Logger.h"

#include "DynLib/DynLib.h"

#include "Misc/PixelFormat.h"
#include "Misc/BitBuffer.h"
#include "Misc/Handle.h"

#include "Checksum/Checksum.h"

#include "Threading/Threading.h"

#include "Json/Json.h"

#include "RTTI/RTTI.h"
#include HV_INCLUDE_RTTI_MODULE(Core)
