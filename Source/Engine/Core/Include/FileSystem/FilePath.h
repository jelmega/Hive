// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// FilePath.h: Filesystem path
#pragma once
#include "Core/CoreHeaders.h"
#include "String/String.h"
#include "HAL/HalFileSystem.h"

namespace Hv::FileSystem {
	
	/**
	 * File/Directory path
	 */
	class Path
	{
	public:
		static constexpr AnsiChar DirSeparators[2] = { '\\', '/' };

		/**
		 * Create an empty path
		 */
		Path();
		/**
		 * Create a path from a string
		 * @param[in] path	Path string
		 */
		template<typename C>
		Path(const TString<C>& path);
		/**
		 * Create a path from a string
		 * @param[in] path	Path string
		 */
		Path(const AnsiChar* path);
		/**
		 * Create a path from a string
		 * @param[in] path	Path string
		 */
		Path(const WideChar* path);
		/**
		 * Create a path from another path
		 * @param[in] path	Path
		 */
		Path(const Path& path);
		/**
		 * Move a path into this path
		 * @param[in] path	Path
		 */
		Path(Path&& path) noexcept;

		Path operator+(const Path& path) const;

		Path operator/(const Path& path) const;

		template<typename C>
		Path& operator=(const TString<C>& path);
		Path& operator=(const Path& path);
		Path& operator=(Path&& path) noexcept;

		Path& operator+=(const Path& path);
		Path& operator/=(const Path& path);

		b8 operator==(const Path& path) const;
		b8 operator!=(const Path& path) const;

		/**
		 * Append a path to this path (with a native separator inbetween)
		 * @param[in] path	Path
		 * @return			Reference to the path
		 */
		Path& Append(const Path& path);
		/**
		 * Concatinate a path to this path
		 * @param[in] path	Path
		 * @return			Reference to the path
		 */
		Path& Concat(const Path& path);

		/**
		 * Clear the path
		 */
		void Clear();
		/**
		 * Convert all directory separators with the native separator
		 * @note	Separator: \\ (Windows), / (other OS's)
		 */
		void MakePrefered();

		/**
		 * Remove the filename from the path
		 */
		void RemoveFilename();
		/**
		 * Replace the filename in the path
		 * @param[in] filename	Filename
		 */
		void ReplaceFilename(const Path& filename);
		/**
		 * Remove the extension from the path
		 */
		void RemoveExtension();
		/**
		 * Replace the extension in the path
		 * @param[in] extension	Extension
		 */
		void ReplaceExtension(const Path& extension);

		/**
		 * Get the root name of the path
		 * @return	Root name
		 */
		Path RootName() const;
		/**
		 * Get the root directory of the path
		 * @return	Root directory
		 */
		Path RootDirectory() const;
		/**
		 * Get the root path of the path (rootname + root directory)
		 * @return	Root path
		 */
		Path RootPath() const;
		/**
		 * Get the relative path of the path (without rootpath)
		 * @return	Relative path
		 */
		Path RelativePath() const;
		/**
		 * Get the parent path of the path (containing directory)
		 * @return	Parent path
		 */
		Path ParentPath() const;
		/**
		 * Get the filename of the path
		 * @return	Filename
		 */
		Path Filename() const;
		/**
		 * Get the stem of the path (filename without extension)
		 * @return	Stem
		 */
		Path Stem() const;
		/**
		 * Get the extension of the path
		 * @return	Extension
		 */
		Path Extension() const;

		/**
		 * Check if the path has a rootname
		 * @return	True if the path has a rootname, false otherwise
		 */
		b8 HasRootName() const;
		/**
		 * Check if the path has a root directory
		 * @return	True if the path has a root directory, false otherwise
		 */
		b8 HasRootDirectory() const;
		/**
		 * Check if the path has a root path
		 * @return	True if the path has a root path, false otherwise
		 */
		b8 HasRootPath() const;
		/**
		 * Check if the path has a relative path
		 * @return	True if the path has a relative path, false otherwise
		 */
		b8 HasRelativePath() const;
		/**
		 * Check if the path has a parent path
		 * @return	True if the path has a parent path, false otherwise
		 */
		b8 HasParentPath() const;
		/**
		 * Check if the path has a filename
		 * @return	True if the path has a filename, false otherwise
		 */
		b8 HasFilename() const;
		/**
		 * Check if the path has a stem
		 * @return	True if the path has a stem, false otherwise
		 */
		b8 HasStem() const;
		/**
		 * Check if the path has an extension
		 * @return	True if the path has an extension, false otherwise
		 */
		b8 HasFileExtension() const;
		/**
		 * Check if the path is relative
		 * @return	True if the path is relative, false otherwise
		 */
		b8 IsAbsolute() const;
		/**
		 * Check if the path is absolute
		 * @return	True if the path is absolute, false otherwise
		 */
		b8 IsRelative() const;
		/**
		 * Check if the path points to a mounted directory
		 * @return	True if the path refers to a mounted directory, false otherwise
		 * @note	Since the path has no access to the file system, the function will only return if the path start with a mounted directory macro
		 */
		b8 IsMounted() const;
		/**
		 * Check if the path is empty
		 * @return	True if the path is empty, false otherwise
		 */
		b8 IsEmpty() const;
		/**
		 * Get the path as a string
		 * @return	String
		 */
		const String& ToString() const;
		/**
		 * Get the path as a string in its system native representation
		 * @return	String in its system native representation
		 */
		String ToNativeString() const;

		/**
		 * Swap 2 paths
		 * @param[in] path	Path to swap with
		 */
		void Swap(Path& path);

	private:
		/**
		 * Get the end index of the root name
		 * @return			index to the end of the root name, if there is no rootname, NPos is returned
		 */
		sizeT PrefixEnd() const;
		/**
		 * Get the end index of the root path
		 * @return			index to the end of the root path, if there is no rootname, NPos is returned
		 */
		sizeT RootEnd() const;

		String m_Path;
	};

}

#include "Inline/FilePath.inl"

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR(Hv::FileSystem::Path);