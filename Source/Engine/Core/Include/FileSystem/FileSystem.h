// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// FileSystem.h: File system
#pragma once
#include "HAL/HalFileSystem.h"
#include "FilePath.h"
#include "File.h"
#include "DirectoryEntry.h"
#include "DirectoryIterator.h"
#include "Containers/HashMap.h"


namespace Hv::FileSystem {
	
	namespace Detail {
		/**
		* Get a reference to the mounted directory hashmap
		* @return	Reference to the mounted directory hashmap
		*/
		HIVE_API HashMap<String, String>& GetMountedDirMap();
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Path
	////////////////////////////////////////////////////////////////////////////////
	/**
	 * Get the current/working directory of the application
	 * @return	Current/Working directory
	 * @note	When running the application from an external program (e.g. Visual studio),\n
	 *			the path is not guarranteed to return the directory where the apllication is located
	 */
	HIVE_API Path GetCurrentDirectory();

	/**
	 * Mount a new directory in the filesystem
	 * @param[in] macro	Macro refering to the mounted directory
	 * @param[in] path	Path to the directory to mount
	 * @return			True if the directory could be mounted, false if the macro already exists
	 * @note			Mounted directories can be refered to with the following at the start of the path "$macro",\n
	 *					macros will be replaced with its matching directory
	 */
	HIVE_API b8 Mount(const String& macro, const Path& path);
	/**
	 * Unmount a directory
	 * @param[in] macro	Macro refering to the mounted directory
	 * @return			True if the macro was removed, false otherwise (non-existing macro)
	 */
	HIVE_API b8 UnMount(const String& macro);
	/**
	 * Get the mounted directory pointed to by a macro
	 * @param[in] macro	Macro to the mouted directory
	 * @return			Mounted directory or "" if the mounted directory doesn't exist
	 */
	HIVE_API Path GetMountedDirectory(const String& macro);
	/**
	 * Replace the macro with it's mounted directory
	 * @param[in] path	Path with macro
	 * @return			Path with mounted directory or path ithout macro, if macro doesn't exist
	 */
	HIVE_API Path InsertMountedDirectory(const Path& path);

	/**
	 * Get an 'absolute' path with base as root path
	 * @param[in] path	Relative path
	 * @param[in] base	Base of absolute path
	 * @return			Absolute path
	 * @note			If 'path' is absolute, 'path' will be returned\n
	 *					If 'path' has a root name, but no root directory, the base will be inserted
	 */
	HIVE_API Path AbsolutePath(const Path& path, const Path& base = GetCurrentDirectory());
	/**
	 * Get the absolute path, usig the current/working folder for the base
	 * @param[in] path	Relative path
	 * @return			Absolute path
	 * @note			If 'path' is absolute, 'path' will be returned
	 */
	HIVE_API Path SystemAbsolutePath(const Path& path);

	/**
	 * Resolve the path so it can be used in functions
	 * @param[in] path	Path to resolve
	 * @return			Resolved path
	 */
	HIVE_API Path ResolvePath(const Path& path);

	////////////////////////////////////////////////////////////////////////////////
	//	Directories
	////////////////////////////////////////////////////////////////////////////////
	/**
	 * Create a directory
	 * @param[in] path	Path of the new directory
	 * @return			True if directory creation succeded, false otherwise
	 */
	HIVE_API b8 CreateDirectory(const Path& path);
	/**
	* Create a directory with the same attributes as the template directory
	* @param[in] path			Path of the new directory
	* @param[in] templatePath	Path to the template directory
	* @return					True if directory creation succeded, false otherwise
	*/
	HIVE_API b8 CreateDirectory(const Path& path, const Path& templatePath);
	/**
	* Create all directories in the path (if they don't exist yet)
	* @param[in] path	Path of the new directory
	* @return			True if directory creation succeded, false otherwise
	*/
	HIVE_API b8 CreateDirectories(const Path& path);

	/**
	 * Remove/Delete an empty directory
	 * @param[in] path	Path to the directory
	 * @return			True if the directory was removed succesfully, false otherwise
	 */
	HIVE_API b8 RemoveDirectory(const Path& path);
	
	/**
	 * Check if a path point to a directory
	 * @param[in] path	Path to the directory
	 * @return			True if the path points to a directory, false otherwise
	 */
	HIVE_API b8 IsDirectory(const Path& path);

	/**
	 * Check if a directory exists
	 * @param[in] path	Path to the directory
	 * @return			True if the directory exist, false otherwise
	 */
	HIVE_API b8 DoesDirectoryExist(const Path& path);

	/**
	 * Move a directory from one directory to another directory
	 * @param[in] from	Path to the file to move
	 * @param[in] to	Path to the directory to move the file to (excluding filename)
	 * @return			True, if the file was moved successfully, false otherwise
	 * @note			Moving between drives is currently not supported
	 */
	HIVE_API b8 MoveDirectory(const Path& from, const Path& to);
	/**
	 * Copy a directory from one directory to another directory
	 * @param[in] from		Path to the file to copy
	 * @param[in] to		Path to the file to copy to (including filename)
	 * @param[in] settings	Copy settings
	 * @return				True, if the directory was copied completely, false otherwise
	 */
	HIVE_API b8 CopyDirectory(const Path& from, const Path& to, CopySettings settings);
	/**
	 * Rename a directory
	 * @param[in] path	Path to the directory to rename
	 * @param[in] name	New directory name
	 * @return			True, if the directory was renamed successfully, false otherwise
	 */
	HIVE_API b8 RenameDirectory(const Path& path, const String name);

	////////////////////////////////////////////////////////////////////////////////
	//	Files
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Create a file and/or open it
	 * @param[in] path	Path to the file to create
	 * @param[in] mode	File mode flags
	 * @return			Created file
	 * @note			Create specific flags have documentation that starts with [Create]
	 *					File mode open flags are ignored
	 */
	HIVE_API File CreateFile(const Path& path, FileMode mode, AccessMode access, ShareMode share = ShareMode::None);

	/**
	 * Delete a file
	 * @param[in] path	Path to the file to delete
	 * @return			True if the file was successfully deleted
	 */
	HIVE_API b8 DeleteFile(const Path& path);

	/**
	* Check if a path point to a file
	* @param[in] path	Path to the file
	* @return			True if the path points to a file, false otherwise
	*/
	HIVE_API b8 IsFile(const Path& path);
	/**
	* Check if a file exists
	* @param[in] path	Path to the file
	* @return			True if the file exist, false otherwise
	*/
	HIVE_API b8 DoesFileExist(const Path& path);

	/**
	 * Move a file from one directory to another directory
	 * @param[in] from	Path to the file to move
	 * @param[in] to	Path to the directory to move the file to (excluding filename)
	 * @return			True, if the file was moved successfully, false otherwise
	 * @note			Moving between drives is currently not supported
	 */
	HIVE_API b8 MoveFile(const Path& from, const Path& to);
	/**
	 * Copy a file from one directory to another directory
	 * @param[in] from		Path to the file to copy
	 * @param[in] to		Path to the file to copy to (including filename)
	 * @param[in] settings	Copy settings
	 * @return				True, if the file was copied successfully, false otherwise
	 */
	HIVE_API b8 CopyFile(const Path& from, const Path& to, CopySettings settings);
	/**
	 * Rename a file
	 * @param[in] path	Path to the file to rename
	 * @param[in] name	New file name
	 * @return			True, if the file was moved successfully, false otherwise
	 */
	HIVE_API b8 RenameFile(const Path& path, const String name);

	////////////////////////////////////////////////////////////////////////////////
	//	Iteration
	////////////////////////////////////////////////////////////////////////////////
	/**
	* Get the file handle to the first entry in a directory
	* @param[in] path	Path to the directory to iterate in
	* @return			Directory iterator
	*/
	HIVE_API DirectoryIterator GetFirstDirectoryEntry(const Path& path);

	/**
	 * Get all entries in a folder, matching a patern
	 * @param[in] baseFolder	Path to folder to search in
	 * @param[in] patern		Patern to match
	 * @return					DynArray with matching directory entries
	 */
	HIVE_API DynArray<DirectoryEntry> FindPatern(const Path& baseFolder, const String& patern);

}

namespace HvFS = Hv::FileSystem;