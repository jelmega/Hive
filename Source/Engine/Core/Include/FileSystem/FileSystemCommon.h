// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// FileSystemCommon.h: Common filesystem types
#pragma once
#include "Core/CoreHeaders.h"

namespace Hv::FileSystem {
	
	/**
	 * File access modes
	 */
	enum class AccessMode
	{
		None		= 0x00,			/**< No flags */
		Read		= 0x01,			/**< Read from file */
		Write		= 0x02,			/**< Write to file */
		ReadWrite	= Read | Write	/**< Read and write from/to file */
	};
	HV_ENABLE_ENUM_FLAG_OPERATORS(AccessMode);

	/**
	 * Sharing mode, allows another application or a new filesystem file to access the file with the corresponding accessmode
	 * @note	If a sharing flags is enabled, but the corresponding access flag is not set, the flag will have no effect
	 */
	enum class ShareMode
	{
		None		= 0x00,			/**< No flags */
		Read		= 0x01,			/**< Share read access */
		Write		= 0x02,			/**< Share write access */
		ReadWrite	= Read | Write	/**< Share read and write access */
	};
	HV_ENABLE_ENUM_FLAG_OPERATORS(ShareMode);

	/**
	 * Properties for creating and opening files
	 */
	enum class FileMode : u8
	{
		None			= 0x00,	/**< No mode */
		Create			= 0x01,	/**< [Create] Create a file, if the file exists, overwrite it */
		CreateNew		= 0x02,	/**< [Create] Create a file, only if the file does not exist.\n If the file already exist, an invalid file is returned*/
		Open			= 0x03,	/**< [Open] Open an existing file*/
		OpenOrCreate	= 0x04,	/**< [Open] Try to open a file, if the file doesn't exist, create a new file*/
		Truncate		= 0x05,	/**< [Open] Truncate the file when opening (Write access is needed to use this flag, if no write flag is defined, this flag will have no effect) */
		Append			= 0x09	/**< [Open] Open an existing file and go to the end (Write access is needed to use this flag, if no write flag is defined, this flag will have no effect) */
	};
	HV_ENABLE_ENUM_FLAG_OPERATORS(FileMode);

	/**
	 * Copy settings for copying directories or files
	 */
	enum class CopySettings : u8
	{
		None				= 0x00,	/**< No settings */
		SkipExisting		= 0x01,	/**< [File] If file exists in copy location, do nothing*/
		OverwriteExisting	= 0x02,	/**< [File] If file exists in copy location, overwrite it*/
		OverwriteIfNewer	= 0x03,	/**< [File] If file exists in copy location, overwite if the file to copy is newer */
		Recursive			= 0x10,	/**< [Directory] Copy sub-directories recursivly*/

		FileMask			= 0x03	/**< [Mask] Mask for file settings */
	};
	HV_ENABLE_ENUM_FLAG_OPERATORS(CopySettings);

	enum class EntryType
	{
		None,
		File,
		Directory
		//SymLink
	};

}
