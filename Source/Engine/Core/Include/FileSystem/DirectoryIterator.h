// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DirectoryIterator.h: File system directory iterator
#pragma once
#include "HAL/HalFileSystem.h"
#include "DirectoryEntry.h"

namespace Hv::FileSystem {

	class HIVE_API DirectoryIterator
	{
	public:
		/**
		* Create an empty directory iterator
		*/
		DirectoryIterator();
		/**
		* Create a directory iterator from a directory entry
		* @param[in] entry	Directory entry
		*/
		DirectoryIterator(const DirectoryEntry& entry, const FileSearchHandle& handle);
		/**
		* Create a directory iterator from another iterator
		* @param it	Directory iterator
		*/
		DirectoryIterator(const DirectoryIterator& it);
		/**
		* Move a directory iterator into this another iterator
		* @param it	Directory iterator
		*/
		DirectoryIterator(DirectoryIterator&& it) noexcept;
		~DirectoryIterator();

		DirectoryEntry* operator->();
		const DirectoryEntry* operator->() const;
		DirectoryEntry& operator*();
		const DirectoryEntry& operator*() const;

		DirectoryIterator& operator++();
		DirectoryIterator operator++(int);

		DirectoryIterator& operator=(const DirectoryIterator& it);
		DirectoryIterator& operator=(DirectoryIterator&& it) noexcept;

		b8 operator==(const DirectoryIterator& it) const;
		b8 operator!=(const DirectoryIterator& it) const;

	private:
		FileSearchHandle m_Handle;
		DirectoryEntry m_Entry;
	};

}
