// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// File.inl: File system file
#pragma once
#include "FileSystem/File.h"

namespace Hv::FileSystem {
	
	template <typename T>
	b8 File::Read(T& value)
	{
		return ReadBytes((u8*)&value, sizeof(T));
	}

	template <typename T>
	b8 File::Read(T* array, sizeT count, sizeT* pValuesRead)
	{
		b8 res = ReadBytes((u8*)array, count * sizeof(T), pValuesRead);
		if (pValuesRead)
			*pValuesRead /= sizeof(T);
		return res;
	}

	template <typename T>
	b8 File::Write(T value)
	{
		return WriteBytes((u8*)&value, sizeof(T));
	}

	template <typename T>
	b8 File::Write(T* array, sizeT count)
	{
		return WriteBytes((u8*)array, count * sizeof(T));
	}
}
