// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// FilePath.inl: Filesystem path
#pragma once
#include "FileSystem/FilePath.h"

#define IS_SEP(x) (x == DirSeparators[0] ||x == DirSeparators[1])

namespace Hv::FileSystem {
	
	HV_FORCE_INL Path::Path()
	{
	}

	template<typename C>
	HV_INL Path::Path(const TString<C>& path)
		: m_Path(path)
	{
	}

	HV_INL Path::Path(const AnsiChar* path)
		: m_Path(path)
	{
	}

	HV_INL Path::Path(const WideChar* path)
		: m_Path(path)
	{
	}

	HV_INL Path::Path(const Path& path)
		: m_Path(path.m_Path)
	{
	}

	HV_INL Path::Path(Path&& path) noexcept
		: m_Path(Move(path.m_Path))
	{
	}

	HV_INL Path Path::operator+(const Path& path) const
	{
		return Path(*this).Concat(path);
	}

	HV_INL Path Path::operator/(const Path& path) const
	{
		return Path(*this).Append(path);
	}

	template<typename C>
	HV_INL Path& Path::operator=(const TString<C>& path)
	{
		m_Path = path;
		return *this;
	}

	HV_INL Path& Path::operator=(const Path& path)
	{
		m_Path = path.m_Path;
		return *this;
	}

	HV_INL Path& Path::operator=(Path&& path) noexcept
	{
		m_Path = Move(path.m_Path);
		return *this;
	}

	HV_INL Path& Path::operator+=(const Path& path)
	{
		Concat(path);
		return *this;
	}

	HV_INL Path& Path::operator/=(const Path& path)
	{
		Append(path);
		return *this;
	}

	HV_FORCE_INL b8 Path::operator==(const Path& path) const
	{
		return m_Path == path.m_Path;
	}

	HV_FORCE_INL b8 Path::operator!=(const Path& path) const
	{
		return m_Path != path.m_Path;
	}

	HV_INL Path& Path::Append(const Path& path)
	{
		String tmp = path.m_Path;
		if (tmp.StartsWith(DirSeparators[0]) || tmp.StartsWith(DirSeparators[1]))
			tmp.Erase(sizeT(0u), sizeT(1u)); // sizeT casts for x64

		if (m_Path.EndsWith(DirSeparators[0]) || m_Path.EndsWith(DirSeparators[1]))
			m_Path.Append(path.m_Path);
		else
			m_Path.Append(HAL::FileSystem::g_NativeSeparator).Append(path.m_Path);
		return *this;
	}

	HV_INL Path& Path::Concat(const Path& path)
	{
		m_Path.Append(path.m_Path);
		return *this;
	}

	HV_INL void Path::Clear()
	{
		m_Path.Clear(true);
	}

	HV_INL void Path::MakePrefered()
	{
		m_Path.ReplaceChar(DirSeparators, HAL::FileSystem::g_NativeSeparator);
	}

	HV_INL void Path::RemoveFilename()
	{
		sizeT idx = m_Path.RFindAny(DirSeparators, 2, String::NPos);
		if (idx != String::NPos)
		{
			if (sizeT(idx) == m_Path.Length() - 1)
				m_Path.PopBack();
			else
				m_Path.Erase(idx, String::NPos);
		}
		else
			m_Path.Clear(true);
	}

	HV_INL void Path::ReplaceFilename(const Path& filename)
	{
		sizeT idx = m_Path.RFindAny(DirSeparators, 2, String::NPos);
		if (idx != String::NPos)
		{
			if (sizeT(idx) == m_Path.Length() - 1)
				m_Path.Append(filename.m_Path);
			else
			{
				m_Path.Erase(idx + 1, String::NPos);
				m_Path.Append(filename.m_Path);
			}
		}
		else
			m_Path = filename.m_Path;
	}

	HV_INL void Path::RemoveExtension()
	{
		sizeT idx = m_Path.RFind('.');
		if (idx == String::NPos)
			return;
		sizeT sepIdx = m_Path.RFindAny(DirSeparators, 2, String::NPos);
		if (idx < sepIdx && sepIdx != String::NPos)
			return;
		if (idx == m_Path.Length() - 1)
			m_Path.PopBack();
		else
			m_Path.Erase(idx, String::NPos);
	}

	HV_INL void Path::ReplaceExtension(const Path& extension)
	{
		String ext = extension.m_Path;
		ext.TrimLeft('.');
		sizeT idx = m_Path.RFind('.');
		sizeT sepIdx = m_Path.RFindAny(DirSeparators, 2, String::NPos);
		if (idx < sepIdx)
			return;
		if (idx == m_Path.Length() - 1)
			m_Path.Append(ext);
		else
		{
			if (idx != String::NPos)
				m_Path.Erase(idx, String::NPos);
			m_Path.Append('.').Append(ext);
		}
	}

	HV_INL Path Path::RootName() const
	{
		sizeT idx = PrefixEnd();
		if (idx == String::NPos)
			return Path();
		return Path(m_Path.SubString(0, idx));
	}

	HV_INL Path Path::RootDirectory() const
	{
		sizeT start = PrefixEnd();
		if (start == String::NPos)
			return Path();
		sizeT end = RootEnd();
		if (end == String::NPos)
			return Path();
		return Path(m_Path.SubString(start, end - start));
	}

	HV_INL Path Path::RootPath() const
	{
		sizeT end = RootEnd();
		if (end == String::NPos)
			return Path();
		return Path(m_Path.SubString(0, end));
	}

	HV_INL Path Path::RelativePath() const
	{
		sizeT idx = RootEnd();
		if (idx == String::NPos)
			return *this;
		return Path(m_Path.SubString(idx));
	}

	HV_INL Path Path::ParentPath() const
	{
		sizeT idx = m_Path.RFindAny(DirSeparators, 2u, String::NPos);
		if (idx == String::NPos)
			return Path();
		return Path(m_Path.SubString(0, idx));
	}

	HV_INL Path Path::Filename() const
	{
		sizeT idx = m_Path.RFindAny(DirSeparators, 2u, String::NPos);
		if (idx == String::NPos)
			return *this;
		return Path(m_Path.SubString(idx + 1));
	}

	HV_INL Path Path::Stem() const
	{
		Path tmp = *this;
		tmp.RemoveExtension();
		return tmp;
	}

	HV_INL Path Path::Extension() const
	{
		sizeT idx = m_Path.RFind('.');
		if (idx == String::NPos)
			return Path();
		sizeT sepIdx = m_Path.RFindAny(DirSeparators, 2, String::NPos);
		if (idx < sepIdx && sepIdx != String::NPos)
			return Path();
		return Path(m_Path.SubString(idx + 1));
	}

	HV_INL b8 Path::HasRootName() const
	{
		return !RootName().m_Path.IsWhitespaceOrEmpty();
	}

	HV_INL b8 Path::HasRootDirectory() const
	{
		return !RootDirectory().m_Path.IsWhitespaceOrEmpty();
	}

	HV_INL b8 Path::HasRootPath() const
	{
		return !RootPath().m_Path.IsWhitespaceOrEmpty();
	}

	HV_INL b8 Path::HasRelativePath() const
	{
		return !RelativePath().m_Path.IsWhitespaceOrEmpty();
	}

	HV_INL b8 Path::HasParentPath() const
	{
		return !ParentPath().m_Path.IsWhitespaceOrEmpty();
	}

	HV_INL b8 Path::HasFilename() const
	{
		return !Filename().m_Path.IsWhitespaceOrEmpty();
	}

	HV_INL b8 Path::HasStem() const
	{
		return !Stem().m_Path.IsWhitespaceOrEmpty();
	}

	HV_INL b8 Path::HasFileExtension() const
	{
		return !Extension().m_Path.IsWhitespaceOrEmpty();
	}

	HV_FORCE_INL b8 Path::IsAbsolute() const
	{
		return !IsRelative();
	}

	HV_FORCE_INL b8 Path::IsRelative() const
	{
		return RootPath().m_Path.IsEmpty();
	}

	HV_FORCE_INL b8 Path::IsMounted() const
	{
		return m_Path.StartsWith('$');
	}

	HV_FORCE_INL b8 Path::IsEmpty() const
	{
		return m_Path.IsEmpty();
	}

	HV_FORCE_INL const String& Path::ToString() const
	{
		return m_Path;
	}

	HV_INL String Path::ToNativeString() const
	{
		String tmp = m_Path;
		tmp.ReplaceChar(DirSeparators, HAL::FileSystem::g_NativeSeparator);
		return tmp;
	}

	HV_FORCE_INL void Path::Swap(Path& path)
	{
		m_Path.Swap(path.m_Path);
	}

	HV_INL sizeT Path::PrefixEnd() const
	{
		sizeT len = m_Path.Length();
		// Check for network folder
		if (len > 2 && IS_SEP(m_Path[0]) && IS_SEP(m_Path[1]) && !IS_SEP(m_Path[2]))
			return 2;

		//Check for platform root
		return HAL::FileSystem::PrefixEnd(m_Path);
	}

	HV_INL sizeT Path::RootEnd() const
	{
		sizeT end = PrefixEnd();
		if (end == String::NPos)
			return String::NPos;
		sizeT idx = m_Path.FindAny(DirSeparators, 2u, end);
		return idx == String::NPos ? String::NPos : idx + 1;
	}

}

#undef IS_SEP