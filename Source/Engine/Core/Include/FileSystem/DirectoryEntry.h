// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DirectoryEntry.h: File system directory entry
#pragma once
#include "FileSystemCommon.h"
#include "File.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv::FileSystem {
	
	class Path;

	class HIVE_API DirectoryEntry
	{
	public:
		/**
		* Create an empty directory entry
		*/
		DirectoryEntry();
		/**
		* Create a directory entry from a file handle
		* @param[in] path	Path to the file
		* @param[in] type	Entry type
		*/
		DirectoryEntry(const Path& path, EntryType type);

		/**
		* Create a directory entry from another entry
		* @param entry	Directory iterator
		*/
		DirectoryEntry(const DirectoryEntry& entry);
		/**
		* Move a directory entry into this entry
		* @param entry	Directory entry
		*/
		DirectoryEntry(DirectoryEntry&& entry) noexcept;

		DirectoryEntry& operator=(const DirectoryEntry& entry);
		DirectoryEntry& operator=(DirectoryEntry&& entry) noexcept;

		b8 operator==(const DirectoryEntry& entry) const;
		b8 operator!=(const DirectoryEntry& entry) const;

		/**
		 * Replace the name of the current entry (file/directory name)
		 * @return	True if the name of the entry could be changed
		 */
		b8 ReplaceEntryName(const Path& name);
		/**
		 * Get the entry's name
		 * @return	Entry's name
		 */
		Path GetPath() const;
		/**
		 * Check if the entry is valid
		 * @return	True if the entry is valid, false otherwise
		 */
		b8 IsValid() const;
		/**
		 * Get the entry type
		 * @return	Entry type
		 */
		EntryType GetType() const;
		/**
		 * Check if the entry is a file
		 * @return	True, if the entry is a file, false otherwise
		 */
		b8 IsFile() const;
		/**
		 * Check if the entry is a directory
		 * @return	True, if the entry is a directory, false otherwise
		 */
		b8 IsDirectory() const;
		/**
		 * Get the file in the current entry
		 * @param[in] access	Access mode
		 * @param[in] share		Share mode
		 * @return				File
		 * @note				If the entry is not a file, and invalid file will be returned
		 */
		File GetFile(AccessMode access, ShareMode share = ShareMode::None) const;


	private:
		friend class DirectoryIterator;

		Path m_Path;

		EntryType m_Type;
	};

}

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR(Hv::FileSystem::DirectoryEntry);

#pragma warning(pop) 
