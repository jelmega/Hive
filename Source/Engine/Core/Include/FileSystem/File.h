// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// File.h: File system file
#pragma once
#include "HAL/HalFileSystem.h"
#include "FilePath.h"
#include "Time/DateTime.h"

namespace Hv::FileSystem {
	
	/**
	 * File properties
	 */
	struct FileProperties
	{
		b8 valid;					/**< True if the file is valid, false otherwise (all values will be default values and won't mean anything)  */
		Path filepath;				/**< File filepath  */
		AccessMode access;			/**< Access mode  */
		sizeT size;					/**< File size, sizeT(-1) when the size could not be retrieved  */
		sizeT filePointer;			/**< Position of the file pointer  */

		DateTime creationTime;		/**< Date and time of creation  */
		DateTime lastAccessTime;	/**< Date and time of last access  */
		DateTime lastEditTime;		/**< Date and time of last edit  */
	};

	/**
	 * File
	 * @note Files that aren't closed, will persist until the end of the application and may possibly prohibit access to the file until then
	 */
	class HIVE_API File
	{
	public:
		/**
		 * Create an uninitializer file
		 */
		File();
		/**
		 * Create a file from a native file handle
		 * @param[in] handle	Native file handle
		 * @param[in] mode		Access mode
		 * @param[in] filepath	Filepath
		 * @note				Used for internal purposes, this ctor is not expected to be used by a user
		 */
		File(FileHandle handle, AccessMode mode, const String& filepath);
		/**
		 * Create a file from a native file handle
		 * @param[in] handle	Native file handle
		 * @param[in] mode		Access mode
		 * @param[in] filepath	Filepath
		 * @note				Used for internal purposes, this ctor is not expected to be used by a user
		 */
		File(FileHandle handle, AccessMode mode, const Path& filepath);
		/**
		 * Create a file from another file
		 * @param[in] file	File
		 */
		File(const File& file);
		/**
		 * Move a file into this file
		 * @param[in] file	File
		 */
		File(File&& file) noexcept;
		~File();

		File& operator=(const File& file);
		File& operator=(File&& file) noexcept;

		/**
		 * Read a line as an ansi string
		 * @return	Ansi string, if no line could be read, an empty string is returned
		 */
		AnsiString GetLineA();
		/**
		 * Read a line as a wide string
		 * @return	Wide string, if no line could be read, an empty string is returned
		 */
		WideString GetLineW();
		/**
		 * Read a bytes from the file
		 * @param[out] pBytes	Buffer to read to
		 * @param[in] size		Number of bytes to read
		 * @param[out] pBytesRead	Amount of values read
		 * @return				True, if the file was able to read, false otherwise
		 */
		b8 ReadBytes(u8* pBytes, sizeT size, sizeT* pBytesRead = nullptr);
		/**
		 * Read a primitive type from the file
		 * @tparam T			Primitive type to read
		 * @param[inout] value	Reference to value
		 * @return				True, if the file was able to read, false otherwise
		 */
		template<typename T>
		b8 Read(T& value);
		/**
		 * Read a primitive type from the file
		 * @tparam T				Primitive type to read
		 * @param[inout] array		Buffer to read to
		 * @param[in] count			Amount of values to read
		 * @param[out] pValuesRead	Amount of values read
		 * @return					True, if the file was able to read, false otherwise
		 */
		template<typename T>
		b8 Read(T* array, sizeT count, sizeT* pValuesRead = nullptr);

		/**
		* Write a line to a file (ANSI)
		* @param line	Line to write
		* @return		True, if the file was able to write, false otherwise
		*/
		b8 WriteLine(const AnsiChar* line);
		/**
		* Write a line to a file (WIDE CHARACTERS)
		* @param line	Line to write
		* @return		True, if the file was able to write, false otherwise
		*/
		b8 WriteLine(const WideChar* line);
		/**
		 * Write a line to a file (ANSI)
		 * @param line	Line to write
		 * @return		True, if the file was able to write, false otherwise
		 */
		b8 WriteLine(const AnsiString& line);
		/**
		 * Write a line to a file (WIDE CHARACTERS)
		 * @param line	Line to write
		 * @return		True, if the file was able to write, false otherwise
		 */
		b8 WriteLine(const WideString& line);
		/**
		 * Write bytes to the file
		 * @tparam T			Primitive type
		 * @param[inout] pBytes	Value to write
		 * @param[in] size		Number of bytes to write
		 * @return				True, if the file was able to write, false otherwise
		 */
		b8 WriteBytes(u8* pBytes, sizeT size);
		/**
		 * Write a primitive type to the file
		 * @tparam T		Primitive type
		 * @param[in] value	Value to write
		 * @return			True, if the file was able to write, false otherwise
		 */
		template<typename T>
		b8 Write(T value);
		/**
		 * Write an array of primitive types to the file
		 * @tparam T		Primitive type
		 * @param[inout] array	Array of values to write
		 * @param[in] count	Amount of values to write
		 * @return			True, if the file was able to write, false otherwise
		 */
		template<typename T>
		b8 Write(T * array, sizeT count);

		/**
		 * Close the file
		 * @return	True if the file closed correctly, false otherwise
		 */
		b8 Close();

		/**
		 * Set the current position of the file pointer
		 * @param[in] pos	Position of the file pointer
		 * @note			The file pointer is the location where the OS reads and writes from and to the file
		 */
		b8 SetPosition(sizeT pos);
		/**
		 * Get the current position of the file pointer
		 * @return	Position of the file pointer, sizeT(-1) if the position could not be retrieved
		 * @note	The file pointer is the location where the OS reads and writes from and to the file
		 */
		sizeT GetPosition() const;
		/**
		 * Move the current position of the file pointer
		 * @param[in] offset	Mount to move the file pointer (positive -> forward, negative -> backwards)
		 */
		b8 MovePosition(i64 offset);
		/**
		 * Set the current position to the begin of the file
		 */
		b8 MoveToBegin();
		/**
		 * Set the current position to the end of the file
		 */
		b8 MoveToEnd();
		/**
		 * Check if the end of the file is reached
		 * @return	True, if the end of the file is reached, false otherwise
		 */
		b8 EoF();

		/**
		 * Check if the file is a valid file
		 * @return	True if the file is valid, false otherwise
		 */
		b8 IsValid() const;
		/**
		 * Get the access mode of the file
		 * @return Access mode
		 */
		AccessMode GetAccessMode() const;
		/**
		 * Get the filepath of the file
		 * @return	Extension of the file
		 */
		const Path& GetFilePath() const;
		/**
		 * Get the size of the file
		 * @return	File size
		 * @note	File size will be sizeT(-1), when the size could not be retrieved
		 */
		sizeT GetFileSize() const;
		/**
		 * Get the date and time of creation
		 * @return	Date and time of creation
		 */
		DateTime GetCreationTime() const;
		/**
		 * Get the date and time of the last access
		 * @return	Date and time of the last access
		 */
		DateTime GetLastAccessTime() const;
		/**
		 * Get the date and time of the last edit
		 * @return	Date and time of the last edit
		 */
		DateTime GetLastEditTime() const;

		/**
		 * Get all properties of the file
		 * @return	File properties
		 */
		FileProperties GetProperties() const;

	private:
		FileHandle m_Handle;	/**< File handle */
		AccessMode m_Access;	/**< Access mode */

#pragma warning(push)
#pragma warning(disable: 4251) // "'m_FilePath': class 'Hv::FileSystem::Path' needs to have dll-interface to be used by clients of class 'Hv::FileSystem::File'" <- Path is inline!
		Path m_FilePath;		/**< File path */
#pragma warning(pop)
	};

}

#include "Inline/File.inl"
