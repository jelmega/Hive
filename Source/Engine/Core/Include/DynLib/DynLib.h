// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DynLib.h: Dynamic library handling
#pragma once
#include "HAL/HalDynLib.h"

namespace Hv::DynLib {
	
	/**
	 * Load a dynamic library
	 * @param[in] path	Path to dynamic library
	 * @return			Handle to dynamic library, invalid handle if library could not be loaded
	 */
	HIVE_API DynLibHandle Load(const String& path);
	/**
	* Unload a dynamic library
	* @param[in] handle	Handle to dynamic library
	*/
	HIVE_API void Unload(DynLibHandle& handle);
	/**
	 * Get the address of a function
	 * @param[in] handle	Handle to dynamic library
	 * @param[in] procName	Name of the function to load
	 */
	HIVE_API DynLibProcHandle GetProc(DynLibHandle& handle, const String& procName);

	/**
	 * Dynamic library extension
	 */
	constexpr const AnsiChar* g_Extension = Hv::HAL::DynLib::g_DynLibExtension;

}
