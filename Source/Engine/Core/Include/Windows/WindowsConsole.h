// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// SystemConsole.h: Windows specifc console types
#pragma once
#include "HAL/HalConsole.h"

namespace Hv::HAL::Console {
	
	struct WindowsConsoleHandle
	{
		HANDLE input = INVALID_HANDLE_VALUE;
		HANDLE output = INVALID_HANDLE_VALUE;
	};

}