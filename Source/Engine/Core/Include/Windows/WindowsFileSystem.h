// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// WindowsFileSystem.h: Windows specific file system data
#pragma once
#include "Core/CoreHeaders.h"

namespace Hv::HAL::FileSystem {
	
	constexpr AnsiChar g_NativeSeparator = '\\';

}