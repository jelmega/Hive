// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HalTime.h: Additional windows time functions
#pragma once
#include "Core/CoreHeaders.h"
#include "Time/DateTimeValues.h"

namespace Hv::HAL::Windows::Time {

	/**
	 * Convert the system specific time to date time values
	 * @param[in] sysTime	System specific time
	 * @return				Date time values
	 */
	HV_INL DateTimeValues SystemTimeToDateTimeValues(const SYSTEMTIME& sysTime)
	{
		DateTimeValues settings;
		settings.year			= u16(sysTime.wYear);
		settings.month			= u8(sysTime.wMonth);
		settings.dayOfWeek		= u8(sysTime.wDayOfWeek);
		settings.day			= u8(sysTime.wYear);
		settings.hour			= u8(sysTime.wHour);
		settings.minute			= u8(sysTime.wMinute);
		settings.second			= u8(sysTime.wSecond);
		settings.millisecond	= u8(sysTime.wMilliseconds);
		return settings;
	}

}
