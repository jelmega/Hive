// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// WindowsThreading.inl: Windows specific threading implementation
#pragma once
#include "HAL/HalThreading.h"
#include <intrin.h>

namespace Hv::HAL::Threading {

	template <typename T>
	T AtomicStore(T& dst, T val)
	{
		if constexpr (sizeof(T) == 1)
		{
			return InterlockedExchange8((volatile char*)&dst, (char)val);
		}
		else if constexpr (sizeof(T) == 2)
		{
			return InterlockedExchange16((volatile short*)&dst, (short)val);
		}
		else if constexpr (sizeof(T) == 4)
		{
			return InterlockedExchange((volatile long*)&dst, (long)val);
		}
		else if constexpr (sizeof(T) == 8)
		{
			return InterlockedExchange64((volatile __int64*)&dst, (__int64)val);
		}
		else
		{
			return 0;
		}
	}

	template <typename T>
	T AtomicLoad(T& val)
	{
		if constexpr (sizeof(T) == 1)
		{
			return InterlockedOr8((volatile char*)&val, 0);
		}
		else if constexpr (sizeof(T) == 2)
		{
			return InterlockedOr16((volatile short*)&val, 0);
		}
		else if constexpr (sizeof(T) == 4)
		{
			return InterlockedOr((volatile long*)&val, 0);
		}
		else if constexpr (sizeof(T) == 8)
		{
			return InterlockedOr64((volatile __int64*)&val, 0);
		}
		else
		{
			return 0;
		}
	}

	template<typename T>
	T AtomicIncrement(T& val)
	{
		if constexpr (sizeof(T) == 1)
		{
			// No _InterlockedIncrement8
			u8 oldVal, newVal;
			do
			{
				oldVal = val;
				newVal = oldVal + 1;
			} while (_InterlockedCompareExchange8((volatile char*)&val, newVal, oldVal) != newVal);
			return newVal;
		}
		else if constexpr (sizeof(T) == 2)
		{
			return InterlockedIncrement16((volatile short*)&val);
		}
		else if constexpr (sizeof(T) == 4)
		{
			return InterlockedIncrement((volatile long*)&val);
		}
		else if constexpr (sizeof(T) == 8)
		{
			return InterlockedIncrement64((volatile __int64*)&val);
		}
		else
		{
			return 0;
		}
	}

	template <typename T>
	T AtomicDecrement(T& val)
	{
		if constexpr (sizeof(T) == 1)
		{
			// No _InterlockedIncrement8
			u8 oldVal, newVal;
			do
			{
				oldVal = val;
				newVal = oldVal - 1;
			} while (_InterlockedCompareExchange8((volatile char*)&val, newVal, oldVal) != newVal);
			return newVal;
		}
		else if constexpr (sizeof(T) == 2)
		{
			return InterlockedDecrement16((volatile short*)&val);
		}
		else if constexpr (sizeof(T) == 4)
		{
			return InterlockedDecrement((volatile long*)&val);
		}
		else if constexpr (sizeof(T) == 8)
		{
			return InterlockedDecrement64((volatile __int64*)&val);
		}
		else
		{
			return 0;
		}
	}

	template <typename T>
	T AtomicAddFetch(T& val, T add)
	{
		if constexpr (sizeof(T) == 1)
		{
			return InterlockedExchangeAdd8((volatile char*)&val, (char)add);
		}
		else if constexpr (sizeof(T) == 2)
		{
			return _InterlockedExchangeAdd16((volatile short*)&val, (short)add);
		}
		else if constexpr (sizeof(T) == 4)
		{
			return InterlockedExchangeAdd((volatile long*)&val, (long)add);
		}
		else if constexpr (sizeof(T) == 8)
		{
			return InterlockedExchangeAdd64((volatile __int64*)&val, (__int64)add);
		}
		else
		{
			return 0;
		}
	}

	template <typename T>
	T AtomicSubFetch(T& val, T sub)
	{
		if constexpr (sizeof(T) == 1)
		{
			// No InterlockedExchangeSubtract for 8-bit
			char oldVal, newVal;
			do {
				oldVal = val;
				newVal = oldVal - sub;
			} while (_InterlockedCompareExchange8((volatile char*)val,
				newVal,
				oldVal) != oldVal);
			return static_cast<T>(newVal);
		}
		else if constexpr (sizeof(T) == 2)
		{
			// No InterlockedExchangeSubtract for 16-bit
			short oldVal, newVal;
			do {
				oldVal = val;
				newVal = oldVal - sub;
			} while (InterlockedCompareExchange16((volatile short*)val,
				newVal,
				oldVal) != oldVal);
			return static_cast<T>(newVal);
		}
		else if constexpr (sizeof(T) == 4)
		{
			return InterlockedExchangeSubtract((volatile unsigned long*)&val, (unsigned long) sub);
		}
		else if constexpr (sizeof(T) == 8)
		{
			return InterlockedExchangeSubtract((volatile unsigned __int64*)&val, (unsigned __int64)sub);
		}
		else
		{
			return 0;
		}
	}

}
