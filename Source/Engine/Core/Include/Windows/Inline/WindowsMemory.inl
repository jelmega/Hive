// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// WindowsMemory.h: Windows memory
#pragma once
#include "HAL/HalMemory.h"

namespace Hv::HAL::Memory {

	HV_FORCE_INL void* Allocate(sizeT size, u16 alignment)
	{
		return _aligned_malloc(size, alignment);
	}

	HV_FORCE_INL void Free(void* ptr)
	{
		_aligned_free(ptr);
	}
	
}