// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// CoreHeaders.h: Core headers include file
#pragma once

// Macros
#include "CoreMacros.h"
#include "BuildMacros.h"
#include "Platform.h"
#include "CompilerMacros.h"

// Types
#include "CoreTypes.h"

// Traits
#include "TypeTraits.h"

// Utility
#include "Core/Assertions.h"
#include "Utility.h"
#include "Pair.h"
#include "Hash.h"
#include "Tuple.h"
#include "Delegate.h"
#include "Endianess.h"
#include "BitUtils.h"
#include "EnumFlags.h"