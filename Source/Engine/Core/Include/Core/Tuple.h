// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Tuple.h: Tuple
#pragma once
#include "CoreTypes.h"
#include "Assertions.h"

namespace Hv {
	
	namespace Detail
	{

	/**
	 * Tuple base
	 */
		template<typename T, typename... Args>
		struct TupleBase : public TupleBase<Args...>
		{
			using BaseType = TupleBase<Args...>;
			static constexpr u8 Count = BaseType::Count + 1;

			TupleBase()
				: BaseType()
			{
			}

			TupleBase(const T& t, const Args&... args)
				: val(t)
				, BaseType(args...)
			{
			}

			T val;
		};

		/**
		 * End tuple base
		 */
		template<typename T>
		struct TupleBase<T>
		{
			static constexpr u8 Count = 1;

			TupleBase()
			{
			}

			TupleBase(const T& t)
				: val(t)
			{
			}

			T val;
		};

		template <sizeT I0, sizeT I1>
		struct TupleConditional
		{
			static constexpr sizeT Value = I0 < I1 ? I0 : I1;
		};

		template<sizeT index, typename T, typename... Args>
		struct TupleGetImpl
		{
			static auto Value(const TupleBase<T, Args...>* t) -> decltype(TupleGetImpl<TupleConditional<TupleBase<Args...>::Count - 1, index - 1>::Value, Args...>::Value(t))
			{
				return TupleGetImpl<TupleConditional<TupleBase<Args...>::Count - 1, index - 1>::Value, Args...>::Value(t);
			}
		};

		template<sizeT index, typename T>
		struct TupleGetImpl<index, T>
		{
			static T Value(const TupleBase<T>* t)
			{
				return t->val;
			}
		};

		template<typename T, typename... Args>
		struct TupleGetImpl<0, T, Args...>
		{
			static T Value(const TupleBase<T, Args...>* t)
			{
				return t->val;
			}
		};
	}

	/**
	 * Tuple
	 * @tparam Args	Argument types
	 */
	template<typename T, typename... Args>
	struct Tuple : Detail::TupleBase<T, Args...>
	{
		using BaseType = Detail::TupleBase<T, Args...>;
		template<sizeT index>
		using GetImpl = Detail::TupleGetImpl<index, T, Args...>;

		static constexpr u8 Count = BaseType::Count;

		Tuple()
			: BaseType()
		{
		}

		Tuple(const T& val, const Args&... args)
			: BaseType(val, args...)
		{
		}

		template<sizeT index>
		auto Get() -> decltype(GetImpl<index>::Value(this))
		{
			return GetImpl<index>::Value(this);
		}
	};


}
