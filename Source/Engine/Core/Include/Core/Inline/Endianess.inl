// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Endianess.inl: Endianess helpers
#pragma once
#include "Core/Endianess.h"
#include "Core/CompilerMacros.h"
#include "Core/TypeTraits.h"

namespace Hv {
	
	template<typename T>
	HV_FORCE_INL T SwitchEndianess(T val)
	{
		HV_STATIC_ASSERT_MSG(Traits::IsArithmaticV<T>, "Only arithmatic type are supported to switch endianness");
		// mask at returns is to fix problems with signs for signed values
		if constexpr (sizeof(T) == 1)
			return val;
		else if constexpr (sizeof(T) == 2)
			return (val << 8) | ((val >> 8) & 0xFF);
		else if constexpr (sizeof(T) == 4)
		{
			val = ((val << 8) & 0xFF00FF00) | ((val >> 8) & 0xFF00FF);
			return (val << 16) | ((val >> 16) & 0xFFFF);
		}
		else
		{
			val = ((val << 8) & 0xFF00FF00FF00FF00ULL) | ((val >> 8) & 0x00FF00FF00FF00FFULL);
			val = ((val << 16) & 0xFFFF0000FFFF0000ULL) | ((val >> 16) & 0x0000FFFF0000FFFFULL);
			return (val << 32) | ((val >> 32) & 0xFFFFFFFFULL);
		}
	}

	template <typename T>
	T SwitchSystemLittleEndian(T val)
	{
#if HV_LITTLE_ENDIAN
		return val;
#else
		return SwitchEndianess(val);
#endif
	}

	template <typename T>
	T SwitchSystemBigEndian(T val)
	{
#if HV_BIG_ENDIAN
		return val;
#else
		return SwitchEndianess(val);
#endif
	}

}
