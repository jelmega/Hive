// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Pair.inl: Pair
#pragma once
#include "Core/Pair.h"
#include "Core/CompilerMacros.h"
#include "Core/Utility.h"

namespace Hv {
	
	template <typename T0, typename T1>
	HV_FORCE_INL Pair<T0, T1>::Pair()
	{
	}

	template <typename T0, typename T1>
	HV_FORCE_INL Pair<T0, T1>::Pair(const T0& val0, const T1& val1)
		: first(val0)
		, second(val1)
	{
	}

	template <typename T0, typename T1>
	HV_FORCE_INL Pair<T0, T1>::Pair(T0&& val0, const T1& val1)
		: first(Move(val0))
		, second(val1)
	{
	}

	template <typename T0, typename T1>
	HV_FORCE_INL Pair<T0, T1>::Pair(const T0& val0, T1&& val1)
		: first(val0)
		, second(Move(val1))
	{
	}

	template <typename T0, typename T1>
	HV_FORCE_INL Pair<T0, T1>::Pair(T0&& val0, T1&& val1)
		: first(Move(val0))
		, second(Move(val1))
	{
	}

	template <typename T0, typename T1>
	HV_FORCE_INL Pair<T0, T1>::Pair(const Pair& pair)
		: first(pair.first)
		, second(pair.second)
	{
	}

	template <typename T0, typename T1>
	HV_FORCE_INL Pair<T0, T1>::Pair(Pair&& pair) noexcept
		: first(Move(pair.first))
		, second(Move(pair.second))
	{
	}

	template <typename T0, typename T1>
	HV_FORCE_INL Pair<T0, T1>::~Pair()
	{
	}

	template <typename T0, typename T1>
	HV_FORCE_INL Pair<T0, T1>& Pair<T0, T1>::operator=(const Pair& pair)
	{
		first = pair.first;
		second = pair.second;
		return *this;
	}

	template <typename T0, typename T1>
	HV_FORCE_INL Pair<T0, T1>& Pair<T0, T1>::operator=(Pair&& pair) noexcept
	{
		first = Move(pair.first);
		second = Move(pair.second);
		return *this;
	}

	template <typename T0, typename T1>
	HV_FORCE_INL void Pair<T0, T1>::Swap(Pair& pair)
	{
		Swap(first, pair.first);
		Swap(second, pair.second);
	}

	template <typename T0, typename T1>
	HV_FORCE_INL Pair<T0, T1> MakePair(const T0& val0, T1& val1)
	{
		return Pair<T0, T1>(val0, val1);
	}

	template <typename T0, typename T1>
	HV_FORCE_INL Pair<T0, T1> MakePair(T0&& val0, const T1& val1)
	{
		return Pair<T0, T1>(Move(val0), val1);
	}

	template <typename T0, typename T1>
	HV_FORCE_INL Pair<T0, T1> MakePair(const T0& val0, T1&& val1)
	{
		return Pair<T0, T1>(val0, Move(val1));
	}

	template <typename T0, typename T1>
	HV_FORCE_INL Pair<T0, T1> MakePair(T0&& val0, T1&& val1)
	{
		return Pair<T0, T1>(Move(val0), Move(val1));
	}

}
