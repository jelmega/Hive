// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// BitUtils.inl: Bit maipulation utilities
#pragma once
#include "Core/BitUtils.h"
#include "Core/CoreMacros.h"

namespace Hv {

	HV_INL u32 ReverseBits(u32 val, u8 numBits)
	{
		if (numBits == 0)
			return 0;
		if (numBits == 8)
			return Detail::ReversedBits[val & 0xFF];
		if (numBits < 8)
			return Detail::ReversedBits[val & 0xFF] >> (8 - numBits);

		// Larger than 8, slower
		u32 res = 0;
		for (u8 i = 0; i < numBits; ++i)
		{
			res <<= 1;
			res |= val & 0x01;
			val >>= 1;
		}
		return res;
	}

}
