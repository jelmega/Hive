// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// TypeTraits.h: Type traits wrapper
#pragma once
// types
#include "CoreTypes.h"
// std type traits
#define	 _SILENCE_CXX17_RESULT_OF_DEPRECATION_WARNING //Silence result_of deprication warning for c++17
#include <type_traits>

namespace Hv::Traits {
	
	// Primitive type categories
	template<typename T>
	struct IsVoid { static constexpr b8 Value = std::is_void_v<T>; };
	template<typename T>
	constexpr b8 IsVoidV = IsVoid<T>::Value;

	template<typename T> 
	struct IsNullPointer { static constexpr b8 Value = std::is_null_pointer_v<T>; };
	template<typename T>
	constexpr b8 IsNullPointerV = IsNullPointer<T>::Value;

	template<typename T>
	struct IsIntegral { static constexpr b8 Value = std::is_integral_v<T>; };
	template<typename T>
	constexpr b8 IsIntegralV = IsIntegral<T>::Value;

	template<typename T>
	struct IsFloatingPoint { static constexpr b8 Value = std::is_floating_point_v<T>; };
	template<typename T>
	constexpr b8 IsFloatingPointV = IsFloatingPoint<T>::Value;

	template<typename T>
	struct IsArray { static constexpr b8 Value = std::is_array_v<T>; };
	template<typename T>
	constexpr b8 IsArrayV = IsArray<T>::Value;

	template<typename T>
	struct IsEnum { static constexpr b8 Value = std::is_enum_v<T>; };
	template<typename T>
	constexpr b8 IsEnumV = IsEnum<T>::Value;

	template<typename T>
	struct IsUnion { static constexpr b8 Value = std::is_union_v<T>; };
	template<typename T>
	constexpr b8 IsUnionV = IsUnion<T>::Value;

	template<typename T>
	struct IsClass { static constexpr b8 Value = std::is_class_v<T>; };
	template<typename T>
	constexpr b8 IsClassV = IsClass<T>::Value;

	template<typename T>
	struct IsFunction { static constexpr b8 Value = std::is_function_v<T>; };
	template<typename T>
	constexpr b8 IsFunctionV = IsFunction<T>::Value;

	template<typename T>
	struct IsPointer { static constexpr b8 Value = std::is_pointer_v<T>; };
	template<typename T>
	constexpr b8 IsPointerV = IsPointer<T>::Value;

	template<typename T>
	struct IsLvalueReference { static constexpr b8 Value = std::is_lvalue_reference_v<T>; };
	template<typename T>
	constexpr b8 IsLvalueReferenceV = IsLvalueReference<T>::Value;

	template<typename T>
	struct IsRvalueReference { static constexpr b8 Value = std::is_rvalue_reference_v<T>; };
	template<typename T>
	constexpr b8 IsRvalueReferenceV = IsRvalueReference<T>::Value;

	template<typename T>
	struct IsMemberObjectPointer { static constexpr b8 Value = std::is_member_object_pointer_v<T>; };
	template<typename T>
	constexpr b8 IsMemberObjectPointerV = IsMemberObjectPointer<T>::Value;

	template<typename T>
	struct IsMemberFunctionPointer { static constexpr b8 Value = std::is_member_object_pointer_v<T>; };
	template<typename T>
	constexpr b8 IsMemberFunctionPointerV = IsMemberFunctionPointer<T>::Value;

	// Composite type categories
	template<typename T>
	struct IsFundamental { static constexpr b8 Value = std::is_fundamental_v<T>; };
	template<typename T>
	constexpr b8 IsFundamentalV = IsFundamental<T>::Value;

	template<typename T>
	struct IsArithmatic { static constexpr b8 Value = std::is_arithmetic_v<T>; };
	template<typename T>
	constexpr b8 IsArithmaticV = IsArithmatic<T>::Value;

	template<typename T>
	struct IsScalar { static constexpr b8 Value = std::is_scalar_v<T>; };
	template<typename T>
	constexpr b8 IsScalarV = IsScalar<T>::Value;

	template<typename T>
	struct IsObject { static constexpr b8 Value = std::is_object_v<T>; };
	template<typename T>
	constexpr b8 IsObjectV = IsObject<T>::Value;

	template<typename T>
	struct IsCompound { static constexpr b8 Value = std::is_compound_v<T>; };
	template<typename T>
	constexpr b8 IsCompoundV = IsCompound<T>::Value;

	template<typename T>
	struct IsReference { static constexpr b8 Value = std::is_fundamental_v<T>; };
	template<typename T>
	constexpr b8 IsReferenceV = IsReference<T>::Value;

	template<typename T>
	struct IsMemberPointer { static constexpr b8 Value = std::is_member_pointer_v<T>; };
	template<typename T>
	constexpr b8 IsMemberPointerV = IsMemberPointer<T>::Value;

	// type properties
	template<typename T>
	struct IsConst { static constexpr b8 Value = std::is_const_v<T>; };
	template<typename T>
	constexpr b8 IsConstV = IsConst<T>::Value;

	template<typename T>
	struct IsVolatile { static constexpr b8 Value = std::is_volatile_v<T>; };
	template<typename T>
	constexpr b8 IsVolatileV = IsVolatile<T>::Value;

	template<typename T>
	struct IsTrivial { static constexpr b8 Value = std::is_trivial_v<T>; };
	template<typename T>
	constexpr b8 IsTrivialV = IsTrivial<T>::Value;

	template<typename T>
	struct IsTriviallyCopyable { static constexpr b8 Value = std::is_trivially_copyable_v<T>; };
	template<typename T>
	constexpr b8 IsTriviallyCopyableV = IsTriviallyCopyable<T>::Value;

	template<typename T>
	struct IsStandardLayout { static constexpr b8 Value = std::is_standard_layout_v<T>; };
	template<typename T>
	constexpr b8 IsStandardLayoutV = IsStandardLayout<T>::Value;

	// is_pod, depricated in c++20

	// is_literal_type, depricated in c++17

	// has_uniue_object_representations, added in c++17, support unknown

	template<typename T>
	struct IsEmpty { static constexpr b8 Value = std::is_empty_v<T>; };
	template<typename T>
	constexpr b8 IsEmptyV = IsEmpty<T>::Value;

	template<typename T>
	struct IsPolymorphic { static constexpr b8 Value = std::is_polymorphic_v<T>; };
	template<typename T>
	constexpr b8 IsPolymorphicV = IsPolymorphic<T>::Value;

	template<typename T>
	struct IsAbstract { static constexpr b8 Value = std::is_abstract_v<T>; };
	template<typename T>
	constexpr b8 IsAbstractV = IsAbstract<T>::Value;

	template<typename T>
	struct IsFinal { static constexpr b8 Value = std::is_final_v<T>; };
	template<typename T>
	constexpr b8 IsFinalV = IsFinal<T>::Value;

	// is_aggregate_v, added in c++17, support unknown

	template<typename T>
	struct IsSigned { static constexpr b8 Value = std::is_signed_v<T>; };
	template<typename T>
	constexpr b8 IsSignedV = IsSigned<T>::Value;

	template<typename T>
	struct IsUnsigned { static constexpr b8 Value = std::is_unsigned_v<T>; };
	template<typename T>
	constexpr b8 IsUnsignedV = IsUnsigned<T>::Value;

	// Supported operations
	template<typename T>
	struct IsConstructable { static constexpr b8 Value = std::is_constructible_v<T>; };
	template<typename T>
	constexpr b8 IsConstructableV = IsConstructable<T>::Value;

	template<typename T>
	struct IsTriviallyConstructable { static constexpr b8 Value = std::is_trivially_constructible_v<T>; };
	template<typename T>
	constexpr b8 IsTriviallyConstructableV = IsTriviallyConstructable<T>::Value;

	template<typename T>
	struct IsNothrowConstructable { static constexpr b8 Value = std::is_nothrow_constructible_v<T>; };
	template<typename T>
	constexpr b8 IsNothrowConstructableV = IsNothrowConstructable<T>::Value;

	template<typename T>
	struct IsDefaultConstructable { static constexpr b8 Value = std::is_default_constructible_v<T>; };
	template<typename T>
	constexpr b8 IsDefaultConstructableV = IsDefaultConstructable<T>::Value;

	template<typename T>
	struct IsTriviallyDefaultConstructable { static constexpr b8 Value = std::is_trivially_default_constructible_v<T>; };
	template<typename T>
	constexpr b8 IsTriviallyDefaultConstructableV = IsTriviallyDefaultConstructable<T>::Value;

	template<typename T>
	struct IsNothrowDefaultConstructable { static constexpr b8 Value = std::is_nothrow_default_constructible_v<T>; };
	template<typename T>
	constexpr b8 IsNothrowDefaultConstructableV = IsNothrowDefaultConstructable<T>::Value;

	template<typename T>
	struct IsCopyConstructable { static constexpr b8 Value = std::is_copy_constructible_v<T>; };
	template<typename T>
	constexpr b8 IsCopyConstructableV = IsCopyConstructable<T>::Value;

	template<typename T>
	struct IsTriviallyCopyConstructable { static constexpr b8 Value = std::is_trivially_copy_constructible_v<T>; };
	template<typename T>
	constexpr b8 IsTriviallyCopyConstructableV = IsTriviallyCopyConstructable<T>::Value;

	template<typename T>
	struct IsNothrowCopyConstructable { static constexpr b8 Value = std::is_nothrow_copy_constructible_v<T>; };
	template<typename T>
	constexpr b8 IsNothrowCopyConstructableV = IsNothrowCopyConstructable<T>::Value;

	template<typename T>
	struct IsMoveConstructable { static constexpr b8 Value = std::is_move_constructible_v<T>; };
	template<typename T>
	constexpr b8 IsMoveConstructableV = IsMoveConstructable<T>::Value;

	template<typename T>
	struct IsTriviallyMoveConstructable { static constexpr b8 Value = std::is_trivially_move_constructible_v<T>; };
	template<typename T>
	constexpr b8 IsTriviallyMoveConstructableV = IsTriviallyMoveConstructable<T>::Value;

	template<typename T>
	struct IsNothrowMoveConstructable { static constexpr b8 Value = std::is_nothrow_move_constructible_v<T>; };
	template<typename T>
	constexpr b8 IsNothrowMoveConstructableV = IsNothrowMoveConstructable<T>::Value;

	template<typename T>
	struct IsAssignable { static constexpr b8 Value = std::is_assignable_v<T>; };
	template<typename T>
	constexpr b8 IsAssignableV = IsAssignable<T>::Value;

	template<typename T>
	struct IsTriviallyAssignable { static constexpr b8 Value = std::is_trivially_assignable_v<T>; };
	template<typename T>
	constexpr b8 IsTriviallyAssignableV = IsTriviallyAssignable<T>::Value;

	template<typename T>
	struct IsNothrowAssignable { static constexpr b8 Value = std::is_nothrow_assignable_v<T>; };
	template<typename T>
	constexpr b8 IsNothrowAssignableV = IsNothrowAssignable<T>::Value;

	template<typename T>
	struct IsCopyAssignable { static constexpr b8 Value = std::is_copy_assignable_v<T>; };
	template<typename T>
	constexpr b8 IsCopyAssignableV = IsCopyAssignable<T>::Value;

	template<typename T>
	struct IsTriviallyCopyAssignable { static constexpr b8 Value = std::is_trivially_copy_assignable_v<T>; };
	template<typename T>
	constexpr b8 IsTriviallyCopyAssignableV = IsTriviallyCopyAssignable<T>::Value;

	template<typename T>
	struct IsNothrowCopyAssignable { static constexpr b8 Value = std::is_nothrow_copy_assignable_v<T>; };
	template<typename T>
	constexpr b8 IsNothrowCopyAssignableV = IsNothrowCopyAssignable<T>::Value;

	template<typename T>
	struct IsMoveAssignable { static constexpr b8 Value = std::is_move_assignable_v<T>; };
	template<typename T>
	constexpr b8 IsMoveAssignableV = IsMoveAssignable<T>::Value;

	template<typename T>
	struct IsTriviallyMoveAssignable { static constexpr b8 Value = std::is_trivially_move_assignable_v<T>; };
	template<typename T>
	constexpr b8 IsTriviallyMoveAssignableV = IsTriviallyMoveAssignable<T>::Value;

	template<typename T>
	struct IsNothrowMoveAssignable { static constexpr b8 Value = std::is_nothrow_move_assignable_v<T>; };
	template<typename T>
	constexpr b8 IsNothrowMoveAssignableV = IsNothrowMoveAssignable<T>::Value;

	template<typename T>
	struct IsDestructable { static constexpr b8 Value = std::is_destructible_v<T>; };
	template<typename T>
	constexpr b8 IsDestructableV = IsDestructable<T>::Value;

	template<typename T>
	struct IsTriviallyDestructable { static constexpr b8 Value = std::is_trivially_destructible_v<T>; };
	template<typename T>
	constexpr b8 IsTriviallyDestructableV = IsTriviallyDestructable<T>::Value;

		template<typename T>
	struct IsNothrowDestructable { static constexpr b8 Value = std::is_nothrow_destructible_v<T>; };
	template<typename T>
	constexpr b8 IsNothrowDestructableV = IsDestructable<T>::Value;

	template<typename T>
	struct HasVirtualDestructor { static constexpr b8 Value = std::has_virtual_destructor_v<T>; };
	template<typename T>
	constexpr b8 HasVirtualDestructorV = HasVirtualDestructor<T>::Value;

	// is_swappable_with, added in c++17, support unknown
	// is_swappable, added in c++17, support unknown
	// is_nothrow_swappable_with, added in c++17, support unknown
	// is_nothrow_swappable, added in c++17, support unknown

	// Property queries
	template<typename T>
	struct AlignmentOf { static constexpr sizeT Value = std::alignment_of_v<T>; };
	template<typename T>
	constexpr sizeT AlignmentOfV = AlignmentOf<T>::Value;

	template<typename T>
	struct Rank { static constexpr sizeT Value = std::rank_v<T>; };
	template<typename T>
	constexpr sizeT RankV = Rank<T>::Value;

	template<typename T>
	struct Extent { static constexpr sizeT Value = std::extent_v<T>; };
	template<typename T>
	constexpr sizeT ExtentV = AlignmentOf<T>::Value;

	// Type relationships
	template<typename A, typename B>
	struct IsSame { static constexpr b8 Value = std::is_same_v<A, B>; };
	template<typename A, typename B>
	constexpr b8 IsSameV = IsSame<A, B>::Value;

	template<typename Base, typename Derived>
	struct IsBaseOf { static constexpr b8 Value = std::is_base_of_v<Base, Derived>; };
	template<typename Base, typename Derived>
	constexpr b8 IsBaseOfV = IsBaseOf<Base, Derived>::Value;

	template<typename From, typename To>
	struct IsConvertable { static constexpr b8 Value = std::is_convertible_v<From, To>; };
	template<typename From, typename To>
	constexpr b8 IsConvertableV = IsConvertable<From, To>::Value;

	// is_invocable, added in c++17, support unknown
	// is_invocable_r, added in c++17, support unknown
	// is_nothrow_invocable, added in c++17, support unknown
	// is_nothrow_invocable_r, added in c++17, support unknown

	// Const-volatility specifiers
	template<typename T>
	struct RemoveCV { using Type = std::remove_cv_t<T>; };
	template<typename T>
	using RemoveCVT = typename RemoveCV<T>::Type;

	template<typename T>
	struct RemoveConst { using Type = std::remove_const_t<T>; };
	template<typename T>
	using RemoveConstT = typename RemoveConst<T>::Type;

	template<typename T>
	struct RemoveVolatile { using Type = std::remove_volatile_t<T>; };
	template<typename T>
	using RemoveVolatileT = typename RemoveVolatile<T>::Type;

	// References
	template<typename T>
	struct RemoveReference { using Type = std::remove_reference_t<T>; };
	template<typename T>
	using RemoveReferenceT = typename RemoveReference<T>::Type;

	template<typename T>
	struct AddLvalueReference { using Type = std::add_lvalue_reference_t<T>; };
	template<typename T>
	using AddLvalueReferenceT = typename AddLvalueReference<T>::Type;

	template<typename T>
	struct AddRvalueReference { using Type = std::add_rvalue_reference_t<T>; };
	template<typename T>
	using AddRvalueReferenceT = typename AddRvalueReference<T>::Type;

	// Pointers
	template<typename T>
	struct RemovePointer { using Type = std::remove_pointer_t<T>; };
	template<typename T>
	using RemovePointerT = typename RemovePointer<T>::Type;

	template<typename T>
	struct AddPointer { using Type = std::add_pointer_t<T>; };
	template<typename T>
	using AddPointerT = typename AddPointer<T>::Type;

	// Sign modifiers
	template<typename T>
	struct MakeSigned { using Type = std::make_signed_t<T>; };
	template<typename T>
	using MakeSignedT = typename MakeSigned<T>::Type;

	template<typename T>
	struct MakeUnsigned { using Type = std::make_unsigned_t<T>; };
	template<typename T>
	using MakeUnsignedT = typename MakeUnsigned<T>::Type;

	// Arrays
	template<typename T>
	struct RemoveExtent { using Type = std::remove_extent_t<T>; };
	template<typename T>
	using RemoveExtentT = typename RemoveExtent<T>::Type;

	template<typename T>
	struct RemoveAllExtents { using Type = std::remove_all_extents_t<T>; };
	template<typename T>
	using RemoveAllExtentsT = typename RemoveAllExtents<T>::Type;

	// Miscellaneous transformations
	template<sizeT Length, sizeT Align = alignof(max_align_t)>
	struct AlignedStorage { using Type = std::aligned_storage_t<Length, Align>; };
	template<sizeT Length, sizeT Align = alignof(max_align_t)>
	using AlignedStorageT = typename AlignedStorage<Length, Align>::Type;

	template<sizeT Length, typename... Types>
	struct AlignedUnion { using Type = std::aligned_union_t<Length, Types...>; };
	template<sizeT Length, typename... Types>
	using AlignedUnionT = typename AlignedUnion<Length, Types...>::Type;

	template<typename T>
	struct Decay { using Type = std::decay_t<T>; };
	template<typename T>
	using DecayT = typename Decay<T>::Type;

	// remove_cvref, added in c++20, support unknown

	template<b8 Test, typename T = void>
	struct EnableIf {  };
	template<typename T>
	struct EnableIf<true, T> { using Type = T; };
	template<b8 Test, typename T = void>
	using EnableIfT = typename EnableIf<Test, T>::Type;

	template<b8 Test, typename A, typename B>
	struct Conditional { using Type = std::conditional_t<Test, A, B>; };
	template<b8 Test, typename A, typename B>
	using ConditionalT = typename Conditional<Test, A, B>::Type;

	template<typename... Types>
	struct CommonType { using Type = std::common_type_t<Types...>; };
	template<typename... Types>
	using CommonTypeT = typename CommonType<Types...>::Type;

	template<typename T>
	struct UnderlyingType { using Type = std::underlying_type_t<T>; };
	template<typename T>
	using UnderlyingTypeT = typename UnderlyingType<T>::Type;

	// result_of, depricated in c++17, but used instead of
	// invoke_result, added in c++17, support unknown

	template<typename F, typename... ArgTypes>
	struct InvokeResult { using Type = std::result_of<F(ArgTypes...)>; };
	template<typename F, typename... ArgTypes>
	using InvokeResultT = typename InvokeResult<F, ArgTypes...>::Type;

	template<typename...>
	using VoidT = void;

	// Operations on traits
	// conjugation, added in c++17, support unknown
	// disjunction, added in c++17, support unknown
	// negation, added in c++17, support unknown

	// Endian
	// endian, added in c++20, support unknown
}
