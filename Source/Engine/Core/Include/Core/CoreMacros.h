// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// CoreMacros.h: Core macros
#pragma once

#include "Platform.h"
#include "CompilerMacros.h"

/** Inline macros */
#define HV_INL inline

/** Align macro */
#define HV_ALIGN(x) alignas(x)

/** Unreferenced parameter */
#define HV_UNREFERENCED_PARAM(x) (void)x


/** Disable copying of a class */
#define HV_MAKE_NON_COPYABLE(name)\
	name(const name&) = delete;\
	name& operator=(const name&) = delete;

/** Template argument helper */
#define HV_TARGS(...) __VA_ARGS__

/** Template type helper */
#define HV_TTYPE(type, ...) type<__VA_ARGS__>

/** Stringify text to an ansi literal */
#define HV_STRINGIFY(text) #text

/** Stringify text to a wide literal */
#define HV_WSTRINGIFY(text) L ## #text

/** Convert an ansi literal to a wide literal */
#define WTEXT(text) L ## text

/** Create a handle */
#define HV_CREATE_HANDLE(name) typedef struct name##T* name
/** Create an invalid handle */
#define HV_CREATE_INVALID_HANDLE(name, val) const name Invalid##name = (name)val

#ifndef HV_EXPORT
#	define HV_EXPORT 0
#endif

// DLL
#if HV_PLATFORM_WINDOWS
#	if HV_COMPILER_MSVC
#		if HV_EXPORT
#			define HIVE_API __declspec(dllexport)
#		else
#			define HIVE_API __declspec(dllimport)
#		endif
#	else
#		if HV_EXPORT
#			define HIVE_API __attribute__((dllexport))
#		else
#			define HIVE_API __attribute__((dllimport))
#		endif
#	endif
#else
#	define HIVE_API
#endif