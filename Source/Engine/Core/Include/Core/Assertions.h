// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Assertions.h: Assertion macors
#pragma once

#include "BuildMacros.h"
#include "Platform.h"
#include "CompilerMacros.h"
#include "CoreTypes.h"
#include "HAL/HalSystem.h"
#include <iostream>

namespace Hv::Assertion {
	
	/**
	 * Assert when a condition fails
	 * @param[in] expr		Expression to test
	 * @param[in] exprStr	Expression as a string
	 * @param[in] file		File
	 * @param[in] line		Line
	 * @note				If MSVC is used, message will also be outputed to the Visual Studio output
	 */
	HV_FORCE_INL void Assert(b8 expr, const AnsiChar* exprStr, const AnsiChar* file, u32 line)
	{
		if (!expr)
		{
			std::cerr << "Assertion failed: " << exprStr << ", at: " << file << " : " << line << '\n';

			if (HAL::System::IsDebuggerAttached())
			{
				AnsiChar buf[512] = {};
				snprintf(buf, 512, "Assertion failed: %s, at: %s : %u \n", exprStr, file, line);
				HAL::System::LogDebugger(buf);
			}
			HV_DEBUG_BREAK;
		}
	}

	/**
	 * Assert with a message when a condition fails
	 * @param[in] expr		Expression to test
	 * @param[in] exprStr	Expression as a string
	 * @param[ni] msg		Assert message
	 * @param[in] file		File
	 * @param[in] line		Line
	 * @note				If MSVC is used, message will also be outputed to the Visual Studio output
	 */
	HV_FORCE_INL void AssertMessage(b8 expr, const AnsiChar* exprStr, const AnsiChar* msg, const AnsiChar* file, u32 line)
	{
		if (!expr)
		{
			std::cerr << "Assertion failed: " << exprStr << ", msg: " << msg << ", at: " << file << " : " << line << '\n';

			if (HAL::System::IsDebuggerAttached())
			{
				AnsiChar buf[512] = {};
				snprintf(buf, 512, "Assertion failed: %s, at: %s : %u \n", exprStr, file, line);
				HAL::System::LogDebugger(buf);
			}
			HV_DEBUG_BREAK;
		}
	}

}

#if HV_USE_ASSERT
	/**
	 * Assert on a condition
	 * @param[in] expr	Expression to test
	 */
#	define HV_ASSERT(expr) Hv::Assertion::Assert(expr, #expr, __FILE__, __LINE__)
	/**
	 * Assert with a message on a condition
	 * @param[in] expr	Expression to test
	 * @param[in] msg	Assert message
	 */
#	define HV_ASSERT_MSG(expr, msg) Hv::Assertion::AssertMessage(expr, #expr, msg, __FILE__, __LINE__)
	/**
	 * Code which should only be executed when assertions are enabled
	 */
#	define HV_ASSERT_CODE(x) x
#else
	/**
	 * Assert on a condition
	 * @param[in] expr	Expression to test
	 */
#	define HV_ASSERT(expr)
	/**
	 * Assert with a message on a condition
	 * @param[in] expr	Expression to test
	 * @param[in] msg	Assert message
	 */
#	define HV_ASSERT_MSG(expr, msg)
	 /**
	  * Code which should only be executed when assertions are enabled
	  */
#	define HV_ASSERT_CODE(x)
#endif

#if HV_USE_SLOW_ASSERT
	/**
	 * Assert on a condition
	 * @param[in] expr	Expression to test
	 * @note			Slow assert, use for frequent assert or for slow expression
	 */
#	define HV_SLOW_ASSERT(expr) Hv::Assertion::Assert(expr, #expr, __FILE__, __LINE__)
	/**
	 * Assert with a message on a condition
	 * @param[in] expr	Expression to test
	 * @param[in] msg	Assert message
	 * @note			Slow assert, use for frequent assert or for slow expression
	 */
#	define HV_SLOW_ASSERT_MSG(expr, msg) Hv::Assertion::AssertMessage(expr, #expr, msg, __FILE__, __LINE__)
	 /**
	 * Code which should only be executed when assertions are enabled
	 */
#	define HV_SLOW_ASSERT_CODE(x) x
#else
	/**
	 * Assert on a condition
	 * @param[in] expr	Expression to test
	 * @note			Slow assert, use for frequent assert or for slow expression
	 */
#	define HV_SLOW_ASSERT(expr)
	/**
	 * Assert with a message on a condition
	 * @param[in] expr	Expression to test
	 * @param[in] msg	Assert message
	 * @note			Slow assert, use for frequent assert or for slow expression
	 */
#	define HV_SLOW_ASSERT_MSG(expr, msg)
	 /**
	 * Code which should only be executed when assertions are enabled
	 */
#	define HV_SLOW_ASSERT_CODE(x)
#endif

/**
 * Staticly assert a compile-time expression
 * @param[in] expr	Expression to test
 */
#define HV_STATIC_ASSERT(expr) static_assert(expr, #expr)
/**
 * Staticly assert a compile-time expression
 * @param[in] expr	Expression to test
 * @param[in] msg	Assert message
 */
#define HV_STATIC_ASSERT_MSG(expr, msg) static_assert(expr, msg)
