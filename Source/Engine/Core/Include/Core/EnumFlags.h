// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// EnumFlags.h: Defines for enum class flags
#pragma once
#include "CoreTypes.h"

namespace Hv::Utility {

	template<sizeT Size>
	struct EnumSizeUIntType
	{
		using type = u64;
	};
	template<>
	struct EnumSizeUIntType<4>
	{
		using type = u32;
	};
	template<>
	struct EnumSizeUIntType<2>
	{
		using type = u16;
	};
	template<>
	struct EnumSizeUIntType<1>
	{
		using type = u8;
	};

	template<typename T>
	struct EnumUIntType
	{
		using type = typename EnumSizeUIntType<sizeof(T)>::type;
	};
}

#define HV_ENABLE_ENUM_FLAG_OPERATORS(Enum)\
	HV_INL Enum operator~(Enum e) { using UnderlyingType = typename Hv::Utility::EnumUIntType<Enum>::type; return Enum(~UnderlyingType(e)); }\
	HV_INL Enum operator|(Enum e0, Enum e1) { using UnderlyingType = typename Hv::Utility::EnumUIntType<Enum>::type; return Enum(UnderlyingType(e0) | UnderlyingType(e1)); }\
	HV_INL Enum operator&(Enum e0, Enum e1) { using UnderlyingType = typename Hv::Utility::EnumUIntType<Enum>::type; return Enum(UnderlyingType(e0) & UnderlyingType(e1)); }\
	HV_INL Enum operator^(Enum e0, Enum e1) { using UnderlyingType = typename Hv::Utility::EnumUIntType<Enum>::type; return Enum(UnderlyingType(e0) ^ UnderlyingType(e1)); }\
	HV_INL Enum& operator|=(Enum& e0, Enum e1) { using UnderlyingType = typename Hv::Utility::EnumUIntType<Enum>::type; e0 = Enum(UnderlyingType(e0) | UnderlyingType(e1)); return e0; }\
	HV_INL Enum& operator&=(Enum& e0, Enum e1) { using UnderlyingType = typename Hv::Utility::EnumUIntType<Enum>::type; e0 = Enum(UnderlyingType(e0) & UnderlyingType(e1)); return e0; }\
	HV_INL Enum& operator^=(Enum& e0, Enum e1) { using UnderlyingType = typename Hv::Utility::EnumUIntType<Enum>::type; e0 = Enum(UnderlyingType(e0) ^ UnderlyingType(e1)); return e0; }

#define HV_ENABLE_DUAL_ENUM_FLAG_OPERATORS(Enum0, Enum1)\
	HV_INL Enum0 operator|(Enum0 e0, Enum1 e1) { using UnderlyingType = typename Hv::Utility::EnumUIntType<Enum0>::type; return Enum0(UnderlyingType(e0) | UnderlyingType(e1)); }\
	HV_INL Enum0 operator&(Enum0 e0, Enum1 e1) { using UnderlyingType = typename Hv::Utility::EnumUIntType<Enum0>::type; return Enum0(UnderlyingType(e0) & UnderlyingType(e1)); }\
	HV_INL Enum0 operator^(Enum0 e0, Enum1 e1) { using UnderlyingType = typename Hv::Utility::EnumUIntType<Enum0>::type; return Enum0(UnderlyingType(e0) ^ UnderlyingType(e1)); }\
	HV_INL Enum0& operator|=(Enum0& e0, Enum1 e1) { using UnderlyingType = typename Hv::Utility::EnumUIntType<Enum0>::type; e0 = Enum0(UnderlyingType(e0) | UnderlyingType(e1)); return e0; }\
	HV_INL Enum0& operator&=(Enum0& e0, Enum1 e1) { using UnderlyingType = typename Hv::Utility::EnumUIntType<Enum0>::type; e0 = Enum0(UnderlyingType(e0) & UnderlyingType(e1)); return e0; }\
	HV_INL Enum0& operator^=(Enum0& e0, Enum1 e1) { using UnderlyingType = typename Hv::Utility::EnumUIntType<Enum0>::type; e0 = Enum0(UnderlyingType(e0) ^ UnderlyingType(e1)); return e0; }

#define HV_IS_ENUM_FLAG_SET(flag, mask) ((flag & mask) == mask)