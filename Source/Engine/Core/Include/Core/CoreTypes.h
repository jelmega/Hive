// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// CoreTypes.h: Primitive types
#pragma once

using nullptrT = decltype(nullptr);

template<typename...>
using voidT = void;

template<typename T32, typename T64, int Size>
struct SelectPtrType
{
	using Type = nullptrT;
};

template<typename T32, typename T64>
struct SelectPtrType<T32, T64, 4>
{
	using Type = T32;
};

template<typename T32, typename T64>
struct SelectPtrType<T32, T64, 8>
{
	using Type = T64;
};

using b8  = bool;

using i8  = char;
using i16 = short;
using i32 = int;
using i64 = long long;

using u8  = unsigned char;
using u16 = unsigned short;
using u32 = unsigned int;
using u64 = unsigned long long;

using f32 = float;
using f64 = double;

using AnsiChar = char;
using WideChar = wchar_t;

using intPtrT = SelectPtrType<i32, i64, sizeof(void*)>::Type;
using isizeT = intPtrT;

using uintPtrT = SelectPtrType<u32, u64, sizeof(void*)>::Type;
using sizeT = uintPtrT;

namespace TypeChecks {
	
	template<typename A, typename B>
	struct AreEqual
	{
		static constexpr bool Value = false;
	};

	template<typename T>
	struct AreEqual<T, T>
	{
		static constexpr bool Value = true;
	};

	static_assert(sizeof(b8) == 1, "i8 should be 1 byte");

	static_assert(sizeof(i8)  == 1, "i8 should be 1 byte");
	static_assert(sizeof(i16) == 2, "i16 should be 2 bytes");
	static_assert(sizeof(i32) == 4, "i32 should be 4 bytes");
	static_assert(sizeof(i64) == 8, "i64 should be 8 bytes");

	static_assert(sizeof(i8)  == 1, "u8 should be 1 byte");
	static_assert(sizeof(i16) == 2, "u16 should be 2 bytes");
	static_assert(sizeof(i32) == 4, "u32 should be 4 bytes");
	static_assert(sizeof(i64) == 8, "u64 should be 8 bytes");

	static_assert(sizeof(f32) == 4, "f32 should be 4 bytes");
	static_assert(sizeof(f64) == 8, "f64 should be 8 bytes");

	static_assert(!AreEqual<AnsiChar, WideChar>::Value, "AnsiChar and WideChar should be different types");

	static_assert(!AreEqual<intPtrT, nullptrT>::Value, "intPtrT shouldn't be be of type nullptrT");
	static_assert(!AreEqual<uintPtrT, nullptrT>::Value, "AnsiChar shouldn't be be of type nullptrT");
	static_assert(!AreEqual<intPtrT, uintPtrT>::Value, "AnsiChar and WideChar should be different types");
}
