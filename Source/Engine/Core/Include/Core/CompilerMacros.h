// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// CompilerMacros.h: Compiler macros
#pragma once

// Auto compiler detection
#if defined(_MSC_VER)
#	define HV_COMPILER_MSVC 1
#elif defined(__GNUC__)
#	define HV_COMPILER_GCC 1
#elif defined (__clang__)
#	define HV_COMPILER_CLANG 1
#endif

#ifndef HV_COMPILER_MSVC
#	define HV_COMPILER_MSVC 0
#endif

#ifndef HV_COMPILER_GCC
#	define HV_COMPILER_GCC 0
#endif

#ifndef HV_COMPILER_CLANG
#	define HV_COMPILER_CLANG 0
#endif

#if (HV_COMPILER_MSVC + HV_COMPILER_GCC + HV_COMPILER_CLANG != 1)
#	error Multiple compiler defines detected
#endif

#if HV_COMPILER_MSVC
#	define HV_FORCE_INL __forceinline
#	define HV_DEBUG_BREAK __debugbreak() // MSVC debug break
#	define HV_NATVIS_CODE(x) x
#else
#	define HV_FORCE_INL inline __attribute__((always_inline))
#	define HV_DEBUG_BREAK *nullptr = 0 // Debug break by assigning value to a nullptr
#	define HV_NATVIS_CODE(x)
#endif