// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PlatformMacros.h: Platform macros and includes
#pragma once

// Auto platform detection
#if !defined(HV_PLATFORM_WINDOWS)
#	if defined(_WIN32)
#		define HV_PLATFORM_WINDOWS 1
#	endif
#endif

#ifndef HV_PLATFORM_WINDOWS
#	define HV_PLATFROM_WINDOWS 0
#endif

#if (HV_PLATFORM_WINDOWS != 1)
#	error Either the platform is unsupported, or none or multiple platforms are defined
#endif

#ifndef HV_X64
#	if defined(_M_X64) || defined (_WIN64) || defined(__x86_64__) || defined(__ppc64__)
#		define HV_X64 1
#	else
#		define HV_X64 0
#	endif
#endif

#if HV_PLATFORM_WINDOWS
//#	define WIN32_LEAN_AND_MEAN
#	include <Windows.h>
#	include <Windowsx.h>

// Console
#	undef WriteConsole
#	undef ReadConsole
// Window
#	undef CreateWindow
#	undef GetWindowStyle
#	undef IsMaximized
#	undef IsMinimized
// File system
#	undef GetCurrentDirectory
#	undef CreateDirectory
#	undef RemoveDirectory
#	undef CreateFile
#	undef DeleteFile
#	undef MoveFile
#	undef CopyFile
// Threading
#	undef CreateMutex
#	undef AddJob
// Graphics
#	undef CreateSemaphore
#else
#	error No platform includes
#endif