// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Pair.h: Pair
#pragma once

namespace Hv {
	
	/**
	 * Pair
	 * @tparam T0	Value type of first element
	 * @tparam T1	Value type of second element
	 */
	template<typename T0, typename T1>
	struct Pair
	{
		T0 first;	/**< First value */
		T1 second;	/**< Second value */

		/**
		 * Create a pair
		 */
		Pair();
		/**
		 * Create a pair from values
		 * @param[in] val0	1st value
		 * @param[in] val1	2nd value
		 */
		Pair(const T0& val0, const T1& val1);
		/**
		 * Create a pair from values, where val0 is moved
		 * @param[in] val0	1st value
		 * @param[in] val1	2nd value
		 */
		Pair(T0&& val0, const T1& val1);
		/**
		 * Create a pair from values, where val1 is moved
		 * @param[in] val0	1st value
		 * @param[in] val1	2nd value
		 */
		Pair(const T0& val0, T1&& val1);
		/**
		 * Create a pair from values, by moving the values
		 * @param[in] val0	1st value
		 * @param[in] val1	2nd value
		 */
		Pair(T0&& val0, T1&& val1);
		/**
		 * Create a pair from another pair
		 * @param[in] pair	Other pair
		 */
		Pair(const Pair& pair);
		/**
		 * Move a pair into this pair
		 * @param[in] pair	Other pair
		 */
		Pair(Pair&& pair) noexcept;
		~Pair();

		Pair& operator=(const Pair& pair);
		Pair& operator=(Pair&& pair) noexcept;

		/**
		 * Swap the content of 2 Pairs
		 * @param[in] pair	Pair to swap content with
		 */
		void Swap(Pair& pair);
	};

	/**
	 * Create a pair from 2 variables, with the types derived from the variables
	 * @tparam T0		Value type of 1st value
	 * @tparam T1		Value type of 2nd value
	 * @param[in] val0	1st value
	 * @param[in] val1	2nd value
	 */
	template<typename T0, typename T1>
	Pair<T0, T1> MakePair(const T0& val0, T1& val1);
	/**
	 * Create a pair from 2 variables, where val0 is moved, with the types derived from the variables
	 * @tparam T0		Value type of 1st value
	 * @tparam T1		Value type of 2nd value
	 * @param[in] val0	1st value
	 * @param[in] val1	2nd value
	 */
	template<typename T0, typename T1>
	Pair<T0, T1> MakePair(T0&& val0, const T1& val1);
	/**
	 * Create a pair from 2 variables, where val1 is moved, with the types derived from the variables
	 * @tparam T0		Value type of 1st value
	 * @tparam T1		Value type of 2nd value
	 * @param[in] val0	1st value
	 * @param[in] val1	2nd value
	 */
	template<typename T0, typename T1>
	Pair<T0, T1> MakePair(const T0& val0, T1&& val1);
	/**
	 * Create a pair from 2 variables, by moving the values, with the types derived from the variables
	 * @tparam T0		Value type of 1st value
	 * @tparam T1		Value type of 2nd value
	 * @param[in] val0	1st value
	 * @param[in] val1	2nd value
	 */
	template<typename T0, typename T1>
	Pair<T0, T1> MakePair(T0&& val0, T1&& val1);

}

#include "Inline/Pair.inl"
