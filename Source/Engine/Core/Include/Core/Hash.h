// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Hash.h: Hash algorithms
#pragma once
#include "Platform.h"
#include "CoreTypes.h"

#if HV_X64
using hashT = u64;
#else
using hashT = u32;
#endif

namespace Hv {
	
	template<typename T>
	struct HashFNV1a
	{
		hashT operator()(const T& t) const
		{
			return (*this)(&t, 1);
		}

		hashT operator()(const T* arr, sizeT len) const
		{
#if HV_X64
			hashT hash = 14695981039346656037u;
			hashT prime = 1099511628211u;
#else
			hashT hash = 2166136261u;
			hashT prime = 16777619u;
#endif
			for (sizeT i = 0; i < len; ++i)
			{
				u8* data = (u8*)&arr[i];
				for (sizeT j = 0; j < sizeof(T); ++j)
				{
					hash ^= data[j];
					hash *= prime;
				}
			}
			return hash;
		}
	};

}
