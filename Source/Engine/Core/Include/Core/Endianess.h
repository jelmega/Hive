// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Endianess.h: Endianess helpers
#pragma once
#include "Platform.h"

#if HV_PLATFORM_WINDOWS
#	define HV_LITTLE_ENDIAN 1
#	define HV_BIG_ENDIAN 0
#else
#	define HV_LITTLE_ENDIAN 0
#	define HV_BIG_ENDIAN 1
#endif

namespace Hv {
	
	/**
	* Convert the endianess of a value
	* @tparam T		Primitive type
	* @param[in] val	Value to convert
	* @return			Value with the system endianess
	*/
	template<typename T>
	T SwitchEndianess(T val);

	/**
	* Switch between system endianess and little endian
	* @tparam T		Primitive type
	* @param[in] val	Value to convert
	* @return			Switched value
	*/
	template<typename T>
	T SwitchSystemLittleEndian(T val);

	/**
	* Switch between system endianess and big endian
	* @tparam T		Primitive type
	* @param[in] val	Value to convert
	* @return			Switched value
	*/
	template<typename T>
	T SwitchSystemBigEndian(T val);

}

#include "Inline/Endianess.inl"
