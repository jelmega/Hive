// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Delegate.h: Delegate
#pragma once
#include "Assertions.h"
#include "Utility.h"

// Declaration an definition are to be combined to prevent compilation error for <R(Args...)>

namespace Hv {

	// Based on https://codereview.stackexchange.com/questions/14730/impossibly-fast-delegate-in-c11

	template<typename T>
	class Delegate;

	/**
	 * Function Delegate
	 * @tparam R	Return type
	 * @tparam Args	Argument types
	 * @note		Delegate has no support for lambdas
	 * @note		Ctor for functor needs to use (&functorObject, FunctorType::operator())
	 */
	template<typename R, typename... Args>
	class Delegate<R(Args...)>
	{
	private:
		using StubPointer = R(*)(void*, const Args&...);

		template<typename C>
		using DelPair = Pair<C*, R(C::*)(Args...)>;

		Delegate(void* pObj, StubPointer pStub)
			: m_pObj(pObj)
			, m_pData(nullptr)
			, m_pStub(pStub)
		{
		}

		template<typename C>
		Delegate(DelPair<C> pair)
			: m_pData(nullptr)
		{
			*((DelPair<C>*)&m_pObj) = pair;
			m_pStub = PairStub<DelPair<C>>;
		}

	public:

		/**
		* Create an empty delegate
		*/
		Delegate()
			: m_pObj(nullptr)
			, m_pData(nullptr)
			, m_pStub(nullptr)
		{
		}
		/**
		* Create a delegate from an object and member funcion
		* @tparam C				Object class/struct type
		* @param[in] pObj		Object
		* @param[in] pMethod	Object member function
		*/
		template<typename C>
		Delegate(C* pObj, R(C::* pMethod)(Args...))
		{
			*this = From(pObj, pMethod);
		}

		/**
		* Create a delegate from a free functions
		* @param[in] pMethod	Free function
		*/
		Delegate(R(*pMethod)(Args...))
			: m_pData(nullptr)
		{
			m_pObj = pMethod;
			m_pStub = FunctorStub<R(*)(Args ...)>;
		}

		// Temporarily disabled, since it causes crashes, use Bind, similar to std::bind
		/**
		 * Create a delegate from a lambda
		 * @tparam T			Lambda 'type'
		 * @param[in] lambda	Lambda function
		 * @note				There are no checks if the lambda matches the function type, could possibly be checked in the future using RTTI system
		 */
		template<typename T,
				typename = Traits::EnableIfT<!Traits::IsSame<Delegate, Traits::DecayT<T>>::Value>>
		Delegate(T&& lambda)
			: m_pData(nullptr)
		{
			using lambdaType = Traits::DecayT<T>;

			m_pObj = &lambda;

			m_pStub = LambdaStub<lambdaType>;
		}

		/**
		* Set a delegate to empty
		*/
		Delegate(nullptrT)
			: m_pData(nullptr)
			, m_pObj(nullptr)
			, m_pStub(nullptr)
		{
		}

		/**
		* Create a delegate from another delegate
		* @param[in] del	Delegate
		*/
		Delegate(const Delegate& del)
			: m_pObj(del.m_pObj)
			, m_pData(del.m_pData)
			, m_pStub(del.m_pStub)
		{
		}
		/**
		* Move a delegate into this delegate
		* @param[in] del	Delegate
		*/
		Delegate(Delegate&& del) noexcept
			: m_pObj(del.m_pObj)
			, m_pData(del.m_pData)
			, m_pStub(del.m_pStub)
		{
		}

		Delegate& operator=(const Delegate& del)
		{
			HV_ASSERT_MSG(this != &del, "Self assignment!");
			m_pObj = del.m_pObj;
			m_pData = del.m_pData;
			m_pStub = del.m_pStub;
			return *this;
		}

		Delegate& operator=(Delegate&& del) noexcept
		{
			HV_ASSERT_MSG(this != &del, "Self assignment!");
			m_pObj = del.m_pObj;
			m_pData = del.m_pData;
			m_pStub = del.m_pStub;
			return *this;
		}

		b8 operator==(const Delegate& del) const
		{
			HV_ASSERT_MSG(this != &del, "Compare with self!");
			return m_pStub == del.m_pStub && m_pObj == del.m_pObj;
		}

		b8 operator!=(const Delegate& del) const
		{
			HV_ASSERT_MSG(this != &del, "Compare with self!");
			return m_pStub != del.m_pStub || m_pObj != del.m_pObj;
		}

		b8 operator==(std::nullptr_t) const
		{
			return !m_pStub;
		}

		b8 operator!=(std::nullptr_t) const
		{
			return m_pStub;
		}

		explicit operator b8() const
		{
			return m_pStub;
		}

		R operator()(const Args&... args)
		{
			HV_ASSERT_MSG(m_pStub, "No function assigned!");
			if (m_pData)
				return m_pStub(&m_pObj, args...);
			return m_pStub(m_pObj, args...);
		}

		/**
		* Set the object
		* @param[in] pObj	Object
		* @note			The user is responsible for giving in an object of the correct type
		*/
		void SetObject(void* pObj)
		{
			m_pObj = pObj;
		}
		/**
		* Check if the delegate has an object
		* @return	True if the delegate has an object, false otherwise
		*/
		b8 HasObject() const
		{
			return m_pObj;
		}
		/**
		* Invoke the delegate without any checks
		* @param[in] args	Arguments
		*/
		R Invoke(Args... args)
		{
			return m_pStub(Forward<Args>(args)...);
		}
		/**
		* Try to invoke a function (does <b>NOT</b> check for an object)
		* @return			True if the function can be called
		*/
		b8 CanInvoke() const
		{
			return m_pStub;
		}
		/**
		* Try to invoke a function (checks for an object)
		* @return			True if the function can be called
		*/
		b8 CanMemberInvoke() const
		{
			return m_pStub && m_pObj;
		}

		/**
		* Create a delegate from a free pointer
		* @tparam Func	Function pointer
		* @return		Delegate
		*/
		template<R(*Func)(Args...)>
		static Delegate From()
		{
			return Delegate(nullptr, &FunctionStub<Func>);
		}
		/**
		* Create a delegate from a member function
		* @tparam C		Class
		* @tparam Func	Member function pointer
		* @return		Delegate
		*/
		template<typename C, R(C::*Func)(Args...)>
		static Delegate From()
		{
			return Delegate(nullptr, &MemberStub<C, Func>);
		}
		/**
		* Create a delegate from a member function and an object
		* @tparam C			Class
		* @tparam Func		Member function pointer
		* @param[in] pObj	Object
		* @return			Delegate
		*/
		template<typename C, R(C::*Func)(Args...)>
		static Delegate From(C* pObj)
		{
			return Delegate(pObj, &MemberStub<C, Func>);
		}
		/**
		* Create a delegate from a member function and an object
		* @tparam C			Class
		* @tparam Func		Member function pointer
		* @param[in] pObj	Object
		* @return			Delegate
		*/
		template<typename C, R(C::*Func)(Args...)>
		static Delegate From(const C& pObj)
		{
			return Delegate(static_cast<C*>(&pObj), &MemberStub<C, Func>);
		}

		/**
		* Swap the contents of 2 delegates
		* @param[in] del	Delegate
		*/
		void Swap(Delegate& del)
		{
			Hv::Swap(m_pStub, del.m_pStub);
			Hv::Swap(m_pObj, del.m_pObj);
		}

		// Create use function pointer as param instead of tparam

		template<typename C>
		static Delegate From(C* pObj, R(C::* pMethod)(Args...))
		{
			return DelPair<C>(pObj, pMethod);
		}

		static Delegate From(R(*pMethod)(Args...))
		{
			return pMethod;
		}

	private:

		template<R(*Func)(Args...)>
		static R FunctionStub(void*, const Args&... args)
		{
			return Func(args...);
		}

		template<typename C, R(C::*Func)(Args...)>
		static R MemberStub(void* pObj, const Args&... args)
		{
			HV_ASSERT_MSG(pObj, "No object assigned!");
			return (static_cast<C*>(pObj)->*Func)(args...);
		}

		template<typename T>
		static R PairStub(void* pObj, const Args&... args)
		{
			T* pair = (T*)pObj;
			return (pair->first->*
				(pair->second))
				(args...);
		}

		template<typename C>
		static R FunctorStub(void* pObj, const Args&... args)
		{
			return (*(C)pObj)(args...);
		}

		template<typename L>
		static R LambdaStub(void* pObj, const Args&... args)
		{
			return (*(L*)pObj)(args...);
		}

		void* m_pObj;			/**< pointer to object */
								// vv magic :) vv
		void* m_pData;			/**< Extra data */
		StubPointer m_pStub;	/**< Pointer to function stub */
	};

}