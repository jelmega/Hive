// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Utlity.h: Misc utilities
#pragma once
#include "TypeTraits.h"

namespace Hv {
	
	template<typename T>
	constexpr Traits::RemoveReferenceT<T>&& Move(T&& t) noexcept
	{
		return static_cast<Traits::RemoveReferenceT<T>&&>(t);
	}

	template<typename T>
	constexpr T&& Forward(Traits::RemoveReferenceT<T>&& t) noexcept
	{
		static_assert(!Traits::IsLvalueReferenceV<T>, "Bad forward call!");
		return static_cast<T&&>(t);
	}

	template<typename T>
	constexpr T&& Forward(T& t) noexcept
	{
		return static_cast<T&&>(t);
	}

	template<typename T>
	void Swap(T& t0, T& t1)
	{
		T tmp(Move(t0));
		t0 = Move(t1);
		t1 = Move(tmp);
	}

	template<typename T>
	void SwapRanges(T* t0, T* t0end, T* t1)
	{
		for (T* it0 = t0, *it1 = t1; it0 != t0end; ++it0, ++it1)
		{
			Swap(*it0, *it1);
		}
	}

}
