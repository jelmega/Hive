// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// BuildMacros.h: Macros dependent on build settings
#pragma once

// Debug build
#ifndef HV_DEBUG
#define HV_DEBUG 0
#endif

// Development build
#ifndef HV_DEVELOPMENT
#define HV_DEVELOPMENT 0
#endif

// Test build (Optimized test build)
#ifndef HV_TEST
#define HV_TEST 0
#endif

// Shipping build (final build)
#ifndef HV_SHIPPING
#define HV_SHIPPING 0
#endif

// Check if he build configuration was set up correctly
#if (HV_DEBUG + HV_DEVELOPMENT + HV_TEST + HV_SHIPPING != 1)
#error Invalid build configuration
#endif

// Uncomment to globaly disable assert and stats, instead of file local disables
//#define HV_DISABLE_ASSERT 1
//#define HV_DISABLE_STATS 1

// Explicitly disable asserts
#ifndef HV_DISABLE_ASSERT
#	define HV_DISABLE_ASSERT 0
#endif

// Explicitly disable statistics
#ifndef HV_DISABLE_STATS
#	define HV_DISABLE_STATS 0
#endif

/**
 * Info about build dependent defines
 * 
 * HV_USE_ASSERT		Use assertions
 * HV_USE_SLOW_ASSERT	Use slow assertions, assertions that are called frequently or have expensive expressions
 * HV_STATISTICS		Collect engine statistics
 */

#if HV_DEBUG
#	define HV_USE_ASSERT		1 && !HV_DISABLE_ASSERT
#	define HV_USE_SLOW_ASSERT	1 && !HV_DISABLE_ASSERT
#	define HV_STATISTICS		1 && !HV_DISABLE_STATS
#elif HV_DEVELOPMENT
#	define HV_USE_ASSERT		1 && !HV_DISABLE_ASSERT
#	define HV_USE_SLOW_ASSERT	0
#	define HV_STATISTICS		1 && !HV_DISABLE_STATS
#elif HV_TEST
#	define HV_USE_ASSERT		0
#	define HV_USE_SLOW_ASSERT	0
#	define HV_STATISTICS		1 & !HV_DISABLE_STATS
#elif HV_SHIPPING
#	define HV_USE_ASSERT		0
#	define HV_USE_SLOW_ASSERT	0
#	define HV_STATISTICS		0
#endif