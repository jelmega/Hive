// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// IMemoryAllocator.h: Memory arena interface
#pragma once
#include "Memory/Allocators/IAllocator.h"

namespace Hv::Memory {
	
	class IMemoryArena : public IAllocator
	{
	public:
		/**
		 * Default constructor
		 */
		IMemoryArena();
		/**
		 * Constructor with debug name
		 * @param[in] numAllocators	Number of internal allocators
		 * @param[in] name			Debug name
		 */
		IMemoryArena(u8 numAllocators, const AnsiChar* name = "Memory arena");
		/**
		 * Move an arena into this arena
		 * @param[in] arena	Arena to move into this allocator
		 */
		IMemoryArena(IMemoryArena&& arena) noexcept;
		virtual ~IMemoryArena();

		IMemoryArena& operator=(IMemoryArena&& arena) noexcept;

		/**
		 * Allocate memory
		 * @param[in] size		Size to allocate
		 * @param[in] alignment	Alignment of the allocation
		 * @param[in] context	Alloc context
		 * @return				Pointer to allocated memory or nullptr if allocation failed
		 */
		virtual	void* Allocate(sizeT size, u16 alignment, AllocContext context = AllocContext()) = 0;
		/**
		 * Reallocate memory
		 * @param[in] pOriginal	Original memory
		 * @param[in] size		New size of the allocation
		 * @param[in] alignment	New alignment of the allocation
		 * @param[in] context	Alloc context
		 * @return				Pointer to reallocated memory or nullptr, is size == 0 or reallocation failed
		 * @note				When a nullptr is returned and size != 0, reallocation failed and data is still in the original location
		 */
		virtual	void* Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context = AllocContext()) = 0;
		/**
		 * Free memory
		 * @param[in] ptr		Memory to free
		 * @param[in] context	Alloc context
		 */
		virtual void Free(void* ptr, AllocContext context = AllocContext()) = 0;
		/**
		 * Check if the pointer is allocated using the allocator
		 * @return True if the pointer is allocated using the allocator, false otherwise
		 */
		virtual b8 Owns(void* ptr) = 0;

		/**
		 * Get the threshold for the allocator at an index
		 * @param[in] index	Index of the allocator to get the threshold for
		 */
		virtual sizeT GetAllocatorThreshold(u8 index) = 0;
		/**
		* Get the allocator at an index
		* @param[in] index	Index of the allocator to get
		*/
		virtual IAllocator* GetAllocator(u8 index) = 0;

		/**
		 * Get the size of the number of internal allocators
		 * @return	Size of the number of internal allocators
		 */
		HV_FORCE_INL sizeT GetNumInternalAllocators() const { return m_NumAllocators; }

	protected:
		HV_MAKE_NON_COPYABLE(IMemoryArena);

		u8 m_NumAllocators;	/**< Number of internal allocators */
	};

}
