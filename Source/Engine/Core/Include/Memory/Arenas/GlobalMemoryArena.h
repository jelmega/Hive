// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// GloablMemoryAllocator.h: Global memory arena
#pragma once
#include "IMemoryArena.h"
#include "Memory/Allocators/PagedPoolAllocator.h"
#include "Memory/Allocators/PagedFreeListAllocator.h"
#include "Memory/Allocators/PagedBuddyAllocator.h"
#include "Memory/Allocators/Mallocator.h"
#include "Threading/CriticalSection.h"

namespace Hv::Memory {
	
	class GlobalMemoryArena final : public IMemoryArena
	{
	public:
		/**
		 * Default constructor
		 */
		GlobalMemoryArena();
		/**
		 * Constructor with debug name
		 * @param[in] name	Debug name
		 */
		explicit GlobalMemoryArena(const AnsiChar* name);
		/**
		 * Move an arena into this arena
		 * @param[in] arena	Arena to move into this allocator
		 */
		GlobalMemoryArena(GlobalMemoryArena&& arena) noexcept;
		virtual ~GlobalMemoryArena();

		GlobalMemoryArena& operator=(GlobalMemoryArena&& arena) noexcept;

		/**
		 * Allocate memory
		 * @param[in] size		Size to allocate
		 * @param[in] alignment	Alignment of the allocation
		 * @param[in] context	Alloc context
		 * @return				Pointer to allocated memory or nullptr if allocation failed
		 */
		void* Allocate(sizeT size, u16 alignment, AllocContext context = AllocContext()) override final;
		/**
		 * Reallocate memory
		 * @param[in] pOriginal	Original memory
		 * @param[in] size		New size of the allocation
		 * @param[in] alignment	New alignment of the allocation
		 * @param[in] context	Alloc context
		 * @return				Pointer to reallocated memory or nullptr, is size == 0 or reallocation failed
		 * @note				When a nullptr is returned and size != 0, reallocation failed and data is still in the original location
		 */
		void* Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context = AllocContext()) override final;
		/**
		 * Free memory
		 * @param[in] ptr		Memory to free
		 * @param[in] context	Alloc context
		 */
		void Free(void* ptr, AllocContext context = AllocContext()) override final;
		/**
		 * Check if the pointer is allocated using the allocator
		 * @return True if the pointer is allocated using the allocator, false otherwise
		 */
		b8 Owns(void* ptr) override final;

		/**
		 * Get the threshold for the allocator at an index
		 * @param[in] index	Index of the allocator to get the threshold for
		 */
		sizeT GetAllocatorThreshold(u8 index) override final;
		/**
		* Get the allocator at an index
		* @param[in] index	Index of the allocator to get
		*/
		IAllocator* GetAllocator(u8 index) override final;

	private:
		HV_MAKE_NON_COPYABLE(GlobalMemoryArena);

		// Allocators
		PagedPoolAllocator m_PoolAlloc;									/**< Pool Allocator */
		PagedFreeListAllocator m_FreelistAlloc;							/**< Freelist Allocator */
		PagedBuddyAllocator m_BuddyAlloc;								/**< Buddy allocator */
		Mallocator m_Malloc;											/**< Mallocator */

		static Threading::CriticalSection m_CriticalSection;

		// Thresholds (sizes below threshold will use the respective allocator)
		static constexpr sizeT m_PoolThreshold = 32;					/**< Pool allocator threshold */
		static constexpr sizeT m_FreelistThreshold = 1024 * 1024;		/**< Freelist allocator threshold */
		static constexpr sizeT m_BuddyThreshold = 64 * 1024 * 1024;		/**< Buddy allocator threshold */

		// Sizes
		static constexpr sizeT m_PoolBlockSize = 32;					/**< Pool allocator block size */
		static constexpr sizeT m_PoolPageSize = 1024 * 1024;			/**< Pool allcoator page size */
		static constexpr u16   m_PoolAlignment = 16;					/**< Pool allcoator alignment */
		static constexpr sizeT m_FreelistSize = 32 * 1024 * 1024;		/**< Freelist allocator size */
		static constexpr sizeT m_BuddyMinSize = m_FreelistThreshold;	/**< Buddy allocator min size */
		static constexpr sizeT m_BuddyMaxSize = m_BuddyThreshold;		/**< Buddy allocator max size */
		static constexpr u16   m_BuddyAlignment = 16;					/**< Pool allcoator alignment */

	};

}
