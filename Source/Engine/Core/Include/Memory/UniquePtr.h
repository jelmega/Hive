// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// UniquePtr.h: Unique pointer
#pragma once
#include "Core/CoreHeaders.h"
#include "Memory/MemoryUtil.h"

namespace Hv::Memory {
	
	/**
	 * Unique pointer
	 * @tparam T		Element type
	 * @tparam Deleter	Deleter predicate
	 */
	template<typename T, typename Deleter = DefaultDeleter<T>>
	class UniquePtr
	{
	public:
		/**
		 * Create an empty UniquePtr
		 */
		UniquePtr();
		/**
		 * Create a UniquePtr from a pointer
		 * @param[in] ptr	Pointer
		 */
		UniquePtr(T* ptr);
		/**
		* Create a UniquePtr from a pointer and a deleter
		* @param[in] ptr		Pointer
		* @param[in] deleter	Deleter
		*/
		UniquePtr(T* ptr, const Deleter& deleter);
		/**
		 * Move a UniquePtr into this UniquePtr
		 * @param[in] ptr	UniquePtr
		 */
		UniquePtr(UniquePtr&& ptr) noexcept;
	
		~UniquePtr();
	
		UniquePtr& operator=(UniquePtr&& ptr) noexcept;
	
		T& operator*();
		const T& operator*() const;
		T* operator->();
		const T* operator->() const;
	
		b8 operator!() const;
		operator b8() const;
	
		/**
		 * Get the raw pointer
		 * @return Raw pointer
		 */
		T* Get();
		/**
		 * Get the raw pointer
		 * @return Raw pointer
		 */
		const T* Get() const;
	
		/**
		 * Get the deleter
		 * @return	Deleter
		 */
		Deleter& GetDeleter();
		/**
		 * Get the deleter
		 * @return	Deleter
		 */
		const Deleter& GetDeleter() const;
	
		/**
		 * Check if the UniquePtr is valid (has a pointer)
		 * @return	If the UniquePtr is valid
		 */
		bool IsValid() const;
		/**
		 * Release the pointer (user needs to delete ptr)
		 * @return	Pointer
		 */
		T* Release();
		/**
		 * Reset pointer (delete old pointer and set 'ptr' as new pointer)
		 * @param[in] ptr	Pointer
		 */
		void Reset(T* ptr);
	
		/**
		 * Swap 2 UniquePtrs
		 * @param[in] ptr	UniquePtr to swap with
		 */
		void Swap(UniquePtr& ptr);
	
	private:
		HV_MAKE_NON_COPYABLE(UniquePtr)
	
		T* m_Ptr;			/**< Pointer */
		Deleter m_Deleter;	/**< Deleter */
	};
	
	/**
	 * Unique pointer (specialization for array)
	 * @tparam T		Element type
	 * @tparam Deleter	Deleter predicate
	 */
	template<typename T, typename Deleter>
	class UniquePtr<T[], Deleter>
	{
	public:
		/**
		 * Create an empty UniquePtr
		 */
		UniquePtr();
		/**
		 * Create a UniquePtr from a pointer
		 * @param[in] ptr	Pointer
		 */
		UniquePtr(T* ptr);
		/**
		 * Create a UniquePtr from a pointer and a deleter
		 * @param[in] ptr		Pointer
		 * @param[in] deleter	Deleter
		 */
		UniquePtr(T* ptr, Deleter deleter);
		/**
		 * Move a UniquePtr into this UniquePtr
		 * @param[in] ptr	UniquePtr
		 */
		UniquePtr(UniquePtr&& ptr) noexcept;
	
		~UniquePtr();
	
		UniquePtr& operator=(UniquePtr&& ptr) noexcept;
	
		T& operator*();
		const T& operator*() const;
		T* operator->();
		const T* operator->() const;
	
		b8 operator!() const;
		operator b8() const;
	
		/**
		 * Get the raw pointer
		 * @return Raw pointer
		 */
		T* Get();
		/**
		 * Get the raw pointer
		 * @return Raw pointer
		 */
		const T* Get() const;
	
		/**
		 * Get the deleter
		 * @return	Deleter
		 */
		Deleter& GetDeleter();
		/**
		 * Get the deleter
		 * @return	Deleter
		 */
		const Deleter& GetDeleter() const;
	
		/**
		 * Check if the UniquePtr is valid (has a pointer)
		 * @return	If the UniquePtr is valid
		 */
		bool IsValid() const;
		/**
		 * Release the pointer (user needs to delete ptr)
		 * @return	Pointer
		 */
		T* Release();
		/**
		 * Reset pointer (delete old pointer and set 'ptr' as new pointer)
		 * @param[in] ptr	Pointer
		 */
		void Reset(T* ptr);
	
		/**
		 * Swap 2 UniquePtrs
		 * @param[in] ptr	UniquePtr to swap with
		 */
		void Swap(UniquePtr& ptr);
	
	private:
		HV_MAKE_NON_COPYABLE(UniquePtr)
	
		T* m_Ptr;			/**< Pointer */
		Deleter m_Deleter;	/**< Deleter */
	};
	
	
	
	template<typename T, typename... Args>
	UniquePtr<T> MakeUnique(Args&&... args);

}

HV_DECLARE_CONTAINER_MEMCPY_TEMPLATE(HV_TARGS(typename T, typename Deleter), HV_TTYPE(Hv::Memory::UniquePtr, T, Deleter));