// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// MemoryUtil.h: Memory utility
#pragma once

#include "HAL/HalMemory.h"

namespace Hv::Memory {
	
	/**
	 * Allocate memory
	 * @param[in] size		Size to allocate
	 * @param[in] alignment	Alignment of the allocation
	 * @param[in] context	Allocation context
	 * @return				Pointer to the allocated memory
	 */
	void* Allocate(sizeT size, u16 alignment = 8, AllocContext context = AllocContext());
	/**
	 * Free memory
	 * @param[in] ptr	Memory to free
	 */
	void Free(void* ptr, AllocContext context = AllocContext());

	/**
	 * Copy a range of memory from one location to another
	 * @param[in] pDst	Pointer to destination to copy to
	 * @param[in] pSrc	Pointer to source to copy from
	 * @param[in] size	Number of bytes to copy
	 * @return			Pointer to the destination
	 */
	void* Copy(void* pDst, const void* pSrc, sizeT size);
	/**
	 * Move a range of memory from one location to another
	 * @param[in] pDst	Pointer to destination to move to
	 * @param[in] pSrc	Pointer to source to move from
	 * @param[in] size	Number of bytes to move
	 * @return			Pointer to the destination
	 */
	void* Move(void* pDst, const void* pSrc, sizeT size);
	/**
	 * Set a range of memory to a value
	 * @param[in] ptr	Pointer to memory to set
	 * @param[in] val	value to set memory to
	 * @param[in] size	Number of bytes to set
	 * @return			Pointer to the destination
	 */
	void* Set(void* ptr, u8 val, sizeT size);
	/**
	 * Clear a range of memory
	 * @param[in] ptr	Pointer to memory to set
	 * @param[in] size	Number of bytes to set
	 * @return			Pointer to the destination
	 */
	void* Clear(void* ptr, sizeT size);
	/**
	 * Set the content of a structure to 0
	 * @tparam T		Structure type
	 * @param[in] ptr	Pointer to structure
	 * @return			Pointer to structure
	 */
	template<typename T>
	void* Zero(T* ptr);

	/**
	 * Compare the content of a 2 memory chunks
	 * @tparam T		Structure type
	 * @param[in] ptr0	Pointer to first memory chunk
	 * @param[in] ptr1	Pointer to second memory chunk
	 * @param[in] size	Amount of bytes to compare
	 * @return			Relationship between chunks of memory\n
	 *					0 : chunks match
	 *					-1: chunks don't match, first byte that doesn't match, has a lower value in ptr0 than in ptr1\n
	 *					1 : chunks don't match, first byte that doesn't match, has a greater value in ptr0 than in ptr1
	 */
	i32 Compare(const void* ptr0, const void* ptr1, sizeT size);

	/**
	 * Default deleter for smart pointers
	 */
	template<typename T>
	struct DefaultDeleter
	{
		void operator()(T* ptr)
		{
			delete ptr;
		}
	};

	/**
	* Default deleter for smart pointers (array)
	*/
	template<typename T>
	struct DefaultDeleter<T[]>
	{
		void operator()(T* ptr)
		{
			delete[] ptr;
		}
	};

}

#define HvNew new
#define HvDelete delete

#include "Inline/MemoryUtil.inl"