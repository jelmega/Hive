// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// SharedPtr.inl: Shared pointer
#pragma once
#include "Memory/SharedPtr.h"

namespace Hv::Memory {
	
	template <typename T, typename Deleter>
	SharedPtr<T, Deleter>::SharedPtr()
		: m_pRef(nullptr)
	{
	}

	template <typename T, typename Deleter>
	SharedPtr<T, Deleter>::SharedPtr(T* ptr)
		: m_pRef(new PtrRef<T, Deleter>(ptr))
	{
	}

	template <typename T, typename Deleter>
	SharedPtr<T, Deleter>::SharedPtr(T* ptr, const Deleter& deleter)
		: m_pRef(new PtrRef<T, Deleter>(ptr, deleter))
	{
	}

	template <typename T, typename Deleter>
	SharedPtr<T, Deleter>::SharedPtr(const SharedPtr& ptr)
		: m_pRef(ptr.m_pRef)
	{
		if (m_pRef)
			m_pRef->Ref();
	}

	template <typename T, typename Deleter>
	SharedPtr<T, Deleter>::SharedPtr(SharedPtr&& ptr) noexcept
		: m_pRef(ptr.m_pRef)
	{
		ptr.m_pRef = nullptr;
	}

	template <typename T, typename Deleter>
	SharedPtr<T, Deleter>::~SharedPtr()
	{
		if (m_pRef)
		{
			m_pRef->Deref();
			if (m_pRef->RefCount() == 0)
			{
				delete m_pRef;
			}
		}
	}

	template <typename T, typename Deleter>
	SharedPtr<T, Deleter>& SharedPtr<T, Deleter>::operator=(const SharedPtr& ptr)
	{
		if (m_pRef)
		{
			m_pRef->Deref();
			if (m_pRef->RefCount() == 0)
			{
				delete m_pRef;
			}
		}
		m_pRef = ptr.m_pRef;
		if (m_pRef)
			m_pRef->Ref();
		return *this;
	}

	template <typename T, typename Deleter>
	SharedPtr<T, Deleter>& SharedPtr<T, Deleter>::operator=(SharedPtr&& ptr) noexcept
	{
		if (m_pRef)
		{
			m_pRef->Deref();
			if (m_pRef->RefCount() == 0)
			{
				delete m_pRef;
			}
		}
		m_pRef = ptr.m_pRef;
		ptr.m_pRef = nullptr;
		return *this;
	}

	template <typename T, typename Deleter>
	T& SharedPtr<T, Deleter>::operator*()
	{
		return m_pRef->operator*();
	}

	template <typename T, typename Deleter>
	const T& SharedPtr<T, Deleter>::operator*() const
	{
		return m_pRef->operator*();
	}

	template <typename T, typename Deleter>
	T* SharedPtr<T, Deleter>::operator->()
	{
		return m_pRef->operator->();
	}

	template <typename T, typename Deleter>
	const T* SharedPtr<T, Deleter>::operator->() const
	{
		return m_pRef->operator->();
	}

	template <typename T, typename Deleter>
	b8 SharedPtr<T, Deleter>::operator!() const
	{
		return m_pRef && m_pRef->operator*();
	}

	template <typename T, typename Deleter>
	SharedPtr<T, Deleter>::operator b8() const
	{
		return m_pRef && *m_pRef;
	}

	template <typename T, typename Deleter>
	T* SharedPtr<T, Deleter>::Get()
	{
		if (!m_pRef)
			return nullptr;
		return m_pRef->Get();
	}

	template <typename T, typename Deleter>
	const T* SharedPtr<T, Deleter>::Get() const
	{
		if (!m_pRef)
			return nullptr;
		return m_pRef->Get();
	}

	template <typename T, typename Deleter>
	Deleter& SharedPtr<T, Deleter>::GetDeleter()
	{
		HV_ASSERT(m_pRef);
		return m_pRef->GetDeleter();
	}

	template <typename T, typename Deleter>
	const Deleter& SharedPtr<T, Deleter>::GetDeleter() const
	{
		HV_ASSERT(m_pRef);
		return m_pRef->GetDeleter();
	}

	template <typename T, typename Deleter>
	sizeT SharedPtr<T, Deleter>::RefCount() const
	{
		if (!m_pRef)
			return 0;
		return m_pRef->RefCount();
	}

	template <typename T, typename Deleter>
	bool SharedPtr<T, Deleter>::IsValid() const
	{
		return m_pRef && m_pRef->IsValid();
	}

	template <typename T, typename Deleter>
	bool SharedPtr<T, Deleter>::IsUnique() const
	{
		return m_pRef && m_pRef->IsUnique();
	}

	template <typename T, typename Deleter>
	void SharedPtr<T, Deleter>::Reset(T* ptr)
	{
		if (m_pRef)
		{
			m_pRef->Deref();
			if (m_pRef->RefCount() == 0)
			{
				delete m_pRef;
			}
		}
		m_pRef = new PtrRef<T, Deleter>(ptr);
	}

	template <typename T, typename Deleter>
	void SharedPtr<T, Deleter>::Swap(SharedPtr& ptr)
	{
		Hv::Swap(m_pRef, ptr.m_pRef);
	}


	template <typename T, typename ... Args>
	SharedPtr<T> Memory::MakeShared(Args&&... args)
	{
		return SharedPtr<T>(HvNew T(Forward<Args>(args...)));
	}

}