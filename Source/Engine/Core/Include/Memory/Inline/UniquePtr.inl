// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// UniquePtr.inl: Unique pointer
#pragma once
#include "Memory/UniquePtr.h"

namespace Hv::Memory {
	
	template <typename T, typename Deleter>
	HV_FORCE_INL UniquePtr<T, Deleter>::UniquePtr()
		: m_Ptr(nullptr)
	{
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL UniquePtr<T, Deleter>::UniquePtr(T* ptr)
		: m_Ptr(ptr)
	{
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL UniquePtr<T, Deleter>::UniquePtr(T* ptr, const Deleter& deleter)
		: m_Ptr(ptr)
		, m_Deleter(deleter)
	{
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL UniquePtr<T, Deleter>::UniquePtr(UniquePtr&& ptr) noexcept
		: m_Ptr(ptr.m_Ptr)
		, m_Deleter(m_Deleter)
	{
		ptr.m_Ptr = nullptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL UniquePtr<T, Deleter>::~UniquePtr()
	{
		if (m_Ptr)
			m_Deleter(m_Ptr);
	}

	template <typename T, typename Deleter>
	HV_INL UniquePtr<T, Deleter>& UniquePtr<T, Deleter>::operator=(UniquePtr&& ptr) noexcept
	{
		T* old = m_Ptr;
		m_Ptr = ptr.m_Ptr;
		ptr.m_Ptr = nullptr;
		if (old)
			m_Deleter(old);
		m_Deleter = ptr.m_Deleter;
		return *this;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL T& UniquePtr<T, Deleter>::operator*()
	{
		return *m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL const T& UniquePtr<T, Deleter>::operator*() const
	{
		return *m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL T* UniquePtr<T, Deleter>::operator->()
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL const T* UniquePtr<T, Deleter>::operator->() const
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL b8 UniquePtr<T, Deleter>::operator!() const
	{
		return !m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL UniquePtr<T, Deleter>::operator b8() const
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL T* UniquePtr<T, Deleter>::Get()
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL const T* UniquePtr<T, Deleter>::Get() const
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL Deleter& UniquePtr<T, Deleter>::GetDeleter()
	{
		return m_Deleter;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL const Deleter& UniquePtr<T, Deleter>::GetDeleter() const
	{
		return m_Deleter;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL bool UniquePtr<T, Deleter>::IsValid() const
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL T* UniquePtr<T, Deleter>::Release()
	{
		T* ptr = m_Ptr;
		m_Ptr = nullptr;
		return ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL void UniquePtr<T, Deleter>::Reset(T* ptr)
	{
		T* old = m_Ptr;
		m_Ptr = ptr;
		if (old)
			m_Deleter(old);
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL void UniquePtr<T, Deleter>::Swap(UniquePtr& ptr)
	{
		Hv::Swap(m_Ptr, ptr.m_Ptr);
		Hv::Swap(m_Deleter, ptr.m_Deleter);
	}

	////////////////////////////////////////////////////////////////////////////////

	template <typename T, typename Deleter>
	HV_FORCE_INL UniquePtr<T[], Deleter>::UniquePtr()
		: m_Ptr(nullptr)
	{
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL UniquePtr<T[], Deleter>::UniquePtr(T* ptr)
		: m_Ptr(ptr)
	{
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL UniquePtr<T[], Deleter>::UniquePtr(T* ptr, Deleter deleter)
		: m_Ptr(ptr)
		, m_Deleter(deleter)
	{
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL UniquePtr<T[], Deleter>::UniquePtr(UniquePtr&& ptr) noexcept
		: m_Ptr(ptr.m_Ptr)
		, m_Deleter(ptr.m_Deleter)
	{
		ptr.m_Ptr = nullptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL UniquePtr<T[], Deleter>::~UniquePtr()
	{
		if (m_Ptr)
			m_Deleter(m_Ptr);
	}

	template <typename T, typename Deleter>
	HV_INL UniquePtr<T[], Deleter>& UniquePtr<T[], Deleter>::operator=(UniquePtr&& ptr) noexcept
	{
		T* old = m_Ptr;
		m_Ptr = ptr.m_Ptr;
		ptr.m_Ptr = nullptr;
		if (old)
			m_Deleter(m_Ptr);
		m_Deleter = ptr.m_Deleter;
		return *this;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL T& UniquePtr<T[], Deleter>::operator*()
	{
		return *m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL const T& UniquePtr<T[], Deleter>::operator*() const
	{
		return *m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL T* UniquePtr<T[], Deleter>::operator->()
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL const T* UniquePtr<T[], Deleter>::operator->() const
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL b8 UniquePtr<T[], Deleter>::operator!() const
	{
		return !m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL UniquePtr<T[], Deleter>::operator b8() const
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL T* UniquePtr<T[], Deleter>::Get()
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL const T* UniquePtr<T[], Deleter>::Get() const
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL Deleter& UniquePtr<T[], Deleter>::GetDeleter()
	{
		return m_Deleter;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL const Deleter& UniquePtr<T[], Deleter>::GetDeleter() const
	{
		return m_Deleter;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL bool UniquePtr<T[], Deleter>::IsValid() const
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL T* UniquePtr<T[], Deleter>::Release()
	{
		T* ptr = m_Ptr;
		m_Ptr = nullptr;
		return ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL void UniquePtr<T[], Deleter>::Reset(T* ptr)
	{
		T* old = m_Ptr;
		m_Ptr = ptr;
		if (old)
			m_Deleter(old);
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL void UniquePtr<T[], Deleter>::Swap(UniquePtr& ptr)
	{
		Hv::Swap(m_Ptr, ptr.m_Ptr);
		Hv::Swap(m_Deleter, ptr.m_Deleter);
	}

	template <typename T, typename ... Args>
	HV_FORCE_INL UniquePtr<T> MakeUnique(Args&&... args)
	{
		return UniquePtr<T>(HvNew T(Forward<Args>(args...)));
	}

}