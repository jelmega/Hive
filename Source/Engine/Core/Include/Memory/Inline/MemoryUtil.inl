// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// MemoryUtil.inl: Memory utility
#pragma once

#include "Memory/MemoryUtil.h"

namespace Hv::Memory {

	HV_FORCE_INL void* Allocate(sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context); // TODO

		HV_SLOW_ASSERT_MSG(size, "Can't allocate 0 bytes!");
		HV_SLOW_ASSERT_MSG(alignment, "Can't align to 0 bytes!");
		HV_SLOW_ASSERT_MSG(!(alignment & (alignment - 1)), "Can't align to 0 bytes!");

		return HAL::Memory::Allocate(size, alignment);
	}

	HV_FORCE_INL void Free(void* ptr, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context); // TODO

		HV_SLOW_ASSERT_MSG(ptr, "Can't free to a nullptr!");

		HAL::Memory::Free(ptr);
	}

	HV_FORCE_INL void* Copy(void* pDst, const void* pSrc, sizeT size)
	{
		HV_SLOW_ASSERT_MSG(pDst, "Can't move to a nullptr!");
		HV_SLOW_ASSERT_MSG(pSrc, "Can't move from a nullptr!");
		HV_SLOW_ASSERT_MSG(size, "Can't move 0 bytes!");
		return memcpy(pDst, pSrc, size);
	}

	HV_FORCE_INL void* Move(void* pDst, const void* pSrc, sizeT size)
	{
		HV_SLOW_ASSERT_MSG(pDst, "Can't move to a nullptr!");
		HV_SLOW_ASSERT_MSG(pSrc, "Can't move from a nullptr!");
		HV_SLOW_ASSERT_MSG(size, "Can't move 0 bytes!");
		return memmove(pDst, pSrc, size); 
	}

	HV_FORCE_INL void* Set(void* ptr, u8 val, sizeT size)
	{
		HV_SLOW_ASSERT_MSG(ptr, "Can't set a nullptr's memory!");
		HV_SLOW_ASSERT_MSG(size, "Can't set 0 bytes!");
		return memset(ptr, val, size);
	}

	HV_FORCE_INL void* Clear(void* ptr, sizeT size)
	{
		HV_SLOW_ASSERT_MSG(ptr, "Can't clear a nullptr's memory!");
		HV_SLOW_ASSERT_MSG(size, "Can't clear 0 bytes!");
		return memset(ptr, 0, size);
	}

	HV_FORCE_INL i32 Compare(const void* ptr0, const void* ptr1, sizeT size)
	{
		HV_ASSERT_MSG(ptr0, "Can't compare with nullptr!");
		HV_ASSERT_MSG(ptr1, "Can't compare with nullptr!");
		HV_ASSERT_MSG(size != 0, "Can't compare 0 bytes!");
		return memcmp(ptr0, ptr1, size);
	}

	template <typename T>
	HV_FORCE_INL void* Zero(T* ptr)
	{
		HV_ASSERT_MSG(ptr, "Can't zero a nullptr's memory!");
		return memset(ptr, 0, sizeof(T));
	}
}
