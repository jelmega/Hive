// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// WeakPtr.inl: Weak pointer
#pragma once
#include "Memory/WeakPtr.h"
#include "Memory/SharedPtr.h"

namespace Hv::Memory {
	
	template <typename T, typename Deleter>
	WeakPtr<T, Deleter>::WeakPtr(const SharedPtr<T, Deleter>& ptr)
		: m_pRef(ptr.m_pRef)
	{
	}

	template <typename T, typename Deleter>
	WeakPtr<T, Deleter>::WeakPtr(const WeakPtr& ptr)
		: m_pRef(ptr.m_pRef)
	{
	}

	template <typename T, typename Deleter>
	WeakPtr<T, Deleter>::WeakPtr(WeakPtr&& ptr) noexcept
		: m_pRef(ptr.m_pRef)
	{
		ptr.m_pRef = nullptr;
	}

	template <typename T, typename Deleter>
	WeakPtr<T, Deleter>::~WeakPtr()
	{
	}

	template <typename T, typename Deleter>
	WeakPtr<T, Deleter>& WeakPtr<T, Deleter>::operator=(const SharedPtr<T, Deleter>& ptr)
	{
		m_pRef = ptr.m_pRef;
		return *this;
	}

	template <typename T, typename Deleter>
	WeakPtr<T, Deleter>& WeakPtr<T, Deleter>::operator=(const WeakPtr& ptr)
	{
		m_pRef = ptr.m_pRef;
		return *this;
	}

	template <typename T, typename Deleter>
	WeakPtr<T, Deleter>& WeakPtr<T, Deleter>::operator=(WeakPtr&& ptr) noexcept
	{
		m_pRef = ptr.m_pRef;
		ptr.m_pRef = nullptr;
		return *this;
	}

	template <typename T, typename Deleter>
	T& WeakPtr<T, Deleter>::operator*()
	{
		return m_pRef->operator*();
	}

	template <typename T, typename Deleter>
	const T& WeakPtr<T, Deleter>::operator*() const
	{
		return m_pRef->operator*();
	}

	template <typename T, typename Deleter>
	T* WeakPtr<T, Deleter>::operator->()
	{
		return m_pRef->operator->();
	}

	template <typename T, typename Deleter>
	const T* WeakPtr<T, Deleter>::operator->() const
	{
		return m_pRef->operator->();
	}

	template <typename T, typename Deleter>
	b8 WeakPtr<T, Deleter>::operator!() const
	{
		return m_pRef && m_pRef->operator*();
	}

	template <typename T, typename Deleter>
	WeakPtr<T, Deleter>::operator b8() const
	{
		return m_pRef && *m_pRef;
	}

	template <typename T, typename Deleter>
	T* WeakPtr<T, Deleter>::Get()
	{
		if (!m_pRef)
			return nullptr;
		return m_pRef->Get();
	}

	template <typename T, typename Deleter>
	const T* WeakPtr<T, Deleter>::Get() const
	{
		if (!m_pRef)
			return nullptr;
		return m_pRef->Get();
	}

	template <typename T, typename Deleter>
	Deleter& WeakPtr<T, Deleter>::GetDeleter()
	{
		HV_ASSERT(m_pRef);
		return m_pRef->GetDeleter();
	}

	template <typename T, typename Deleter>
	const Deleter& WeakPtr<T, Deleter>::GetDeleter() const
	{
		HV_ASSERT(m_pRef);
		return m_pRef->GetDeleter();
	}

	template <typename T, typename Deleter>
	sizeT WeakPtr<T, Deleter>::RefCount() const
	{
		if (!m_pRef)
			return 0;
		return m_pRef->RefCount();
	}

	template <typename T, typename Deleter>
	bool WeakPtr<T, Deleter>::IsValid() const
	{
		return m_pRef && m_pRef->IsValid();
	}

	template <typename T, typename Deleter>
	bool WeakPtr<T, Deleter>::IsUnique() const
	{
		return m_pRef && m_pRef->IsUnique();
	}

	template <typename T, typename Deleter>
	void WeakPtr<T, Deleter>::Swap(WeakPtr& ptr)
	{
		Hv::Swap(m_pRef, ptr.m_pRef);
	}

}
