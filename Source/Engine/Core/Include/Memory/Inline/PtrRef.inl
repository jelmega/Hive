// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PtrRef.inl: Pointer reference
#pragma once
#include "Memory/PtrRef.h"

namespace Hv::Memory {

	template <typename T, typename Deleter>
	HV_FORCE_INL PtrRef<T, Deleter>::PtrRef()
		: m_Ptr(nullptr)
		, m_RefCount(1)
	{
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL PtrRef<T, Deleter>::PtrRef(T* ptr)
		: m_Ptr(ptr)
		, m_RefCount(1)
	{
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL PtrRef<T, Deleter>::PtrRef(T* ptr, const Deleter& deleter)
		: m_Ptr(ptr)
		, m_Deleter(deleter)
		, m_RefCount(1)
	{
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL PtrRef<T, Deleter>::~PtrRef()
	{
		if (m_Ptr && m_RefCount == 0)
			m_Deleter(m_Ptr);
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL T& PtrRef<T, Deleter>::operator*()
	{
		return *m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL const T& PtrRef<T, Deleter>::operator*() const
	{
		return *m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL T* PtrRef<T, Deleter>::operator->()
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL const T* PtrRef<T, Deleter>::operator->() const
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL PtrRef<T, Deleter>::operator b8() const
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL b8 PtrRef<T, Deleter>::operator!() const
	{
		return !m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL void PtrRef<T, Deleter>::Ref()
	{
		++m_RefCount;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL void PtrRef<T, Deleter>::Deref()
	{
		--m_RefCount;
		if (m_Ptr && m_RefCount == 0)
		{
			m_Deleter(m_Ptr);
			m_Ptr = nullptr;
		}
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL T* PtrRef<T, Deleter>::Get()
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL const T* PtrRef<T, Deleter>::Get() const
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL Deleter& PtrRef<T, Deleter>::GetDeleter()
	{
		return m_Deleter;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL const Deleter& PtrRef<T, Deleter>::GetDeleter() const
	{
		return m_Deleter;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL sizeT PtrRef<T, Deleter>::RefCount() const
	{
		return m_RefCount;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL b8 PtrRef<T, Deleter>::IsValid() const
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL b8 PtrRef<T, Deleter>::IsUnique() const
	{
		return m_RefCount == 1;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL void PtrRef<T, Deleter>::Swap(PtrRef& ref)
	{
		Hv::Swap(m_Ptr, ref.m_Ptr);
		Hv::Swap(m_Deleter, ref.m_Deleter);
		Hv::Swap(m_RefCount, ref.m_RefCount);
	}

	////////////////////////////////////////////////////////////////////////////////

	template <typename T, typename Deleter>
	HV_FORCE_INL PtrRef<T[], Deleter>::PtrRef()
		: m_Ptr(nullptr)
		, m_RefCount(1)
	{
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL PtrRef<T[], Deleter>::PtrRef(T* ptr)
		: m_Ptr(ptr)
		, m_RefCount(1)
	{
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL PtrRef<T[], Deleter>::PtrRef(T* ptr, const Deleter& deleter)
		: m_Ptr(ptr)
		, m_Deleter(deleter)
		, m_RefCount(1)
	{
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL PtrRef<T[], Deleter>::~PtrRef()
	{
		if (m_Ptr && m_RefCount == 0)
			m_Deleter(m_Ptr);
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL T& PtrRef<T[], Deleter>::operator*()
	{
		return *m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL const T& PtrRef<T[], Deleter>::operator*() const
	{
		return *m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL T* PtrRef<T[], Deleter>::operator->()
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL const T* PtrRef<T[], Deleter>::operator->() const
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL PtrRef<T[], Deleter>::operator b8() const
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL b8 PtrRef<T[], Deleter>::operator!() const
	{
		return !m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL void PtrRef<T[], Deleter>::Ref()
	{
		++m_RefCount;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL void PtrRef<T[], Deleter>::Deref()
	{
		--m_RefCount;
		if (m_Ptr && m_RefCount == 0)
		{
			m_Deleter(m_Ptr);
			m_Ptr = nullptr;
		}
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL T* PtrRef<T[], Deleter>::Get()
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL const T* PtrRef<T[], Deleter>::Get() const
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL Deleter& PtrRef<T[], Deleter>::GetDeleter()
	{
		return m_Deleter;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL const Deleter& PtrRef<T[], Deleter>::GetDeleter() const
	{
		return m_Deleter;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL sizeT PtrRef<T[], Deleter>::RefCount() const
	{
		return m_RefCount;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL b8 PtrRef<T[], Deleter>::IsValid() const
	{
		return m_Ptr;
	}

	template <typename T, typename Deleter>
	HV_FORCE_INL b8 PtrRef<T[], Deleter>::IsUnique() const
	{
		return m_RefCount == 1;
	}

	template <typename T, typename Deleter>
	void PtrRef<T[], Deleter>::Swap(PtrRef& ref)
	{
		Hv::Swap(m_Ptr, ref.m_Ptr);
		Hv::Swap(m_Deleter, ref.m_Deleter);
		Hv::Swap(m_RefCount, ref.m_RefCount);
	}
	
}