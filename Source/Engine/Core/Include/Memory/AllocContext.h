// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// AllocContext.h: Allocation context
#pragma once
#include "Core/CoreHeaders.h"

namespace Hv::Memory {

	/**
	 * Allocation category
	 */
	enum class AllocCategory
	{
		None,		/**< No category */
		Allocator,	/**< Memory allocated for allocators */
		NewDelete,	/**< Memory allocated with new and freed with delete */
		Container,	/**< Memory allocated for containers */
		String,		/**< Memory allocated for strings */
		HAL,		/**< Menory allocated for HAL */
		RHI,		/**< Menory allocated for RHI */
	};

	/**
	 * Allocation context (additional info about allocation)
	 */
	struct AllocContext
	{
		/**
		 * Create a default/null allocation context
		 */
		HV_FORCE_INL AllocContext()
			: category(AllocCategory::None)
			, file("")
			, function("")
			, line(0)
		{
		}

		/**
		 * Create an allocation context
		 * @param[in] file		File in which memory is allocated
		 * @param[in] function	Function in which memory is allocated
		 * @param[in] line		Line on which memory is allocated
		 */
		HV_FORCE_INL AllocContext(AllocCategory category, const AnsiChar* file, const AnsiChar* function, u32 line)
			: category(category)
			, file(file)
			, function(function)
			, line(line)
		{
		}

		const AllocCategory category;	/**< Allocation category */
		const AnsiChar* file;			/**< File in which memory is allocated */
		const AnsiChar* function;		/**< Function in which memory is allocated */
		const u32 line;					/**< Line on which memory is allocated */
	};


}