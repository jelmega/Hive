// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// SmartPtrs.h: Include file for 'smart' pointers
#pragma once

#include "PtrRef.h"
#include "UniquePtr.h"
#include "SharedPtr.h"
#include "WeakPtr.h"

#include "Inline/PtrRef.inl"
#include "Inline/UniquePtr.inl"
#include "Inline/SharedPtr.inl"
#include "Inline/WeakPtr.inl"
