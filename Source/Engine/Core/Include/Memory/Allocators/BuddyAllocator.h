// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// BuddyAllocator.h: Buddy allocator
#pragma once
#include "IAllocator.h"

namespace Hv::Memory {

	class HIVE_API BuddyAllocator final : public IAllocator
	{
	public:
		/**
		 * Default constructor
		 */
		BuddyAllocator();
		/**
		 * Constructor
		 * @param[in] maxSize	Maximum size of buddy
		 * @param[in] minSize	Minimum size of buddy
		 * @param[in] alignment	Internal buffer alignment
		 * @param[in] name		Debug name
		 */
		BuddyAllocator(sizeT maxSize, sizeT minSize, u16 alignment, const AnsiChar* name = "Buddy Allocator");
		/**
		 * Constructor
		 * @param[in] pBuffer			Internal buffer (size = maxSize + ((16 for x86, 32 for x64) * (((maxSize / minSize) * 2) - 1) + (alignment - 1) & ~(alignment - 1))
		 * @param[in] maxSize			Maximum size of buddy
		 * @param[in] minSize			Minimum size of buddy
		 * @param[in] alignment			Internal buffer alignment
		 * @param[in] transferOwnership	If Ownership is transfered to the allocator (memory needs to be allocated with Memory::Allocate!)
		 * @param[in] name				Debug name
		 */
		BuddyAllocator(u8* pBuffer, sizeT maxSize, sizeT minSize, u16 alignment, b8 transferOwnership = false, const AnsiChar* name = "Buddy Allocator");
		/**
		 * Move an allocator into this allocator
		 * @param[in] alloc	Allocator to move into this allocator
		 */
		BuddyAllocator(BuddyAllocator&& alloc) noexcept;
		~BuddyAllocator();

		BuddyAllocator& operator=(BuddyAllocator&& alloc) noexcept;

		/**
		 * Allocate memory (8 or 16 byte overhead)
		 * @param[in] size		Size to allocate
		 * @param[in] alignment	Alignment of the allocation
		 * @param[in] context	Alloc context
		 * @return				Pointer to allocated memory or nullptr if allocation failed
		 */
		void* Allocate(sizeT size, u16 alignment, AllocContext context = AllocContext()) override final;
		/**
		 * Reallocate memory
		 * @param[in] pOriginal	Original memory
		 * @param[in] size		New size of the allocation
		 * @param[in] alignment	New alignment of the allocation
		 * @param[in] context	Alloc context
		 * @return				Pointer to reallocated memory or nullptr, is size == 0 or reallocation failed
		 * @note				When a nullptr is returned and size != 0, reallocation failed and data is still in the original location
		 */
		void* Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context = AllocContext()) override final;
		/**
		 * Free memory
		 * @param[in] ptr		Memory to free
		 * @param[in] context	Alloc context
		 */
		void Free(void* ptr, AllocContext context = AllocContext()) override final;
		/**
		 * Check if the pointer is allocated using the allocator
		 * @return True if the pointer is allocated using the allocator, false otherwise
		 */
		b8 Owns(void* ptr) override final;


	private:
		HV_MAKE_NON_COPYABLE(BuddyAllocator);
		friend class PagedBuddyAllocator;

		sizeT m_MinSize;		/**< Max buddy size */
		sizeT m_MaxSize;		/**< Min buddy size */
		u16 m_Levels;			/**< Number of buddy levels */
		sizeT m_Offset;			/**< Offset to actual data */

		struct BuddyHeader
		{
			sizeT size;			/**< Size */
			sizeT offset;		/**< offset to chunk */
			u16 buddy;			/**< Buddy index */
			u16 child;			/**< Child index */
			u16 parent;			/**< Parent index */
			b8 used;			/**< Amount of data used in block */
			b8 isSplit;			/**< Chunk is split */
#if HV_STATISTICS
			sizeT allocSize;	/**< Size of allocated data (statistics) */
#endif
		};
	};

}
