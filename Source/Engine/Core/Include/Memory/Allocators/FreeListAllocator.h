// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// FreeListAllocator.h: Freelist allocator
#pragma once
#include "IAllocator.h"

namespace Hv::Memory {

	class HIVE_API FreeListAllocator final : public IAllocator
	{
	public:
		/**
		 * Default constructor
		 */
		FreeListAllocator();
		/**
		 * Constructor
		 * @param[in] size		Internal buffer size
		 * @param[in] alignment	Internal buffer alignment
		 * @param[in] name		Debug name
		 */
		FreeListAllocator(sizeT size, u16 alignment, const AnsiChar* name = "Freelist Allocator");
		/**
		 * Constructor
		 * @param[in] pBuffer			Internal buffer
		 * @param[in] size				Internal buffer size
		 * @param[in] alignment			Internal buffer alignment
		 * @param[in] transferOwnership	If Ownership is transfered to the allocator (memory needs to be allocated with Memory::Allocate!)
		 * @param[in] name				Debug name
		 */
		FreeListAllocator(u8* pBuffer, sizeT size, u16 alignment, b8 transferOwnership = false, const AnsiChar* name = "Freelist Allocator");
		/**
		 * Move an allocator into this allocator
		 * @param[in] alloc	Allocator to move into this allocator
		 */
		FreeListAllocator(FreeListAllocator&& alloc) noexcept;
		~FreeListAllocator();

		FreeListAllocator& operator=(FreeListAllocator&& alloc) noexcept;

		/**
		 * Allocate memory (8 or 16 byte overhead)
		 * @param[in] size		Size to allocate
		 * @param[in] alignment	Alignment of the allocation
		 * @param[in] context	Alloc context
		 */
		void* Allocate(sizeT size, u16 alignment, AllocContext context = AllocContext()) override final;
		/**
		 * Reallocate memory
		 * @param[in] pOriginal	Original memory
		 * @param[in] size		New size of the allocation
		 * @param[in] alignment	New alignment of the allocation
		 * @param[in] context	Alloc context
		 */
		void* Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context = AllocContext()) override final;
		/**
		 * Free memory
		 * @param[in] ptr		Memory to free
		 * @param[in] context	Alloc context
		 */
		void Free(void* ptr, AllocContext context = AllocContext()) override final;
		/**
		 * Check if the pointer is allocated using the allocator
		 * @return True if the pointer is allocated using the allocator, false otherwise
		 */
		b8 Owns(void* ptr) override final;


	private:
		HV_MAKE_NON_COPYABLE(FreeListAllocator);

		struct FreeHeader
		{
			u8* pNext;		/**< Next entry */
			sizeT size;		/**< Size */
		};

		struct Header
		{
			sizeT size;		/**< Size */
			u16 padding;	/**< Padding */
			u8 backPadding;	/**< Back padding */
		};

		/**
		 * Find the first freespace that fits the memory
		 * @param[in] size		Size of allocation
		 * @param[in] alignment	Alignment of allocation
		 * @param[out] padding	Padding for memory
		 * @return				Free header
		 */
		FreeHeader* FindFreeSpace(sizeT size, u16 alignment, u16& padding, FreeHeader*& pPrevHeader);
		/**
		 * Coalesce new free header with neighbours if possible
		 * @param[in] pPrev	Previous free header
		 * @param[in] pFree	New free header
		 */
		void Coalesce(FreeHeader* pPrev, FreeHeader* pFree);
		/**
		 * Calculate the padding for an aligned memory adress with space for a header
		 * @param[in] address		Starting adress to pad from
		 * @param[in] alignment		Alignment of memory to pad for
		 * @param[in] headerSize	Header size
		 * @return					Padding
		 */
		u16 CalculatePaddingWithHeader(sizeT address, u16 alignment, u16 headerSize);

		u8* m_pFreeList;			/**< Free list */
	};

}
