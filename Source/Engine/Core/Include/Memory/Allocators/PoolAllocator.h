// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PoolAllocator.h: Pool allocator
#pragma once
#include "IAllocator.h"

namespace Hv::Memory {

	/**
	 * Pool allocator
	 * @note	Usage and padded usage will be the same
	 */
	class HIVE_API PoolAllocator final : public IAllocator
	{
	public:
		/**
		 * Default constructor
		 */
		PoolAllocator();
		/**
		 * Constructor
		 * @param[in] size		Internal buffer size
		 * @param[in] alignment	Internal buffer alignment
		 * @param[in] blockSize	Size of a block in the pool
		 * @param[in] name		Debug name
		 */
		PoolAllocator(sizeT size, u16 alignment, u32 blockSize, const AnsiChar* name = "Pool Allocator");
		/**
		 * Constructor
		 * @param[in] pBuffer			Internal buffer
		 * @param[in] size				Internal buffer size
		 * @param[in] alignment			Internal buffer alignment
		 * @param[in] blockSize			Size of a block in the pool
		 * @param[in] transferOwnership	If Ownership is transfered to the allocator (memory needs to be allocated with Memory::Allocate!)
		 * @param[in] name				Debug name
		 */
		PoolAllocator(u8* pBuffer, sizeT size, u16 alignment, u32 blockSize, b8 transferOwnership = false, const AnsiChar* name = "Pool Allocator");
		/**
		 * Move an allocator into this allocator
		 * @param[in] alloc	Allocator to move into this allocator
		 */
		PoolAllocator(PoolAllocator&& alloc) noexcept;
		~PoolAllocator();

		PoolAllocator& operator=(PoolAllocator&& alloc) noexcept;

		/**
		 * Allocate memory (8 or 16 byte overhead)
		 * @param[in] size		Size to allocate
		 * @param[in] alignment	Alignment of the allocation
		 * @param[in] context	Alloc context
		 * @return				Pointer to allocated memory or nullptr if allocation failed
		 */
		void* Allocate(sizeT size, u16 alignment, AllocContext context = AllocContext()) override final;
		/**
		 * Reallocate memory
		 * @param[in] pOriginal	Original memory
		 * @param[in] size		New size of the allocation
		 * @param[in] alignment	New alignment of the allocation
		 * @param[in] context	Alloc context
		 * @return				Pointer to reallocated memory or nullptr, is size == 0 or reallocation failed
		 * @note				When a nullptr is returned and size != 0, reallocation failed and data is still in the original location
		 */
		void* Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context = AllocContext()) override final;
		/**
		 * Free memory
		 * @param[in] ptr		Memory to free
		 * @param[in] context	Alloc context
		 */
		void Free(void* ptr, AllocContext context = AllocContext()) override final;
		/**
		 * Check if the pointer is allocated using the allocator
		 * @return True if the pointer is allocated using the allocator, false otherwise
		 */
		b8 Owns(void* ptr) override final;


	private:
		HV_MAKE_NON_COPYABLE(PoolAllocator);
		friend class PagedPoolAllocator;

		u32 m_BlockSize;
		u32 m_AlignedBlockSize;

		/**
		 * Free block
		 */
		struct FreeBlock
		{
			FreeBlock* pNext;	/**< Next free block */
		}* m_pFree;				/**< First free block */

	};

}
