// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PagedFreeListAllocator.h: Paged freelist allocator
#pragma once
#include "IPagedAllocator.h"
#include "FreelistAllocator.h"

namespace Hv::Memory {
	
	class PagedFreeListAllocator final : public IPagedAllocator<FreeListAllocator>
	{	
		public:
		/**
		 * Default constructor
		 */
		PagedFreeListAllocator();
		/**
		 * Constructor with debug name
		 * @param[in] size		Page buffer size
		 * @param[in] alignment	Page buffer alignment
		 * @param[in] name	Debug name
		 */
		PagedFreeListAllocator(sizeT size, u16 alignment, const AnsiChar* name);
		/**
		 * Move an allocator into this allocator
		 * @param[in] alloc	Allocator to move into this allocator
		 */
		PagedFreeListAllocator(PagedFreeListAllocator&& alloc) noexcept;
		virtual ~PagedFreeListAllocator();

		PagedFreeListAllocator& operator=(PagedFreeListAllocator&& alloc) noexcept;

		/**
		 * Allocate memory
		 * @param[in] size		Size to allocate
		 * @param[in] alignment	Alignment of the allocation
		 * @param[in] context	Alloc context
		 * @return				Pointer to allocated memory or nullptr if allocation failed
		 */
		void* Allocate(sizeT size, u16 alignment, AllocContext context = AllocContext()) override final;
		/**
		 * Reallocate memory
		 * @param[in] pOriginal	Original memory
		 * @param[in] size		New size of the allocation
		 * @param[in] alignment	New alignment of the allocation
		 * @param[in] context	Alloc context
		 * @return				Pointer to reallocated memory or nullptr, is size == 0 or reallocation failed
		 * @note				When a nullptr is returned and size != 0, reallocation failed and data is still in the original location
		 */
		void* Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context = AllocContext()) override final;
		/**
		 * Free memory
		 * @param[in] ptr		Memory to free
		 * @param[in] context	Alloc context
		 */
		void Free(void* ptr, AllocContext context = AllocContext()) override final;
		/**
		 * Check if the pointer is allocated using the allocator
		 * @return True if the pointer is allocated using the allocator, false otherwise
		 */
		b8 Owns(void* ptr) override final;

	protected:
		HV_MAKE_NON_COPYABLE(PagedFreeListAllocator);

		/**
		 * Create a new page
		 * @return	New page
		 */
		virtual Page* CreatePage();

	};

}
