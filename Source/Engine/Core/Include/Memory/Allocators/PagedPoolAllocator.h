// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PagedPoolAllocator.h: Paged pool allocator
#pragma once
#include "IPagedAllocator.h"
#include "PoolAllocator.h"

namespace Hv::Memory {

	class PagedPoolAllocator final : public IPagedAllocator<PoolAllocator>
	{
	public:
		/**
		 * Default constructor
		 */
		PagedPoolAllocator();
		/**
		 * Constructor with debug name
		 * @param[in] size		Internal buffer size
		 * @param[in] alignment	Internal buffer alignment
		 * @param[in] blockSize	Size of a block in the pool
		 * @param[in] name		Debug name
		 */
		PagedPoolAllocator(sizeT size, u16 alignment, u32 blockSize, const AnsiChar* name);
		/**
		 * Move an allocator into this allocator
		 * @param[in] alloc	Allocator to move into this allocator
		 */
		PagedPoolAllocator(PagedPoolAllocator&& alloc) noexcept;
		virtual ~PagedPoolAllocator();

		PagedPoolAllocator& operator=(PagedPoolAllocator&& alloc) noexcept;

		/**
		 * Allocate memory
		 * @param[in] size		Size to allocate
		 * @param[in] alignment	Alignment of the allocation
		 * @param[in] context	Alloc context
		 * @return				Pointer to allocated memory or nullptr if allocation failed
		 */
		void* Allocate(sizeT size, u16 alignment, AllocContext context = AllocContext()) override final;
		/**
		 * Reallocate memory
		 * @param[in] pOriginal	Original memory
		 * @param[in] size		New size of the allocation
		 * @param[in] alignment	New alignment of the allocation
		 * @param[in] context	Alloc context
		 * @return				Pointer to reallocated memory or nullptr, is size == 0 or reallocation failed
		 * @note				When a nullptr is returned and size != 0, reallocation failed and data is still in the original location
		 */
		void* Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context = AllocContext()) override final;
		/**
		 * Free memory
		 * @param[in] ptr		Memory to free
		 * @param[in] context	Alloc context
		 */
		void Free(void* ptr, AllocContext context = AllocContext()) override final;
		/**
		 * Check if the pointer is allocated using the allocator
		 * @return True if the pointer is allocated using the allocator, false otherwise
		 */
		b8 Owns(void* ptr) override final;

	protected:
		HV_MAKE_NON_COPYABLE(PagedPoolAllocator);

		u32 m_BlockSize;	/**< Pool allocator block size */
	
		/**
		 * Create a new page
		 * @return	New page
		 */
		virtual Page* CreatePage();

	};

}
