// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Mallocator.h: 'Malloc' allocator
#pragma once
#include "IAllocator.h"

namespace Hv::Memory {

	class HIVE_API Mallocator final : public IAllocator
	{
	public:
		/**
		 * Constructor
		 * @param[in] name		Debug name
		 */
		Mallocator(const AnsiChar* name = "Mallocator");
		/**
		 * Move an allocator into this allocator
		 * @param[in] alloc	Allocator to move into this allocator
		 */
		Mallocator(Mallocator&& alloc) noexcept;
		~Mallocator();

		Mallocator& operator=(Mallocator&& alloc) noexcept;

		/**
		 * Allocate memory (8 or 16 byte overhead)
		 * @param[in] size		Size to allocate
		 * @param[in] alignment	Alignment of the allocation
		 * @param[in] context	Alloc context
		 */
		void* Allocate(sizeT size, u16 alignment, AllocContext context = AllocContext()) override final;
		/**
		 * Reallocate memory
		 * @param[in] pOriginal	Original memory
		 * @param[in] size		New size of the allocation
		 * @param[in] alignment	New alignment of the allocation
		 * @param[in] context	Alloc context
		 */
		void* Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context = AllocContext()) override final;
		/**
		 * Free memory
		 * @param[in] ptr		Memory to free
		 * @param[in] context	Alloc context
		 */
		void Free(void* ptr, AllocContext context = AllocContext()) override final;
		/**
		 * Check if the pointer is allocated using the allocator
		 * @return True if the pointer is allocated using the allocator, false otherwise
		 */
		b8 Owns(void* ptr) override final;


	private:
		HV_MAKE_NON_COPYABLE(Mallocator);

		/**
		 * Mallocator header
		 */
		struct Header
		{
			sizeT size;								/**< Size */
			u16 padding;							/**< Padding */
			u16 check;								/**< Check to know if allocation was made by mallocator, no 100% certainty */
		};

		static constexpr u16 m_CheckCode = 0xA10C;	/**< Check code (ALOC) */
	};

}
