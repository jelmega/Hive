// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// IAllocator.h: Allocator interface
#pragma once
#include "HAL/HalMemory.h"
#include "Memory/MemoryUtil.h"

namespace Hv::Memory {
	
	class HIVE_API IAllocator
	{
	public:
		/**
		 * Default constructor
		 */
		IAllocator();
		/**
		 * Constructor with debug name
		 * @param[in] name	Debug name
		 */
		explicit IAllocator(const AnsiChar* name);
		/**
		 * Move an allocator into this allocator
		 * @param[in] alloc	Allocator to move into this allocator
		 */
		IAllocator(IAllocator&& alloc) noexcept;
		virtual ~IAllocator();

		IAllocator& operator=(IAllocator&& alloc) noexcept;

		/**
		 * Allocate memory
		 * @param[in] size		Size to allocate
		 * @param[in] alignment	Alignment of the allocation
		 * @param[in] context	Alloc context
		 * @return				Pointer to allocated memory or nullptr if allocation failed
		 */
		virtual	void* Allocate(sizeT size, u16 alignment, AllocContext context = AllocContext()) = 0;
		/**
		 * Reallocate memory
		 * @param[in] pOriginal	Original memory
		 * @param[in] size		New size of the allocation
		 * @param[in] alignment	New alignment of the allocation
		 * @param[in] context	Alloc context
		 * @return				Pointer to reallocated memory or nullptr, is size == 0 or reallocation failed
		 * @note				When a nullptr is returned and size != 0, reallocation failed and data is still in the original location
		 */
		virtual	void* Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context = AllocContext()) = 0;
		/**
		 * Free memory
		 * @param[in] ptr		Memory to free
		 * @param[in] context	Alloc context
		 */
		virtual void Free(void* ptr, AllocContext context = AllocContext()) = 0;
		/**
		 * Check if the pointer is allocated using the allocator
		 * @return True if the pointer is allocated using the allocator, false otherwise
		 */
		virtual b8 Owns(void* ptr) = 0;

		/**
		 * Get the size of the internal buffer
		 * @return	Size of the internal buffer
		 */
		HV_FORCE_INL sizeT GetInternalSize() const { return m_Size; }
		/**
		 * Get alignment of the internal buffer
		 * @return	Alignment of the internal buffer
		 */
		HV_FORCE_INL u16 GetInternalAlignment() const { return m_Alignment; }
		/**
		 * Check if the allocator owns the 
		 * @return	True if the allocator owns the internal buffer, false otherwise
		 */
		HV_FORCE_INL b8 OwnsInternalBuffer() const { return m_OwnsBuffer; }
		/**
		 * Get the internal buffer
		 * @return	Internal buffer
		 */
		HV_FORCE_INL u8* GetInternalBuffer() { return m_pBuffer; }
		/**
		 * Get the internal buffer
		 * @return	Internal buffer
		 */
		HV_FORCE_INL const u8* GetInternalBuffer() const { return m_pBuffer; }

		/**
		 * Get number of allocation
		 * @return	Number of allocations
		 */
		HV_FORCE_INL sizeT GetNumAllocations() const { return m_NumAllocs; }
		/**
		 * Get peak number of allocation
		 * @return	Peak number of allocations
		 */
		HV_FORCE_INL sizeT GetPeakNumAllocations() const { return m_PeakNumAllocs; }
		/**
		 * Get the memory usage
		 * @return	Memory usage  
		 */
		HV_FORCE_INL sizeT GetUsage() const { return m_Usage; }
		/**
		 * Get the peak memory usage
		 * @return	Peak memory usage  
		 */
		HV_FORCE_INL sizeT GetPeakUsage() const { return m_PeakUsage; }
		/**
		 * Get the padded memory usage
		 * @return	Padded memory usage
		 */
		HV_FORCE_INL sizeT GetPaddedUsage() const { return m_PaddedUsage; }
		/**
		 * Get the peak padded memory usage
		 * @return	Peak padded memory usage
		 */
		HV_FORCE_INL sizeT GetPeakPaddedUsage() const { return m_PeakPaddedUsage; }
		/**
		 * Get the debug name
		 * @return	Debug name
		 */
		HV_FORCE_INL const AnsiChar* GetName() const { return m_Name; }

	protected:
		HV_MAKE_NON_COPYABLE(IAllocator);

		sizeT m_Size;				/**< Size of the internal buffer, 0 if not used */
		u16 m_Alignment;			/**< Alignment of the internal buffer, 0 if not used */
		b8 m_OwnsBuffer;			/**< If the allocator owns the buffer */
		u8* m_pBuffer;				/**< Internal buffer, nullptr if not used */

		sizeT m_NumAllocs;			/**< Number of allocations */
		sizeT m_PeakNumAllocs;		/**< Peak number of allocations */
		sizeT m_Usage;				/**< Memory usage */
		sizeT m_PeakUsage;			/**< Peak memory usage */
		sizeT m_PaddedUsage;		/**< Padded memory usage */
		sizeT m_PeakPaddedUsage;	/**< Peak padded memory usage */

		const AnsiChar* m_Name;		/**< Debug name */
	};

}
