// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// IPagedAllocator.inl: Paged allocator interface
#pragma once
#include "Memory/Allocators/IPagedAllocator.h"

namespace Hv::Memory {

#if HV_STATISTICS
#	define SUB_STATS(alloc)\
		m_Usage -= alloc.GetUsage();\
		m_PaddedUsage -= alloc.GetPaddedUsage();

#	define ADD_STATS(alloc)\
		m_Usage += alloc.GetUsage();\
		if (m_PeakUsage < m_Usage)\
			m_PeakUsage = m_Usage;\
		m_PaddedUsage += alloc.GetPaddedUsage();\
		if (m_PeakPaddedUsage < m_PaddedUsage)\
			m_PeakPaddedUsage = m_PaddedUsage;

#	define INC_ALLOCS\
		++m_NumAllocs;\
		if (m_PeakNumAllocs < m_NumAllocs)\
			m_PeakNumAllocs = m_NumAllocs;

#	define DEC_ALLOCS\
		--m_NumAllocs;

#else
#	define SUB_STATS(alloc)
#	define ADD_STATS(alloc)
#	define INC_ALLOCS
#	define DEC_ALLOCS
#endif
	
	template <typename Alloc>
	IPagedAllocator<Alloc>::IPagedAllocator()
		: IAllocator()
		, m_pFirst(nullptr)
		, m_pLast(nullptr)
	{
	}

	template <typename Alloc>
	IPagedAllocator<Alloc>::IPagedAllocator(const AnsiChar* name)
		: IAllocator(name)
		, m_pFirst(nullptr)
		, m_pLast(nullptr)
	{
	}

	template <typename Alloc>
	IPagedAllocator<Alloc>::IPagedAllocator(sizeT size, u16 alignment, const AnsiChar* name)
		: IAllocator(name)
		, m_pFirst(nullptr)
		, m_pLast(nullptr)
	{
		m_Size = size;
		m_Alignment = alignment;
	}

	template <typename Alloc>
	IPagedAllocator<Alloc>::IPagedAllocator(IPagedAllocator&& alloc) noexcept
		: IAllocator(Forward<IPagedAllocator<Alloc>>(alloc))
		, m_pFirst(alloc.m_pFirst)
		, m_pLast(alloc.m_pLast)
	{
		alloc.m_pFirst = alloc.m_pLast = nullptr;
	}

	template <typename Alloc>
	IPagedAllocator<Alloc>::~IPagedAllocator()
	{
		Page* pPage = m_pFirst;
		while (pPage)
		{
			Page* pNext = pPage->pNext;
			pPage->alloc.~Alloc();
			Memory::Free(pPage, HV_ALLOC_CONTEXT(AllocCategory::Allocator));
			pPage = pNext;
		}
	}

	template <typename Alloc>
	IPagedAllocator<Alloc>& IPagedAllocator<Alloc>::operator=(IPagedAllocator&& alloc) noexcept
	{
		IAllocator::operator=(Forward<IPagedAllocator>(alloc));
		m_pFirst = alloc.m_pFirst;
		m_pLast = alloc.m_pLast;
		return *this;
	}

	template <typename Alloc>
	void* IPagedAllocator<Alloc>::Allocate(sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");
		HV_ASSERT_MSG(alignment <= m_Alignment, "Alignment needs to be the smaller or equal to the allocators alignment!");

		if (size > m_Size)
			return nullptr;

		Page* pPage = m_pFirst;
		u8* ptr = nullptr;
		while (pPage)
		{
			SUB_STATS(pPage->alloc);
			ptr = (u8*)pPage->alloc.Allocate(size, alignment);
			ADD_STATS(pPage->alloc);
			if (ptr)
			{
				INC_ALLOCS;
				return ptr;
			}
			pPage = pPage->pNext;
		}

		// Create page and allocate
		Page* pNewPage = CreatePage();
		m_pLast->pNext = pNewPage;
		m_pLast = pNewPage;

		return pNewPage->alloc.Allocate(size, alignment);
	}

	template <typename Alloc>
	void* IPagedAllocator<Alloc>::Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);

		// Try reallocating on same page
		u8* ptr = nullptr;
		Page* pPage = m_pFirst;
		while (pPage)
		{
			if (pPage->alloc.Owns(pOriginal))
			{
				SUB_STATS(pPage->alloc);
				ptr = (u8*)pPage->alloc.Reallocate(pOriginal, size, alignment);
				ADD_STATS(pPage->alloc);
				break;
			}
		}

		// Failed to reallocate on the page, allocate and free on different pages
		// size == 0 and pOriginal == nullptr, should be handled by Reallocate function of Alloc
		if (!ptr && size)
		{
			ptr = (u8*)Allocate(size, alignment);
			Memory::Copy(ptr, pOriginal, size);
			Free(pOriginal);
		}
		else if (pOriginal && !size)
		{
			DEC_ALLOCS;
		}
		else if (!pOriginal && size)
		{
			INC_ALLOCS;
		}

		return ptr;
	}

	template <typename Alloc>
	void IPagedAllocator<Alloc>::Free(void* ptr, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		Page* pPage = m_pFirst;
		while (pPage)
		{
			if (pPage->alloc.Owns(ptr))
			{
				SUB_STATS(pPage->alloc);
				pPage->alloc.Free(ptr);
				ADD_STATS(pPage->alloc);
				DEC_ALLOCS;
				break;
			}
			pPage = pPage->pNext;
		}
	}

	template <typename Alloc>
	b8 IPagedAllocator<Alloc>::Owns(void* ptr)
	{
		Page* pPage = m_pFirst;
		while (pPage)
		{
			if (pPage->alloc.Owns(ptr))
				return true;
			pPage = pPage->pNext;
		}
		return false;
	}

	/*
	 * CreatePage example
	 *
	 * template <typename Alloc>
	 * typename IPagedAllocator<Alloc>::Page* IPagedAllocator<Alloc>::CreatePage(Alloc&& alloc)
	 * {
	 *		Page* pPage = (Page*)Memory::Allocate(sizeof(Page), 8, HV_ALLOC_CONTEXT(AllocCategory::Allocator));
	 *		pPage->pNext = nullptr;
	 *		pPage->alloc = Forward<Alloc>(alloc);
	 *		return pPage;
	 * }
	 */

#undef ADD_STATS
#undef SUB_STATS
#undef INC_ALLOCS
#undef DEC_ALLOCS

}
