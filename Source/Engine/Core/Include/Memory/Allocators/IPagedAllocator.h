// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// IPagedAllocator.h: Paged allocator interface
#pragma once
#include "IAllocator.h"

namespace Hv::Memory {
	
	/**
	 * Paged allocator interface
	 * @note To be able to use memory contexts, handle context manually and call respective IPageAllocator function
	 */
	template<typename Alloc>
	class IPagedAllocator : public IAllocator
	{
		HV_STATIC_ASSERT_MSG((Traits::IsBaseOfV<IAllocator, Alloc>), "Alloc needs to derive from IAllocator");
		HV_STATIC_ASSERT_MSG(!(Traits::IsBaseOfV<IPagedAllocator, Alloc>), "Alloc can't derive from IAllocator");

	protected:
		struct Page
		{
			Page* pNext;
			Alloc alloc;
		};

	public:
		/**
		 * Default constructor
		 */
		IPagedAllocator();
		/**
		 * Constructor with debug name
		 * @param[in] name	Debug name
		 */
		explicit IPagedAllocator(const AnsiChar* name);
		/**
		 * Constructor with debug name
		 * @param[in] size		Page buffer size
		 * @param[in] alignment	Page buffer alignment
		 * @param[in] name	Debug name
		 */
		IPagedAllocator(sizeT size, u16 alignment, const AnsiChar* name);
		/**
		 * Move an allocator into this allocator
		 * @param[in] alloc	Allocator to move into this allocator
		 */
		IPagedAllocator(IPagedAllocator&& alloc) noexcept;
		virtual ~IPagedAllocator();

		IPagedAllocator& operator=(IPagedAllocator&& alloc) noexcept;

		/**
		 * Allocate memory
		 * @param[in] size		Size to allocate
		 * @param[in] alignment	Alignment of the allocation
		 * @param[in] context	Alloc context
		 * @return				Pointer to allocated memory or nullptr if allocation failed
		 */
		virtual	void* Allocate(sizeT size, u16 alignment, AllocContext context = AllocContext()) override;
		/**
		 * Reallocate memory
		 * @param[in] pOriginal	Original memory
		 * @param[in] size		New size of the allocation
		 * @param[in] alignment	New alignment of the allocation
		 * @param[in] context	Alloc context
		 * @return				Pointer to reallocated memory or nullptr, is size == 0 or reallocation failed
		 * @note				When a nullptr is returned and size != 0, reallocation failed and data is still in the original location
		 */
		virtual	void* Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context = AllocContext()) override;
		/**
		 * Free memory
		 * @param[in] ptr		Memory to free
		 * @param[in] context	Alloc context
		 */
		virtual void Free(void* ptr, AllocContext context = AllocContext()) override;
		/**
		 * Check if the pointer is allocated using the allocator
		 * @return True if the pointer is allocated using the allocator, false otherwise
		 */
		virtual b8 Owns(void* ptr) override;

	protected:
		HV_MAKE_NON_COPYABLE(IPagedAllocator);

		/**
		 * Create a new page
		 * @return	New page
		 */
		virtual Page* CreatePage() = 0;

		Page* m_pFirst;
		Page* m_pLast;
	};
}

#include "Inline/IPageAllocator.inl"