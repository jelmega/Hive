// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PagedBuddyAllocator.h: Paged buddy allocator
#pragma once
#include "IPagedAllocator.h"
#include "BuddyAllocator.h"

namespace Hv::Memory {

	class PagedBuddyAllocator final : public IPagedAllocator<BuddyAllocator>
	{
	public:
		/**
		 * Default constructor
		 */
		PagedBuddyAllocator();
		/**
		 * Constructor with debug name
		 * @param[in] maxSize	Maximum size of buddy
		 * @param[in] minSize	Minimum size of buddy
		 * @param[in] alignment	Page buffer alignment
		 * @param[in] name	Debug name
		 */
		PagedBuddyAllocator(sizeT minSize, sizeT maxSize, u16 alignment, const AnsiChar* name);
		/**
		 * Move an allocator into this allocator
		 * @param[in] alloc	Allocator to move into this allocator
		 */
		PagedBuddyAllocator(PagedBuddyAllocator&& alloc) noexcept;
		virtual ~PagedBuddyAllocator();

		PagedBuddyAllocator& operator=(PagedBuddyAllocator&& alloc) noexcept;

		/**
		 * Allocate memory
		 * @param[in] size		Size to allocate
		 * @param[in] alignment	Alignment of the allocation
		 * @param[in] context	Alloc context
		 * @return				Pointer to allocated memory or nullptr if allocation failed
		 */
		void* Allocate(sizeT size, u16 alignment, AllocContext context = AllocContext()) override final;
		/**
		 * Reallocate memory
		 * @param[in] pOriginal	Original memory
		 * @param[in] size		New size of the allocation
		 * @param[in] alignment	New alignment of the allocation
		 * @param[in] context	Alloc context
		 * @return				Pointer to reallocated memory or nullptr, is size == 0 or reallocation failed
		 * @note				When a nullptr is returned and size != 0, reallocation failed and data is still in the original location
		 */
		void* Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context = AllocContext()) override final;
		/**
		 * Free memory
		 * @param[in] ptr		Memory to free
		 * @param[in] context	Alloc context
		 */
		void Free(void* ptr, AllocContext context = AllocContext()) override final;
		/**
		 * Check if the pointer is allocated using the allocator
		 * @return True if the pointer is allocated using the allocator, false otherwise
		 */
		b8 Owns(void* ptr) override final;

	protected:
		HV_MAKE_NON_COPYABLE(PagedBuddyAllocator);

		sizeT m_MinSize;	/**< Min block size */
	
		/**
		 * Create a new page
		 * @return	New page
		 */
		virtual Page* CreatePage();

	};

}
