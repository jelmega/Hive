// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// StackAllocator.h: Stack allocator
#pragma once
#include "IAllocator.h"

namespace Hv::Memory {
	
	/**
	* Linear allocator, can only reallocate or free in the reverse order of allocations and can reset
	*/
	class HIVE_API StackAllocator final : public IAllocator
	{
	public:
		/**
		 * Default constructor
		 */
		StackAllocator();
		/**
		 * Constructor
		 * @param[in] size		Internal buffer size
		 * @param[in] alignment	Internal buffer alignment
		 * @param[in] name		Debug name
		 */
		StackAllocator(sizeT size, u16 alignment, const AnsiChar* name = "Stack Allocator");
		/**
		 * Constructor
		 * @param[in] pBuffer			Internal buffer
		 * @param[in] size				Internal buffer size
		 * @param[in] alignment			Internal buffer alignment
		 * @param[in] transferOwnership	If Ownership is transfered to the allocator (memory needs to be allocated with Memory::Allocate!)
		 * @param[in] name				Debug name
		 */
		StackAllocator(u8* pBuffer, sizeT size, u16 alignment, b8 transferOwnership = false, const AnsiChar* name = "Stack Allocator");
		/**
		 * Move an allocator into this allocator
		 * @param[in] alloc	Allocator to move into this allocator
		 */
		StackAllocator(StackAllocator&& alloc) noexcept;
		~StackAllocator();

		StackAllocator& operator=(StackAllocator&& alloc) noexcept;

		/**
		 * Allocate memory (8 or 16 byte overhead)
		 * @param[in] size		Size to allocate
		 * @param[in] alignment	Alignment of the allocation
		 * @param[in] context	Alloc context
		 */
		void* Allocate(sizeT size, u16 alignment, AllocContext context = AllocContext()) override final;
		/**
		 * Reallocate memory
		 * @param[in] pOriginal	Original memory
		 * @param[in] size		New size of the allocation
		 * @param[in] alignment	New alignment of the allocation
		 * @param[in] context	Alloc context
		 */
		void* Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context = AllocContext()) override final;
		/**
		 * Free memory
		 * @param[in] ptr		Memory to free
		 * @param[in] context	Alloc context
		 */
		void Free(void* ptr, AllocContext context = AllocContext()) override final;
		/**
		 * Check if the pointer is allocated using the allocator
		 * @return True if the pointer is allocated using the allocator, false otherwise
		 */
		b8 Owns(void* ptr) override final;

		/**
		 * Reset the allocator
		 * @note	All allocations made before the reset is called, will be invalid
		 */
		void Reset();

	private:
		HV_MAKE_NON_COPYABLE(StackAllocator);

		sizeT m_Index;	/**< Index in buffer */
		u8* m_pPrev;	/**< Pointer to previous allocation */

		/**
		 * Stack header
		 */
		struct Header
		{
			u8* pPrev;		/**< Pointer to previous memory 'chunk' */
			u16 padding;	/**< Padding */
		};
	};

}
