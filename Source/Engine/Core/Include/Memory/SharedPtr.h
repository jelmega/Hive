// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// SharedPtr.h: Shared pointer
#pragma once
#include "Core/CoreHeaders.h"
#include "PtrRef.h"

namespace Hv::Memory {
	
	template<typename T, typename Deleter>
	class WeakPtr;

	/**
	 * Shared pointer
	 * @tparam T		Element type
	 * @tparam Deleter	Deleter predicate
	 */
	template<typename T, typename Deleter = DefaultDeleter<T>>
	class SharedPtr
	{
	public:
		/**
		 * Create an empty SharedPtr
		 */
		SharedPtr();
		/**
		 * Create a SharedPtr from a pointer
		 * @param[in] ptr	Pointer
		 */
		SharedPtr(T* ptr);
		/**
		 * Create a SharedPtr from a pointer and a deleter
		 * @param[in] ptr		Pointer
		 * @param[in] deleter	Deleter
		 */
		SharedPtr(T* ptr, const Deleter& deleter);
		/**
		 * Create a SharedPtr from another SharedPtr
		 * @param[in] ptr	SharedPtr
		 */
		SharedPtr(const SharedPtr& ptr);
		/**
		 * Move a SharedPtr into this SharedPtr
		 * @param[in] ptr	SharedPtr
		 */
		SharedPtr(SharedPtr&& ptr) noexcept;
		~SharedPtr();

		SharedPtr& operator=(const SharedPtr& ptr);
		SharedPtr& operator=(SharedPtr&& ptr) noexcept;

		T& operator*();
		const T& operator*() const;
		T* operator->();
		const T* operator->() const;

		b8 operator!() const;
		operator b8() const;

		/**
		 * Get the raw pointer
		 * @return Raw pointer
		 */
		T* Get();
		/**
		 * Get the raw pointer
		 * @return Raw pointer
		 */
		const T* Get() const;

		/**
		 * Get the deleter
		 * @return	Deleter
		 */
		Deleter& GetDeleter();
		/**
		 * Get the deleter
		 * @return	Deleter
		 */
		const Deleter& GetDeleter() const;

		/**
		 * Get the reference count
		 * @return	Reference count
		 */
		sizeT RefCount() const;
		/**
		 * Check if the SharedPtr is valid (has a pointer)
		 * @return	If the SharedPtr is valid
		 */
		bool IsValid() const;
		/**
		 * Check if the SharedPtr is unique
		 * @return	If the SharedPtr is unique
		 */
		bool IsUnique() const;
		/**
		 * Reset pointer (delete old pointer and set 'ptr' as new pointer)
		 * @param[in] ptr	Pointer
		 */
		void Reset(T* ptr);

		/**
		 * Swap 2 SharedPtrs
		 * @param[in] ptr	SharedPtr to swap with
		 */
		void Swap(SharedPtr& ptr);

	private:
		friend class WeakPtr<T, Deleter>;

		PtrRef<T, Deleter>* m_pRef;
	};

	template<typename T, typename... Args>
	SharedPtr<T> MakeShared(Args&&... args);

}

HV_DECLARE_CONTAINER_COPY_CONSTRUCTOR_TEMPLATE(HV_TARGS(typename T, typename Deleter), HV_TTYPE(Hv::Memory::SharedPtr, T, Deleter));
