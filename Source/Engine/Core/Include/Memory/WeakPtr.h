// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// WeakPtr.h: Weak pointer
#pragma once
#include "Core/CoreHeaders.h"
#include "PtrRef.h"

namespace Hv::Memory {

	template<typename T, typename Deleter>
	class SharedPtr;
	
	/**
	 * Shared pointer
	 * @tparam T		Element type
	 * @tparam Deleter	Deleter predicate
	 */
	template<typename T, typename Deleter = DefaultDeleter<T>>
	class WeakPtr
	{
	public:
		/**
		 * Create a WeakPtr from a SharedPtr
		 * @param[in] ptr	SharedPtr
		 */
		WeakPtr(const SharedPtr<T, Deleter>& ptr);
		/**
		 * Create a WeakPtr from another WeakPtr
		 * @param[in] ptr	WeakPtr
		 */
		WeakPtr(const WeakPtr& ptr);
		/**
		 * Move a WeakPtr into this WeakPtr
		 * @param[in] ptr	WeakPtr
		 */
		WeakPtr(WeakPtr&& ptr) noexcept;
		~WeakPtr();

		WeakPtr& operator=(const SharedPtr<T, Deleter>& ptr);
		WeakPtr& operator=(const WeakPtr& ptr);
		WeakPtr& operator=(WeakPtr&& ptr) noexcept;

		T& operator*();
		const T& operator*() const;
		T* operator->();
		const T* operator->() const;

		b8 operator!() const;
		operator b8() const;

		/**
		 * Get the raw pointer
		 * @return Raw pointer
		 */
		T* Get();
		/**
		 * Get the raw pointer
		 * @return Raw pointer
		 */
		const T* Get() const;

		/**
		 * Get the deleter
		 * @return	Deleter
		 */
		Deleter& GetDeleter();
		/**
		 * Get the deleter
		 * @return	Deleter
		 */
		const Deleter& GetDeleter() const;

		/**
		 * Get the reference count
		 * @return	Reference count
		 */
		sizeT RefCount() const;
		/**
		 * Check if the WeakPtr is valid (has a pointer)
		 * @return	If the WeakPtr is valid
		 */
		bool IsValid() const;
		/**
		 * Check if the WeakPtr is unique
		 * @return	If the WeakPtr is unique
		 */
		bool IsUnique() const;

		/**
		 * Swap 2 WeakPtrs
		 * @param[in] ptr	WeakPtr to swap with
		 */
		void Swap(WeakPtr& ptr);

	private:
		PtrRef<T, Deleter>* m_pRef;
	};

}

HV_DECLARE_CONTAINER_MEMCPY_TEMPLATE(HV_TARGS(typename T, typename Deleter), HV_TTYPE(Hv::Memory::WeakPtr, T, Deleter));