// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PtrRef.h: Pointer reference
#pragma once
#include "Core/CoreHeaders.h"
#include "MemoryUtil.h"
#include "Containers/ContainerUtils.h"

namespace Hv::Memory {
	
	/**
	 * Pointer reference
	 * @note PtrRef is used in SharedPtr and WeakPtr and is NOT meant to be used separately
	 * @tparam T	Element type
	 */
	template<typename T, typename Deleter = DefaultDeleter<T>>
	class PtrRef
	{
	public:
		/**
		 * Create an empty pointer ref
		 */
		PtrRef();
		/**
		 * Creata a pointer red from a pointer
		 * @param[in] ptr	Pointer
		 */
		PtrRef(T* ptr);
		/**
		 * Creata a pointer red from a pointer and a deleter
		 * @param[in] ptr	Pointer
		 */
		PtrRef(T* ptr, const Deleter& deleter);
		~PtrRef();

		T& operator*();
		const T& operator*() const;
		T* operator->();
		const T* operator->() const;

		operator b8() const;
		b8 operator!() const;

		/**
		 * Add a reference to the pointer ref
		 */
		void Ref();
		/**
		 * Remove a reference to the pointer ref
		 */
		void Deref();

		/**
		 * Get the raw pointer
		 * @return	Raw pointer
		 */
		T* Get();
		/**
		 * Get the raw pointer
		 * @return	Raw pointer
		 */
		const T* Get() const;

		/**
		 * Get the deleter
		 * @return	Deleter
		 */
		Deleter& GetDeleter();
		/**
		 * Get the deleter
		 * @return	Deleter
		 */
		const Deleter& GetDeleter() const;

		/**
		 * Get the reference count
		 * @return	Reference count
		 */
		sizeT RefCount() const;

		/**
		 * Check if the pointer ref is valid
		 * @return	If the pointer ref is valid
		 */
		b8 IsValid() const;
		/**
		 * Check if the pointer ref is unique
		 * @return	If the pointer ref is unique
		 */
		b8 IsUnique() const;

		/**
		 * Swap 2 pointer refs
		 * @param[in] ref	PtrRef to swap with
		 */
		void Swap(PtrRef& ref);

	private:

		T* m_Ptr;
		Deleter m_Deleter;
		sizeT m_RefCount;

	};

	/**
	* Pointer reference (specializarion for arrays)
	* @note PtrRef is used in SharedPtr and WeakPtr and is NOT meant to be used separately
	* @tparam T	Element type
	*/
	template<typename T, typename Deleter>
	class PtrRef<T[], Deleter>
	{
	public:
		/**
		* Create an empty pointer ref
		*/
		PtrRef();
		/**
		* Creata a pointer red from a pointer
		* @param[in] ptr	Pointer
		*/
		PtrRef(T* ptr);
		/**
		 * Creata a pointer red from a pointer and a deleter
		 * @param[in] ptr	Pointer
		 */
		PtrRef(T* ptr, const Deleter& deleter);
		~PtrRef();

		T& operator*();
		const T& operator*() const;
		T* operator->();
		const T* operator->() const;

		operator b8() const;
		b8 operator!() const;

		/**
		 * Add a reference to the pointer ref
		 */
		void Ref();
		/**
		 * Remove a reference to the pointer ref
		 */
		void Deref();

		/**
		 * Get the raw pointer
		 * @return	Raw pointer
		 */
		T* Get();
		/**
		 * Get the raw pointer
		 * @return	Raw pointer
		 */
		const T* Get() const;

		/**
		 * Get the deleter
		 * @return	Deleter
		 */
		Deleter& GetDeleter();
		/**
		 * Get the deleter
		 * @return	Deleter
		 */
		const Deleter& GetDeleter() const;

		/**
		 * Get the reference count
		 * @return	Reference count
		 */
		sizeT RefCount() const;

		/**
		 * Check if the pointer ref is valid
		 * @return	If the pointer ref is valid
		 */
		b8 IsValid() const;
		/**
		 * Check if the pointer ref is unique
		 * @return	If the pointer ref is unique
		 */
		b8 IsUnique() const;

		/**
		 * Swap 2 pointer refs
		 * @param[in] ref	PtrRef to swap with
		 */
		void Swap(PtrRef& ref);

	private:

		T* m_Ptr;
		Deleter m_Deleter;
		sizeT m_RefCount;
	};

}

HV_DECLARE_CONTAINER_MEMCPY_TEMPLATE(HV_TARGS(typename T, typename Deleter), HV_TTYPE(Hv::Memory::PtrRef, T, Deleter));