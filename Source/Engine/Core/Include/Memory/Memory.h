// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Memory.h: Main memory header
#pragma once

#include "MemoryUtil.h"
#include "Allocators/LinearAllocator.h"
#include "Allocators/StackAllocator.h"
#include "Allocators/PoolAllocator.h"
#include "Allocators/BuddyAllocator.h"
#include "Allocators/FreelistAllocator.h"
#include "Allocators/Mallocator.h"
#include "Allocators/PagedPoolAllocator.h"
#include "Allocators/PagedBuddyAllocator.h"
#include "Allocators/PagedFreelistAllocator.h"

#include "Arenas/GlobalMemoryArena.h"

#include "Memory/SmartPtrs.h"

namespace Hv::Memory
{
	HIVE_API IMemoryArena& GetGlobalAllocator();
}

#define g_Alloctor Hv::Memory::GetGlobalAllocator()
#define g_pAllocator (&Hv::Memory::GetGlobalAllocator())