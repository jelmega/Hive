// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HalSystem.h: HAL system
#pragma once
#include "Core/CoreTypes.h"
#include "Core/CoreMacros.h"

namespace Hv {
	
	/**
	 * System handle
	 */
	HV_CREATE_HANDLE(SystemHandle);
	/**
	 * Invalid system handle
	 */
	HV_CREATE_INVALID_HANDLE(SystemHandle, -1);

}

namespace Hv::HAL::System {
	
	/**
	 * Check if a debugger is attached
	 * @return	True if a debugger is attached, false otherwise
	 */
	HIVE_API b8 IsDebuggerAttached();

	/**
	 * Log a message to the debuggers output
	 * @param[in] text	Text to log
	 */
	HIVE_API void LogDebugger(const AnsiChar* text);
	/**
	 * Log a message to the debuggers output
	 * @param[in] text	Text to log
	 */
	HIVE_API void LogDebugger(const WideChar* text);

	/**
	 * Update the system (OS)
	 */
	HIVE_API b8 TickSystem();

}