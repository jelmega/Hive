// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HalTime.h: HAL time
#pragma once
#include "Core/CoreHeaders.h"
#include "Time/DateTimeValues.h"

namespace Hv::HAL::Time {
	
	/**
	 * Get the tick frequency of the system
	 * @return	Tick frequency of the system
	 */
	HIVE_API u64 GetTickFrequency();

	/**
	 * Get the current system tick count
	 * @return	Current system tick count
	 */
	HIVE_API u64 GetCurrentTicks();

	/**
	 * Get the current date time
	 * @return	Current date time
	 */
	HIVE_API DateTimeValues GetCurrentDataTime();

}

#if HV_PLATFORM_WINDOWS
#	include "Windows/WindowsTime.h"
#else
#endif
