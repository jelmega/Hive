// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HalFileSystem.h: HAL File System
#pragma once
#include "FileSystem/FileSystemCommon.h"
#include "String/String.h"

namespace Hv {
	class DateTime;
}

namespace Hv::FileSystem {

	/**
	* File handle
	*/
	HV_CREATE_HANDLE(FileHandle);
	/**
	 * Invalid file handle
	 */
	HV_CREATE_INVALID_HANDLE(FileHandle, -1);
	/**
	 * File search handle
	 */
	HV_CREATE_HANDLE(FileSearchHandle);
	/**
	* Invalid file search handle
	*/
	HV_CREATE_INVALID_HANDLE(FileSearchHandle, -1);
}

namespace Hv::HAL::FileSystem {
	
	using FileMode = Hv::FileSystem::FileMode;
	using AccessMode = Hv::FileSystem::AccessMode;
	using ShareMode = Hv::FileSystem::ShareMode;
	using CopySettings = Hv::FileSystem::CopySettings;
	using EntryType = Hv::FileSystem::EntryType;
	using FileHandle = Hv::FileSystem::FileHandle;
	using FileSearchHandle = Hv::FileSystem::FileSearchHandle;

	/**
	 * Get the end index of the system specific root name
	 * @param[in] path	Path
	 * @return			index to the end of the rootname, if there is no rootname, NPos is returned	
	 */
	sizeT PrefixEnd(const String& path);

	/**
	 * Get the path of the current/working directory (folder containing exe)
	 */
	String GetCurrentDirectory();

	/**
	 * Create a direcory
	 * @param[in] path	Path of the new directory
	 * @return			True if directory creation succeded, false otherwise
	 */
	b8 CreateDirectory(const String& path);
	/**
	 * Create a direcory with the same attributes as the template directory
	 * @param[in] path			Path of the new directory
	 * @param[in] templatePath	Path to the template directory
	 * @return					True if directory creation succeded, false otherwise
	 */
	b8 CreateDirectory(const String& path, const String& templatePath);

	/**
	* Remove/Delete a directory
	* @param[in] path	Path to the directory
	* @return			True if the directory was removed succesfully, false otherwise
	*/
	b8 RemoveDirectory(const String& path);

	/**
	 * Check if a directory exists
	 * @param[in] path	Path to the directory
	 * @return			True if the directory exist, false otherwise
	 */
	b8 DoesDirectoryExist(const String& path);

	/**
	 * Move a directory from one directory to another directory
	 * @param[in] from	Path to the directory to move
	 * @param[in] to	Path to the directory to move the directory to
	 * @return			True if the file was moved successfully, false otherwise
	 */
	b8 MoveDirectory(const String& from, const String& to);

	////////////////////////////////////////////////////////////////////////////////
	//	Files
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Create a file and open it
	 * @param[in] path	Path to the file to create
	 * @param[in] mode	File mode flags
	 * @return			Created file
	 * @note			Create specific flags have documentation that starts with [Create]
	 *					File mode open flags are ignored
	 */
	FileHandle CreateFile(const String& path, FileMode mode, AccessMode access, ShareMode share = ShareMode::None);

	/**
	 * Close the file
	 * @param[in] handle	Handle to file to close
	 * @note				The function expects the handle to be valid, currently, no checks are done before closing the file
	 */
	b8 CloseFile(FileHandle& handle);

	/**
	 * Delete a file
	 * @param[in] path	Path to the file to delete
	 * @return			True if the file was successfully deleted
	 */
	b8 DeleteFile(const String& path);

	/**
	 * Read an array of values form a file
	 * @param[in] handle		Native file handle
	 * @param[inout] pBytes		Buffer to read to
	 * @param[in] size			Number of bytes to read
	 * @param[in] bytesRead		Number of bytes that were read
	 * @return					True if reading the value was successful, false otherwise
	 * @note					If reading failed, the file pointer gets reset to the position where it was before trying to read it
	 */
	b8 ReadFromFile(FileHandle& handle, u8* pBytes, sizeT size, u32& bytesRead);

	/**
	 * Write an array of values to a file
	 * @tparam T			Primitive type
	 * @param[in] handle	Native file handle
	 * @param[in] pBytes	Buffer to write from
	 * @param[in] count		Number of bytes to write
	 * @return				True if writinf the value was successful, false otherwise
	 */
	b8 WriteToFile(FileHandle handle, const u8* pBytes, sizeT count);

	/**
	 * Get the current position of the file pointer
	 * @param[in] handle	Native file handle
	 * @return				Current location
	 */
	sizeT GetFilePointer(const FileHandle& handle);
	/**
	 * Set the current position of the file pointer
	 * @param[in] handle	Native file handle
	 * @param[in] position	Position of the file pointer
	 * @return				Current location, sizeT(-1) if the position could not be retrieved
	 */
	b8 SetFilePointer(const FileHandle& handle, sizeT position);
	/**
	 * Get the current position of the file pointer
	 * @param[in] handle	Native file handle
	 * @param[in] offset	Mount to move the file pointer (positive -> forward, negative -> backwards)
	 */
	b8 MoveFilePointer(const FileHandle& handle, i64 offset);
	/**
	* Set current position of the file pointer to the start of the file
	* @param[in] handle	Native file handle
	*/
	b8 MoveFilePointerToBegin(const FileHandle& handle);
	/**
	* Set current position of the file pointer to  the end of the file
	* @param[in] handle	Native file handle
	*/
	b8 MoveFilePointerToEnd(const FileHandle& handle);

	/**
	 * Check if the end of the file is reached
	 * @return	True if the end of the file is reached, false otherwise
	 */
	b8 EndOfFile(const FileHandle& handle);

	/**
	 * Get the size of the file
	 * @param[in] handle	Native file handle
	 * @return				File size
	 * @note				File size will be sizeT(-1), when the size could not be retrieved
	 */
	sizeT GetFileSize(const FileHandle& handle);
	/**
	 * Get the creation, last access and last edit time
	 * @param[out] pCreation	Data and time of creation
	 * @param[out] pLastAccess	Date and time of the last access
	 * @param[out] pLastEdit	Date and time of the last edit
	 * @return					True if the date and time could be retrieved
	 */
	b8 GetFileTime(const FileHandle& handle, DateTime* pCreation, DateTime* pLastAccess, DateTime* pLastEdit);

	/**
	* Check if a file exists
	* @param[in] path	Path to the file
	* @return			True if the file exist, false otherwise
	*/
	b8 DoesFileExist(const String& path);

	/**
	 * Move a file from one directory to another directory
	 * @param[in] from	Path to the file to move
	 * @param[in] to	Path to the directory to move the file to
	 * @return			True if the file was moved successfully, false otherwise
	 */
	b8 MoveFile(const String& from, const String& to);
	/**
	 * Copy a file from one directory to another directory
	 * @param[in] from		Path to the file to copy
	 * @param[in] to		Path to the file to copy to
	 * @param[in] settings	Copy settings
	 * @return				True if the file was copied successfully, false otherwise
	 */
	b8 CopyFile(const String& from, const String& to, CopySettings settings);

	////////////////////////////////////////////////////////////////////////////////
	//	Search and iteration
	////////////////////////////////////////////////////////////////////////////////
	/**
	 * Get the native search handle to the entry
	 * @param[in] path	Path to the entry to find
	 * @return			Native file handle 
	 */
	FileSearchHandle FindEntry(const String& path);

	/**
	 * Get the native search handle to the first entry in a directory
	 * @param[in] path		Path to the directory to iterate in
	 * @param[out] filepath	Path to the first entry
	 * @param[out] type		Entry type
	 * @return			Native file handle
	 */
	FileSearchHandle GetFirstDirectoryEntry(const String& path, String& filepath, EntryType& type);
	/**
	 * Set the native search handle to the next entry in the directory
	 * @param[in] handle		Handle to update
	 * @param[in] dir		Containing directory
	 * @param[out] filename	Path to the first entry
	 * @param[out] type		Entry type
	 * @return				True if the next file could be found, false otherwise;
	 */
	b8 GetNextEntry(FileSearchHandle& handle, const String& dir, String& filename, EntryType& type);

	/**
	 * Check if 2 search handles are the same
	 * @param[in] h0	First native search handle
	 * @param[in] h1	First native search handle
	 * @return			True if the handles are the same, false otherwise
	 */
	b8 CompareSearchHandles(const FileSearchHandle& h0, const FileSearchHandle& h1);

	/**
	 * Closes a native search handle
	 * @param[in] handle	Native search handle to close
	 * @return				True if the native search handle could be closed, false otherwise
	 */
	b8 CloseSearchHandle(FileSearchHandle& handle);

}

#if HV_PLATFORM_WINDOWS
#include "Windows/WindowsFileSystem.h"
#else
#	error File system for platform is not supported yet!
#endif