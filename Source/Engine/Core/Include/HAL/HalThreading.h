// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HalThreading.h: HAL threading
#pragma once
#include "Core/CoreHeaders.h"

namespace Hv::Threading {

	/**
	* Thread handle
	*/
	HV_CREATE_HANDLE(ThreadHandle);
	/**
	* Invalid thread handle
	*/
	HV_CREATE_INVALID_HANDLE(ThreadHandle, nullptr);

	/**
	* Mutex handle
	*/
	HV_CREATE_HANDLE(MutexHandle);
	/**
	* Invalid mutex handle
	*/
	HV_CREATE_INVALID_HANDLE(MutexHandle, nullptr);

	/**
	* Critical section handle
	*/
	HV_CREATE_HANDLE(CriticalSectionHandle);
	/**
	* Invalid mutex handle
	*/
	HV_CREATE_INVALID_HANDLE(CriticalSectionHandle, nullptr);
}

namespace Hv::HAL::Threading {
	
	using ThreadHandle = Hv::Threading::ThreadHandle;
	using MutexHandle = Hv::Threading::MutexHandle;
	using CriticalSectionHandle = Hv::Threading::CriticalSectionHandle;

	/**
	* Suspend execution on the current thread for a time-interval
	* @param[in] ms		Milliseconds to sleep
	*/
	HIVE_API void Sleep(u32 ms);
	/**
	* Get the amount of logical cores in the system
	* @return	Number of logical cores
	*/
	HIVE_API u32 GetLogicalCoreCount();

	/**
	 * Create a thread
	 * @param[in] id			Thread id
	 * @param[in] debugName		Debug name
	 * @return					Handle to the thread, invalid handle if creation failed
	 */
	HIVE_API ThreadHandle CreateThread(u32& id, Delegate<b8()>& delegate, const WideChar* debugName);
	/**
	* Destroy a thread
	* @param[in] thread		Handle to thread
	* @return				True if the thread was successfully destroyed, false otherwise
	*/
	HIVE_API b8 DestroyThread(ThreadHandle& thread);
	/**
	* Terminate a thread
	* @param[in] thread		Handle to thread
	* @return				True if the thread was successfully terminate, false otherwise
	*/
	HIVE_API b8 TerminateThread(ThreadHandle& thread);
	/**
	* Wait for a thread to finish
	* @param[in] thread		Handle to thread
	*/
	HIVE_API void WaitForThread(ThreadHandle& thread);
	/**
	 * Get the current thread id
	 * @return	Current thread id
	 */
	HIVE_API u32 GetCurrentThreadId();

	/**
	 * Create a mutex
	 * @param[in] name	Name
	 * @return			Handle to the mutex, invalid handle if creation failed
	 */
	HIVE_API MutexHandle CreateMutex(const WideChar* name);
	/**
	 * Destory a mutex
	 * @param[in] mutex		Handle to mutex
	 * @return				True if the thread was successfully destroyed, false otherwise
	 */
	HIVE_API b8 DestroyMutex(MutexHandle& mutex);
	/**
	 * Lock a mutex
	 * @param[in] mutex		Handle to mutex
	 * @param[in] timeout	Milliseconds to wait for the mutex
	 */
	HIVE_API void LockMutex(MutexHandle& mutex, u32 timeout);
	/**
	 * Try to lock a mutex
	 * @param[in] mutex		Handle to mutex
	 * @param[in] timeout	Milliseconds to wait for the mutex
	 * @return				True if the mutex is locked, false otherwise
	 */
	HIVE_API b8 TryLockMutex(MutexHandle& mutex, u32 timeout);
	/**
	 * Unlock a mutex
	 * @param[in] mutex		Handle to mutex
	 */
	HIVE_API void UnlockMutex(MutexHandle& mutex);

	/**
	 * Create a critical section
	 * @return	Handle to the mutex, invalid handle if creation failed
	 */
	HIVE_API CriticalSectionHandle CreateCriticalSection();
	/**
	* Create a critical section
	* @param[in] spinCount	Spin count
	* @return				Handle to the mutex, invalid handle if creation failed
	*/
	HIVE_API CriticalSectionHandle CreateCriticalSectionWithSpinCount(u32 spinCount);
	/**
	 * Destory a critical section
	 * @param[in] criticalSection	Handle to mutex
	 * @return						True if the thread was successfully destroyed, false otherwise
	 */
	HIVE_API b8 DestroyCriticalSection(CriticalSectionHandle& criticalSection);
	/**
	 * Enter a critical section
	 * @param[in] criticalSection	Handle to critical section
	 */
	HIVE_API void EnterCriticalSection(CriticalSectionHandle& criticalSection);
	/**
	* Enter a critical section
	* @param[in] criticalSection	Handle to critical section
	* @return						True if the critical section was entered, false otherwise
	*/
	HIVE_API b8 TryEnterCriticalSection(CriticalSectionHandle& criticalSection);
	/**
	 * Try to lock a critical section
	 * @param[in] criticalSection	Handle to critical section
	 * @return						True if the mutex is locked, false otherwise
	 */
	HIVE_API void LeaveCriticalSection(CriticalSectionHandle& criticalSection);


	////////////////////////////////////////////////////////////////////////////////
	// Atomics																	  //
	////////////////////////////////////////////////////////////////////////////////
	/**
	* Load the value atomically
	* @param[in] val	Destination
	* @param[in] val	Value to store
	* @return			Loaded value
	*/
	template<typename T>
	T AtomicStore(T& dst, T val);
	/**
	 * Load the value atomically
	 * @param[in] val	Value to load
	 * @return			Loaded value
	 */
	template<typename T>
	T AtomicLoad(T& val);

	/**
	* Increment a value atomically and than fetch the resulting value
	* @param[in] val	Value
	* @return			Value after addition
	*/
	template<typename T>
	T AtomicIncrement(T& val);
	/**
	* Descrement a value atomically and than fetch the resulting value
	* @param[in] val	Value
	* @return			Value after subtraction
	*/
	template<typename T>
	T AtomicDecrement(T& val);
	/**
	 * Add a value atomically and than fetch the resulting value
	 * @param[in] val	Value
	 * @param[in] add	Value to add
	 * @return			Value after addition
	 */
	template<typename T>
	T AtomicAddFetch(T& val, T add);
	/**
	 * Subtract a value atomically and than fetch the resulting value
	 * @param[in] val	Value
	 * @param[in] sub	Value to subtract
	 * @return			Value after subtraction
	 */
	template<typename T>
	T AtomicSubFetch(T& val, T sub);
}