// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HalMemory.h: HAL memory
#pragma once
#include "Core/CoreHeaders.h"
#include "Memory/AllocContext.h"

namespace Hv::HAL::Memory {
	
	/**
	 * Allocate memory
	 * @param[in] size		Size to allocate
	 * @param[in] alignment	Alignment of the allocation
	 * @return				Pointer to the allocated memory
	 */
	void* Allocate(sizeT size, u16 alignment = 8);
	/**
	 * Free memory
	 * @param[in] ptr	Memory to free
	 */
	void Free(void* ptr);

}

#if HV_PLATFORM_WINDOWS
#include "Windows/Inline/WindowsMemory.inl"
#else
#	error Memory allocation for platform is not supported yet!
#endif

#if HV_COMPILER_MSVC || HV_COMPILER_CLANG
#	define HV_ALLOC_CONTEXT(category) Hv::Memory::AllocContext(category, __FILE__, __FUNCTION__, __LINE__)
#elif HV_COMPILER_GCC
#	define HV_ALLOC_CONTEXT(category) Hv::Memory::AllocContext(category, __FILE__, __PRETTY_FUNCTION__ , __LINE__)
#else
#	define HV_ALLOC_CONTEXT(category) Hv::Memory::AllocContext(category, __FILE__, "", __LINE__)
#endif