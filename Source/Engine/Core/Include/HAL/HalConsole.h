// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HalConsole.h: HAL console
#pragma once
#include "Core/CoreHeaders.h"
#include "String/String.h"

namespace Hv::System {
	
	/**
	 * System console handle
	 */
	HV_CREATE_HANDLE(ConsoleHandle);
	/**
	* Invalid system console handle
	*/
	HV_CREATE_INVALID_HANDLE(ConsoleHandle, -1);
}

namespace Hv::HAL::Console {

	constexpr u8 g_MaxNumConsoleReadChars = 127;

	/**
	 * Create a console handle
	 * @return	Console handle
	 */
	HIVE_API Hv::System::ConsoleHandle CreateConsoleHandle();

	/**
	* Destroy a console handle
	* @return	True if the handle was destroyed, false otherwise
	*/
	HIVE_API b8 DestroyConsoleHandle(Hv::System::ConsoleHandle& handle);

	/**
	 * Write a string to the console
	 * @param[in] handle	Console handle
	 * @param[in] text		Test to write
	 * @return				True if the text was successfully written to the console, false otherwise
	 */
	HIVE_API b8 WriteConsole(Hv::System::ConsoleHandle& handle, String text);

	/**
	 * Read a string to the console
	 * @param[in] handle	Console handle
	 * @param[out] text		Test to read
	 * @return				True if the text was successfully read to the console, false otherwise
	 */
	HIVE_API b8 ReadConsole(Hv::System::ConsoleHandle& handle, String& text);

	/**
	 * Set the color of the console output
	 * @param[in] handle	Console handle
	 * @param[out] color	Color to use
	 * @return				True if the color was successfully set, false otherwise
	 */
	HIVE_API b8 SetConsoleColor(Hv::System::ConsoleHandle& handle, u8 color);

}
