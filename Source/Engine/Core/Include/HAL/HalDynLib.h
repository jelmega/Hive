// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HalDynLib.h: HAL dynamic library handling
#pragma once
#include "String/String.h"

namespace Hv::DynLib {
	
	/**
	 * DynLib handle
	 */
	typedef struct DynLibHandleT* DynLibHandle;
	/**
	* Invalid DynLib handle
	*/
	const DynLibHandle InvalidDynLibHandle = nullptr;
	/**
	* DynLib function handle
	*/
	typedef struct DynLibProcHandleT* DynLibProcHandle;
	/**
	* Invalid DynLib function handle
	*/
	const DynLibProcHandle InvalidDynLibProcHandle = nullptr;
}

namespace Hv::HAL::DynLib {
	
	using DynLibHandle = Hv::DynLib::DynLibHandle;
	using DynLibProcHandle = Hv::DynLib::DynLibProcHandle;

	/**
	 * Load a dynamic library
	 * @param[in] path	Path to dynamic library
	 * @return			Handle to dynamic library, invalid handle if library could not be loaded
	 */
	HIVE_API DynLibHandle Load(const String& path);
	/**
	* Unload a dynamic library
	* @param[in] handle	Handle to dynamic library
	*/
	HIVE_API void Unload(DynLibHandle& handle);
	/**
	 * Get the address of a function
	 * @param[in] handle	Handle to dynamic library
	 * @param[in] procName	Name of the function to load
	 */
	HIVE_API DynLibProcHandle GetProc(DynLibHandle& handle, const AnsiString& procName);

#ifdef HV_PLATFORM_WINDOWS
	constexpr const AnsiChar* g_DynLibExtension = ".dll";
#else
	constexpr const AnsiChar* g_DynLibExtension = ".so";
#endif

}