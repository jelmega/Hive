// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Logger.h: Logger
#include "Logging/Logger.h"
#include "System/SystemConsole.h"
#include "HAL/HalThreading.h"
#include "Threading/Mutex.h"

namespace Hv {

	LogCategory::LogCategory(const char* name, LogLevel level)
		: categoryName(name)
		, categoryLevel(level)
		, categoryNameLength(u8(StringUtils::CStringLength(name)))
	{
	}

	Threading::CriticalSection Logger::m_CriticalSection;

	Logger::Logger()
		: m_LogToConsole(true)
		, m_LogToFile(true)
		, m_DefaultCategory("Logger", LogLevel::All)
	{
		HvFS::Path filename = "Log " + DateTime::Now().GetDefaultFromattedString() + ".txt";
		HvFS::Path dir = String("Logs");
		HvFS::CreateDirectory(dir);
		m_File = HvFS::CreateFile(dir / HvFS::Path(filename), FileSystem::FileMode::Create, FileSystem::AccessMode::Write);
	}

	Logger::~Logger()
	{
		m_File.Close();
	}

	void Logger::NewLine(u8 nr)
	{
		if (nr == 0)
			return;
		String str('\n', nr);
		if (m_LogToConsole)
			g_Console.Write(str);
		if (m_LogToFile && m_File.IsValid())
			m_File.Write(str.CStr(), str.Length());
	}

	void Logger::Log(const LogCategory& category, LogLevel level, const String& str)
	{
		// Don't log if the level isn't 'supported' by the category
		if (u8(level) > u8(category.categoryLevel))
		{
			return;
		}

		String datetimeString = DateTime::Now().GetDefaultFromattedString();
		const String& levelTag = LevelToString(level);

		constexpr sizeT additionalSpace = 5;
		String logString(datetimeString.Length() + levelTag.Length() + str.Length() + additionalSpace + m_MaxCategoryNameLen + 1);
		logString.Append(datetimeString);
		logString.Append(' ');
		logString.Append(levelTag);

		logString.Append("[");
		u8 len = category.categoryNameLength;
		len = len <= m_MaxCategoryNameLen ? len : m_MaxCategoryNameLen;
		logString.Append(category.categoryName, len);
		u8 addLen = m_MaxCategoryNameLen - len;
		if (addLen > 0)
			logString.Append(' ', addLen);
		logString.Append("]: ");

		logString.Append(str);

		m_CriticalSection.Enter();
		if (m_LogToConsole)
		{
			SetLevelConsoleColor(level);
			g_Console.WriteLine(logString);
			g_Console.ResetConsoleColor();

			if (HAL::System::IsDebuggerAttached())
			{
				logString.Append('\n');
				HAL::System::LogDebugger(logString.CStr());
			}
		}
		if (m_LogToFile && m_File.IsValid())
			m_File.WriteLine(logString);
		m_CriticalSection.Leave();
	}

	void Logger::LogVerbose(const String& str)
	{
		Log(m_DefaultCategory, LogLevel::Verbose, str);
	}

	void Logger::LogVerbose(const LogCategory& cat, const String& str)
	{
		Log(cat, LogLevel::Verbose, str);
	}

	void Logger::LogInfo(const String& str)
	{
		Log(m_DefaultCategory, LogLevel::Info, str);
	}

	void Logger::LogInfo(const LogCategory& cat, const String& str)
	{
		Log(cat, LogLevel::Info, str);
	}

	void Logger::LogDetail(const String& str)
	{
		Log(m_DefaultCategory, LogLevel::Detail, str);
	}

	void Logger::LogDetail(const LogCategory& cat, const String& str)
	{
		Log(cat, LogLevel::Detail, str);
	}

	void Logger::LogWarning(const String& str)
	{
		Log(m_DefaultCategory, LogLevel::Warning, str);
	}

	void Logger::LogWarning(const LogCategory& cat, const String& str)
	{
		Log(cat, LogLevel::Warning, str);
	}

	void Logger::LogError(const String& str)
	{
		Log(m_DefaultCategory, LogLevel::Error, str);
	}

	void Logger::LogError(const LogCategory& cat, const String& str)
	{
		Log(cat, LogLevel::Error, str);
	}

	void Logger::LogFatal(const String& str)
	{
		Log(m_DefaultCategory, LogLevel::Fatal, str);
	}

	void Logger::LogFatal(const LogCategory& cat, const String& str)
	{
		Log(cat, LogLevel::Fatal, str);
	}

	void Logger::SetLogToConsole(b8 enable)
	{
		m_LogToConsole = enable;
	}

	void Logger::SetLogToFile(b8 enable)
	{
		m_LogToFile = enable;
	}

	const String Logger::LevelToString(LogLevel level) const
	{
		switch (level)
		{
		default:
		case LogLevel::Info:
			return "[INFO ]";
		case LogLevel::Warning:
			return "[WARN ]";
		case LogLevel::Error:
			return "[ERROR]";
		case LogLevel::Fatal:
			return "[FATAL]";
		}
	}

	void Logger::SetLevelConsoleColor(LogLevel level)
	{
		switch (level)
		{
		default:
		case LogLevel::Info:
			g_Console.SetConsoleColor(System::ConsoleColor::ForeWhite | System::ConsoleColor::BackBlack);
			break;
		case LogLevel::Warning:
			g_Console.SetConsoleColor(System::ConsoleColor::ForeDarkYellow | System::ConsoleColor::BackBlack);
			break;
		case LogLevel::Error:
			g_Console.SetConsoleColor(System::ConsoleColor::ForeDarkRed | System::ConsoleColor::BackBlack);
			break;
		case LogLevel::Fatal:
			g_Console.SetConsoleColor(System::ConsoleColor::ForeBlack | System::ConsoleColor::BackRed);
			break;
		}
	}

	Logger& GetLogger()
	{
		static Logger logger;
		return logger;
	}

}
