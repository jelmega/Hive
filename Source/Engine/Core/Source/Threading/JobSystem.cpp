// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// JobSystem.h: Job system
#include "Threading/JobSystem.h"
#include "Threading/JobRunner.h"
#include "String/TString.h"

namespace Hv::Threading {

	JobSystem::JobSystem()
	{
	}

	JobSystem::~JobSystem()
	{
	}

	b8 JobSystem::Init(u8 numThreads)
	{
		String threadName;
		m_Threads.Resize(numThreads);
		for (u8 i = 0; i < numThreads; ++i)
		{
			threadName = "JobThread%u";
			threadName.FormatF(threadName, i);
			m_Threads[i] = Thread(threadName.CStr());

			JobRunner* pRunner = HvNew JobRunner(this);
			m_Threads[i].Create(Delegate<b8()>(pRunner, &JobRunner::Run));

			m_Runners.Push(pRunner);
		}
		return true;
	}

	b8 JobSystem::Shutdown()
	{
		// Terminate runners
		for (u8 i = 0; i < m_Runners.Size(); ++i)
		{
			m_Runners[i]->Terminate();
		}

		// Wait for thread end
		for (u8 i = 0; i < m_Threads.Size(); ++i)
		{
			m_Threads[i].Wait();
		}

		// Cleanup
		for (u8 i = 0; i < m_Threads.Size(); ++i)
		{
			m_Threads[i].Terminate();
			HvDelete m_Runners[i];
			m_Threads[i].Destroy();
		}
		m_Runners.Clear();
		m_Threads.Clear();

		return true;
	}

	void JobSystem::AddJob(IJob* pJob)
	{
		m_CritSec.Enter();
		switch (pJob->GetJobPriority())
		{
		default:
		case IJob::Priority::High:
			m_HighPriory.Push(pJob);
			break;
		case IJob::Priority::Medium:
			m_MediumPriory.Push(pJob);
			break;
		case IJob::Priority::Low:
			m_LowPriory.Push(pJob);
			break;
		}
		m_CritSec.Leave();
	}

	void JobSystem::Tick()
	{
		while (true)
		{
			if (m_HighPriory.Size() > 0)
			{
				Sleep(1);
			}
			else if (m_MediumPriory.Size() > 0)
			{
				IJob* pJob = m_MediumPriory.Peek();
				if (pJob->GetFramesLeft() == 0)
					Sleep(1);
			}
			else if (m_LowPriory.Size() > 0)
			{
				IJob* pJob = m_LowPriory.Peek();
				if (pJob->GetFramesLeft() == 0)
					Sleep(1);
			}
			else
			{
				break;
			}
		}

		m_CritSec.Enter();
		// Tick frames left before job completion
		for (IJob* pJob : m_MediumPriory)
		{
			pJob->TickFramesLeft();
		}
		for (IJob* pJob : m_LowPriory)
		{
			pJob->TickFramesLeft();
		}

		m_CritSec.Leave();
	}

	IJob* JobSystem::RequestJob()
	{
		IJob* pJob = nullptr;
		m_CritSec.Enter(); 
		if (m_HighPriory.Size() > 0)
		{
			pJob = m_HighPriory.Peek();
			m_HighPriory.Pop();
		}
		else if (m_MediumPriory.Size() > 0)
		{
			pJob = m_MediumPriory.Peek();
			m_MediumPriory.Pop();
		}
		else if (m_LowPriory.Size() > 0)
		{
			pJob = m_LowPriory.Peek();
			m_LowPriory.Pop();
		}
		m_CritSec.Leave();
		return pJob;
	}

	JobSystem& GetJobSystem()
	{
		static JobSystem jobSystem;
		return jobSystem;
	}
}
