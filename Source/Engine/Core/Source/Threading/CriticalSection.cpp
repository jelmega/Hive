// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// CriticalSection.cpp: Critical section
#include "Threading/CriticalSection.h"

namespace Hv::Threading {


	CriticalSection::CriticalSection()
		: m_SpinCount(0)
	{
		m_Handle = HAL::Threading::CreateCriticalSection();
	}

	CriticalSection::CriticalSection(u32 spinCount)
		: m_SpinCount(spinCount)
	{
		m_Handle = HAL::Threading::CreateCriticalSectionWithSpinCount(m_SpinCount);
	}

	CriticalSection::~CriticalSection()
	{
		if (m_Handle != InvalidCriticalSectionHandle)
			HAL::Threading::DestroyCriticalSection(m_Handle);
	}

	void CriticalSection::Enter()
	{
		HAL::Threading::EnterCriticalSection(m_Handle);
	}

	b8 CriticalSection::TryEnter()
	{
		return HAL::Threading::TryEnterCriticalSection(m_Handle);
	}

	void CriticalSection::Leave()
	{
		HAL::Threading::LeaveCriticalSection(m_Handle);
	}
}
