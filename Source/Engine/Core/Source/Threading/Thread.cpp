// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Thread.cpp: Thread
#pragma once
#include "Threading/Thread.h"
#include "Logging/Logger.h"

HV_DECLARE_LOG_CATEGORY(Threading, Hv::LogLevel::All);

namespace Hv::Threading {

	Thread::Thread(const WideChar* debugName)
		: m_Handle(InvalidThreadHandle)
		, m_Id(0)
		, m_Name(debugName)
	{
	}

	Thread::~Thread()
	{
	}

	Thread& Thread::operator=(Thread&& thread)
	{
		m_Handle = thread.m_Handle;
		m_Delegate = thread.m_Delegate;
		m_Id = thread.m_Id;
		m_Name = thread.m_Name;

		thread.m_Handle = InvalidThreadHandle;
		thread.m_Id = 0xFFFF'FFFF;
		thread.m_Name = L"";

		return *this;
	}

	b8 Thread::Create(Delegate<b8()> delegate)
	{
		m_Delegate = delegate;
		m_Handle = HAL::Threading::CreateThread(m_Id, m_Delegate, m_Name);
		return m_Handle != InvalidThreadHandle;
	}

	b8 Thread::Destroy()
	{
		b8 res = true;
		if (m_Handle != InvalidThreadHandle)
		{
			res = HAL::Threading::DestroyThread(m_Handle);
			m_Handle = InvalidThreadHandle;
			m_Id = 0;
		}
		return res;
	}

	b8 Thread::Terminate()
	{
		return HAL::Threading::TerminateThread(m_Handle);
	}

	void Thread::Wait()
	{
		HAL::Threading::WaitForThread(m_Handle);
	}
}
