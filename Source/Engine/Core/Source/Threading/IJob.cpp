// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// IJob.h: Job interface
#include "Threading/IJob.h"
#include "Threading/ThreadingFunc.h"

namespace Hv::Threading {

	IJob::IJob(Priority priority, b8 destroyOnCompletion)
		: m_Priority(priority)
		, m_DestroyOnCompletion(destroyOnCompletion)
		, m_Completed(false)
	{
		switch (priority)
		{
		default:
		case Priority::High:
			m_FramesLeft = 0;
			break;
		case Priority::Medium:
			m_FramesLeft = m_MedPriorityFrames;
			break;
		case Priority::Low:
			m_FramesLeft = m_LowPriorityFrames;
			break;
		}
	}

	IJob::~IJob()
	{
	}

	void IJob::AwaitCompleted()
	{
		while (!m_Completed)
		{
			Sleep(1);
		}
	}
}
