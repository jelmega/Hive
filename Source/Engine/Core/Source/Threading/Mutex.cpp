// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Mutex.cpp: mutex
#include "Threading/Mutex.h"

namespace Hv::Threading {


	Mutex::Mutex(const WideChar* name)
		: m_Handle(InvalidMutexHandle)
		, m_Name(name)
	{
		m_Handle = HAL::Threading::CreateMutex(name);
	}

	Mutex::~Mutex()
	{
		if (m_Handle != InvalidMutexHandle)
			HAL::Threading::DestroyMutex(m_Handle);
	}

	void Mutex::Lock()
	{
		HAL::Threading::LockMutex(m_Handle, 1000);
	}

	b8 Mutex::TryLock(u32 timeout)
	{
		return HAL::Threading::TryLockMutex(m_Handle, timeout);
	}

	void Mutex::Unlock()
	{
		HAL::Threading::UnlockMutex(m_Handle);
	}
}

