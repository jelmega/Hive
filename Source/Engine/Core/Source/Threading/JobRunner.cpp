// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// JobRunner.cpp: Job runner
#pragma once
#include "Threading/JobRunner.h"
#include "Threading/IJob.h"
#include "Threading/JobSystem.h"
#include "Threading/ThreadingFunc.h"
#include "Threading/Threading.h"

namespace Hv::Threading {

	JobRunner::JobRunner(JobSystem* pJobSystem)
		: m_pJobSystem(pJobSystem)
		, m_Running(true)
	{
	}

	JobRunner::~JobRunner()
	{
	}

	b8 JobRunner::Run()
	{
		while (m_Running)
		{
			IJob* pJob = m_pJobSystem->RequestJob();

			if (pJob)
			{
				b8 res = pJob->Execute();
				if (!res)
				{
					g_Logger.LogError(LogThreading(), "Job failed!");
				}
				if (pJob->NeedsDeleteOnCompletion())
				{
					HvDelete pJob;
				}
				else
				{
					pJob->CompleteJob();
				}
			}
			else
			{
				Sleep(1);
			}
		}
		return true; // Required for thread
	}

	void JobRunner::Terminate()
	{
		m_Running = false;
	}
}
