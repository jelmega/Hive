// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Json.cpp: Json 'object'
#pragma once
#include "Json/Json.h"

namespace Hv {


	Json::Json()
		: m_Type(Type::Null)
		, m_SubObjects()
	{
	}

	Json::Json(b8 val)
		: m_Type(Type::Bool)
		, m_Bool(val)
	{
	}

	Json::Json(f64 val)
		: m_Type(Type::Float)
		, m_Float(val)
	{
	}

	Json::Json(i64 val)
		: m_Type(Type::Integer)
		, m_Integer(val)
	{
	}

	Json::Json(i32 val)
		: m_Type(Type::Integer)
		, m_Integer(val)
	{
	}

	Json::Json(u64 val)
		: m_Type(Type::Unsigned)
		, m_Unsigned(val)
	{
	}

	Json::Json(u32 val)
		: m_Type(Type::Unsigned)
		, m_Unsigned(val)
	{
	}

	Json::Json(const AnsiChar* val)
		: m_Type(Type::String)
		, m_String(val)
	{
	}

	Json::Json(const WideChar* val)
		: m_Type(Type::String)
		, m_String(val)
	{
	}

	Json::Json(const AnsiString& val)
		: m_Type(Type::String)
		, m_String(val)
	{
	}

	Json::Json(const WideString& val)
		: m_Type(Type::String)
		, m_String(val)
	{
	}

	Json::Json(const Json& json)
		: m_Type(json.m_Type)
	{
		switch (m_Type)
		{
		case Type::Object:
			new (&m_SubObjects) HashMap<String, Json>(json.m_SubObjects);
			break;
		case Type::Array:
			new (&m_Array) DynArray<Json>(json.m_Array);
			break;
		case Type::String:
			new (&m_String) String(json.m_String);
			break;
		case Type::Float:
			m_Float = json.m_Float;
			break;
		case Type::Integer:
			m_Integer = json.m_Integer;
			break;
		case Type::Unsigned:
			m_Unsigned = json.m_Unsigned;
			break;
		case Type::Bool:
			m_Bool = json.m_Bool;
			break;
		default:
			break;
		}
	}

	Json::Json(Json&& json) noexcept
		: m_Type(json.m_Type)
	{
		switch (m_Type)
		{
		case Type::Object:
			new (&m_SubObjects) HashMap<String, Json>(Forward<HashMap<String, Json>>(json.m_SubObjects));
			break;
		case Type::Array:
			new (&m_Array) DynArray<Json>(Forward<DynArray<Json>>(json.m_Array));
			break;
		case Type::String:
			new (&m_String) String(Forward<String>(json.m_String));
			break;
		case Type::Float:
			m_Float = json.m_Float;
			break;
		case Type::Integer:
			m_Integer = json.m_Integer;
			break;
		case Type::Bool:
			m_Bool = json.m_Bool;
			break;
		default:
			break;
		}
	}

	Json::~Json()
	{
		Cleanup();
	}

	Json& Json::operator=(const Json& json)
	{
		Cleanup();
		m_Type = json.m_Type;
		switch (m_Type)
		{
		case Type::Object:
			new (&m_SubObjects) HashMap<String, Json>(json.m_SubObjects);
			break;
		case Type::Array:
			new (&m_Array) DynArray<Json>(json.m_Array);
			break;
		case Type::String:
			new (&m_String) String(json.m_String);
			break;
		case Type::Float:
			m_Float = json.m_Float;
			break;
		case Type::Integer:
			m_Integer = json.m_Integer;
			break;
		case Type::Unsigned:
			m_Unsigned = json.m_Unsigned;
			break;
		case Type::Bool:
			m_Bool = json.m_Bool;
			break;
		default: 
			break;
		}
		return *this;
	}

	Json& Json::operator=(Json&& json) noexcept
	{
		m_Type = json.m_Type;
		switch (m_Type)
		{
		case Type::Object:
			m_SubObjects = Forward<HashMap<String, Json>>(json.m_SubObjects);
			break;
		case Type::Array:
			m_Array = Forward<DynArray<Json>>(json.m_Array);
			break;
		case Type::String:
			m_String = Forward<String>(json.m_String);
			break;
		case Type::Float:
			m_Float = json.m_Float;
			break;
		case Type::Integer:
			m_Integer = json.m_Integer;
			break;
		case Type::Bool:
			m_Bool = json.m_Bool;
			break;
		default:
			break;
		}
		return *this;
	}

	Json& Json::operator[](const AnsiChar* key)
	{
		HV_ASSERT(m_Type == Type::Object);
		return operator[](String(key));
	}

	Json& Json::operator[](const WideChar* key)
	{
		HV_ASSERT(m_Type == Type::Object);
		return operator[](String(key));
	}

	Json& Json::operator[](const AnsiString& key)
	{
		HV_ASSERT(m_Type == Type::Object);
		return operator[](String(key));
	}

	Json& Json::operator[](const WideString& key)
	{
		HV_ASSERT(m_Type == Type::Object);
		auto it = m_SubObjects.Find(key);
		if (it == m_SubObjects.Back())
		{
			auto pair = m_SubObjects.Insert(key, Json());
			return pair.first->second;
		}
		return it->second;
	}

	Json& Json::operator[](u32 idx)
	{
		HV_ASSERT(m_Type == Type::Array);
		return m_Array[idx];
	}

	void Json::SetBool(b8 val)
	{
		Cleanup();
		m_Type = Type::Bool;
		m_Bool = val;
	}

	void Json::SetFloat(f64 val)
	{
		Cleanup();
		m_Type = Type::Float;
		m_Float = val;
	}

	void Json::SetInteger(i64 val)
	{
		Cleanup();
		m_Type = Type::Integer;
		m_Integer = val;
	}

	void Json::SetUnsigned(u64 val)
	{
		Cleanup();
		m_Type = Type::Unsigned;
		m_Unsigned = val;
	}

	void Json::SetString(const AnsiChar* val)
	{
		Cleanup();
		m_Type = Type::String;
		new (&m_String) String(val);
	}

	void Json::SetString(const WideChar* val)
	{
		Cleanup();
		m_Type = Type::String;
		new (&m_String) String(val);
	}

	void Json::SetString(const AnsiString& val)
	{
		Cleanup();
		m_Type = Type::String;
		new (&m_String) String(val);
	}

	void Json::SetString(const WideString& val)
	{
		Cleanup();
		m_Type = Type::String;
		new (&m_String) String(val);
	}

	void Json::SetObject()
	{
		Cleanup();
		m_Type = Type::Object;
		new (&m_SubObjects) HashMap<String, Json>();
	}

	void Json::SetArray()
	{
		Cleanup();
		m_Type = Type::Array;
		new (&m_Array) DynArray<Json>();
	}

	void Json::ArrayPush(const Json& json)
	{
		HV_ASSERT(m_Type == Type::Array);
		m_Array.Push(json);
	}

	void Json::Cleanup()
	{
		switch (m_Type)
		{
		case Type::String:
			m_String.~String();
			break;
		case Type::Object:
			m_SubObjects.~HashMap();
			break;
		case Type::Array:
			m_Array.~DynArray();
			break;
		default:
			break;
		}
	}
}
