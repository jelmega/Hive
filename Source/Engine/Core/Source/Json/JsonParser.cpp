// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// JsonParser.h: Json parser
#include "Json/JsonParser.h"
#include "Logging/Logger.h"

namespace Hv::JsonParser {

	Json ParseFile(const String& filepath)
	{
		HvFS::File file = HvFS::CreateFile(HvFS::Path(filepath), HvFS::FileMode::Open, HvFS::AccessMode::Read, HvFS::ShareMode::Read);
		if (!file.IsValid())
			return Json();
		Json json = ParseFile(file);
		file.Close();
		return json;
	}

	Json ParseFile(FileSystem::File& file)
	{
		// Temp
		DynArray<u8> data;
		data.Resize(file.GetFileSize());
		file.ReadBytes(data.Data(), data.Size());
		return ParseFromMemoryA(data);
	}

	Json ParseFromMemoryA(const DynArray<u8>& data)
	{
		String str = String((AnsiChar*)data.Data(), data.Size());
		DynArray<String> lines = str.SplitChar("\n\0");

		Detail::JsonParseData jsonData = {};
		jsonData.pCurJson = &jsonData.json;
		jsonData.json.SetObject();
		for (String& line : lines)
		{
			Detail::InterpretLine(line, jsonData);
			if (jsonData.indentCount == 0)
				break;
		}
		return jsonData.json;
	}

	namespace Detail
	{
		void InterpretLine(const String& line, JsonParseData& data)
		{
			String tmp = line;
			tmp.Trim(); // Trim away whitespace (spaces and tabs)
			sizeT quote = tmp.Find('"');

			constexpr const AnsiChar* nonQuoteFindValue = "{}[]:,tTfF-0123456789";
			if (quote == String::NPos)
			{
				// No quotes, so {}[]:, or value (tTfF for bools and -0123456789 for numbers) or combination of them
				// Find the first character that isn't a space
				sizeT pos = tmp.FindAny(nonQuoteFindValue);
				while (pos != String::NPos)
				{
					sizeT next = tmp.FindAny(nonQuoteFindValue, pos + 1); // end is when another character is found
					sizeT end = tmp.Find(' ', pos + 1); // or when a space is found
					if (end > next)
						end = next;
					String value = tmp.SubString(pos, end - pos);
					InterpretValue(value, data);
					pos = next;
				}
			}
			else
			{
				sizeT pos = tmp.FindAny(nonQuoteFindValue);
				while (pos != String::NPos || quote != String::NPos)
				{
					sizeT next = tmp.FindAny(nonQuoteFindValue, pos + 1);
					if (pos < quote)
					{
						sizeT end = tmp.Find(' ', pos + 1); // or when a space is found
						if (end > next)
							end = next;
						String value = tmp.SubString(pos, end - pos);
						InterpretValue(value, data);
						pos = next;
					}
					else
					{
						sizeT endQuote = tmp.Find('"', quote + 1);
						while (endQuote != String::NPos && tmp[endQuote - 1] == '\\')
						{
							endQuote = tmp.Find('"', quote);
						}
						if (endQuote == String::NPos)
						{
							g_Logger.LogError("JSON: Invalid value or key!");
						}

						String value = tmp.SubString(quote + 1, endQuote - quote - 1);
						if (data.curKey.IsEmpty() && !data.inArray)
						{
							data.curKey = value;
						}
						else
						{
							Json json(value);
							if (data.inArray)
								(*data.pCurJson).ArrayPush(json);
							else
								(*data.pCurJson)[data.curKey] = json;
						}
						quote = tmp.Find('"', endQuote + 1);
						if (pos < quote)
							pos = tmp.FindAny(nonQuoteFindValue, endQuote + 1);
					}
				}
			}

		}

		void InterpretValue(String& value, JsonParseData& data)
		{
			switch (value[0])
			{
			case '{': // begin object
			{
				++data.indentCount;
				if (data.indentCount > 1)
				{
					if (data.inArray)
					{
						Json json;
						json.SetObject();
						data.pCurJson->ArrayPush(json);
						u32 idx = data.pCurJson->ArraySize() - 1;
						data.pCurJson = &(*data.pCurJson)[idx];
						data.indices.Push(idx);
					}
					else
					{
						if (data.curKey == "")
						{
							g_Logger.LogError("Json invalid object (no key to associate with)");
							return;
						}

						Json json;
						json.SetObject();
						(*data.pCurJson)[data.curKey] = json;
						data.pCurJson = &(*data.pCurJson)[data.curKey];
						data.keys.Push(data.curKey);
						data.curKey.Clear();
					}
				}
				data.inArray = false;
				break;
			}
			case '[': // begin list
			{
				++data.indentCount;
				if (data.inArray)
				{
					Json json;
					json.SetArray();
					data.pCurJson->ArrayPush(json);
					u32 idx = json.ArraySize() - 1;
					data.pCurJson = &(*data.pCurJson)[idx];
					data.indices.Push(idx);
				}
				else
				{
					if (data.curKey.IsEmpty())
					{
						g_Logger.LogError("Json invalid object (no key to associate with)");
						return;
					}

					Json json;
					json.SetArray();
					(*data.pCurJson)[data.curKey] = json;
					data.pCurJson = &(*data.pCurJson)[data.curKey];
					data.keys.Push(data.curKey);
					data.curKey.Clear();
				}
				data.inArray = true;
				break;
			}
			case ']': // end list
			case '}': // end object
			{
				--data.indentCount;
				if (data.indentCount > 0)
				{
					data.pCurJson = GetCurrentJson(data);
					if (data.pCurJson->IsArray())
						data.indices.Pop();
					else
						data.keys.Pop();
				}
				data.inArray = data.pCurJson->IsArray();
				break;
			}
			case ':': // key value seperator
			{
				break;
			}
			case ',': // next val
			{
				data.curKey.Clear();
				break;
			}
			default:	// Value (bool or numeric value)
			{
				constexpr const AnsiChar* numericChars = "0123456789.-";
				// Check if the value is a number or not
				sizeT cpos = value.FindAny(numericChars);
				if (cpos == 0) // value
				{
					if (value.Find('.') != String::NPos) // Floating point
					{
						if (value.Find('e') != String::NPos) // scientific notation
						{
							f64 val = ParseF64Sci(value);
							Json json(val);
							if (data.inArray)
								(*data.pCurJson).ArrayPush(json);
							else
								(*data.pCurJson)[data.curKey] = json;
						}
						else
						{
							f64 val = ParseF64(value);
							Json json(val);
							if (data.inArray)
								(*data.pCurJson).ArrayPush(json);
							else
								(*data.pCurJson)[data.curKey] = json;
						}
					}
					if (value.StartsWith('-')) // Integer
					{
						i64 val = ParseI64(value);
						Json json(val);
						if (data.inArray)
							(*data.pCurJson).ArrayPush(json);
						else
							(*data.pCurJson)[data.curKey] = json;
					}
					else // unsigned
					{
						u64 val = ParseU64(value);
						Json json(val);
						if (data.inArray)
							(*data.pCurJson).ArrayPush(json);
						else
							(*data.pCurJson)[data.curKey] = json;
					}
				}
				else
				{
					value.ToLower();
					if (value == "true")
					{
						Json json(true);
						if (data.inArray)
							(*data.pCurJson).ArrayPush(json);
						else
							(*data.pCurJson)[data.curKey] = json;
					}
					else if (value == "false")
					{
						Json json(false);
						if (data.inArray)
							(*data.pCurJson).ArrayPush(json);
						else
							(*data.pCurJson)[data.curKey] = json;
					}
					else
					{
						g_Logger.LogError("JSON: Invalid value!");
					}
				}
			}
			break;
			}
		}

		Json* GetCurrentJson(JsonParseData& data)
		{
			Json* json = &data.json;
			u32 keyIdx = 0, arrIdx = 0;
			HV_ASSERT(data.indentCount > 0);
			for (u32 i = 0; i < u8(data.indentCount - 1); ++i)
			{
				if (json->IsArray())
					json = &(*json)[data.indices[arrIdx++]];
				else
					json = &(*json)[data.keys[keyIdx++]];
			}
			return json;
		}
	}
}
