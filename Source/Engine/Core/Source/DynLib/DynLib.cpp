// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DynLib.h: Dynamic library handling
#pragma once
#include "DynLib/DynLib.h"
#include "FileSystem/FileSystem.h"

namespace Hv::DynLib {


	DynLibHandle Load(const String& path)
	{
		FileSystem::Path filePath = path;
		if (!FileSystem::DoesFileExist(filePath))
		{
			// Path not found and only a file is provided, try to load a system dynlib
			if (!filePath.HasParentPath() && filePath.HasFilename() && filePath.HasFileExtension())
				return HAL::DynLib::Load(path);
			return InvalidDynLibHandle;
		}

		String fullPath = FileSystem::ResolvePath(path).ToNativeString();
		return HAL::DynLib::Load(fullPath);	
	}

	void Unload(DynLibHandle& handle)
	{
		if (handle != InvalidDynLibHandle)
			HAL::DynLib::Unload(handle);
	}

	DynLibProcHandle GetProc(DynLibHandle& handle, const String& procName)
	{
		if (handle == InvalidDynLibHandle)
			return InvalidDynLibProcHandle;
		return HAL::DynLib::GetProc(handle, procName);
	}
}
