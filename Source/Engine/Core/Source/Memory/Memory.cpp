// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Memory.cpp: Main memory cpp
#include "Memory/Memory.h"

namespace Hv::Memory {

	IMemoryArena& GetGlobalAllocator()
	{
		static GlobalMemoryArena arena;
		return arena;
	}
}

void* operator new(size_t size)
{
	return g_Alloctor.Allocate(size, 8, HV_ALLOC_CONTEXT(Hv::Memory::AllocCategory::NewDelete));
}

void* operator new[](size_t size)
{
	return g_Alloctor.Allocate(size, 8, HV_ALLOC_CONTEXT(Hv::Memory::AllocCategory::NewDelete));
}

void operator delete(void* ptr)
{
	return g_Alloctor.Free(ptr, HV_ALLOC_CONTEXT(Hv::Memory::AllocCategory::NewDelete));
}

void operator delete[](void* ptr)
{
	return g_Alloctor.Free(ptr, HV_ALLOC_CONTEXT(Hv::Memory::AllocCategory::NewDelete));
}