// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// GloablMemoryAllocator.h: Global memory arena
#include "Memory/Arenas/GlobalMemoryArena.h"

namespace Hv::Memory {

#if HV_STATISTICS
#	define SUB_STATS(alloc)\
		m_Usage -= alloc.GetUsage();\
		m_PaddedUsage -= alloc.GetPaddedUsage();

#	define ADD_STATS(alloc)\
		m_Usage += alloc.GetUsage();\
		if (m_PeakUsage < m_Usage)\
			m_PeakUsage = m_Usage;\
		m_PaddedUsage += alloc.GetPaddedUsage();\
		if (m_PeakPaddedUsage < m_PaddedUsage)\
			m_PeakPaddedUsage = m_PaddedUsage;

#	define INC_ALLOCS\
		++m_NumAllocs;\
		if (m_PeakNumAllocs < m_NumAllocs)\
			m_PeakNumAllocs = m_NumAllocs;

#	define DEC_ALLOCS\
		--m_NumAllocs;

#else
#	define SUB_STATS(alloc)
#	define ADD_STATS(alloc)
#	define INC_ALLOCS
#	define DEC_ALLOCS
#endif

	Threading::CriticalSection GlobalMemoryArena::m_CriticalSection;

	GlobalMemoryArena::GlobalMemoryArena()
		: IMemoryArena(4, "Global memory arena")
		, m_PoolAlloc(m_PoolPageSize, m_PoolAlignment, m_PoolBlockSize, "[Global Arena] Pool Allocator")
		, m_FreelistAlloc(m_FreelistSize, 8, "[Global Arena] Freelist Allocator")
		, m_BuddyAlloc(m_BuddyMinSize, m_BuddyMaxSize, m_BuddyAlignment, "[Global Arena] Buddy Allocator")
		, m_Malloc("[Global Arena] Mallocator")
	{
	}

	GlobalMemoryArena::GlobalMemoryArena(const AnsiChar* name)
		: IMemoryArena(4, name)
		, m_PoolAlloc(m_PoolPageSize, m_PoolAlignment, m_PoolBlockSize, "[Global Arena] Pool Allocator")
		, m_FreelistAlloc(m_FreelistSize, 8, "[Global Arena] Freelist Allocator")
		, m_BuddyAlloc(m_BuddyMinSize, m_BuddyMaxSize, m_BuddyAlignment, "[Global Arena] Buddy Allocator")
		, m_Malloc("[Global Arena] Mallocator")
	{
	}

	GlobalMemoryArena::GlobalMemoryArena(GlobalMemoryArena&& arena) noexcept
		: IMemoryArena(Forward<GlobalMemoryArena>(arena))
		, m_PoolAlloc(Forward<PagedPoolAllocator>(arena.m_PoolAlloc))
		, m_FreelistAlloc(Forward<PagedFreeListAllocator>(arena.m_FreelistAlloc))
		, m_BuddyAlloc(Forward<PagedBuddyAllocator>(arena.m_BuddyAlloc))
		, m_Malloc(Forward<Mallocator>(arena.m_Malloc))
	{
	}

	GlobalMemoryArena::~GlobalMemoryArena()
	{
	}

	GlobalMemoryArena& GlobalMemoryArena::operator=(GlobalMemoryArena&& arena) noexcept
	{
		IMemoryArena::operator=(Forward<GlobalMemoryArena>(arena));
		m_PoolAlloc = Forward<PagedPoolAllocator>(arena.m_PoolAlloc);
		m_FreelistAlloc = Forward<PagedFreeListAllocator>(arena.m_FreelistAlloc);
		m_BuddyAlloc = Forward<PagedBuddyAllocator>(arena.m_BuddyAlloc);
		m_Malloc = Forward<Mallocator>(arena.m_Malloc);
		return *this;
	}

	void* GlobalMemoryArena::Allocate(sizeT size, u16 alignment, AllocContext context)
	{
		m_CriticalSection.Enter();
		HV_UNREFERENCED_PARAM(context);
		HV_ASSERT_MSG(size != 0, "Buffer can't be 0 bytes!");
		HV_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");

		void* ptr = nullptr;
		if (size <= m_PoolThreshold && alignment <= m_PoolAlignment)
		{
			SUB_STATS(m_PoolAlloc);
			ptr = m_PoolAlloc.Allocate(size, alignment);
			ADD_STATS(m_PoolAlloc);
			INC_ALLOCS;
		}
		else if (size <= m_FreelistThreshold)
		{
			SUB_STATS(m_FreelistAlloc);
			ptr = m_FreelistAlloc.Allocate(size, alignment);
			ADD_STATS(m_FreelistAlloc);
			INC_ALLOCS;
		}
		else if (size <= m_BuddyThreshold && alignment <= m_BuddyAlignment)
		{
			SUB_STATS(m_BuddyAlloc);
			ptr = m_BuddyAlloc.Allocate(size, alignment);
			ADD_STATS(m_BuddyAlloc);
			INC_ALLOCS;
		}
		else
		{
			SUB_STATS(m_Malloc);
			ptr = m_Malloc.Allocate(size, alignment);
			ADD_STATS(m_Malloc);
			INC_ALLOCS;
		}

		m_CriticalSection.Leave();
		return ptr;
	}

	void* GlobalMemoryArena::Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context)
	{
		m_CriticalSection.Enter();
		HV_UNREFERENCED_PARAM(context);
		HV_ASSERT(pOriginal || size);

		if (!pOriginal)
			return Allocate(size, alignment);
		if (!size)
		{
			Free(pOriginal);
			return nullptr;
		}

		HV_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");

		void* ptr = nullptr;
		if (m_PoolAlloc.Owns(pOriginal))
		{
			if (size <= m_PoolThreshold && alignment <= m_PoolAlignment)
			{
				SUB_STATS(m_PoolAlloc);
				ptr = m_PoolAlloc.Reallocate(pOriginal, size, alignment);
				ADD_STATS(m_PoolAlloc);
			}
			else
			{
				ptr = Allocate(size, alignment);
				Memory::Copy(ptr, pOriginal, size);

				SUB_STATS(m_PoolAlloc);
				m_PoolAlloc.Free(pOriginal);
				ADD_STATS(m_PoolAlloc);
				DEC_ALLOCS;
			}
		}
		else if (m_FreelistAlloc.Owns(pOriginal))
		{
			if (size > m_PoolThreshold && size <= m_FreelistThreshold)
			{
				SUB_STATS(m_FreelistAlloc);
				ptr = m_FreelistAlloc.Reallocate(pOriginal, size, alignment);
				ADD_STATS(m_FreelistAlloc);
			}
			else
			{
				ptr = Allocate(size, alignment);
				Memory::Copy(ptr, pOriginal, size);

				SUB_STATS(m_FreelistAlloc);
				m_FreelistAlloc.Free(pOriginal);
				ADD_STATS(m_FreelistAlloc);
				DEC_ALLOCS;
			}
		}
		else if (m_BuddyAlloc.Owns(pOriginal))
		{
			if (size > m_FreelistThreshold && size <= m_BuddyThreshold && alignment <= m_BuddyAlignment)
			{
				SUB_STATS(m_BuddyAlloc);
				ptr = m_BuddyAlloc.Reallocate(pOriginal, size, alignment);
				ADD_STATS(m_BuddyAlloc);
			}
			else
			{
				ptr = Allocate(size, alignment);
				Memory::Copy(ptr, pOriginal, size);

				SUB_STATS(m_BuddyAlloc);
				m_BuddyAlloc.Free(pOriginal);
				ADD_STATS(m_BuddyAlloc);
				DEC_ALLOCS;
			}
		}
		else if(m_Malloc.Owns(pOriginal))
		{
			if (size > m_BuddyThreshold)
			{
				SUB_STATS(m_Malloc);
				m_Malloc.Reallocate(pOriginal, size, alignment);
				ADD_STATS(m_Malloc);
			}
			else
			{
				ptr = Allocate(size, alignment);
				Memory::Copy(ptr, pOriginal, size);

				SUB_STATS(m_Malloc);
				m_Malloc.Free(pOriginal);
				ADD_STATS(m_Malloc);
				DEC_ALLOCS;
			}
		}
		m_CriticalSection.Leave();
		return ptr;
	}

	void GlobalMemoryArena::Free(void* ptr, AllocContext context)
	{
		m_CriticalSection.Enter();
		HV_UNREFERENCED_PARAM(context);

		if (m_PoolAlloc.Owns(ptr))
		{
			SUB_STATS(m_PoolAlloc);
			m_PoolAlloc.Free(ptr);
			ADD_STATS(m_PoolAlloc);
			DEC_ALLOCS;
		}
		else if (m_FreelistAlloc.Owns(ptr))
		{
			SUB_STATS(m_FreelistAlloc);
			m_FreelistAlloc.Free(ptr);
			ADD_STATS(m_FreelistAlloc);
			DEC_ALLOCS;
		}
		else if (m_BuddyAlloc.Owns(ptr))
		{
			SUB_STATS(m_BuddyAlloc);
			m_BuddyAlloc.Free(ptr);
			ADD_STATS(m_BuddyAlloc);
			DEC_ALLOCS;
		}
		else if (m_Malloc.Owns(ptr))
		{
			SUB_STATS(m_Malloc);
			m_Malloc.Free(ptr);
			ADD_STATS(m_Malloc);
			DEC_ALLOCS;
		}
		m_CriticalSection.Leave();
	}

	b8 GlobalMemoryArena::Owns(void* ptr)
	{
		m_CriticalSection.Enter();
		b8 res = m_PoolAlloc.Owns(ptr) ||
			m_FreelistAlloc.Owns(ptr) ||
			m_BuddyAlloc.Owns(ptr) ||
			m_Malloc.Owns(ptr);
		m_CriticalSection.Leave();
		return res;
	}

	sizeT GlobalMemoryArena::GetAllocatorThreshold(u8 index)
	{
		HV_ASSERT(index < m_NumAllocators);
		switch (index)
		{
		case 0:
			return m_PoolThreshold;
		case 1:
			return m_FreelistThreshold;
		case 2:
			return m_BuddyThreshold;
		default:
			return sizeT(-1);
		}
	}

	IAllocator* GlobalMemoryArena::GetAllocator(u8 index)
	{
		HV_ASSERT(index < m_NumAllocators);
		switch (index)
		{
		case 0:
			return &m_PoolAlloc;
		case 1:
			return &m_FreelistAlloc;
		case 2:
			return &m_BuddyAlloc;
		default:
			return &m_Malloc;
		}
	}

#undef ADD_STATS
#undef SUB_STATS
#undef INC_ALLOCS
#undef DEC_ALLOCS
}
