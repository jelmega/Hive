// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// IMemoryAllocator.cpp: Memory arena interface
#include "Memory/Arenas/IMemoryArena.h"

namespace Hv::Memory {


	IMemoryArena::IMemoryArena()
		: IAllocator()
		, m_NumAllocators(0)
	{
	}

	IMemoryArena::IMemoryArena(u8 numAllocators, const AnsiChar* name)
		: IAllocator(name)
		, m_NumAllocators(numAllocators)
	{
	}

	IMemoryArena::IMemoryArena(IMemoryArena&& arena) noexcept
		: IAllocator(Forward<IMemoryArena>(arena))
		, m_NumAllocators(arena.m_NumAllocators)
	{
	}

	IMemoryArena::~IMemoryArena()
	{
	}

	IMemoryArena& IMemoryArena::operator=(IMemoryArena&& arena) noexcept
	{
		IAllocator::operator=(Forward<IMemoryArena>(arena));
		m_NumAllocators = arena.m_NumAllocators;
		return *this;
	}
}
