// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// IAllocator.h: Allocator interface
#include "Memory/Allocators/IAllocator.h"

namespace Hv::Memory {

	IAllocator::IAllocator()
		: m_Size(0)
		, m_Alignment(0)
		, m_OwnsBuffer(false)
		, m_pBuffer(nullptr)
		, m_NumAllocs(0)
		, m_PeakNumAllocs(0)
		, m_Usage(0)
		, m_PeakUsage(0)
		, m_PaddedUsage(0)
		, m_PeakPaddedUsage(0)
		, m_Name("")
	{
	}

	IAllocator::IAllocator(const AnsiChar* name)
		: m_Size(0)
		, m_Alignment(0)
		, m_OwnsBuffer(false)
		, m_pBuffer(nullptr)
		, m_NumAllocs(0)
		, m_PeakNumAllocs(0)
		, m_Usage(0)
		, m_PeakUsage(0)
		, m_PaddedUsage(0)
		, m_PeakPaddedUsage(0)
		, m_Name(name)
	{

	}

	IAllocator::IAllocator(IAllocator&& alloc) noexcept
		: m_Size(alloc.m_Size)
		, m_Alignment(alloc.m_Alignment)
		, m_OwnsBuffer(alloc.m_OwnsBuffer)
		, m_pBuffer(alloc.m_pBuffer)
		, m_NumAllocs(alloc.m_NumAllocs)
		, m_PeakNumAllocs(alloc.m_PeakNumAllocs)
		, m_Usage(alloc.m_Usage)
		, m_PeakUsage(alloc.m_PeakUsage)
		, m_PaddedUsage(alloc.m_PaddedUsage)
		, m_PeakPaddedUsage(alloc.m_PeakPaddedUsage)
		, m_Name(alloc.m_Name)
	{
		alloc.m_pBuffer = nullptr;
	}

	IAllocator::~IAllocator()
	{
	}

	IAllocator& IAllocator::operator=(IAllocator&& alloc) noexcept
	{
		m_Size = alloc.m_Size;
		m_Alignment = alloc.m_Alignment;
		m_OwnsBuffer = alloc.m_OwnsBuffer;
		m_pBuffer = alloc.m_pBuffer;
		m_NumAllocs = alloc.m_NumAllocs;
		m_PeakNumAllocs = alloc.m_PeakNumAllocs;
		m_Usage = alloc.m_Usage;
		m_PeakUsage = alloc.m_PeakUsage;
		m_PaddedUsage = alloc.m_PaddedUsage;
		m_PeakPaddedUsage = alloc.m_PeakPaddedUsage;
		m_Name = alloc.m_Name;

		alloc.m_pBuffer = nullptr;

		return *this;
	}
}
