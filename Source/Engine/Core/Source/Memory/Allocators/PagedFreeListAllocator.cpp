// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PagedFreeListAllocator.h: Paged freelist allocator
#include "Memory/Allocators/PagedFreeListAllocator.h"

namespace Hv::Memory {

	PagedFreeListAllocator::PagedFreeListAllocator()
		: IPagedAllocator()
	{
	}

	PagedFreeListAllocator::PagedFreeListAllocator(sizeT size, u16 alignment, const AnsiChar* name)
		: IPagedAllocator(size, alignment, name)
	{
		HV_ASSERT_MSG(size != 0, "Buffer can't be 0 bytes!");
		HV_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");

		m_pFirst = m_pLast = CreatePage();
	}

	PagedFreeListAllocator::PagedFreeListAllocator(PagedFreeListAllocator&& alloc) noexcept
		: IPagedAllocator(Forward<PagedFreeListAllocator>(alloc))
	{
	}

	PagedFreeListAllocator::~PagedFreeListAllocator()
	{
	}

	PagedFreeListAllocator& PagedFreeListAllocator::operator=(PagedFreeListAllocator&& alloc) noexcept
	{
		IPagedAllocator<FreeListAllocator>::operator=(Forward<PagedFreeListAllocator>(alloc));
		return *this;
	}

	void* PagedFreeListAllocator::Allocate(sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");
		HV_ASSERT_MSG(alignment <= m_Alignment, "Alignment needs to be the smaller or equal to the allocators alignment!");

		return IPagedAllocator<FreeListAllocator>::Allocate(size, alignment);
	}

	void* PagedFreeListAllocator::Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		HV_ASSERT(pOriginal || size);

		return IPagedAllocator<FreeListAllocator>::Reallocate(pOriginal, size, alignment);
	}

	void PagedFreeListAllocator::Free(void* ptr, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		HV_ASSERT_MSG(ptr != nullptr, "can't free a nullptr!");

		IPagedAllocator<FreeListAllocator>::Free(ptr);
	}

	b8 PagedFreeListAllocator::Owns(void* ptr)
	{
		return IPagedAllocator<FreeListAllocator>::Owns(ptr);
	}

	IPagedAllocator<FreeListAllocator>::Page* PagedFreeListAllocator::CreatePage()
	{
		Page* pPage = (Page*)Memory::Allocate(sizeof(Page), 8, HV_ALLOC_CONTEXT(AllocCategory::Allocator));
		pPage->pNext = nullptr;
		pPage->alloc = FreeListAllocator(m_Size, m_Alignment, "Freelist Allocator (Page)");
		return pPage;
	}
}
