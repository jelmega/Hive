// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PoolAllocator.h: Pool allocator

#include "Memory/Allocators/PoolAllocator.h"

namespace Hv::Memory {


	PoolAllocator::PoolAllocator()
		: IAllocator()
		, m_BlockSize(0)
		, m_AlignedBlockSize(0)
		, m_pFree(nullptr)
	{
	}

	PoolAllocator::PoolAllocator(sizeT size, u16 alignment, u32 blockSize, const AnsiChar* name)
		: IAllocator(name)
		, m_BlockSize(blockSize)
		, m_pFree(nullptr)
	{
		HV_ASSERT_MSG(size, "Size needs to be bigger than 0!");
		HV_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");
		HV_ASSERT_MSG(blockSize > sizeof(FreeBlock), "Poolsize needs to be larger than a FreeBlock (size of a pointer)");

		m_Size = size;
		m_Alignment = alignment;
		m_pBuffer = (u8*)Memory::Allocate(size, alignment, HV_ALLOC_CONTEXT(AllocCategory::Allocator));
		Memory::Clear(m_pBuffer, m_Size);
		m_OwnsBuffer = true;

		// Setup pool
		sizeT alignMask = alignment - 1;
		m_AlignedBlockSize = u32((blockSize + alignMask) & ~alignMask);

		FreeBlock* pPrev = nullptr;
		for (sizeT i = 0; i < m_Size; i += m_AlignedBlockSize)
		{
			FreeBlock* pBlock = (FreeBlock*)(m_pBuffer + i);
			if (pPrev)
				pPrev->pNext = pBlock;
			pPrev = pBlock;
		}
		m_pFree = (FreeBlock*)m_pBuffer;
	}

	PoolAllocator::PoolAllocator(u8* pBuffer, sizeT size, u16 alignment, u32 blockSize, b8 transferOwnership,
		const AnsiChar* name)
		: IAllocator(name)
		, m_BlockSize(blockSize)
		, m_pFree(nullptr)
	{
		HV_ASSERT_MSG(size, "Size needs to be bigger than 0!");
		HV_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");
		HV_ASSERT_MSG(blockSize >= sizeof(FreeBlock), "Poolsize needs to be larger than, or the same size as, a FreeBlock (size of a pointer)");
		HV_ASSERT_MSG(pBuffer, "pBuffer can't be nullptr");

		m_Size = size;
		m_Alignment = alignment;
		m_pBuffer = pBuffer;
		Memory::Clear(m_pBuffer, m_Size);
		m_OwnsBuffer = transferOwnership;

		// Setup pool
		sizeT alignMask = alignment - 1;
		m_AlignedBlockSize = u32((blockSize + alignMask) & ~alignMask);

		FreeBlock* pPrev = nullptr;
		for (sizeT i = 0; i < m_Size; i += m_AlignedBlockSize)
		{
			FreeBlock* pBlock = (FreeBlock*)(m_pBuffer + i);
			if (pPrev)
				pPrev->pNext = pBlock;
			pPrev = pBlock;
		}
		m_pFree = (FreeBlock*)m_pBuffer;
	}

	PoolAllocator::PoolAllocator(PoolAllocator&& alloc) noexcept
		: IAllocator(Forward<PoolAllocator>(alloc))
		, m_BlockSize(alloc.m_BlockSize)
		, m_AlignedBlockSize(alloc.m_AlignedBlockSize)
		, m_pFree(alloc.m_pFree)
	{
		alloc.m_pFree = nullptr;
	}

	PoolAllocator::~PoolAllocator()
	{
		if (m_OwnsBuffer && m_pBuffer)
			Memory::Free(m_pBuffer, HV_ALLOC_CONTEXT(AllocCategory::Allocator));
	}

	PoolAllocator& PoolAllocator::operator=(PoolAllocator&& alloc) noexcept
	{
		IAllocator::operator=(Forward<PoolAllocator>(alloc));
		m_BlockSize = alloc.m_BlockSize;
		m_AlignedBlockSize = alloc.m_AlignedBlockSize;
		m_pFree = alloc.m_pFree;
		alloc.m_pFree = nullptr;
		return *this;
	}

	void* PoolAllocator::Allocate(sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		HV_UNREFERENCED_PARAM(alignment); // Only used for assertions
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");
		HV_ASSERT_MSG(alignment <= m_Alignment, "Alignment needs to be the smaller or equal to the allocators alignment!");

		if (!m_pBuffer || !m_pFree || size > m_BlockSize)
			return nullptr;

		FreeBlock* ptr = (FreeBlock*)m_pFree;
		m_pFree = m_pFree->pNext;
		ptr->pNext = nullptr;

#if HV_STATISTICS
		++m_NumAllocs;
		if (m_PeakNumAllocs < m_NumAllocs)
			m_PeakNumAllocs = m_NumAllocs;
		m_Usage += m_AlignedBlockSize;
		if (m_PeakUsage < m_Usage)
			m_PeakUsage = m_Usage;
		m_PaddedUsage += m_AlignedBlockSize;
		if (m_PeakPaddedUsage < m_PaddedUsage)
			m_PeakPaddedUsage = m_PaddedUsage;
#endif
		return ptr;
	}

	void* PoolAllocator::Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context)
	{
		if (!m_pBuffer || !m_pFree || size > m_BlockSize)
			return nullptr;

		HV_UNREFERENCED_PARAM(context);
		HV_ASSERT(pOriginal || size);

		if (pOriginal == nullptr)
		{
			return Allocate(size, alignment);
		}
		if (size == 0)
		{
			Free(pOriginal);
			return nullptr;
		}

		HV_ASSERT_MSG(size <= m_BlockSize, "Size needs to be smaller than, or the same size as, the size of a block");
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");
		HV_ASSERT_MSG(alignment <= m_Alignment, "Alignment needs to be the smaller or equal to the allocators alignment!");

		return pOriginal;
	}

	void PoolAllocator::Free(void* ptr, AllocContext context)
	{
		if (!m_pBuffer)
			return;

		HV_UNREFERENCED_PARAM(context);
		HV_ASSERT_MSG(ptr != nullptr, "can't free a nullptr!");

		Memory::Clear(ptr, m_AlignedBlockSize);
		((FreeBlock*)ptr)->pNext = m_pFree;
		m_pFree = (FreeBlock*)ptr;


#if HV_STATISTICS
		--m_NumAllocs;
		m_Usage -= m_AlignedBlockSize;
		m_PaddedUsage -= m_AlignedBlockSize;
#endif
	}

	b8 PoolAllocator::Owns(void* ptr)
	{
		return ptr >= m_pBuffer && ptr <= m_pBuffer + m_Size;
	}
}
