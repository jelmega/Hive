// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PagedPoolAllocator.h: Paged pool allocator
#include "Memory/Allocators/PagedPoolAllocator.h"

namespace Hv::Memory {

	PagedPoolAllocator::PagedPoolAllocator()
		: IPagedAllocator()
		, m_BlockSize(0)
	{
	}

	PagedPoolAllocator::PagedPoolAllocator(sizeT size, u16 alignment, u32 blockSize, const AnsiChar* name)
		: IPagedAllocator(size, alignment, name)
		, m_BlockSize(blockSize)
	{
		HV_ASSERT_MSG(size, "Size needs to be bigger than 0!");
		HV_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");
		HV_ASSERT_MSG(blockSize > sizeof(PoolAllocator::FreeBlock), "Poolsize needs to be larger than a FreeBlock (size of a pointer)");

		m_pFirst = m_pLast = CreatePage();
	}

	PagedPoolAllocator::PagedPoolAllocator(PagedPoolAllocator&& alloc) noexcept
		: IPagedAllocator(Forward<PagedPoolAllocator>(alloc))
		, m_BlockSize(alloc.m_BlockSize)
	{
	}

	PagedPoolAllocator::~PagedPoolAllocator()
	{
	}

	PagedPoolAllocator& PagedPoolAllocator::operator=(PagedPoolAllocator&& alloc) noexcept
	{
		IPagedAllocator<PoolAllocator>::operator=(Forward<PagedPoolAllocator>(alloc));
		m_BlockSize = alloc.m_BlockSize;
		return *this;
	}

	void* PagedPoolAllocator::Allocate(sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");
		HV_ASSERT_MSG(alignment <= m_Alignment, "Alignment needs to be the smaller or equal to the allocators alignment!");

		return IPagedAllocator<PoolAllocator>::Allocate(size, alignment);
	}

	void* PagedPoolAllocator::Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		HV_ASSERT(pOriginal || size);

		return IPagedAllocator<PoolAllocator>::Reallocate(pOriginal, size, alignment);
	}

	void PagedPoolAllocator::Free(void* ptr, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		HV_ASSERT_MSG(ptr != nullptr, "can't free a nullptr!");

		IPagedAllocator<PoolAllocator>::Free(ptr);
	}

	b8 PagedPoolAllocator::Owns(void* ptr)
	{
		return IPagedAllocator<PoolAllocator>::Owns(ptr);
	}

	IPagedAllocator<PoolAllocator>::Page* PagedPoolAllocator::CreatePage()
	{
		Page* pPage = (Page*)Memory::Allocate(sizeof(Page), 8, HV_ALLOC_CONTEXT(AllocCategory::Allocator));
		pPage->pNext = nullptr;
		pPage->alloc = PoolAllocator(m_Size, m_Alignment, m_BlockSize, "Pool Allocator (Page)");
		return pPage;
	}
}
