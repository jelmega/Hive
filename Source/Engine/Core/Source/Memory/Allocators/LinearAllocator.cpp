// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// LinearAllocator.cpp: Linear allocator
#include "Memory/Allocators/LinearAllocator.h"
#include "Memory/MemoryUtil.h"

namespace Hv::Memory {


	LinearAllocator::LinearAllocator()
		: IAllocator()
		, m_Index(0)
	{
	}

	LinearAllocator::LinearAllocator(sizeT size, u16 alignment, const AnsiChar* name)
		: IAllocator(name)
		, m_Index(0)
	{
		HV_ASSERT_MSG(size, "Size needs to be bigger than 0!");
		HV_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");

		m_Size = size;
		m_Alignment = alignment;
		m_pBuffer = (u8*)Memory::Allocate(size, alignment, HV_ALLOC_CONTEXT(AllocCategory::Allocator));
		Memory::Clear(m_pBuffer, m_Size);
		m_OwnsBuffer = true;
	}

	LinearAllocator::LinearAllocator(u8* pBuffer, sizeT size, u16 alignment, b8 transferOwnership, const AnsiChar* name)
		: IAllocator(name)
		, m_Index(0)
	{
		HV_ASSERT_MSG(size, "Size needs to be bigger than 0!");
		HV_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");
		HV_ASSERT_MSG(pBuffer, "pBuffer can't be nullptr");

		m_Size = size;
		m_Alignment = alignment;
		m_pBuffer = pBuffer;
		m_OwnsBuffer = transferOwnership;
	}

	LinearAllocator::LinearAllocator(LinearAllocator&& alloc) noexcept
		: IAllocator(Forward<LinearAllocator>(alloc))
		, m_Index(alloc.m_Index)
	{
	}

	LinearAllocator::~LinearAllocator()
	{
		if (m_OwnsBuffer && m_pBuffer)
			Memory::Free(m_pBuffer, HV_ALLOC_CONTEXT(AllocCategory::Allocator));
	}

	LinearAllocator& LinearAllocator::operator=(LinearAllocator&& alloc) noexcept
	{
		IAllocator::operator=(Forward<LinearAllocator>(alloc));
		m_Index = alloc.m_Index;
		return *this;
	}

	void* LinearAllocator::Allocate(sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);

		if (!m_pBuffer || size > m_Size - m_Index)
			return nullptr;

		HV_ASSERT_MSG(size, "Can't allocate 0 bytes!");
		HV_ASSERT_MSG(alignment, "Can't align to 0 bytes!");
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");

		sizeT cur = sizeT(m_pBuffer) + m_Index;
		u16 padding = cur & (alignment - 1);
		u8* ptr = (u8*)cur + padding;
		sizeT total = padding + size;
		m_Index += total;

#if HV_STATISTICS
		++m_NumAllocs;
		if (m_PeakNumAllocs < m_NumAllocs)
			m_PeakNumAllocs = m_NumAllocs;
		m_Usage += size;;
		if (m_PeakUsage < m_Usage)
			m_PeakUsage = m_Usage;
		m_PaddedUsage += total;
		if (m_PeakPaddedUsage < m_PaddedUsage)
			m_PeakPaddedUsage = m_PaddedUsage;
#endif

		return ptr;
	}

	void* LinearAllocator::Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		HV_UNREFERENCED_PARAM(pOriginal);
		HV_UNREFERENCED_PARAM(size);
		HV_UNREFERENCED_PARAM(alignment);

		HV_ASSERT_MSG(false, "Linear Allocator can't reallocate pointers!");
		return nullptr;
	}

	void LinearAllocator::Free(void* ptr, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		HV_UNREFERENCED_PARAM(ptr);

		HV_ASSERT_MSG(false, "Linear Allocator can't free pointers, use Reset instead!");
	}

	b8 LinearAllocator::Owns(void* ptr)
	{
		return ptr >= m_pBuffer && ptr < m_pBuffer + m_Size;
	}

	void LinearAllocator::Reset()
	{
		m_Index = 0;
		if (m_pBuffer && m_Size)
			Memory::Clear(m_pBuffer, m_Size);

#if HV_STATISTICS
		m_NumAllocs = 0;
		m_Usage = 0;
		m_PaddedUsage = 0;
#endif
	}
}
