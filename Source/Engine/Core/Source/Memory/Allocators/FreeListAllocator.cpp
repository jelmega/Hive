// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// FreeListAllocator.h: Freelist allocator
#include "Memory/Allocators/FreelistAllocator.h"

namespace Hv::Memory {

	FreeListAllocator::FreeListAllocator()
		: IAllocator()
		, m_pFreeList(nullptr)
	{
	}

	FreeListAllocator::FreeListAllocator(sizeT size, u16 alignment, const AnsiChar* name)
		: IAllocator(name)
		, m_pFreeList(nullptr)
	{
		HV_ASSERT_MSG(size != 0, "Buffer can't be 0 bytes!");
		HV_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");

		m_Size = size;
		m_Alignment = alignment;
		m_pBuffer = static_cast<u8*>(Memory::Allocate(m_Size, m_Alignment));
		Memory::Clear(m_pBuffer, m_Size);
		m_OwnsBuffer = true;

		// Free list
		m_pFreeList = m_pBuffer;
		((FreeHeader*)m_pFreeList)->size = m_Size;
		((FreeHeader*)m_pFreeList)->pNext = nullptr;
	}

	FreeListAllocator::FreeListAllocator(u8* pBuffer, sizeT size, u16 alignment, b8 transferOwnership,
		const AnsiChar* name)
		: IAllocator(name)
		, m_pFreeList(nullptr)
	{
		HV_ASSERT_MSG(size != 0, "Buffer can't be 0 bytes!");
		HV_ASSERT_MSG(pBuffer, "Can't use nullptr as buffer!");
		HV_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");
		HV_ASSERT_MSG(pBuffer, "pBuffer can't be nullptr");

		m_Size = size;
		m_Alignment = alignment;
		m_pBuffer = static_cast<u8*>(pBuffer);
		Memory::Clear(m_pBuffer, m_Size);
		m_OwnsBuffer = transferOwnership;

		// Free list
		m_pFreeList = m_pBuffer;
		((FreeHeader*)m_pFreeList)->size = m_Size;
		((FreeHeader*)m_pFreeList)->pNext = nullptr;
	}

	FreeListAllocator::FreeListAllocator(FreeListAllocator&& alloc) noexcept
		: IAllocator(Forward<FreeListAllocator>(alloc))
		, m_pFreeList(alloc.m_pFreeList)
	{
		alloc.m_pFreeList = nullptr;
	}

	FreeListAllocator::~FreeListAllocator()
	{
		if (m_OwnsBuffer && m_pBuffer)
			Memory::Free(m_pBuffer, HV_ALLOC_CONTEXT(AllocCategory::Allocator));
	}

	FreeListAllocator& FreeListAllocator::operator=(FreeListAllocator&& alloc) noexcept
	{
		IAllocator::operator=(Forward<FreeListAllocator>(alloc));
		m_pFreeList = alloc.m_pFreeList;
		alloc.m_pFreeList = nullptr;
		return *this;
	}

	void* FreeListAllocator::Allocate(sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		HV_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");

		if (!m_pBuffer || !m_pFreeList || size > m_Size)
			return nullptr;

		u16 padding = 0;
		FreeHeader* pPrevHeader = nullptr;
		FreeHeader* pFreeHeader = FindFreeSpace(size, alignment, padding, pPrevHeader);
		if (!pFreeHeader)
			return nullptr;

		sizeT newSize = pFreeHeader->size - padding - size;
		u8* ptr = (u8*)pFreeHeader + padding;
		Header* pHeader = (Header*)(ptr - sizeof(Header));

		if (newSize < sizeof(FreeHeader))
		{
			if (pPrevHeader)
				pPrevHeader->pNext = pFreeHeader->pNext;
			else
				m_pFreeList = pFreeHeader->pNext;
			pHeader->backPadding = u8(newSize);
		}
		else
		{
			FreeHeader* pNew = (FreeHeader*)(ptr + size);
			pNew->size = newSize;
			pNew->pNext = pFreeHeader->pNext;
			if (pPrevHeader)
				pPrevHeader->pNext = (u8*)pNew;
			else
				m_pFreeList = (u8*)pNew;
			pHeader->backPadding = 0;
		}
		pHeader->padding = padding;
		pHeader->size = size;

		Memory::Clear(ptr, size);

#if HV_STATISTICS
		++m_NumAllocs;
		if (m_PeakNumAllocs < m_NumAllocs)
			m_PeakNumAllocs = m_NumAllocs;
		m_Usage += size;
		if (m_PeakUsage < m_Usage)
			m_PeakUsage = m_Usage;
		m_PaddedUsage += size + pHeader->padding + pHeader->backPadding;
		if (m_PeakPaddedUsage < m_PaddedUsage)
			m_PeakPaddedUsage = m_PaddedUsage;
#endif

		return ptr;
	}

	void* FreeListAllocator::Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);

		HV_ASSERT(pOriginal || size);
		if (!m_pBuffer || !m_pFreeList || size > m_Size)
			return nullptr;

		if (pOriginal == nullptr)
		{
			return Allocate(size, alignment);
		}
		if (size == 0)
		{
			Free(pOriginal);
			return nullptr;
		}

		HV_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");

		void* ptr = Allocate(size, alignment);
		Memory::Copy(ptr, pOriginal, size);
		Free(pOriginal);
		return ptr;
	}

	void FreeListAllocator::Free(void* ptr, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);

		HV_ASSERT_MSG(ptr, "Can't deallocate a nullptr!");
		HV_UNREFERENCED_PARAM(ptr);

		if (!m_pBuffer)
			return;

		Header* pHeader = (Header*)((u8*)ptr - sizeof(Header));
		FreeHeader *prevIt = nullptr, *it = (FreeHeader*)m_pFreeList;
		while (it)
		{
			if (ptr < it)
				break;

			prevIt = it;
			it = (FreeHeader*)it->pNext;
		}

		u8* address = (u8*)ptr - pHeader->padding;
#if HV_STATISTICS
		sizeT size = pHeader->size;
		sizeT paddedSize = size + pHeader->padding + pHeader->backPadding;
#endif
		sizeT totalSize = pHeader->padding + pHeader->size + pHeader->backPadding;
		FreeHeader* pFreeHeader = (FreeHeader*)address;
		Memory::Clear(pFreeHeader, totalSize);
		pFreeHeader->size = totalSize;
		pFreeHeader->pNext = (u8*)it;
		if (prevIt)
			prevIt->pNext = (u8*)pFreeHeader;
		else
			m_pFreeList = (u8*)pFreeHeader;

		Coalesce(prevIt, pFreeHeader);

#if HV_STATISTICS
		--m_NumAllocs;
		m_Usage -= size;
		m_PaddedUsage -= paddedSize;
#endif
	}

	b8 FreeListAllocator::Owns(void* ptr)
	{
		return ptr >= m_pBuffer && ptr < m_pBuffer + m_Size;
	}

	FreeListAllocator::FreeHeader* FreeListAllocator::FindFreeSpace(sizeT size, u16 alignment, u16& padding,
		FreeHeader*& pPrevHeader)
	{
		FreeHeader* pHeader = (FreeHeader*)m_pFreeList;
		while (pHeader)
		{
			padding = CalculatePaddingWithHeader(sizeT(pHeader), alignment, sizeof(Header));
			if (padding + size <= pHeader->size)
				break;

			pPrevHeader = pHeader;
			pHeader = (FreeHeader*)pHeader->pNext;
		}
		return pHeader;
	}

	void FreeListAllocator::Coalesce(FreeHeader* pPrev, FreeHeader* pFree)
	{
		if (pFree->pNext && (u8*)pFree + pFree->size == (u8*)pFree->pNext)
		{
			FreeHeader* pNext = (FreeHeader*)pFree->pNext;
			pFree->size += pNext->size;
			pFree->pNext = pNext->pNext;
		}
		if (pPrev && (u8*)pPrev + pPrev->size == (u8*)pFree)
		{
			pPrev->size += pFree->size;
			pPrev->pNext = pFree->pNext;
		}
	}

	u16 FreeListAllocator::CalculatePaddingWithHeader(sizeT address, u16 alignment, u16 headerSize)
	{
		u16 padding = alignment - (u16(address) & (alignment - 1));
		padding &= (alignment - 1);

		if (padding < headerSize)
		{
			u16 neededSpace = headerSize - padding;
			padding += alignment * (neededSpace / alignment);
			if ((neededSpace & (alignment - 1)) != 0)
				padding += alignment;
		}
		return padding;
	}

}
