// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// BuddyAllocator.cpp: Buddy allocator
#include "Memory/Allocators/BuddyAllocator.h"

namespace Hv::Memory {


	BuddyAllocator::BuddyAllocator()
		: IAllocator()
		, m_MinSize(0)
		, m_MaxSize(0)
		, m_Levels(0)
		, m_Offset(0)
	{
	}

	BuddyAllocator::BuddyAllocator(sizeT maxSize, sizeT minSize, u16 alignment, const AnsiChar* name)
		: IAllocator(name)
		, m_MinSize(minSize)
		, m_MaxSize(maxSize)
		, m_Levels(0)
	{
		HV_ASSERT_MSG(minSize, "minSize needs to be bigger than 0!");
		HV_ASSERT_MSG(maxSize, "maxSize needs to be bigger than 0!");
		HV_ASSERT_MSG(minSize < maxSize, "minSize needs to be smaller than maxSize!");
		HV_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");
		HV_ASSERT_MSG(!(maxSize & (minSize - 1)), "maxSize needs to be a multiple of minSize!");
		HV_ASSERT_MSG(!(minSize & (alignment - 1)), "minSize and maxSize need to be multiples of alignment!");

		m_Offset = sizeof(BuddyHeader) * (((maxSize / minSize) * 2) - 1);
		m_Size = maxSize + m_Offset;
		m_Alignment = alignment;
		m_pBuffer = (u8*)Memory::Allocate(m_Size, alignment, HV_ALLOC_CONTEXT(AllocCategory::Allocator));
		HV_ASSERT(m_pBuffer);
		Memory::Clear(m_pBuffer, m_Size);
		m_OwnsBuffer = true;

		// Setup buddies

		// Layout
		// +------------+ -+
		// |  header 0  |  |
		// | header 1-0 |  +- Buddy headers
		// | header 1-1 |  |
		// | header 2-0 |  |
		// |     ...    |  |
		// +------------+ -+
		// |   Memory   |  +- Buddy memory
		// +------------+ -+

		u16 index = 1;
		u16 parentIndex = 0;
		u16 count = 1;
		BuddyHeader* pBuddy = (BuddyHeader*)m_pBuffer;

		// Initialize basic values
		for (sizeT size = maxSize; size >= minSize; size /= 2, ++m_Levels)
		{
			for (u16 i = 0; i < count; ++i)
			{
				pBuddy->size = size;
				pBuddy->offset = m_Offset + i * size;
				pBuddy->child = index + i * 2;
				pBuddy->buddy = count + (i & 1 ? i - 1 : i + 1) - 1;

				if (m_Levels)
				{
					pBuddy->parent = parentIndex + i / 2;
				}
				++pBuddy;
			}
			count *= 2;
			if (m_Levels > 0)
				parentIndex = (count - 1) / 2;
			index += count;
		}
	}

	BuddyAllocator::BuddyAllocator(u8* pBuffer, sizeT maxSize, sizeT minSize, u16 alignment, b8 transferOwnership,
		const AnsiChar* name)
		: IAllocator(name)
		, m_MinSize(minSize)
		, m_MaxSize(maxSize)
		, m_Levels(0)
		, m_Offset(0)
	{
		HV_ASSERT_MSG(minSize, "minSize needs to be bigger than 0!");
		HV_ASSERT_MSG(maxSize, "maxSize needs to be bigger than 0!");
		HV_ASSERT_MSG(minSize < maxSize, "minSize needs to be smaller than maxSize!");
		HV_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");
		HV_ASSERT_MSG(!(maxSize & (minSize - 1)), "maxSize needs to be a multiple of minSize!");
		HV_ASSERT_MSG(!(minSize & (alignment - 1)), "minSize and maxSize need to be multiples of alignment!");
		HV_ASSERT_MSG(pBuffer, "pBuffer can't be nullptr");

		sizeT alignMask = alignment - 1;
		m_Offset = (sizeof(BuddyHeader) * (((maxSize / minSize) * 2) - 1) + alignMask) & ~alignMask;
		m_Size = maxSize + m_Offset;
		m_Alignment = alignment;
		m_pBuffer = pBuffer;
		Memory::Clear(m_pBuffer, m_Size);
		m_OwnsBuffer = transferOwnership;

		// Setup buddies

		// Layout
		// +------------+ -+
		// |  header 0  |  |
		// | header 1-0 |  +- Buddy headers
		// | header 1-1 |  |
		// |     ...    |  |
		// +------------+ -+
		// |   Memory   |  +- Buddy memory
		// +------------+ -+

		u16 index = 1;
		u16 parentIndex = 0;
		u16 count = 1;
		BuddyHeader* pBuddy = (BuddyHeader*)m_pBuffer;

		// Initialize basic values
		for (sizeT size = maxSize; size >= minSize; size /= 2, ++m_Levels)
		{
			for (u16 i = 0; i < count; ++i)
			{
				pBuddy->size = size;
				pBuddy->offset = m_Offset + i * size;
				pBuddy->child = index + i * 2;
				pBuddy->buddy = count + (i & 1 ? i - 1 : i + 1) - 1;

				if (m_Levels)
				{
					pBuddy->parent = parentIndex + i / 2;
				}
				++pBuddy;
			}
			count *= 2;
			if (m_Levels > 0)
				parentIndex = (count - 1) / 2;
			index += count;
		}
	}

	BuddyAllocator::BuddyAllocator(BuddyAllocator&& alloc) noexcept
		: IAllocator(Forward<BuddyAllocator>(alloc))
		, m_MinSize(alloc.m_MinSize)
		, m_MaxSize(alloc.m_MaxSize)
		, m_Levels(alloc.m_Levels)
		, m_Offset(alloc.m_Offset)
	{
	}

	BuddyAllocator::~BuddyAllocator()
	{
		if (m_OwnsBuffer && m_pBuffer)
			Memory::Free(m_pBuffer, HV_ALLOC_CONTEXT(AllocCategory::Allocator));
	}

	BuddyAllocator& BuddyAllocator::operator=(BuddyAllocator&& alloc) noexcept
	{
		IAllocator::operator=(Forward<BuddyAllocator>(alloc));
		m_MinSize = alloc.m_MinSize;
		m_MaxSize = alloc.m_MaxSize;
		m_Levels = alloc.m_Levels;
		m_Offset = alloc.m_Offset;
		return *this;
	}

	void* BuddyAllocator::Allocate(sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		HV_UNREFERENCED_PARAM(alignment); // Only used for assertions
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");
		HV_ASSERT_MSG(alignment <= m_Alignment, "Alignment needs to be the smaller or equal to the allocators alignment!");

		if (size > m_MaxSize || !m_pBuffer)
			return nullptr;

		u16 level = 0;
		sizeT nextSize = m_MaxSize / 2;
		BuddyHeader* pHeaders = (BuddyHeader*)m_pBuffer;

		// Get level
		while (size <= nextSize && nextSize >= m_MinSize)
		{
			++level;
			nextSize /= 2;
		}

		if (level == 0)
		{
			if (pHeaders->isSplit || pHeaders->used)
				return nullptr;
			pHeaders->used = true;
#if HV_STATISTICS
			pHeaders->allocSize = size;
			++m_NumAllocs;
			if (m_PeakNumAllocs < m_NumAllocs)
				m_PeakNumAllocs = m_NumAllocs;
			m_Usage += size;
			if (m_PeakUsage < m_Usage)
				m_PeakUsage = m_Usage;
			m_PaddedUsage += pHeaders->size;
			if (m_PeakPaddedUsage < m_PaddedUsage)
				m_PeakPaddedUsage = m_PaddedUsage;
#endif
			return m_pBuffer + m_Offset;
		}

		u8* ptr = nullptr;

		// Find the best chunk
		u16 curLevel = 0;
		u16 index = 0;
		while (true)
		{
			BuddyHeader& header = pHeaders[index];
			// If level is correct and chunk is free, return it
			if (curLevel == level && !header.isSplit && !header.used)
			{
				header.used = true;
				ptr = m_pBuffer + header.offset;
				break;
			}

			// If child is used go to the buddy
			if (header.used)
			{
				// we are in the first child, go to buddy
				if (index & 1)
				{
					index = header.buddy;
				}
				// we are in the second child, go to the parent's buddy, or break if parent is root
				else
				{
					if (header.parent == 0)
						break;

					const BuddyHeader& parent = pHeaders[header.parent];
					index = parent.buddy;
				}
				continue;
			}

			if (header.isSplit)
			{
				if (curLevel == level)
				{
					// we are in the first child, go to buddy
					if (index & 1)
					{
						index = header.buddy;
					}
					// we are in the second child, go to the parent's buddy, or break if parent is root
					else
					{
						if (header.parent == 0)
							break;

						const BuddyHeader& parent = pHeaders[header.parent];
						index = parent.buddy;
					}
				}
				else
				{
					index = header.child;
					++curLevel;
				}
				continue;
			}

			header.isSplit = true;
			index = header.child;
			++curLevel;
		}

		if (!ptr)
			return nullptr;

#if HV_STATISTICS
		pHeaders[index].allocSize = size;

		++m_NumAllocs;
		if (m_PeakNumAllocs < m_NumAllocs)
			m_PeakNumAllocs = m_NumAllocs;
		m_Usage += size;
		if (m_PeakUsage < m_Usage)
			m_PeakUsage = m_Usage;
		m_PaddedUsage += pHeaders[index].allocSize;
		if (m_PeakPaddedUsage < m_PaddedUsage)
			m_PeakPaddedUsage = m_PaddedUsage;
#endif

		// Backpropagate and set used
		while (curLevel)
		{
			BuddyHeader& cur = pHeaders[index];
			BuddyHeader& buddy = pHeaders[cur.buddy];
			BuddyHeader& parent = pHeaders[cur.parent];

			if (buddy.used)
				parent.used = true;
			else
				break;

			index = cur.parent;
			--curLevel;
		}

		return ptr;
	}

	void* BuddyAllocator::Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		HV_ASSERT(pOriginal || size);

		if (size > m_MaxSize || !m_pBuffer)
			return nullptr;

		if (!pOriginal)
			return Allocate(size, alignment);

		if (size == 0)
		{
			Free(pOriginal);
			return nullptr;
		}

		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");
		HV_ASSERT_MSG(alignment <= m_Alignment, "Alignment needs to be the smaller or equal to the allocators alignment!");

		if (size >= m_MaxSize)
			return nullptr;

		sizeT offset = sizeT(pOriginal) - sizeT(m_pBuffer);
		BuddyHeader* pHeaders = (BuddyHeader*)m_pBuffer;
		u16 curLevel = 0;
		u16 index = 0;
		// Find header for memory (binary search)
		while (true)
		{
			BuddyHeader& header = pHeaders[index];

			if (header.isSplit)
			{
				index = header.child;
				if (offset == header.offset + header.size / 2)
				{
					index = pHeaders[index].buddy;
				}
				++curLevel;
			}
			else
				break;
		}

		BuddyHeader& origHeader = pHeaders[index];

		// Same size block or in smallest block
		if (size <= origHeader.size && (size > origHeader.size / 2 || origHeader.size == m_MinSize))
			return pOriginal;

		// smaller allocation
		u8* ptr = nullptr;
		sizeT origSize = origHeader.size;
#if HV_STATISTICS
		sizeT origAllocSize = origHeader.allocSize;
#endif
		if (size <= origHeader.size / 2)
		{
			while (true)
			{
				BuddyHeader& header = pHeaders[index];
				if (size > header.size / 2 || header.size == m_MinSize)
				{
					header.used = true;
#if HV_STATISTICS
					header.allocSize = size;
#endif
					Memory::Clear((u8*)pOriginal + header.size, origSize - header.size);
					ptr = (u8*)pOriginal;
					break;
				}
				else
				{
					header.isSplit = true;
					header.used = false;
#if HV_STATISTICS
					header.allocSize = 0;
#endif
					index = header.child;
				}
			}
		}
		else // bigger allocation
		{
			u16 origIndex = index;
			u16 curIndex = index;
			u16 origLevel = curLevel;
			// find appropriate level and pseudo-free the memory
			while (pHeaders[index].size < size)
			{
				BuddyHeader& header = pHeaders[index];
				BuddyHeader& buddy = pHeaders[header.buddy];

				if (!header.isSplit && !buddy.isSplit && !buddy.used)
					curIndex = index;

				index = header.parent;
				--curLevel;
			}

			// Check for free block of the appropriate level
			u16 headerCount = (1 << curLevel);
			u16 headerOffset = headerCount - 1;

			b8 found = false;
			for (u16 i = 0, idx = headerOffset; i < headerCount; ++i, ++idx)
			{
				BuddyHeader& header = pHeaders[idx];
				if (!header.used && !header.isSplit || idx == curIndex)
				{
					header.used = true;
#if HV_STATISTICS
					header.allocSize = size;
#endif
					ptr = m_pBuffer + header.offset;
					Memory::Move(ptr, pOriginal, origSize);
					if (ptr != pOriginal)
						Memory::Clear(pOriginal, origSize);
					found = true;
					curIndex = idx;
					break;
				}
			}

			// Free original memory
			if (found)
			{
				curIndex = index;
				index = origIndex;
				curLevel = origLevel;
				while (curLevel && index != curIndex)
				{
					BuddyHeader& header = pHeaders[index];
					BuddyHeader& buddy = pHeaders[header.buddy];
					BuddyHeader& parent = pHeaders[header.parent];

					header.used = false;
#if HV_STATISTICS
					header.allocSize = 0;
#endif

					if (!header.isSplit && !buddy.isSplit && !buddy.used)
						parent.isSplit = false;

					index = header.parent;
					--curLevel;
				}
			}
			index = curIndex;
		}

#if HV_STATISTICS
		pHeaders[index].allocSize = size;

		++m_NumAllocs;
		if (m_PeakNumAllocs < m_NumAllocs)
			m_PeakNumAllocs = m_NumAllocs;
		m_Usage += i64(origSize) - i64(size);
		if (m_PeakUsage < m_Usage)
			m_PeakUsage = m_Usage;
		m_PaddedUsage += i64(origAllocSize) + i64(pHeaders[index].allocSize);
		if (m_PeakPaddedUsage < m_PaddedUsage)
			m_PeakPaddedUsage = m_PaddedUsage;
#endif

		return ptr;
	}

	void BuddyAllocator::Free(void* ptr, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		HV_ASSERT_MSG(ptr != nullptr, "can't free a nullptr!");

		if (!m_pBuffer)
			return;

		sizeT offset = sizeT(ptr) - sizeT(m_pBuffer);
		BuddyHeader* pHeaders = (BuddyHeader*)m_pBuffer;
		u16 curLevel = 0;
		u16 index = 0;
		// Find header for memory (binary search)
		while (true)
		{
			BuddyHeader& header = pHeaders[index];

			if (header.isSplit)
			{
				index = header.child;
				if (offset >= header.offset + header.size / 2)
				{
					index = pHeaders[index].buddy;
				}
				++curLevel;
			}
			else
				break;
		}

		Memory::Clear(ptr, pHeaders[index].size);

		// Propagate and coalesce
		while (curLevel)
		{
			BuddyHeader& header = pHeaders[index];
			BuddyHeader& buddy = pHeaders[header.buddy];
			BuddyHeader& parent = pHeaders[header.parent];

			header.used = false;
#if HV_STATISTICS
			header.allocSize = 0;
#endif

			if (!header.isSplit && !buddy.isSplit && !buddy.used)
				parent.isSplit = false;

			index = header.parent;
			--curLevel;
		}
		// Update root
		pHeaders->used = false;

#if HV_STATISTICS
		++m_NumAllocs;
		m_Usage -= pHeaders[index].allocSize;
		m_PaddedUsage -= pHeaders[index].size;
		pHeaders[index].allocSize = 0;
#endif

	}

	b8 BuddyAllocator::Owns(void* ptr)
	{
		u8* end = m_pBuffer + m_Size;
		return ptr >= end - m_MaxSize && ptr < end;
	}
}
