// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// StackAllocator.h: Stack allocator
#include "Memory/Allocators/StackAllocator.h"

namespace Hv::Memory {

	StackAllocator::StackAllocator()
		: IAllocator()
		, m_Index(0)
		, m_pPrev(nullptr)
	{
	}

	StackAllocator::StackAllocator(sizeT size, u16 alignment, const AnsiChar* name)
		: IAllocator(name)
		, m_Index(0)
		, m_pPrev(nullptr)
	{
		HV_SLOW_ASSERT_MSG(size, "Size needs to be bigger than 0!");
		HV_SLOW_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_SLOW_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");

		m_Size = size;
		m_Alignment = alignment;
		m_pBuffer = (u8*)Memory::Allocate(size, alignment, HV_ALLOC_CONTEXT(AllocCategory::Allocator));
		Memory::Clear(m_pBuffer, m_Size);
		m_OwnsBuffer = true;
	}

	StackAllocator::StackAllocator(u8* pBuffer, sizeT size, u16 alignment, b8 transferOwnership, const AnsiChar* name)
		: IAllocator(name)
		, m_Index(0)
		, m_pPrev(nullptr)
	{
		HV_SLOW_ASSERT_MSG(size, "Size needs to be bigger than 0!");
		HV_SLOW_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_SLOW_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");
		HV_ASSERT_MSG(pBuffer, "pBuffer can't be nullptr");

		m_Size = size;
		m_Alignment = alignment;
		m_pBuffer = pBuffer;
		m_OwnsBuffer = transferOwnership;
	}

	StackAllocator::StackAllocator(StackAllocator&& alloc) noexcept
		: IAllocator(Forward<StackAllocator>(alloc))
		, m_Index(alloc.m_Index)
		, m_pPrev(nullptr)
	{
	}

	StackAllocator::~StackAllocator()
	{
		if (m_OwnsBuffer && m_pBuffer)
			Memory::Free(m_pBuffer, HV_ALLOC_CONTEXT(AllocCategory::Allocator));
	}

	StackAllocator& StackAllocator::operator=(StackAllocator&& alloc) noexcept
	{
		IAllocator::operator=(Forward<StackAllocator>(alloc));
		m_Index = alloc.m_Index;
		return *this;
	}

	void* StackAllocator::Allocate(sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);

		HV_SLOW_ASSERT_MSG(alignment, "Can't align to 0 bytes!");
		HV_SLOW_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");

		if (!m_pBuffer || size > m_Size - m_Index)
			return nullptr;

		u8* pBase = m_pBuffer + m_Index;
		sizeT alignMask = alignment - 1;
		u8* ptr = (u8*)(sizeT(pBase + sizeof(Header) + alignMask) & ~alignMask);

		if (ptr >= m_pBuffer + m_Size)
			return nullptr;

		Header* pHeader = (Header*)(ptr - sizeof(Header));
		pHeader->pPrev = m_pPrev;
		pHeader->padding = u16(sizeT(pHeader) - sizeT(pBase));

		m_Index = sizeT(ptr) - sizeT(m_pBuffer) + size;

#if HV_STATISTICS
		++m_NumAllocs;
		if (m_PeakNumAllocs < m_NumAllocs)
			m_PeakNumAllocs = m_NumAllocs;
		m_Usage += size;
		if (m_PeakUsage < m_Usage)
			m_PeakUsage = m_Usage;
		m_PaddedUsage += size + pHeader->padding + sizeof(Header);
		if (m_PeakPaddedUsage < m_PaddedUsage)
			m_PeakPaddedUsage = m_PaddedUsage;
#endif

		m_pPrev = ptr;
		return ptr;
	}

	void* StackAllocator::Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		HV_ASSERT(pOriginal || size);

		if (!m_pBuffer || size > m_Size - m_Index)
			return nullptr;

		if (pOriginal == nullptr)
		{
			return Allocate(size, alignment);
		}

		HV_ASSERT_MSG(pOriginal == m_pPrev, "ptr is not pointing to the last allocation!");
		if (size == 0)
		{
			Free(pOriginal);
			return nullptr;
		}

		HV_SLOW_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_SLOW_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");

		Header* pHeader = (Header*)((u8*)pOriginal - sizeof(Header));
		u8* pBase = (u8*)pOriginal - sizeof(Header) - pHeader->padding;
		sizeT oldSize = m_Index - (sizeT(pOriginal) - sizeT(m_pBuffer));

		sizeT alignMask = alignment - 1;
		if (sizeT(pOriginal) & alignMask) // Not aligned
		{
			Header tmpHeader = *pHeader;
			u8* pNewPtr = (u8*)(sizeT(pBase + sizeof(Header) + alignMask) & ~alignMask);
			Memory::Move(pNewPtr, pOriginal, oldSize < size ? oldSize : size);

			pHeader = (Header*)(pNewPtr - sizeof(Header));
			pHeader->padding = u16(sizeT(pNewPtr) - sizeT(pBase));
			pHeader->pPrev = tmpHeader.pPrev;
			m_pPrev = pNewPtr;

#if HV_STATISTICS
			i64 diff = i64(size) - i64(oldSize);
			m_Usage = sizeT(m_Usage + diff);
			if (m_PeakUsage < m_Usage)
				m_PeakUsage = m_Usage;
			m_PaddedUsage = sizeT(m_PaddedUsage + diff + i64(pHeader->padding) - i64(tmpHeader.padding));
			if (m_PeakPaddedUsage < m_PaddedUsage)
				m_PeakPaddedUsage = m_PaddedUsage;
#endif

			return pNewPtr;
		}

		// Aligned
		{
			i64 diff = i64(size) - i64(oldSize);
			Memory::Clear((u8*)pOriginal + oldSize + diff, sizeT(diff < 0 ? -diff : diff));

#if HV_STATISTICS
			m_Usage = sizeT(m_Usage + diff);
			if (m_PeakUsage < m_Usage)
				m_PeakUsage = m_Usage;
			m_PaddedUsage = sizeT(m_PaddedUsage + diff);
			if (m_PeakPaddedUsage < m_PaddedUsage)
				m_PeakPaddedUsage = m_PaddedUsage;
#endif

			return pOriginal;
		}
	}

	void StackAllocator::Free(void* ptr, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);

		HV_ASSERT_MSG(ptr == m_pPrev, "ptr is not pointing to the last allocation!");
		
		Header* pHeader = (Header*)((u8*)ptr - sizeof(Header));
		sizeT size = m_Index - (sizeT(ptr) - sizeT(m_pBuffer));
		sizeT totalSize = size + sizeof(Header) + pHeader->padding;
		u8* pBase = (u8*)pHeader - pHeader->padding;
		m_Index = sizeT(pBase) - sizeT(m_pBuffer);

#if HV_STATISTICS
		--m_NumAllocs;
		m_Usage -= size;
		m_PaddedUsage -= totalSize;
#endif
		m_pPrev = pHeader->pPrev;
		Memory::Clear(pBase, totalSize);
	}

	b8 StackAllocator::Owns(void* ptr)
	{
		return ptr >= m_pBuffer && ptr < m_pBuffer + m_Size;
	}

	void StackAllocator::Reset()
	{
		m_Index = 0;
		m_pPrev = nullptr;
		if (m_pBuffer && m_Size)
			Memory::Clear(m_pBuffer, m_Size);

#if HV_STATISTICS
		m_NumAllocs = 0;
		m_Usage = 0;
		m_PaddedUsage = 0;
#endif
	}
}
