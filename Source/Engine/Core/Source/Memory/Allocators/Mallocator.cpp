// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Mallocator.cpp: Mallocator
#include "Memory/Allocators/Mallocator.h"

namespace Hv::Memory {

	Mallocator::Mallocator(const AnsiChar* name)
		: IAllocator(name)
	{
	}

	Mallocator::Mallocator(Mallocator&& alloc) noexcept
		: IAllocator(Forward<Mallocator>(alloc))
	{
	}

	Mallocator::~Mallocator()
	{
	}

	Mallocator& Mallocator::operator=(Mallocator&& alloc) noexcept
	{
		IAllocator::operator=(Forward<Mallocator>(alloc));
		return *this;
	}

	void* Mallocator::Allocate(sizeT size, u16 alignment, AllocContext context)
	{
		HV_SLOW_ASSERT_MSG(size, "Can't allocate 0 bytes!");
		HV_SLOW_ASSERT_MSG(alignment, "Can't align to 0 bytes!");
		HV_SLOW_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");

#if HV_STATISTICS
		sizeT alignMask = alignment - 1;
		u16 padding = u16((sizeof(Header) + alignMask) & ~alignMask);
		sizeT paddedSize = size + sizeof(Header) + padding;

		u8* ptr = (u8*)Memory::Allocate(paddedSize, alignment, context);
		ptr += padding;
		Header* pHeader = (Header*)(ptr - sizeof(Header));

		padding -= sizeof(Header);
		pHeader->padding = padding;
		pHeader->size = size;
		pHeader->check = m_CheckCode;

		++m_NumAllocs;
		if (m_PeakNumAllocs < m_NumAllocs)
			m_PeakNumAllocs = m_NumAllocs;
		m_Usage += size;
		if (m_PeakUsage < m_Usage)
			m_PeakUsage = m_Usage;
		m_PaddedUsage += size + pHeader->padding + sizeof(Header);
		if (m_PeakPaddedUsage < m_PaddedUsage)
			m_PeakPaddedUsage = m_PaddedUsage;

		return ptr;
#else
		return Memory::Allocate(size, alignment, context);
#endif
	}

	void* Mallocator::Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);

		HV_ASSERT(pOriginal || size);

		if (pOriginal == nullptr)
		{
			return Allocate(size, alignment);
		}
		if (size == 0)
		{
			Free(pOriginal);
			return nullptr;
		}

		HV_SLOW_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_SLOW_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");

		void* ptr = Allocate(size, alignment);
		Memory::Copy(ptr, pOriginal, size);
		Free(pOriginal); 
		return ptr;
	}

	void Mallocator::Free(void* ptr, AllocContext context)
	{
#if HV_STATISTICS
		Header* pHeader = (Header*)((u8*)ptr - sizeof(Header));
		u8* pTmp = (u8*)ptr - sizeof(Header) - pHeader->padding;

		--m_NumAllocs;
		m_Usage += pHeader->size;
		m_PaddedUsage += pHeader->size + pHeader->padding + sizeof(Header);
		if (m_PeakPaddedUsage < m_PaddedUsage)
			m_PeakPaddedUsage = m_PaddedUsage;

		Memory::Free(pTmp, context);
#else
		Memory::Free(ptr, context);
#endif
	}

	b8 Mallocator::Owns(void* ptr)
	{
		if (!ptr)
			return false;
#if HV_STATISTICS
		// If mallocator is used, most likely that the mallocator owns the memory,
		// but check to be certain
		Header* pHeader = (Header*)((u8*)ptr - sizeof(Header));
		return pHeader->check = m_CheckCode;
#else
		// If mallocator is used, most likely that the mallocator owns the memory
		return true;
#endif
	}
}
