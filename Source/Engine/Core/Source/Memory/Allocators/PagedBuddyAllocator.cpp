// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PagedBuddyAllocator.h: Paged buddy allocator
#include "Memory/Allocators/PagedBuddyAllocator.h"

namespace Hv::Memory {

	PagedBuddyAllocator::PagedBuddyAllocator()
		: IPagedAllocator()
		, m_MinSize(0)
	{
	}

	PagedBuddyAllocator::PagedBuddyAllocator(sizeT minSize, sizeT maxSize, u16 alignment, const AnsiChar* name)
		: IPagedAllocator(maxSize, alignment, name)
		, m_MinSize(minSize)
	{
		HV_ASSERT_MSG(minSize, "minSize needs to be bigger than 0!");
		HV_ASSERT_MSG(maxSize, "maxSize needs to be bigger than 0!");
		HV_ASSERT_MSG(minSize < maxSize, "minSize needs to be smaller than maxSize!");
		HV_ASSERT_MSG(alignment, "Alignment needs to be bigger than 0!");
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");
		HV_ASSERT_MSG(!(maxSize & (minSize - 1)), "maxSize needs to be a multiple of minSize!");
		HV_ASSERT_MSG(!(minSize & (alignment - 1)), "minSize and maxSize need to be multiples of alignment!");

		m_pFirst = m_pLast = CreatePage();
	}

	PagedBuddyAllocator::PagedBuddyAllocator(PagedBuddyAllocator&& alloc) noexcept
		: IPagedAllocator(Forward<PagedBuddyAllocator>(alloc))
		, m_MinSize(alloc.m_MinSize)
	{
	}

	PagedBuddyAllocator::~PagedBuddyAllocator()
	{
	}

	PagedBuddyAllocator& PagedBuddyAllocator::operator=(PagedBuddyAllocator&& alloc) noexcept
	{
		IPagedAllocator<BuddyAllocator>::operator=(Forward<PagedBuddyAllocator>(alloc));
		m_MinSize = alloc.m_MinSize;
		return *this;
	}

	void* PagedBuddyAllocator::Allocate(sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		HV_ASSERT_MSG(!(alignment & (alignment - 1)), "Alignment needs to be a power of 2!");
		HV_ASSERT_MSG(alignment <= m_Alignment, "Alignment needs to be the smaller or equal to the allocators alignment!");

		return IPagedAllocator<BuddyAllocator>::Allocate(size, alignment);
	}

	void* PagedBuddyAllocator::Reallocate(void* pOriginal, sizeT size, u16 alignment, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		HV_ASSERT(pOriginal || size);

		return IPagedAllocator<BuddyAllocator>::Reallocate(pOriginal, size, alignment);
	}

	void PagedBuddyAllocator::Free(void* ptr, AllocContext context)
	{
		HV_UNREFERENCED_PARAM(context);
		HV_ASSERT_MSG(ptr != nullptr, "can't free a nullptr!");

		IPagedAllocator<BuddyAllocator>::Free(ptr);
	}

	b8 PagedBuddyAllocator::Owns(void* ptr)
	{
		return IPagedAllocator<BuddyAllocator>::Owns(ptr);
	}

	IPagedAllocator<BuddyAllocator>::Page* PagedBuddyAllocator::CreatePage()
	{
		Page* pPage = (Page*)Memory::Allocate(sizeof(Page), 8, HV_ALLOC_CONTEXT(AllocCategory::Allocator));
		pPage->pNext = nullptr;
		pPage->alloc = BuddyAllocator(m_Size, m_MinSize, m_Alignment, "Buddy Allocator (Page)");
		return pPage;
	}
}
