// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Timer.cpp: Time duration
#include "Time/Timer.h"
#include "HAL/HalTime.h"

namespace Hv {
	
	Timer::Timer(b8 start)
		: m_PauseTicks(0)
		, m_PauseDiff(0)
		, m_Running(start)
		, m_Paused(false)
	{
		if (start)
			m_StartTicks = HAL::Time::GetCurrentTicks();
		else
			m_StartTicks = 0;
	}

	Timer::Timer(const Timer& timer)
		: m_StartTicks(timer.m_StartTicks)
		, m_PauseTicks(timer.m_PauseTicks)
		, m_PauseDiff(timer.m_PauseDiff)
		, m_Running(timer.m_Running)
		, m_Paused(timer.m_PauseDiff)
	{
	}

	Timer::Timer(Timer&& timer) noexcept
		: m_StartTicks(timer.m_StartTicks)
		, m_PauseTicks(timer.m_PauseTicks)
		, m_PauseDiff(timer.m_PauseDiff)
		, m_Running(timer.m_Running)
		, m_Paused(timer.m_PauseDiff)
	{
	}

	Timer::~Timer()
	{
	}

	Timer& Timer::operator=(const Timer& timer)
	{
		m_StartTicks = timer.m_StartTicks;
		m_PauseTicks = timer.m_PauseTicks;
		m_PauseDiff = timer.m_PauseDiff;
		m_Running = timer.m_Running;
		m_Paused = timer.m_PauseDiff;
		return *this;
	}

	Timer& Timer::operator=(Timer&& timer) noexcept
	{
		m_StartTicks = timer.m_StartTicks;
		m_PauseTicks = timer.m_PauseTicks;
		m_PauseDiff = timer.m_PauseDiff;
		m_Running = timer.m_Running;
		m_Paused = timer.m_PauseDiff;
		return *this;
	}

	void Timer::Start()
	{
		if (!m_Running)
		{
			m_StartTicks = HAL::Time::GetCurrentTicks();
			m_Running = true;
		}
		else if (m_Paused)
		{
			u64 curTicks = HAL::Time::GetCurrentTicks();
			m_PauseDiff += curTicks - m_PauseTicks;
			m_Paused = false;
		}
	}

	TimeDuration Timer::Reset()
	{
		u64 curTicks = HAL::Time::GetCurrentTicks();
		m_Paused = false;
		u64 diff = curTicks - m_StartTicks - m_PauseDiff;
		m_StartTicks = curTicks;
		m_PauseDiff = 0;
		return TimeDuration(diff);
	}

	TimeDuration Timer::Stop()
	{
		u64 curTicks = HAL::Time::GetCurrentTicks();
		m_Running = false;
		m_Paused = false;
		u64 diff = curTicks - m_StartTicks - m_PauseDiff;
		m_StartTicks = curTicks;
		m_PauseDiff = 0;
		return TimeDuration(diff);
	}

	TimeDuration Timer::Pause()
	{
		m_Paused = true;
		u64 curTicks = HAL::Time::GetCurrentTicks();
		u64 diff = curTicks - m_StartTicks - m_PauseDiff;
		m_PauseTicks = curTicks;
		return TimeDuration(diff);
	}

	TimeDuration Timer::GetCurrentDuration() const
	{
		u64 curTicks = HAL::Time::GetCurrentTicks();
		return TimeDuration(curTicks - m_StartTicks - m_PauseDiff);
	}

}