// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// SystemConsole.cpp: System console
#include "System/SystemConsole.h"

namespace Hv::System {

	Threading::CriticalSection Console::m_CriticalSection;
	
	HV_INL Console::Console()
	{
		m_Handle = HAL::Console::CreateConsoleHandle();
	}

	Console::~Console()
	{
		// To be certain
		ResetConsoleColor();
		HAL::Console::DestroyConsoleHandle(m_Handle);
	}

	void Console::Write(const String& str)
	{
		if (m_Handle != InvalidConsoleHandle)
		{
			m_CriticalSection.Enter();
			HAL::Console::WriteConsole(m_Handle, str);
			m_CriticalSection.Leave();
		}
	}

	void Console::WriteLine(const String& str)
	{
		if (m_Handle != InvalidConsoleHandle)
		{
			m_CriticalSection.Enter();
			HAL::Console::WriteConsole(m_Handle, str);
			HAL::Console::WriteConsole(m_Handle, "\n");
			m_CriticalSection.Leave();
		}
	}

	String Console::ReadLine()
	{
		if (m_Handle == InvalidConsoleHandle)
			return String();
		String str;
		m_CriticalSection.Enter();
		HAL::Console::ReadConsole(m_Handle, str);
		m_CriticalSection.Leave();
		return str;
	}

	void Console::SetConsoleColor(ConsoleColor color)
	{
		if (m_Handle != InvalidConsoleHandle)
		{
			m_CriticalSection.Enter();
			HAL::Console::SetConsoleColor(m_Handle, u8(color));
			m_CriticalSection.Leave();
		}
	}

	void Console::ResetConsoleColor()
	{
		if (m_Handle != InvalidConsoleHandle)
		{
			m_CriticalSection.Enter();
			HAL::Console::SetConsoleColor(m_Handle, u8(ConsoleColor::ForeLightGrey | ConsoleColor::BackBlack));
			m_CriticalSection.Leave();
		}
	}

	Console& GetConsole()
	{
		static Console console;
		return console;
	}

}