// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// File.cpp: File system file
#include "FileSystem/File.h"

namespace Hv::FileSystem {
	
	File::File()
		: m_Handle(InvalidFileHandle)
		, m_Access(AccessMode::None)
	{
	}

	File::File(FileHandle handle, AccessMode mode, const String& filepath)
		: m_Handle(handle)
		, m_Access(mode)
		, m_FilePath(filepath)
	{
	}

	File::File(FileHandle handle, AccessMode mode, const Path& filepath)
		: m_Handle(handle)
		, m_Access(mode)
		, m_FilePath(filepath)
	{
	}

	File::File(const File& file)
		: m_Handle(file.m_Handle)
		, m_Access(file.m_Access)
		, m_FilePath(file.m_FilePath)
	{
	}

	File::File(File&& file) noexcept
		: m_Handle(file.m_Handle)
		, m_Access(file.m_Access)
		, m_FilePath(Forward<Path>(file.m_FilePath))
	{
		file.m_Handle = InvalidFileHandle;
	}

	File::~File()
	{
	}

	File& File::operator=(const File& file)
	{
		m_Handle = file.m_Handle;
		m_Access = file.m_Access;
		m_FilePath = file.m_FilePath;
		return *this;
	}

	File& File::operator=(File&& file) noexcept
	{
		m_Handle = file.m_Handle;
		m_Access = file.m_Access;
		m_FilePath = Forward<Path>(file.m_FilePath);
		file.m_Handle = InvalidFileHandle;
		return *this;
	}

	AnsiString File::GetLineA()
	{
		if (!HV_IS_ENUM_FLAG_SET(m_Access, AccessMode::Read) || m_Handle == InvalidFileHandle)
			return AnsiString();

		AnsiString str;
		constexpr u8 readSize = 64;
		AnsiChar arr[readSize];
		b8 end = false;
		sizeT bytesRead;
		while (true)
		{
			Read(arr, readSize, &bytesRead);
			str.Reserve(str.Length() + bytesRead);
			u8 i = 0;
			for (;i < bytesRead; ++i)
			{
				AnsiChar c = arr[i];
				if (bytesRead == 0 ||c == '\n' || c == '\0')
				{
					end = true;
					break;
				}
				if (c != '\r')
					str.Append(c);
			}

			if (end)
			{
				if (i != bytesRead)
					MovePosition(-i64(bytesRead - i - 1));
				return str;
			}
		}
	}

	WideString File::GetLineW()
	{
		if (!HV_IS_ENUM_FLAG_SET(m_Access, AccessMode::Read) || m_Handle == InvalidFileHandle)
			return WideString();

		WideString str;
		WideChar arr[16];
		b8 end = false;
		sizeT bytesRead;
		while (true)
		{
			Read(arr, 16 * sizeof(WideChar), &bytesRead);
			str.Reserve(str.Length() + bytesRead);
			u8 i = 0;
			u8 max = u8(bytesRead / 2);
			for (; i < max; ++i)
			{
				WideChar c = arr[i];
				if (bytesRead == 0 || c == '\n' || c == '\0')
				{
					end = true;
					break;
				}
				if (c != '\r')
					str.Append(c);
			}

			if (end)
			{
				if (i != bytesRead)
					MovePosition(-i64((max - i - 1) * sizeof(WideChar)));
				return str;
			}
		}
	}

	b8 File::ReadBytes(u8* pBytes, sizeT size, sizeT* pBytesRead)
	{
		if (!HV_IS_ENUM_FLAG_SET(m_Access, AccessMode::Read) || m_Handle == InvalidFileHandle)
			return false;

		u32 bytesRead;
		b8 res = HAL::FileSystem::ReadFromFile(m_Handle, pBytes, size, bytesRead);
		if (pBytesRead)
			*pBytesRead = bytesRead;
		return res;
	}

	b8 File::WriteLine(const AnsiChar* line)
	{
		if (!HV_IS_ENUM_FLAG_SET(m_Access, AccessMode::Write) || m_Handle == InvalidFileHandle)
			return false;

		b8 res = HAL::FileSystem::WriteToFile(m_Handle, (u8*)line, StringUtils::CStringLength(line) * sizeof(AnsiChar));
		if (!res)
			return false;
		constexpr AnsiChar lineEnd[2] = { '\r', '\n' };
		return HAL::FileSystem::WriteToFile(m_Handle, (u8*)lineEnd, 2 * sizeof(AnsiChar));
	}

	b8 File::WriteLine(const WideChar* line)
	{
		if (!HV_IS_ENUM_FLAG_SET(m_Access, AccessMode::Write) || m_Handle == InvalidFileHandle)
			return false;

		b8 res = HAL::FileSystem::WriteToFile(m_Handle, (u8*)line, StringUtils::CStringLength(line) * sizeof(WideChar));
		if (!res)
			return false;
		constexpr WideChar lineEnd[2] = { '\r', '\n' };
		return HAL::FileSystem::WriteToFile(m_Handle, (u8*)lineEnd, 2 * sizeof(WideChar));
	}

	b8 File::WriteLine(const AnsiString& line)
	{
		if (!HV_IS_ENUM_FLAG_SET(m_Access, AccessMode::Write) || m_Handle == InvalidFileHandle)
			return false;

		b8 res = HAL::FileSystem::WriteToFile(m_Handle, (u8*)line.CStr(), line.Length() * sizeof(AnsiChar));
		if (!res)
			return false;
		constexpr AnsiChar lineEnd[2] = { '\r', '\n' };
		return HAL::FileSystem::WriteToFile(m_Handle, (u8*)lineEnd, 2 * sizeof(AnsiChar));
	}

	b8 File::WriteLine(const WideString& line)
	{
		if (!HV_IS_ENUM_FLAG_SET(m_Access, AccessMode::Write) || m_Handle == InvalidFileHandle)
			return false;

		b8 res = HAL::FileSystem::WriteToFile(m_Handle, (u8*)line.CStr(), line.Length() * sizeof(WideChar));
		if (!res)
			return false;
		constexpr WideChar lineEnd[2] = { '\r', '\n' };
		return HAL::FileSystem::WriteToFile(m_Handle, (u8*)lineEnd, 2 * sizeof(WideChar));
	}

	b8 File::WriteBytes(u8* pBytes, sizeT size)
	{
		if (!HV_IS_ENUM_FLAG_SET(m_Access, AccessMode::Write) || m_Handle == InvalidFileHandle)
			return false;

		return HAL::FileSystem::WriteToFile(m_Handle, pBytes, size);
	}

	b8 File::Close()
	{
		if (m_Handle == InvalidFileHandle)
			return false;
		return HAL::FileSystem::CloseFile(m_Handle);
	}

	b8 File::SetPosition(sizeT pos)
	{
		if (m_Handle == InvalidFileHandle)
			return false;
		return HAL::FileSystem::SetFilePointer(m_Handle, pos);
	}

	sizeT File::GetPosition() const
	{
		if (m_Handle == InvalidFileHandle)
			return sizeT(-1);
		return HAL::FileSystem::GetFilePointer(m_Handle);
	}

	b8 File::MovePosition(i64 offset)
	{
		if (m_Handle == InvalidFileHandle)
			return false;
		return HAL::FileSystem::MoveFilePointer(m_Handle, offset);
	}

	b8 File::MoveToBegin()
	{
		if (m_Handle == InvalidFileHandle)
			return false;
		return HAL::FileSystem::MoveFilePointerToBegin(m_Handle);
	}

	b8 File::MoveToEnd()
	{
		if (m_Handle == InvalidFileHandle)
			return false;
		return HAL::FileSystem::MoveFilePointerToEnd(m_Handle);
	}

	b8 File::EoF()
	{
		if (m_Handle == InvalidFileHandle)
			return false;
		return HAL::FileSystem::EndOfFile(m_Handle);
	}

	HV_FORCE_INL b8 File::IsValid() const
	{
		return m_Handle != InvalidFileHandle;
	}

	HV_FORCE_INL AccessMode File::GetAccessMode() const
	{
		return m_Access;
	}

	HV_FORCE_INL const Path& File::GetFilePath() const
	{
		return m_FilePath;
	}

	sizeT File::GetFileSize() const
	{
		if (m_Handle == InvalidFileHandle)
			return sizeT(-1);
		return HAL::FileSystem::GetFileSize(m_Handle);
	}

	DateTime File::GetCreationTime() const
	{
		DateTime dateTime;
		if (m_Handle == InvalidFileHandle)
			return dateTime;
		HAL::FileSystem::GetFileTime(m_Handle, &dateTime, nullptr, nullptr);
		return dateTime;
	}

	DateTime File::GetLastAccessTime() const
	{
		DateTime dateTime;
		if (m_Handle == InvalidFileHandle)
			return dateTime;
		HAL::FileSystem::GetFileTime(m_Handle, nullptr, &dateTime, nullptr);
		return dateTime;
	}

	DateTime File::GetLastEditTime() const
	{
		DateTime dateTime;
		if (m_Handle == InvalidFileHandle)
			return dateTime;
		HAL::FileSystem::GetFileTime(m_Handle, nullptr, nullptr, &dateTime);
		return dateTime;
	}

	FileProperties File::GetProperties() const
	{
		FileProperties properties;

		properties.valid = m_Handle != InvalidFileHandle;
		if (!properties.valid)
		{
			properties.access = AccessMode::None;
			properties.size = 0;
			properties.filePointer = 0;
			return properties;
		}
		properties.access = m_Access;
		properties.filepath = m_FilePath;
		properties.size = HAL::FileSystem::GetFileSize(m_Handle);
		properties.filePointer = HAL::FileSystem::GetFilePointer(m_Handle);
		HAL::FileSystem::GetFileTime(m_Handle, &properties.creationTime, &properties.lastAccessTime, &properties.lastEditTime);

		return properties;
	}

}