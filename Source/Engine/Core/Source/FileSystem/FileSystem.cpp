// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// FileSystem.cpp: File system
#include "FileSystem/FileSystem.h"

namespace Hv::FileSystem {

	namespace Detail {

		HashMap<String, String>& Detail::GetMountedDirMap()
		{
			static HashMap<String, String> map;
			return map;
		}
	
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	////	Path
	//////////////////////////////////////////////////////////////////////////////////

	Path GetCurrentDirectory()
	{
		static Path dir;
		if (dir.IsEmpty())
			dir = HAL::FileSystem::GetCurrentDirectory();
		return dir;
	}

	 b8 Mount(const String& macro, const Path& path)
	{
		auto& map = Detail::GetMountedDirMap();
		auto pair = map.Insert(macro, path.ToNativeString());
		return pair.second;
	}

	 b8 UnMount(const String& macro)
	{
		auto& map = Detail::GetMountedDirMap();
		auto it = map.Find(macro);
		if (it == map.Back())
			return false;
		map.Erase(it);
		return true;
	}

	 Path GetMountedDirectory(const String& macro)
	{
		auto& map = Detail::GetMountedDirMap();
		auto it = map.Find(macro);
		if (it == map.Back())
			return Path();
		return Path(it->second);
	}

	 Path InsertMountedDirectory(const Path& path)
	{
		if (!path.IsMounted())
			return path;
		Path tmp = path.ToNativeString();
		sizeT idx = tmp.ToString().Find(HAL::FileSystem::g_NativeSeparator);
		String macro = tmp.ToString().SubString(1, idx - 1);
		if (idx == String::NPos)
			tmp = Path();
		else
			tmp = tmp.ToString().SubString(idx + 1);
		Path mountedDir = GetMountedDirectory(macro);
		if (mountedDir.IsEmpty())
			return tmp;
		if (tmp.IsEmpty())
			return mountedDir;
		return mountedDir / tmp;
	}

	 Path AbsolutePath(const Path& path, const Path& base)
	{
		b8 hasRootName = path.HasRootName();
		b8 hasRootDir = path.HasRootDirectory();
		if (hasRootName && hasRootDir)	// path.IsAbsolute
			return path;
		if (hasRootName && !hasRootDir)
			return path.RootName() / base.RootDirectory() / base.RelativePath() / path.RelativePath();
		if (!hasRootName && hasRootDir)
			return base.RootName() / path.RootDirectory() / base.RelativePath() / path.RelativePath();
		return base / path;
	}

	 Path SystemAbsolutePath(const Path& path)
	{
		return AbsolutePath(path);
	}

	 Path ResolvePath(const Path& path)
	{
		if (path.IsEmpty())
			return path;
		Path tmp = path.ToNativeString();
		while (tmp.IsMounted())
		{
			tmp = InsertMountedDirectory(tmp);
		}
		tmp = AbsolutePath(tmp);

		//resolve occurances of ..
		String str = tmp.ToString();
		const char tofind[4] = { HAL::FileSystem::g_NativeSeparator,'.','.','\0' };
		sizeT idx = str.Find(tofind);
		while (idx != String::NPos)
		{
			sizeT i = str.RFind(HAL::FileSystem::g_NativeSeparator, idx);
			if (i == String::NPos)
			{
				str.Erase(idx, 3);
			}
			else
			{
				str.Erase(i, idx - i + 3);
			}
		}
		return Path(str);
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Directories
	////////////////////////////////////////////////////////////////////////////////

	 b8 CreateDirectory(const Path& path)
	{
		if (path.IsEmpty())
			return false;
		Path tmp = ResolvePath(path);
		return HAL::FileSystem::CreateDirectory(tmp.ToString());
	}

	 b8 CreateDirectory(const Path& path, const Path& templatePath)
	{
		if (path.IsEmpty())
			return false;
		Path tmp = ResolvePath(path);
		Path tmplt = ResolvePath(templatePath);
		return HAL::FileSystem::CreateDirectory(tmp.ToString(), tmplt.ToString());
	}

	 b8 CreateDirectories(const Path& path)
	{
		b8 res;
		Path orig = path.ToNativeString();
		if (orig.IsEmpty())
			return false;
		DynArray<String> da = orig.RelativePath().ToString().SplitChar(Path::DirSeparators);
		Path tmp = orig.RootPath() + da[0];
		res = HAL::FileSystem::CreateDirectory(tmp.ToString());
		for (sizeT i = 1, size = da.Size(); i < size; ++i)
		{
			tmp /= da[i];
			res &= HAL::FileSystem::CreateDirectory(tmp.ToString());
		}
		return res;
	}

	 b8 RemoveDirectory(const Path& path)
	{
		Path tmp = ResolvePath(path);
		return HAL::FileSystem::RemoveDirectory(tmp.ToString());
	}

	b8 IsDirectory(const Path& path)
	{
		return DoesDirectoryExist(path);
	}

	 b8 DoesDirectoryExist(const Path& path)
	{
		if (path.IsEmpty())
			return false;
		Path tmp = ResolvePath(path);
		return HAL::FileSystem::DoesDirectoryExist(path.ToString());
	}

	 b8 MoveDirectory(const Path& from, const Path& to)
	{
		if (from.IsEmpty())
			return false;
		if (to.IsEmpty())
			return false;
		Path tmpFrom = ResolvePath(from);
		Path tmpTo = ResolvePath(to);

		return HAL::FileSystem::MoveDirectory(tmpFrom.ToString(), tmpTo.ToString());
	}

	 b8 CopyDirectory(const Path& from, const Path& to, CopySettings settings)
	{
		if (from.IsEmpty())
			return false;
		if (to.IsEmpty())
			return false;
		Path tmpFrom = ResolvePath(from);
		Path tmpTo = ResolvePath(to);

		b8 res = CreateDirectory(tmpTo, tmpFrom);
		if (!res)
			return false;
		DirectoryIterator it = GetFirstDirectoryEntry(tmpFrom);
		for (DirectoryIterator end; it != end; ++it)
		{
			DirectoryEntry& entry = *it;
			if (!entry.IsValid())
				continue;
			switch (entry.GetType())
			{
			default:;
			case EntryType::None:
				break;
			case EntryType::File:
			{
				Path filepath = entry.GetPath();
				Path filename = filepath.Filename();
				Path newPath = tmpTo / filename;
				res &= CopyFile(filepath, newPath, settings);
				break;
			}
			case EntryType::Directory:
			{
				Path filepath = entry.GetPath();
				Path filename = filepath.Filename();
				Path newPath = tmpTo / filename;
				if (HV_IS_ENUM_FLAG_SET(settings, CopySettings::Recursive))
					res &= CopyDirectory(filepath, newPath, settings);
				else
					res &= CreateDirectory(newPath, filepath);
				break;
			}
			}
		}
		return res;
	}

	 b8 RenameDirectory(const Path& path, const String name)
	{
		if (path.IsEmpty())
			return false;
		Path origFile = ResolvePath(path);
		Path newFile = origFile.ParentPath() / Path(name);

		return HAL::FileSystem::MoveDirectory(origFile.ToString(), newFile.ToString());
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Files
	////////////////////////////////////////////////////////////////////////////////

	 File CreateFile(const Path& path, FileMode mode, AccessMode access, ShareMode share)
	{
		if (path.IsEmpty())
			return File();
		Path tmp = ResolvePath(path);
		File file = File(HAL::FileSystem::CreateFile(tmp.ToString(), mode, access, share), access, tmp.ToString());

		if (mode == FileMode::Append && file.IsValid())
			file.MoveToEnd();

		return file;
	}

	 b8 DeleteFile(const Path& path)
	{
		if (path.IsEmpty())
			return false;
		Path tmp = ResolvePath(path);
		return HAL::FileSystem::DeleteFile(tmp.ToString());
	}

	 b8 IsFile(const Path& path)
	{
		return DoesFileExist(path);
	}

	 b8 DoesFileExist(const Path& path)
	{
		if (path.IsEmpty())
			return false;
		Path tmp = ResolvePath(path);
		return HAL::FileSystem::DoesFileExist(tmp.ToString());
	}

	 b8 MoveFile(const Path& from, const Path& to)
	{
		if (from.IsEmpty())
			return false;
		if (to.IsEmpty())
			return false;
		Path tmpFrom = ResolvePath(from);
		Path tmpTo = ResolvePath(to);
		if (!tmpTo.HasFilename())
			tmpTo /= from.Filename();

		return HAL::FileSystem::MoveFile(tmpFrom.ToString(), tmpTo.ToString());
	}

	 b8 CopyFile(const Path& from, const Path& to, CopySettings settings)
	{
		if (from.IsEmpty())
			return false;
		if (to.IsEmpty())
			return false;
		Path tmpFrom = ResolvePath(from);
		Path tmpTo = ResolvePath(to);

		return HAL::FileSystem::CopyFile(tmpFrom.ToString(), tmpTo.ToString(), settings);
	}

	 b8 RenameFile(const Path& path, const String name)
	{
		if (path.IsEmpty())
			return false;
		Path origFile = ResolvePath(path);
		Path filename = name;
		if (!filename.HasFileExtension())
			filename.ReplaceExtension(origFile.Extension());
		Path newFile = origFile.ParentPath() / filename;

		return HAL::FileSystem::MoveFile(origFile.ToString(), newFile.ToString());
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Iteration
	////////////////////////////////////////////////////////////////////////////////
	 DirectoryIterator GetFirstDirectoryEntry(const Path& path)
	{
		if (path.IsEmpty())
			return DirectoryIterator();
		Path tmp = ResolvePath(path);
		String filepath;
		EntryType type = EntryType::None;
		HAL::FileSystem::FileSearchHandle handle = HAL::FileSystem::GetFirstDirectoryEntry(tmp.ToString(), filepath, type);
		if (handle == InvalidFileSearchHandle)
			return DirectoryIterator();
		DirectoryEntry entry(Path(filepath), type);
		return DirectoryIterator(entry, handle);
	}

	DynArray<DirectoryEntry> FindPatern(const Path& baseFolder, const String& patern)
	{
		DynArray<DirectoryEntry> entries;

		DirectoryIterator it = GetFirstDirectoryEntry(baseFolder);

		for (;it != DirectoryIterator(); ++it)
		{
			if (Regex::Match(it->GetPath().Filename().ToString(), patern))
				entries.Push(*it);


			if (it->IsDirectory())
			{
				DynArray<DirectoryEntry> temp = FindPatern(it->GetPath(), patern);
				for (DirectoryEntry entry : temp)
				{
					entries.Push(entry);
				}
			}
		}


		return entries;
	}

}
