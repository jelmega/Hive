// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DirectoryIterator.h: File system directory iterator
#include "FileSystem/DirectoryIterator.h"

namespace Hv::FileSystem {
	
	DirectoryIterator::DirectoryIterator()
		: m_Handle(InvalidFileSearchHandle)
	{
	}

	DirectoryIterator::DirectoryIterator(const DirectoryEntry& entry, const FileSearchHandle& handle)
		: m_Entry(entry)
		, m_Handle(handle)
	{
	}

	DirectoryIterator::DirectoryIterator(const DirectoryIterator& it)
		: m_Entry(it.m_Entry)
	{
		m_Handle = HAL::FileSystem::FindEntry(m_Entry.m_Path.ToString());
	}

	DirectoryIterator::DirectoryIterator(DirectoryIterator&& it) noexcept
		: m_Entry(Forward<DirectoryEntry>(it.m_Entry))
		, m_Handle(it.m_Handle)
	{
	}

	DirectoryIterator::~DirectoryIterator()
	{
		if (m_Handle != InvalidFileSearchHandle)
			HAL::FileSystem::CloseSearchHandle(m_Handle);
	}

	DirectoryEntry* DirectoryIterator::operator->()
	{
		return &m_Entry;
	}

	const DirectoryEntry* DirectoryIterator::operator->() const
	{
		return &m_Entry;
	}

	DirectoryEntry& DirectoryIterator::operator*()
	{
		return m_Entry;
	}

	const DirectoryEntry& DirectoryIterator::operator*() const
	{
		return m_Entry;
	}

	DirectoryIterator& DirectoryIterator::operator++()
	{
		String path;
		HAL::FileSystem::GetNextEntry(m_Handle, m_Entry.m_Path.ParentPath().ToString(), path, m_Entry.m_Type);
		m_Entry.m_Path = path;
		return *this;
	}

	DirectoryIterator DirectoryIterator::operator++(int)
	{
		DirectoryIterator it(*this);
		++*this;
		return it;
	}

	DirectoryIterator& DirectoryIterator::operator=(const DirectoryIterator& it)
	{
		m_Entry = it.m_Entry;
		m_Handle = HAL::FileSystem::FindEntry(m_Entry.m_Path.ToString());
		return *this;
	}

	DirectoryIterator& DirectoryIterator::operator=(DirectoryIterator&& it) noexcept
	{
		m_Entry = Forward<DirectoryEntry>(it.m_Entry);
		m_Handle = it.m_Handle;
		return *this;
	}

	b8 DirectoryIterator::operator==(const DirectoryIterator& it) const
	{
		return m_Entry == it.m_Entry;
	}

	b8 DirectoryIterator::operator!=(const DirectoryIterator& it) const
	{
		return m_Entry != it.m_Entry;
	}

}