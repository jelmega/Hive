// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DirectoryEntry.cpp: File system directory entry
#include "FileSystem/DirectoryEntry.h"
#include "FileSystem/FilePath.h"

namespace Hv::FileSystem {

	DirectoryEntry::DirectoryEntry()
		: m_Type(EntryType::None)
	{
	}

	DirectoryEntry::DirectoryEntry(const Path& path, EntryType type)
		: m_Path(path)
		, m_Type(type)
	{
	}

	DirectoryEntry::DirectoryEntry(const DirectoryEntry& entry)
		: m_Path(entry.m_Path)
		, m_Type(entry.m_Type)
	{
	}

	DirectoryEntry::DirectoryEntry(DirectoryEntry&& entry) noexcept
		: m_Path(Forward<Path>(entry.m_Path))
		, m_Type(entry.m_Type)
	{
	}

	DirectoryEntry& DirectoryEntry::operator=(const DirectoryEntry& entry)
	{
		m_Path = entry.m_Path;
		m_Type = entry.m_Type;
		return *this;
	}

	DirectoryEntry& DirectoryEntry::operator=(DirectoryEntry&& entry) noexcept
	{
		m_Path = Forward<Path>(entry.m_Path);
		m_Type = entry.m_Type;
		return *this;
	}

	b8 DirectoryEntry::operator==(const DirectoryEntry& entry) const
	{
		return m_Type == entry.m_Type && m_Path == entry.m_Path;
	}

	b8 DirectoryEntry::operator!=(const DirectoryEntry& entry) const
	{
		return m_Type != entry.m_Type || m_Path != entry.m_Path;
	}

	b8 DirectoryEntry::ReplaceEntryName(const Path& name)
	{
		switch (m_Type)
		{
		default:
		case EntryType::None:
			return false;
		case EntryType::File:
		{
			Path entryName = name;
			if (!entryName.HasFileExtension())
				entryName /= m_Path.Extension();
			return HAL::FileSystem::MoveFile(m_Path.ToString(), (m_Path.ParentPath() / entryName).ToString());
		}
		case EntryType::Directory:
			return HAL::FileSystem::MoveDirectory(m_Path.ToString(), (m_Path.ParentPath() / name).ToString());
		}
	}

	Path DirectoryEntry::GetPath() const
	{
		return m_Path;
	}

	b8 DirectoryEntry::IsValid() const
	{
		return m_Type != EntryType::None;
	}

	EntryType DirectoryEntry::GetType() const
	{
		return m_Type;
	}

	b8 DirectoryEntry::IsFile() const
	{
		return m_Type == EntryType::File;
	}

	b8 DirectoryEntry::IsDirectory() const
	{
		return m_Type == EntryType::Directory;
	}

	File DirectoryEntry::GetFile(AccessMode access, ShareMode share) const
	{
		if (!(m_Type == EntryType::File))
			return File();
		return File(HAL::FileSystem::CreateFile(m_Path.ToString(), FileMode::Open, access, share), access, m_Path);
	}
	
}