// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// BitBuffer.cpp: Bit buffer
#pragma once
#include "Misc/BitBuffer.h"

namespace Hv {

	BitBuffer::BitBuffer()
		: m_BytePos(0)
		, m_BitPos(0)
		, m_LSB(false)
	{
	}

	BitBuffer::BitBuffer(const DynArray<u8>& buffer, b8 readLSBFirst)
		: m_Buffer(buffer)
		, m_BytePos(0)
		, m_BitPos(0)
		, m_LSB(readLSBFirst)
	{
	}

	BitBuffer::~BitBuffer()
	{
	}

	void BitBuffer::SkipToByteBoundry()
	{
		if (m_BitPos != 0)
		{
			m_BitPos = 0;
			++m_BytePos;
		}
	}

	void BitBuffer::Skip(u8 bits)
	{
		m_BytePos += (m_BitPos + bits) / 8;
		m_BitPos = (m_BitPos + bits) & 0x07;
	}

	u8 BitBuffer::GetBit()
	{
		if (m_BitPos == 8)
		{
			m_BitPos = 0;
			m_BytePos++;
		}
		u8 bit;
		bit = (m_Buffer[m_BytePos] >> (m_LSB ? m_BitPos : (7 - m_BitPos))) & 0x01;
		++m_BitPos;
		return bit;
	}

	u64 BitBuffer::GetValueLSB(u8 bits)
	{
		u64 val = 0;
		for (u64 i = 0; i < bits; ++i)
		{
			u64 bit = GetBit();
			bit <<= i;
			val |= bit;
		}
		return val;
	}

	u64 BitBuffer::GetValueMSB(u8 bits)
	{
		u32 val = 0;
		for (u32 i = 0; i < bits; ++i)
		{
			u32 bit = GetBit();
			val <<= 1;
			val |= bit;
		}
		return val;
	}

	void BitBuffer::Reset()
	{
		m_BytePos = m_BitPos = 0;
	}

	void BitBuffer::Reset(const DynArray<u8>& pBuffer)
	{
		m_BytePos = m_BitPos = 0;
		m_Buffer = pBuffer;
	}

	void BitBuffer::CopyBytesFromBoundry(u8* pDest, u32 numBytes)
	{
		SkipToByteBoundry();
		Memory::Copy(pDest, m_Buffer.Data() + m_BytePos, numBytes);
		m_BytePos += numBytes;
	}
}
