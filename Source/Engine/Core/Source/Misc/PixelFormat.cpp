// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PixelFormat.cpp: pixel format
#include "Misc/PixelFormat.h"
#include "Core/Assertions.h"

namespace Hv {

	b8 PixelFormat::HasDepthComponent() const
	{
		switch (components)
		{
		case PixelFormatComponents::D24:
		case PixelFormatComponents::D24S8:
		case PixelFormatComponents::D32:
		case PixelFormatComponents::D32S8:
			return true;
		default:
			return false;
		}
	}

	b8 PixelFormat::HasStencilComponent() const
	{
		switch (components)
		{
		case PixelFormatComponents::D24S8:
		case PixelFormatComponents::D32S8:
			return true;
		default:
			return false;
		}
	}

	u8 PixelFormat::GetSize() const
	{
		HV_ASSERT(u8(components) < u8(PixelFormatComponents::Count));
		return Detail::g_FormatSizes[u8(components)];
	}

	String PixelFormat::ToString()
	{
		HV_ASSERT(u8(components) < u8(PixelFormatComponents::Count));
		HV_ASSERT(u8(transform) < u8(PixelFormatTransform::Count));
		String name = Detail::g_FormatCompNames[u8(components)];
		name += "_";
		name += Detail::g_FormatTransNames[u8(transform)];
		return name;
	}
}
