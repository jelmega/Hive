// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// CRC.cpp: CRC (Cyclical Redundance Code) Checksum
#pragma once
#include "Checksum/CRC.h"
#include "Containers/Array.h"

namespace Hv::Checksum {

	namespace Detail {

		Array<u32, 256>& GetCrc32Table()
		{
			static Array<u32, 256> table;
			static b8 calculated = false;

			if (!calculated)
			{
				for (u32 i = 0; i < 256; ++i)
				{
					u32 c = i;
					for (u8 j = 0; j < 8; ++j)
					{
						if (c & 1)
							c = 0xEDB88320L ^ (c >> 1);
						else
							c = c >> 1;
					}
					table[i] = c;
				}

				calculated = true;
			}
			return table;
		}
	}


	u32 UpdateCRC32(const u8* pData, u64 size, u32 crc)
	{
		Array<u32, 256>& table = Detail::GetCrc32Table();
		u32 c = crc;

		for (u64 i = 0; i < size; ++i)
		{
			u32 index = (c ^ pData[i]) & 0xFF;
			c = table[index] ^ (c >> 8);
		}

		return c;
	}

	u32 CRC32(const u8* pData, u64 size)
	{
		return UpdateCRC32(pData, size, 0xFFFF'FFFF) ^ 0xFFFF'FFFF;
	}

	u32 CRC32(const DynArray<u8>& data)
	{
		return UpdateCRC32(data.Data(), data.Size(), 0xFFFF'FFFF) ^ 0xFFFF'FFFF;
	}
}
