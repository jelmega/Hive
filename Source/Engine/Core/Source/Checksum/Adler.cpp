// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Adler.cpp: Adler Checksum
#pragma once
#include "Checksum/Adler.h"

namespace Hv::Checksum {

	u32 Adler32(u8* pData, u64 size, u32 adler)
	{
		u32 s1 = adler & 0xFFFF;
		u32 s2 = (adler >> 16) & 0xFFFF;
		constexpr u32 base = 65521; // Longest prime under 65536
		for (u32 i = 0; i < size; ++i)
		{
			s1 = (s1 + pData[i]) % base;
			s2 = (s2 + s1) % base;
		}
		return (s2 << 16) + s1;
	}
}
