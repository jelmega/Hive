// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// WindowsFileSystem.cpp: Windows specific file system implementation
#include "HAL/HalFileSystem.h"
#include "Time/DateTime.h"

#if HV_PLATFORM_WINDOWS

namespace Hv::HAL::FileSystem {
	
	sizeT PrefixEnd(const String& path)
	{
		if (path.Length() > 2 && (path[0] >= 'C' && path[0] <= 'Z') && path[1] == ':')
			return 2;
		return sizeT(-1);
	}

	String GetCurrentDirectory()
	{
		DWORD len = ::GetCurrentDirectoryW(0, nullptr);
		String path;
		path.Resize(len - 1, '\0'); // resize with '\0' to reserve space and set length
		DWORD success = ::GetCurrentDirectoryW(len, path.CStr());
		if (!success)
		{
			DWORD error = ::GetLastError();
			HV_ASSERT_MSG(false, Hv::FormatF(AnsiString("GetCurrentDirectory encountered and error (code: %u)"), error).CStr());
			HV_UNREFERENCED_PARAM(error);
		}
		return path;
	}

	b8 CreateDirectory(const String& path)
	{
		return ::CreateDirectoryW(path.CStr(), nullptr);
	}

	b8 CreateDirectory(const String& path, const String& templatePath)
	{
		b8 res = ::CreateDirectoryExW(templatePath.CStr(), path.CStr(), nullptr);
		if (res)
			return true;
		// Couldn't create directory, but it could already exist, which causes the function to succeed
		sizeT error = ::GetLastError();
		return error == ERROR_ALREADY_EXISTS;
	}

	b8 RemoveDirectory(const String& path)
	{
		return ::RemoveDirectoryW(path.CStr());
	}

	b8 DoesDirectoryExist(const String& path)
	{
		DWORD attrib = ::GetFileAttributesW(path.CStr());
		return attrib != INVALID_FILE_ATTRIBUTES && (attrib & FILE_ATTRIBUTE_DIRECTORY);
	}

	b8 MoveDirectory(const String& from, const String& to)
	{
		return ::MoveFileW(from.CStr(), to.CStr());
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Files
	////////////////////////////////////////////////////////////////////////////////

	FileHandle CreateFile(const String& path, FileMode mode, AccessMode access, ShareMode share)
	{
		if (!HV_IS_ENUM_FLAG_SET(access, AccessMode::Write) && (mode == FileMode::Truncate || mode == FileMode::Append))
		{
			FileHandle handle;
			handle = Hv::FileSystem::InvalidFileHandle;
			return handle;
		}

		// Convert flags to corresponding system flags
		DWORD accessFlags = (HV_IS_ENUM_FLAG_SET(access, AccessMode::Read) ? GENERIC_READ : 0) |
			(HV_IS_ENUM_FLAG_SET(access, AccessMode::Write) ? GENERIC_WRITE : 0);

		DWORD shareFlags = 0;
		if (HV_IS_ENUM_FLAG_SET(access, AccessMode::Read) && HV_IS_ENUM_FLAG_SET(share, ShareMode::Read))
			shareFlags |= FILE_SHARE_READ;
		if (HV_IS_ENUM_FLAG_SET(access, AccessMode::Write) && HV_IS_ENUM_FLAG_SET(share, ShareMode::Write))
			shareFlags |= FILE_SHARE_WRITE;

		DWORD creationFlags = 0;
		switch (mode)
		{
		default:
		case FileMode::None: break;
		case FileMode::Create:
			creationFlags = CREATE_ALWAYS;
			break;
		case FileMode::CreateNew:
			creationFlags = CREATE_NEW;
			break;
		case FileMode::Open:
		case FileMode::Append:
			creationFlags = OPEN_EXISTING;
			break;
		case FileMode::OpenOrCreate:
			creationFlags = OPEN_ALWAYS;
			break;
		case FileMode::Truncate:
			creationFlags = TRUNCATE_EXISTING;
			break;
		}

		// Create file
		FileHandle handle;
		handle = (FileHandle)::CreateFileW(path.CStr(), accessFlags, shareFlags, nullptr, creationFlags, FILE_ATTRIBUTE_NORMAL, nullptr);

		// Check if resulting handle is valid
		sizeT error = ::GetLastError();
		switch (mode)
		{
		default:
		case FileMode::None:
			handle = Hv::FileSystem::InvalidFileHandle;
			break;
		case FileMode::Create:
			if (error != 0 && error != ERROR_ALREADY_EXISTS)
				handle = Hv::FileSystem::InvalidFileHandle;
			break;
		case FileMode::CreateNew:
			if (error == ERROR_FILE_EXISTS)
				handle = Hv::FileSystem::InvalidFileHandle;
			break;
		case FileMode::Open:
		case FileMode::Append:
			if (error == ERROR_FILE_NOT_FOUND)
				handle = Hv::FileSystem::InvalidFileHandle;
			break;
		case FileMode::OpenOrCreate:
			if (error != 0 && error != ERROR_ALREADY_EXISTS)
				handle = Hv::FileSystem::InvalidFileHandle;
			break;
		case FileMode::Truncate:
			if (error == ERROR_FILE_NOT_FOUND)
				handle = Hv::FileSystem::InvalidFileHandle;
			break;
		}
		return handle;
	}

	b8 CloseFile(FileHandle& handle)
	{
		b8 res = ::CloseHandle(handle);
		if (res)
			handle = Hv::FileSystem::InvalidFileHandle;
		return res;
	}

	b8 DeleteFile(const String& path)
	{
		return ::DeleteFileW(path.CStr());
	}

	b8 ReadFromFile(FileHandle& handle, u8* pBytes, sizeT size, u32& bytesRead)
	{
		b8 res = ::ReadFile((HANDLE)handle, pBytes, DWORD(size), (DWORD*)&bytesRead, nullptr);
		if (!res)
			return false;
		return bytesRead == size;
	}

	b8 WriteToFile(FileHandle handle, const u8* pBytes, sizeT size)
	{
		DWORD bytesWritten;
		b8 res = ::WriteFile((HANDLE)handle, pBytes, DWORD(size), &bytesWritten, nullptr);
		if (!res)
			return false;
		if (bytesWritten != size)
		{
			MoveFilePointer(handle, -i64(bytesWritten));
			::SetEndOfFile((HANDLE)handle);
			return false;
		}
		return true;
	}

	sizeT GetFilePointer(const FileHandle& handle)
	{
		LARGE_INTEGER move;
		move.QuadPart = 0;
		LARGE_INTEGER pos;

		bool res = ::SetFilePointerEx((HANDLE)handle, move, &pos, FILE_CURRENT);
		if (!res)
			return sizeT(-1);
		return sizeT(pos.QuadPart);
	}

	b8 SetFilePointer(const FileHandle& handle, sizeT position)
	{
		LARGE_INTEGER pos;
		pos.QuadPart = position;
		return ::SetFilePointerEx((HANDLE)handle, pos, nullptr, FILE_BEGIN);
	}

	b8 MoveFilePointer(const FileHandle& handle, i64 offset)
	{
		LARGE_INTEGER move;
		move.QuadPart = offset;
		return ::SetFilePointerEx((HANDLE)handle, move, nullptr, FILE_CURRENT);
	}

	b8 MoveFilePointerToBegin(const FileHandle& handle)
	{
		LARGE_INTEGER move;
		move.QuadPart = 0;
		return ::SetFilePointerEx((HANDLE)handle, move, nullptr, FILE_BEGIN);
	}

	b8 MoveFilePointerToEnd(const FileHandle& handle)
	{
		LARGE_INTEGER move;
		move.QuadPart = 0;
		return ::SetFilePointerEx((HANDLE)handle, move, nullptr, FILE_END);
	}

	b8 EndOfFile(const FileHandle& handle)
	{
		DWORD bytesRead;
		u8 data;
		b8 res = ::ReadFile((HANDLE)handle, &data, sizeof(u8), &bytesRead, nullptr);
		if (!res)
			return false;
		if (bytesRead == 0)
			return true;
		MoveFilePointer(handle, -i64(bytesRead));
		return false;
	}

	sizeT GetFileSize(const FileHandle& handle)
	{
		LARGE_INTEGER size;
		b8 res = ::GetFileSizeEx((HANDLE)handle, &size);
		if (!res)
			return sizeT(-1);
		return sizeT(size.QuadPart);
	}

	b8 GetFileTime(const FileHandle& handle, DateTime* pCreation, DateTime* pLastAccess, DateTime* pLastEdit)
	{
		FILETIME fileCreation, fileLastAccess, fileLastEdit;
		b8 res = ::GetFileTime((HANDLE)handle, &fileCreation, &fileLastAccess, &fileLastEdit);
		if (!res)
			return false;
		res = false;
		if (pCreation)
		{
			SYSTEMTIME sysTime;
			::FileTimeToSystemTime(&fileCreation, &sysTime);
			*pCreation = DateTime(Windows::Time::SystemTimeToDateTimeValues(sysTime));
			res = true;
		}
		if (pLastAccess)
		{
			SYSTEMTIME sysTime;
			::FileTimeToSystemTime(&fileLastAccess, &sysTime);
			*pLastAccess = DateTime(Windows::Time::SystemTimeToDateTimeValues(sysTime));
			res = true;
		}
		if (pLastEdit)
		{
			SYSTEMTIME sysTime;
			::FileTimeToSystemTime(&fileLastEdit, &sysTime);
			*pLastEdit = DateTime(Windows::Time::SystemTimeToDateTimeValues(sysTime));
			res = true;
		}
		return res;
	}

	b8 DoesFileExist(const String& path)
	{
		DWORD attrib = ::GetFileAttributesW(path.CStr());
		return attrib != INVALID_FILE_ATTRIBUTES && !(attrib & FILE_ATTRIBUTE_DIRECTORY);
	}

	b8 MoveFile(const String& from, const String& to)
	{
		return ::MoveFileW(from.CStr(), to.CStr());
	}

	b8 CopyFile(const String& from, const String& to, CopySettings settings)
	{
		switch (settings & CopySettings::FileMask)
		{
		default:
		case CopySettings::None:
			return false;
		case CopySettings::SkipExisting:
		{
			b8 res = ::CopyFileW(from.CStr(), to.CStr(), true);
			if (!res)
			{
				sizeT error = ::GetLastError();
				return error == ERROR_FILE_EXISTS;
			}
			return true;
		}
		case CopySettings::OverwriteExisting:
			return ::CopyFileW(from.CStr(), to.CStr(), false);
		case CopySettings::OverwriteIfNewer:
		{
			// Get file to possibly overwrite
			HANDLE toFile = ::CreateFileW(to.CStr(), GENERIC_READ, 0, nullptr, OPEN_EXISTING, 0, nullptr);
			if (toFile == INVALID_HANDLE_VALUE)
			{
				// File does not exist -> normal copy
				return ::CopyFileW(from.CStr(), to.CStr(), false);
			}
			else
			{
				//File exists -> check if file is newer, if so, overwrite file
				HANDLE fromFile = ::CreateFileW(from.CStr(), GENERIC_READ, 0, nullptr, OPEN_EXISTING, 0, nullptr);
				if (fromFile == INVALID_HANDLE_VALUE)
				{
					::CloseHandle(toFile);
					return false;
				}
				FILETIME fromTime, toTime;
				b8 res;
				res = ::GetFileTime(fromFile, nullptr, nullptr, &fromTime);
				res &= b8(::GetFileTime(toFile, nullptr, nullptr, &toTime));
				res &= b8(::CloseHandle(fromFile));
				res &= b8(::CloseHandle(toFile));
				LARGE_INTEGER fromLi, toLi;
				fromLi.HighPart = fromTime.dwHighDateTime;
				fromLi.LowPart = fromTime.dwLowDateTime;
				toLi.HighPart = toTime.dwHighDateTime;
				toLi.LowPart = toTime.dwLowDateTime;
				if (toLi.QuadPart < fromLi.QuadPart)
				{
					return ::CopyFileW(from.CStr(), to.CStr(), false);
				}
				return true;
			}
		}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	//	Iterations
	////////////////////////////////////////////////////////////////////////////////

	FileSearchHandle FindEntry(const String& path)
	{
		FileSearchHandle handle;
		HANDLE tmp = ::FindFirstFileW(path.CStr(), nullptr);
		handle = tmp == INVALID_HANDLE_VALUE ? Hv::FileSystem::InvalidFileSearchHandle : (FileSearchHandle)tmp;
		return handle;
	}

	FileSearchHandle GetFirstDirectoryEntry(const String& path, String& filepath, EntryType& type)
	{
		String tmp = path;
		if (!tmp.EndsWith(g_NativeSeparator))
			tmp.Append(g_NativeSeparator);
		tmp.Append('*');

		FileSearchHandle handle;
		WIN32_FIND_DATAW findData;
		HANDLE tmpHandle = ::FindFirstFileW(tmp.CStr(), &findData);
		handle = tmpHandle == INVALID_HANDLE_VALUE ? Hv::FileSystem::InvalidFileSearchHandle : (FileSearchHandle)tmpHandle;
		if (handle == Hv::FileSystem::InvalidFileSearchHandle)
		{
			handle = Hv::FileSystem::InvalidFileSearchHandle;
			filepath.Clear();
			type = EntryType::None;
			return handle;
		}

		// Skip '.' and '..' directories
		b8 res = true;
		while (res && (StringUtils::Compare(findData.cFileName, L".") == 0 || StringUtils::Compare(findData.cFileName, L"..") == 0))
		{
			res &= b8(::FindNextFileW((HANDLE)handle, &findData));
		}
		if (!res)
		{
			handle = Hv::FileSystem::InvalidFileSearchHandle;
			filepath.Clear();
			type = EntryType::None;
			return handle;
		}

		filepath = tmp.Replace(tmp.Length() - 1, findData.cFileName);
		if (findData.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY)
			type = EntryType::Directory;
		else
			type = EntryType::File;
		return handle;
	}

	b8 GetNextEntry(FileSearchHandle& handle, const String& dir, String& filepath, EntryType& type)
	{
		WIN32_FIND_DATAW findData;
		b8 res = ::FindNextFileW((HANDLE)handle, &findData);
		if (!res)
		{
			CloseSearchHandle(handle);
			filepath.Clear();
			type = EntryType::None;
			return false;
		}
		filepath = dir;
		if (!filepath.EndsWith(g_NativeSeparator))
			filepath.Append(g_NativeSeparator);
		filepath.Append(findData.cFileName);
		if (findData.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY)
			type = EntryType::Directory;
		else
			type = EntryType::File;
		return true;
	}

	b8 CompareSearchHandles(const FileSearchHandle& h0, const FileSearchHandle& h1)
	{
		return h0 == h1;
	}

	b8 CloseSearchHandle(FileSearchHandle& handle)
	{
		b8 res = ::FindClose((HANDLE)handle);
		handle = Hv::FileSystem::InvalidFileSearchHandle;
		return res;
	}

}

#endif