// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HalDynLib.h: HAL dynamic library handling
#include "HAL/HalDynLib.h"

namespace Hv::HAL::DynLib {
	
	DynLibHandle Load(const String& path)
	{
		return (DynLibHandle)::LoadLibraryW(path.CStr());
	}

	void Unload(DynLibHandle& handle)
	{
		::FreeLibrary((HMODULE)handle);
	}

	DynLibProcHandle GetProc(DynLibHandle& handle, const AnsiString& procName)
	{
		return (DynLibProcHandle)::GetProcAddress((HMODULE)handle, procName.CStr());
	}

}
