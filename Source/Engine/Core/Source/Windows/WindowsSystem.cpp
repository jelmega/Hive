// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// WindowsSystem.cpp: Windows system
#include "HAL/HalSystem.h"

#if HV_PLATFORM_WINDOWS

namespace Hv::HAL::System {
	
	b8 IsDebuggerAttached()
	{
		return ::IsDebuggerPresent();
	}

	void LogDebugger(const AnsiChar* text)
	{
		::OutputDebugStringA(text);
	}

	void LogDebugger(const WideChar* text)
	{
		::OutputDebugStringW(text);
	}

	b8 TickSystem()
	{
		MSG msg;
		while (::PeekMessageW(&msg, nullptr, 0, 0, PM_REMOVE) > 0)
		{
			::TranslateMessage(&msg);
			::DispatchMessageW(&msg);
		}
		return true;
	}

}

#endif