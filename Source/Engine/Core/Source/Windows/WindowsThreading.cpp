// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// WindowsThreading.cpp: Windows specific threading implementation
#include "HAL/HalThreading.h"
#include "Threading/Thread.h"
#include "Memory/MemoryUtil.h"

#if HV_PLATFORM_WINDOWS

namespace Hv::HAL::Threading {

	DWORD WINAPI WinThreadProc(LPVOID lpParameter)
	{
		Delegate<b8()>& delegate = *(Delegate<b8()>*)lpParameter;

		b8 res = delegate();
		return DWORD(res);
	}

	void Sleep(u32 ms)
	{
		::Sleep(ms);
	}

	u32 GetLogicalCoreCount()
	{
		SYSTEM_INFO sysInfo;
		::GetSystemInfo(&sysInfo);
		return sysInfo.dwNumberOfProcessors;
	}

	ThreadHandle CreateThread(u32& id, Delegate<b8()>& delegate, const WideChar* debugName)
	{
		DWORD tmp;
		HANDLE handle = ::CreateThread(nullptr, 0, &WinThreadProc, &delegate, 0, &tmp);
		if (handle == INVALID_HANDLE_VALUE)
			return Hv::Threading::InvalidThreadHandle;
		::SetThreadDescription(handle, debugName);

		id = tmp;

		return (ThreadHandle)handle;
	}

	b8 DestroyThread(ThreadHandle& thread)
	{
		return ::CloseHandle((HANDLE)thread);
	}

	b8 TerminateThread(ThreadHandle& thread)
	{
		DWORD exitCode;
		b8 res = ::GetExitCodeThread((HANDLE)thread, &exitCode);
		if (!res)
			return false;
		return ::TerminateThread((HANDLE)thread, exitCode);
	}

	void WaitForThread(ThreadHandle& thread)
	{
		::WaitForSingleObject((HANDLE)thread, INFINITE);
	}

	u32 GetCurrentThreadId()
	{
		return ::GetCurrentThreadId();
	}

	MutexHandle CreateMutex(const WideChar* name)
	{
		HANDLE handle = ::CreateMutexW(nullptr, false, name);
		if (handle == INVALID_HANDLE_VALUE)
			return Hv::Threading::InvalidMutexHandle;
		return (MutexHandle)handle;
	}

	b8 DestroyMutex(MutexHandle& mutex)
	{
		return ::CloseHandle((HANDLE)mutex);
	}

	void LockMutex(MutexHandle& mutex, u32 ms)
	{
		::WaitForSingleObject((HANDLE)mutex, ms);
	}

	b8 TryLockMutex(MutexHandle& mutex, u32 timeout)
	{
		DWORD check = ::WaitForSingleObject((HANDLE)mutex, timeout);
		return check != WAIT_TIMEOUT;
	}

	void UnlockMutex(MutexHandle& mutex)
	{
		::ReleaseMutex((HANDLE)mutex);
	}

	CriticalSectionHandle CreateCriticalSection()
	{
		// Memory::Allocate to prevent circular calls
		CRITICAL_SECTION* pSection = (CRITICAL_SECTION*)Memory::Allocate(sizeof(CRITICAL_SECTION));
		::InitializeCriticalSection(pSection);
		return (CriticalSectionHandle)pSection;
	}

	CriticalSectionHandle CreateCriticalSectionWithSpinCount(u32 spinCount)
	{
		// Memory::Allocate to prevent circular calls
		CRITICAL_SECTION* pSection = (CRITICAL_SECTION*)Memory::Allocate(sizeof(CRITICAL_SECTION));
		::InitializeCriticalSectionAndSpinCount(pSection, spinCount);
		return (CriticalSectionHandle)pSection;
	}

	b8 DestroyCriticalSection(CriticalSectionHandle& criticalSection)
	{
		::DeleteCriticalSection((CRITICAL_SECTION*)criticalSection);
		// Memory::Free to prevent circular calls
		Memory::Free(criticalSection);
		return true;
	}

	void EnterCriticalSection(CriticalSectionHandle& criticalSection)
	{
		::EnterCriticalSection((CRITICAL_SECTION*)criticalSection);
	}

	b8 TryEnterCriticalSection(CriticalSectionHandle& criticalSection)
	{
		return ::TryEnterCriticalSection((CRITICAL_SECTION*)criticalSection);
	}

	void LeaveCriticalSection(CriticalSectionHandle& criticalSection)
	{
		::LeaveCriticalSection((CRITICAL_SECTION*)criticalSection);
	}

}

#endif