// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// SystemConsole.h: Windows console implementation
#include "Windows/WindowsConsole.h"

#if HV_PLATFORM_WINDOWS

namespace Hv::HAL::Console {

	Hv::System::ConsoleHandle CreateConsoleHandle()
	{
		WindowsConsoleHandle* handle = (WindowsConsoleHandle*)g_Alloctor.Allocate(sizeof(WindowsConsoleHandle), 8, HV_ALLOC_CONTEXT(Hv::Memory::AllocCategory::HAL));
		handle->input = GetStdHandle(STD_INPUT_HANDLE);
		handle->output = GetStdHandle(STD_OUTPUT_HANDLE);
		return (Hv::System::ConsoleHandle)handle;
	}

	b8 DestroyConsoleHandle(Hv::System::ConsoleHandle& handle)
	{
		g_Alloctor.Free((WindowsConsoleHandle*)handle, HV_ALLOC_CONTEXT(Hv::Memory::AllocCategory::HAL));
		return true;
	}

	b8 WriteConsole(Hv::System::ConsoleHandle& handle, String text)
	{
		if (handle == Hv::System::InvalidConsoleHandle)
			return false;

		WindowsConsoleHandle* winHandle = (WindowsConsoleHandle*)handle;
		if (winHandle->output == INVALID_HANDLE_VALUE)
			return false;

		DWORD charsWritten;
		b8 res = ::WriteConsoleW(winHandle->output, text.CStr(), DWORD(text.Length()), &charsWritten, nullptr);
		return res && charsWritten == text.Length();
	}

	b8 ReadConsole(Hv::System::ConsoleHandle& handle, String& text)
	{
		if (handle == Hv::System::InvalidConsoleHandle)
			return false;
		
		WindowsConsoleHandle* winHandle = (WindowsConsoleHandle*)handle;
		if (winHandle->output == INVALID_HANDLE_VALUE)
			return false;

		text.Resize(g_MaxNumConsoleReadChars, '\0');
		DWORD charsRead;
		CONSOLE_READCONSOLE_CONTROL readconsoleControl;
		readconsoleControl.nLength = sizeof(CONSOLE_READCONSOLE_CONTROL);
		readconsoleControl.nInitialChars = 0;
		readconsoleControl.dwCtrlWakeupMask = '\n';
		readconsoleControl.dwControlKeyState = 0;

		b8 res = ::ReadConsoleW(winHandle->input, text.CStr(), DWORD(text.Length()), &charsRead, &readconsoleControl);
		if (charsRead <= 2)
		{
			text.Clear();
			return res && charsRead == 2;
		}
		text.Resize(charsRead - 2); // - 2 <- remove /r/n
		return res;
	}

	b8 SetConsoleColor(Hv::System::ConsoleHandle& handle, u8 color)
	{
		WindowsConsoleHandle* pConHandle = (WindowsConsoleHandle*)handle;
		return ::SetConsoleTextAttribute(pConHandle->output, color);
	}

}

#endif