// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// HalTime.h: HAL time
#pragma once
#include "Core/CoreHeaders.h"
#include "HAL/HalTime.h"

#if HV_PLATFORM_WINDOWS

namespace Hv::HAL::Time {
	
	u64 GetTickFrequency()
	{
		static LARGE_INTEGER li = {};
		if (li.QuadPart == 0)
			::QueryPerformanceFrequency(&li); // Will always succeed on windows XP and above
		return u64(li.QuadPart);
	}

	u64 GetCurrentTicks()
	{
		LARGE_INTEGER li;
		::QueryPerformanceCounter(&li);
		return u64(li.QuadPart);
	}

	DateTimeValues GetCurrentDataTime()
	{
		SYSTEMTIME sysTime;
		::GetLocalTime(&sysTime);
		DateTimeValues settings;
		settings.year = u16(sysTime.wYear);
		settings.month = u8(sysTime.wMonth);
		settings.dayOfWeek = u8(sysTime.wDayOfWeek);
		settings.day = u8(sysTime.wDay);
		settings.hour = u8(sysTime.wHour);
		settings.minute = u8(sysTime.wMinute);
		settings.second = u8(sysTime.wSecond);
		settings.millisecond = u16(sysTime.wMilliseconds);
		return settings;
	}

}

#endif