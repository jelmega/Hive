// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// rendererCorePCH.h: Renderer Core Precompiled Header
#pragma once

#include <Core.h>
#include <System.h>
#include HV_INCLUDE_RTTI_MODULE(RendererCore)

HV_DECLARE_LOG_CATEGORY_EXTERN(RenderCore);