// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// RHIContext.h: RHI Context
#pragma once
#include "RendererCorePCH.h"
#include "Renderer/Core/Queue.h"
#include "Renderer/Core/CommandListManager.h"
#include "Renderer/Core/DescriptorSet.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv::RHI {
	struct RHIDesc;

	/**
	 * RHI Context, contains data for the creation and use of RHI Objects
	 */
	class HIVE_API RHIContext
	{
	public:
		RHIContext();
		virtual ~RHIContext();

		/**
		 * Initialize the RHI context
		 * @return	True if the context was initialized successfully initialized, false otherwise
		 */
		virtual b8 Init(const RHIDesc& desc) = 0;
		/**
		 * Destroy the RHI context
		 * @return	True if the context was destroyed successfully initialized, false otherwise
		 */
		virtual b8 Destroy() = 0;

		/**
		 * Get a queue from a certain type
		 * @param[in] type	Queue type
		 * @return			Best fitting queue for its type
		 */
		Renderer::Core::Queue* GetQueue(Renderer::QueueType type);
		/**
		 * Get the command list manager
		 * @return	Command list manager
		 */
		Renderer::Core::CommandListManager* GetCommandListManager() { return m_pCommandListManager; }
		/**
		 * Get the descriptor set manager
		 * @return	Descriptor set manager
		 */
		Renderer::Core::DescriptorSetManager* GetDescriptorSetManager() { return m_pDescriptorSetManager; }

	protected:
		Renderer::Core::CommandListManager* m_pCommandListManager;		/**< Command list manager */
		Renderer::Core::DescriptorSetManager* m_pDescriptorSetManager;	/**< Descriptor set manager */
		DynArray<Renderer::Core::Queue*> m_Queues;						/**< Device queues */

	};

}

#pragma warning(pop)
