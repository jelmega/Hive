// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DynamicRHI.h: Dynamic RHI (implemented by API specific RHIs)
#pragma once
#include "RendererCorePCH.h"
#include "Renderer/RendererCoreCommon.h"
#include "RHI/RHIContext.h"
#include "Renderer/Core/Buffer.h"
#include "Renderer/Core/SwapChain.h"
#include "Renderer/Core/Shader.h"
#include "Renderer/Core/Pipeline.h"
#include "Renderer/Core/RenderPass.h"
#include "Renderer/Core/Framebuffer.h"
#include "Renderer/Core/CommandListManager.h"
#include "Renderer/Core/DescriptorSetManager.h"
#include "Renderer/Core/Sampler.h"
#include "Renderer/Core/Texture.h"

namespace Hv {namespace Renderer {namespace Core {
	struct RenderTargetDesc;
}}}

namespace Hv::RHI {

	/**
	 * RHI description
	 */
	struct RHIDesc
	{
		Renderer::RHIValidationLevel validationLevel;
		DynArray<Renderer::QueueInfo> queueInfo;
	};
	
	/**
	 * Dynamic RHI interface
	 */
	class HIVE_API IDynamicRHI
	{
	public:
		/**
		 * Create the dynamic RHI interface
		 */
		IDynamicRHI();
		virtual ~IDynamicRHI();

		/**
		 * Initialize the dynamic RHI
		 * @return	True if the dynamic RHI was initialized successfully, false otherwise
		 */
		virtual b8 Init(const RHIDesc& desc) = 0;
		/**
		 * Destroy the dynamic RHI
		 * @return	True if the dynamic RHI was shut down successfully, false otherwise
		 */
		virtual b8 Destroy() = 0;

		////////////////////////////////////////////////////////////////////////////////
		// Renderview																  //
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Create a renderview
		 * @param[in] pWindow	Window to create a renderview for
		 * @param[in] vsync		V-sync mode
		 * @return				Pointer to the renderview, nullptr if the creation failed
		 */
		virtual Renderer::Core::SwapChain* CreateRenderView(Window* pWindow, Renderer::VSyncMode vsync) = 0;
		/**
		 * Destroy a renderview
		 * @param[in] pRenderView	Renderview to destroy
		 * @return					True if the renderview was destroyed successfully, false otherwise
		 */
		virtual b8 DestroyRenderView(Renderer::Core::SwapChain* pRenderView) = 0;

		////////////////////////////////////////////////////////////////////////////////
		// Shaders																	  //
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Create a shader
		 * @param[in] desc	Shader description
		 * @return			Pointer to the shader, nullptr if the creation failed
		 */
		virtual Renderer::Core::Shader* CreateShader(const Renderer::Core::ShaderDesc& desc) = 0;
		/**
		 * Destroy a shader
		 * @param[in] pShader	Shader to destroy
		 * @return				True if the shader was destroyed successfully, false otherwise
		 */
		virtual b8 DestroyShader(Renderer::Core::Shader* pShader) = 0;

		////////////////////////////////////////////////////////////////////////////////
		// Render pass																  //
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Create a render pass
		 * @param[in] attachments	Render pass attachments
		 * @param[in] subpasses		Sub render passes
		 * @return					Pointer to the render pass, nullptr if the creation failed
		 */
		virtual Renderer::Core::RenderPass* CreateRenderPass(const DynArray<Renderer::Core::RenderPassAttachment>& attachments, const DynArray<Renderer::Core::SubRenderPass>& subpasses) = 0;
		/**
		 * Destroy a render pass
		 * @param[in] pRenderPass	Render pass to destroy
		 * @return					True if the render pass was destroyed successfully, false otherwise
		 */
		virtual b8 DestroyRenderPass(Renderer::Core::RenderPass* pRenderPass) = 0;

		////////////////////////////////////////////////////////////////////////////////
		// Pipelines																  //
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Create a graphics pipeline
		 * @param[in] desc	Graphics pipeline desc
		 * @return			Pointer to the pipeline, nullptr if the creation failed
		 */
		virtual Renderer::Core::Pipeline* CreatePipeline(const Renderer::Core::GraphicsPipelineDesc& desc) = 0;
		/**
		 * Create a copute pipeline
		 * @param[in] desc	Compute pipeline desc
		 * @return			Pointer to the pipeline, nullptr if the creation failed
		 */
		virtual Renderer::Core::Pipeline* CreatePipeline(const Renderer::Core::ComputePipelineDesc& desc) = 0;
		/**
		 * Destroy a copute pipeline
		 * @param[in] pPipeline	Pipeline to destroy
		 * @return				True if the pipeline was destroyed successfully, false otherwise
		 */
		virtual b8 DestroyPipeline(Renderer::Core::Pipeline* pPipeline) = 0;

		////////////////////////////////////////////////////////////////////////////////
		// Framebuffers																  //
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Create a framebuffer
		 * @param[in] renderTargets		Render targets
		 * @param[in] pRenderPass		Associated render pass
		 * @return						Pointer to the framebuffer, nullptr if the creation failed
		 */
		virtual Renderer::Core::Framebuffer* CreateFramebuffer(const DynArray<Renderer::Core::RenderTarget*>& renderTargets, Renderer::Core::RenderPass* pRenderPass) = 0;
		/**
		 * Destroy a framebuffer
		 * @param[in] pFramebuffer	Framebuffer to destroy
		 * @return					True if the framebuffer was destroyed successfully, false otherwise
		 */
		virtual b8 DestroyFramebuffer(Renderer::Core::Framebuffer* pFramebuffer) = 0;

		////////////////////////////////////////////////////////////////////////////////
		// Command lists															  //
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Get the command list manager
		 * @return	Pointer to the command list manager, nullptr if the creation failed
		 */
		Renderer::Core::CommandListManager* GetCommandListManager() { return m_pContext->GetCommandListManager(); }

		////////////////////////////////////////////////////////////////////////////////
		// Samplers																	  //
		////////////////////////////////////////////////////////////////////////////////
		/**
		* Create a sampler
		* @param[in] desc	Sampler description
		* @return			Pointer to the sampler, nullptr if the creation failed
		*/
		virtual Renderer::Core::Sampler* CreateSampler(const Renderer::Core::SamplerDesc& desc) = 0;
		/**
		* Destroy a sampler
		* @param[in] pSampler	Sampler to destroy
		* @return				True if the sampler was destroyed successfully, false otherwise
		*/
		virtual b8 DestroySampler(Renderer::Core::Sampler* pSampler) = 0;

		////////////////////////////////////////////////////////////////////////////////
		// Textures																	  //
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Create a texture
		 * @param[in] desc			Texture description
		 * @param[in] pCommandList	Command list used for layout transition, if nullptr, default command list will be used
		 * @return					Pointer to the texture, nullptr if the creation failed
		 */
		virtual Renderer::Core::Texture* CreateTexture(const Renderer::Core::TextureDesc& desc, Renderer::Core::CommandList* pCommandList = nullptr) = 0;
		/**
		 * Destroy a texture
		 * @param[in] pTexture	Texture to destroy
		 * @return				True if the texture was destroyed successfully, false otherwise
		 */
		virtual b8 DestroyTexture(Renderer::Core::Texture* pTexture) = 0;

		////////////////////////////////////////////////////////////////////////////////
		// Render targets															  //
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Create a render target
		 * @param[in] desc	Render target description
		 * @return			Pointer to the render target, nullptr if the creation failed
		 */
		virtual Renderer::Core::RenderTarget* CreateRenderTarget(const Renderer::Core::RenderTargetDesc& desc) = 0;
		/**
		 * Destroy a render target
		 * @param[in] pRenderTarget		Render target to destroy
		 * @return						True if the render target was destroyed successfully, false otherwise
		 */
		virtual b8 DestroyRenderTarget(Renderer::Core::RenderTarget* pRenderTarget) = 0;

		////////////////////////////////////////////////////////////////////////////////
		// Buffers																	  //
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Create a buffer
		 * @param[in] type			Buffer type
		 * @param[in] size			Buffer size
		 * @param[in] flags			Buffer flags
		 * @return					Pointer to the buffer, nullptr if the creation failed
		 */
		virtual Renderer::Core::Buffer* CreateBuffer(Renderer::BufferType type, u64 size, Renderer::BufferFlags flags) = 0;
		/**
		 * Create a buffer
		 * @param[in] vertexCount	Number of vertices
		 * @param[in] vertexSize	Size of a vertex
		 * @param[in] flags			Buffer flags
		 * @return					Pointer to the buffer, nullptr if the creation failed
		 */
		virtual Renderer::Core::Buffer* CreateVertexBuffer(u32 vertexCount, u16 vertexSize, Renderer::BufferFlags flags) = 0;
		/**
		 * Create an index buffer
		 * @param[in] indexCount	Number of indices
		 * @param[in] indexType		Index type
		 * @param[in] flags			Buffer flags
		 * @return					Pointer to the buffer, nullptr if the creation failed
		 */
		virtual Renderer::Core::Buffer* CreateIndexBuffer(u32 indexCount, Renderer::IndexType indexType, Renderer::BufferFlags flags) = 0;

		/**
		 * Destroy a GPU buffer
		 * @param[in] pBuffer	Buffer to destroy
		 * @return				True if the buffer was destroyed successfully, false otherwise
		 */
		virtual b8 DestroyBuffer(Renderer::Core::Buffer* pBuffer) = 0;

		////////////////////////////////////////////////////////////////////////////////
		// Desriptor sets															  //
		////////////////////////////////////////////////////////////////////////////////
		/**
		* Get the command list manager
		* @return	Pointer to the command list manager, nullptr if the creation failed
		*/
		Renderer::Core::DescriptorSetManager* GetDescriptorSetManager() { return m_pContext->GetDescriptorSetManager(); }


		////////////////////////////////////////////////////////////////////////////////
		// Other																	  //
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Wait for the dynamic RHI to be idle
		 * @return	True if the dynamic RHI is idle
		 */
		virtual b8 WaitIdle() = 0;

		/**
		 * Get the RHI context
		 * @return RHI context
		 */
		RHIContext* GetContext() { return m_pContext; };

	protected:

		RHIDesc m_Desc;			/**< RHI desc */
		RHIContext* m_pContext;	/**< RHI context */

	};

	// DynLib Interface
	using LoadDynamicRHIFunc = IDynamicRHI * (*)();

	// Expected function in RHI:
	//extern "C"
	//{
	//	/**
	//	 * Generate the application for the engine to use
	//	 * @return	Generated dynamic RHI
	//	 */
	//	HIVE_API IDynamicRHI* LoadDynamicRHI();
	//}
	//
	// From here on, no other functions need to be loaded from the dynamic library

}
