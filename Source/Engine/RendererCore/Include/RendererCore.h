// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// RendererCore.h: Rednerer core includes files
#pragma once
#include "RendererCorePCH.h"

#include "Renderer/RendererCoreCommon.h"

#include "RHI/IDynamicRHI.h"
#include "RHI/RHIContext.h"

#include "Renderer/Core/RendererCoreHelpers.h"

#include "Renderer/Core/InputDescriptor.h"

#include "Renderer/Core/Queue.h"

#include "Renderer/Core/SwapChain.h"

#include "Renderer/Core/Buffer.h"
#include "Renderer/Core/Shader.h"
#include "Renderer/Core/RenderPass.h"
#include "Renderer/Core/Pipeline.h"
#include "Renderer/Core/Framebuffer.h"

#include "Renderer/Core/Texture.h"
#include "Renderer/Core/RenderTarget.h"
#include "Renderer/Core/Sampler.h"

#include "Renderer/Core/CommandListManager.h"
#include "Renderer/Core/CommandList.h"

#include "Renderer/Core/DescriptorSet.h"
#include "Renderer/Core/DescriptorSetManager.h"

#include "Renderer/Core/Semaphore.h"
#include "Renderer/Core/Fence.h"
