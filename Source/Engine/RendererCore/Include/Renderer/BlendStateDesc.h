// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// BlendStateDesc.h: Blend state descriptor
#pragma once
#include "RendererCoreCommon.h"

namespace Hv::Renderer {
	
	struct BlendAttachment
	{
		ColorComponentMask components;
		b8 enable;
		BlendFactor srcColorBlendFactor;
		BlendFactor dstColorBlendFactor;
		BlendOp colorBlendOp;
		BlendFactor srcAlphaBlendFactor;
		BlendFactor dstAlphaBlendFactor;
		BlendOp alphaBlendOp;
	};

	struct BlendStateDesc
	{
		b8 enableLogicOp;
		LogicOp logicOp;
		DynArray<BlendAttachment> attachments;
		// TODO: Add constant color
	};

}
