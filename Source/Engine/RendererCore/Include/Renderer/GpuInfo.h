// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// GpuInfo.h: Gpu Info
#pragma once

namespace Hv::Renderer {

	/**
	* Gpu driver version
	*/
	struct DriverVersion
	{
		u16 major;
		u16 minor;
	};
	/**
	* GPU vendor
	*/
	enum class GpuVendor : u8
	{
		Unknown,
		Nvidia,
		AMD,
		Intel,
		ARM,
		Qualcomm,
		ImgTec
	};

	enum class GpuType : u8
	{
		Unknown,
		Discrete,
		Integrated,
		CPU,
		Software,
		Count
	};

	struct GpuInfo
	{
		DriverVersion driver;		/**< Driver version */
		GpuVendor vendor;			/**< GPU vendor */
		GpuType type;				/**< GPU type */
		AnsiChar name[256] = {};	/**< GPU name */
	};


}
