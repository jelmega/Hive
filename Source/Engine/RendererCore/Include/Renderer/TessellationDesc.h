// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// BlendStateDesc.h: Blend state descriptor
#pragma once
#include "RendererCoreCommon.h"

namespace Hv::Renderer {
	namespace Core {
		class Shader;
	}

	struct TessellationDesc
	{
		b8 enabled;
		Core::Shader* pHullShader;
		Core::Shader* pDomainShader;
		u32 controlPoints;
	};

}
