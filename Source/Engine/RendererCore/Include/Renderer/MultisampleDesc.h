// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// MultisampleDesc.h: Multisample descriptor
#pragma once
#include "RendererCorePCH.h"
#include "Renderer/RendererCoreCommon.h"

namespace Hv::Renderer {

	// TODO check for additional options, like vulkan has
	struct MultisampleDesc
	{
		b8 enable;				/**< Enable multisampling */
		SampleCount samples;	/**< Multisample count */
	};

}
