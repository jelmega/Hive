// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Semaphore.h: Semaphore
#pragma once
#include "RendererCorePCH.h"

namespace Hv::RHI {
	class RHIContext;
}

namespace Hv::Renderer::Core {
	
	/**
	 * Semaphore (GPU-GPU fence)
	 */
	class HIVE_API Semaphore
	{
	public:
		Semaphore();
		virtual ~Semaphore();

		/**
		 * Create the semaphore
		 * @param[in] pContext	RHI context
		 * @return				True if the semaphore was created successfully, false otherwise
		 */
		virtual b8 Create(RHI::RHIContext* pContext) = 0;
		/**
		 * Destroy the semaphore
		 * @return	True if the semaphore was destroyed successfully, false otherwise
		 */
		virtual b8 Destroy() = 0;

	protected:
		RHI::RHIContext* m_pContext;
	};

}
