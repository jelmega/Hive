// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// RenderPass.h: render pass
#pragma once
#include "RendererCorePCH.h"
#include "Renderer/RendererCoreCommon.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv::RHI {
	class RHIContext;
}

namespace Hv::Renderer::Core {
	class RenderTarget;

	struct RenderPassAttachment
	{
		PixelFormat format		= PixelFormat();
		SampleCount samples		= SampleCount::Sample1;
		RenderTargetType type	= RenderTargetType::None;
		LoadOp loadOp			= LoadOp::DontCare;
		StoreOp storeOp			= StoreOp::DontCare;
		LoadOp stencilLoadOp	= LoadOp::DontCare;
		StoreOp stencilStoreOp	= StoreOp::DontCare;
	};

	struct RenderPassAttachmentRef
	{
		u32 index;				/**< Reference to attachment */
		RenderTargetType type;	/**< As what render target type the render target will be used in the subpass */
	};

	struct SubRenderPass
	{
		DynArray<RenderPassAttachmentRef> attachments;
	};

	class HIVE_API RenderPass
	{
	public:
		RenderPass();
		virtual ~RenderPass();

		/**
		 * Create a render pass
		 * @param[in] pContext		RHI context
		 * @param[in] attachments	Attachments
		 * @param[in] subpasses		Sub passes
		 * @return					True if the renderpass was created successfully, false otherwise
		 */
		virtual b8 Create(RHI::RHIContext* pContext, const DynArray<RenderPassAttachment>& attachments, const DynArray<SubRenderPass>& subpasses) = 0;

		/**
		 * Destroy a renderpass
		 * @return	True if the renderpass was desstroyed successfully, false otherwise
		 */
		virtual b8 Destroy() = 0;

	protected:
		RHI::RHIContext* m_pContext;					/**< RHI context */
		DynArray<RenderPassAttachment> m_Attachments;	/**< Render pass attachments */
		DynArray<SubRenderPass> m_SubPasses;			/**< Subpasses */
	};

}

#pragma warning(pop)
