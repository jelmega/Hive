// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// RendererCoreHelpers.h: Renderer core helpers
#pragma once
#include "RendererCorePCH.h"
#include "Renderer/RendererCoreCommon.h"
#include "Texture.h"

namespace Hv::Renderer::Helpers {
	
	/**
	* Get the GPU vendor as a string
	* @param[in] vendor	GPU vendor
	* @return				GPU vendor as a sting
	*/
	HIVE_API const AnsiChar* GetGpuVendorString(GpuVendor vendor);
	/**
	 * Get the texture layout for a rendertarget type
	 * @param[in] type	Rendertarget type
	 * @return			Layout
	 */
	HIVE_API TextureLayout GetTextureLayoutFromRTType(RenderTargetType type);

	// ReSharper disable CppNonInlineVariableDefinitionInHeaderFile
	namespace Tables {
		
		static TextureLayout g_RTLayouts[u8(RenderTargetType::MultisampleResolve) + 1] = {
			TextureLayout::General,
			TextureLayout::ColorAttachment,
			TextureLayout::ColorAttachment,
			TextureLayout::DepthStencil,
			TextureLayout::ColorAttachment
		};

	}
	// ReSharper restore CppNonInlineVariableDefinitionInHeaderFile
}
