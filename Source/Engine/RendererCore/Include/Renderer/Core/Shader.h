// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Shader.h: Shader
#pragma once
#include "RendererCorePCH.h"
#include "Renderer/RendererCoreCommon.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv::RHI {
	class RHIContext;
}

namespace Hv::Renderer::Core {
	
	struct ShaderDesc
	{
		ShaderType type;			/**< Shader type */
		String filePath;			/**< Path to shader source file */
		String entryPoint;			/**< Shader entry point */
		ShaderLanguage language;	/**< Shader language */
	};

	class HIVE_API Shader
	{
	public:
		Shader();
		virtual ~Shader();

		/**
		 * Create a shader
		 * @param[in] pContext		RHI context
		 * @param[in] desc			Shader description
		 * @return	True if the shader was created successfully, false otherwise
		 */
		virtual b8 Create(RHI::RHIContext* pContext, const ShaderDesc& desc) = 0;

		/**
		 * Destroy the shader
		 * @return	True if the shader was destroyed successfully, false otherwise
		 */
		virtual b8 Destroy() = 0;

		/**
		 * Get the shader type
		 * @return	Shader type
		 */
		ShaderType GetType() const { return m_Type; }
		/**
		 * Get the shader type
		 * @return	Shader type
		 */
		const String& GetFilePath() const { return m_FilePath; }
		/**
		 * Get the shader type
		 * @return	Shader type
		 */
		const DynArray<u8>& GetSource() const { return m_Source; }
		/**
		* Get the shader language
		* @return	Shader language
		*/
		ShaderLanguage GetLanguage() const { return m_Language; }
		/**
		* When the shader source is text, check if it is compiled
		* @return	True if the text shader source is compiled, false otherwise
		*/
		b8 IsCompiled() const { return m_IsCompiled; }

	protected:
		/**
		 * Load the shader source
		 * @return True if the shader source was loaded, false otherwise
		 */
		b8 LoadShaderSource();

		RHI::RHIContext* m_pContext;	/**< RHI context */
		ShaderType m_Type;				/**< Shader type */
		String m_FilePath;				/**< Path to source */
		AnsiString m_EntryPoint;		/**< Entry point of the shader */
		DynArray<u8> m_Source;			/**< Source code */

		ShaderLanguage m_Language;		/**< Shader language */
		b8 m_IsCompiled;				/**< If the source was text, has it been compiled? */
	};

}

#pragma warning(pop)
