// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DescriptorSetLayout.h: Descriptor set layout
#pragma once
#include "RendererCorePCH.h"
#include "Renderer/RendererCoreCommon.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv::RHI {
	class RHIContext;
}

namespace Hv::Renderer::Core {
	class DescriptorSetManager;

	struct DescriptorSetBinding
	{
		DescriptorSetBindingType type;	/**< Descriptor set type */
		ShaderType shadertype;	/**< Shader type/stage */
		u32 count;				/**< Number of uniform variables of this type, 0 is reserved */
	};

	class HIVE_API DescriptorSetLayout
	{
	public:
		DescriptorSetLayout();
		virtual ~DescriptorSetLayout();

		/**
		 * Create the descriptor set
		 * @param[in] pContext	RHI context
		 * @param[in] pManager	Descriptor set manager
		 * @param[in] bindings	Descriptor set bindings
		 * @return				True if the descriptor set was created successfully, false otherwise
		 */
		virtual b8 Create(RHI::RHIContext* pContext, DescriptorSetManager* pManager, const DynArray<DescriptorSetBinding>& bindings) = 0;
		/**
		 * Destroy the descriptor set
		 * @return	True if the descriptor set was destroyed successfully, false otherwise
		 */
		virtual b8 Destroy() = 0;

		/**
		 * Check if a group of binding matches the layout's bindings
		 * @return	True if the bindings match, false otherwise
		 */
		b8 Matches(const DynArray<DescriptorSetBinding>& bindings);

		/**
		 * Get the layout's descriptor set bindings
		 * @return	Descriptor set bindings
		 */
		DynArray<DescriptorSetBinding>& GetBindings() { return m_Bindings; }

		/**
		 * Increment the layout reference count
		 */
		void IncRefs() { ++m_RefCount; }
		/**
		 * Decrement the layout reference count
		 */
		void DecRefs() { if (m_RefCount > 0) { --m_RefCount; } }
		/**
		 * Get the layout's reference count
		 * @return	Reference count
		 */
		u32 GetReferenceCount() const { return m_RefCount; }
		
	protected:
		RHI::RHIContext* m_pContext;
		DescriptorSetManager* m_pManager;
		DynArray<DescriptorSetBinding> m_Bindings;
		u32 m_RefCount;
	};

}

#pragma warning(pop)
