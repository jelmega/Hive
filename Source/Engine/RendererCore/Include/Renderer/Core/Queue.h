// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Queue.h: GPU queue
#pragma once
#include "RendererCorePCH.h"
#include "Renderer/RendererCoreCommon.h"

namespace Hv::RHI {
	class RHIContext;
}

namespace Hv::Renderer::Core {

	class HIVE_API Queue
	{
	public:
		Queue();
		virtual ~Queue();

		/**
		 * Initialize the queue
		 * @param[in] pContext	RHI context
		 * @param[in] type		Queue type
		 * @param[in] index		Queue index
		 * @param[in] priority	Queue priority
		 * @return				True if the queue was initialize successfully, false otherwise
		 */
		virtual b8 Init(RHI::RHIContext* pContext, QueueType type, u32 index, Renderer::QueuePriority priority) = 0;

		/**
		 * Wait for the queue to be idle
		 * @return	True if queue is idle, false otherwise
		 */
		virtual b8 WaitIdle() = 0;


		/**
		 * Get the queue type
		 * @return	Queue type
		 */
		QueueType GetQueueType() const { return m_Type; }

	protected:
		RHI::RHIContext* m_pContext;
		QueueType m_Type;
		QueuePriority m_Priority;
	};

}
