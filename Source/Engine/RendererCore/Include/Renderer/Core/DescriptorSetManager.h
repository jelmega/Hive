// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DescriptorSetManager.h: Descriptor set manager
#pragma once
#include "RendererCorePCH.h"
#include "DescriptorSet.h"
#include "DescriptorSetLayout.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv::RHI {
	class RHIContext;
}

namespace Hv::Renderer::Core {

	class HIVE_API DescriptorSetManager
	{
	public:
		DescriptorSetManager();
		virtual ~DescriptorSetManager();

		/**
		 * Create the descriptor set manager
		 * @param[in] pContext	RHI context
		 * @return				True if the descriptor set manager was created successfully, false otherwise
		 */
		virtual b8 Create(RHI::RHIContext* pContext) = 0;
		/**
		 * Create the descriptor set manager
		 * @return	True if the descriptor set manager was destroyed successfully, false otherwise
		 */
		virtual b8 Destroy() = 0;

		/**
		 * Create a descriptor set
		 * @param[in] bindings	Descriptor set bindings
		 * @return				Pointer to a descriptor set, nullptr if the creation failed
		 */
		virtual DescriptorSetLayout* CreateDescriptorSetLayout(const DynArray<DescriptorSetBinding>& bindings) = 0;
		/**
		 * Create a descriptor set
		 * @param[in] pLayout	Descriptor set layout
		 * @return				Pointer to a descriptor set, nullptr if the creation failed
		 */
		virtual DescriptorSet* CreateDescriptorSet(DescriptorSetLayout* pLayout) = 0;
		/**
		 * Destroy a descriptor set
		 * @param[in] pLayout	Descriptor set layout to destroy
		 * @return				True if the descriptor set layout was destroyed successfully, false otherwise
		 */
		virtual b8 DestroyDescriptorSetLayout(DescriptorSetLayout* pLayout) = 0;
		/**
		 * Destroy a descriptor set
		 * @param[in] pDescriptorSet	Descriptor set to destroy
		 * @return						True if the descriptor set was destroyed successfully, false otherwise
		 */
		virtual b8 DestroyDescriptorSet(DescriptorSet* pDescriptorSet) = 0;


	protected:
		RHI::RHIContext* m_pContext;
		DynArray<DescriptorSetLayout*> m_Layouts;
		DynArray<DescriptorSet*> m_DescriptorSets;
	};

}

#pragma warning(pop)
