// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Sampler.h: Sampler
#pragma once
#include "RendererCorePCH.h"
#include "Renderer/RendererCoreCommon.h"

namespace Hv::RHI {
	class RHIContext;
}

namespace Hv::Renderer::Core {
	
	struct SamplerDesc
	{
		FilterMode magFilter;		/**< Magnification filter */
		FilterMode minFilter;		/**< Minification filter */
		AddressMode addressU;		/**< U coordinate address mode */
		AddressMode addressV;		/**< V coordinate address mode */
		AddressMode addressW;		/**< W coordinate address mode */

		b8 enableAnisotropy;		/**< Enable anisotropic filtering */
		u8 anisotropy;				/**< Anisotropy used */
		
		b8 enableCompare;			/**< Enable reference value compare */
		CompareOp compareOp;		/**< Compare op */

		f32 mipLodBias;				/**< LOD bias */
		f32 minLod;					/**< Min LOD */
		f32 maxLod;					/**< Max LOD */
		
		f32v4 borderColor;			/**< Border color (exact color not quarenteed and based upon RHI) */

		b8 unnormalizedCoordinates;	/**< Use unnormalized coordinates (has restriction on use cases) */

		/**
		 * Set the sampler's filter mode
		 * @param[in] filter	Filter mode
		 */
		void SetFilter(FilterMode filter) { magFilter = minFilter = filter; }
		/**
		 * Set the sampler's address mode
		 * @param[in] addressMode	Address mode
		 */
		void SetAddressMode(AddressMode addressMode) { addressU = addressV = addressW = addressMode; }
	};

	class HIVE_API Sampler
	{
	public:
		Sampler();
		virtual ~Sampler();

		/**
		 * Create the sampler
		 * @param[in] pContext	RHI context
		 * @param[in] desc		Sampler description
		 * @return				True if the sampler was created successfully, false otherwise
		 */
		virtual b8 Create(RHI::RHIContext* pContext, const SamplerDesc& desc) = 0;
		/**
		 * Create the sampler
		 * @return				True if the sampler was destroyed successfully, false otherwise
		 */
		virtual b8 Destroy() = 0;

	protected:
		RHI::RHIContext* m_pContext;	/**< RHI context */
		SamplerDesc m_Desc;				/**< Sampler description */
	};

}