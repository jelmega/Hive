// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ClearValue.h: Clear value
#pragma once
#include "RendererCorePCH.h"

#pragma warning(push)
#pragma warning(disable : 4201) // nonstandard extension used: nameless struct/union

namespace Hv::Renderer::Core {

	struct ClearValue
	{
		union
		{
			f32v4 color;
			struct
			{
				f32 depth;
				u32 stencil;
			};
		};

		ClearValue()
			: color()
		{
		}

		ClearValue(f32v4 color)
			: color(color)
		{
		}

		ClearValue(f32 depth, u32 stencil)
			: depth(depth)
			, stencil(stencil)
		{
		}

		ClearValue(const ClearValue& value)
			: color(value.color)
		{
		}

		ClearValue(ClearValue&& value) noexcept
			: color(Forward<f32v4>(value.color))
		{
		}

		ClearValue& operator=(const ClearValue& value)
		{
			color = value.color;
			return *this;
		}

		ClearValue& operator=(ClearValue&& value) noexcept
		{
			color = Forward<f32v4>(value.color);
			return *this;
		}
	};

}

#pragma warning(pop)
