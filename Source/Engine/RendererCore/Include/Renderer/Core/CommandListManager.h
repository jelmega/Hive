// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// CommandListManager.h: Command list manager
#pragma once
#include "RendererCorePCH.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv::RHI {
	class RHIContext;
}

namespace Hv::Renderer::Core {

	class CommandList;
	class Queue;
	
	class HIVE_API CommandListManager
	{
	public:
		CommandListManager();
		virtual ~CommandListManager();

		/**
		 * Create a command list manager
		 * @param[in] pContext	RHI context
		 * @return				True if the command list manager was created successfully, false otherwise
		 */
		virtual b8 Create(RHI::RHIContext* pContext) = 0;
		/**
		 * Destroy the command list manager
		 * @return				True if the command list manager was created successfully, false otherwise
		 */
		virtual b8 Destroy() = 0;

		/**
		 * Create a command list for a certain queue
		 * @param[in] pQueue	Queue
		 * @return				Pointer to a command buffer, nullptr if creation failed
		 */
		virtual CommandList* CreateCommandList(Queue* pQueue) = 0;
		/**
		 * Destroy a command list
		 * @param[in] pCommandList	Command list to destroy
		 * @return					True if the command list was successfully destroyed, false otherwise
		 */
		virtual b8 DestroyCommandList(CommandList* pCommandList) = 0;

		/**
		 * Create and begin a single time command list for a certain queue
		 * @param[in] pQueue	Queue
		 * @return				Pointer to a single time command buffer, nullptr if creation failed
		 */
		virtual CommandList* CreateSingleTimeCommandList(Queue* pQueue) = 0;
		/**
		 * End and estroy a singe time command list
		 * @param[in] pCommandList	Single time command list
		 * @return					True if the single time command list was successfully destroyed, false otherwise
		 */
		virtual b8 EndSingleTimeCommandList(CommandList* pCommandList) = 0;

	protected:
		RHI::RHIContext* m_pContext;				/**< RHI Context */
		DynArray<CommandList*> m_CommandLists;		/**< Command lists */
		DynArray<CommandList*> m_STCommandLists;	/**< Single time command lists */
		CommandList* m_pActiveCommandList;			/**< Active command list */
	};

}

#pragma warning(pop)
