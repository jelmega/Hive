// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// CommandListManager.cpp: Command list manager
#include "RendererCorePCH.h"
#include "Renderer/Core/CommandListManager.h"

namespace Hv::Renderer::Core {


	CommandListManager::CommandListManager()
		: m_pContext(nullptr)
		, m_pActiveCommandList(nullptr)
	{
	}

	CommandListManager::~CommandListManager()
	{
	}
}
