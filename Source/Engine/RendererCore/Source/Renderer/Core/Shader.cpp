// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// GpuShader.cpp: Shader
#include "RendererCorePCH.h"
#include "Renderer/Core/Shader.h"

namespace Hv::Renderer::Core {

	Shader::Shader()
		: m_pContext(nullptr)
		, m_Type(ShaderType::None)
		, m_IsCompiled(false)
		, m_Language(ShaderLanguage::None)
	{
	}

	Shader::~Shader()
	{
	}

	b8 Shader::LoadShaderSource()
	{
		HvFS::Path path(m_FilePath);
		HvFS::File file = FileSystem::CreateFile(path, HvFS::FileMode::Open, HvFS::AccessMode::Read);
		if (!file.IsValid())
		{
			g_Logger.LogFormat(LogRenderCore(), LogLevel::Error, "Failed to open shader file: %s!", m_FilePath);
			return false;
		}
		sizeT size = file.GetFileSize();
		m_Source.Resize(size);
		b8 ret = file.ReadBytes(m_Source.Data(), size);
		if (!ret)
		{
			g_Logger.LogFormat(LogRenderCore(), LogLevel::Error, "Failed to load shader source: %s!", m_FilePath);
		}
		b8 res = file.Close();
		if (!res)
		{
			g_Logger.LogFormat(LogRenderCore(), LogLevel::Warning, "Failed to clode shader file: %s (file will not be able to be opened until engine shuts down)!", m_FilePath);
		}

		return ret;
	}
}
