// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Fence.cpp: Fence
#pragma once
#include "RendererCorePCH.h"
#include "Renderer/Core/Fence.h"
#include "Renderer/RendererCoreCommon.h"

namespace Hv::Renderer::Core {


	Fence::Fence()
		: m_pContext(nullptr)
		, m_Status(FenceStatus::Unsignaled)
		, m_FenceValue(0)
	{
	}

	Fence::~Fence()
	{
	}
}
