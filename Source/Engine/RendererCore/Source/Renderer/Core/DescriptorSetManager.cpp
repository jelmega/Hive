// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DescriptorSetManager.h: Descriptor set manager
#pragma once
#include "RendererCorePCH.h"
#include "Renderer/Core/DescriptorSetManager.h"

namespace Hv::Renderer::Core {

	DescriptorSetManager::DescriptorSetManager()
		: m_pContext(nullptr)
	{
	}

	DescriptorSetManager::~DescriptorSetManager()
	{
	}

}
