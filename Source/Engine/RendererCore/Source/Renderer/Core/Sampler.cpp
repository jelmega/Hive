// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Sampler.cpp: Sampler
#include "RendererCorePCH.h"
#include "Renderer/Core/Sampler.h"

namespace Hv::Renderer::Core {


	Sampler::Sampler()
		: m_pContext(nullptr)
		, m_Desc()
	{
	}

	Sampler::~Sampler()
	{
	}
}
