// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Framebuffer.cpp: framebuffer
#include "RendererCorePCH.h"
#include "Renderer/Core/Framebuffer.h"

namespace Hv::Renderer::Core {

	Framebuffer::Framebuffer()
		: m_pContext(nullptr)
		, m_pRenderPass(nullptr)
	{
	}

	Framebuffer::~Framebuffer()
	{
	}
}
