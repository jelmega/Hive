// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Texture.cpp: Texture
#include "RendererCorePCH.h"
#include "Renderer/Core/Texture.h"

namespace Hv::Renderer::Core {


	Texture::Texture()
		: m_pContext(nullptr)
		, m_Desc()
		, m_MemorySize(0)
	{
	}

	Texture::~Texture()
	{
	}
}
