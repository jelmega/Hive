// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Semaphore.cpp: Semaphore
#pragma once
#include "RendererCorePCH.h"
#include "Renderer/Core/Semaphore.h"

namespace Hv::Renderer::Core {


	Semaphore::Semaphore()
		: m_pContext(nullptr)
	{
	}

	Semaphore::~Semaphore()
	{
	}
}
