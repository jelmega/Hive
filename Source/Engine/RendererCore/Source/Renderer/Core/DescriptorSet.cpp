// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DescriptorSet.h: Descriptor set
#pragma once
#include "RendererCorePCH.h"
#include "Renderer/Core/DescriptorSet.h"

namespace Hv::Renderer::Core {

	DescriptorSet::DescriptorSet()
		: m_pContext(nullptr)
		, m_pManager(nullptr)
	{
	}

	DescriptorSet::~DescriptorSet()
	{
	}
}
