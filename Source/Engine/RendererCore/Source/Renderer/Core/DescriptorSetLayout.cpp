// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DescriptorSetLayout.cpp: Descriptor set layout
#include "RendererCorePCH.h"
#include "Renderer/Core/DescriptorSetLayout.h"

namespace Hv::Renderer::Core {


	DescriptorSetLayout::DescriptorSetLayout()
		: m_pContext(nullptr)
		, m_pManager(nullptr)
	{
	}

	DescriptorSetLayout::~DescriptorSetLayout()
	{
	}

	b8 DescriptorSetLayout::Matches(const DynArray<DescriptorSetBinding>& bindings)
	{
		if (bindings.Size() != m_Bindings.Size())
			return false;

		for (sizeT i = 0; i < m_Bindings.Size(); ++i)
		{
			const DescriptorSetBinding& binding0 = m_Bindings[i];
			const DescriptorSetBinding& binding1 = bindings[i];

			if (binding0.type != binding1.type ||
				binding0.shadertype != binding1.shadertype ||
				binding0.count != binding1.count)
			{
				return false;
			}
		}
		return true;
	}
}
