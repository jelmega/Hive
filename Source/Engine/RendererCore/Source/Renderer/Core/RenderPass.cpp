// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// RenderPass.cpp: render pass
#include "RendererCorePCH.h"
#include "Renderer/Core/RenderPass.h"

namespace Hv::Renderer::Core {


	RenderPass::RenderPass()
		: m_pContext(nullptr)
	{
	}

	RenderPass::~RenderPass()
	{
	}
}
