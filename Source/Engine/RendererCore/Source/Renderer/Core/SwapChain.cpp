// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// RenderView.cpp: Render view (swapchain)
#include "RendererCorePCH.h"
#include "Renderer/Core/SwapChain.h"

namespace Hv::Renderer::Core {


	SwapChain::SwapChain()
		: m_pContext(nullptr)
		, m_pWindow(nullptr)
		, m_VSync(VSyncMode::Off)
		, m_pPresentQueue(nullptr)
		, m_RenderTargetIndex(0)
		, m_SemaphoreIndex(0)
	{
	}

	SwapChain::~SwapChain()
	{
	}

	RenderTarget* SwapChain::GetRenderTarget(u32 index)
	{
		if (index >= m_RenderTargets.Size())
			return nullptr;

		return m_RenderTargets[index];
	}
}
