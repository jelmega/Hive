// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Pipeline.cpp: Pipeline
#include "RendererCorePCH.h"
#include "Renderer/Core/Pipeline.h"

namespace Hv::Renderer::Core {

	Pipeline::Pipeline()
		: m_pContext()
		, m_Type()
		, m_GraphicsDesc()
	{
	}

	Pipeline::~Pipeline()
	{
	}
}
