// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Queue.cpp: GPU queue
#include "RendererCorePCH.h"
#include "Renderer/Core/Queue.h"

namespace Hv::Renderer::Core {

	Queue::Queue()
		: m_pContext(nullptr)
		, m_Type(QueueType::Unknown)
		, m_Priority(QueuePriority::High)
	{
	}

	Queue::~Queue()
	{
	}
}
