// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// RenderTarget.cpp: Render target
#include "RendererCorePCH.h"
#include "Renderer/Core/RenderTarget.h"

namespace Hv::Renderer::Core {

	RenderTarget::RenderTarget()
		: m_pContext(nullptr)
		, m_pTexture(nullptr)
		, m_Type(RenderTargetType::None)
	{
	}

	RenderTarget::~RenderTarget()
	{
	}
}
