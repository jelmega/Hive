// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// RendererCoreHelpers.cpp: Renderer core helpers
#include "RendererCorePCH.h"
#include "Renderer/Core/RendererCoreHelpers.h"

namespace Hv::Renderer::Helpers {

	const AnsiChar* GetGpuVendorString(GpuVendor vendor)
	{
		switch (vendor)
		{
		default:
		case GpuVendor::Unknown:	return "Unkown";
		case GpuVendor::Nvidia:		return "Nvidia";
		case GpuVendor::AMD:		return "AMD";
		case GpuVendor::Intel:		return "Intel";
		case GpuVendor::ARM:		return "ARM";
		case GpuVendor::Qualcomm:	return "Qualcomm";
		case GpuVendor::ImgTec:		return "Imagination Technologies";
		}
	}

	TextureLayout GetTextureLayoutFromRTType(RenderTargetType type)
	{
		HV_ASSERT(u8(type) <= u8(RenderTargetType::MultisampleResolve));
		return Tables::g_RTLayouts[u8(type)];
	}
}
