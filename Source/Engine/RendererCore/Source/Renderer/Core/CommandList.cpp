// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// CommandList.cpp: Command list
#include "RendererCorePCH.h"
#include "Renderer/Core/CommandList.h"
#include "Renderer/Core/Fence.h"

namespace Hv::Renderer::Core {


	CommandList::CommandList()
		: m_pContext(nullptr)
		, m_pManager(nullptr)
		, m_pQueue(nullptr)
		, m_Status(CommandListState::Reset)
		, m_pFence(nullptr)
		, m_pPipeline(nullptr)
		, m_pRenderPass(nullptr)
		, m_pFramebuffer(nullptr)
	{
	}

	CommandList::~CommandList()
	{
	}

	CommandListState CommandList::GetState()
	{
		if (m_Status == CommandListState::Submited)
		{
			if (m_pFence->GetStatus() == FenceStatus::Signaled)
				m_Status = CommandListState::Finished;
		}
		return m_Status;
	}
}
