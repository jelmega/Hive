// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// DynamicRHI.cpp: Dynamic RHI (implemented by API specific RHIs)
#include "RendererCorePCH.h"
#include "RHI/IDynamicRHI.h"

HV_DECLARE_LOG_CATEGORY(RenderCore, Hv::LogLevel::All);

namespace Hv::RHI {
	
	IDynamicRHI::IDynamicRHI()
		: m_Desc()
		, m_pContext(nullptr)
	{
	}

	IDynamicRHI::~IDynamicRHI()
	{
	}
}
