// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// RHIContext.cpp: RHI Context
#include "RendererCorePCH.h"
#include "RHI/RHIContext.h"

namespace Hv::RHI {

	RHIContext::RHIContext()
		: m_pCommandListManager()
	{
	}

	RHIContext::~RHIContext()
	{
	}

	Renderer::Core::Queue* RHIContext::GetQueue(Renderer::QueueType type)
	{
		Renderer::Core::Queue* pOutQueue = nullptr;
		for (Renderer::Core::Queue* pQueue : m_Queues)
		{
			Renderer::QueueType queueType = pQueue->GetQueueType();
			if (pOutQueue)
			{
				if (HV_IS_ENUM_FLAG_SET(queueType, type))
					pOutQueue = pQueue;
				if ((queueType & ~type) == Renderer::QueueType::Unknown)
					return pOutQueue;
			}
			else
			{
				if ((queueType & ~type) == Renderer::QueueType::Unknown)
					return pQueue;
			}
			
		}
		return pOutQueue;
	}
}
