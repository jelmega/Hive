// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// FrameGraph.cpp: Frame graph
#include "RendererPCH.h"
#include "Renderer/FrameGraph.h"
#include "Renderer/PassContext.h"

namespace Hv::Renderer {

	FrameGraphResources::FrameGraphResources(RHI::IDynamicRHI* pRHI, ResourceManager* pResourceManager, FrameGraphRegistry* pRegistry)
		: m_pRHI(pRHI)
		, m_pResourceManager(pResourceManager)
		, m_pRegistry(pRegistry)
	{
	}

	FrameGraphResources::~FrameGraphResources()
	{
	}

	void FrameGraphResources::AddResource(const FrameGraphTextureDesc& desc)
	{
		// Check if an unused resource is available
		for (u32 i = 0; i < m_Availability.Size(); ++i)
		{
			Resource& res = m_Resources[i];
			if (m_Availability[i] && !res.isBuffer)
			{
				FrameGraphTextureDesc& texDesc = res.tex.desc;
				if (desc.width == texDesc.width &&
					desc.height == texDesc.height &&
					desc.format == texDesc.format)
				{
					m_ResourceMapping.Push(i);
					return;
				}
			}
		}

		// No resource can be reused, create new resource
		Core::RenderTargetDesc rtDesc = {};
		rtDesc.width = desc.width;
		rtDesc.height = desc.height;
		rtDesc.format = desc.format;
		rtDesc.samples = SampleCount::Sample1;
		if (desc.format.HasDepthComponent())
			rtDesc.type = RenderTargetType::DepthStencil;
		else
			rtDesc.type = RenderTargetType::Color;

		Resource resource = {};
		resource.tex.desc = desc;

		resource.tex.pRT = m_pResourceManager->CreateRenderTarget(rtDesc);

		if (rtDesc.type == RenderTargetType::DepthStencil)
		{
			Core::ClearValue clearValue;
			clearValue.depth = 1.f;
			resource.tex.pRT->SetClearColor(clearValue);
		}

		m_ResourceMapping.Push(u32(m_Resources.Size()));
		m_Resources.Push(resource);
		m_Availability.Push(false);
	}

	void FrameGraphResources::AddResource(const FrameGraphBufferDesc& desc)
	{
		// Check if an unused resource is available
		for (u32 i = 0; i < m_Availability.Size(); ++i)
		{
			Resource& res = m_Resources[i];
			if (m_Availability[i] && res.isBuffer)
			{
				FrameGraphBufferDesc& bufDesc = res.buf.desc;
				if (desc.size == bufDesc.size &&
					desc.texelFormat == bufDesc.texelFormat)
				{
					m_ResourceMapping.Push(i);
					return;
				}
			}
		}

		// No resource can be reused, create new resource
		BufferType bufType = BufferType::Uniform | BufferType::Storage;
		if (desc.texelFormat.components == PixelFormatComponents::UNKOWN)
		{
			bufType |= BufferType::StorageTexel | BufferType::UniformTexel;
		}

		Resource resource = {};
		resource.buf.desc = desc;

		resource.buf.pBuffer = m_pResourceManager->CreateBuffer(bufType, desc.size, BufferFlags::None);

		m_ResourceMapping.Push(u32(m_Resources.Size()));
		m_Resources.Push(resource);
		m_Availability.Push(false);
	}

	void FrameGraphResources::Reset()
	{
		for (bool& availability : m_Availability)
		{
			availability = true;
		}
		m_ResourceMapping.Clear();
	}

	void FrameGraphResources::Cleanup()
	{
		// Destroy resources
		for (Resource& resource : m_Resources)
		{
			if (resource.isBuffer)
			{
				m_pResourceManager->DestroyBuffer(resource.buf.pBuffer);
			}
			else
			{
				m_pResourceManager->DestroyRenderTarget(resource.tex.pRT);
			}
		}
	}

	Core::RenderTarget* FrameGraphResources::GetRenderTarget(FrameGraphResource resource)
	{
		if (resource == InvalidFrameGraphResource)
			return nullptr;

		Detail::FrameGraphAlias& alias = m_pRegistry->GetAlias(resource);
		Resource& res = m_Resources[m_ResourceMapping[alias.resIdx]];

		if (res.isBuffer)
			return nullptr;

		return res.tex.pRT;
	}

	Core::Texture* FrameGraphResources::GetTexture(FrameGraphResource resource)
	{
		if (resource == InvalidFrameGraphResource)
			return nullptr;

		Detail::FrameGraphAlias& alias = m_pRegistry->GetAlias(resource);
		Resource& res = m_Resources[m_ResourceMapping[alias.resIdx]];

		if (res.isBuffer)
			return nullptr;

		return res.tex.pRT->GetTexture();
	}

	Core::Buffer* FrameGraphResources::GetBuffer(FrameGraphResource resource)
	{
		if (resource == InvalidFrameGraphResource)
			return nullptr;

		Detail::FrameGraphAlias& alias = m_pRegistry->GetAlias(resource);
		Resource& res = m_Resources[m_ResourceMapping[alias.resIdx]];

		if (!res.isBuffer)
			return nullptr;

		return res.buf.pBuffer;
	}

	////////////////////////////////////////////////////////////////////////////////

	FrameGraphRegistry::FrameGraphRegistry()
	{
	}

	FrameGraphRegistry::~FrameGraphRegistry()
	{
	}

	FrameGraphResource FrameGraphRegistry::Create(const String& iden, const FrameGraphTextureDesc& desc, b8 isMutable,
		b8 isImported)
	{
		Detail::FrameGraphAlias alias = {};
		alias.isImported = isImported;
		alias.isMutable = isMutable;
		alias.isBuffer = false;
		alias.isCreated = true;
		alias.descIdx = u32(m_TexDescs.Size());
		alias.idenIdx = u32(m_Identifiers.Size());
		alias.transientIdx = u32(m_TexDescs.Size() + m_BufDescs.Size());

		m_TexDescs.Push(desc);
		m_Identifiers.Push(iden);

		FrameGraphResource resource = (FrameGraphResource)m_Aliases.Size();
		m_Aliases.Push(alias);

		m_CurResIdenMapping.Insert(iden, u32(m_CurResources.Size()));
		m_CurResources.Push(resource);

		return resource;
	}

	FrameGraphResource FrameGraphRegistry::Create(const String& iden, const FrameGraphBufferDesc& desc, b8 isMutable,
		b8 isImported)
	{
		Detail::FrameGraphAlias alias = {};
		alias.isImported = isImported;
		alias.isMutable = isMutable;
		alias.isBuffer = true;
		alias.isCreated = true;
		alias.descIdx = u32(m_BufDescs.Size());
		alias.idenIdx = u32(m_Identifiers.Size());
		alias.transientIdx = u32(m_TexDescs.Size() + m_BufDescs.Size());

		m_BufDescs.Push(desc);
		m_Identifiers.Push(iden);

		FrameGraphResource resource = (FrameGraphResource)m_Aliases.Size();
		m_Aliases.Push(alias);

		m_CurResIdenMapping.Insert(iden, u32(m_CurResources.Size()));
		m_CurResources.Push(resource);

		return resource;
	}

	FrameGraphResource FrameGraphRegistry::GetResource(const String& iden)
	{
		auto it = m_CurResIdenMapping.Find(iden);
		if (it == m_CurResIdenMapping.Back())
			return InvalidFrameGraphResource;
		return m_CurResources[it->second];
	}

	FrameGraphResource FrameGraphRegistry::CreateNewAlias(FrameGraphResource resource)
	{
		Detail::FrameGraphAlias& alias = m_Aliases[u64(resource)];
		String& iden = m_Identifiers[alias.idenIdx];

		auto it = m_CurResIdenMapping.Find(iden);
		if (it == m_CurResIdenMapping.Back())
			return InvalidFrameGraphResource;

		Detail::FrameGraphAlias newAlias = alias;
		newAlias.isCreated = false;
		FrameGraphResource newRes = (FrameGraphResource)m_Aliases.Size();
		m_Aliases.Push(newAlias);
		m_CurResources[it->second] = newRes;

		return newRes;
	}

	FrameGraphResource FrameGraphRegistry::CreateNewAlias(const String& iden)
	{
		auto it = m_CurResIdenMapping.Find(iden);
		if (it == m_CurResIdenMapping.Back())
			return InvalidFrameGraphResource;

		FrameGraphResource resource = m_CurResources[it->second];
		Detail::FrameGraphAlias newAlias = m_Aliases[sizeT(resource)];
		newAlias.isCreated = false;
		FrameGraphResource newRes = (FrameGraphResource)m_Aliases.Size();
		m_Aliases.Push(newAlias);
		m_CurResources[it->second] = newRes;

		return newRes;
	}

	////////////////////////////////////////////////////////////////////////////////

	FrameGraphBuilder::FrameGraphBuilder(Detail::FrameGraphPass* pPass, FrameGraphRegistry* pRegistry)
		: m_pPass(pPass)
		, m_pRegistry(pRegistry)
	{
	}

	FrameGraphBuilder::~FrameGraphBuilder()
	{
	}

	FrameGraphResource FrameGraphBuilder::Create(const String& iden, const FrameGraphTextureDesc& desc)
	{
		FrameGraphResource resource = m_pRegistry->Create(iden, desc);
		m_pPass->creates.Push(resource);
		return resource;
	}

	FrameGraphResource FrameGraphBuilder::Create(const String& iden, const FrameGraphBufferDesc& desc)
	{
		FrameGraphResource resource = m_pRegistry->Create(iden, desc);
		m_pPass->creates.Push(resource);
		return resource;
	}

	FrameGraphResource FrameGraphBuilder::Import(const String& iden, const FrameGraphTextureDesc& desc, b8 isMutable)
	{
		FrameGraphResource resource = m_pRegistry->Create(iden, desc, isMutable, true);
		return resource;
	}

	FrameGraphResource FrameGraphBuilder::Import(const String& iden, const FrameGraphBufferDesc& desc, b8 isMutable)
	{
		FrameGraphResource resource = m_pRegistry->Create(iden, desc, isMutable, true);
		return resource;
	}

	FrameGraphResource FrameGraphBuilder::Read(FrameGraphResource resource, FrameGraphReadFlags flags)
	{
		m_pPass->reads.Push(Pair<FrameGraphResource, FrameGraphReadFlags>(resource, flags));
		return resource;
	}

	FrameGraphResource FrameGraphBuilder::Read(const String& iden, FrameGraphReadFlags flags)
	{
		FrameGraphResource resource = m_pRegistry->GetResource(iden);
		m_pPass->reads.Push(Pair<FrameGraphResource, FrameGraphReadFlags>(resource, flags));
		return resource;
	}

	FrameGraphResource FrameGraphBuilder::Write(FrameGraphResource resource, FrameGraphWriteFlags flags)
	{
		FrameGraphResource newRes = m_pRegistry->CreateNewAlias(resource);
		m_pPass->writes.Push(Pair<FrameGraphResource, FrameGraphWriteFlags>(newRes, flags));
		return newRes;
	}

	FrameGraphResource FrameGraphBuilder::Write(const String& iden, FrameGraphWriteFlags flags)
	{
		FrameGraphResource newRes = m_pRegistry->CreateNewAlias(iden);
		m_pPass->writes.Push(Pair<FrameGraphResource, FrameGraphWriteFlags>(newRes, flags));
		return newRes;
	}

	////////////////////////////////////////////////////////////////////////////////

	FrameGraph::FrameGraph(RHI::IDynamicRHI* pRHI, ResourceManager* pResourceManager)
		: m_pRHI(pRHI)
		, m_pRegistry(nullptr)
		, m_pResources(nullptr)
	{
		m_pRegistry = HvNew FrameGraphRegistry();
		m_pResources = HvNew FrameGraphResources(m_pRHI, pResourceManager, m_pRegistry);
	}

	FrameGraph::~FrameGraph()
	{
	}

	void FrameGraph::AddPass(Pass* pPass)
	{
		Detail::FrameGraphPass pass = {};

		pass.pPass = pPass;
		pass.identifier = pPass->GetIdentifier();

		FrameGraphBuilder builder(&pass, m_pRegistry);
		pPass->Setup(builder);

		m_IdenMapping.Insert(pass.identifier, u32(m_Passes.Size()));

		m_Passes.Push(pass);
	}

	void FrameGraph::Initialize()
	{
		for (Detail::FrameGraphPass& pass : m_Passes)
		{
			// Create render pass
			if (!pass.pRenderPass)
			{
				Core::RenderPassAttachment depthStencilAttachment = {};
				b8 hasDepthStencil = false;
				DynArray<Core::RenderPassAttachment> attachments;

				for (Pair<FrameGraphResource, FrameGraphReadFlags> pair : pass.reads)
				{
					if (pair.second != FrameGraphReadFlags::AsDepthStencil)
						continue;

					Detail::FrameGraphAlias& alias = m_pRegistry->GetAlias(pair.first);
					if (alias.isBuffer)
						continue;

					FrameGraphTextureDesc& desc = m_pRegistry->GetTextureDescription(alias.descIdx);

					depthStencilAttachment.format = desc.format;
					depthStencilAttachment.samples = SampleCount::Sample1;
					depthStencilAttachment.loadOp = (alias.isCreated || desc.initialState == FrameGraphResourseState::Clear) ? LoadOp::Clear : LoadOp::Load;
					depthStencilAttachment.storeOp = StoreOp::Store;

					if (desc.format.HasStencilComponent())
					{
						depthStencilAttachment.stencilLoadOp = (alias.isCreated || desc.initialState == FrameGraphResourseState::Clear) ? LoadOp::Clear : LoadOp::Load;
						depthStencilAttachment.stencilStoreOp = StoreOp::Store;
					}
					else
					{
						depthStencilAttachment.stencilStoreOp = StoreOp::DontCare;
						depthStencilAttachment.stencilLoadOp = LoadOp::DontCare;
					}

					depthStencilAttachment.type = RenderTargetType::DepthStencil;
					hasDepthStencil = true;
				}

				for (Pair<FrameGraphResource, FrameGraphWriteFlags> pair : pass.writes)
				{
					if (pair.second == FrameGraphWriteFlags::AsDepthStencil && hasDepthStencil)
						continue;

					Detail::FrameGraphAlias& alias = m_pRegistry->GetAlias(pair.first);
					if (!alias.isBuffer)
					{
						FrameGraphTextureDesc& desc = m_pRegistry->GetTextureDescription(alias.descIdx);
						if (pair.second == FrameGraphWriteFlags::AsColor)
						{
							Core::RenderPassAttachment attachment = {};
							attachment.format = desc.format;
							attachment.samples = SampleCount::Sample1;
							attachment.loadOp = (alias.isCreated || desc.initialState == FrameGraphResourseState::Clear) ? LoadOp::Clear : LoadOp::Load;
							attachment.storeOp = StoreOp::Store;
							attachment.stencilLoadOp = LoadOp::DontCare;
							attachment.stencilStoreOp = StoreOp::DontCare;
							attachment.type = RenderTargetType::Color;
							attachments.Push(attachment);
						}
						else if (pair.second == FrameGraphWriteFlags::AsDepthStencil)
						{
							depthStencilAttachment.format = desc.format;
							depthStencilAttachment.samples = SampleCount::Sample1;
							depthStencilAttachment.loadOp = (alias.isCreated || desc.initialState == FrameGraphResourseState::Clear) ? LoadOp::Clear : LoadOp::Load;
							depthStencilAttachment.storeOp = StoreOp::Store;

							if (desc.format.HasStencilComponent())
							{
								depthStencilAttachment.stencilLoadOp = (alias.isCreated || desc.initialState == FrameGraphResourseState::Clear) ? LoadOp::Clear : LoadOp::Load;
								depthStencilAttachment.stencilStoreOp = StoreOp::Store;
							}
							else
							{
								depthStencilAttachment.stencilStoreOp = StoreOp::DontCare;
								depthStencilAttachment.stencilLoadOp = LoadOp::DontCare;
							}

							depthStencilAttachment.type = RenderTargetType::DepthStencil;
							hasDepthStencil = true;
						}
					}
				}

				if (hasDepthStencil)
					attachments.Push(depthStencilAttachment);

				DynArray<Core::RenderPassAttachmentRef> attachmentRefs;
				for (u32 i = 0; i < attachments.Size(); ++i)
				{
					Core::RenderPassAttachment& attachment = attachments[i];
					Core::RenderPassAttachmentRef ref = {};
					ref.type = attachment.type;
					ref.index = i;
					attachmentRefs.Push(ref);
				}

				DynArray<Core::SubRenderPass> subpasses;
				Core::SubRenderPass subPass;
				subPass.attachments = attachmentRefs;
				subpasses.Push(subPass);

				pass.pRenderPass = m_pRHI->CreateRenderPass(attachments, subpasses);
			}

			// Pass contexts
			if (!pass.pContext)
			{
				pass.pContext = HvNew PassContext(m_pRHI, pass.pRenderPass);
			}
		}
	}

	void FrameGraph::Destroy()
	{
		Core::CommandListManager* pCmdListManager = m_pRHI->GetCommandListManager();

		// Cleanup passes
		for (Detail::FrameGraphPass& pass : m_Passes)
		{
			if (pass.pContext)
			{
				Core::CommandList* pCmdList = pass.pContext->GetCommandList();
				if (pCmdList)
					pCmdListManager->DestroyCommandList(pCmdList);
				HvDelete pass.pContext;
			}
			if (pass.pRenderPass)
				m_pRHI->DestroyRenderPass(pass.pRenderPass);
		}

		// Destroy resources and registry
		m_pResources->Cleanup();
		HvDelete m_pResources;
		HvDelete m_pRegistry;
	}

	void FrameGraph::Compile()
	{
		// Reset ref counts
		for (Detail::FrameGraphPass& pass : m_Passes)
		{
			pass.refCount = 0;
		}

		DynArray<Detail::FrameGraphAlias>& aliases = m_pRegistry->GetAliases();
		for (Detail::FrameGraphAlias& alias : aliases)
		{
			alias.refCount = 0;
		}

		// Set ref counts
		for (Detail::FrameGraphPass& pass : m_Passes)
		{
			pass.refCount = u32(pass.writes.Size());
			for (Pair<FrameGraphResource, FrameGraphReadFlags> read : pass.reads)
			{
				Detail::FrameGraphAlias& alias = aliases[sizeT(read.first)];
				++alias.refCount;
			}
		}

		// Go through graph and update ref counts
		for (u32 i = u32(m_Passes.Size()) - 1; i != 0xFFFF'FFFF; --i)
		{
			Detail::FrameGraphPass& pass = m_Passes[i];
			for (Pair<FrameGraphResource, FrameGraphWriteFlags> pair : pass.writes)
			{
				Detail::FrameGraphAlias& alias = aliases[sizeT(pair.first)];
				if (alias.refCount == 0 && !alias.isImported)
					--pass.refCount;
			}
			if (pass.refCount == 0)
			{
				for (Pair<FrameGraphResource, FrameGraphReadFlags> pair : pass.reads)
				{
					Detail::FrameGraphAlias& alias = aliases[sizeT(pair.first)];
					--alias.refCount;
				}
			}
		}

		// Cull passes
		m_CulledPasses.Clear();
		for (u32 i = 0; i < m_Passes.Size(); ++i)
		{
			Detail::FrameGraphPass& pass = m_Passes[i];
			if (pass.refCount > 0)
			{
				m_CulledPasses.Push(i);
			}
		}

		// Transient resource ranges
		DynArray<Detail::FrameGraphTransientResourceRange> transientRanges;
		for (u32 idx : m_CulledPasses)
		{
			Detail::FrameGraphPass& pass = m_Passes[idx];
			for (Pair<FrameGraphResource, FrameGraphReadFlags> pair : pass.reads)
			{
				Detail::FrameGraphAlias& alias = aliases[sizeT(pair.first)];
				if (alias.isImported)
					continue;
				if (alias.transientIdx >= transientRanges.Size())
					transientRanges.Resize(alias.transientIdx + 1);

				Detail::FrameGraphTransientResourceRange& range = transientRanges[alias.transientIdx];
				if (!range.initialized)
				{
					range.isBuffer = alias.isBuffer;
					range.descIdx = alias.descIdx;
					range.firstPass = range.lastPass = idx;
					range.initialized = true;
				}
				else
				{
					transientRanges[alias.transientIdx].lastPass = idx;
				}
			}

			for (Pair<FrameGraphResource, FrameGraphWriteFlags> pair : pass.writes)
			{
				Detail::FrameGraphAlias& alias = aliases[sizeT(pair.first)];
				if (alias.isImported)
					continue;
				if (alias.transientIdx >= transientRanges.Size())
					transientRanges.Resize(alias.transientIdx + 1);

				Detail::FrameGraphTransientResourceRange& transient = transientRanges[alias.transientIdx];
				if (!transient.initialized)
				{
					transient.isBuffer = alias.isBuffer;
					transient.descIdx = alias.descIdx;
					transient.firstPass = transient.lastPass = idx;
					transient.initialized = true;
				}
				else
				{
					transientRanges[alias.transientIdx].lastPass = idx;
				}
			}
		}

		// Convert transient ranges to transient resources
		DynArray<Detail::FrameGraphTransientResource> transientResources;
		DynArray<u32> transientIndices;
		for (Detail::FrameGraphTransientResourceRange& range : transientRanges)
		{
			if (!range.initialized)
			{
				transientIndices.Push(0xFFFF);
				continue;
			}

			b8 found = false;
			for (u32 i = 0; i < transientResources.Size(); ++i)
			{
				Detail::FrameGraphTransientResource& resource = transientResources[i];
				// Check if other resource could be reused
				if (resource.lastPass < range.firstPass && resource.isBuffer == range.isBuffer)
				{
					if (range.isBuffer)
					{
						FrameGraphBufferDesc rangeDesc = m_pRegistry->GetBufferDescription(range.descIdx);
						FrameGraphBufferDesc resDesc = m_pRegistry->GetBufferDescription(resource.descIdx);

						if (rangeDesc.size == resDesc.size &&
							rangeDesc.texelFormat == resDesc.texelFormat)
						{
							transientIndices.Push(i);
							found = true;
							break;
						}
					}
					else
					{
						FrameGraphTextureDesc rangeDesc = m_pRegistry->GetTextureDescription(range.descIdx);
						FrameGraphTextureDesc resDesc = m_pRegistry->GetTextureDescription(resource.descIdx);
						if (rangeDesc.width == resDesc.width &&
							rangeDesc.height == resDesc.height &&
							rangeDesc.format == resDesc.format)
						{
							transientIndices.Push(i);
							found = true;
							break;
						}
					}
				}
			}
			if (found)
				continue;

			// No resource can be reused, so create new one
			Detail::FrameGraphTransientResource transient = {};
			transient.isBuffer = range.isBuffer;
			transient.descIdx = range.descIdx;
			transient.lastPass = range.lastPass;
			transientIndices.Push(u32(transientResources.Size()));
			transientResources.Push(transient);
		}

		// Update resource indices
		for (Detail::FrameGraphAlias& alias : aliases)
		{
			// Only set resIdx when transient resource is used or created
			if (alias.transientIdx < transientIndices.Size())
				alias.resIdx = transientIndices[alias.transientIdx];
			else
				alias.resIdx = 0xFFFF;
		}

		// Assign physical resources to transient ones
		m_pResources->Reset();
		for (Detail::FrameGraphTransientResource& transient : transientResources)
		{
			if (transient.isBuffer)
			{
				FrameGraphBufferDesc& desc = m_pRegistry->GetBufferDescription(transient.descIdx);
				m_pResources->AddResource(desc);
			}
			else
			{
				FrameGraphTextureDesc& desc = m_pRegistry->GetTextureDescription(transient.descIdx);
				m_pResources->AddResource(desc);
			}
		}
	}

	void FrameGraph::Execute()
	{
		Core::Semaphore* pSemaphore = nullptr;

		for (u32 idx : m_CulledPasses)
		{
			Detail::FrameGraphPass& pass = m_Passes[idx];
			PassContext* pContext = pass.pContext;

			pContext->Reset();
			Core::CommandList* pCommandList = pContext->GetCommandList();

			pCommandList->Begin();

			// Transition resources
			b8 depthTransitioned = false;

			// Transition read textures (color -> shader read, depth -> depth)
			for (Pair<FrameGraphResource, FrameGraphReadFlags> pair : pass.reads)
			{
				Detail::FrameGraphAlias& alias = m_pRegistry->GetAlias(pair.first);
				if (alias.isBuffer || alias.isImported)
					continue;

				if (pair.second == FrameGraphReadFlags::AsDepthStencil)
					depthTransitioned = true;

				Core::Texture* pTexture = m_pResources->GetTexture(pair.first);
				TextureLayoutTransition transition = {};
				transition.layout = (pair.second == FrameGraphReadFlags::AsColor) ? TextureLayout::ShaderReadOnly : TextureLayout::DepthStencil;
				transition.baseArrayLayer = 0;
				transition.layerCount = 1;
				transition.baseMipLevel = 0;
				transition.mipLevelCount = 1;
				pCommandList->TransitionTextureLayout(PipelineStage::Transfer, PipelineStage::TopOfPipe, pTexture, transition);
			}

			// Transition write textures (color -> color, depth -> depth)
			for (Pair<FrameGraphResource, FrameGraphWriteFlags> pair : pass.writes)
			{
				Detail::FrameGraphAlias& alias = m_pRegistry->GetAlias(pair.first);
				if (alias.isBuffer || alias.isImported || (pair.second == FrameGraphWriteFlags::AsDepthStencil && depthTransitioned))
					continue;

				Core::Texture* pTexture = m_pResources->GetTexture(pair.first);
				TextureLayoutTransition transition = {};
				transition.layout = (pair.second == FrameGraphWriteFlags::AsColor) ? TextureLayout::ColorAttachment : TextureLayout::DepthStencil;
				transition.baseArrayLayer = 0;
				transition.layerCount = 1;
				transition.baseMipLevel = 0;
				transition.mipLevelCount = 1;
				pCommandList->TransitionTextureLayout(PipelineStage::Transfer, PipelineStage::TopOfPipe, pTexture, transition);
			}

			pass.pPass->Draw(*m_pResources, *pContext);

			pCommandList->EndRenderPass();
			pCommandList->End();

			pCommandList->Submit(/**/);
		}

		// Go over all command lists and check if they are finished executing
		for (u32 idx : m_CulledPasses)
		{
			Detail::FrameGraphPass& pass = m_Passes[idx];
			Core::CommandList* pCmdList = pass.pContext->GetCommandList();

			pCmdList->Wait();
		}
	}

	void FrameGraph::ExportGraphViz(const String& filepath)
	{
		HvFS::Path path = filepath;
		HvFS::File file = HvFS::CreateFile(path, HvFS::FileMode::Create, HvFS::AccessMode::Write);

		file.WriteLine("digraph framegraph \n{");
		file.WriteLine("\trankdir = LR");
		file.WriteLine("\tnode [shape = rectangle, font = \"helvatica\", fontsize = 12]");

		for (u32 i = 0; i < m_Passes.Size(); ++i)
		{
			u32 id = 0xFFFF'FFFF;
			const u32* it = m_CulledPasses.Find(i);
			if (it != m_CulledPasses.Back())
			{
				id = u32(it - m_CulledPasses.Front()) / sizeof(u32);
			}

			Detail::FrameGraphPass& pass = m_Passes[i];
			AnsiString label = pass.identifier + "\\nRefs: " + ToString(pass.refCount);
			if (id == 0xFFFF'FFFF)
				label += "\\nCulled";
			else
				label += "\\nID: " + ToString(i);

			AnsiString str = "\t\"" + pass.identifier + "\" [label = \"" + label + "\", style = \"filled, rounded\", fillcolor = darkorange]";
			file.WriteLine(str);
		}

		DynArray<Detail::FrameGraphAlias>& aliases = m_pRegistry->GetAliases();
		for (sizeT i = 0; i < aliases.Size(); ++i)
		{
			Detail::FrameGraphAlias& alias = aliases[i];
			FrameGraphResource resource = (FrameGraphResource)i;
			String label = m_pRegistry->GetIdentifier(resource);
			if (alias.isBuffer)
			{
				FrameGraphBufferDesc& desc = m_pRegistry->GetBufferDescription(resource);
				label += "\nSize: " + ToString(desc.size);
			}
			else
			{
				FrameGraphTextureDesc& desc = m_pRegistry->GetTextureDescription(resource);
				label += "\\nFormat: " + desc.format.ToString();
				label += "\\n" + ToString(desc.width) + "x" + ToString(desc.height);
			}
			label += "\\nRefs: " + ToString(alias.refCount);

			AnsiString str = "\t\"res" + ToString(i) + "\" [label = \"" + label + "\", style = filled, fillcolor = " + (aliases[i].isImported ? "steelblue" : "skyblue") + "]";
			file.WriteLine(str);
		}

		for (Detail::FrameGraphPass& pass : m_Passes)
		{
			AnsiString str = "\t\"" + pass.identifier + "\" -> {";
			file.WriteLine(str);
			for (Pair<FrameGraphResource, FrameGraphWriteFlags>& pair : pass.writes)
			{
				str = "\t\t\"res" + ToString(u64(pair.first)) + "\"";
				file.WriteLine(str);
			}
			file.WriteLine("\t} [color = firebrick]");

			for (Pair<FrameGraphResource, FrameGraphReadFlags>& pair : pass.reads)
			{
				str = "\t\"res" + ToString(u64(pair.first)) + "\" -> \"" + pass.identifier + "\" [color = seagreen]";
				file.WriteLine(str);
			}

			for (FrameGraphResource resource : pass.creates)
			{
				str = "\t\"res" + ToString(u64(resource)) + "\" -> \"" + pass.identifier + "\" [color = darkturquoise]";
				file.WriteLine(str);
			}
		}

		file.WriteLine("}");

		file.Close();
	}

	Core::RenderPass* FrameGraph::GetRenderPass(const String& iden)
	{
		auto it = m_IdenMapping.Find(iden);
		if (it == m_IdenMapping.Back())
			return nullptr;

		Detail::FrameGraphPass& pass = m_Passes[it->second];
		return pass.pRenderPass;
	}
}
