// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Renderer.h: Renderer
#include "RendererPCH.h"
#include "Renderer/Renderer.h"
#include "Renderer/FrameGraph.h"
#include "Renderer/Material/MaterialInstance.h"

HV_DECLARE_LOG_CATEGORY(Renderer, Hv::LogLevel::All);

namespace Hv::Renderer::TestPasses {

	class ZPrePass : public Pass
	{
	public:
		ZPrePass() : Pass("ZPrePass") {}
		~ZPrePass() {}

		void Setup(FrameGraphBuilder& builder) override final
		{
			FrameGraphTextureDesc desc = {};
			desc.width = 1024;
			desc.height = 1024;
			desc.format = PixelFormat(PixelFormatComponents::D24, PixelFormatTransform::UNORM);
			desc.initialState = FrameGraphResourseState::Clear;

			depth = builder.Create("depth_buffer", desc);
			builder.Write(depth, FrameGraphWriteFlags::AsDepthStencil);
		}

		void Draw(FrameGraphResources& resources, PassContext& context) override final
		{
			context.SetDepthStencil(resources.GetRenderTarget(depth));
			context.BindRenderTargets();

			// Rendering here
		}

		FrameGraphResource depth = InvalidFrameGraphResource;
	};

	class GBufferPass : public Pass
	{
	public:
		GBufferPass() : Pass("GBufferPass") {}
		~GBufferPass() {}

		void Setup(FrameGraphBuilder& builder) override final
		{
			FrameGraphTextureDesc desc = {};
			desc.width = 1024;
			desc.height = 1024;
			desc.format = PixelFormat(PixelFormatComponents::R8G8B8A8, PixelFormatTransform::UNORM);
			desc.initialState = FrameGraphResourseState::Clear;

			color = builder.Create("color_rt", desc);
			builder.Write(color, FrameGraphWriteFlags::AsColor);

			desc.format = PixelFormat(PixelFormatComponents::R11G11B10, PixelFormatTransform::UFLOAT);
			normals = builder.Create("normal_rt", desc);
			builder.Write(normals, FrameGraphWriteFlags::AsColor);

			desc.format = PixelFormat(PixelFormatComponents::R8G8, PixelFormatTransform::UNORM);
			pbr = builder.Create("pbr_rt", desc);
			builder.Write(pbr, FrameGraphWriteFlags::AsColor);

			builder.Read("depth_buffer", FrameGraphReadFlags::AsDepthStencil);
		}

		void Draw(FrameGraphResources& resources, PassContext& context) override final
		{

		}

		FrameGraphResource depth = InvalidFrameGraphResource;
		FrameGraphResource color = InvalidFrameGraphResource;
		FrameGraphResource normals = InvalidFrameGraphResource;
		FrameGraphResource pbr = InvalidFrameGraphResource;
	};

	class FinalPass : public Pass
	{
	public:
		FinalPass() : Pass("FinalPass") {}
		~FinalPass() {}

		void Setup(FrameGraphBuilder& builder) override final
		{
			FrameGraphTextureDesc desc = {};
			desc.width = 1024;
			desc.height = 1024;
			desc.format = PixelFormat(PixelFormatComponents::R8G8B8A8, PixelFormatTransform::UNORM);
			desc.initialState = FrameGraphResourseState::Clear;

			out = builder.Import("out_buffer", desc, true);
			builder.Write(out, FrameGraphWriteFlags::AsColor);

			builder.Read("color_rt", FrameGraphReadFlags::None);
			builder.Read("normal_rt", FrameGraphReadFlags::None);
			builder.Read("pbr_rt", FrameGraphReadFlags::None);
			builder.Read("filtered_ao_rt", FrameGraphReadFlags::None);
		}

		void Draw(FrameGraphResources& resources, PassContext& context) override final
		{

		}

		FrameGraphResource out = InvalidFrameGraphResource;
	};

	class AOPass : public Pass
	{
	public:
		AOPass() : Pass("AOPass") {}
		~AOPass() {}

		void Setup(FrameGraphBuilder& builder) override final
		{
			FrameGraphTextureDesc desc = {};
			desc.width = 1024;
			desc.height = 1024;
			desc.format = PixelFormat(PixelFormatComponents::R8G8B8A8, PixelFormatTransform::UNORM);
			desc.initialState = FrameGraphResourseState::Clear;

			ao = builder.Create("ao_rt", desc);
			builder.Write(ao, FrameGraphWriteFlags::AsColor);

			builder.Read("depth_buffer", FrameGraphReadFlags::None);
		}

		void Draw(FrameGraphResources& resources, PassContext& context) override final
		{

		}

		FrameGraphResource ao = InvalidFrameGraphResource;
	};

	class AOFilterPass : public Pass
	{
	public:
		AOFilterPass() : Pass("AOFilterPass") {}
		~AOFilterPass() {}

		void Setup(FrameGraphBuilder& builder) override final
		{
			FrameGraphTextureDesc desc = {};
			desc.width = 1024;
			desc.height = 1024;
			desc.format = PixelFormat(PixelFormatComponents::R8G8B8A8, PixelFormatTransform::UNORM);
			desc.initialState = FrameGraphResourseState::Clear;

			ao = builder.Create("filtered_ao_rt", desc);
			builder.Write(ao, FrameGraphWriteFlags::AsColor);

			builder.Read("ao_rt", FrameGraphReadFlags::None);
		}

		void Draw(FrameGraphResources& resources, PassContext& context) override final
		{

		}

		FrameGraphResource ao = InvalidFrameGraphResource;
	};

	class UselessPass : public Pass
	{
	public:
		UselessPass() : Pass("UselessPass") {}
		~UselessPass() {}

		void Setup(FrameGraphBuilder& builder) override final
		{
			FrameGraphTextureDesc desc = {};
			desc.width = 1024;
			desc.height = 1024;
			desc.format = PixelFormat(PixelFormatComponents::R8G8B8A8, PixelFormatTransform::SFLOAT);
			desc.initialState = FrameGraphResourseState::Clear;

			depth = builder.Create("useless_rt", desc);
			builder.Write(depth, FrameGraphWriteFlags::AsColor);

			builder.Read("color_rt", FrameGraphReadFlags::None);
			builder.Read("normal_rt", FrameGraphReadFlags::None);
			builder.Read("pbr_rt", FrameGraphReadFlags::None);
		}

		void Draw(FrameGraphResources& resources, PassContext& context) override final
		{

		}

		FrameGraphResource depth = InvalidFrameGraphResource;
	};

	class SimpleForwardPass : public Pass
	{
	public:
		SimpleForwardPass() : Pass("MainPass") {}
		~SimpleForwardPass() {}

		void Setup(FrameGraphBuilder& builder) override final
		{
			FrameGraphTextureDesc desc = {};
			desc.width = 1024;
			desc.height = 1024;
			desc.format = PixelFormat(PixelFormatComponents::D24, PixelFormatTransform::UNORM);
			desc.initialState = FrameGraphResourseState::Clear;

			depth = builder.Create("depth_buffer", desc);
			builder.Write(depth, FrameGraphWriteFlags::AsDepthStencil);

			desc.format = PixelFormat(PixelFormatComponents::R8G8B8A8, PixelFormatTransform::UNORM);
			color = builder.Import("back_buffer", desc, true);
			builder.Write(color, FrameGraphWriteFlags::AsColor);
		}

		void Draw(FrameGraphResources& resources, PassContext& context) override final
		{
			context.SetDepthStencil(resources.GetRenderTarget(depth));

			// Get back buffer
			Core::SwapChain* pRV = g_Renderer.GetSwapChain(0);
			if (!pRV)
				return;

			pBackBuffer = pRV->GetCurrentRenderTarget();

			context.AddRenderTarget(pBackBuffer);
			context.BindRenderTargets();

			auto& renderables = g_Renderer.GetRenderables();

			View& view = g_Renderer.GetMainView();
			b8 viewSet = false;
			Core::DescriptorSet* pTransformDescriptorSet = g_Renderer.GetTransformDescriptorSet();

			// Loop over materials
			for (auto& matPair : renderables)
			{
				context.SetMaterial(matPair.first);

				if (!viewSet)
				{
					context.SetView(view);
					viewSet = true;
				}

				// Loop over material instances
				for (auto& instPair : matPair.second)
				{
					context.SetMaterialInstance(instPair.first);

					for (MeshData data : instPair.second)
					{
						context.DrawMesh(data.mesh, pTransformDescriptorSet, data.transformOffset);
					}
				}
			}
		}

		FrameGraphResource depth = InvalidFrameGraphResource;
		FrameGraphResource color = InvalidFrameGraphResource;
		Core::RenderTarget* pBackBuffer = nullptr;
	};
}

namespace Hv::Renderer {


	Renderer::Renderer()
		: m_Desc()
		, m_RHIDynLib(DynLib::InvalidDynLibHandle)
		, m_pRHI(nullptr)
		, m_pFrameGraph(nullptr)
		, m_pResourceManager(nullptr)
		, m_pShaderManager(nullptr)
		, m_pCommandListManager(nullptr)
		, m_pDescriptorSetManager(nullptr)
	{
	}

	Renderer::~Renderer()
	{
	}

	b8 Renderer::Init(const RendererDesc& desc)
	{
		m_Desc = desc;

		g_Logger.LogInfo(LogRenderer(), "Initializing renderer!");

		// Load DynLib
		FileSystem::Path rhiPath("$bin");

		// On fail, look for fallback
		switch (m_Desc.rhiType)
		{
		case Hv::Renderer::RHIType::OpenGL:
			g_Logger.LogError("OpenGL RHI unsupported!");
			return false;
		case Hv::Renderer::RHIType::DirectX11:
			g_Logger.LogError("DirectX11 RHI unsupported!");
			return false;
		case Hv::Renderer::RHIType::DirectX12:
			g_Logger.LogError("DirectX12 RHI unsupported!");
			return false;
		case Hv::Renderer::RHIType::Vulkan:
			rhiPath /= "HiveVulkanRHI";
			break;
		default:
			g_Logger.LogError("Unknown RHI type!");
			return false;
		}

		rhiPath += DynLib::g_Extension;
		rhiPath = FileSystem::ResolvePath(rhiPath);
		m_RHIDynLib = DynLib::Load(rhiPath.ToString());
		if (m_RHIDynLib == DynLib::InvalidDynLibHandle)
		{
			g_Logger.LogFormat(LogRenderer(), LogLevel::Fatal, "Failed to load RHI dynamic library (path: %s)", rhiPath.ToString());
			return false;
		}

		RHI::LoadDynamicRHIFunc fLoadRHI = (RHI::LoadDynamicRHIFunc)DynLib::GetProc(m_RHIDynLib, "LoadDynamicRHI");
		if ((DynLib::DynLibProcHandle)fLoadRHI == DynLib::InvalidDynLibProcHandle)
		{
			g_Logger.LogFatal(LogRenderer(), "Failed to retrieve LoadDynamicRHI() procedure!");
			Shutdown();
			return false;
		}

		m_pRHI = fLoadRHI();
		if (!m_pRHI)
		{
			g_Logger.LogFatal(LogRenderer(), "Failed to load RHI!");
			HvDelete m_pRHI;
			m_pRHI = nullptr;
			Shutdown();
			return false;
		}

		RHI::RHIDesc rhiDesc = {};
		rhiDesc.validationLevel = desc.rhiValidationLevel;

		// Queue info (currently: 1 graphics, 1 compute and 1 transfer/sparse)
		QueueInfo queueInfo = {};
		queueInfo.count = 1;
		queueInfo.priority = QueuePriority::High;
		queueInfo.type = QueueType::Graphics;
		rhiDesc.queueInfo.Push(queueInfo);
		queueInfo.type = QueueType::Compute;
		rhiDesc.queueInfo.Push(queueInfo);
		queueInfo.type = QueueType::Transfer;
		rhiDesc.queueInfo.Push(queueInfo);

		b8 res = m_pRHI->Init(rhiDesc);
		if (!res)
		{
			g_Logger.LogFatal(LogRenderer(), "Failed to initialize the RHI. No fallback detected!");
			// Delete RHI here, since RHI will already have been destroyed, so no need to let it happen again in Shutdown()
			HvDelete m_pRHI;
			m_pRHI = nullptr;
			Shutdown();
			return false;
		}

		// Get command list and descriptor set manager
		m_pCommandListManager = m_pRHI->GetCommandListManager();
		m_pDescriptorSetManager = m_pRHI->GetDescriptorSetManager();

		m_pResourceManager = HvNew ResourceManager();
		m_pResourceManager->Create(m_pRHI, m_pCommandListManager);

		m_pShaderManager = HvNew ShaderManager();
		m_pShaderManager->Create(m_pRHI);

		m_pMaterialManager = HvNew MaterialManager();
		m_pMaterialManager->Create(m_pRHI, m_pResourceManager);

		// Create framegraph
		m_pFrameGraph = HvNew FrameGraph(m_pRHI, m_pResourceManager);

		// TEMP

		/*m_pFrameGraph->AddPass(HvNew TestPasses::ZPrePass());
		m_pFrameGraph->AddPass(HvNew TestPasses::GBufferPass());
		m_pFrameGraph->AddPass(HvNew TestPasses::AOPass());
		m_pFrameGraph->AddPass(HvNew TestPasses::AOFilterPass());
		m_pFrameGraph->AddPass(HvNew TestPasses::FinalPass());
		m_pFrameGraph->AddPass(HvNew TestPasses::UselessPass());*/

		m_pFrameGraph->AddPass(HvNew TestPasses::SimpleForwardPass);

		m_pFrameGraph->Initialize();
		m_pFrameGraph->Compile();

		m_pFrameGraph->ExportGraphViz("FrameGraph.gv");

		m_View.viewport.width = m_View.viewport.height = 1024;
		m_View.viewport.maxDepth = 1;
		m_View.scissor.width = m_View.scissor.height = 1024;
		m_View.projection = f32m4::CreatePerspective(Math::ToRadians(60.f), 1.f, 0.001f, 1000.f);
		m_View.view = f32m4::CreateLookat(f32v3(-1, 1, -2), f32v3::Zero, f32v3::Up);

		if (m_Desc.rhiType == RHIType::Vulkan)
			m_View.projection *= Hm::VulkanCorrection;

		// TODO: Figure out better way for layouts
		Core::DescriptorSetManager* pDescriptorSetManager = m_pRHI->GetDescriptorSetManager();
		// Frame bindings
		DynArray<Core::DescriptorSetBinding> bindings;
		bindings.Push({ DescriptorSetBindingType::Uniform, ShaderType::Vertex | ShaderType::Fragment, 1 });
		m_pFrameLayout = pDescriptorSetManager->CreateDescriptorSetLayout(bindings);

		// View bindings
		bindings.Clear();
		bindings.Push({ DescriptorSetBindingType::Uniform, ShaderType::Vertex | ShaderType::Fragment, 1 });
		m_pViewLayout = pDescriptorSetManager->CreateDescriptorSetLayout(bindings);

		// Mesh bindings
		bindings.Clear();
		bindings.Push({ DescriptorSetBindingType::DynamicUniformBuffer, ShaderType::Vertex | ShaderType::Fragment, 1 });
		m_pObjectLayout = pDescriptorSetManager->CreateDescriptorSetLayout(bindings);

		m_View.pDescriptorSet = m_pDescriptorSetManager->CreateDescriptorSet(m_pViewLayout);
		m_View.pBuffer = m_pResourceManager->CreateBuffer(BufferType::Uniform, 2 * sizeof(f32m4), BufferFlags::None);
		m_View.pDescriptorSet->Write(0, m_View.pBuffer);
		m_View.pBuffer->Write(0, sizeof(f32m4), &m_View.projection);
		m_View.pBuffer->Write(sizeof(f32m4), sizeof(f32m4), &m_View.view);

		m_pTransformBuffer = m_pResourceManager->CreateBuffer(BufferType::Uniform, sizeof(f32m4) * MaxRenderedObjects, BufferFlags::Dynamic);
		bindings.Clear();
		m_pTransformDescriptorLayout = m_pObjectLayout;
		m_pTransformDescriptorSet = m_pDescriptorSetManager->CreateDescriptorSet(m_pTransformDescriptorLayout);
		m_pTransformDescriptorSet->Write(0, m_pTransformBuffer);

		// END TEMP

		g_Logger.LogInfo(LogRenderer(), "Renderer initialized successfully!");
		return true;
	}

	b8 Renderer::Shutdown()
	{
		g_Logger.LogInfo(LogRenderer(), "Shutting down renderer!");

		m_pRHI->WaitIdle();

		b8 res = true;
		// Destroy all objects

		// Manager destroyed by context
		m_pCommandListManager = nullptr;
		m_pDescriptorSetManager = nullptr;

		// Framebuffers
		for (Core::Framebuffer* pFramebuffer : m_Framebuffers)
		{
			res &= m_pRHI->DestroyFramebuffer(pFramebuffer);
		}
		m_Framebuffers.Clear();

		// Pipelines
		for (Core::Pipeline* pPipeline : m_Pipelines)
		{
			res &= m_pRHI->DestroyPipeline(pPipeline);
		}
		m_Pipelines.Clear();

		// Render passes
		for (Core::RenderPass* pRenderPass : m_RenderPasses)
		{
			res &= m_pRHI->DestroyRenderPass(pRenderPass);
		}
		m_RenderPasses.Clear();

		// Samplers
		for (Core::Sampler* pSampler : m_Samplers)
		{
			res &= m_pRHI->DestroySampler(pSampler);
		}
		m_Samplers.Clear();

		res &= m_pResourceManager->Destroy();
		res &= m_pShaderManager->Destroy();
		res &= m_pMaterialManager->Destroy();

		HvDelete m_pResourceManager;
		HvDelete m_pShaderManager;
		HvDelete m_pMaterialManager;

		// Unregister all windows
		while (m_SwapChains.Size() > 0)
		{
			res &= UnregisterWindow(m_SwapChains.Front()->first);
		}

		if (m_pRHI)
		{
			res &= m_pRHI->Destroy();
			HvDelete m_pRHI;
			m_pRHI = nullptr;
		}

		if (m_RHIDynLib != DynLib::InvalidDynLibHandle)
		{
			DynLib::Unload(m_RHIDynLib);
			m_RHIDynLib = DynLib::InvalidDynLibHandle;
		}

		g_Logger.LogInfo(LogRenderer(), "Renderer shut down successfully!");

		return res;
	}

	void Renderer::Draw()
	{
		if (!g_WindowManager.IsAnyWindowOpen())
			return;

		m_pFrameGraph->Compile();
		m_pFrameGraph->Execute();

		for (Pair<unsigned short, Core::SwapChain*> pair : m_SwapChains)
		{
			pair.second->Present();
		}

		// Temp
		// Clear renderables before next frame
		for (auto& matPair : m_Renderables)
		{
			for (auto& instPair : matPair.second)
			{
				instPair.second.Clear();
			}
		}
		m_NumRenderables = 0;
	}

	void Renderer::RegisterMesh(MeshHandle mesh)
	{
		u64 id = mesh.GetId();
		if (!mesh.IsValid())
			return;

		if (m_MeshBuffers.Size() > id && m_MeshBuffers[id].first)
		{
			m_pResourceManager->DestroyBuffer(m_MeshBuffers[id].first);
			m_pResourceManager->DestroyBuffer(m_MeshBuffers[id].second);
		}

		DynArray<Vertex>& vertices = mesh->GetVertices();
		Core::Buffer* pVertexBuffer = m_pResourceManager->CreateVertexBuffer(u32(vertices.Size()), sizeof(Vertex), BufferFlags::None);
		pVertexBuffer->Write(0, vertices.Size() * sizeof(Vertex), vertices.Data());

		DynArray<u32>& indices = mesh->GetIndices();
		Core::Buffer* pIndexBuffer = m_pResourceManager->CreateIndexuffer(u32(indices.Size()), IndexType::UInt, BufferFlags::None);
		pIndexBuffer->Write(0, indices.Size() * sizeof(u32), indices.Data());

		Pair<Core::Buffer*, Core::Buffer*> buffers = Pair<Core::Buffer*, Core::Buffer*>(pVertexBuffer, pIndexBuffer);

		if (m_MeshBuffers.Size() <= id)
			m_MeshBuffers.Resize(id + 1);

		m_MeshBuffers[id] = buffers;
	}

	void Renderer::AddRenderable(f32m4& transform, MaterialInstanceHandle materialInstance, MeshHandle mesh)
	{
		if (!materialInstance.IsValid())
			return;

		MaterialHandle material = materialInstance->GetMaterial();

		if (m_Renderables.Size() <= material.GetId())
			m_Renderables.Resize(u32(material.GetId() + 1), Pair<MaterialHandle, DynArray<Pair<MaterialInstanceHandle, DynArray<MeshData>>>>());

		auto& matEntry = m_Renderables[material.GetId()];
		matEntry.first = material;

		auto& instances = matEntry.second;

		if (instances.Size() <= materialInstance.GetId())
			instances.Resize(u32(materialInstance.GetId() + 1), Pair<MaterialInstanceHandle, DynArray<MeshData>>());

		Pair<MaterialInstanceHandle, DynArray<MeshData>>& instEntry = instances[materialInstance.GetId()];
		instEntry.first = materialInstance;

		DynArray<MeshData>& meshes = instEntry.second;
		MeshData data;
		data.mesh = mesh;
		data.transformOffset = m_NumRenderables * sizeof(f32m4);
		meshes.Push(data);

		m_pTransformBuffer->Write(data.transformOffset, sizeof(f32m4), &transform);
		++m_NumRenderables;
	}

	b8 Renderer::RegisterWindow(Window* pWindow)
	{
		return RegisterWindow(pWindow, m_Desc.defaultVSyncMode);
	}

	b8 Renderer::RegisterWindow(Window* pWindow, VSyncMode vsync)
	{
		u16 id = pWindow->GetId();
		Core::SwapChain* pRenderView = m_pRHI->CreateRenderView(pWindow, vsync);
		if (!pRenderView)
		{
			g_Logger.LogFatal(LogRenderer(), "Failed to register a window (renderview creation)!");
			return false;
		}
		auto res = m_SwapChains.Insert(id, pRenderView);
		if (!res.second)
		{
			g_Logger.LogFatal(LogRenderer(), "Failed to register a window (adding to hashmap)!");
			return false;
		}

		// Set callbacks
		pWindow->SetOnRendererUnregisterCallback(Delegate<b8(u16)>(this, &Renderer::UnregisterWindow));

		return true;
	}

	b8 Renderer::UnregisterWindow(Window* pWindow)
	{
		u16 id = pWindow->GetId();
		return UnregisterWindow(id);
	}

	b8 Renderer::UnregisterWindow(u16 id)
	{
		auto it = m_SwapChains.Find(id);
		if (it == m_SwapChains.Back())
		{
			g_Logger.LogWarning(LogRenderer(), "Trying to unregister unknown window (incorrect id or window alread unregistered?)!");
			return true;
		}
		b8 res = m_pRHI->DestroyRenderView(it->second);
		if (!res)
		{
			g_Logger.LogWarning(LogRenderer(), "Failed to unregister a window (may cause issues later during runtime)!");
			return false;
		}

		m_SwapChains.Erase(it);
		return true;
	}

	Core::SwapChain* Renderer::GetSwapChain(u16 windowId)
	{
		auto it = m_SwapChains.Find(windowId);
		if (it == m_SwapChains.Back())
			return nullptr;
		return it->second;
	}

	////////////////////////////////////////////////////////////////////////////////
	// Core (Allow plugins and apps to add things to renderer)					  //
	////////////////////////////////////////////////////////////////////////////////
	Core::Shader* Renderer::CoreAddShader(const Core::ShaderDesc& desc)
	{
		return m_pShaderManager->LoadShader(desc);
	}

	b8 Renderer::CoreRemoveShader(Core::Shader* pShader)
	{
		return m_pShaderManager->UnloadShader(pShader);
	}

	Core::RenderPass* Renderer::CoreAddRenderPass(const DynArray<Core::RenderPassAttachment>& attachments,
		const DynArray<Core::SubRenderPass>& subpasses)
	{
		Core::RenderPass* pRenderPass = m_pRHI->CreateRenderPass(attachments, subpasses);
		if (!pRenderPass)
		{
			g_Logger.LogError(LogRenderer(), "Failed to add a render pass to the renderer");
			return nullptr;
		}
		m_RenderPasses.Push(pRenderPass);
		return pRenderPass;
	}

	b8 Renderer::CoreRemoveRenderPass(Core::RenderPass* pRenderPass)
	{
		Core::RenderPass** it = m_RenderPasses.Find(pRenderPass);
		if (it == m_RenderPasses.Back())
		{
			g_Logger.LogWarning("Failed to remove a render pass from the renderer");
			return false;
		}
		b8 res = m_pRHI->DestroyRenderPass(*it);
		m_RenderPasses.Erase(it);

		return res;
	}

	Core::Pipeline* Renderer::CoreAddPipeline(const Core::GraphicsPipelineDesc& desc)
	{
		Core::Pipeline* pPipeline = m_pRHI->CreatePipeline(desc);
		if (!pPipeline)
		{
			g_Logger.LogError(LogRenderer(), "Failed to add a graphics pipeline to the renderer");
			return nullptr;
		}
		m_Pipelines.Push(pPipeline);
		return pPipeline;
	}

	Core::Pipeline* Renderer::CoreAddPipeline(const Core::ComputePipelineDesc& desc)
	{
		Core::Pipeline* pPipeline = m_pRHI->CreatePipeline(desc);
		if (!pPipeline)
		{
			g_Logger.LogError(LogRenderer(), "Failed to add a compute pipeline to the renderer");
			return nullptr;
		}
		m_Pipelines.Push(pPipeline);
		return pPipeline;
	}

	b8 Renderer::CoreRemovePipeline(Core::Pipeline* pPipeline)
	{
		Core::Pipeline** it = m_Pipelines.Find(pPipeline);
		if (it == m_Pipelines.Back())
		{
			g_Logger.LogWarning("Failed to remove a pipeline from the renderer");
			return false;
		}
		b8 res = m_pRHI->DestroyPipeline(*it);
		m_Pipelines.Erase(it);

		return res;
	}

	Core::Framebuffer* Renderer::CoreAddFramebuffer(const DynArray<Core::RenderTarget*>& renderTargets, Core::RenderPass* pRenderPass)
	{
		Core::Framebuffer* pFramebuffer = m_pRHI->CreateFramebuffer(renderTargets, pRenderPass);
		if (!pFramebuffer)
		{
			g_Logger.LogError(LogRenderer(), "Failed to add a framebuffer to the renderer");
			return nullptr;
		}
		m_Framebuffers.Push(pFramebuffer);
		return pFramebuffer;
	}

	b8 Renderer::CoreRemoveFramebuffer(Core::Framebuffer* pFramebuffer)
	{
		Core::Framebuffer** it = m_Framebuffers.Find(pFramebuffer);
		if (it == m_Framebuffers.Back())
		{
			g_Logger.LogWarning("Failed to remove a framebuffer from the renderer");
			return false;
		}
		b8 res = m_pRHI->DestroyFramebuffer(*it);
		m_Framebuffers.Erase(it);

		return res;
	}

	Core::CommandList* Renderer::CoreAddCommandList(Core::Queue* pQueue)
	{
		return m_pCommandListManager->CreateCommandList(pQueue);
	}

	b8 Renderer::CoreRemoveCommandList(Core::CommandList* pCommandList)
	{
		return m_pCommandListManager->DestroyCommandList(pCommandList);
	}

	Core::Sampler* Renderer::CoreAddSampler(const Core::SamplerDesc& samplerDesc)
	{
		Core::Sampler* pSampler = m_pRHI->CreateSampler(samplerDesc);
		if (!pSampler)
		{
			g_Logger.LogError(LogRenderer(), "Failed to add a sampler to the renderer");
			return nullptr;
		}
		m_Samplers.Push(pSampler);
		return pSampler;
	}

	b8 Renderer::CoreRemoveSampler(Core::Sampler* pSampler)
	{
		Core::Sampler** it = m_Samplers.Find(pSampler);
		if (it == m_Samplers.Back())
		{
			g_Logger.LogWarning("Failed to remove a sampler from the renderer");
			return false;
		}
		b8 res = m_pRHI->DestroySampler(*it);
		m_Samplers.Erase(it);

		return res;
	}

	Core::Texture* Renderer::CoreAddTexture(const Core::TextureDesc& desc, QueueType owningQueueType)
	{
		return m_pResourceManager->CreateTexture(desc, owningQueueType);
	}

	Core::Texture* Renderer::CoreAddTexture(const Core::TextureDesc& desc, QueueType owningQueueType, Image* pImage)
	{
		DynArray<u8>& data = pImage->GetData();
		return m_pResourceManager->CreateTexture(desc, owningQueueType, data.Data(), u64(data.Size()));
	}

	Core::Texture* Renderer::CoreAddTexture(const Core::TextureDesc& desc, QueueType owningQueueType, u8* pData, u64 dataSize)
	{
		return m_pResourceManager->CreateTexture(desc, owningQueueType, pData, dataSize);
	}

	Core::Texture* Renderer::CoreAddTexture(Image* pImage, SampleCount samples, TextureFlags flags, TextureLayout layout, QueueType owningQueueType)
	{
		Core::TextureDesc desc = {};
		desc.width = pImage->GetWidth();
		desc.height = pImage->GetHeight();
		desc.depth = pImage->GetDepth();
		desc.layerCount = pImage->GetLayerCount();
		desc.mipLevels = pImage->GetMipLevels();
		desc.format = pImage->GetFormat();
		desc.samples = samples;
		desc.flags = flags;

		if (pImage->IsCubemap())
		{
			if (desc.layerCount > 1)
				desc.type = TextureType::CubemapArray;
			else
				desc.type = TextureType::Cubemap;
		}
		else if (desc.layerCount > 1)
		{
			if (desc.height > 1)
				desc.type = TextureType::Tex2DArray;
			else
				desc.type = TextureType::Tex1DArray;
		}
		else
		{
			if (desc.depth > 1)
				desc.type = TextureType::Tex3D;
			else if (desc.height > 1)
				desc.type = TextureType::Tex2D;
			else
				desc.type = TextureType::Tex1D;
		}

		DynArray<u8>& data = pImage->GetData();
		return m_pResourceManager->CreateTexture(desc, owningQueueType, data.Data(), u64(data.Size()));
	}

	b8 Renderer::CoreRemoveTexture(Core::Texture* pTexture)
	{
		return m_pResourceManager->DestroyTexture(pTexture);
	}

	Core::RenderTarget* Renderer::CoreAddRenderTarget(const Core::RenderTargetDesc& desc)
	{
		return m_pResourceManager->CreateRenderTarget(desc);
	}

	b8 Renderer::CoreRemoveRenderTarget(Core::RenderTarget* pRT)
	{
		return m_pResourceManager->DestroyRenderTarget(pRT);
	}

	Core::Buffer* Renderer::CoreAddBuffer(BufferType type, u64 size, BufferFlags flags)
	{
		return m_pResourceManager->CreateBuffer(type, size, flags);
	}

	Core::Buffer* Renderer::CoreAddVertexBuffer(u32 vertexCount, u16 vertexSize, BufferFlags flags)
	{
		return m_pResourceManager->CreateVertexBuffer(vertexCount, vertexSize, flags);
	}

	Core::Buffer* Renderer::CoreAddIndexuffer(u32 indexCount, IndexType indexType, BufferFlags flags)
	{
		return m_pResourceManager->CreateIndexuffer(indexCount, indexType, flags);
	}

	b8 Renderer::CoreRemoveBuffer(Core::Buffer* pBuffer)
	{
		return m_pResourceManager->DestroyBuffer(pBuffer);
	}

	Core::DescriptorSet* Renderer::CoreAddDescriptorSet(const DynArray<Core::DescriptorSetBinding>& bindings)
	{
		Core::DescriptorSetLayout* pLayout = m_pDescriptorSetManager->CreateDescriptorSetLayout(bindings);
		return m_pDescriptorSetManager->CreateDescriptorSet(pLayout);
	}

	b8 Renderer::CoreRemoveDescriptorSet(Core::DescriptorSet* pDescriptorSet)
	{
		return m_pDescriptorSetManager->DestroyDescriptorSet(pDescriptorSet);
	}

	// Temp
	Pair<Core::Buffer*, Core::Buffer*> Renderer::GetBuffers(MeshHandle mesh)
	{
		if (!mesh.IsValid())
			return Pair<Core::Buffer*, Core::Buffer*>(nullptr, nullptr);

		u64 id = mesh.GetId();
		if (m_MeshBuffers.Size() <= id)
			return Pair<Core::Buffer*, Core::Buffer*>(nullptr, nullptr);
		return m_MeshBuffers[id];
	}

	Renderer& GetRenderer()
	{
		static Renderer renderer;
		return renderer;
	}
}
