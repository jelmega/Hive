// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PassContext.cpp: Render pass context
#include "RendererPCH.h"
#include "Renderer/PassContext.h"
#include "Renderer/Renderer.h"
#include "Renderer/Material/Material.h"
#include "Renderer/Material/MaterialInstance.h"
#include "Renderer/View.h"

namespace Hv::Renderer {

	PassContext::PassContext(RHI::IDynamicRHI* pRHI, Core::RenderPass* pRenderPass)
		: m_pRHI(pRHI)
		, m_pRenderPass(pRenderPass)
		, m_pCommandList(nullptr)
		, m_pDepthStencil(nullptr)
		, m_pFramebuffer(nullptr)
		, m_InRenderPass(false)
	{
		Core::CommandListManager* pManager = m_pRHI->GetCommandListManager();
		Core::Queue* pQueue = m_pRHI->GetContext()->GetQueue(QueueType::Graphics);
		m_pCommandList = pManager->CreateCommandList(pQueue);
	}

	PassContext::~PassContext()
	{
	}

	void PassContext::Reset()
	{
		m_RenderTargets.Clear();
		m_pDepthStencil = nullptr;
		m_InRenderPass = false;

		if (m_pFramebuffer)
		{
			m_pRHI->DestroyFramebuffer(m_pFramebuffer);
			m_pFramebuffer = nullptr;
		}
	}

	void PassContext::AddRenderTarget(Core::RenderTarget* pRenderTarget)
	{
		if (m_InRenderPass)
		{
			g_Logger.LogWarning(LogRenderer(), "PassContext: Cannot add a render target after targets were bound");
			return;
		}

		m_RenderTargets.Push(pRenderTarget);
	}

	void PassContext::SetDepthStencil(Core::RenderTarget* pDepthStencil)
	{
		if (m_InRenderPass)
		{
			g_Logger.LogWarning(LogRenderer(), "PassContext: Cannot set the depth stencil after targets were bound");
			return;
		}
		m_pDepthStencil = pDepthStencil;
	}

	void PassContext::BindRenderTargets()
	{
		if (m_InRenderPass)
		{
			g_Logger.LogWarning(LogRenderer(), "PassContext: Render targets already bound");
			return;
		}
		if (m_pDepthStencil)
			m_RenderTargets.Push(m_pDepthStencil);

		m_pFramebuffer = m_pRHI->CreateFramebuffer(m_RenderTargets, m_pRenderPass);

		m_pCommandList->BeginRenderPass(m_pRenderPass, m_pFramebuffer);
		m_InRenderPass = true;
	}

	void PassContext::SetMaterial(MaterialHandle material)
	{
		if (!m_InRenderPass)
		{
			g_Logger.LogWarning(LogRenderer(), "PassContext: Render targets not bound before setting material");
			return;
		}
		if (!material.IsValid())
		{
			g_Logger.LogWarning(LogRenderer(), "PassContext: Invalid material handle");
			return;
		}

		Core::Pipeline* pPipeline = material->GetPipeline();
		m_pCommandList->BindPipeline(pPipeline);
		m_CurMaterial = material;
	}

	void PassContext::SetMaterialInstance(MaterialInstanceHandle matInstance)
	{
		if (matInstance->GetMaterial() != m_CurMaterial)
		{
			g_Logger.LogWarning(LogRenderer(), "PassContext: Material instance is not an instance of the current material");
			return;
		}

		// Material descriptor set is bound to slot 2 !!!
		Core::DescriptorSet* pDescriptorSet = matInstance->GetDescriptorSet();
		m_pCommandList->BindDescriptorSets(1, pDescriptorSet);
	}

	void PassContext::SetView(View& view)
	{
		m_pCommandList->SetScissor(view.scissor);
		m_pCommandList->SetViewport(view.viewport);
		m_pCommandList->BindDescriptorSets(0, view.pDescriptorSet);
	}

	void PassContext::DrawMesh(MeshHandle mesh, Core::DescriptorSet* pTransformDescriptorSet, u64 transformOffset)
	{
		if (!m_InRenderPass)
		{
			g_Logger.LogWarning(LogRenderer(), "PassContext: Render targets not bound before setting material");
			return;
		}

		Pair<Core::Buffer*, Core::Buffer*> buffers = g_Renderer.GetBuffers(mesh);

		m_pCommandList->BindDescriptorSets(2, pTransformDescriptorSet, u32(transformOffset));
		m_pCommandList->BindVertexBuffer(0, buffers.first, 0);
		m_pCommandList->BindIndexBuffer(buffers.second, 0);
		m_pCommandList->DrawIndexed(buffers.second->GetIndexCount());
	}
}
