// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Material.cpp: Material
#include "RendererPCH.h"
#include "Renderer/Material/Material.h"
#include "Renderer/FrameGraph.h"
#include "Renderer/Renderer.h"
#include "Renderer/Material/MaterialHelpers.h"

namespace Hv::Renderer {

	b8 MaterialDesc::IsValid() const
	{
		if (!shaders.pVertexShader || !shaders.pFragmentShader)
			return false;
		return true;
	}

	Material::Material()
		: m_pRHI(nullptr)
		, m_pResourceManager(nullptr)
		, m_pPipeline(nullptr)
		, m_pDescriptorSetLayout(nullptr)
		, m_PrimitiveBufferSize(0)
		, m_PrimitiveCount(0)
	{
	}

	Material::~Material()
	{
	}

	b8 Material::Create(RHI::IDynamicRHI* pRHI, const MaterialDesc& desc, const String& identifier)
	{
		if (!desc.IsValid())
			return false;

		CustomMaterialDesc matDesc = Helpers::GetCustomMaterialDesc(desc);
		// TODO: Find better way of doing this
		matDesc.pFrameLayout = g_Renderer.GetFrameLayout();  
		matDesc.pViewLayout = g_Renderer.GetViewLayout();
		matDesc.pObjectLayout = g_Renderer.GetObjectLayout();

		return Create(pRHI, matDesc, identifier);
	}

	b8 Material::Create(RHI::IDynamicRHI* pRHI, const CustomMaterialDesc& desc, const String& identifier)
	{
		m_pRHI = pRHI;
		m_Identifier = identifier;

		// Process material parameters
		DynArray<Core::DescriptorSetBinding> bindings;

		// TODO: Do primitive params need to packed or not (currently packed)
		ShaderType paramShaderTypes = ShaderType::None;
		for (const MaterialParam& param : desc.parameters)
		{
			if (u8(param.type) > u8(MaterialParamType::Float))
			{
				u32 size = Helpers::GetMaterialParamSize(param.type);
				m_PrimitiveOffset.Push(m_PrimitiveBufferSize);
				m_PrimitiveBufferSize += size * param.arrayElements;
				paramShaderTypes |= param.shadertype;
				++m_PrimitiveCount;

				m_MaterialParamsMapping.Insert(param.name, u32(m_MaterialParams.Size()));
				m_MaterialParams.Push(param);
			}
		}

		// Create bindings
		if (m_PrimitiveCount > 0)
		{
			Core::DescriptorSetBinding binding = {};
			binding.type = DescriptorSetBindingType::Uniform;
			binding.shadertype = paramShaderTypes;
			binding.count = 1;
			bindings.Push(binding);
		}

		for (const MaterialParam& param : desc.parameters)
		{
			if (u8(param.type) < u8(MaterialParamType::Float))
			{
				Core::DescriptorSetBinding binding = {};
				binding.type = DescriptorSetBindingType(param.type);
				binding.shadertype = param.shadertype;
				binding.count = param.arrayElements;
				bindings.Push(binding);

				m_MaterialParamsMapping.Insert(param.name, u32(m_MaterialParams.Size()));
				m_MaterialParams.Push(param);
			}
		}

		// Create descriptor set layout
		Core::DescriptorSetManager* pDescriptorSetManager = m_pRHI->GetDescriptorSetManager();
		m_pDescriptorSetLayout = pDescriptorSetManager->CreateDescriptorSetLayout(bindings);

		// Create pipeline
		FrameGraph* pFrameGraph = g_Renderer.GetFrameGraph();
		Core::RenderPass* pRenderPass = pFrameGraph->GetRenderPass(desc.passIden);

		Core::GraphicsPipelineDesc pipelineDesc = {};
		pipelineDesc.pRenderPass = pRenderPass;

		pipelineDesc.pVertexShader = desc.shaders.pVertexShader;
		pipelineDesc.pGeometryShader = desc.shaders.pGeometryShader;
		pipelineDesc.pFragmentShader = desc.shaders.pFragmentShader;

		pipelineDesc.blendState = desc.blendState;
		pipelineDesc.tesellation = desc.tessellation;

		pipelineDesc.rasterizer = desc.rasterizer;
		pipelineDesc.depthStencil = desc.depthStencil;
		pipelineDesc.inputDescriptor = desc.inputDescriptor;

		pipelineDesc.primitiveTopology = desc.primitiveTopology;
		pipelineDesc.enablePrimitiveRestart = desc.enablePrimitiveRestart;

		pipelineDesc.dynamicState = DynamicState::Viewport | DynamicState::Scissor;

		// TODO: Better way
		//pipelineDesc.descriptorSetLayouts.Push(desc.pFrameLayout);
		pipelineDesc.descriptorSetLayouts.Push(desc.pViewLayout);
		pipelineDesc.descriptorSetLayouts.Push(m_pDescriptorSetLayout);
		pipelineDesc.descriptorSetLayouts.Push(desc.pObjectLayout);

		m_pPipeline = m_pRHI->CreatePipeline(pipelineDesc);

		return true;
	}

	b8 Material::Destroy()
	{
		if (m_pPipeline)
		{
			m_pRHI->DestroyPipeline(m_pPipeline);
			m_pPipeline = nullptr;
		}

		if (m_pDescriptorSetLayout)
		{
			if (m_pDescriptorSetLayout->GetReferenceCount() == 0)
			{
				Core::DescriptorSetManager* pManager = m_pRHI->GetDescriptorSetManager();
				pManager->DestroyDescriptorSetLayout(m_pDescriptorSetLayout);
			}
			m_pDescriptorSetLayout = nullptr;
		}

		return true;
	}

	u32 Material::GetMaterialParamIndex(const String& name)
	{
		auto it = m_MaterialParamsMapping.Find(name);
		if (it == m_MaterialParamsMapping.Back())
			return u32(-1);
		return it->second;
	}
}
