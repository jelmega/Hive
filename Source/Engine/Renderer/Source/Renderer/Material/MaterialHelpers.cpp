// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// MaterialHelpers.cpp: Material helpers
#include "RendererPCH.h"
#include "Renderer/Material/MaterialHelpers.h"
#include "Renderer/Renderer.h"

namespace Hv::Renderer::Helpers {

	// TODO: Update if needed
	CustomMaterialDesc GetCustomMaterialDesc(const MaterialDesc& desc)
	{
		CustomMaterialDesc out = {};
		out.shaders.pVertexShader = desc.shaders.pVertexShader;
		out.shaders.pGeometryShader = desc.shaders.pGeometryShader;
		out.shaders.pFragmentShader = desc.shaders.pFragmentShader;

		RenderingMode renderMode = g_Renderer.GetRenderingMode();

		out.blendState = GetMaterialBlendState(desc.blendMode);
		out.rasterizer = GetMaterialRasterDesc(desc.twoSided);
		out.depthStencil = GetMaterialDepthStencilDesc(desc.blendMode);
		out.primitiveTopology = PrimitiveTopology::Triangle;
		out.enablePrimitiveRestart = false;
		out.passIden = GetMaterialPass(desc.domain, renderMode);
		out.inputDescriptor = GetMaterialInputDescriptor(desc.domain);
		out.parameters = desc.parameters;

		return out;
	}

	BlendStateDesc GetMaterialBlendState(MaterialBlendMode blendMode)
	{
		switch (blendMode)
		{
		default:
		case MaterialBlendMode::Opaque:
		{
			BlendStateDesc desc = {};
			BlendAttachment attachment = {};
			attachment.components = ColorComponentMask::R | ColorComponentMask::G | ColorComponentMask::B | ColorComponentMask::A;
			desc.attachments.Push(attachment);
			return desc;
		}
		case MaterialBlendMode::Transparent:
		{
			// TODO
			BlendStateDesc desc = {};
			BlendAttachment attachment = {};
			desc.attachments.Push(attachment);
			return desc;
		}
		case MaterialBlendMode::Masked:
		{
			// TODO
			BlendStateDesc desc = {};
			BlendAttachment attachment = {};
			desc.attachments.Push(attachment);
			return desc;
		}
		}
	}

	RasterizerDesc GetMaterialRasterDesc(b8 twoSided)
	{
		RasterizerDesc raster = {};

		if (twoSided)
		{
			raster.cullMode = CullMode::None;
		}
		else
		{
			raster.cullMode = CullMode::Back;
			raster.frontFace = FrontFace::CounterClockWise;
		}
		raster.fillMode = FillMode::Solid;

		return raster;
	}

	DepthStencilDesc GetMaterialDepthStencilDesc(MaterialBlendMode blendMode)
	{
		switch (blendMode)
		{
		default:
		case MaterialBlendMode::Opaque:
		{
			DepthStencilDesc desc = {};
			desc.depthCompareOp = CompareOp::Less;
			desc.enableDepthTest = true;
			// Temp
			desc.enableDepthWrite = true;
			return desc;
		}
		case MaterialBlendMode::Transparent:
		{
			DepthStencilDesc desc = {};
			desc.depthCompareOp = CompareOp::Less;
			desc.enableDepthTest = true;
			return desc;
		}
		case MaterialBlendMode::Masked:
		{
			DepthStencilDesc desc = {};
			desc.depthCompareOp = CompareOp::Less;
			desc.enableDepthTest = true;
			return desc;
		}
		}
	}

	const AnsiChar* GetMaterialPass(MaterialDomain domain, RenderingMode mode)
	{
		// TODO: Keep in sync with framegraph
		switch (domain)
		{
		default:
		case MaterialDomain::Surface:
		{
			switch (mode)
			{
			default:
			case RenderingMode::Deferred:		return "MainGBufferPass";
			case RenderingMode::Forward:		return "MainPass";
			case RenderingMode::ForwardPlus:	return "MainPass";
			}
		}
		}
	}

	InputDescriptor GetMaterialInputDescriptor(MaterialDomain domain)
	{
		// TODO: Generate from shader, instead of domain
		switch (domain)
		{
		default:
		case MaterialDomain::Surface:
		{
			return Mesh::GetInputDescriptor();
		}
		}
	}

	u32 GetMaterialParamSize(MaterialParamType type)
	{
		switch (type)
		{
		case MaterialParamType::Float:		return 4;
		case MaterialParamType::Float2:		return 8;
		case MaterialParamType::Float3:		return 12;
		case MaterialParamType::Float4:		return 16;
		case MaterialParamType::Int:		return 4;
		case MaterialParamType::Int2:		return 8;
		case MaterialParamType::Int3:		return 12;
		case MaterialParamType::Int4:		return 16;
		case MaterialParamType::Bool:		return 1;
		case MaterialParamType::Float2x2:	return 16;
		case MaterialParamType::Float3x3:	return 36;
		case MaterialParamType::Float4x4:	return 64;
		default:							return 0;
		}
	}
}
