// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// MaterialManager.cpp: Material manager
#include "RendererPCH.h"
#include "Renderer/Material/MaterialManager.h"
#include "Renderer/Material/Material.h"
#include "Renderer/Material/MaterialInstance.h"
#include "Renderer/Renderer.h"

namespace Hv::Renderer {


	MaterialManager::MaterialManager()
		: m_pRHI(nullptr)
		, m_pResourceManager(nullptr)
		, m_CurInstanceId(0)
	{
	}

	MaterialManager::~MaterialManager()
	{
	}

	void MaterialManager::Create(RHI::IDynamicRHI* pRHI, ResourceManager* pResourceManager)
	{
		m_pRHI = pRHI;
		m_pResourceManager = pResourceManager; 
	}

	b8 MaterialManager::Destroy()
	{
		b8 res = true;
		for (MaterialGroup* pGroup : m_Materials)
		{
			for (MaterialInstance* pInstance : pGroup->instances)
			{
				res &= pInstance->Destroy();
				HvDelete pInstance;
			}
			res &= pGroup->pMaterial->Destroy();
			HvDelete pGroup->pMaterial;
			HvDelete pGroup;
		}
		m_Materials.Clear();
		m_MaterialMapping.Clear();
		m_InstanceMapping.Clear();

		return res;
	}

	MaterialHandle MaterialManager::CreateMaterial(const String& iden, const MaterialDesc& desc)
	{
		MaterialHandle handle = GetMaterial(iden);
		if (handle.IsValid())
			return handle;

		Material* pMaterial = HvNew Material();
		pMaterial->Create(m_pRHI, desc, iden);

		u32 id = u32(m_Materials.Size());
		m_MaterialMapping.Insert(iden, id);
		MaterialGroup* pGroup = HvNew MaterialGroup();
		pGroup->pMaterial = pMaterial;
		m_Materials.Push(pGroup);

		return m_MatHandleManager.Create(id, pMaterial);
	}

	MaterialHandle MaterialManager::CreateMaterial(const String& iden, const CustomMaterialDesc& desc)
	{
		MaterialHandle handle = GetMaterial(iden);
		if (handle.IsValid())
			return handle;

		Material* pMaterial = HvNew Material();
		pMaterial->Create(m_pRHI, desc, iden);

		u32 id = u32(m_Materials.Size());
		m_MaterialMapping.Insert(iden, id);
		MaterialGroup* pGroup = HvNew MaterialGroup();
		pGroup->pMaterial = pMaterial;
		m_Materials.Push(pGroup);

		return m_MatHandleManager.Create(id, pMaterial);
	}

	MaterialInstanceHandle MaterialManager::CreateMaterialInstance(const String& iden, MaterialHandle material)
	{
		if (!material.IsValid())
			return MaterialInstanceHandle();

		u64 matId = material.GetId();

		MaterialGroup* pGroup = m_Materials[matId];

		MaterialInstance* pInstance = HvNew MaterialInstance();
		pInstance->Create(m_pRHI, m_pResourceManager, material, iden);

		u64 id;
		if (m_FreeInstanceIds.Size() > 0)
		{
			id = *m_FreeInstanceIds.Last();
			m_FreeInstanceIds.Pop();
		}
		else
		{
			id = m_CurInstanceId;
			++m_CurInstanceId;
		}

		u32 instIdx;
		if (pGroup->freeInstanceSlots.Size() > 0)
		{
			instIdx = *pGroup->freeInstanceSlots.Back();
			pGroup->freeInstanceSlots.Pop();
			pGroup->instances[instIdx] = pInstance;
			pGroup->idMapping[instIdx] = u32(id);
		}
		else
		{
			instIdx = u32(pGroup->instances.Size());
			pGroup->instances.Push(pInstance);
			pGroup->idMapping.Push(u32(id));
		}
		pGroup->mapping.Insert(iden, instIdx);
		m_InstanceMapping.Insert(iden, instIdx);

		return m_MatInstanceHandleManager.Create(id, pInstance);
	}

	b8 MaterialManager::DestroyMaterial(MaterialHandle& handle)
	{
		if (!handle.IsValid())
			return false;

		u32 id = u32(handle.GetId());
		MaterialGroup* pGroup = m_Materials[id];

		u32 instanceCount = 0;
		for (u32 i = 0; i < pGroup->instances.Size(); ++i)
		{
			MaterialInstance* pInstance = pGroup->instances[i];
			if (pInstance)
			{
				m_InstanceMapping.Erase(pInstance->GetIdentifier());

				pInstance->Destroy();
				HvDelete pInstance;

				u64 tmpId = pGroup->idMapping[i];
				m_MatInstanceHandleManager.Invalidate(tmpId);

				++instanceCount;
			}
		}
		Material* pMaterial = pGroup->pMaterial;
		m_MaterialMapping.Erase(pMaterial->GetIdentifier());
		pMaterial->Destroy();
		HvDelete pMaterial;

		HvDelete pGroup;
		m_Materials[id] = nullptr;

		if (instanceCount > 0)
			g_Logger.LogFormat(LogRenderer(), LogLevel::Warning, "Destroying material with %u instances!", instanceCount);
		return true;
	}

	b8 MaterialManager::DestroyMaterialInstance(MaterialInstanceHandle& handle)
	{
		if (!handle.IsValid())
			return false;

		MaterialHandle material = handle->GetMaterial();
		u64 matId = material.GetId();
		MaterialGroup* pGroup = m_Materials[matId];

		u64 id = handle.GetId();
		for (u32 i = 0; i < pGroup->idMapping.Size(); ++i)
		{
			if (id == pGroup->idMapping[i])
			{
				MaterialInstance* pInstance = handle.Get();
				pGroup->mapping.Erase(pInstance->GetIdentifier());
				m_InstanceMapping.Erase(pInstance->GetIdentifier());

				pInstance->Destroy();
				HvDelete pInstance;

				pGroup->freeInstanceSlots.Push(i);

				m_MatInstanceHandleManager.Invalidate(id);
				m_FreeInstanceIds.Push(u32(id));
				return true;
			}
		}
		return false;

	}

	MaterialHandle MaterialManager::GetMaterial(u32 idx)
	{
		if (idx > m_Materials.Size() || !m_Materials[idx])
			return MaterialHandle();

		Material* pMaterial = m_Materials[idx]->pMaterial;
		return m_MatHandleManager.Create(idx, pMaterial);
	}

	MaterialHandle MaterialManager::GetMaterial(const String& iden)
	{
		auto it = m_MaterialMapping.Find(iden);
		if (it == m_MaterialMapping.Back())
			return MaterialHandle();

		u32 id = it->second;
		Material* pMaterial = m_Materials[id]->pMaterial;
		return m_MatHandleManager.Create(id, pMaterial);
	}

	MaterialInstanceHandle MaterialManager::GetMaterialInstance(u32 matIdx, u32 instanceIdx)
	{
		if (matIdx > m_Materials.Size() || !m_Materials[matIdx])
			return MaterialInstanceHandle();

		DynArray<MaterialInstance*>& instances = m_Materials[matIdx]->instances;
		if (instanceIdx > instances.Size() || !instances[instanceIdx])
			return MaterialInstanceHandle();

		u64 id = (u64(matIdx) << 32) | instanceIdx;
		MaterialInstance* pInstance = instances[instanceIdx];
		return m_MatInstanceHandleManager.Create(id, pInstance);
	}

	MaterialInstanceHandle MaterialManager::GetMaterialInstance(const String& iden)
	{
		auto it = m_InstanceMapping.Find(iden);
		if (it == m_InstanceMapping.Back())
			return MaterialInstanceHandle();

		u64 id = it->second;
		MaterialGroup* pGroup = m_Materials[id];

		it = pGroup->mapping.Find(iden);
		if (it == pGroup->mapping.Back())
			return MaterialInstanceHandle();

		id <<= 32;
		id |= it->second;

		MaterialInstance* pInstance = pGroup->instances[it->second];
		return m_MatInstanceHandleManager.Create(id, pInstance);
	}
}
