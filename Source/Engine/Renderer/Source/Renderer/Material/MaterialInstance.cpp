// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// MaterialInstance.h: Material instance
#include "RendererPCH.h"
#include "Renderer/Material/MaterialInstance.h"
#include "Renderer/Material/Material.h"
#include "Renderer/ResourceManager.h"

// TODO: Better param assignment

namespace Hv::Renderer {

	MaterialInstance::MaterialInstance()
		: m_pRHI(nullptr)
		, m_pResourceManager(nullptr)
		, m_pDescriptorSet(nullptr)
		, m_PrimiteWrite(false)
		, m_pPrimitiveBuffer(nullptr)
	{
	}

	MaterialInstance::~MaterialInstance()
	{
	}

	b8 MaterialInstance::Create(RHI::IDynamicRHI* pRHI, ResourceManager* pResourceManager, MaterialHandle material, const String& identifier)
	{
		if (!material.IsValid())
			return false;

		m_pRHI = pRHI;
		m_pResourceManager = pResourceManager;
		m_Material = material;
		m_Identifier = identifier;

		// Create descriptor set
		Core::DescriptorSetManager* pDescriptorSetManager = m_pRHI->GetDescriptorSetManager();

		Core::DescriptorSetLayout* pLayout = m_Material->GetDescriptorSetLayout();
		m_pDescriptorSet = pDescriptorSetManager->CreateDescriptorSet(pLayout);

		// Setup parameters ??
		if (m_Material->GetPrimitiveCount() > 0)
		{
			m_PrimitiveData.Resize(m_Material->GetPrimitiveBufferSize());
			m_pPrimitiveBuffer = m_pResourceManager->CreateBuffer(BufferType::Uniform, m_Material->GetPrimitiveBufferSize(), BufferFlags::None);
		}

		return true;
	}

	b8 MaterialInstance::Destroy()
	{
		b8 res = true;
		if (m_pPrimitiveBuffer)
		{
			m_pResourceManager->DestroyBuffer(m_pPrimitiveBuffer);
			m_pPrimitiveBuffer = nullptr;
		}

		if (m_pDescriptorSet)
		{
			Core::DescriptorSetManager* pDescriptorSetManager = m_pRHI->GetDescriptorSetManager();
			res = pDescriptorSetManager->DestroyDescriptorSet(m_pDescriptorSet);
			m_pDescriptorSet = nullptr;
		}
		return res;
	}

	void MaterialInstance::UpdatePrimitives()
	{
		if (m_PrimiteWrite)
		{
			m_pPrimitiveBuffer->Write(0, (u64)m_PrimitiveData.Size(), m_PrimitiveData.Data());
			m_PrimiteWrite = false;
		}
	}

	void MaterialInstance::SetParam(const String& name, Core::Texture* pTexture)
	{
		u32 idx = m_Material->GetMaterialParamIndex(name);
		if (idx == u32(-1))
			return;

		MaterialParam& param = m_Material->GetParameter(idx);
		 
		if (param.type != MaterialParamType::SampledImage &&
			param.type != MaterialParamType::StorageImage)
		{
			return;
		}

		m_pDescriptorSet->Write(idx, pTexture, nullptr);
	}

	void MaterialInstance::SetParam(const String& name, Core::Sampler* pSampler)
	{
		u32 idx = m_Material->GetMaterialParamIndex(name);
		if (idx == u32(-1))
			return;

		MaterialParam& param = m_Material->GetParameter(idx);

		if (param.type != MaterialParamType::Sampler)
		{
			return;
		}

		m_pDescriptorSet->Write(idx, nullptr, pSampler);
	}

	void MaterialInstance::SetParam(const String& name, Core::Texture* pTexture, Core::Sampler* pSampler)
	{
		u32 idx = m_Material->GetMaterialParamIndex(name);
		if (idx == u32(-1))
			return;

		MaterialParam& param = m_Material->GetParameter(idx);

		if (param.type != MaterialParamType::CombinedImageSampler)
		{
			return;
		}

		m_pDescriptorSet->Write(idx, pTexture, pSampler);
	}

	void MaterialInstance::SetParam(const String& name, Core::Buffer* pBuffer)
	{
		u32 idx = m_Material->GetMaterialParamIndex(name);
		if (idx == u32(-1))
			return;

		MaterialParam& param = m_Material->GetParameter(idx);

		if (param.type == MaterialParamType::SampledImage ||
			param.type == MaterialParamType::StorageImage ||
			param.type == MaterialParamType::Sampler ||
			param.type == MaterialParamType::CombinedImageSampler)
		{
			return;
		}

		m_pDescriptorSet->Write(idx, pBuffer);
	}

	void MaterialInstance::SetParam(const String& name, b8 val)
	{
		u32 idx = m_Material->GetMaterialParamIndex(name);
		if (idx == u32(-1))
			return;

		MaterialParam& param = m_Material->GetParameter(idx);
		if (param.type != MaterialParamType::Bool)
			return;

		u32 offset = m_Material->GetPrimitiveOffset(idx);
		m_PrimitiveData[offset] = u8(val);
		m_PrimiteWrite = true;
	}

	void MaterialInstance::SetParam(const String& name, f32 val)
	{
		u32 idx = m_Material->GetMaterialParamIndex(name);
		if (idx == u32(-1))
			return;

		MaterialParam& param = m_Material->GetParameter(idx);
		if (param.type != MaterialParamType::Float)
			return;

		u32 offset = m_Material->GetPrimitiveOffset(idx);
		*((f32*)&m_PrimitiveData[offset]) = val;
		m_PrimiteWrite = true;
	}

	void MaterialInstance::SetParam(const String& name, f32v2 val)
	{
		u32 idx = m_Material->GetMaterialParamIndex(name);
		if (idx == u32(-1))
			return;

		MaterialParam& param = m_Material->GetParameter(idx);
		if (param.type != MaterialParamType::Float2)
			return;

		u32 offset = m_Material->GetPrimitiveOffset(idx);
		*((f32v2*)&m_PrimitiveData[offset]) = val;
		m_PrimiteWrite = true;
	}

	void MaterialInstance::SetParam(const String& name, f32v3 val)
	{
		u32 idx = m_Material->GetMaterialParamIndex(name);
		if (idx == u32(-1))
			return;

		MaterialParam& param = m_Material->GetParameter(idx);
		if (param.type != MaterialParamType::Float3)
			return;

		u32 offset = m_Material->GetPrimitiveOffset(idx);
		*((f32v3*)&m_PrimitiveData[offset]) = val;
		m_PrimiteWrite = true;
	}

	void MaterialInstance::SetParam(const String& name, f32v4 val)
	{
		u32 idx = m_Material->GetMaterialParamIndex(name);
		if (idx == u32(-1))
			return;

		MaterialParam& param = m_Material->GetParameter(idx);
		if (param.type != MaterialParamType::Float4)
			return;

		u32 offset = m_Material->GetPrimitiveOffset(idx);
		*((f32v4*)&m_PrimitiveData[offset]) = val;
		m_PrimiteWrite = true;
	}

	void MaterialInstance::SetParam(const String& name, i32 val)
	{
		u32 idx = m_Material->GetMaterialParamIndex(name);
		if (idx == u32(-1))
			return;

		MaterialParam& param = m_Material->GetParameter(idx);
		if (param.type != MaterialParamType::Int)
			return;

		u32 offset = m_Material->GetPrimitiveOffset(idx);
		*((i32*)&m_PrimitiveData[offset]) = val;
		m_PrimiteWrite = true;
	}

	void MaterialInstance::SetParam(const String& name, i32v2 val)
	{
		u32 idx = m_Material->GetMaterialParamIndex(name);
		if (idx == u32(-1))
			return;

		MaterialParam& param = m_Material->GetParameter(idx);
		if (param.type != MaterialParamType::Int2)
			return;

		u32 offset = m_Material->GetPrimitiveOffset(idx);
		*((i32v2*)&m_PrimitiveData[offset]) = val;
		m_PrimiteWrite = true;
	}

	void MaterialInstance::SetParam(const String& name, i32v3 val)
	{
		u32 idx = m_Material->GetMaterialParamIndex(name);
		if (idx == u32(-1))
			return;

		MaterialParam& param = m_Material->GetParameter(idx);
		if (param.type != MaterialParamType::Int3)
			return;

		u32 offset = m_Material->GetPrimitiveOffset(idx);
		*((i32v3*)&m_PrimitiveData[offset]) = val;
		m_PrimiteWrite = true;
	}

	void MaterialInstance::SetParam(const String& name, i32v4 val)
	{
		u32 idx = m_Material->GetMaterialParamIndex(name);
		if (idx == u32(-1))
			return;

		MaterialParam& param = m_Material->GetParameter(idx);
		if (param.type != MaterialParamType::Int4)
			return;

		u32 offset = m_Material->GetPrimitiveOffset(idx);
		*((i32v4*)&m_PrimitiveData[offset]) = val;
		m_PrimiteWrite = true;
	}
}
