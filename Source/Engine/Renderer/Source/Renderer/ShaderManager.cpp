// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ShaderManager.h: Shader manager
#pragma once
#include "RendererPCH.h"
#include "Renderer/ShaderManager.h"
#include "Renderer/Renderer.h"

namespace Hv::Renderer {


	ShaderManager::ShaderManager()
	{
	}

	ShaderManager::~ShaderManager()
	{
	}

	b8 ShaderManager::Create(RHI::IDynamicRHI* pRHI)
	{
		m_pRHI = pRHI;
		return true;
	}

	b8 ShaderManager::Destroy()
	{
		b8 res = true;
		for (auto pair : m_Shaders)
		{
			m_pRHI->DestroyShader(pair.second.pShader);
		}
		m_Shaders.Clear();
		return res;
	}

	Core::Shader* ShaderManager::LoadShader(const Core::ShaderDesc& desc)
	{
		auto it = m_Shaders.Find(desc.filePath);
		if (it != m_Shaders.Back())
			return it->second.pShader;

		// If not loaded, load the actual shader
		ShaderData data = {};
		data.desc = desc;

		data.pShader = m_pRHI->CreateShader(desc);
		if (!data.pShader)
		{
			g_Logger.LogFormat(LogRenderer(), LogLevel::Error, "Failed to load shader: %s", desc.filePath);
			return nullptr;
		}

		m_Shaders.Insert(desc.filePath, data);
		return data.pShader;
	}

	b8 ShaderManager::UnloadShader(const String& path)
	{
		auto it = m_Shaders.Find(path);
		if (it != m_Shaders.Back()) // Shader not present, so it is technically unloaded
			return true;

		b8 res = m_pRHI->DestroyShader(it->second.pShader);
		m_Shaders.Erase(it);
		return res;
	}

	b8 ShaderManager::UnloadShader(Core::Shader* pShader)
	{
		b8 res = true;
		for (auto it = m_Shaders.Front(); it != m_Shaders.Back(); ++it)
		{
			if (it->second.pShader == pShader)
			{
				res = m_pRHI->DestroyShader(pShader);
				m_Shaders.Erase(it);
				break;
			}
		}
		return res;
	}
}
