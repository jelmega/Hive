// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ResourceManager.cpp: Graphics resource manager
#include "RendererPCH.h"
#include "Renderer/ResourceManager.h"

namespace Hv::Renderer {


	ResourceManager::ResourceManager()
		: m_pRHI(nullptr)
		, m_pCommandListManager(nullptr)
		, m_UsedTextureMemory(0)
		, m_UsedBufferMemory(0)
		, m_UsedRenderTargetMemory(0)
	{
	}

	ResourceManager::~ResourceManager()
	{
	}

	void ResourceManager::Create(RHI::IDynamicRHI* pRHI, Core::CommandListManager* pCommandListManager)
	{
		m_pRHI = pRHI;
		m_pCommandListManager = pCommandListManager;
	}

	b8 ResourceManager::Destroy()
	{
		b8 res = true;

		for (Core::RenderTarget* pRT : m_RenderTargets)
		{
			res &= m_pRHI->DestroyRenderTarget(pRT);
		}
		m_RenderTargets.Clear();
		m_UsedRenderTargetMemory = 0;
		
		for (Core::Texture* pTexture : m_Textures)
		{
			res &= m_pRHI->DestroyTexture(pTexture);
		}
		m_Textures.Clear();
		m_UsedTextureMemory = 0;

		for (Core::Buffer* pBuffer : m_Buffers)
		{
			res &= m_pRHI->DestroyBuffer(pBuffer);
		}
		m_Buffers.Clear();
		m_UsedBufferMemory = 0;

		return res;
	}

	////////////////////////////////////////////////////////////////////////////////
	Core::Texture* ResourceManager::CreateTexture(const Core::TextureDesc& desc, QueueType owningQueueType)
	{
		Core::Queue* pQueue = m_pRHI->GetContext()->GetQueue(owningQueueType);
		Core::CommandList* pCommandList = m_pCommandListManager->CreateSingleTimeCommandList(pQueue);

		Core::Texture* pTexture = m_pRHI->CreateTexture(desc, pCommandList);
		if (!pTexture)
		{
			g_Logger.LogError(LogRenderer(), "Failed to add a texture to the resource manager");
			return nullptr;
		}
		m_Textures.Push(pTexture);

		m_pCommandListManager->EndSingleTimeCommandList(pCommandList);

		m_UsedTextureMemory += pTexture->GetMemorySize();

		return pTexture;
	}

	Core::Texture* ResourceManager::CreateTexture(const Core::TextureDesc& desc, QueueType owningQueueType, u8* pData,
		u64 dataSize)
	{
		Core::TextureDesc tmpDesc = desc;
		tmpDesc.layout = TextureLayout::Unknown;

		Core::Queue* pQueue = m_pRHI->GetContext()->GetQueue(owningQueueType);
		Core::CommandList* pCommandList = m_pCommandListManager->CreateSingleTimeCommandList(pQueue);

		Core::Texture* pTexture = m_pRHI->CreateTexture(tmpDesc, pCommandList);
		if (!pTexture)
		{
			g_Logger.LogError(LogRenderer(), "Failed to add a texture to the resource manager");
			return nullptr;
		}
		m_Textures.Push(pTexture);

		// Write data to texture
		TextureRegion region = {};
		region.layerCount = desc.layerCount;
		region.extent.x = desc.width;
		region.extent.y = desc.height;
		region.extent.z = desc.depth;

		b8 res = pTexture->Write(region, dataSize, pData, pCommandList);
		if (!res)
		{
			g_Logger.LogError(LogRenderer(), "Failed to write image to texture (even though texture creation was successful)");
			return pTexture;
		}

		// Transition to the final layout
		TextureLayoutTransition transition = {};
		transition.layerCount = desc.layerCount;
		transition.layout = desc.layout;
		transition.mipLevelCount = desc.mipLevels;
		pCommandList->TransitionTextureLayout(PipelineStage::Transfer, PipelineStage::Transfer, pTexture, transition);

		m_pCommandListManager->EndSingleTimeCommandList(pCommandList);

		m_UsedTextureMemory += pTexture->GetMemorySize();

		return pTexture;
	}

	b8 ResourceManager::DestroyTexture(Core::Texture* pTexture)
	{
		Core::Texture** it = m_Textures.Find(pTexture);
		if (it == m_Textures.Back())
		{
			g_Logger.LogWarning("Failed to remove a sampler from the resource manager");
			return false;
		}
		m_UsedTextureMemory -= pTexture->GetMemorySize();
		b8 res = m_pRHI->DestroyTexture(*it);
		m_Textures.Erase(it);

		return res;
	}

	Core::RenderTarget* ResourceManager::CreateRenderTarget(const Core::RenderTargetDesc& desc)
	{
		Core::RenderTarget* pRT = m_pRHI->CreateRenderTarget(desc);
		if (!pRT)
		{
			g_Logger.LogError(LogRenderer(), "Failed to add a texture to the resource manager");
			return nullptr;
		}
		m_RenderTargets.Push(pRT);

		return pRT;
	}

	b8 ResourceManager::DestroyRenderTarget(Core::RenderTarget* pRT)
	{
		Core::RenderTarget** it = m_RenderTargets.Find(pRT);
		if (it == m_RenderTargets.Back())
		{
			g_Logger.LogWarning("Failed to remove a sampler from the resource manager");
			return false;
		}
		b8 res = m_pRHI->DestroyRenderTarget(*it);
		m_RenderTargets.Erase(it);

		return res;
	}

	////////////////////////////////////////////////////////////////////////////////
	Core::Buffer* ResourceManager::CreateBuffer(BufferType type, u64 size, BufferFlags flags)
	{
		Core::Buffer* pBuffer = m_pRHI->CreateBuffer(type, size, flags);
		if (!pBuffer)
		{
			g_Logger.LogError(LogRenderer(), "Failed to add a buffer to the resource manager");
			return nullptr;
		}
		m_Buffers.Push(pBuffer);
		return pBuffer;
	}

	Core::Buffer* ResourceManager::CreateVertexBuffer(u32 vertexCount, u16 vertexSize, BufferFlags flags)
	{
		Core::Buffer* pBuffer = m_pRHI->CreateVertexBuffer(vertexCount, vertexSize, flags);
		if (!pBuffer)
		{
			g_Logger.LogError(LogRenderer(), "Failed to add a vertex buffer to the resource manager");
			return nullptr;
		}
		m_Buffers.Push(pBuffer);
		return pBuffer;
	}

	Core::Buffer* ResourceManager::CreateIndexuffer(u32 indexCount, IndexType indexType, BufferFlags flags)
	{
		Core::Buffer* pBuffer = m_pRHI->CreateIndexBuffer(indexCount, indexType, flags);
		if (!pBuffer)
		{
			g_Logger.LogError(LogRenderer(), "Failed to add a index buffer to the resource manager");
			return nullptr;
		}
		m_Buffers.Push(pBuffer);
		return pBuffer;
	}

	b8 ResourceManager::DestroyBuffer(Core::Buffer* pBuffer)
	{
		Core::Buffer** it = m_Buffers.Find(pBuffer);
		if (it == m_Buffers.Back())
		{
			g_Logger.LogWarning("Failed to remove a frame buffer from the resource manager");
			return false;
		}
		b8 res = m_pRHI->DestroyBuffer(*it);
		m_Buffers.Erase(it);

		return res;
	}
	////////////////////////////////////////////////////////////////////////////////
}
