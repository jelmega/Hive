// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// RendererPCH.h: Renderer Precompiled header
#pragma once
#include <Core.h>
#include <RendererCore.h>
#include <AssetSystem.h>
#include HV_INCLUDE_RTTI_MODULE(Renderer)

HV_DECLARE_LOG_CATEGORY_EXTERN(Renderer);