// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Pass.h: Render pass
#pragma once
#include "RendererPCH.h"
#include "FrameGraph.h"

namespace Hv::Renderer {
	
	class Pass
	{
	public:
		Pass(const String& iden) : m_PassIdentifier(iden) {}
		virtual ~Pass() {}

		/**
		 * Setup the render pass
		 */
		virtual void Setup(FrameGraphBuilder& builder) = 0;
		/**
		 * Draw the pass
		 */
		virtual void Draw(FrameGraphResources& resources, PassContext& context) = 0;

		/**
		 * Get the pass's identifier
		 * @return	Identifier
		 */
		const String& GetIdentifier() const { return m_PassIdentifier; }

	protected:
		String m_PassIdentifier;
	};

}
