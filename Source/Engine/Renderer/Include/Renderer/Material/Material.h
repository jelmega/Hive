// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Material.h: Material
#pragma once
#include "RendererPCH.h"
#include "MaterialParam.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv::Renderer {
	class MaterialManager;
	class ResourceManager;

	enum class MaterialDomain : u8
	{
		Surface
	};

	enum class MaterialBlendMode : u8
	{
		Opaque,
		Transparent,
		Masked
	};

	enum class MaterialShadingMode : u8
	{
		Lit,
		Unlit,
	};

	struct MaterialDesc
	{
		struct
		{
			Core::Shader* pVertexShader;
			Core::Shader* pGeometryShader;
			Core::Shader* pFragmentShader;
		} shaders;							/**< Shaders for material */

		MaterialDomain domain;				/**< Material domain */
		MaterialBlendMode blendMode;		/**< Blend mode */
		MaterialShadingMode shadingMode;	/**< Shading mode */
		b8 twoSided;						/**< If the material is two sided */

		DynArray<MaterialParam> parameters;	/**< Material parameters */

		/**
		 * Check if the material description is valid
		 * @return	True if the description is valid
		 */
		b8 IsValid() const;
	};

	/**
	 * Custom material desc\n
	 * Allows for a lot more control over the material, but also requires the user to have greater knowledge of the required parameters and the inner workings of the engine
	 */
	struct CustomMaterialDesc
	{
		String passIden;								/**< Identifier of framegraph pass this material will be used in */
		struct
		{
			Core::Shader* pVertexShader;
			Core::Shader* pGeometryShader;
			Core::Shader* pFragmentShader;
		} shaders;										/**< Shaders for material */

		BlendStateDesc blendState;						/**< Material blend state */
		TessellationDesc tessellation;					/**< Tessellation for material */

		RasterizerDesc rasterizer;						/**< Material rasterizer desc */
		PrimitiveTopology primitiveTopology;			/**< Primitive topology */
		b8 enablePrimitiveRestart;						/**< If primitive restart is enabled */
		DepthStencilDesc depthStencil;					/**< Material depth stencil desc */

		InputDescriptor inputDescriptor;				/**< Input descriptor */
		DynArray<MaterialParam> parameters;				/**< Material parameters */

		Core::DescriptorSetLayout* pFrameLayout;		/**< Descriptor set layout for frame data */
		Core::DescriptorSetLayout* pViewLayout;			/**< Descriptor set layout for view data */
		Core::DescriptorSetLayout* pObjectLayout;		/**< Descriptor set layout for model data */
	};

	/**
	 * Material\n
	 * Used as a base for material instances (does not have textures, ...)
	 */
	class HIVE_API Material
	{
	public:
		Material();
		~Material();

		/**
		 * Create a material
		 * @param[in] pRHI	RHI
		 * @param[in] desc	Material description
		 * @return			True if the material was created successfully, false otherwise
		 */
		b8 Create(RHI::IDynamicRHI* pRHI, const MaterialDesc& desc, const String& identifier);
		/**
		 * Create a material
		 * @param[in] pRHI	RHI
		 * @param[in] desc	Material description
		 * @return			True if the material was created successfully, false otherwise
		 */
		b8 Create(RHI::IDynamicRHI* pRHI, const CustomMaterialDesc& desc, const String& identifier);
		/**
		 * Destroy the material
		 * @return		True if the material was destroyed successfully, false otherwise
		 */
		b8 Destroy();

		/**
		 * Get the pipeline for the material
		 * @return	Pipeline
		 */
		Core::Pipeline* GetPipeline() { return m_pPipeline; }
		/**
		 * Get the descriptor set bindings for the material
		 * @return	Descriptor set bindings
		 */
		Core::DescriptorSetLayout* GetDescriptorSetLayout() { return m_pDescriptorSetLayout; }
		/**
		 * Get the instance's identifier
		 * @return	Identifier
		 */
		const String& GetIdentifier() const { return m_Identifier; }

		/**
		 * [INTERNAL] Get the material parameters definitions
		 * @return	Material parameter definitions
		 */
		DynArray<MaterialParam>& GetParameters() { return m_MaterialParams; }
		/**
		* [INTERNAL] Get a material parameter definition
		* @param[in] index	Parameter index
		* @return			Material parameter definition
		*/
		MaterialParam& GetParameter(u32 index) { return m_MaterialParams[index]; }
		/**
		 * [INTERNAL] Get the index of a material parameter from it's name
		 * @param[in] name	Parameter name
		 * @return			Parameter index
		 */
		u32 GetMaterialParamIndex(const String& name);
		
		/**
		 * [INTERNAL] Get the size of the primitive buffer
		 * @return	Primitive buffer size
		 */
		u32 GetPrimitiveBufferSize() { return m_PrimitiveBufferSize; }
		/**
		 * [INTERNAL] Get the number of the primitive values
		 * @return	Primitive count
		 */
		u32 GetPrimitiveCount() { return m_PrimitiveCount; }
		/**
		 * [INTERNAL] Get a primitive's offset
		 * @param[in] index		Primitive index
		 * @return				Primitive offset
		 */
		u32 GetPrimitiveOffset(u32 index) { return m_PrimitiveOffset[index]; }

	private:
		RHI::IDynamicRHI* m_pRHI;
		ResourceManager* m_pResourceManager;

		Core::Pipeline* m_pPipeline;
		Core::DescriptorSetLayout* m_pDescriptorSetLayout;

		String m_Identifier;
		DynArray<MaterialParam> m_MaterialParams;
		HashMap<String, u32> m_MaterialParamsMapping;
		DynArray<u32> m_PrimitiveOffset;
		u32 m_PrimitiveBufferSize;
		u32 m_PrimitiveCount;
	};

	using MaterialHandle = Handle<Material>;
}

#pragma warning(pop)
