// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// MaterialParam.h: Material parameter
#pragma once
#include "RendererPCH.h"

namespace Hv::Renderer {
	
	/**
	 * Material parameter type
	 * @note When primitive types are used, they will be contained in a buffer at binding 0
	 */
	enum class MaterialParamType
	{
		// Mapped to DescriptorSetBindingType
		SampledImage = 1,
		CombinedImageSampler,
		Sampler,
		StorageImage,
		UniformTexelBuffer,
		StorageTexelBuffer,
		Uniform,
		Storage,
		DynamicUniformBuffer,
		DynamicStorageBuffer,
		InputAttachment,

		// Primitives // TODO: extend
		Float,
		Float2,
		Float3,
		Float4,
		Int,
		Int2,
		Int3,
		Int4,
		Bool,
		Float2x2,
		Float3x3,
		Float4x4
	};

	struct MaterialParam
	{
		String name;
		MaterialParamType type;
		ShaderType shadertype;
		u32 arrayElements;
	};

}
