// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// MaterialInstance.h: Material instance
#pragma once
#include "RendererPCH.h"
#include "Material.h"
#include "MaterialParam.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv::Renderer {
	class ResourceManager;

	/**
	 * Material instance\n
	 * Specifies the parameters for the material (textures, samplers, ...)
	 */
	class HIVE_API MaterialInstance
	{
	public:
		MaterialInstance();
		~MaterialInstance();

		/**
		 * Create a new instance from a material
		 * @param[in] pRHI				RHI
		 * @param[in] pResourceManager	RHI
		 * @param[in] pMaterial			Material to create instance for
		 * @param[in] identifier		Instance identifier
		 * @return						True if the material was created successfully, false otherwise
		 */
		b8 Create(RHI::IDynamicRHI* pRHI, ResourceManager* pResourceManager, MaterialHandle pMaterial, const String& identifier);
		/**
		* Destroy the material instance
		* @return	True if the material was destroyed successfully, false otherwise
		*/
		b8 Destroy();

		/**
		 * Update the primitive buffer
		 */
		void UpdatePrimitives();

		/**
		 * Set a texture parameter
		 * @param[in] name		Parameter name
		 * @param[in] pTexture	Texture
		 */
		void SetParam(const String& name, Core::Texture* pTexture);
		/**
		* Set a texture parameter
		* @param[in] name		Parameter name
		* @param[in] pSampler	Sampler
		*/
		void SetParam(const String& name, Core::Sampler* pSampler);
		/**
		* Set a texture and sampler parameter
		* @param[in] name		Parameter name
		* @param[in] pTexture	Texture
		* @param[in] pSampler	Sampler
		*/
		void SetParam(const String& name, Core::Texture* pTexture, Core::Sampler* pSampler);
		/**
		* Set a buffer parameter
		* @param[in] name		Parameter name
		* @param[in] pBuffer	Buffer
		*/
		void SetParam(const String& name, Core::Buffer* pBuffer);

		/**
		 * Set a boolean parameter
		 * @param[in] name	Parameter name
		 * @param[in] val	Value
		 */
		void SetParam(const String& name, b8 val);
		/**
		 * Set a float parameter
		 * @param[in] name	Parameter name
		 * @param[in] val	Value
		 */
		void SetParam(const String& name, f32 val);
		/**
		 * Set a 2D float vector parameter
		 * @param[in] name	Parameter name
		 * @param[in] val	Value
		 */
		void SetParam(const String& name, f32v2 val);
		/**
		 * Set a 2D float vector parameter
		 * @param[in] name	Parameter name
		 * @param[in] val	Value
		 */
		void SetParam(const String& name, f32v3 val);
		/**
		 * Set a 2D float vector parameter
		 * @param[in] name	Parameter name
		 * @param[in] val	Value
		 */
		void SetParam(const String& name, f32v4 val);
		/**
		 * Set a 2D float vector parameter
		 * @param[in] name	Parameter name
		 * @param[in] val	Value
		 */
		void SetParam(const String& name, i32 val);
		/**
		 * Set a 2D float vector parameter
		 * @param[in] name	Parameter name
		 * @param[in] val	Value
		 */
		void SetParam(const String& name, i32v2 val);
		/**
		 * Set a 2D float vector parameter
		 * @param[in] name	Parameter name
		 * @param[in] val	Value
		 */
		void SetParam(const String& name, i32v3 val);
		/**
		 * Set a 2D float vector parameter
		 * @param[in] name	Parameter name
		 * @param[in] val	Value
		 */
		void SetParam(const String& name, i32v4 val);
			
		/**
		 * Get the material this instance was created from
		 * @return	Material
		 */
		MaterialHandle& GetMaterial() { return m_Material; }
		/**
		 * Get the instance's descriptor set
		 * @return Descriptor set
		 */
		Core::DescriptorSet* GetDescriptorSet() { return m_pDescriptorSet; }
		/**
		 * Get the instance's identifier
		 * @return	Identifier
		 */
		const String& GetIdentifier() const { return m_Identifier; }

	private:

		RHI::IDynamicRHI* m_pRHI;
		ResourceManager* m_pResourceManager;

		MaterialHandle m_Material;
		Core::DescriptorSet* m_pDescriptorSet;

		String m_Identifier;
		DynArray<u8> m_PrimitiveData;
		b8 m_PrimiteWrite;
		Core::Buffer* m_pPrimitiveBuffer;
	};

	using MaterialInstanceHandle = Handle<MaterialInstance>;
}

#pragma warning(pop)