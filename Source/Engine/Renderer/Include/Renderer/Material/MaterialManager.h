// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// MaterialManager.h: Material manager
#pragma once
#include "RendererPCH.h"
#include "Renderer/ResourceManager.h"
#include "Material.h"
#include "MaterialInstance.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv::Renderer {
	struct MaterialDesc;
	struct CustomMaterialDesc;
	class MaterialInstance;
	class Material;

	struct MaterialGroup
	{
		Material* pMaterial;					/**< Base material */
		DynArray<MaterialInstance*> instances;	/**< Material instances */
		HashMap<String, u32> mapping;			/**< Mapping between instance names and material instances */
		DynArray<u32> freeInstanceSlots;	/**< Available instance slots that can be reused */
		DynArray<u32> idMapping;				/**< Mapping from instance index to id */
	};

	class HIVE_API MaterialManager
	{
	public:
		MaterialManager();
		~MaterialManager();

		/**
		 * Create the material manager
		 * @param[in] pRHI				RHI
		 * @param[in] pResourceManager	Resource manager
		 */
		void Create(RHI::IDynamicRHI* pRHI, ResourceManager* pResourceManager);
		/**
		 * Destroy the material manager
		 * @return	True if the manager was destroyed successfully, false otherwise
		 */
		b8 Destroy();

		/**
		 * Create a new material
		 * @param[in] iden	Identifier
		 * @param[in] desc	Material description
		 * @return			Created material
		 */
		MaterialHandle CreateMaterial(const String& iden, const MaterialDesc& desc);
		/**
		 * Create a new material
		 * @param[in] iden	Identifier
		 * @param[in] desc	Material description
		 * @return			Created material
		 */
		MaterialHandle CreateMaterial(const String& iden, const CustomMaterialDesc& desc);
		/**
		 * Create an instance of a material
		 * @param[in] iden			Identifier
		 * @param[in] pMaterial		Material to make the instance from
		 * @return					Material instance
		 */
		MaterialInstanceHandle CreateMaterialInstance(const String& iden, MaterialHandle pMaterial);

		/**
		 * Destroy a material instance
		 * @param[in] handle	Handle to material instance
		 * @return				True if the instance was successfully destroyed, false otherwise
		 * @note				Destroys all instances of this material, will give a warning!
		 */
		b8 DestroyMaterial(MaterialHandle& handle);
		/**
		 * Destroy a material instance
		 * @param[in] handle	Handle to material instance
		 * @return				True if the instance was successfully destroyed, false otherwise
		 */
		b8 DestroyMaterialInstance(MaterialInstanceHandle& handle);

		/**
		 * Get a material
		 * @param[in] idx	Material index
		 * @return			Material
		 */
		MaterialHandle GetMaterial(u32 idx);
		/**
		* Get a material
		* @param[in] iden	Material identifier
		* @return			Material
		*/
		MaterialHandle GetMaterial(const String& iden);
		/**
		* Get a material instance
		* @param[in] matIdx	Material index
		* @param[in] instanceIdx	Instance index
		* @return			Material instance
		*/
		MaterialInstanceHandle GetMaterialInstance(u32 matIdx, u32 instanceIdx);
		/**
		* Get a material instance
		* @param[in] iden	Material instance identifier
		* @return			Material instance
		*/
		MaterialInstanceHandle GetMaterialInstance(const String& iden);

	private:
		RHI::IDynamicRHI* m_pRHI;									/**< RHI */
		ResourceManager* m_pResourceManager;						/**< Resource manager */

		HandleManager<Material> m_MatHandleManager;					/**< Material handle manager */
		HandleManager<MaterialInstance> m_MatInstanceHandleManager;	/**< Material instance handle manager */

		DynArray<MaterialGroup*> m_Materials;						/**< Material groups */
		HashMap<String, u32> m_MaterialMapping;						/**< Mapping between material names and groups */
		HashMap<String, u32> m_InstanceMapping;						/**< Mapping between instance names and groups */
		DynArray<u32> m_FreeMaterialSlots;							/**< Available material slots that can be reused */
		DynArray<u32> m_FreeInstanceIds;							/**< Available material slots that can be reused */
		u32 m_CurInstanceId;
	};

}

#pragma warning(pop)
