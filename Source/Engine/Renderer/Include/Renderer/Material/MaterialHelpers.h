// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// MaterialHelpers.h: Material helpers
#pragma once
#include "RendererPCH.h"
#include "Material.h"
#include "Renderer/RendererCommon.h"

namespace Hv::Renderer::Helpers {
	
	/**
	 * Convert a material description into a custom material description
	 * @param[in] desc	Material description
	 * @return			Custom material description
	 */
	HIVE_API CustomMaterialDesc GetCustomMaterialDesc(const MaterialDesc& desc);

	/**
	 * Get the blend state for the material blend mode
	 * @param[in] blendMode		Material blend mode
	 * @return					Blend state
	 */
	HIVE_API BlendStateDesc GetMaterialBlendState(MaterialBlendMode blendMode);

	/**
	 * Get the rasterizer state for a material
	 * @param[in] twoSided	If the material is two sided
	 * @return				Rasterizer state
	 */
	HIVE_API RasterizerDesc GetMaterialRasterDesc(b8 twoSided);

	/**
	 * Get the material depth stencil description
	 * 
	 */
	HIVE_API DepthStencilDesc GetMaterialDepthStencilDesc(MaterialBlendMode blendMode);

	/**
	 * Get the corresponding pass for a domain
	 * @param[in] domain	Material domain
	 * @param[in] mode		Rendering mode
	 * @return				Pass identifier
	 */
	HIVE_API const AnsiChar* GetMaterialPass(MaterialDomain domain, RenderingMode mode);
	/**
	* Get the corresponding pass for a domain
	* @param[in] domain	Material domain
	* @return				Pass identifier
	*/
	InputDescriptor GetMaterialInputDescriptor(MaterialDomain domain);

	/**
	 * Get the size required for a primitive parameter
	 * @param[in] type	Parameter type
	 * @return			Parameter size
	 */
	u32 GetMaterialParamSize(MaterialParamType type);
}
