// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Renderer.h: Renderer
// TODO: Multi material meshes
#pragma once
#include "RendererPCH.h"
#include "RendererCommon.h"
#include "ResourceManager.h"
#include "ShaderManager.h"
#include "FrameGraph.h"
#include "View.h"

#include "Material/MaterialManager.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv::Renderer {

	/**
	 * Renderer description
	 */
	struct RendererDesc
	{
		RHIType rhiType;
		RHIValidationLevel rhiValidationLevel;
		VSyncMode defaultVSyncMode;
		RenderingMode renderingMode;
	};

	// Temp
	struct MeshData
	{
		MeshHandle mesh;
		u64 transformOffset;
	};

	class HIVE_API Renderer
	{
	public:
		/**
		 * Create the renderer
		 */
		Renderer();
		~Renderer();

		/**
		 * Initialize the renderer
		 * @param[in] desc	Renderer description
		 * @return			True if the renderer was initialized successfully false otherwise
		 */
		b8 Init(const RendererDesc& desc);
		/**
		 * Shutdown the renderer
		 * @return			True if the renderer was shut down successfully false otherwise
		 */
		b8 Shutdown();

		/**
		 * Tick the renderer
		 */
		void Draw();

		////////////////////////////////////////////////////////////////////////////////
		// Graphics																	  //
		////////////////////////////////////////////////////////////////////////////////

		// Temp
		/**
		 * Register mesh
		 */
		void RegisterMesh(MeshHandle mesh);

		/**
		 * Add a mesh and material to render
		 */
		void AddRenderable(f32m4& transform, MaterialInstanceHandle materialInstance, MeshHandle mesh);

		////////////////////////////////////////////////////////////////////////////////
		// Window																	  //
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Register a window to the renderer with the default v-sync mode
		 * @param[in] pWindow	Window to register
		 * @return				True if the window was registered successfully, false otherwise
		 */
		b8 RegisterWindow(Window* pWindow);
		/**
		 * Register a window to the renderer
		 * @param[in] pWindow	Window to register
		 * @param[in] vsync		V-sync mode
		 * @return				True if the window was registered successfully, false otherwise
		 */
		b8 RegisterWindow(Window* pWindow, VSyncMode vsync);
		/**
		 * Unregister a window to the renderer
		 * @param[in] pWindow	Window to unregister
		 * @return				True if the window was unregistered successfully, false otherwise
		 */
		b8 UnregisterWindow(Window* pWindow);
		/**
		* Unregister a window to the renderer using its Id
		* @param[in] id		Id of window to unregister
		* @return			True if the window was unregistered successfully, false otherwise
		*/
		b8 UnregisterWindow(u16 id);

		/**
		 * Set the default v-sync mode
		 * @param[in] vsync		V-sync mode
		 */
		void SetDefaultVSyncMode(VSyncMode vsync) { m_Desc.defaultVSyncMode = vsync; }

		/**
		 * Get the swapchain associated with a window
		 * @param[in] windowId	Window Id
		 * @return				Render view
		 * @note				[TEMPORARY]
		 */
		Core::SwapChain* GetSwapChain(u16 windowId);

		////////////////////////////////////////////////////////////////////////////////
		// Core (Allow plugins and apps to add things to renderer)					  //
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * [CORE] Add a shader to the renderer
		 * @param[in] desc	Shader description
		 * @return			Pointer to shader
		 * @note			This is a core function, this allows plugins to add an additional shaders, but is not meant for normal applications
		 */
		Core::Shader* CoreAddShader(const Core::ShaderDesc& desc);
		/**
		 * [CORE] Remove a shader from the renderer
		 * @param[in] pShader	Shader
		 * @return				True if the shader was successfully removed, false otherwise
		 * @note				This is a core function, this allows plugins to remove an additional shaders, but is not meant for normal applications
		 */
		b8 CoreRemoveShader(Core::Shader* pShader);
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * [CORE] Add a render pass to the renderer
		 * @param[in] attachments	Render pass attachments
		 * @param[in] subpasses		Sub passes
		 * @return					Pointer to renderpass
		 * @note						This is a core function, this allows plugins to add an additional render passes, but is not meant for normal applications
		 */
		Core::RenderPass* CoreAddRenderPass(const DynArray<Core::RenderPassAttachment>& attachments, const DynArray<Core::SubRenderPass>& subpasses);
		/**
		 * [CORE] Remove a render pass from the renderer
		 * @param[in] pRenderPass	Render pass
		 * @return					True if the render pass was successfully removed, false otherwise
		 * @note					This is a core function, this allows plugins to remove an additional render pass, but is not meant for normal applications
		 */
		b8 CoreRemoveRenderPass(Core::RenderPass* pRenderPass);
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * [CORE] Add a graphics pipeline to the renderer
		 * @param[in] desc	Graphics pipeline description
		 * @return			Pointer to pipeline
		 * @note			This is a core function, this allows plugins to add an additional graphics pipeline, but is not meant for normal applications
		 */
		Core::Pipeline* CoreAddPipeline(const Core::GraphicsPipelineDesc& desc);
		/**
		 * [CORE] Add a compute pipeline to the renderer
		 * @param[in] desc	Graphics pipeline description
		 * @return			Pointer to pipeline
		 * @note			This is a core function, this allows plugins to add an additional compute pipeline, but is not meant for normal applications
		 */
		Core::Pipeline* CoreAddPipeline(const Core::ComputePipelineDesc& desc);
		/**
		 * [CORE] Remove a pipeline from the renderer
		 * @param[in] pPipeline		Pipeline
		 * @return					True if the pipeline was successfully removed, false otherwise
		 * @note					This is a core function, this allows plugins to remove an additional pipeline, but is not meant for normal applications
		 */
		b8 CoreRemovePipeline(Core::Pipeline* pPipeline);
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * [CORE] Add a render pass to the renderer
		 * @param[in] renderTargets		Render targets
		 * @param[in] pRenderPass		Associated renderpass
		 * @return						Pointer to framebuffer
		 * @note						This is a core function, this allows plugins to add additional framebuffers, but is not meant for normal applications
		 */
		Core::Framebuffer* CoreAddFramebuffer(const DynArray<Core::RenderTarget*>& renderTargets, Core::RenderPass* pRenderPass);
		/**
		 * [CORE] Remove a framebuffer from the renderer
		 * @param[in] pFramebuffer	Framebuffer
		 * @return					True if the framebuffer was successfully removed, false otherwise
		 * @note					This is a core function, this allows plugins to remove an additional framebuffer, but is not meant for normal applications
		 */
		b8 CoreRemoveFramebuffer(Core::Framebuffer* pFramebuffer);

		/**
		 * [CORE] Create a command list
		 * @param[in] pQueue	Queue
		 * @return				Pointer to the command list
		 * @note				This is a core function, this allows plugins to create a command list, but is not meant for normal applications
		 */
		Core::CommandList* CoreAddCommandList(Core::Queue* pQueue);
		/**
		 * [CORE] Destroy a command list from the renderer
		 * @param[in] pCommandList	Command list
		 * @return					True if the command list was successfully destroyed, false otherwise
		 * @note					This is a core function, this allows plugins to destroy a command list, but is not meant for normal applications
		 */
		b8 CoreRemoveCommandList(Core::CommandList* pCommandList);
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * [CORE] Create a sampler
		 * @param[in] samplerDesc	Sampler description
		 * @return					Pointer to the sampler
		 * @note					This is a core function, this allows plugins to create a sampler, but is not meant for normal applications
		 */
		Core::Sampler* CoreAddSampler(const Core::SamplerDesc& samplerDesc);
		/**
		 * [CORE] Destroy a sampler from the renderer
		 * @param[in] pSampler	Sampler
		 * @return				True if the sampler was successfully destroyed, false otherwise
		 * @note				This is a core function, this allows plugins to destroy a sampler, but is not meant for normal applications
		 */
		b8 CoreRemoveSampler(Core::Sampler* pSampler);
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * [CORE] Create a texture
		 * @param[in] desc				Texture description
		 * @param[in] owningQueueType	Type of queue that owns the texture
		 * @return						Pointer to the texture
		 * @note						This is a core function, this allows plugins to create a texture, but is not meant for normal applications
		 */
		Core::Texture* CoreAddTexture(const Core::TextureDesc& desc, QueueType owningQueueType);
		/**
		 * [CORE] Create a texture
		 * @param[in] desc				Texture description
		 * @param[in] owningQueueType	Type of queue that owns the texture
		 * @param[in] pImage			Image with texture data
		 * @return						Pointer to the texture
		 * @note						This is a core function, this allows plugins to create a texture, but is not meant for normal applications
		 */
		Core::Texture* CoreAddTexture(const Core::TextureDesc& desc, QueueType owningQueueType, Image* pImage);
		/**
		 * [CORE] Create a texture
		 * @param[in] desc				Texture description
		 * @param[in] owningQueueType	Type of queue that owns the texture
		 * @param[in] pData				Raw texture data
		 * @param[in] dataSize			Raw texture data size in bytes
		 * @return						Pointer to the texture
		 * @note						This is a core function, this allows plugins to create a texture, but is not meant for normal applications
		 */
		Core::Texture* CoreAddTexture(const Core::TextureDesc& desc, QueueType owningQueueType, u8* pData, u64 dataSize);
		/**
		 * [CORE] Create a texture
		 * @param[in] pImage			Image with texture data
		 * @param[in] samples			Texture samples
		 * @param[in] flags				Texture flags
		 * @param[in] layout			Texture layout
		 * @param[in] owningQueueType	Type of queue that owns the texture
		 * @return						Pointer to the texture
		 * @note						This is a core function, this allows plugins to create a texture, but is not meant for normal applications
		 */
		Core::Texture* CoreAddTexture(Image* pImage, SampleCount samples, TextureFlags flags, TextureLayout layout, QueueType owningQueueType);
		/**
		 * [CORE] Destroy a texture
		 * @param[in] pTexture	Texture
		 * @return				True if the texture was successfully destroyed, false otherwise
		 * @note				This is a core function, this allows plugins to destroy a texture, but is not meant for normal applications
		 */
		b8 CoreRemoveTexture(Core::Texture* pTexture);
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * [CORE] Create a render target
		 * @param[in] desc	Render target description
		 * @return			Pointer to the render target
		 * @note			This is a core function, this allows plugins to create a render target, but is not meant for normal applications
		 */
		Core::RenderTarget* CoreAddRenderTarget(const Core::RenderTargetDesc& desc);
		/**
		 * [CORE] Destroy a render target
		 * @param[in] pRT	Render target
		 * @return			True if the render target was successfully destroyed, false otherwise
		 * @note			This is a core function, this allows plugins to destroy a render target, but is not meant for normal applications
		 */
		b8 CoreRemoveRenderTarget(Core::RenderTarget* pRT);
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * [CORE] Create a buffer
		 * @param[in] type		Buffer type
		 * @param[in] size		Buffer size
		 * @param[in] flags		Buffer flags
		 * @return				Pointer to the command list
		 * @note				This is a core function, this allows plugins to create a buffer, but is not meant for normal applications
		 */
		Core::Buffer* CoreAddBuffer(BufferType type, u64 size, BufferFlags flags);
		/**
		 * [CORE] Create a vertex buffer
		 * @param[in] vertexCount	Vertex count
		 * @param[in] vertexSize	Vertex size
		 * @param[in] flags			Buffer flags
		 * @return					Pointer to the command list
		 * @note					This is a core function, this allows plugins to create a buffer, but is not meant for normal applications
		 */
		Core::Buffer* CoreAddVertexBuffer(u32 vertexCount, u16 vertexSize, BufferFlags flags);
		/**
		 * [CORE] Create an index buffer
		 * @param[in] indexCount	Vertex count
		 * @param[in] indexType		Index type
		 * @param[in] flags			Buffer flags
		 * @return					Pointer to the command list
		 * @note					This is a core function, this allows plugins to create a buffer, but is not meant for normal applications
		 */
		Core::Buffer* CoreAddIndexuffer(u32 indexCount, IndexType indexType, BufferFlags flags);
		/**
		 * [CORE] Destroy a buffer from the renderer
		 * @param[in] pBuffer	Buffer
		 * @return				True if the command list was successfully destroyed, false otherwise
		 * @note				This is a core function, this allows plugins to destroy a buffer, but is not meant for normal applications
		 */
		b8 CoreRemoveBuffer(Core::Buffer* pBuffer);
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * [CORE] Create a descriptor set
		 * @param[in] bindings	Desriptor set bindings
		 * @return				Pointer to the descriptor set
		 * @note				This is a core function, this allows plugins to create a descriptor set, but is not meant for normal applications
		 */
		Core::DescriptorSet* CoreAddDescriptorSet(const DynArray<Core::DescriptorSetBinding>& bindings);
		/**
		 * [CORE] Destroy a descriptor set from the renderer
		 * @param[in] pDescriptorSet	Descriptor set
		 * @return						True if the descriptor set was successfully destroyed, false otherwise
		 * @note						This is a core function, this allows plugins to destroy a descriptor set, but is not meant for normal applications
		 */
		b8 CoreRemoveDescriptorSet(Core::DescriptorSet* pDescriptorSet);
		////////////////////////////////////////////////////////////////////////////////
		/**
		* Get the dynamic RHI
		* @return	Dynamic RHI
		* @note		[TEMPORARY]
		*/
		RHI::IDynamicRHI* GetDynamicRHI() { return m_pRHI; }
		/**
		 * Get the frame graph
		 * @return	Frame graph
		 */
		FrameGraph* GetFrameGraph() { return m_pFrameGraph; }
		/**
		* Get the frame graph
		* @return	Frame graph
		*/
		MaterialManager* GetMaterialManager() { return m_pMaterialManager; }
		/**
		 * Get the current rendering mode
		 * @return	Rendering mode
		 */
		RenderingMode GetRenderingMode() { return m_Desc.renderingMode; }

		// Temp
		DynArray<Pair<MaterialHandle, DynArray<Pair<MaterialInstanceHandle, DynArray<MeshData>>>>>& GetRenderables() { return m_Renderables; }
		Pair<Core::Buffer*, Core::Buffer*> GetBuffers(MeshHandle mesh);
		View& GetMainView() { return m_View; }
		Core::DescriptorSetLayout* GetFrameLayout() { return m_pFrameLayout; }
		Core::DescriptorSetLayout* GetViewLayout() { return m_pViewLayout; }
		Core::DescriptorSetLayout* GetObjectLayout() { return m_pObjectLayout; }
		Core::DescriptorSet* GetTransformDescriptorSet() { return m_pTransformDescriptorSet; }

	private:

		RendererDesc m_Desc;									/**< Renderer description */

		DynLib::DynLibHandle m_RHIDynLib;						/**< Handle to RHI dynlib */
		RHI::IDynamicRHI* m_pRHI;								/**< Dynamic RHI */

		FrameGraph* m_pFrameGraph;								/**< Frame graph */

		ResourceManager* m_pResourceManager;					/**< Resource manager */
		ShaderManager* m_pShaderManager;						/**< Shader manager */
		MaterialManager* m_pMaterialManager;					/**< Material manager */

		////////////////////////////////////////////////////////////////////////////////
		// Renderer data															  //
		////////////////////////////////////////////////////////////////////////////////

		// TODO: Decent system to handle rendering of objects
		// Temp
		Core::Buffer* m_pTransformBuffer;
		Core::DescriptorSetLayout* m_pTransformDescriptorLayout;
		Core::DescriptorSet* m_pTransformDescriptorSet;
		static constexpr u32 MaxRenderedObjects = 1024; // For now

		DynArray<Pair<MaterialHandle, DynArray<Pair<MaterialInstanceHandle, DynArray<MeshData>>>>> m_Renderables;
		u32 m_NumRenderables = 0;
		DynArray<Pair<Core::Buffer*, Core::Buffer*>> m_MeshBuffers;
		View m_View;

		// TODO: Figure out better way for layouts
		Core::DescriptorSetLayout* m_pFrameLayout;
		Core::DescriptorSetLayout* m_pViewLayout;
		Core::DescriptorSetLayout* m_pObjectLayout;

		////////////////////////////////////////////////////////////////////////////////
		// Renderer core data														  //
		////////////////////////////////////////////////////////////////////////////////
		// Render views
		HashMap<u16, Core::SwapChain*> m_SwapChains;			/**< Renderviews */

		// Render passes
		DynArray<Core::RenderPass*> m_RenderPasses;				/**< Rener passes */

		// Pipelines
		DynArray<Core::Pipeline*> m_Pipelines;					/**< Pipelines */

		// Framebuffers
		DynArray<Core::Framebuffer*> m_Framebuffers;			/**< Framebuffers */

		// Command lists
		Core::CommandListManager* m_pCommandListManager;		/**< Command list manager */

		// Textures and samplers
		DynArray<Core::Sampler*> m_Samplers;					/**< Samplers */

		// Descriptor sets
		Core::DescriptorSetManager* m_pDescriptorSetManager;	/**< DescriptorSetManager */
	};

	HIVE_API Renderer& GetRenderer();

}

#define g_Renderer Hv::Renderer::GetRenderer()

#pragma warning(pop)