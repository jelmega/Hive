// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ShaderManager.h: Shader manager
#pragma once
#include "RendererPCH.h"

namespace Hv::Renderer {
	
	class ShaderManager
	{
	private:
		struct ShaderData
		{
			Core::ShaderDesc desc;
			Core::Shader* pShader;
		};

	public:
		ShaderManager();
		~ShaderManager();

		/**
		 * Create the shader manager
		 * @param[in] pRHI	RHI
		 * @return			True if the shader manager was created successfully, false otherwise
		 */
		b8 Create(RHI::IDynamicRHI* pRHI);
		/**
		 * Cleanup all shader
		 * @return	True if all shaders were cleaned up correctly, false otherwise
		 */
		b8 Destroy();

		/**
		 * Load a shader
		 * @param[in] desc	Shader description
		 * @return			Shader
		 */
		Core::Shader* LoadShader(const Core::ShaderDesc& desc);
		/**
		 * Unload a shader
		 * @param[in] path	Path to the shader to unload
		 * @return			True if the shader was loaded successfully, false otherwise
		 */
		b8 UnloadShader(const String& path);
		/**
		 * Unload a shader
		 * @param[in] pShader	Shader to unload
		 * @return			True if the shader was loaded successfully, false otherwise
		 */
		b8 UnloadShader(Core::Shader* pShader);

	private:
		RHI::IDynamicRHI* m_pRHI;				/**< RHI */
		HashMap<String, ShaderData> m_Shaders;	/**< Map with shaders */

	};

}
