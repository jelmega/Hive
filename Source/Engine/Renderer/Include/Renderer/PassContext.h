// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PassContext.h: Render pass context
#pragma once
#include "RendererPCH.h"
#include "Material/Material.h"
#include "Material/MaterialInstance.h"

namespace Hv::Renderer {
	struct View;
	class MaterialInstance;
	class Material;

	class PassContext
	{
	public:
		PassContext(RHI::IDynamicRHI* pRHI, Core::RenderPass* pRenderPass);
		~PassContext();

		/**
		 * Reset the pass context for the next frame
		 */
		void Reset();

		/**
		 * Add a render target
		 * @param[in] pRenderTarget		Render target
		 */
		void AddRenderTarget(Core::RenderTarget* pRenderTarget);
		/**
		 * Set the render target
		 * @param[in] pDepthStencil		Depth stencil
		 */
		void SetDepthStencil(Core::RenderTarget* pDepthStencil);
		/**
		 * Bind the render targets
		 */
		void BindRenderTargets();

		/**
		 * Set the material to use
		 * @param[in] material		Material
		 */
		void SetMaterial(MaterialHandle material);
		/**
		 * Set the material instance to use
		 * @param[in] matInstance	Material instance
		 * @note	Instance need to be an instance of the current material that is set
		 */
		void SetMaterialInstance(MaterialInstanceHandle matInstance);

		/**
		 * Set the active view
		 * @param[in] view	View
		 */
		void SetView(View& view);

		/**
		 * Draw a mesh
		 * @param[in] mesh		Mesh to draw
		 */
		void DrawMesh(MeshHandle mesh, Core::DescriptorSet* pTransformDescriptorSet, u64 transformOffset);

		/**
		 * Get the underlying command list
		 * @return	Command list
		 */
		Core::CommandList* GetCommandList() { return m_pCommandList; }

	private:
		RHI::IDynamicRHI* m_pRHI;
		Core::RenderPass* m_pRenderPass;

		Core::CommandList* m_pCommandList;

		DynArray<Core::RenderTarget*> m_RenderTargets;
		Core::RenderTarget* m_pDepthStencil;
		Core::Framebuffer* m_pFramebuffer;
		b8 m_InRenderPass;

		MaterialHandle m_CurMaterial;
	};

}
