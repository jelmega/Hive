// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// FrameGraph.h: Frame graph
#pragma once
#include "RendererPCH.h"
#include "ResourceManager.h"

namespace Hv::Renderer {
	class PassContext;

	class Pass;
	class FrameGraphRegistry;
	
	HV_CREATE_HANDLE(FrameGraphResource);
	HV_CREATE_INVALID_HANDLE(FrameGraphResource, 0xFFFF'FFFF'FFFF'FFFF);

	enum class FrameGraphWriteFlags
	{
		None,
		AsColor,
		AsDepthStencil
	};

	enum class FrameGraphReadFlags
	{
		None,
		AsColor,
		AsDepthStencil
	};

	enum class FrameGraphResourseState
	{
		Clear,	/**< Clear the resource */
		Load	/**< Load the resource */
	};

	struct FrameGraphTextureDesc
	{
		u32 width;								/**< Texture width */
		u32 height;								/**< Texture height */
		PixelFormat format;						/**< Texture format */
		FrameGraphResourseState initialState;	/**< Initial resource state */
	};

	struct FrameGraphBufferDesc
	{
		u64 size;								/**< Buffer size */
		PixelFormat texelFormat;				/**< Format for texel buffer (undefined when buffer isn't a texel buffer) */
		FrameGraphResourseState initialState;	/**< Initial resource state */
	};

	namespace Detail {
		
		struct FrameGraphAlias
		{
			b8 isMutable : 1;
			b8 isImported : 1;
			b8 isBuffer : 1;
			b8 isCreated : 1;
			u8 refCount;
			u16 descIdx;		/**< description index */
			u16 idenIdx;		/**< Identifier index */
			u16 transientIdx;	/**< Transient range index */
			u16 resIdx;			/**< Physical resource index */
		};

		struct FrameGraphPass
		{
			String identifier;													/**< Indentifier */
			Pass* pPass;														/**< Pass */

			Core::RenderPass* pRenderPass;										/**< Render pass */
			PassContext* pContext;												/**< Pass context */

			DynArray<Pair<FrameGraphResource, FrameGraphReadFlags>> reads;		/**< Resources read by the pass */
			DynArray<Pair<FrameGraphResource, FrameGraphWriteFlags>> writes;	/**< Resources written to by the pass */
			DynArray<FrameGraphResource> creates;								/**< Resources created by the pass */

			u8 refCount;
		};

		struct FrameGraphTransientResourceRange
		{
			b8 initialized;
			b8 isBuffer;
			u16 descIdx;
			u16 firstPass;
			u16 lastPass;
		};

		struct FrameGraphTransientResource
		{
			b8 isBuffer;
			u16 descIdx;
			u16 resIdx;
			u16 lastPass;
		};
	}

	/**
	 * Frame graph resources\n
	 * Allows users to retrieve the needed resources
	 */
	class FrameGraphResources
	{
	public:
		FrameGraphResources(RHI::IDynamicRHI* pRHI, ResourceManager* pResourceManager, FrameGraphRegistry* pRegistry);
		~FrameGraphResources();

		/**
		 * Add a resource
		 * @param[in] desc	Texture description
		 */
		void AddResource(const FrameGraphTextureDesc& desc);
		/**
		 * Add a resource
		 * @param[in] desc	Buffer description
		 */
		void AddResource(const FrameGraphBufferDesc& desc);

		/**
		 * Reset the resources
		 */
		void Reset();
		/**
		 * Cleanup the resources
		 */
		void Cleanup();

		/**
		 * Retrieve the resource's render target
		 * @param[in] resource	Resource
		 * @return				Render target
		 */
		Core::RenderTarget* GetRenderTarget(FrameGraphResource resource);
		/**
		 * Retrieve the resource's render target
		 * @param[in] resource	Resource
		 * @return				Render target
		 */
		Core::Texture* GetTexture(FrameGraphResource resource);
		/**
		 * Retrieve the resource's render target
		 * @param[in] resource	Resource
		 * @return				Render target
		 */
		Core::Buffer* GetBuffer(FrameGraphResource resource);

	private:
		struct Resource
		{
			Resource()
				: isBuffer(false)
				, tex()
			{}

			b8 isBuffer;
			union
			{
				struct Tex
				{
					FrameGraphTextureDesc desc;
					Core::RenderTarget* pRT;
				} tex;
				struct Buf
				{
					FrameGraphBufferDesc desc;
					Core::Buffer* pBuffer;
				} buf;
			};
		};

		RHI::IDynamicRHI* m_pRHI;
		FrameGraphRegistry* m_pRegistry;
		ResourceManager* m_pResourceManager;

		DynArray<Resource> m_Resources;
		DynArray<b8> m_Availability;
		DynArray<u32> m_ResourceMapping;
	};

	////////////////////////////////////////////////////////////////////////////////
	/**
	 * Frame graph registry\n
	 * Registry of registered resources in the entire frame graph
	 */
	class FrameGraphRegistry
	{
	public:
		FrameGraphRegistry();
		~FrameGraphRegistry();

		/**
		 * Create a texture resource from a description
		 * @param[in] iden			Resource identifier
		 * @param[in] desc			Texture desc
		 * @param[in] isMutable		If the resource is mutable
		 * @param[in] isImported	If the resource is imported
		 * @return					Resource handle
		 */
		FrameGraphResource Create(const String& iden, const FrameGraphTextureDesc& desc, b8 isMutable = true, b8 isImported = false);
		/**
		 * Create a buffer resource from a description
		 * @param[in] iden			Resource identifier
		 * @param[in] desc			Buffer desc
		 * @param[in] isMutable		If the resource is mutable
		 * @param[in] isImported	If the resource is imported
		 * @return					Resource handle
		 */
		FrameGraphResource Create(const String& iden, const FrameGraphBufferDesc& desc, b8 isMutable = true, b8 isImported = false);

		/**
		 * Get a resource based on it's identifier
		 * @param[in] iden	Resource identifier
		 * @return			Resource handle
		 */
		FrameGraphResource GetResource(const String& iden);

		/**
		 * Create a new resource alias and handle (post change)
		 * @param[in] resource	Resource handle
		 * @return				New resource handle
		 */
		FrameGraphResource CreateNewAlias(FrameGraphResource resource);
		/**
		 * Create a new resource alias and handle (post change)
		 * @param[in] iden	Resource identifier
		 * @return			New resource handle
		 */
		FrameGraphResource CreateNewAlias(const String& iden);

		/**
		 * Get the alias for a resource
		 * @param[in] resource	Resource
		 * @return				Resource alias
		 */
		Detail::FrameGraphAlias& GetAlias(FrameGraphResource resource) { return m_Aliases[sizeT(resource)]; }
		/**
		 * Get all resource aliases
		 * @return Resource aliases
		 */
		DynArray<Detail::FrameGraphAlias>& GetAliases() { return m_Aliases; }

		/**
		 * Get the identifier for a resource
		 * @param[in] resource	Resource
		 * @return				Resource identifier
		 */
		const String& GetIdentifier(FrameGraphResource resource) { return m_Identifiers[m_Aliases[u64(resource)].idenIdx]; }

		/**
		 * Get the description for a texture resource
		 * @param[in] resource	Resource
		 * @return				Texture description
		 */
		FrameGraphTextureDesc& GetTextureDescription(FrameGraphResource resource) { return m_TexDescs[m_Aliases[u64(resource)].descIdx]; }
		/**
		* Get the description for a texture resource
		* @param[in] index	Description index
		* @return			Texture description
		*/
		FrameGraphTextureDesc& GetTextureDescription(u32 index) { return m_TexDescs[index]; }
		/**
		 * Get the description for a buffer resource
		 * @param[in] resource	Resource
		 * @return				Buffer description
		 */
		FrameGraphBufferDesc& GetBufferDescription(FrameGraphResource resource) { return m_BufDescs[m_Aliases[u64(resource)].descIdx]; }
		/**
		* Get the description for a buffer resource
		* @param[in] index	Description index
		* @return			Buffer description
		*/
		FrameGraphBufferDesc& GetBufferDescription(u32 index) { return m_BufDescs[index]; }

	private:
		DynArray<Detail::FrameGraphAlias> m_Aliases;
		DynArray<FrameGraphTextureDesc> m_TexDescs;
		DynArray<FrameGraphBufferDesc> m_BufDescs;
		DynArray<String> m_Identifiers;

		DynArray<FrameGraphResource> m_CurResources;
		HashMap<String, u32> m_CurResIdenMapping;
	};

	////////////////////////////////////////////////////////////////////////////////
	/**
	 * Frame graph builder\n
	 * Used when defining each framepass
	 */
	class FrameGraphBuilder
	{
	public:
		/**
		 * Create a frame graph builder
		 * @param[in] pPass			Pass to build
		 * @param[in] pRegistry		Registry
		 */
		FrameGraphBuilder(Detail::FrameGraphPass* pPass, FrameGraphRegistry* pRegistry);
		~FrameGraphBuilder();

		/**
		 * Create a texture resource from a description
		 * @param[in] iden	Resource identifier
		 * @param[in] desc	Texture desc
		 * @return			Resource handle
		 */
		FrameGraphResource Create(const String& iden, const FrameGraphTextureDesc& desc);
		/**
		 * Create a buffer resource from a description
		 * @param[in] iden	Resource identifier
		 * @param[in] desc	Buffer desc
		 * @return			Resource handle
		 */
		FrameGraphResource Create(const String& iden, const FrameGraphBufferDesc& desc);

		/**
		 * Import a texture resource defined by a description
		 * @param[in] iden			Resource identifier
		 * @param[in] desc			Texture desc
		 * @param[in] isMutable		If the resource is mutable
		 * @return					Resource handle
		 * @note					External resources are set during update
		 */
		FrameGraphResource Import(const String& iden, const FrameGraphTextureDesc& desc, b8 isMutable);
		/**
		 * Import a buffer resource defined by a description
		 * @param[in] iden			Resource identifier
		 * @param[in] desc			Buffer desc
		 * @param[in] isMutable		If the resource is mutable
		 * @return					Resource handle
		 * @note					External resources are set during update
		 */
		FrameGraphResource Import(const String& iden, const FrameGraphBufferDesc& desc, b8 isMutable);

		/**
		 * Read data from a resource
		 * @param[in] resource	Resource to read from
		 * @param[in] flags		Read flags
		 */
		FrameGraphResource Read(FrameGraphResource resource, FrameGraphReadFlags flags);
		/**
		 * Read data from a resource
		 * @param[in] iden		Resource identifier
		 * @param[in] flags		Read flags
		 */
		FrameGraphResource Read(const String& iden, FrameGraphReadFlags flags);
		/**
		 * Write data to a resource
		 * @param[in] resource	Resource to write to
		 * @param[in] flags		Write flags
		 */
		FrameGraphResource Write(FrameGraphResource resource, FrameGraphWriteFlags flags);
		/**
		 * Write data to a resource
		 * @param[in] iden		Resource identifier
		 * @param[in] flags		Write flags
		 */
		FrameGraphResource Write(const String& iden, FrameGraphWriteFlags flags);

	private:
		Detail::FrameGraphPass* m_pPass;
		FrameGraphRegistry* m_pRegistry;
	};

	////////////////////////////////////////////////////////////////////////////////
	/**
	 * Frame graph\n
	 * Optimizes the rendering for each frame
	 */
	class FrameGraph
	{
	public:
		FrameGraph(RHI::IDynamicRHI* pRHI, ResourceManager* pResourceManager);
		~FrameGraph();

		/**
		 * Add a pass to the framegraph
		 * @note	The framegraph takes ownership of the pass (create pass in arguments i.e. AddPass(HvNew Pass());)
		 */
		void AddPass(Pass* pPass);

		/**
		 * Initialize all required data for passes (called after adding all passes)
		 */
		void Initialize();

		/**
		 * Destroy the framegraph and its resources
		 */
		void Destroy();

		/**
		 * Compile the frame graph
		 */
		void Compile();

		/**
		 * Execute the frame graph
		 */
		void Execute();

		/**
		 * Export the graph to a graphviz file
		 * @param[in] filepath	Path to export to
		 */
		void ExportGraphViz(const String& filepath);

		/**
		 * Get the renderpass for a pass
		 * @param[in] iden	Identifier
		 * @return			Render pass
		 */
		Core::RenderPass* GetRenderPass(const String& iden);

	private:
		RHI::IDynamicRHI* m_pRHI;
		FrameGraphRegistry* m_pRegistry;
		FrameGraphResources* m_pResources;

		HashMap<String, u32> m_IdenMapping;
		DynArray<Detail::FrameGraphPass> m_Passes;
		DynArray<u32> m_CulledPasses;

	};

}

#include "Pass.h"
#include "PassContext.h"