// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// RendererCommon.h: Renderer common
#pragma once
#include "RendererPCH.h"

namespace Hv::Renderer {

	/**
	* Rendering mode
	*/
	enum class RenderingMode
	{
		Deferred,
		Forward,
		ForwardPlus,
		//Custom, // Future option ???
	};

}