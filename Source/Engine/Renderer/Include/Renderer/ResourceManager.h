// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ResourceManager.h: Graphics resource manager
#pragma once
#include "RendererPCH.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv::Renderer {
	
	/**
	 * Resource manager
	 * @note	Manager resource like buffers, textures (resources which rely on large chunks of GPU manager)
	 */
	class HIVE_API ResourceManager
	{
	public:
		ResourceManager();
		~ResourceManager();

		/**
		 * Create the resouce manager
		 * @param[in] pRHI					RHI
		 * @param[in] pCommandListManager	Command list manager
		 */
		void Create(RHI::IDynamicRHI* pRHI, Core::CommandListManager* pCommandListManager);

		/**
		 * Cleanup all resources
		 * @return	True if all resources were destroyed successfully, false otherwise
		 */
		b8 Destroy();

		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Create a texture
		 * @param[in] desc				Texture description
		 * @param[in] owningQueueType	Type of queue that owns the texture
		 * @return						Pointer to the texture
		 */
		Core::Texture* CreateTexture(const Core::TextureDesc& desc, QueueType owningQueueType);
		/**
		 * Create a texture
		 * @param[in] desc				Texture description
		 * @param[in] owningQueueType	Type of queue that owns the texture
		 * @param[in] pData				Raw texture data
		 * @param[in] dataSize			Raw texture data size in bytes
		 * @return						Pointer to the texture
		 */
		Core::Texture* CreateTexture(const Core::TextureDesc& desc, QueueType owningQueueType, u8* pData, u64 dataSize);
		/**
		 * Destroy a texture
		 * @param[in] pTexture	Texture
		 * @return				True if the texture was successfully destroyed, false otherwise
		 */
		b8 DestroyTexture(Core::Texture* pTexture);
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Create a render target
		 * @param[in] desc				Render target description
		 * @return						Pointer to the render target
		 */
		Core::RenderTarget* CreateRenderTarget(const Core::RenderTargetDesc& desc);
		/**
		 * Destroy a render target
		 * @param[in] pRT	Render target
		 * @return			True if the render target was successfully destroyed, false otherwise
		 */
		b8 DestroyRenderTarget(Core::RenderTarget* pRT);
		////////////////////////////////////////////////////////////////////////////////
		/**
		 * Create a buffer
		 * @param[in] type		Buffer type
		 * @param[in] size		Buffer size
		 * @param[in] flags		Buffer flags
		 * @return				Pointer to the command list
		 */
		Core::Buffer* CreateBuffer(BufferType type, u64 size, BufferFlags flags);
		/**
		 * Create a vertex buffer
		 * @param[in] vertexCount	Vertex count
		 * @param[in] vertexSize	Vertex size
		 * @param[in] flags			Buffer flags
		 * @return					Pointer to the command list
		 */
		Core::Buffer* CreateVertexBuffer(u32 vertexCount, u16 vertexSize, BufferFlags flags);
		/**
		 * Create an index buffer
		 * @param[in] indexCount	Vertex count
		 * @param[in] indexType		Index type
		 * @param[in] flags			Buffer flags
		 * @return					Pointer to the command list
		 */
		Core::Buffer* CreateIndexuffer(u32 indexCount, IndexType indexType, BufferFlags flags);
		/**
		 * Destroy a buffer from the renderer
		 * @param[in] pBuffer	Buffer
		 * @return				True if the command list was successfully destroyed, false otherwise
		 */
		b8 DestroyBuffer(Core::Buffer* pBuffer);
		////////////////////////////////////////////////////////////////////////////////

		/**
		 * Get the used texture memory
		 * @return	Used texture memory
		 */
		u64 GetUsedTextureMemory() const { return m_UsedTextureMemory; }
		/**
		 * Get the used buffer memory
		 * @return	Used buffer memory
		 */
		u64 GetUsedBufferMemory() const { return m_UsedBufferMemory; }
		/**
		* Get the used render target memory
		* @return	Used render target memory
		*/
		u64 GetUsedRenderTargetMemory() const { return m_UsedRenderTargetMemory; }
		/**
		 * Get the used resource memory
		 * @return	Used resource memory
		 */
		u64 GetUsedMemory() const { return m_UsedTextureMemory + m_UsedBufferMemory + m_UsedRenderTargetMemory; }

	private:
		RHI::IDynamicRHI* m_pRHI;								/**< Dynamic RHI */
		Core::CommandListManager* m_pCommandListManager;		/**< Command list manager */

		// Resources
		DynArray<Core::Texture*> m_Textures;					/**< Textures */
		DynArray<Core::Buffer*> m_Buffers;						/**< Buffers */
		DynArray<Core::RenderTarget*> m_RenderTargets;			/**< Render targets */

		u64 m_UsedTextureMemory;								/**< Used texture GPU memory */
		u64 m_UsedBufferMemory;									/**< Used buffer GPU memory */
		u64 m_UsedRenderTargetMemory;							/**< Used render target GPU memory */
	};

}

#pragma warning(pop)
