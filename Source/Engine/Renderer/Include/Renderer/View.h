// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// View.h: View for rendering
#pragma once
#include "RendererPCH.h"

#pragma warning(push)
#pragma warning(disable : 4201) // nonstandard extension used: nameless struct/union

namespace Hv::Renderer {

	/**
	 * View used for rendering (gets subscribed to passes)
	 */
	struct View
	{
		b8 isOrthographic;
		f32m4 projection;
		f32m4 view;
		f32 znear;
		f32 zfar;

		union
		{
			struct
			{
				f32 fov;
				f32 aspect;
			} perspective;
			struct
			{
				i32 top;
				i32 left;
				i32 bottom;
				i32 right;
			} ortho;
		};

		Viewport viewport;
		ScissorRect scissor;

		Core::DescriptorSet* pDescriptorSet;
		Core::Buffer* pBuffer;
	};
}

#pragma warning(pop)
