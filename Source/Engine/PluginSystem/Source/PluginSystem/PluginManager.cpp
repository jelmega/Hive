// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PluginManager.cpp: Plugin manager
#include "PluginSystemPCH.h"
#include "PluginSystem/PluginManager.h"
#include "PluginSystem/IPlugin.h"

HV_DECLARE_LOG_CATEGORY(PluginSys, Hv::LogLevel::All);

namespace Hv::PluginSystem {

	PluginManager::PluginManager()
	{
	}

	PluginManager::~PluginManager()
	{
	}

	// TODO: Multi module plugins + store plugin info
	b8 PluginManager::Init()
	{
		// Go through plugin folder and find plugins, load dll and call OnLoad function

		// Iterate over all directories in the plugin folder
		DynArray<HvFS::DirectoryEntry> entries = HvFS::FindPatern("$plugins", "*.hvplugin");

		b8 loadRes = true;
		for (FileSystem::DirectoryEntry& entry : entries)
		{
			PluginInfo info = {};
			// Read .hvplugin and set info
			Json pluginJson = JsonParser::ParseFile(entry.GetPath().ToString());

			// Extract module name
			Json& modules = pluginJson["Modules"];
			if (!modules.IsArray())
			{
				g_Logger.LogError(LogPluginSys(), ".hvplugin does not contain 'Modules' array!");
				continue;;
			}
			if (modules.ArraySize() == 0)
			{
				g_Logger.LogError(LogPluginSys(), ".hvplugin's 'Modules' array is empty!");
				continue;;
			}
			Json& module = modules[0u];
			if (!modules.IsArray())
			{
				g_Logger.LogFormat(LogPluginSys(), LogLevel::Error, ".hvplugin's 'Modules' array element %u is not an object!", 0);
				continue;;
			}
			Json& moduleNameJson = module["Name"];
			if (!moduleNameJson.IsString())
			{
				g_Logger.LogFormat(LogPluginSys(), LogLevel::Error, ".hvplugin's 'Modules' array element %u has invalid name!", 0);
				continue;;
			}
			String moduleName = moduleNameJson;

			// Load plugin
			HvFS::Path parentPath = entry.GetPath().ParentPath();
			HvFS::Path pluginDynLibPath = parentPath / moduleName + Hv::DynLib::g_Extension;
			info.handle = DynLib::Load(pluginDynLibPath.ToString());
			if (info.handle == DynLib::InvalidDynLibHandle)
			{
				g_Logger.LogFormat(LogPluginSys(), LogLevel::Error, "Failed to load dynamic library for plugin '%s'", entry.GetPath().Filename().Stem().ToString());
				loadRes &= false;
				continue;
			}
			DynLib::DynLibProcHandle loadPlugin = DynLib::GetProc(info.handle, "LoadPlugin");
			if (loadPlugin == DynLib::InvalidDynLibProcHandle)
			{
				g_Logger.LogFormat(LogPluginSys(), LogLevel::Error, "Failed to load 'LoadPlugin' procedure for plugin '%s'", entry.GetPath().Filename().Stem().ToString());
				DynLib::Unload(info.handle);
				loadRes &= false;
				continue;
			}

			using LoadPluginFunc = IPlugin*(*)();

			info.pPlugin = ((LoadPluginFunc)loadPlugin)();
			if (!info.pPlugin)
			{
				g_Logger.LogFormat(LogPluginSys(), LogLevel::Error, "Failed to load  plugin '%s'", entry.GetPath().Filename().Stem().ToString());
				DynLib::Unload(info.handle);
				loadRes &= false;
				continue;
			}
			b8 res = info.pPlugin->OnLoad();
			if (!res)
			{
				HvDelete info.pPlugin;
				DynLib::Unload(info.handle);
			}

			m_Plugins.Push(info);
		}


		return loadRes;
	}

	b8 PluginManager::Shutdown()
	{
		b8 res = true;
		for (PluginInfo& info : m_Plugins)
		{
			if (info.pPlugin)
			{
				res &= info.pPlugin->OnUnload();
				HvDelete info.pPlugin;
			}

			if (info.handle != DynLib::InvalidDynLibHandle)
			{
				DynLib::Unload(info.handle);
			}
		}
		m_Plugins.Clear();

		return res;
	}

	b8 PluginManager::RegisterPlugins()
	{
		b8 res = true;
		for (PluginInfo& info : m_Plugins)
		{
			res &= info.pPlugin->OnRegister();
		}
		return res;
	}

	b8 PluginManager::UnregisterPlugins()
	{
		b8 res = true;
		for (PluginInfo& info : m_Plugins)
		{
			res &= info.pPlugin->OnUnregister();
		}
		return res;
	}

	PluginManager& GetPluginManager()
	{
		static PluginManager manager;
		return manager;
	}
}
