// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PluginSystemPCH.h: Plugin system Precompiled header
#pragma once
#include <Core.h>
#include HV_INCLUDE_RTTI_MODULE(PluginSystem)

HV_DECLARE_LOG_CATEGORY_EXTERN(PluginSys)