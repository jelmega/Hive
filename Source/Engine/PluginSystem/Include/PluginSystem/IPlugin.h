// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// IPlugin.h: Plugin interface
#pragma once
#include "PluginSystemPCH.h"

namespace Hv::PluginSystem {
	
	class HIVE_API IPlugin
	{
	public:
		IPlugin() {}
		virtual ~IPlugin() {}

		/**
		 * This function is called when the plugin is loaded, use this for pre initialization actions
		 * @note Not all systems are created, do not use this function to register anything to the engine
		 */
		virtual b8 OnLoad() = 0;
		/**
		* This function is called when the plugin is registered, use this to initialize and/or register systems
		*/
		virtual b8 OnRegister() = 0;
		/**
		* This function is called when the plugin is enabled during runtime
		*/
		virtual b8 OnEnable() = 0;
		/**
		* This function is called when the plugin is disabled during runtime
		*/
		virtual b8 OnDisable() = 0;
		/**
		* This function is called when the plugin is unregistered, use this to shutdown and/or unregister systems
		*/
		virtual b8 OnUnregister() = 0;
		/**
		* This function is called when the plugin is unloaded, use this for post shutdown actions
		* @note Not all systems are created, do not use this function to register anything to the engine
		*/
		virtual b8 OnUnload() = 0;

	};

	// Expected function in plugin:
	//extern "C"
	//{
	//	/**
	//	 * Generate the plugin for the engine to use
	//	 * @return	Generated plugin
	//	 */
	//	HIVE_API IApplication* LoadPlugin();
	//}
	//
	// From here on, no other functions need to be loaded from the dynamic library

}