// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PluginManager.h: Plugin manager
#pragma once
#include "PluginSystemPCH.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv::PluginSystem {
	
	class IPlugin;

	struct PluginInfo
	{
		DynLib::DynLibHandle handle;
		IPlugin* pPlugin;
	};

	class HIVE_API PluginManager
	{
	public:
		PluginManager();
		~PluginManager();

		/**
		 * Initialize the plugin manager
		 * @return	True if the plugin manager has been initialize successfully, false otherwise
		 */
		b8 Init();
		/**
		 * Shutdown the plugin manager
		 * @return	True if the plugin manager has been shut down successfully, false otherwise
		 */
		b8 Shutdown();

		/**
		 * Register plugins
		 * @return	True if the all plugins were registered successfully, false otherwise
		 */
		b8 RegisterPlugins();
		/**
		 * Unregister plugins
		 * @return	True if the all plugins were unregistered successfully, false otherwise
		 */
		b8 UnregisterPlugins();

	private:
		HV_MAKE_NON_COPYABLE(PluginManager);

		DynArray<PluginInfo> m_Plugins;	/**< Plugins */
	};

	HIVE_API PluginManager& GetPluginManager();

}

#define g_PluginManager Hv::PluginSystem::GetPluginManager()

#pragma warning(pop)
