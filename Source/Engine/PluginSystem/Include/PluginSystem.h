// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// PluginSystem.h: Plugin system main header
#pragma once
#include "PluginSystemPCH.h"

#include "PluginSystem/IPlugin.h"
#include "PluginSystem/PluginManager.h"
