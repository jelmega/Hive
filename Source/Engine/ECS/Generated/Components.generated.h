// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Generated RTTI data for Components.h
// Do not manually modify this file
#pragma once
#pragma warning(push)
#pragma warning(disable: 4307) // '...': integral constant overflow (for RTTIHashCombine)
#include <RTTI/RTTI.h>

namespace Hv
{
	struct TransformComponent;
}
namespace Hv
{
	template<>
	class RTTI<::Hv::TransformComponent>
	{
	public:
		static constexpr const AnsiChar* Name = "TransformComponent";
		static constexpr const AnsiChar* FullName = "Hv::TransformComponent";
		static constexpr const AnsiChar* Namespace = "Hv";
		static constexpr u64 Hash = 10425570001768719525;
		static constexpr b8 IsClass = false;
		static constexpr RTTIStructFlags StructFlags = RTTIStructFlags(1);
	};
}
namespace Hv
{
	struct MeshComponent;
}
namespace Hv
{
	template<>
	class RTTI<::Hv::MeshComponent>
	{
	public:
		static constexpr const AnsiChar* Name = "MeshComponent";
		static constexpr const AnsiChar* FullName = "Hv::MeshComponent";
		static constexpr const AnsiChar* Namespace = "Hv";
		static constexpr u64 Hash = 5187252423072344076;
		static constexpr b8 IsClass = false;
		static constexpr RTTIStructFlags StructFlags = RTTIStructFlags(1);
	};
}
#pragma warning(pop)