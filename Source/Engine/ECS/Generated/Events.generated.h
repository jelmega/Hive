// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Generated RTTI data for Events.h
// Do not manually modify this file
#pragma once
#pragma warning(push)
#pragma warning(disable: 4307) // '...': integral constant overflow (for RTTIHashCombine)
#include <RTTI/RTTI.h>

namespace Hv::Events
{
	struct EntityCreatedEvent;
}
namespace Hv
{
	template<>
	class RTTI<::Hv::Events::EntityCreatedEvent>
	{
	public:
		static constexpr const AnsiChar* Name = "EntityCreatedEvent";
		static constexpr const AnsiChar* FullName = "Hv::Events::EntityCreatedEvent";
		static constexpr const AnsiChar* Namespace = "Hv::Events";
		static constexpr u64 Hash = 5295873302845714856;
		static constexpr b8 IsClass = false;
		static constexpr RTTIStructFlags StructFlags = RTTIStructFlags(2);
	};
}
namespace Hv::Events
{
	struct EntityDestroyedEvent;
}
namespace Hv
{
	template<>
	class RTTI<::Hv::Events::EntityDestroyedEvent>
	{
	public:
		static constexpr const AnsiChar* Name = "EntityDestroyedEvent";
		static constexpr const AnsiChar* FullName = "Hv::Events::EntityDestroyedEvent";
		static constexpr const AnsiChar* Namespace = "Hv::Events";
		static constexpr u64 Hash = 4957639481258297431;
		static constexpr b8 IsClass = false;
		static constexpr RTTIStructFlags StructFlags = RTTIStructFlags(2);
	};
}
namespace Hv::Events
{
	template<typename Component>
	struct ComponentAddedEvent;
}
namespace Hv
{
	template<typename Component>
	class RTTI<::Hv::Events::ComponentAddedEvent<Component>>
	{
	public:
		static constexpr const AnsiChar* Name = "ComponentAddedEvent";
		static constexpr const AnsiChar* FullName = "Hv::Events::ComponentAddedEvent";
		static constexpr const AnsiChar* Namespace = "Hv::Events";
		static constexpr u64 Hash = RTTIHashCombine(9127830272432499910, RTTI<Component>::Hash);
		static constexpr b8 IsClass = false;
		static constexpr RTTIStructFlags StructFlags = RTTIStructFlags(2);
	};
}
namespace Hv::Events
{
	template<typename Component>
	struct ComponentRemovedEvent;
}
namespace Hv
{
	template<typename Component>
	class RTTI<::Hv::Events::ComponentRemovedEvent<Component>>
	{
	public:
		static constexpr const AnsiChar* Name = "ComponentRemovedEvent";
		static constexpr const AnsiChar* FullName = "Hv::Events::ComponentRemovedEvent";
		static constexpr const AnsiChar* Namespace = "Hv::Events";
		static constexpr u64 Hash = RTTIHashCombine(10906924322684825867, RTTI<Component>::Hash);
		static constexpr b8 IsClass = false;
		static constexpr RTTIStructFlags StructFlags = RTTIStructFlags(2);
	};
}
#pragma warning(pop)