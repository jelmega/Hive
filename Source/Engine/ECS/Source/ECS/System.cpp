// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// System.cpp: System
#include "ECSPCH.h"
#include "ECS/System.h"

namespace Hv::ECS {

	ISystem::ISystem(SystemPriority priority)
		: m_Priority((u16)priority)
	{
	}

	ISystem::ISystem(u16 priority)
		: m_Priority(priority)
	{
	}

	ISystem::~ISystem()
	{
	}

}

