// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Entity.cpp: Entity
#include "ECSPCH.h"
#include "ECS/Entity.h"
#include "ECS/ECSManager.h"

namespace Hv::ECS {

	Entity::Entity()
		: m_Id(InvalidId())
	{
	}

	Entity::Entity(Id id)
		: m_Id(id)
	{
	}

	Entity::Entity(u64 id)
		: m_Id(Id(id))
	{
	}

	Entity::~Entity()
	{
	}

	void Entity::Create()
	{
		*this = g_ECS.CreateEntity();
	}

	void Entity::Invalidate()
	{
		g_ECS.InvalidateEntity(*this);
	}

	void Entity::Destroy()
	{
		g_ECS.DestroyEntity(*this);
	}

	b8 Entity::Valid()
	{
		return m_Id != InvalidId() && g_ECS.IsEntityValid(*this);
	}

	Entity::Id Entity::InvalidId()
	{
		static Id id = EntityId(0x0000'FFFF'FFFF'FFFF);
		return id;
	}
}
