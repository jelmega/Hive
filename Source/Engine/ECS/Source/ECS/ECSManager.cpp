// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ECSManager.cpp: Entity Component System manager
#include "ECSPCH.h"
#include "ECS/ECSManager.h"
#include "Events/Events.h"
#include "ECS/ComponentId.h"

HV_DECLARE_LOG_CATEGORY(ECS, Hv::LogLevel::All);

namespace Hv::ECS {

	ECSManager::ECSManager()
		: m_CurCompMapping(0)
		, m_CurEventMapping(0)
		, m_CurEntId(0)
	{
	}

	ECSManager::~ECSManager()
	{
	}

	b8 ECSManager::Init()
	{
		// Register ECS events
		RegisterEvent<Events::EntityCreatedEvent>();
		RegisterEvent<Events::EntityDestroyedEvent>();

		return true;
	}

	b8 ECSManager::Shutdown()
	{
		// Destroy all entities
		for (EntityId entId : m_EntityIds)
		{
			Entity ent(entId);
			DestroyEntity(ent);
		}

		// Destroy component pools
		for (IComponentPool* pPool : m_CompPools)
		{
			HvDelete pPool;
		}

		// Destroy owned recievers
		for (DynArray<RecieverPair> recievers : m_Recievers)
		{
			for (RecieverPair pair : recievers)
			{
				if (pair.first)
					HvDelete pair.second;
			}
		}

		// Destroy system (shared ptrs handle this)
		m_Systems.Clear();

		ComponentMapping().Clear();
		EventMapping().Clear();

		return true;
	}

	void ECSManager::Tick()
	{
		for (auto pair : m_Systems)
		{
			auto systems = pair.second;

			for (Memory::SharedPtr<ISystem> system : systems)
			{
				system->Tick();
			}
		}
	}

	Entity ECSManager::CreateEntity()
	{
		EntityId id;
		if (m_AvailableEntities.Size() > 0) // Assign an unused entity id
		{
			id = m_AvailableEntities.Top();
			m_AvailableEntities.Pop();
		}
		else // Create a new entity id and its corresponing owned component bitset
		{
			id = EntityId(m_CurEntId);
			m_OwnedComponents.Push(Bitset());
			m_EntityIds.Push(id);

			++m_CurEntId;
		}
		Entity ent(id);

		Events::EntityCreatedEvent event(ent);
		EmitEvent(event);

		return ent;
	}

	void ECSManager::InvalidateEntity(Entity& entity)
	{
		u64 entId = entity.GetId().ActualId();
		EntityId validId = m_EntityIds[entId];
		u16 ver = validId.Version() + 1;
		u64 id = validId.ActualId();
		id |= u64(ver) << 48;
		validId = EntityId(id);
	}

	void ECSManager::DestroyEntity(Entity& entity)
	{
		Events::EntityDestroyedEvent event(entity);
		EmitEvent(event);

		u64 id = entity.GetId().ActualId();
		m_OwnedComponents[id].Clear();

		InvalidateEntity(entity);
	}

	b8 ECSManager::IsEntityValid(Entity& entity)
	{
		EntityId entId = entity.GetId();
		u64 id = entId.ActualId();
		return m_EntityIds[id] == entId;
	}

	b8 ECSManager::HasAnyComponent(Entity entity)
	{
		u64 id = entity.GetId().ActualId();
		return m_OwnedComponents[id].Any();
	}

	b8 ECSManager::HasComponents(Entity& entity, const Bitset& mask)
	{
		u64 id = entity.GetId().ActualId();
		return m_OwnedComponents[id].Fits(mask);
	}

	u32 ECSManager::GetComponentIdFromhash(u64 hash)
	{
		auto& compMapping = ComponentMapping();
		auto it = compMapping.Find(hash);
		if (it == compMapping.Back())
		{
			return 0xFFFF'FFFF;
		}
		return it->second;
	}

	u32 ECSManager::GetEventIdFromhash(u64 hash)
	{
		auto& evntMapping = EventMapping();
		auto it = evntMapping.Find(hash);
		if (it == evntMapping.Back())
		{
			return 0xFFFF'FFFF;
		}
		return it->second;
	}

	HashMap<u64, u32>& ECSManager::ComponentMapping()
	{
		static HashMap<u64, u32> map;
		return map;
	}

	HashMap<u64, u32>& ECSManager::EventMapping()
	{
		static HashMap<u64, u32> map;
		return map;
	}

	ECSManager& GetECSManager()
	{
		static ECSManager manager;
		return manager;
	}
}
