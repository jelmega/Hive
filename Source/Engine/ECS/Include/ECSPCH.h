// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ECS.h: Entity Component System PreCompiled Headers
#pragma once
#include <Core.h>
#include HV_INCLUDE_RTTI_MODULE(ECS)

HV_DECLARE_LOG_CATEGORY_EXTERN(ECS);