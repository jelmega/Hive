// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ECSManager.h: Entity Component System manager
#pragma once
#include "ECSPCH.h"
#include "Entity.h"
#include "ComponentHandle.h"
#include "System.h"
#include "EventReciever.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv::ECS {
	class IComponentPool;

	class HIVE_API ECSManager
	{
	public:
		ECSManager();
		~ECSManager();
		
		/**
		 * Initialize the ECS manager (registers default components)
		 * @return	True if the ECS manager was initialized successfully, false otherwise
		 */
		b8 Init();
		/**
		* Shutdown the ECS manager (clean up remaining components, entities and systems)
		* @return	True if the ECS manager was shut down successfully, false otherwise
		*/
		b8 Shutdown();
		/**
		 * Tick the ECS manager (update systems)
		 */
		void Tick();

		/**
		 * Create a new entity
		 * @return	
		 */
		Entity CreateEntity();
		/**
		 * Invalidate an entity
		 * @param[in] entity	Entity to invalidate
		 */
		void InvalidateEntity(Entity& entity);
		/**
		* Destroy an entity
		* @param[in] entity	Entity to destroy
		*/
		void DestroyEntity(Entity& entity);
		/**
		 * Check if an entity is valid
		 * @param[in] entity	Entity to check
		 * @return				True if the entity is valid, false otherwise
		 */
		b8 IsEntityValid(Entity& entity);
		/**
		 * Get a valid id from an id
		 * @param[in] id	Actual entity id
		 * @return			Valid entity id
		 */
		EntityId GetValidEntityId(u64 id) const { return m_EntityIds[id]; }
		/**
		 * Get the maximum entity id
		 * @return	Maximum entity id, or 0xFFFF'FFFF'FFFF'FFFF if no entities exist
		 */
		u64 GetMaxEntityId() const { return m_CurEntId - 1; }


		/**
		 * Check if an entity has a component
		 * @tparam Component	Component type to check
		 * @param[in] entity	Entity to check for
		 * @return				True if the entity has the component, false otherwise 
		 */
		template<typename Component>
		b8 HasComponent(Entity entity);
		/**
		 * Check if an entity has any component
		 * @param[in] entity	Entity to check for
		 * @return				True if the entity has any component, false otherwise
		 */
		b8 HasAnyComponent(Entity entity);
		/**
		 * Check if an entity has certain components
		 * @tparam Components	Component types to check
		 * @param[in] entity	Entity to check for
		 * @return				True if the entity has the components in the mask, false otherwise
		 */
		template<typename... Components>
		b8 HasComponents(Entity& entity);
		/**
		 * Check if an entity has certain components
		 * @param[in] entity	Entity to check for
		 * @param[in] mask		Component mask
		 * @return				True if the entity has the components in the mask, false otherwise
		 */
		b8 HasComponents(Entity& entity, const Bitset& mask);

		/**
		 * Add a component to an entity
		 * @tparam Component		Component type to add
		 * @param[in] entity		Entity to add component to
		 * @param[in] component		Component
		 */
		template<typename Component>
		void AddComponent(Entity entity, const Component& component);

		/**
		 * Get a handle to a component
		 * @tparam Component	Component type to get
		 * @param[in] entity	Entity to get the component for
		 * @return				Component handle
		 */
		template<typename Component>
		ComponentHandle<Component> GetComponent(Entity entity);
		/**
		 * Get a component
		 * @tparam Component		Component type to get
		 * @param[in] entity		Entity to get the component for
		 * @param[out] component	Component
		 * @return					True if the entity has the component, false otherwise
		 */
		template<typename Component>
		b8 GetComponent(Entity entity, Component& component);

		/**
		 * Get a component
		 * @tparam Component		Component type to get
		 * @param[in] entity		Entity to get the component for
		 * @return					Component
		 * @note					Unsafe, does not check if the entity has the component
		 */
		template<typename Component>
		Component& GetComponentUnsafe(Entity entity);

		/**
		 * Remove a component and destroy it
		 * @tparam Component	Component type to remove
		 * @param[in] entity	Entity to remove the component from
		 */
		template<typename Component>
		void RemoveComponent(Entity& entity);

		/**
		 * Add a system to the ECS manager
		 * @tparam System	System to add
		 * @tparam Args	Constructor argument types
		 * @param[in] args	System contructor arguments
		 */
		template<typename System, typename... Args>
		void AddSystem(Args&... args);

		/**
		 * Register an event reciever
		 * @tparam Event			Event to register reciever for
		 * @param[in] pReciever		Reciver to register
		 */
		template<typename Event>
		void RegisterReciever(EventReciever<Event>* pReciever);
		/**
		* Register an event reciever
		* @tparam Event		Event to register reciever for
		* @tparam Args			Argument type
		* @param[in] args		Argument to create the reciever with
		*/
		template<typename Event, typename... Args>
		void CreateReciever(const Args&... args);
		/**
		 * Emit an event to all registered recievers
		 * @tparam	Event		Event type
		 * @param[in] event		Event to pass to reciever
		 */
		template<typename Event>
		void EmitEvent(const Event& event);

		/**
		 * Register a component to the ECS
		 * @tparam Component	Component to register
		 */
		template<typename Component>
		b8 RegisterComponent();
		/**
		 * Register a event to the ECS
		 * @tparam Event	Event to register
		 */
		template<typename Event>
		b8 RegisterEvent();

		/**
		 * Get the component id from the component's RTTI hash
		 * @param[in] hash	Component hash
		 * @return			Component id
		 */
		static u32 GetComponentIdFromhash(u64 hash);
		/**
		* Get the event id from the event's RTTI hash
		* @param[in] hash	Event hash
		* @return			Event id
		*/
		static u32 GetEventIdFromhash(u64 hash);

	private:
		static HashMap<u64, u32>& ComponentMapping();				/**< Mapping from component RTTI hash to component id */
		u32 m_CurCompMapping;										/**< Current component id to map to */
		static HashMap<u64, u32>& EventMapping();					/**< Mapping from event RTTI hash to event id */
		u32 m_CurEventMapping;										/**< Current event id to map to */

		DynArray<EntityId> m_EntityIds;								/**< List with valid entities */
		Stack<EntityId> m_AvailableEntities;						/**< Available entity ids */
		u64 m_CurEntId;												/**< First new entity id that's available */

		DynArray<Bitset> m_OwnedComponents;							/**< Owned components */

		DynArray<IComponentPool*> m_CompPools;						/**< Component pools */

		using RecieverPair = Pair<b8, IEventReciever*>;
		DynArray<DynArray<RecieverPair>> m_Recievers;				/**< Event recievers */

		Map<u16, DynArray<Memory::SharedPtr<ISystem>>> m_Systems;	/**< ECS systems */

	};

	HIVE_API ECSManager& GetECSManager();

}

#pragma warning(pop)

#define g_ECS Hv::ECS::GetECSManager()

#include "Inline/ECSManager.inl"
