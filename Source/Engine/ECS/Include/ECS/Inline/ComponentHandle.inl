// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ComponentHandle.h: Component Handle
#pragma once
#include "ECSPCH.h"
#include "ECS/ComponentHandle.h"
#include "ECS/ECSManager.h"

namespace Hv::ECS {
	
	template <typename Component>
	ComponentHandle<Component>::ComponentHandle()
	{
	}

	template <typename Component>
	ComponentHandle<Component>::ComponentHandle(Entity entity)
	{
	}

	template <typename Component>
	ComponentHandle<Component>::~ComponentHandle()
	{
	}

	template <typename Component>
	ComponentHandle<Component>::operator b8()
	{
		return g_ECS.HasComponent<Component>(m_Entity);
	}

	template <typename Component>
	Component* ComponentHandle<Component>::operator->()
	{
		return &g_ECS.GetComponentUnsafe<Component>(m_Entity);
	}

	template <typename Component>
	const Component* ComponentHandle<Component>::operator->() const
	{
		return &g_ECS.GetComponentUnsafe<Component>(m_Entity);
	}

	template <typename Component>
	Component& ComponentHandle<Component>::operator*()
	{
		return g_ECS.GetComponentUnsafe<Component>(m_Entity);
	}

	template <typename Component>
	const Component& ComponentHandle<Component>::operator*() const
	{
		return g_ECS.GetComponentUnsafe<Component>(m_Entity);
	}

	template <typename Component>
	Component& ComponentHandle<Component>::Get()
	{
		return g_ECS.GetComponentUnsafe<Component>(m_Entity);
	}

	template <typename Component>
	const Component& ComponentHandle<Component>::Get() const
	{
		return g_ECS.GetComponentUnsafe<Component>(m_Entity);
	}

	template <typename Component>
	void ComponentHandle<Component>::Remove()
	{
		
	}

	template <typename Component>
	Entity ComponentHandle<Component>::GetEntity()
	{
		return m_Entity;
	}

}
