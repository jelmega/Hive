// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ECSManager.cpp: Entity Component System manager
#pragma once
#include "ECSPCH.h"
#include "ECS/ECSManager.h"
#include "ECS/ComponentId.h"
#include "ECS/ComponentPool.h"
#include "ECS/ComponentRequirement.h"
#include "ECS/EventId.h"
#include "Events/Events.h"

namespace Hv::ECS {

	template <typename Component>
	b8 ECSManager::HasComponent(Entity entity)
	{
		u64 entId = entity.GetId().ActualId();
		u32 compId = ComponentId<Component>::Index();

		return m_OwnedComponents[entId][compId];
	}

	template <typename... Components>
	b8 ECSManager::HasComponents(Entity& entity)
	{
		Requirement<Components> req;
		return HasComponents(req.mask);
	}

	template <typename Component>
	void ECSManager::AddComponent(Entity entity, const Component& component)
	{
		u64 entId = entity.GetId().ActualId();
		u32 compId = ComponentId<Component>::Index();

		ComponentPool<Component>* pPool = (ComponentPool<Component>*)m_CompPools[compId];
		pPool->SetComponent(entId, component);

		m_OwnedComponents[entId].Set(compId);

		Events::ComponentAddedEvent<Component> event(entity);
		EmitEvent(event);
	}

	template <typename Component>
	ComponentHandle<Component> ECSManager::GetComponent(Entity entity)
	{
		return ComponentHandle<Component>(entity);
	}

	template <typename Component>
	b8 ECSManager::GetComponent(Entity entity, Component& component)
	{
		u64 entId = entity.GetId().ActualId();
		u32 compId = ComponentId<Component>::Index();

		if (!m_OwnedComponents[entId][compId])
			return false;

		ComponentPool<Component>* pPool = (ComponentPool<Component>*)m_CompPools[compId];
		component = pPool->GetComponent(entId);
		return true;
	}

	template <typename Component>
	Component& ECSManager::GetComponentUnsafe(Entity entity)
	{
		u64 entId = entity.GetId().ActualId();
		u32 compId = ComponentId<Component>::Index();

		ComponentPool<Component>* pPool = (ComponentPool<Component>*)m_CompPools[compId];
		return pPool->GetComponent(entId);
	}

	template <typename Component>
	void ECSManager::RemoveComponent(Entity& entity)
	{
		Events::ComponentRemovedEvent<Component> event(entity);
		EmitEvent(event);

		u64 entId = entity.GetId().ActualId();
		u32 compId = ComponentId<Component>::Index();

		if (!m_OwnedComponents[entId][compId])
			return;

		ComponentPool<Component>* pPool = (ComponentPool<Component>*)m_CompPools[compId];
		Component& comp = pPool->GetComponent(entId);

		comp.~Component();
		m_OwnedComponents[entId].Unset(compId);
	}

	template <typename System, typename ... Args>
	void ECSManager::AddSystem(Args&... args)
	{
		HV_STATIC_ASSERT((Traits::IsBaseOfV<ISystem, System>));
		Memory::SharedPtr<ISystem> pSys(HvNew System(args...));

		u16 priority = pSys->GetPriority();
		auto it = m_Systems.Find(priority);
		if (it == m_Systems.Back())
		{
			auto pair = m_Systems.Insert(priority, DynArray<Memory::SharedPtr<ISystem>>());
			it = pair.first;
		}

		pSys->Init();
		it->second.Push(pSys);
	}

	template <typename Event>
	void ECSManager::RegisterReciever(EventReciever<Event>* pReciever)
	{
		u32 id = EventId<Event>::Index();
		DynArray<RecieverPair>& recievers = m_Recievers[id];
		recievers.Push(RecieverPair(false, pReciever));
	}

	template <typename Event, typename ... Args>
	void ECSManager::CreateReciever(const Args&... args)
	{
		EventReciever<Event> pReciever = HvNew EventReciever<Event>(args...);

		u32 id = EventId<Event>::Index();
		DynArray<RecieverPair>& recievers = m_Recievers[id];
		recievers.Push(RecieverPair(true, pReciever));
	}

	// TODO: Fix issue to distiguish between templated events (combined hashes with template params?)
	template <typename Event>
	void ECSManager::EmitEvent(const Event& event)
	{
		u32 id = EventId<Event>::Index();
		DynArray<RecieverPair>& recievers = m_Recievers[id];

		for (RecieverPair reciever : recievers)
		{
			((EventReciever<Event>*)reciever.second)->Recieve(event);
		}
	}

	template <typename Component>
	b8 ECSManager::RegisterComponent()
	{
		constexpr u64 hash = RTTI<Component>::Hash;
		constexpr RTTIStructFlags flags = RTTI<Component>::StructFlags;
		if (hash == 0 || !HV_IS_ENUM_FLAG_SET(flags, RTTIStructFlags::Component))
			return false;

		// Add hash to mapping
		Pair<HashMap<u64, u32>::Iterator, b8> pair =  ComponentMapping().Insert(hash, m_CurCompMapping);
		if (!pair.second)
			return false;

		// Create component pool
		IComponentPool* pPool = HvNew ComponentPool<Component>();
		m_CompPools.Push(pPool);
		++m_CurCompMapping;

		// Register component specific events
		RegisterEvent<Events::ComponentAddedEvent<Component>>();
		RegisterEvent<Events::ComponentRemovedEvent<Component>>();

		return true;
	}

	template <typename Event>
	b8 ECSManager::RegisterEvent()
	{
		constexpr u64 hash = RTTI<Event>::Hash;
		constexpr RTTIStructFlags flags = RTTI<Event>::StructFlags;
		if (hash == 0 || !HV_IS_ENUM_FLAG_SET(flags, RTTIStructFlags::Event))
			return false;

		// Add hash to mapping
		Pair<HashMap<u64, u32>::Iterator, b8> pair = EventMapping().Insert(hash, m_CurEventMapping);
		if (!pair.second)
			return false;

		// Add a dynArray for the event reciever
		m_Recievers.Push(DynArray<RecieverPair>());

		++m_CurEventMapping;

		return true;
	}

}
