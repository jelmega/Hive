// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ComponentPool.inl: Component pool
#pragma once
#include "ECSPCH.h"
#include "ECS/ComponentPool.h"

namespace Hv::ECS {
	
	template <typename Component>
	ComponentPool<Component>::ComponentPool()
		: IComponentPool()
	{
		m_Chunks.Push(HvNew Array<Component, ChunkSize>);
	}

	template <typename Component>
	ComponentPool<Component>::~ComponentPool()
	{
	}

	template <typename Component>
	void ComponentPool<Component>::SetComponent(u64 entityId, const Component& component)
	{
		constexpr u64 mask = ChunkSize - 1;
		u64 chunkIdx = entityId / ChunkSize;
		u64 compIdx = entityId & mask;

		if (chunkIdx >= m_Chunks.Size())
		{
			for (sizeT i = m_Chunks.Size(), end = chunkIdx + 1; i <= end; ++i)
			{
				m_Chunks.Push(HvNew Array<Component, ChunkSize>);
			}
		}

		(*m_Chunks[chunkIdx])[compIdx] = component;
	}

	template <typename Component>
	Component& ComponentPool<Component>::GetComponent(u64 entityId)
	{
		constexpr u64 mask = ChunkSize - 1;
		u64 chunkIdx = entityId / ChunkSize;
		u64 compIdx = entityId & mask;

		HV_ASSERT(chunkIdx < m_Chunks.Size());

		return (*m_Chunks[chunkIdx])[compIdx];
	}

}
