// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Event id.inl: Event id
#pragma once
#include "ECSPCH.h"
#include "ECS/EventId.h"
#include "ECS/ECSManager.h"

namespace Hv::ECS {

	template <typename Event>
	u32 EventId<Event>::Index()
	{
		static u32 index = g_ECS.GetEventIdFromhash(RTTI<Event>::Hash);
		return index;
	}

}
