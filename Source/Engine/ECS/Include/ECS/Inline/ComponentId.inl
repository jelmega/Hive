// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ComponentId.inl: Component id
#pragma once
#include "ECSPCH.h"
#include "ECS/ComponentId.h"
#include "ECS/ECSManager.h"

namespace Hv::ECS {

	template <typename Component>
	u32 ComponentId<Component>::Index()
	{
		static u32 index = g_ECS.GetComponentIdFromhash(RTTI<Component>::Hash);
		return index;
	}
	
}
