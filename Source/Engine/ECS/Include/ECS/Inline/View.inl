// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// View.inl: Component view
#pragma once
#include "ECSPCH.h"
#include "ECS/View.h"
#include "ECS/Entity.h"

namespace Hv::ECS {

	template <b8 All>
	ViewIterator<All>::ViewIterator()
		: m_Entity(Entity::InvalidId())
	{
	}

	template <b8 All>
	ViewIterator<All>::ViewIterator(Entity entity, const Bitset& mask)
		: m_Entity(entity)
		, m_Mask(mask)
	{
	}

	template <b8 All>
	ViewIterator<All>::~ViewIterator()
	{
	}

	template <b8 All>
	ViewIterator<All>& ViewIterator<All>::operator++()
	{
		if constexpr(All)
		{
			u64 maxId = g_ECS.GetMaxEntityId();
			u64 id = m_Entity.GetId().ActualId();

			if (id == maxId)
			{
				m_Entity = Entity(Entity::InvalidId());
				return *this;
			}

			++id;
			m_Entity = Entity(g_ECS.GetValidEntityId(id));
			while (id <= maxId && !g_ECS.HasAnyComponent(m_Entity))
			{
				++id;
				if (id > maxId)
				{
					m_Entity = Entity(Entity::InvalidId());
				}
				else
				{
					m_Entity = Entity(g_ECS.GetValidEntityId(id));
				}
				
			}
		}
		else
		{
			u64 maxId = g_ECS.GetMaxEntityId();
			u64 id = m_Entity.GetId().ActualId();

			if (id == maxId)
			{
				m_Entity = Entity(Entity::InvalidId());
				return *this;
			}

			++id;
			m_Entity = Entity(g_ECS.GetValidEntityId(id));
			while (id <= maxId && !g_ECS.HasComponents(m_Entity, m_Mask))
			{
				++id;
				if (id > maxId)
				{
					m_Entity = Entity(Entity::InvalidId());
				}
				else
				{
					m_Entity = Entity(g_ECS.GetValidEntityId(id));
				}
			}
		}
		return *this;
	}

	template <b8 All>
	ViewIterator<All> ViewIterator<All>::operator++(int)
	{
		ViewIterator<All> it = *this;
		++*this;
		return it;
	}

	template <b8 All>
	Entity& ViewIterator<All>::operator*()
	{
		return m_Entity;
	}

	template <b8 All>
	const Entity& ViewIterator<All>::operator*() const
	{
		return m_Entity;
	}

	template <typename ... Components>
	View<Components...>::View()
	{
	}

	template <typename ... Components>
	View<Components...>::~View()
	{
	}

	template <typename ... Components>
	void View<Components...>::Each(Delegate<void(Entity&, Components&...)>& delegate)
	{
		if (g_ECS.GetMaxEntityId() == u64(-1))
			return;

		ViewIterator<false> it = First();
		ViewIterator<false> lastIt = Last();

		for (; it != lastIt; ++it)
		{
			delegate(*it, g_ECS.GetComponentUnsafe<Components>(*it)...);
		}
	}

	template <typename ... Components>
	template <typename Func>
	void View<Components...>::Each(Func&& func)
	{
		if (g_ECS.GetMaxEntityId() == u64(-1))
			return;

		ViewIterator<false> it = First();
		ViewIterator<false> lastIt = Last();

		for (; it != lastIt; ++it)
		{
			func(*it, g_ECS.GetComponentUnsafe<Components>(*it)...);
		}
	}

	template <typename ... Components>
	ViewIterator<false> View<Components...>::First()
	{
		EntityId id = g_ECS.GetValidEntityId(0);
		Entity ent(id);
		ViewIterator<false> viewIt(ent, m_Req.mask);
		if (g_ECS.HasComponents(ent, m_Req.mask))
			return viewIt;
		return ++viewIt;
	}

	template <typename ... Components>
	ViewIterator<false> View<Components...>::Last()
	{
		Entity ent(Entity::InvalidId());
		return ViewIterator<false>(ent, m_Req.mask);
	}

}
