// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ComponentRequirement.h: Component requirement
#pragma once
#include "ECSPCH.h"
#include "ComponentId.h"

namespace Hv::ECS {
	
	template<typename... Components>
	struct Requirement
	{
		HV_STATIC_ASSERT(sizeof...(Components) < 255);
		static constexpr u8 NumComponents = sizeof...(Components);

		Requirement();

		Array<u32, NumComponents> componentIds;
		Bitset mask;
	};

	template <typename ... Components>
	Requirement<Components...>::Requirement()
	{
		componentIds = { (ComponentId<Components>::Index())... };
		for (u8 i = 0; i < NumComponents; ++i)
		{
			mask.Set(componentIds[i]);
		}
	}

}
