// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Event id.h: Event id
#pragma once
#include "ECSPCH.h"

namespace Hv::ECS {

	template<typename Event>
	struct EventId
	{
		static u32 Index();
	};

}

#include "ECS/Inline/EventId.inl"