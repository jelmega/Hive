// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ComponentPool.h: Component pool
#pragma once
#include "ECSPCH.h"

namespace Hv::ECS {
	
	class IComponentPool
	{
	public:
		IComponentPool() {}
		virtual ~IComponentPool() {}
	};


	template<typename Component>
	class ComponentPool : public IComponentPool
	{
	public:
		ComponentPool();
		~ComponentPool();

		/**
		 * Set a component
		 * @param[in] entityId		Entity id
		 * @param[in] component		Component
		 */
		void SetComponent(u64 entityId, const Component& component);
		/**
		 * Get a component
		 * @param[in] entityId	Entity id
		 * @return				Component id
		 */
		Component& GetComponent(u64 entityId);

	private:
		static constexpr u16 ChunkSize = 1024;
		DynArray<Array<Component, ChunkSize>*> m_Chunks;
	};

}

#include "Inline/ComponentPool.inl"