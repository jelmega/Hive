// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ComponentId.h: Component id
#pragma once
#include "ECSPCH.h"

namespace Hv::ECS {

	template<typename Component>
	struct ComponentId
	{
		static u32 Index();
	};

}

#include "Inline/ComponentId.inl"
