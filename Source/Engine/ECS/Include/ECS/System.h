// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// System.h: System
#pragma once
#include "ECSPCH.h"

namespace Hv::ECS {

	/**
	 * System priotity presets
	 */
	enum class SystemPriority : u16
	{
		Critical	= 0,			/**< Critical systems, will be updated first */
		High		= 10,			/**< High priority systems */
		Medium		= 100,			/**< Medium priority systems */
		Low			= 1'000,		/**< Low priority systems */
		Late		= 10'000,		/**< Late systems */
		Render		= 65'000,		/**< Render systems */
	};

	/**
	 * System interface
	 * @note	Systems with the same priority will be executed in the order they were added to the ECS manager
	 * @note	With the current implementation, limiting the amount of priorities will improve performance
	 */
	class HIVE_API ISystem
	{
	public:
		/**
		 * Create a system with a preset priority
		 * @param[in] priority	Preset priority
		 */
		ISystem(SystemPriority priority);
		/**
		* Create a system with a custom priority
		* @param[in] priority	Priority
		*/
		ISystem(u16 priority);
		virtual ~ISystem();

		/**
		 * Initialize the system, called when system is added
		 */
		virtual void Init() = 0;
		/**
		 * Tick the system
		 */
		virtual void Tick() = 0;

		/**
		 * Get the systems priority
		 */
		u16 GetPriority() const { return m_Priority; }

	protected:
		u16 m_Priority;
	};

}
