// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// ComponentHandle.h: Component Handle
#pragma once
#include "ECSPCH.h"
#include "Entity.h"

namespace Hv::ECS {

	/**
	 * Type-safe component handle
	 */
	template<typename Component>
	class ComponentHandle
	{
	public:
		ComponentHandle();
		ComponentHandle(Entity entity);
		~ComponentHandle();

		operator b8();

		Component* operator->();
		const Component* operator->() const;

		Component& operator*();
		const Component& operator*() const;

		/**
		 * Get the component
		 * @return	Component
		 */
		Component& Get();
		/**
		* Get the component
		* @return	Component
		*/
		const Component& Get() const;

		/**
		 * Remove the component
		 */
		void Remove();

		/**
		 * Get the associated entity
		 * @return	Associated entity
		 */
		Entity GetEntity();

	private:
		Entity m_Entity;
	};

}

#include "Inline/ComponentHandle.inl"
