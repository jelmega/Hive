// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Entity.h: Entity
#pragma once
#include "ECSPCH.h"

namespace Hv::ECS {
	
	class HIVE_API Entity
	{
	public:
		class HIVE_API Id
		{
		public:
			Id() : m_Id(0x0000'FFFF'FFFF'FFFF) {}
			explicit Id(u64 id) : m_Id(id) {}
			~Id() {}

			operator u64() const { return m_Id; }

			b8 operator==(const Id& id) const { return m_Id == id.m_Id; }
			b8 operator!=(const Id& id) const { return m_Id != id.m_Id; }
			b8 operator<(const Id& id) const { return m_Id < id.m_Id; }

			/**
			 * Get the actual entity id (0 - 281'474'976'710'655)
			 * @return	Actual id
			 */
			u64 ActualId() const { return m_Id & 0x0000'FFFF'FFFF'FFFF; }
			/**
			 * Get the version (amount the id has been reused (wraps at the 65'536th use))
			 */
			u16 Version() const { return u16(m_Id >> 48); }

		private:
			u64 m_Id;
		};


	public:
		Entity();
		explicit Entity(Id id);
		explicit Entity(u64 id);
		~Entity();

		b8 operator==(const Entity& ent) const { return m_Id == ent.m_Id; }
		b8 operator!=(const Entity& ent) const { return m_Id == ent.m_Id; }

		/**
		 * Create the entity
		 */
		void Create();
		/**
		 * Invalidate the enity
		 * @note Components will still be valid!
		 */
		void Invalidate();
		/**
		 * Destroy the entity
		 */
		void Destroy();

		/**
		 * Check if the entity is valid
		 * @return	True if the entity is valid, false otherwise
		 */
		b8 Valid();

		/**
		 * Get the entity Id
		 * @return	Entity id
		 */
		Id GetId() const { return m_Id; }


	public:
		Id m_Id;

		static Id InvalidId();
	};

	using EntityId = Entity::Id;

}