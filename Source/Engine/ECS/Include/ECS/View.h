// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// View.h: Component view
#pragma once
#include "ECSPCH.h"
#include "ComponentRequirement.h"
#include "Entity.h"

namespace Hv::ECS {

	/**
	 * @tparam All	Whether to iterate over all entities
	 * @note	Even when 'All' == true, the iterator will not go over any entity without a component
	 */
	template<b8 All>
	class ViewIterator
	{
	public:
		ViewIterator();
		ViewIterator(Entity entity, const Bitset& mask);
		~ViewIterator();

		ViewIterator& operator++();
		ViewIterator operator++(int);

		Entity& operator*();
		const Entity& operator*() const;
		
		b8 operator==(const ViewIterator& it) const { return m_Entity.GetId() == it.m_Entity.GetId(); }
		b8 operator!=(const ViewIterator& it) const { return m_Entity.GetId() != it.m_Entity.GetId(); }

	private:
		Entity m_Entity;
		Bitset m_Mask;
	};
	
	template<typename... Components>
	class View
	{
	public:
		using Req = Requirement<Components...>;

	public:

	public:
		View();
		~View();

		void Each(Delegate<void(Entity&, Components&...)>& delegate);
		template<typename Func>
		void Each(Func&& func);

		ViewIterator<false> First();
		ViewIterator<false> Last();

	private:
		Req m_Req;
	};

}

#include "Inline/View.inl"
