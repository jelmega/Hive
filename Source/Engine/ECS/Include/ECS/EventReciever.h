// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// EventReciever.h: Event reciever
#pragma once
#include "ECSPCH.h"

namespace Hv::ECS {

	class HIVE_API IEventReciever
	{
	public:
		IEventReciever() {}
		virtual ~IEventReciever() {}
	};

	template<typename Event>
	class EventReciever : public IEventReciever
	{
	public:
		EventReciever() {}
		virtual ~EventReciever() {}

		virtual void Recieve(const Event& evnt) = 0;
	};

}
