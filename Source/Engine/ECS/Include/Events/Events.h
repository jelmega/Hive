// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// Events.h: Default events
#pragma once
#include "ECSPCH.h"
#include "ECS/Entity.h"
#include "ECS/ComponentHandle.h"

namespace Hv::Events {
	
	HV_STRUCT(Event)
	struct EntityCreatedEvent
	{
		EntityCreatedEvent(ECS::Entity ent) : entity(ent) {}

		ECS::Entity entity;
	};

	HV_STRUCT(Event)
	struct EntityDestroyedEvent
	{
		EntityDestroyedEvent(ECS::Entity ent) : entity(ent) {}

		ECS::Entity entity;
	};

	HV_STRUCT(Event)
	template<typename Component>
	struct ComponentAddedEvent
	{
		ComponentAddedEvent(ECS::Entity ent) : entity(ent), component(ent) {}

		ECS::Entity entity;
		ECS::ComponentHandle<Component> component;
	};

	HV_STRUCT(Event)
	template<typename Component>
	struct ComponentRemovedEvent
	{
		ComponentRemovedEvent(ECS::Entity ent) : entity(ent), component(ent) {}

		ECS::Entity entity;
		ECS::ComponentHandle<Component> component;
	};

}
