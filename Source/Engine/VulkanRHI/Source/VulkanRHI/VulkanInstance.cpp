// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanInstance.cpp: Vulkan Instance
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanContext.h"
#include "VulkanRHI/VulkanInstance.h"
#include "VulkanRHI/VulkanPhysicalDevice.h"
#include "VulkanRHI/VulkanHelpers.h"
#include "VulkanRHI/VulkanBuffer.h"

#define LOAD_VKI_FUNC(funcs, instance, name) funcs.name = (PFN_vk##name)m_Vk.GetInstanceProcAddr(instance, HV_STRINGIFY(vk##name))

namespace Hv::VulkanRHI {


	VulkanInstance::VulkanInstance()
		: m_Vk()
		, m_VkExt()
		, m_VkPhysicalDeviceFuncs()
		, m_pContext(nullptr)
		, m_pAllocCallbacks(nullptr)
		, m_Instance(VK_NULL_HANDLE)
		, m_VulkanApiVersion(VK_API_VERSION_1_0)
		, m_DebugReportCallback(VK_NULL_HANDLE)
	{
	}

	VulkanInstance::~VulkanInstance()
	{
	}

	VkResult VulkanInstance::Init(VulkanContext* pContext, const DynArray<const AnsiChar*>& requestedExtensions, const DynArray<const AnsiChar*>& requestedLayers)
	{
		m_pContext = pContext;
		m_pAllocCallbacks = m_pContext->GetAllocationCallbacks();

		// Load the required functions for instance creation
		LoadFunctions();

		// Enumerate available extensions
		u32 numExtensions = 0;
		VkResult vkres = m_Vk.EnumerateInstanceExtensionProperties(nullptr, &numExtensions, nullptr);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to retrieve the number of available instance extensions (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}
		m_AvailableExtensions.Resize(numExtensions);
		vkres = m_Vk.EnumerateInstanceExtensionProperties(nullptr, &numExtensions, m_AvailableExtensions.Data());
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to retrieve the available instance extensions (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}

		// Enumerate available layers
		u32 numLayers = 0;
		vkres = m_Vk.EnumerateInstanceLayerProperties(&numLayers, nullptr);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to retrieve the number of available instance layers (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}
		m_AvailableLayers.Resize(numLayers);
		vkres = m_Vk.EnumerateInstanceLayerProperties(&numLayers, m_AvailableLayers.Data());
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to retrieve the available instance extensions (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}

		// Retrieve the current vulkan version, if function isn't loaded, use default version (1.0)
		if (m_Vk.EnumerateInstanceVersion)
		{
			m_Vk.EnumerateInstanceVersion(&m_VulkanApiVersion);
		}

		for (const AnsiChar* requested : requestedExtensions)
		{
			if (IsExtensionAvailable(requested))
				m_EnabledExtensions.Push(requested);
		}

		for (const AnsiChar* requested : requestedLayers)
		{
			if (IsLayerAvailable(requested))
				m_EnabledLayers.Push(requested);
		}

		VkApplicationInfo applicationInfo = {};
		applicationInfo.apiVersion = VK_API_VERSION_1_0;// m_VulkanApiVersion;
		// TODO: Add app info ???

		VkInstanceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		createInfo.pApplicationInfo = &applicationInfo;
		createInfo.enabledExtensionCount = u32(m_EnabledExtensions.Size());
		createInfo.ppEnabledExtensionNames = m_EnabledExtensions.Data();
		createInfo.enabledLayerCount = u32(m_EnabledLayers.Size());
		createInfo.ppEnabledLayerNames = m_EnabledLayers.Data();

		vkres = m_Vk.CreateInstance(&createInfo, m_pAllocCallbacks, &m_Instance);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to create vulkan instance (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}

		// Load instance dependent functions
		LoadInstanceFunctions();

		return VK_SUCCESS;
	}

	void VulkanInstance::Destroy()
	{
		DestroyDebugReportCallback();
		if (m_Instance)
		{
			m_Vk.DestroyInstance(m_Instance, m_pAllocCallbacks);
			m_Instance = VK_NULL_HANDLE;
		}
		// Clear function pointers
		m_Vk = {};
	}

	b8 VulkanInstance::IsExtensionAvailable(const String& extension)
	{
		for (const VkExtensionProperties& availableExtension : m_AvailableExtensions)
		{
			if (availableExtension.extensionName == extension)
				return true;
		}
		return false;
	}

	b8 VulkanInstance::IsLayerAvailable(const String& layer)
	{
		for (const VkLayerProperties& availableExtension : m_AvailableLayers)
		{
			if (availableExtension.layerName == layer)
				return true;
		}
		return false;
	}

	b8 VulkanInstance::IsExtensionEnabled(const String& extension)
	{
		for (const AnsiChar* name : m_EnabledExtensions)
		{
			if (name == extension)
				return true;
		}
		return false;
	}

	b8 VulkanInstance::IsLayerEnabled(const String& layer)
	{
		for (const AnsiChar* name : m_EnabledLayers)
		{
			if (name == layer)
				return true;
		}
		return false;
	}

	void VulkanInstance::LogAvailableExtensions()
	{
		for (const VkExtensionProperties& extension : m_AvailableExtensions)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Detail, "instance extension: %s, version: %u", extension.extensionName, extension.specVersion);
		}
	}

	void VulkanInstance::LogAvailableLayers()
	{
		for (const VkLayerProperties& layer : m_AvailableLayers)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Detail, "instance layer: %s, spec version: %u, implementation version: %u, description: %s",
								layer.layerName, layer.specVersion, layer.implementationVersion, layer.description);
		}
	}

	VkResult VulkanInstance::EnumeratePhysicalDevices(DynArray<VulkanPhysicalDevice*>& physicalDevices) const
	{
		u32 numPhysicalDevices;
		VkResult vkres = m_Vk.EnumeratePhysicalDevices(m_Instance, &numPhysicalDevices, nullptr);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to retrieve the number of physical devices (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}
		DynArray<VkPhysicalDevice> vulkanDevices(numPhysicalDevices);
		vkres = m_Vk.EnumeratePhysicalDevices(m_Instance, &numPhysicalDevices, vulkanDevices.Data());
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to retrieve the physical devices (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}

		physicalDevices.Reserve(numPhysicalDevices);
		for (VkPhysicalDevice device : vulkanDevices)
		{
			VulkanPhysicalDevice* pDev = HvNew VulkanPhysicalDevice();
			vkres = pDev->Init(m_pContext, device, m_VkPhysicalDeviceFuncs);
			if (vkres != VK_SUCCESS)
			{
				g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to initialize the physical devices (VkResult: %s)!", Helpers::GetResultString(vkres));
				return vkres;
			}
			physicalDevices.Push(pDev);
		}

		return VK_SUCCESS;
	}

	VkResult VulkanInstance::CreateDebugReportCallback(void* pUserData, PFN_vkDebugReportCallbackEXT pfnCallback, Renderer::RHIValidationLevel level)
	{
		VkDebugReportCallbackCreateInfoEXT createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
		createInfo.pUserData = pUserData;
		createInfo.pfnCallback = pfnCallback;

		switch (level)
		{
		case Renderer::RHIValidationLevel::DebugInfo: 
			createInfo.flags |= VK_DEBUG_REPORT_DEBUG_BIT_EXT;
		case Renderer::RHIValidationLevel::Info:
			createInfo.flags |= VK_DEBUG_REPORT_INFORMATION_BIT_EXT;
		case Renderer::RHIValidationLevel::Performance:
			createInfo.flags |= VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
		case Renderer::RHIValidationLevel::Warning:
			createInfo.flags |= VK_DEBUG_REPORT_WARNING_BIT_EXT;
		case Renderer::RHIValidationLevel::Error:
			createInfo.flags |= VK_DEBUG_REPORT_ERROR_BIT_EXT;
		case Renderer::RHIValidationLevel::None:
		default:
			break;
		}

		VkResult vkres = m_VkExt.CreateDebugReportCallbackEXT(m_Instance, &createInfo, m_pAllocCallbacks, &m_DebugReportCallback);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Error, "Failed to create debug callback (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}
		return vkres;
	}

	void VulkanInstance::DestroyDebugReportCallback()
	{
		if (m_DebugReportCallback)
		{
			m_VkExt.DestroyDebugReportCallbackEXT(m_Instance, m_DebugReportCallback, m_pAllocCallbacks);
		}
	}

#ifdef VK_USE_PLATFORM_WIN32_KHR
	VkResult VulkanInstance::vkCreateSurface(const VkWin32SurfaceCreateInfoKHR& createInfo, VkSurfaceKHR& surface)
	{
		return m_VkExt.CreateWin32SurfaceKHR(m_Instance, &createInfo, m_pAllocCallbacks, &surface);
	}
#else
#endif

	void VulkanInstance::vkDestroySurface(VkSurfaceKHR surface)
	{
		m_VkExt.DestroySurfaceKHR(m_Instance, surface, m_pAllocCallbacks);
	}

	void VulkanInstance::LoadFunctions()
	{
		// Instance independent
		m_Vk.GetInstanceProcAddr = (PFN_vkGetInstanceProcAddr)m_pContext->GetVkInstanceProcAddr();
		LOAD_VKI_FUNC(m_Vk, VK_NULL_HANDLE, CreateInstance);
		LOAD_VKI_FUNC(m_Vk, VK_NULL_HANDLE, EnumerateInstanceExtensionProperties);
		LOAD_VKI_FUNC(m_Vk, VK_NULL_HANDLE, EnumerateInstanceLayerProperties);
		LOAD_VKI_FUNC(m_Vk, VK_NULL_HANDLE, EnumerateInstanceVersion);
	}

	void VulkanInstance::LoadInstanceFunctions()
	{
		// Instance
		LOAD_VKI_FUNC(m_Vk, m_Instance, DestroyInstance);

		// Physical device
		LOAD_VKI_FUNC(m_Vk, m_Instance, EnumeratePhysicalDevices);
		LOAD_VKI_FUNC(m_VkPhysicalDeviceFuncs, m_Instance, GetPhysicalDeviceFeatures);
		LOAD_VKI_FUNC(m_VkPhysicalDeviceFuncs, m_Instance, GetPhysicalDeviceFormatProperties);
		LOAD_VKI_FUNC(m_VkPhysicalDeviceFuncs, m_Instance, GetPhysicalDeviceImageFormatProperties);
		LOAD_VKI_FUNC(m_VkPhysicalDeviceFuncs, m_Instance, GetPhysicalDeviceProperties);
		LOAD_VKI_FUNC(m_VkPhysicalDeviceFuncs, m_Instance, GetPhysicalDeviceQueueFamilyProperties);
		LOAD_VKI_FUNC(m_VkPhysicalDeviceFuncs, m_Instance, GetPhysicalDeviceMemoryProperties);
		LOAD_VKI_FUNC(m_VkPhysicalDeviceFuncs, m_Instance, GetPhysicalDeviceSparseImageFormatProperties);
		LOAD_VKI_FUNC(m_VkPhysicalDeviceFuncs, m_Instance, GetPhysicalDeviceSurfaceSupportKHR);
		LOAD_VKI_FUNC(m_VkPhysicalDeviceFuncs, m_Instance, GetPhysicalDeviceSurfaceFormatsKHR);
		LOAD_VKI_FUNC(m_VkPhysicalDeviceFuncs, m_Instance, GetPhysicalDeviceSurfacePresentModesKHR);
		LOAD_VKI_FUNC(m_VkPhysicalDeviceFuncs, m_Instance, GetPhysicalDeviceSurfaceCapabilitiesKHR);

		// Device extensions
		LOAD_VKI_FUNC(m_VkPhysicalDeviceFuncs, m_Instance, EnumerateDeviceExtensionProperties);
		LOAD_VKI_FUNC(m_VkPhysicalDeviceFuncs, m_Instance, EnumerateDeviceLayerProperties);
		if (IsExtensionEnabled(VK_EXT_DEBUG_REPORT_EXTENSION_NAME))
		{
			LOAD_VKI_FUNC(m_VkExt, m_Instance, CreateDebugReportCallbackEXT);
			LOAD_VKI_FUNC(m_VkExt, m_Instance, DestroyDebugReportCallbackEXT);
		}

		// Surface functions
#if defined VK_USE_PLATFORM_WIN32_KHR
		LOAD_VKI_FUNC(m_VkExt, m_Instance, CreateWin32SurfaceKHR);
#else
#endif
		LOAD_VKI_FUNC(m_VkExt, m_Instance, DestroySurfaceKHR);
	}
}

#undef LOAD_VKI_FUNC