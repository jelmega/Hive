// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanDescriptorSet.cpp: Vulkan descriptor set
#pragma once
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanDescriptorSet.h"
#include "VulkanRHI/VulkanHelpers.h"
#include "VulkanRHI/VulkanDevice.h"
#include "VulkanRHI/VulkanContext.h"
#include "VulkanRHI/VulkanDescriptorSetManager.h"
#include "VulkanRHI/VulkanDescriptorSetLayout.h"

namespace Hv::VulkanRHI {


	VulkanDescriptorSet::VulkanDescriptorSet()
		: m_DescriptorSet(VK_NULL_HANDLE)
		, m_Pool(VK_NULL_HANDLE)
	{
	}

	VulkanDescriptorSet::~VulkanDescriptorSet()
	{
	}

	b8 VulkanDescriptorSet::Create(RHI::RHIContext* pContext, Renderer::Core::DescriptorSetManager* pManager, Renderer::Core::DescriptorSetLayout* pLayout)
	{
		m_pContext = pContext;
		m_pManager = pManager;
		m_pLayout = pLayout;
		m_Pool = ((VulkanDescriptorSetManager*)m_pManager)->GetDescriptorPool();

		VkDescriptorSetLayout layout = ((VulkanDescriptorSetLayout*)m_pLayout)->GetLayout();

		VkDescriptorSetAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocInfo.descriptorPool = m_Pool;
		allocInfo.descriptorSetCount = 1;	// TODO: How to do more than 1???
		allocInfo.pSetLayouts = &layout;

		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
		VkResult vkres = pDevice->vkAllocateDescriptorSet(allocInfo, m_DescriptorSet);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to allocate the vulkan descriptor set (VkResult: %s)!", Helpers::GetResultString(vkres));
			Destroy();
			return false;
		}

		pLayout->IncRefs();
		return true;
	}

	b8 VulkanDescriptorSet::Destroy()
	{
		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();

		if (m_DescriptorSet)
		{
			pDevice->vkFreeDescriptorSet(m_Pool, m_DescriptorSet);
			m_DescriptorSet = VK_NULL_HANDLE;
			m_pLayout->DecRefs();
		}

		return true;
	}

	b8 VulkanDescriptorSet::Write(u32 binding, Renderer::Core::Buffer* pBuffer, u64 offset, u64 range, u32 arrayElement)
	{
		DynArray<Renderer::Core::DescriptorSetBinding> bindings = m_pLayout->GetBindings();

		HV_ASSERT(binding < bindings.Size());
		HV_ASSERT(arrayElement < bindings[binding].count);
		HV_ASSERT(offset < pBuffer->GetSize());

		if (range == u64(-1) || range > pBuffer->GetSize() - offset)
		{
			range = pBuffer->GetSize() - offset;
		}

		VkWriteDescriptorSet writeInfo = {};
		writeInfo.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writeInfo.dstSet = m_DescriptorSet;
		writeInfo.descriptorType = Helpers::GetDescriptorType(bindings[binding].type);
		writeInfo.dstBinding = binding;
		writeInfo.dstArrayElement = arrayElement;

		VkDescriptorBufferInfo bufferInfo = {};
		bufferInfo.offset = 0;
		bufferInfo.buffer = ((VulkanBuffer*)pBuffer)->GetBuffer();
		bufferInfo.offset = offset;
		bufferInfo.range = range;

		writeInfo.descriptorCount = 1;
		writeInfo.pBufferInfo = &bufferInfo;

		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
		pDevice->vkUpdateDescriptorSets(1, &writeInfo, 0, nullptr);

		return true;
	}

	b8 VulkanDescriptorSet::Write(u32 binding, Renderer::Core::Texture* pTexture, Renderer::Core::Sampler* pSampler, u32 arrayElement)
	{
		DynArray<Renderer::Core::DescriptorSetBinding> bindings = m_pLayout->GetBindings();

		HV_ASSERT(binding < bindings.Size());

		VkWriteDescriptorSet writeInfo = {};
		writeInfo.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writeInfo.dstSet = m_DescriptorSet;
		writeInfo.descriptorType = Helpers::GetDescriptorType(bindings[binding].type);
		writeInfo.dstBinding = binding;
		writeInfo.dstArrayElement = arrayElement;

		VkDescriptorImageInfo imageInfo = {};
		imageInfo.imageLayout = Helpers::GetImageLayout(pTexture->GetLayout());
		imageInfo.imageView = ((VulkanTexture*)pTexture)->GetImageView();
		imageInfo.sampler = ((VulkanSampler*)pSampler)->GetSampler();

		writeInfo.descriptorCount = 1;
		writeInfo.pImageInfo = &imageInfo;

		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
		pDevice->vkUpdateDescriptorSets(1, &writeInfo, 0, nullptr);

		return true;
	}

	b8 VulkanDescriptorSet::Write(const DynArray<Renderer::Core::DescriptorSetBufferInfo>& buffers,
		const DynArray<Renderer::Core::DescriptorSetTextureSamplerInfo>& texAndSamplers)
	{
		DynArray<VkWriteDescriptorSet> writeInfos;
		DynArray<VkDescriptorBufferInfo> bufferInfos;
		DynArray<VkBufferView> texelBuffers;
		DynArray<VkDescriptorImageInfo> imageInfos;

		writeInfos.Reserve(buffers.Size() + texAndSamplers.Size());
		bufferInfos.Reserve(buffers.Size());
		imageInfos.Reserve(texAndSamplers.Size());

		DynArray<Renderer::Core::DescriptorSetBinding> bindings = m_pLayout->GetBindings();

		for (const Renderer::Core::DescriptorSetBufferInfo& info : buffers)
		{
			HV_ASSERT(info.buffers.Size() == info.offsets.Size());
			HV_ASSERT(info.buffers.Size() == info.ranges.Size());

			VkWriteDescriptorSet writeInfo = {};
			writeInfo.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			writeInfo.dstSet = m_DescriptorSet;
			writeInfo.descriptorType = Helpers::GetDescriptorType(bindings[info.binding].type);
			writeInfo.dstBinding = info.binding;
			writeInfo.dstArrayElement = info.arrayElement;

			Renderer::DescriptorSetBindingType type = bindings[info.binding].type;
			if (type == Renderer::DescriptorSetBindingType::StorageTexelBuffer || type == Renderer::DescriptorSetBindingType::UniformTexelBuffer)
			{
				sizeT arrOffset = texelBuffers.Size();
				for (sizeT i = 0; i < info.buffers.Size(); ++i)
				{
					VulkanBuffer* pBuffer = (VulkanBuffer*)info.buffers[i];

					texelBuffers.Push(pBuffer->GetBufferView());
				}

				writeInfo.descriptorCount = u32(info.buffers.Size());
				writeInfo.pTexelBufferView = texelBuffers.Data() + arrOffset;
			}
			else
			{
				sizeT arrOffset = bufferInfos.Size();
				for (sizeT i = 0; i < info.buffers.Size(); ++i)
				{
					Renderer::Core::Buffer* pBuffer = info.buffers[i];
					u64 offset = info.offsets[i];
					u64 range = info.ranges[i];

					if (range == u64(-1) || range > pBuffer->GetSize() - offset)
					{
						range = pBuffer->GetSize() - offset;
					}

					VkDescriptorBufferInfo bufferInfo = {};
					bufferInfo.offset = 0;
					bufferInfo.buffer = ((VulkanBuffer*)pBuffer)->GetBuffer();
					bufferInfo.offset = offset;
					bufferInfo.range = range;

					bufferInfos.Push(bufferInfo);
				}

				writeInfo.descriptorCount = u32(info.buffers.Size());
				writeInfo.pBufferInfo = bufferInfos.Data() + arrOffset;
			}

			writeInfos.Push(writeInfo);	
		}

		for (const Renderer::Core::DescriptorSetTextureSamplerInfo& info : texAndSamplers)
		{
			sizeT arrOffset = imageInfos.Size();

			Renderer::DescriptorSetBindingType type = bindings[info.binding].type;
			if (type == Renderer::DescriptorSetBindingType::CombinedImageSampler)
			{
				HV_ASSERT(info.textures.Size() == info.samplers.Size());
				for (sizeT i = 0; i < info.textures.Size(); ++i)
				{
					Renderer::Core::Texture* pTexture = info.textures[i];
					Renderer::Core::Sampler* pSampler = info.samplers[i];
						
					VkDescriptorImageInfo imageInfo = {};
					imageInfo.imageLayout = Helpers::GetImageLayout(pTexture->GetLayout());
					imageInfo.imageView = ((VulkanTexture*)pTexture)->GetImageView();
					imageInfo.sampler = ((VulkanSampler*)pSampler)->GetSampler();

					imageInfos.Push(imageInfo);
				}
			}
			else if (type == Renderer::DescriptorSetBindingType::Sampler)
			{
				for (sizeT i = 0; i < info.samplers.Size(); ++i)
				{
					Renderer::Core::Sampler* pSampler = info.samplers[i];

					VkDescriptorImageInfo imageInfo = {};
					imageInfo.sampler = ((VulkanSampler*)pSampler)->GetSampler();

					imageInfos.Push(imageInfo);
				}
			}
			else
			{
				for (sizeT i = 0; i < info.textures.Size(); ++i)
				{
					Renderer::Core::Texture* pTexture = info.textures[i];

					VkDescriptorImageInfo imageInfo = {};
					imageInfo.imageLayout = Helpers::GetImageLayout(pTexture->GetLayout());
					imageInfo.imageView = ((VulkanTexture*)pTexture)->GetImageView();

					imageInfos.Push(imageInfo);
				}
			}

			VkWriteDescriptorSet writeInfo = {};
			writeInfo.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			writeInfo.dstSet = m_DescriptorSet;
			writeInfo.descriptorType = Helpers::GetDescriptorType(bindings[info.binding].type);
			writeInfo.dstBinding = info.binding;
			writeInfo.dstArrayElement = info.arrayElement;

			writeInfo.descriptorCount = type == Renderer::DescriptorSetBindingType::Sampler ? u32(info.samplers.Size()) : u32(info.textures.Size());
			writeInfo.pImageInfo = imageInfos.Data() + arrOffset;

			writeInfos.Push(writeInfo);
		}

		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
		pDevice->vkUpdateDescriptorSets(u32(writeInfos.Size()), writeInfos.Data(), 0, nullptr);

		return true;
	}
}
