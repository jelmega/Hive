// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanQueue.cpp: Vulkan queue
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanQueue.h"
#include "VulkanRHI/VulkanContext.h"
#include "VulkanRHI/VulkanDevice.h"
#include "VulkanRHI/VulkanHelpers.h"

namespace Hv::VulkanRHI {


	VulkanQueue::VulkanQueue()
		: m_Queue(VK_NULL_HANDLE)
		, m_Vk()
		, m_Family(0)
	{
	}

	VulkanQueue::~VulkanQueue()
	{
	}

	b8 VulkanQueue::Init(RHI::RHIContext* pContext, Renderer::QueueType type, u32 index, Renderer::QueuePriority priority)
	{
		m_pContext = pContext;
		m_Type = type;
		m_Priority = priority;

		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
		m_Vk = pDevice->GetQueueFunctions();
		m_Family = pDevice->GetQueueFamily(m_Type);
		m_Queue = pDevice->vkGetQueue(m_Family, index);

		return true;
	}

	b8 VulkanQueue::WaitIdle()
	{
		VkResult vkres = m_Vk.QueueWaitIdle(m_Queue);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Error, "Failed to wait for the queue to be idle (VkResult: %s)!", Helpers::GetResultString(vkres));
			return false;
		}
		return true;
	}

	VkResult VulkanQueue::vkSubmit(const VkSubmitInfo& submitInfo, VkFence fence)
	{
		return m_Vk.QueueSubmit(m_Queue, 1, &submitInfo, fence);
	}

	VkResult VulkanQueue::vkSubmit(const DynArray<VkSubmitInfo>& submitInfo, VkFence fence)
	{
		return m_Vk.QueueSubmit(m_Queue, u32(submitInfo.Size()), submitInfo.Data(), fence);
	}

	VkResult VulkanQueue::vkPresent(const VkPresentInfoKHR& presentInfo)
	{
		return m_Vk.QueuePresentKHR(m_Queue, &presentInfo);
	}
}
