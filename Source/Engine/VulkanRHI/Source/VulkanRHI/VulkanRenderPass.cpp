// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanRenderPass.cpp: render pass
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanRenderPass.h"
#include "VulkanRHI/VulkanContext.h"
#include "VulkanRHI/VulkanDevice.h"
#include "VulkanRHI/VulkanHelpers.h"
#include "VulkanRHI/VulkanTexture.h"

namespace Hv::VulkanRHI {


	VulkanRenderPass::VulkanRenderPass()
		: m_RenderPass(VK_NULL_HANDLE)
	{
	}

	VulkanRenderPass::~VulkanRenderPass()
	{
	}

	b8 VulkanRenderPass::Create(RHI::RHIContext* pContext,
		const DynArray<Renderer::Core::RenderPassAttachment>& attachments,
		const DynArray<Renderer::Core::SubRenderPass>& subpasses)
	{
		m_pContext = pContext;
		m_Attachments = attachments;
		m_SubPasses = subpasses;

		// Create vulkan render pass attachments
		DynArray<VkAttachmentDescription> vulkanAttachments;
		for (const Renderer::Core::RenderPassAttachment& rpa : m_Attachments)
		{
			VkAttachmentDescription attachment = {};
			attachment.loadOp = Helpers::GetLoadOp(rpa.loadOp);
			attachment.storeOp = Helpers::GetStoreOp(rpa.storeOp);
			attachment.stencilLoadOp = Helpers::GetLoadOp(rpa.stencilLoadOp);
			attachment.stencilStoreOp = Helpers::GetStoreOp(rpa.stencilStoreOp);
			attachment.format = Helpers::GetFormat(rpa.format);
			attachment.samples = Helpers::GetSampleCount(rpa.samples);
			if (rpa.type == Renderer::RenderTargetType::Presentable)
			{
				attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
				attachment.finalLayout = Helpers::GetSubpassAttachmentLayout(rpa.type);
			}
			else
			{
				attachment.initialLayout = attachment.finalLayout = Helpers::GetSubpassAttachmentLayout(rpa.type);
			}

			vulkanAttachments.Push(attachment);
		}

		DynArray<SubpassDescription> subpassDescs;
		subpassDescs.Resize(m_SubPasses.Size());
		DynArray<VkSubpassDescription> vulkanSubpasses;
		vulkanSubpasses.Reserve(m_SubPasses.Size());
		for (sizeT i = 0; i < m_SubPasses.Size(); ++i)
		{
			const Renderer::Core::SubRenderPass& subpass = m_SubPasses[i];
			SubpassDescription& desc = subpassDescs[i];
			desc = {};
			desc.depthAttachment.attachment = VK_ATTACHMENT_UNUSED;

			for (const Renderer::Core::RenderPassAttachmentRef& attachmentRef : subpass.attachments)
			{
				if (attachmentRef.type == Renderer::RenderTargetType::Preserve)
				{
					desc.preserveAttachments.Push(attachmentRef.index);
					continue;
				}

				u32 index = attachmentRef.index;
				const Renderer::Core::RenderPassAttachment& attachment = m_Attachments[index];
				const VkAttachmentDescription& attachDesc = vulkanAttachments[index];

				VkAttachmentReference ref = {};
				ref.attachment = index;
				ref.layout = Helpers::GetSubpassAttachmentLayout(attachmentRef.type);
				if (ref.layout == VK_IMAGE_LAYOUT_PRESENT_SRC_KHR)
					ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

				switch (attachmentRef.type)
				{
				default:
				case Renderer::RenderTargetType::None: 
					break;
				case Renderer::RenderTargetType::Color:
				case Renderer::RenderTargetType::Presentable:
				{
					desc.colorAttachments.Push(ref);
					ref.attachment = VK_ATTACHMENT_UNUSED;
					desc.resolveAttachments.Push(ref);
					break;
				}
				case Renderer::RenderTargetType::DepthStencil:
				{
					desc.depthAttachment = ref;
					break;
				}
				case Renderer::RenderTargetType::MultisampleResolve:
				{
					desc.resolveAttachments.Push(ref);
					ref.attachment = VK_ATTACHMENT_UNUSED;
					desc.colorAttachments.Push(ref);
					break;
				}
				case Renderer::RenderTargetType::Input:
				{
					desc.inputAttachments.Push(ref);
					break;
				}
				}
			}

			desc.subpassDesc.colorAttachmentCount = u32(desc.colorAttachments.Size());
			desc.subpassDesc.pColorAttachments = desc.colorAttachments.Data();
			desc.subpassDesc.pResolveAttachments = desc.resolveAttachments.Data();
			desc.subpassDesc.pDepthStencilAttachment = &desc.depthAttachment;
			desc.subpassDesc.inputAttachmentCount = u32(desc.inputAttachments.Size());
			desc.subpassDesc.pInputAttachments = desc.inputAttachments.Data();
			desc.subpassDesc.preserveAttachmentCount = u32(desc.preserveAttachments.Size());
			desc.subpassDesc.pPreserveAttachments = desc.preserveAttachments.Data();

			desc.subpassDesc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

			vulkanSubpasses.Push(desc.subpassDesc);
		}
		
		VkRenderPassCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		createInfo.attachmentCount = u32(vulkanAttachments.Size());
		createInfo.pAttachments = vulkanAttachments.Data();
		createInfo.subpassCount = u32(vulkanSubpasses.Size());
		createInfo.pSubpasses = vulkanSubpasses.Data();

		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
		VkResult vkres = pDevice->vkCreateRenderPass(createInfo, m_RenderPass);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to create a vulkan render pass!", Helpers::GetResultString(vkres));
			return false;
		}

		return true;
	}

	b8 VulkanRenderPass::Destroy()
	{
		if (m_RenderPass)
		{
			VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
			pDevice->vkDestroyRenderPass(m_RenderPass);
			m_RenderPass = VK_NULL_HANDLE;
		}

		return true;
	}
}
