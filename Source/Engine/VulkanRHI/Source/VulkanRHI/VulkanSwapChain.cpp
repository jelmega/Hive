// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanRenderView.cpp: Vulkan render view (surface + swapchain)
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanSwapChain.h"
#include "VulkanRHI/VulkanContext.h"
#include "VulkanRHI/VulkanInstance.h"
#include "VulkanRHI/VulkanPhysicalDevice.h"
#include "VulkanRHI/VulkanDevice.h"
#include "VulkanRHI/VulkanHelpers.h"
#include "VulkanRHI/VulkanRenderTarget.h"
#include "VulkanRHI/VulkanSemaphore.h"
#include "VulkanRHI/VulkanQueue.h"

namespace Hv::VulkanRHI {
	
	VulkanSwapChain::VulkanSwapChain()
		: SwapChain()
		, m_Surface(VK_NULL_HANDLE)
		, m_Swapchain(VK_NULL_HANDLE)
	{
	}

	VulkanSwapChain::~VulkanSwapChain()
	{
	}

	b8 VulkanSwapChain::Init(RHI::RHIContext* pContext, Window* pWindow, Renderer::VSyncMode vsync, Renderer::Core::Queue* pQueue)
	{
		VulkanInstance* pInstance = ((VulkanContext*)pContext)->GetInstance();
		VulkanDevice* pDevice = ((VulkanContext*)pContext)->GetDevice();

		VkResult vkres;

		// Only create surface if none exists yet
		if (m_Surface == VK_NULL_HANDLE)
		{
			// Set variables (in if statement, since it only needs to be done once)
			m_pContext = pContext;
			m_pWindow = pWindow;
			m_pWindow->SetOnRendererResizeCallback(Delegate<void(u32, u32)>(this, &VulkanSwapChain::ResizeCallback));
			
			// Create surface
#if defined(VK_USE_PLATFORM_WIN32_KHR)

			VkWin32SurfaceCreateInfoKHR createInfo = {};
			createInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
			HAL::WindowSystem::WindowsWindowHandle* pWin32Handle = (HAL::WindowSystem::WindowsWindowHandle*)m_pWindow->GetHandle();
			createInfo.hwnd = pWin32Handle->handle;
			createInfo.hinstance = pWin32Handle->instance;
#else
#endif
			vkres = pInstance->vkCreateSurface(createInfo, m_Surface);
			if (vkres != VK_SUCCESS)
			{
				g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Error, "Failed to create surface (VkResult: %s)!", Helpers::GetResultString(vkres));
				return false;
			}

			// If device exists, only update the logical device's physical device
			if (pDevice)
			{
				// Update physical device surface support
				VulkanPhysicalDevice* pPhysicalDevice = pDevice->GetPhysicalDevice();
				vkres = pPhysicalDevice->UpdateSurfaceSupport(m_Surface);
				if (vkres != VK_SUCCESS)
				{
					g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Error, "Failed to create surface (VkResult: %s)!", Helpers::GetResultString(vkres));
					Destroy();
					return false;
				}
			}
			else // No logical device exists, so update all physical devices
			{
				vkres = ((VulkanContext*)m_pContext)->UpdateSurfaceSupport(m_Surface);
				if (!vkres)
					return false;
				return true;
			}
		}

		// Create swapchain (only possible when device exists)
		if (pDevice)
		{
			m_VSync = vsync;
			m_pPresentQueue = pQueue;

			// If window is minimized, we can't create the swapchain
			if (pWindow->IsMinimized())
				return true;

			VulkanPhysicalDevice* pPhysicalDevice = pDevice->GetPhysicalDevice();
			const VulkanPhysicalDevice::SurfaceSupport& surfaceSupport = pPhysicalDevice->GetSurfaceSupport();
			VkSurfaceFormatKHR format = Helpers::GetOptimalSurfaceFormat(surfaceSupport.formats);
			VkPresentModeKHR presentMode = Helpers::GetOptimalPresentMode(surfaceSupport.modes, m_VSync);

			VkSurfaceCapabilitiesKHR capabilities;
			pPhysicalDevice->QuerySurfaceCapabilities(m_Surface, capabilities);
			VkExtent2D extent = Helpers::GetSwapExtent(m_pWindow->GetSize(), capabilities);

			u32 minImages = vsync == Renderer::VSyncMode::Tripple ? 3 : 2;
			minImages = Math::Min(Math::Max(minImages, capabilities.minImageCount), capabilities.maxImageCount);

			// TODO: check how to handle special params
			VkSwapchainCreateInfoKHR createInfo = {};
			createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
			createInfo.surface = m_Surface;
			createInfo.imageFormat = format.format;
			createInfo.imageColorSpace = format.colorSpace;
			createInfo.presentMode = presentMode;
			createInfo.imageExtent = extent;
			createInfo.minImageCount = minImages;
			createInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
			// We don't want bending with other images for now
			createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
			createInfo.clipped = VK_TRUE;
			// For now, use basic values
			createInfo.imageArrayLayers = 1;
			createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
			// We made sure that the graphics queue supports present
			createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
			// Don't set the oldSwapchain on creation
			createInfo.oldSwapchain = VK_NULL_HANDLE;

			vkres = pDevice->vkCreateSwapchain(createInfo, m_Swapchain);
			if (vkres != VK_SUCCESS)
			{
				g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Error, "Failed to create swapchain (VkResult: %s)!", Helpers::GetResultString(vkres));
				Destroy();
				return false;
			}

			// Create render targets
			u32 imageCount;
			vkres = pDevice->vkGetSwapchainImages(m_Swapchain, imageCount, nullptr);
			if (vkres != VK_SUCCESS)
			{
				g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to retrieve the number of available surface present modes (VkResult: %s)!", Helpers::GetResultString(vkres));
				Destroy();
				return false;
			}
			DynArray<VkImage> images(imageCount);
			vkres = pDevice->vkGetSwapchainImages(m_Swapchain, imageCount, images.Data());
			if (vkres != VK_SUCCESS)
			{
				g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to retrieve the available surface present modes (VkResult: %s)!", Helpers::GetResultString(vkres));
				Destroy();
				return false;
			}

			for (VkImage image : images)
			{
				VulkanRenderTarget* pRenderTarget = HvNew VulkanRenderTarget();
				b8 res = pRenderTarget->Create(m_pContext, image, VK_IMAGE_LAYOUT_SHARED_PRESENT_KHR, extent.width, extent.height, Helpers::GetPixelFormat(format.format), Renderer::SampleCount::Sample1, Renderer::RenderTargetType::Presentable);
				if (!res)
				{
					HvDelete pRenderTarget;
					Destroy();
					return false;
				}
				m_RenderTargets.Push(pRenderTarget);
			}

			// Create semaphores
			for (u32 i = 0; i < m_RenderTargets.Size(); ++i)
			{
				// Wait semaphore
				VulkanSemaphore* pWaitSemaphore = HvNew VulkanSemaphore();
				b8 res = pWaitSemaphore->Create(m_pContext);
				if (!res)
				{
					g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Error, "Failed to create wait semaphore for backbuffer %u!", i);
					Destroy();
					return false;
				}

				m_WaitSemaphores.Push(pWaitSemaphore);

				// Signal semaphore
				VulkanSemaphore* pSignalSemaphore = HvNew VulkanSemaphore();
				res = pSignalSemaphore->Create(m_pContext);
				if (!res)
				{
					g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Error, "Failed to create signal semaphore for backbuffer %u!", i);
					Destroy();
					return false;
				}

				m_SignalSemaphores.Push(pSignalSemaphore);
			}

			// Retrieve the first image
			m_SemaphoreIndex = 0;
			VkSemaphore acquireSemaphore = ((VulkanSemaphore*)m_SignalSemaphores[m_SemaphoreIndex])->GetSemaphore();
			vkres = pDevice->vkAquireNextImage(m_Swapchain, m_RenderTargetIndex, acquireSemaphore);
			if (vkres != VK_SUCCESS)
			{
				g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Error, "Failed to acquire initial swapchain image (VkResult: %s)!", Helpers::GetResultString(vkres));
				Destroy();
				return false;
			}
		}

		return true;
	}

	b8 VulkanSwapChain::Destroy()
	{
		b8 res = true;

		// Make sure renderview resources aren't in use
		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
		if (pDevice)
			res = pDevice->WaitIdle();

		for (Renderer::Core::Semaphore* pSemaphore : m_SignalSemaphores)
		{
			pSemaphore->Destroy();
			HvDelete pSemaphore;
		}
		m_SignalSemaphores.Clear();

		for (Renderer::Core::Semaphore* pSemaphore : m_WaitSemaphores)
		{
			pSemaphore->Destroy();
			HvDelete pSemaphore;
		}
		m_WaitSemaphores.Clear();

		for (Renderer::Core::RenderTarget* pRenderTarget : m_RenderTargets)
		{
			b8 tmpRes = pRenderTarget->Destroy();
			if (!tmpRes)
			{
				g_Logger.LogError("Failed to destroy render view render target!");
			}
			HvDelete pRenderTarget;
			res &= tmpRes;
		}
		m_RenderTargets.Clear();

		if (m_Swapchain)
		{
			pDevice->vkDestroySwapchain(m_Swapchain);
			m_Swapchain = VK_NULL_HANDLE;
		}

		if (m_Surface)
		{
			VulkanInstance* pInstance = ((VulkanContext*)m_pContext)->GetInstance();
			pInstance->vkDestroySurface(m_Surface);
			m_Surface = VK_NULL_HANDLE;
		}
		return res;
	}

	b8 VulkanSwapChain::Present()
	{
		VkPresentInfoKHR presentInfo = {};
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = &m_Swapchain;
		presentInfo.pImageIndices = &m_RenderTargetIndex;

		VkSemaphore semaphore = ((VulkanSemaphore*)m_WaitSemaphores[m_SemaphoreIndex])->GetSemaphore();
		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = &semaphore;

		VkResult vkres = ((VulkanQueue*)m_pPresentQueue)->vkPresent(presentInfo);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to present a vulkan swapchain (VkResult: %s)!", Helpers::GetResultString(vkres));
			Destroy();
			return false;
		}

		++m_SemaphoreIndex %= m_RenderTargets.Size();
		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
		VkSemaphore acquireSemaphore = ((VulkanSemaphore*)m_SignalSemaphores[m_SemaphoreIndex])->GetSemaphore();
		vkres = pDevice->vkAquireNextImage(m_Swapchain, m_RenderTargetIndex, acquireSemaphore);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Error, "Failed to acquire initial swapchain image (VkResult: %s)!", Helpers::GetResultString(vkres));
			Destroy();
			return false;
		}
		
		return true;
	}

	void VulkanSwapChain::ResizeCallback(u32 width, u32 height)
	{
		HV_UNREFERENCED_PARAM(width);
		HV_UNREFERENCED_PARAM(height);
		Destroy();
		Init(m_pContext, m_pWindow, m_VSync, m_pPresentQueue);
	}

}
