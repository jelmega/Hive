// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanPhysicalDevice.cpp: Vulkan physical device
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanPhysicalDevice.h"
#include "VulkanRHI/VulkanHelpers.h"
#include "VulkanRHI/VulkanBuffer.h"
#include "Renderer/Core/Queue.h"

namespace Hv::VulkanRHI {

	VulkanPhysicalDevice::VulkanPhysicalDevice()
		: m_pContext(nullptr)
		, m_Vk()
		, m_PhysicalDevice(VK_NULL_HANDLE)
		, m_Properties()
		, m_Features()
		, m_MemoryProperties()
		, m_FormatProperties{}
		, m_SurfaceSupport()
		, m_GpuInfo()
		, m_ApiVersion()
	{
	}

	VulkanPhysicalDevice::~VulkanPhysicalDevice()
	{
	}

	VkResult VulkanPhysicalDevice::Init(VulkanContext* pContext, VkPhysicalDevice physicalDevice, const VulkanPhysicalDeviceFuncs& vkPhysicalDeviceFuncs)
	{
		m_pContext = pContext;
		m_PhysicalDevice = physicalDevice;
		m_Vk = vkPhysicalDeviceFuncs;

		// Retrieve properties and features
		m_Vk.GetPhysicalDeviceProperties(m_PhysicalDevice, &m_Properties);
		m_Vk.GetPhysicalDeviceFeatures(m_PhysicalDevice, &m_Features);

		// Get device info
		m_GpuInfo.vendor = Helpers::GetVendor(m_Properties.vendorID);
		m_GpuInfo.type = Helpers::GetGpuType(m_Properties.deviceType);
		m_GpuInfo.driver = Helpers::GetDriverVersion(m_Properties.driverVersion);
		Memory::Copy(m_GpuInfo.name, m_Properties.deviceName, sizeof(m_GpuInfo.name));
		m_ApiVersion = Helpers::GetApiVersion(m_Properties.apiVersion);

		// Retrieve queue family properties
		u32 numQueueFamilies;
		m_Vk.GetPhysicalDeviceQueueFamilyProperties(m_PhysicalDevice, &numQueueFamilies, nullptr);
		m_QueueFamilyProperties.Resize(numQueueFamilies);
		m_Vk.GetPhysicalDeviceQueueFamilyProperties(m_PhysicalDevice, &numQueueFamilies, m_QueueFamilyProperties.Data());

		// Retrieve memory properties
		m_Vk.GetPhysicalDeviceMemoryProperties(m_PhysicalDevice, &m_MemoryProperties);

		// Retrieve format properties
		m_FormatProperties[0].supported = true;
		m_FormatProperties[0].properties = {};
		for (u32 i = 1; i < VK_FORMAT_RANGE_SIZE; ++i)
		{
			m_Vk.GetPhysicalDeviceFormatProperties(m_PhysicalDevice, VkFormat(i), &m_FormatProperties[i].properties);
			m_FormatProperties[i].supported = m_FormatProperties[i].properties.bufferFeatures || 
											  m_FormatProperties[i].properties.linearTilingFeatures ||
											  m_FormatProperties[i].properties.optimalTilingFeatures;
		}

		// Enumerate available extensions
		u32 numExtensions = 0;
		VkResult vkres = m_Vk.EnumerateDeviceExtensionProperties(m_PhysicalDevice, nullptr, &numExtensions, nullptr);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to retrieve the number of available instance extensions (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}
		m_AvailableExtensions.Resize(numExtensions);
		vkres = m_Vk.EnumerateDeviceExtensionProperties(m_PhysicalDevice, nullptr, &numExtensions, m_AvailableExtensions.Data());
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to retrieve the available instance extensions (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}

		// Enumerate available layers
		u32 numLayers = 0;
		vkres = m_Vk.EnumerateDeviceLayerProperties(m_PhysicalDevice, &numLayers, nullptr);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to retrieve the number of available instance layers (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}
		m_AvailableLayers.Resize(numLayers);
		vkres = m_Vk.EnumerateDeviceLayerProperties(m_PhysicalDevice, &numLayers, m_AvailableLayers.Data());
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to retrieve the available instance extensions (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}

		return VK_SUCCESS;
	}

	b8 VulkanPhysicalDevice::IsExtensionAvailable(const String& extension)
	{
		for (const VkExtensionProperties& availableExtension : m_AvailableExtensions)
		{
			if (availableExtension.extensionName == extension)
				return true;
		}
		return false;
	}

	b8 VulkanPhysicalDevice::IsLayerAvailable(const String& layer)
	{
		for (const VkLayerProperties& availableExtension : m_AvailableLayers)
		{
			if (availableExtension.layerName == layer)
				return true;
		}
		return false;
	}

	b8 VulkanPhysicalDevice::AreExtensionsSupported(const DynArray<const AnsiChar*>& extensions)
	{
		for (const AnsiChar* name : extensions)
		{
			if (!IsExtensionAvailable(name))
				return false;
		}
		return true;
	}

	b8 VulkanPhysicalDevice::AreLayersSupported(const DynArray<const AnsiChar*>& layers)
	{
		for (const AnsiChar* name : layers)
		{
			if (!IsLayerAvailable(name))
				return false;
		}
		return true;
	}

	void VulkanPhysicalDevice::LogInfo()
	{
		g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Detail, "\tVulkan %u.%u", m_ApiVersion.major, m_ApiVersion.minor);
		g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Detail, "\tVendor: %s", Renderer::Helpers::GetGpuVendorString(m_GpuInfo.vendor));
		g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Detail, "\t%s", m_GpuInfo.name);
		g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Detail, "\tdriver: %u.%u", m_GpuInfo.driver.major, m_GpuInfo.driver.minor);

		// Log device extensions
		for (VkExtensionProperties& extension : m_AvailableExtensions)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Detail, "\tdevice extension: %s, version: %u", extension.extensionName, extension.specVersion);
		}
	}

	u32 VulkanPhysicalDevice::GetQueueFamily(Renderer::QueueType type)
	{
		// TODO: figure out vulkan sparse queues, since AMD has support on more queues than the main queue
		// try to find a specialized queue family for each type
		u32 curFamily = 0;

		// Present support should be initialized
		HV_ASSERT(m_SurfaceSupport.support.Size() == m_QueueFamilyProperties.Size());
		for (u32 i = 0; i < m_QueueFamilyProperties.Size(); ++i)
		{
			const VkQueueFamilyProperties& props = m_QueueFamilyProperties[i];
			b8 supportPresent = m_SurfaceSupport.support[i];

			if (HV_IS_ENUM_FLAG_SET(type, Renderer::QueueType::Graphics))  // Just make sure graphics is supported
			{
				if (props.queueFlags & VK_QUEUE_GRAPHICS_BIT && supportPresent)
				{
					return i;
				}
			}
			else if (HV_IS_ENUM_FLAG_SET(type, Renderer::QueueType::Compute)) // make sure compute is supported, but try to avoid graphics
			{
				if (props.queueFlags & VK_QUEUE_COMPUTE_BIT)
				{
					if (!(props.queueFlags & VK_QUEUE_GRAPHICS_BIT))
						return i;
					curFamily = i;
				}
			}
			else if (HV_IS_ENUM_FLAG_SET(type, Renderer::QueueType::Transfer)) // Make sure transfer is supported and try to avoid graphics and compute
			{
				if (props.queueFlags & VK_QUEUE_TRANSFER_BIT)
				{
					if (!(props.queueFlags & VK_QUEUE_GRAPHICS_BIT) && !(props.queueFlags & VK_QUEUE_COMPUTE_BIT))
						return i;
					curFamily = i;
				}
			}
		}

		return curFamily;
	}

	VkResult VulkanPhysicalDevice::UpdateSurfaceSupport(VkSurfaceKHR surface)
	{
		VkResult vkres;
		// Retrieve present support
		// Present support always needs to be updated, if not called vulkan gives the follwing error:
		// "vkQueuePresentKHR: Presenting image without calling vkGetPhysicalDeviceSurfaceSupportKHR!"
		for (u32 i = 0; i < m_QueueFamilyProperties.Size(); ++i)
		{
			VkBool32 supported;
			vkres = m_Vk.GetPhysicalDeviceSurfaceSupportKHR(m_PhysicalDevice, i, surface, &supported);
			if (vkres != VK_SUCCESS)
			{
				g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to retrieve present support for family %u (VkResult: %s)!", i, Helpers::GetResultString(vkres));
				return vkres;
			}
			if (!m_SurfaceSupport.initialized)
				m_SurfaceSupport.support.Push(supported == VK_TRUE);
		}

		// only needs to run once, since formats and present modes won't change
		if (!m_SurfaceSupport.initialized)
		{
			// Retrieve surface formats
			u32 numFormats;
			vkres = m_Vk.GetPhysicalDeviceSurfaceFormatsKHR(m_PhysicalDevice, surface, &numFormats, nullptr);
			if (vkres != VK_SUCCESS)
			{
				g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to retrieve the number of available surface present modes (VkResult: %s)!", Helpers::GetResultString(vkres));
				return vkres;
			}
			m_SurfaceSupport.formats.Resize(numFormats);
			vkres = m_Vk.GetPhysicalDeviceSurfaceFormatsKHR(m_PhysicalDevice, surface, &numFormats, m_SurfaceSupport.formats.Data());
			if (vkres != VK_SUCCESS)
			{
				g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to retrieve the available surface present modes (VkResult: %s)!", Helpers::GetResultString(vkres));
				return vkres;
			}

			// Retrieve surface modes
			u32 numModes;
			vkres = m_Vk.GetPhysicalDeviceSurfacePresentModesKHR(m_PhysicalDevice, surface, &numModes, nullptr);
			if (vkres != VK_SUCCESS)
			{
				g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to retrieve the number of available surface present modes (VkResult: %s)!", Helpers::GetResultString(vkres));
				return vkres;
			}
			m_SurfaceSupport.modes.Resize(numModes);
			vkres = m_Vk.GetPhysicalDeviceSurfacePresentModesKHR(m_PhysicalDevice, surface, &numModes, m_SurfaceSupport.modes.Data());
			if (vkres != VK_SUCCESS)
			{
				g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to retrieve the available surface present modes (VkResult: %s)!", Helpers::GetResultString(vkres));
				return vkres;
			}

			m_SurfaceSupport.initialized = true;
		}

		return VK_SUCCESS;
	}

	VkResult VulkanPhysicalDevice::QuerySurfaceCapabilities(VkSurfaceKHR surface,
		VkSurfaceCapabilitiesKHR& capabilities)
	{
		VkResult vkres = m_Vk.GetPhysicalDeviceSurfaceCapabilitiesKHR(m_PhysicalDevice, surface, &capabilities);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Error, "Failed to query surface capabilities (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}
		return vkres;
	}
}
