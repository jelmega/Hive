// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanCommandList.h: Vulkan command list
#pragma once
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanCommandList.h"
#include "VulkanRHI/VulkanContext.h"
#include "VulkanRHI/VulkanDevice.h"
#include "VulkanRHI/VulkanHelpers.h"
#include "VulkanRHI/VulkanRenderPass.h"
#include "VulkanRHI/VulkanQueue.h"
#include "VulkanRHI/VulkanFence.h"
#include "VulkanRHI/VulkanSemaphore.h"
#include "VulkanRHI/VulkanDynamicRHI.h"
#include "VulkanRHI/VulkanTexture.h"

#define CHECK_RECORDING\
	if (m_Status != Renderer::CommandListState::Recording)\
	{\
		g_Logger.LogWarning(LogVulkanRHI(), "Can't record to command list that isn't in the recording state");\
		return;\
	}

namespace Hv::VulkanRHI {

	VulkanCommandList::VulkanCommandList()
		: m_CommandBuffer(VK_NULL_HANDLE)
		, m_Vk()
		, m_SrcStage(Renderer::PipelineStage::None)
		, m_DstStage(Renderer::PipelineStage::None)
	{
	}

	VulkanCommandList::~VulkanCommandList()
	{
	}

	b8 VulkanCommandList::Begin()
	{
		if (m_Status != Renderer::CommandListState::Reset && m_Status != Renderer::CommandListState::Finished)
		{
			g_Logger.LogWarning(LogVulkanRHI(), "Failed to begin the command list, command list is still recording or in use!");
			return false;
		}

		// TODO: other flags for command buffer begin ?
		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
		VkResult vkres = m_Vk.BeginCommandBuffer(m_CommandBuffer, &beginInfo);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to begin the vulkan command buffer (VkResult: %s)!", Helpers::GetResultString(vkres));
			return false;
		}

		b8 res = m_pFence->Reset();
		if (!res)
		{
			g_Logger.LogError(LogVulkanRHI(), "Failed to reset the command list's fence");
			return false;
		}

		m_BoundInputSlots.Clear();

		m_Status = Renderer::CommandListState::Recording;
		return true;
	}

	b8 VulkanCommandList::End()
	{
		UpdateBarriers();

		if (m_Status != Renderer::CommandListState::Recording)
		{
			g_Logger.LogWarning(LogVulkanRHI(), "Failed to end the command list, command list is not in a recording state!");
			return false;
		}

		if (m_pRenderPass)
		{
			g_Logger.LogError(LogVulkanRHI(), "Failed to end the command list, command list is still inside renderpass!");
			return false;
		}

		VkResult vkres = m_Vk.EndCommandBuffer(m_CommandBuffer);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to end the vulkan command buffer (VkResult: %s)!", Helpers::GetResultString(vkres));
			return false;
		}

		m_Status = Renderer::CommandListState::Recorded;
		return true;
	}

	void VulkanCommandList::BindPipeline(Renderer::Core::Pipeline* pPipeline)
	{
		CHECK_RECORDING;
		m_pPipeline = pPipeline;

		VulkanPipeline* pVulkanPipeline = (VulkanPipeline*)m_pPipeline;
		VkPipelineBindPoint bindPoint = Helpers::GetPipelineBindPoint(pPipeline->GetType());

		m_Vk.CmdBindPipeline(m_CommandBuffer, bindPoint, pVulkanPipeline->GetPipeline());
	}

	void VulkanCommandList::BeginRenderPass(Renderer::Core::RenderPass* pRenderPass, Renderer::Core::Framebuffer* pFramebuffer)
	{
		CHECK_RECORDING;
		m_pRenderPass = pRenderPass;
		m_pFramebuffer = pFramebuffer;

		VulkanRenderPass* pVulkanRenderPass = (VulkanRenderPass*)m_pRenderPass;
		VulkanFramebuffer* pVulkanFramebuffer = (VulkanFramebuffer*)m_pFramebuffer;

		const DynArray<Renderer::Core::RenderTarget*>& renderTargets = pVulkanFramebuffer->GetRenderTargets();
		DynArray<VkClearValue> clearValues;

		for (const Renderer::Core::RenderTarget* pRT : renderTargets)
		{
			if (pRT->GetType() == Renderer::RenderTargetType::DepthStencil)
			{
				VkClearValue vkValue;
				Renderer::Core::ClearValue value = pRT->GetClearValue();

				vkValue.depthStencil.depth = value.depth;
				vkValue.depthStencil.stencil = value.stencil;
				vkValue.color.float32[0] = value.depth;
				clearValues.Push(vkValue);
			}
			else
			{
				VkClearValue vkValue;
				Renderer::Core::ClearValue value = pRT->GetClearValue();

				vkValue.color.float32[0] = value.color.data[0];
				vkValue.color.float32[1] = value.color.data[1];
				vkValue.color.float32[2] = value.color.data[2];
				vkValue.color.float32[3] = value.color.data[3];
				clearValues.Push(vkValue);
			}
		}

		VkRenderPassBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		beginInfo.renderPass = pVulkanRenderPass->GetRenderPass();
		beginInfo.framebuffer = pVulkanFramebuffer->GetFrameBuffer();
		beginInfo.clearValueCount = u32(clearValues.Size());
		beginInfo.pClearValues = clearValues.Data();
		beginInfo.renderArea.extent.width = m_pFramebuffer->GetWidth();
		beginInfo.renderArea.extent.height = m_pFramebuffer->GetHeight();

		// TODO: Currently disallow secondary buffer, figure it out when implementing DX12
		m_Vk.CmdBeginRenderPass(m_CommandBuffer, &beginInfo, VK_SUBPASS_CONTENTS_INLINE);
	}

	void VulkanCommandList::EndRenderPass()
	{
		CHECK_RECORDING;
		UpdateBarriers();

		if (m_pRenderPass)
		{
			m_Vk.CmdEndRenderPass(m_CommandBuffer);
			m_pRenderPass = nullptr;
		}
	}

	void VulkanCommandList::BindVertexBuffer(u16 inputSlot, Renderer::Core::Buffer* pBuffer, u64 offset)
	{
		CHECK_RECORDING;
		HV_ASSERT(pBuffer->GetType() == Renderer::BufferType::Vertex || pBuffer->GetType() == Renderer::BufferType::Default);
		UpdateBarriers();

		m_BoundInputSlots.Push(inputSlot);
		VkBuffer buffer = ((VulkanBuffer*)pBuffer)->GetBuffer();
		m_Vk.CmdBindVertexBuffers(m_CommandBuffer, inputSlot, 1, &buffer, &offset);
	}

	void VulkanCommandList::BindVertexBuffer(u16 inputSlot, const DynArray<Renderer::Core::Buffer*>& buffers, const DynArray<u64>& offsets)
	{
		CHECK_RECORDING;
		HV_ASSERT(buffers.Size() == offsets.Size());
		UpdateBarriers();

		DynArray<VkBuffer> vkBuffers;
		vkBuffers.Reserve(buffers.Size());
		u16 slot = inputSlot;
		for (Renderer::Core::Buffer* pBuffer : buffers)
		{
			HV_ASSERT(pBuffer->GetType() == Renderer::BufferType::Vertex || pBuffer->GetType() == Renderer::BufferType::Default);
			m_BoundInputSlots.Push(slot++);
			vkBuffers.Push(((VulkanBuffer*)pBuffer)->GetBuffer());
		}
		m_Vk.CmdBindVertexBuffers(m_CommandBuffer, inputSlot, u32(vkBuffers.Size()), vkBuffers.Data(), offsets.Data());
	}

	void VulkanCommandList::BindIndexBuffer(Renderer::Core::Buffer* pBuffer, u64 offset)
	{
		CHECK_RECORDING;
		UpdateBarriers();

		HV_ASSERT(pBuffer->GetType() == Renderer::BufferType::Index);
		VkBuffer buffer = ((VulkanBuffer*)pBuffer)->GetBuffer();
		m_Vk.CmdBindIndexBuffer(m_CommandBuffer, buffer, offset, Helpers::GetIndexType(pBuffer->GetIndexType()));
	}

	void VulkanCommandList::BindIndexBuffer(Renderer::Core::Buffer* pBuffer, u64 offset, Renderer::IndexType type)
	{
		CHECK_RECORDING;
		UpdateBarriers();

		HV_ASSERT(pBuffer->GetType() == Renderer::BufferType::Index || pBuffer->GetType() == Renderer::BufferType::Default);
		VkBuffer buffer = ((VulkanBuffer*)pBuffer)->GetBuffer();
		m_Vk.CmdBindIndexBuffer(m_CommandBuffer, buffer, offset, Helpers::GetIndexType(type));
	}

	void VulkanCommandList::BindDescriptorSets(u32 firstSet, Renderer::Core::DescriptorSet* pSet)
	{
		CHECK_RECORDING;
		UpdateBarriers();

		HV_ASSERT(m_pPipeline);
		VkPipelineBindPoint bindpoint = Helpers::GetPipelineBindPoint(m_pPipeline->GetType());
		VkPipelineLayout layout = ((VulkanPipeline*)m_pPipeline)->GetLayout();
		VkDescriptorSet vulkanSet = ((VulkanDescriptorSet*)pSet)->GetDescriptorSet();

		m_Vk.CmdBindDescriptorSets(m_CommandBuffer, bindpoint, layout, firstSet, 1, &vulkanSet, 0, nullptr);
	}

	void VulkanCommandList::BindDescriptorSets(u32 firstSet, Renderer::Core::DescriptorSet* pSet, u32 dynamicOffset)
	{
		CHECK_RECORDING;
		UpdateBarriers();

		HV_ASSERT(m_pPipeline);
		VkPipelineBindPoint bindpoint = Helpers::GetPipelineBindPoint(m_pPipeline->GetType());
		VkPipelineLayout layout = ((VulkanPipeline*)m_pPipeline)->GetLayout();
		VkDescriptorSet vulkanSet = ((VulkanDescriptorSet*)pSet)->GetDescriptorSet();

		m_Vk.CmdBindDescriptorSets(m_CommandBuffer, bindpoint, layout, firstSet, 1, &vulkanSet, 1, &dynamicOffset);
	}

	void VulkanCommandList::BindDescriptorSets(u32 firstSet, const DynArray<Renderer::Core::DescriptorSet*>& sets)
	{
		CHECK_RECORDING;
		UpdateBarriers();

		HV_ASSERT(m_pPipeline);
		VkPipelineBindPoint bindpoint = Helpers::GetPipelineBindPoint(m_pPipeline->GetType());
		VkPipelineLayout layout = ((VulkanPipeline*)m_pPipeline)->GetLayout();

		DynArray<VkDescriptorSet> vulkanSets;
		vulkanSets.Reserve(sets.Size());
		for (const Renderer::Core::DescriptorSet* pSet : sets)
		{
			vulkanSets.Push(((VulkanDescriptorSet*)pSet)->GetDescriptorSet());
		}

		m_Vk.CmdBindDescriptorSets(m_CommandBuffer, bindpoint, layout, firstSet, u32(vulkanSets.Size()), vulkanSets.Data(), 0, nullptr);
	}

	void VulkanCommandList::BindDescriptorSets(u32 firstSet, const DynArray<Renderer::Core::DescriptorSet*>& sets,
		const DynArray<u32>& dynamicOffsets)
	{
		CHECK_RECORDING;
		UpdateBarriers();

		HV_ASSERT(m_pPipeline);
		VkPipelineBindPoint bindpoint = Helpers::GetPipelineBindPoint(m_pPipeline->GetType());
		VkPipelineLayout layout = ((VulkanPipeline*)m_pPipeline)->GetLayout();

		DynArray<VkDescriptorSet> vulkanSets;
		vulkanSets.Reserve(sets.Size());
		for (const Renderer::Core::DescriptorSet* pSet : sets)
		{
			vulkanSets.Push(((VulkanDescriptorSet*)pSet)->GetDescriptorSet());
		}

		m_Vk.CmdBindDescriptorSets(m_CommandBuffer, bindpoint, layout, firstSet, u32(vulkanSets.Size()), vulkanSets.Data(), u32(dynamicOffsets.Size()), dynamicOffsets.Data());
	}

	void VulkanCommandList::SetViewport(Renderer::Viewport& viewport)
	{
		CHECK_RECORDING;
		UpdateBarriers();

		VkViewport vp;
		vp.x = viewport.x;
		vp.y = viewport.y;
		vp.width = viewport.width;
		vp.height = viewport.height;
		vp.minDepth = viewport.minDepth;
		vp.maxDepth = viewport.maxDepth;
		m_Vk.CmdSetViewport(m_CommandBuffer, 0, 1, &vp);
	}

	void VulkanCommandList::SetScissor(Renderer::ScissorRect& scissor)
	{
		CHECK_RECORDING;
		UpdateBarriers();

		VkRect2D rect;
		rect.offset.x = scissor.x;
		rect.offset.y = scissor.y;
		rect.extent.width = scissor.width;
		rect.extent.height = scissor.height;
		m_Vk.CmdSetScissor(m_CommandBuffer, 0, 1, &rect);
	}

	void VulkanCommandList::Draw(u32 vertexCount, u32 instanceCount, u32 firstVertex, u32 firstInstance)
	{
		CHECK_RECORDING;
		UpdateBarriers();

		m_Vk.CmdDraw(m_CommandBuffer, vertexCount, instanceCount, firstVertex, firstInstance);
	}

	void VulkanCommandList::DrawIndexed(u32 indexCount, u32 instanceCount, u32 firstIndex, u32 vertexOffset,
		u32 firstInstance)
	{
		CHECK_RECORDING;
		UpdateBarriers();

		m_Vk.CmdDrawIndexed(m_CommandBuffer, indexCount, instanceCount, firstIndex, vertexOffset, firstInstance);
	}

	void VulkanCommandList::CopyBuffer(Renderer::Core::Buffer* pSrcBuffer, u64 srcOffset,
		Renderer::Core::Buffer* pDstBuffer, u64 dstOffset, u64 size)
	{
		CHECK_RECORDING;
		UpdateBarriers();

		VkBuffer vkSrcBuffer = ((VulkanBuffer*)pSrcBuffer)->GetBuffer();
		VkBuffer vkDstBuffer = ((VulkanBuffer*)pDstBuffer)->GetBuffer();
		VkBufferCopy region = {};
		region.srcOffset = srcOffset;
		region.dstOffset = dstOffset;
		region.size = size;

		m_Vk.CmdCopyBuffer(m_CommandBuffer, vkSrcBuffer, vkDstBuffer, 1, &region);
	}

	void VulkanCommandList::CopyBuffer(Renderer::Core::Buffer* pSrcBuffer, Renderer::Core::Buffer* pDstBuffer,
		const DynArray<Renderer::BufferCopyRegion>& regions)
	{
		CHECK_RECORDING;
		UpdateBarriers();

		DynArray<VkBufferCopy> copies;
		copies.Reserve(regions.Size());

		for (const Renderer::BufferCopyRegion& region : regions)
		{
			VkBufferCopy copy = {};
			copy.srcOffset = region.srcOffset;
			copy.dstOffset = region.dstOffset;
			copy.size = region.size;
			copies.Push(copy);
		}

		VkBuffer vkSrcBuffer = ((VulkanBuffer*)pSrcBuffer)->GetBuffer();
		VkBuffer vkDstBuffer = ((VulkanBuffer*)pDstBuffer)->GetBuffer();
		m_Vk.CmdCopyBuffer(m_CommandBuffer, vkSrcBuffer, vkDstBuffer, u32(copies.Size()), copies.Data());
	}

	void VulkanCommandList::CopyTexture(Renderer::Core::Texture* pSrcTex, Renderer::Core::Texture* pDstTex,
		const Renderer::TextureCopyRegion& region)
	{
		CHECK_RECORDING;
		UpdateBarriers();

		VkImageCopy copyRegion = {};
		copyRegion.extent.width = region.extent.x;
		copyRegion.extent.height = region.extent.y;
		copyRegion.extent.depth = region.extent.z;

		copyRegion.srcOffset.x = region.srcOffset.x;
		copyRegion.srcOffset.y = region.srcOffset.y;
		copyRegion.srcOffset.z = region.srcOffset.z;

		VkImageAspectFlags aspect = pSrcTex->GetFormat().HasDepthComponent() ? VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;
		if (pSrcTex->GetFormat().HasStencilComponent())
			aspect |= VK_IMAGE_ASPECT_STENCIL_BIT;
		copyRegion.srcSubresource.aspectMask = aspect;
		copyRegion.srcSubresource.mipLevel = region.srcMipLevel;
		copyRegion.srcSubresource.baseArrayLayer = region.srcBaseArrayLayer;
		copyRegion.srcSubresource.layerCount = region.layerCount;

		copyRegion.dstOffset.x = region.dstOffset.x;
		copyRegion.dstOffset.y = region.dstOffset.y;
		copyRegion.dstOffset.z = region.dstOffset.z;

		aspect = pDstTex->GetFormat().HasDepthComponent() ? VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;
		if (pDstTex->GetFormat().HasStencilComponent())
			aspect |= VK_IMAGE_ASPECT_STENCIL_BIT;
		copyRegion.dstSubresource.aspectMask = aspect;
		copyRegion.dstSubresource.mipLevel = region.dstMipLevel;
		copyRegion.dstSubresource.baseArrayLayer = region.dstBaseArrayLayer;
		copyRegion.dstSubresource.layerCount = region.layerCount;

		VkImage srcImage = ((VulkanTexture*)pSrcTex)->GetImage();
		VkImageLayout srcLayout = Helpers::GetImageLayout(pSrcTex->GetLayout());
		VkImage dstImage = ((VulkanTexture*)pDstTex)->GetImage();
		VkImageLayout dstLayout = Helpers::GetImageLayout(pDstTex->GetLayout());
		m_Vk.CmdCopyImage(m_CommandBuffer, srcImage, srcLayout, dstImage, dstLayout, 1, &copyRegion);
	}

	void VulkanCommandList::CopyTexture(Renderer::Core::Texture* pSrcTex, Renderer::Core::Texture* pDstTex,
		const DynArray<Renderer::TextureCopyRegion>& regions)
	{
		CHECK_RECORDING;
		UpdateBarriers();

		DynArray<VkImageCopy> copyRegions;
		copyRegions.Reserve(regions.Size());

		for (const Renderer::TextureCopyRegion& region : regions)
		{
			VkImageCopy copyRegion = {};
			copyRegion.extent.width = region.extent.x;
			copyRegion.extent.height = region.extent.y;
			copyRegion.extent.depth = region.extent.z;

			copyRegion.srcOffset.x = region.srcOffset.x;
			copyRegion.srcOffset.y = region.srcOffset.y;
			copyRegion.srcOffset.z = region.srcOffset.z;

			VkImageAspectFlags aspect = pSrcTex->GetFormat().HasDepthComponent() ? VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;
			if (pSrcTex->GetFormat().HasStencilComponent())
				aspect |= VK_IMAGE_ASPECT_STENCIL_BIT;
			copyRegion.srcSubresource.aspectMask = aspect;
			copyRegion.srcSubresource.mipLevel = region.srcMipLevel;
			copyRegion.srcSubresource.baseArrayLayer = region.srcBaseArrayLayer;
			copyRegion.srcSubresource.layerCount = region.layerCount;

			copyRegion.dstOffset.x = region.dstOffset.x;
			copyRegion.dstOffset.y = region.dstOffset.y;
			copyRegion.dstOffset.z = region.dstOffset.z;

			aspect = pDstTex->GetFormat().HasDepthComponent() ? VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;
			if (pDstTex->GetFormat().HasStencilComponent())
				aspect |= VK_IMAGE_ASPECT_STENCIL_BIT;
			copyRegion.dstSubresource.aspectMask = aspect;
			copyRegion.dstSubresource.mipLevel = region.dstMipLevel;
			copyRegion.dstSubresource.baseArrayLayer = region.dstBaseArrayLayer;
			copyRegion.dstSubresource.layerCount = region.layerCount;

			copyRegions.Push(copyRegion);
		}

		VkImage srcImage = ((VulkanTexture*)pSrcTex)->GetImage();
		VkImageLayout srcLayout = Helpers::GetImageLayout(pSrcTex->GetLayout());
		VkImage dstImage = ((VulkanTexture*)pDstTex)->GetImage();
		VkImageLayout dstLayout = Helpers::GetImageLayout(pDstTex->GetLayout());
		m_Vk.CmdCopyImage(m_CommandBuffer, srcImage, srcLayout, dstImage, dstLayout, u32(copyRegions.Size()), copyRegions.Data());
	}

	void VulkanCommandList::CopyBufferToTexture(Renderer::Core::Buffer* pBuffer, Renderer::Core::Texture* pTexture,
		const Renderer::TextureBufferCopyRegion& region)
	{
		CHECK_RECORDING;
		UpdateBarriers();

		VkBufferImageCopy copyRegion = {};
		copyRegion.bufferOffset = region.bufferOffset;
		copyRegion.bufferRowLength = region.bufferRowLength;
		copyRegion.bufferImageHeight = region.bufferImageHeight;
		copyRegion.imageExtent.width = region.texExtent.x;
		copyRegion.imageExtent.height = region.texExtent.y;
		copyRegion.imageExtent.depth = region.texExtent.z;
		copyRegion.imageOffset.x = i32(region.texOffset.x);
		copyRegion.imageOffset.y = i32(region.texOffset.y);
		copyRegion.imageOffset.z = i32(region.texOffset.z);

		PixelFormat format = pTexture->GetFormat();
		VkImageAspectFlags aspect = format.HasDepthComponent() ? VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;
		if (format.HasStencilComponent())
			aspect |= VK_IMAGE_ASPECT_STENCIL_BIT;

		copyRegion.imageSubresource.aspectMask = aspect;
		copyRegion.imageSubresource.mipLevel = region.mipLevel;
		copyRegion.imageSubresource.baseArrayLayer = region.baseArrayLayer;
		copyRegion.imageSubresource.layerCount = region.layerCount;

		VkBuffer vkBuffer = ((VulkanBuffer*)pBuffer)->GetBuffer();
		VkImage vkImage = ((VulkanTexture*)pTexture)->GetImage();
		VkImageLayout imageLayout = Helpers::GetImageLayout(pTexture->GetLayout());

		m_Vk.CmdCopyBufferToImage(m_CommandBuffer, vkBuffer, vkImage, imageLayout, 1, &copyRegion);
	}

	void VulkanCommandList::CopyBufferToTexture(Renderer::Core::Buffer* pBuffer, Renderer::Core::Texture* pTexture,
		const DynArray<Renderer::TextureBufferCopyRegion>& regions)
	{
		CHECK_RECORDING;
		UpdateBarriers();

		DynArray<VkBufferImageCopy> copyRegions;
		copyRegions.Reserve(regions.Size());

		for (const Renderer::TextureBufferCopyRegion& region : regions)
		{
			VkBufferImageCopy copyRegion = {};
			copyRegion.bufferOffset = region.bufferOffset;
			copyRegion.bufferRowLength = region.bufferRowLength;
			copyRegion.bufferImageHeight = region.bufferImageHeight;
			copyRegion.imageExtent.width = region.texExtent.x;
			copyRegion.imageExtent.height = region.texExtent.y;
			copyRegion.imageExtent.depth = region.texExtent.z;
			copyRegion.imageOffset.x = i32(region.texOffset.x);
			copyRegion.imageOffset.y = i32(region.texOffset.y);
			copyRegion.imageOffset.z = i32(region.texOffset.z);

			PixelFormat format = pTexture->GetFormat();
			VkImageAspectFlags aspect = format.HasDepthComponent() ? VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;
			if (format.HasStencilComponent())
				aspect |= VK_IMAGE_ASPECT_STENCIL_BIT;

			copyRegion.imageSubresource.aspectMask = aspect;
			copyRegion.imageSubresource.mipLevel = region.mipLevel;
			copyRegion.imageSubresource.baseArrayLayer = region.baseArrayLayer;
			copyRegion.imageSubresource.layerCount = region.layerCount;

			copyRegions.Push(copyRegion);
		}

		VkBuffer vkBuffer = ((VulkanBuffer*)pBuffer)->GetBuffer();
		VkImage vkImage = ((VulkanTexture*)pTexture)->GetImage();
		VkImageLayout imageLayout = Helpers::GetImageLayout(pTexture->GetLayout());

		m_Vk.CmdCopyBufferToImage(m_CommandBuffer, vkBuffer, vkImage, imageLayout, u32(copyRegions.Size()), copyRegions.Data());
	}

	void VulkanCommandList::CopyTextureToBuffer(Renderer::Core::Texture* pTexture, Renderer::Core::Buffer* pBuffer,
		const Renderer::TextureBufferCopyRegion& region)
	{
		CHECK_RECORDING;
		UpdateBarriers();

		VkBufferImageCopy copyRegion = {};
		copyRegion.bufferOffset = region.bufferOffset;
		copyRegion.bufferRowLength = region.bufferRowLength;
		copyRegion.bufferImageHeight = region.bufferImageHeight;
		copyRegion.imageExtent.width = region.texExtent.x;
		copyRegion.imageExtent.height = region.texExtent.y;
		copyRegion.imageExtent.depth = region.texExtent.z;
		copyRegion.imageOffset.x = i32(region.texOffset.x);
		copyRegion.imageOffset.y = i32(region.texOffset.y);
		copyRegion.imageOffset.z = i32(region.texOffset.z);

		PixelFormat format = pTexture->GetFormat();
		VkImageAspectFlags aspect = format.HasDepthComponent() ? VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;
		if (format.HasStencilComponent())
			aspect |= VK_IMAGE_ASPECT_STENCIL_BIT;

		copyRegion.imageSubresource.aspectMask = aspect;
		copyRegion.imageSubresource.mipLevel = region.mipLevel;
		copyRegion.imageSubresource.baseArrayLayer = region.baseArrayLayer;
		copyRegion.imageSubresource.layerCount = region.layerCount;

		VkBuffer vkBuffer = ((VulkanBuffer*)pBuffer)->GetBuffer();
		VkImage vkImage = ((VulkanTexture*)pTexture)->GetImage();
		VkImageLayout imageLayout = Helpers::GetImageLayout(pTexture->GetLayout());

		m_Vk.CmdCopyImageToBuffer(m_CommandBuffer, vkImage, imageLayout, vkBuffer, 1, &copyRegion);
	}

	void VulkanCommandList::CopyTextureToBuffer(Renderer::Core::Texture* pTexture, Renderer::Core::Buffer* pBuffer,
		const DynArray<Renderer::TextureBufferCopyRegion>& regions)
	{
		CHECK_RECORDING;
		UpdateBarriers();

		DynArray<VkBufferImageCopy> copyRegions;
		copyRegions.Reserve(regions.Size());

		for (const Renderer::TextureBufferCopyRegion& region : regions)
		{
			VkBufferImageCopy copyRegion = {};
			copyRegion.bufferOffset = region.bufferOffset;
			copyRegion.bufferRowLength = region.bufferRowLength;
			copyRegion.bufferImageHeight = region.bufferImageHeight;
			copyRegion.imageExtent.width = region.texExtent.x;
			copyRegion.imageExtent.height = region.texExtent.y;
			copyRegion.imageExtent.depth = region.texExtent.z;
			copyRegion.imageOffset.x = i32(region.texOffset.x);
			copyRegion.imageOffset.y = i32(region.texOffset.y);
			copyRegion.imageOffset.z = i32(region.texOffset.z);

			PixelFormat format = pTexture->GetFormat();
			VkImageAspectFlags aspect = format.HasDepthComponent() ? VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;
			if (format.HasStencilComponent())
				aspect |= VK_IMAGE_ASPECT_STENCIL_BIT;

			copyRegion.imageSubresource.aspectMask = aspect;
			copyRegion.imageSubresource.mipLevel = region.mipLevel;
			copyRegion.imageSubresource.baseArrayLayer = region.baseArrayLayer;
			copyRegion.imageSubresource.layerCount = region.layerCount;

			copyRegions.Push(copyRegion);
		}

		VkBuffer vkBuffer = ((VulkanBuffer*)pBuffer)->GetBuffer();
		VkImage vkImage = ((VulkanTexture*)pTexture)->GetImage();
		VkImageLayout imageLayout = Helpers::GetImageLayout(pTexture->GetLayout());

		m_Vk.CmdCopyImageToBuffer(m_CommandBuffer, vkImage, imageLayout, vkBuffer, u32(copyRegions.Size()), copyRegions.Data());
	}

	void VulkanCommandList::TransitionTextureLayout(Renderer::PipelineStage srcStage, Renderer::PipelineStage dstStage, Renderer::Core::Texture* pTexture,
		const Renderer::TextureLayoutTransition& transition)
	{
		CHECK_RECORDING;
		if (pTexture->GetLayout() == transition.layout)
			return;

		if (srcStage != m_SrcStage || dstStage != m_DstStage)
		{
			UpdateBarriers();
		}

		m_SrcStage = srcStage;
		m_DstStage = dstStage;

		VkImageMemoryBarrier imageBarrier = {};
		imageBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		imageBarrier.srcAccessMask = Helpers::GetImageTransitionAccessMode(pTexture->GetLayout());
		imageBarrier.dstAccessMask = Helpers::GetImageTransitionAccessMode(transition.layout);
		imageBarrier.oldLayout = Helpers::GetImageLayout(pTexture->GetLayout());
		imageBarrier.newLayout = Helpers::GetImageLayout(transition.layout);
		imageBarrier.image = ((VulkanTexture*)pTexture)->GetImage();

		VkImageAspectFlags aspect = pTexture->GetFormat().HasDepthComponent() ? VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;
		if (pTexture->GetFormat().HasStencilComponent())
			aspect |= VK_IMAGE_ASPECT_STENCIL_BIT;
		imageBarrier.subresourceRange.aspectMask = aspect;
		imageBarrier.subresourceRange.baseArrayLayer = transition.baseArrayLayer;
		imageBarrier.subresourceRange.layerCount = transition.layerCount;
		imageBarrier.subresourceRange.baseMipLevel = transition.baseMipLevel;
		imageBarrier.subresourceRange.levelCount = transition.mipLevelCount;

		u32 owningQueueFamily = ((VulkanTexture*)pTexture)->GetOwningQueueFamily();
		u32 queueFamily = ((VulkanQueue*)m_pQueue)->GetQueueFamily();
		imageBarrier.srcQueueFamilyIndex = owningQueueFamily != u32(-1) ? owningQueueFamily : queueFamily;
		imageBarrier.dstQueueFamilyIndex = queueFamily;

		m_ImageBarriers.Push(imageBarrier);

		pTexture->SetLayout(transition.layout);
		((VulkanTexture*)pTexture)->SetOwningQueueFamily(queueFamily);
	}

	b8 VulkanCommandList::Submit()
	{
		VulkanQueue* pVulkanQueue = (VulkanQueue*)m_pQueue;
		VkQueue queue = pVulkanQueue->GetQueue();
		VkFence fence = ((VulkanFence*)m_pFence)->GetFence();

#if 1
		// Checks before submitting

		// Check data for pipeline
		if (m_pPipeline)
		{
			if (m_pPipeline->GetType() == Renderer::PipelineType::Graphics)
			{
				const Renderer::Core::GraphicsPipelineDesc& desc = m_pPipeline->GetGraphicsDesc();
				const DynArray<u16> inputSlots = desc.inputDescriptor.GetInputSlots();
				for (u16 slot : inputSlots)
				{
					if (m_BoundInputSlots.Find(slot) == m_BoundInputSlots.Back())
					{
						g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Error, "No vertex buffer bound to input slot %u!", slot);
						return false;
					}
				}
			}
		}
#endif

		VkSubmitInfo info = {};
		info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		info.commandBufferCount = 1;
		info.pCommandBuffers = &m_CommandBuffer;

		VkResult vkres = pVulkanQueue->vkSubmit(info, fence);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to submit the vulkan command buffer (VkResult: %s)!", Helpers::GetResultString(vkres));
			return false;
		}

		m_Status = Renderer::CommandListState::Submited;
		m_pFence->SetSubmitted();

		return true;
	}

	b8 VulkanCommandList::Submit(const DynArray<Renderer::Core::Semaphore*>& waitSemaphores, 
		const DynArray<Renderer::PipelineStage>& waitStages,
		const DynArray<Renderer::Core::Semaphore*>& signalSemaphores)
	{
		VulkanQueue* pVulkanQueue = (VulkanQueue*)m_pQueue;
		VkQueue queue = pVulkanQueue->GetQueue();
		VkFence fence = ((VulkanFence*)m_pFence)->GetFence();

		VkSubmitInfo info = {};
		info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		info.commandBufferCount = 1;
		info.pCommandBuffers = &m_CommandBuffer;

		DynArray<VkSemaphore> vulkanWaitSemaphores;
		DynArray<VkPipelineStageFlags> vulkanWaitStages;
		vulkanWaitSemaphores.Reserve(waitSemaphores.Size());
		vulkanWaitStages.Reserve(waitSemaphores.Size());
		for (u32 i = 0; i < waitSemaphores.Size(); ++i)
		{
			vulkanWaitSemaphores.Push(((VulkanSemaphore*)waitSemaphores[i])->GetSemaphore());
			vulkanWaitStages.Push(Helpers::GetPipelineStage(waitStages[i]));
		}
		info.waitSemaphoreCount = u32(vulkanWaitSemaphores.Size());
		info.pWaitSemaphores = vulkanWaitSemaphores.Data();
		info.pWaitDstStageMask = vulkanWaitStages.Data();

		DynArray<VkSemaphore> vulkanSignalSemaphores;
		vulkanSignalSemaphores.Reserve(waitSemaphores.Size());
		for (Renderer::Core::Semaphore* pSemaphore : signalSemaphores)
		{
			vulkanSignalSemaphores.Push(((VulkanSemaphore*)pSemaphore)->GetSemaphore());
		}
		info.signalSemaphoreCount = u32(vulkanSignalSemaphores.Size());
		info.pSignalSemaphores = vulkanSignalSemaphores.Data();

		VkResult vkres = pVulkanQueue->vkSubmit(info, fence);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to submit the vulkan command buffer (VkResult: %s)!", Helpers::GetResultString(vkres));
			return false;
		}
		m_Status = Renderer::CommandListState::Submited;
		m_pFence->SetSubmitted();

		return true;
	}

	b8 VulkanCommandList::Wait(u64 timeout)
	{
		if (m_Status == Renderer::CommandListState::Submited && m_pFence->GetStatus() == Renderer::FenceStatus::Submitted)
		{
			b8 res = m_pFence->Wait(timeout);
			if (!res)
				return false;

			m_Status = Renderer::CommandListState::Finished;
		}
		else if (m_pFence->GetStatus() == Renderer::FenceStatus::Signaled)
		{
			m_Status = Renderer::CommandListState::Finished;
		}
		return true;
	}

	b8 VulkanCommandList::Create(RHI::RHIContext* pContext, Renderer::Core::CommandListManager* pManager, Renderer::Core::Queue* pQueue)
	{
		m_pContext = pContext;
		m_pManager = pManager;
		m_pQueue = pQueue;

		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
		m_Vk = pDevice->GetCommandBufferFunctions();

		// TODO: Secondary buffer?
		VkCommandBufferAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.commandBufferCount = 1;
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocInfo.commandPool = ((VulkanCommandListManager*)m_pManager)->GetCommandPool(m_pQueue);

		VkResult vkres = pDevice->VkAllocateCommandBuffer(allocInfo, &m_CommandBuffer);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Error, "Failed to create a vulkan command buffer (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}

		// Create fence
		m_pFence = HvNew VulkanFence();
		b8 res = m_pFence->Create(m_pContext, false);
		if (!res)
		{
			g_Logger.LogError(LogVulkanRHI(), "Failed to create command list fence");
			Destroy();
			return false;
		}

		return true;
	}

	b8 VulkanCommandList::Destroy()
	{
		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();

		if (m_pFence)
		{
			m_pFence->Destroy();
			HvDelete m_pFence;
			m_pFence = nullptr;
		}

		if (m_CommandBuffer)
		{
			VkCommandPool commandPool = ((VulkanCommandListManager*)m_pManager)->GetCommandPool(m_pQueue);
			pDevice->vkFreeCommandBuffers(commandPool, m_CommandBuffer);
			m_CommandBuffer = VK_NULL_HANDLE;
		}
		m_Vk = {};

		return true;
	}

	void VulkanCommandList::UpdateBarriers()
	{
		if (m_GlobalBarriers.Size() > 0 || m_BufferBarriers.Size() > 0 || m_ImageBarriers.Size() > 0)
		{
			VkPipelineStageFlags srcStage = Helpers::GetPipelineStage(m_SrcStage);
			VkPipelineStageFlags dstStage = Helpers::GetPipelineStage(m_DstStage);
			m_Vk.CmdPipelineBarrier(m_CommandBuffer,
				srcStage, dstStage,
				0, // TODO
				u32(m_GlobalBarriers.Size()), m_GlobalBarriers.Data(),
				u32(m_BufferBarriers.Size()), m_BufferBarriers.Data(),
				u32(m_ImageBarriers.Size()), m_ImageBarriers.Data());

			m_GlobalBarriers.Clear();
			m_BufferBarriers.Clear();
			m_ImageBarriers.Clear();

			m_SrcStage = Renderer::PipelineStage::None;
			m_DstStage = Renderer::PipelineStage::None;
		}
	}
}

#undef CHECK_RECORDING
