// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanContext.cpp: Vulkan RHI Context
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanContext.h"
#include "VulkanRHI/VulkanInstance.h"
#include "VulkanRHI/VulkanPhysicalDevice.h"
#include "VulkanRHI/VulkanHelpers.h"
#include "VulkanRHI/VulkanDynamicRHI.h"
#include "VulkanRHI/VulkanBuffer.h"
#include "VulkanRHI/VulkanDevice.h"
#include "VulkanRHI/VulkanSwapChain.h"
#include "VulkanRHI/VulkanDescriptorSetManager.h"

namespace Hv::VulkanRHI {

	// Vulkan allocation callbacks
	void* VulkanAlloc(void* pUserData, sizeT size, sizeT alignment, VkSystemAllocationScope scope)
	{
		return g_Alloctor.Allocate(size, u16(alignment), HV_ALLOC_CONTEXT(Memory::AllocCategory::RHI));
	}

	void* VulkanRealloc(void* pUserData, void* pOriginal, sizeT size, sizeT alignment, VkSystemAllocationScope scope)
	{
		return g_Alloctor.Reallocate(pOriginal, size, u16(alignment), HV_ALLOC_CONTEXT(Memory::AllocCategory::RHI));
	}

	void VulkanFree(void* pUserData, void* pMemory)
	{
		g_Alloctor.Free(pMemory);
	}

	void VulkanInterAlloc(void* pUserdata, sizeT size, VkInternalAllocationType type, VkSystemAllocationScope scope)
	{
		// don't do anything for now, this is just a notification
	}

	void VulkanInternFree(void* pUserdata, sizeT size, VkInternalAllocationType type, VkSystemAllocationScope scope)
	{
		// don't do anything for now, this is just a notification
	}

	VkBool32 VulkanDebugReportCallback(
		VkDebugReportFlagsEXT                       flags,
		VkDebugReportObjectTypeEXT                  objectType,
		uint64_t                                    object,
		size_t                                      location,
		int32_t                                     messageCode,
		const char*                                 pLayerPrefix,
		const char*                                 pMessage,
		void*                                       pUserData)
	{
		LogLevel level = Helpers::GetDebugReportLogLevel(flags);
		const AnsiChar* type = Helpers::GetObjectTypeString(objectType);

		g_Logger.LogFormat(LogVulkanRHI(), level, "Debug report: [%s] Object %x %s at location %u, code %i: %s", pLayerPrefix, object, type, location, messageCode, pMessage);

		return VK_FALSE;
	}

	VulkanContext::VulkanContext()
		: RHIContext()
		, m_VulkanHandle(DynLib::InvalidDynLibHandle)
		, m_vkGetInstanceProcAddr(nullptr)
		, m_pInstance(nullptr)
		, m_pDevice(nullptr)
		, m_pAllocator(nullptr)
	{
		m_AllocationCallbacks.pUserData = nullptr;
		m_AllocationCallbacks.pfnAllocation = &VulkanAlloc;
		m_AllocationCallbacks.pfnReallocation = &VulkanRealloc;
		m_AllocationCallbacks.pfnFree = &VulkanFree;
		m_AllocationCallbacks.pfnInternalAllocation = &VulkanInterAlloc;
		m_AllocationCallbacks.pfnInternalFree = &VulkanInternFree;
	}

	VulkanContext::~VulkanContext()
	{
	}

	b8 VulkanContext::Init(const RHI::RHIDesc& desc)
	{
		// Load the vulkan library
		
#if HV_PLATFORM_WINDOWS
		m_VulkanHandle = DynLib::Load("vulkan-1.dll");
#else
		m_VulkanHandle = DynLib::Load("vulkan.so");
		if (m_VulkanHandle == DynLib::InvalidDynLibHandle)
		{
			m_VulkanHandle = DynLib::Load("vulkan.so.1");
		}
#endif
		if (m_VulkanHandle == DynLib::InvalidDynLibHandle)
		{
			g_Logger.LogFatal(LogVulkanRHI(), "Failed to load the vulkan library!");
			return false;
		}
		
		// Load vkGetInstanceProcAddr
		m_vkGetInstanceProcAddr = (PFN_vkGetInstanceProcAddr)DynLib::GetProc(m_VulkanHandle, "vkGetInstanceProcAddr");
		if (!m_vkGetInstanceProcAddr)
		{
			g_Logger.LogFatal(LogVulkanRHI(), "Failed to get the vkGetInstanceProcAddr function!");
			Destroy();
			return false;
		}

		// Create Instance
		m_pInstance = HvNew VulkanInstance();

		// TODO: figure out how to request extensions and layers
		DynArray<const AnsiChar*> requestedInstanceExtensions;
		DynArray<const AnsiChar*> requestedInstanceLayers;

		// Add surface extensions
		requestedInstanceExtensions.Push(VK_KHR_SURFACE_EXTENSION_NAME);
#if defined(VK_USE_PLATFORM_WIN32_KHR)
		requestedInstanceExtensions.Push(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);
#else
#endif

		if (desc.validationLevel != Renderer::RHIValidationLevel::None)
		{
			requestedInstanceExtensions.Push(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
			requestedInstanceLayers.Push("VK_LAYER_LUNARG_standard_validation");
		}

		VkResult vkres = m_pInstance->Init(this, requestedInstanceExtensions, requestedInstanceLayers);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFatal(LogVulkanRHI(), "Failed to initialize vulkan instance!");
			Destroy();
			return false;
		}

		if (!m_pInstance->IsExtensionEnabled(VK_KHR_SURFACE_EXTENSION_NAME))
		{
			g_Logger.LogFatal(LogVulkanRHI(), "Vulkan surface KHR extension not enabled!");
			Destroy();
			return false;
		}

		if (desc.validationLevel != Renderer::RHIValidationLevel::None)
		{
			vkres = m_pInstance->CreateDebugReportCallback(nullptr, &VulkanDebugReportCallback, desc.validationLevel);
		}

		// Log instance info
		m_pInstance->LogAvailableExtensions();
		m_pInstance->LogAvailableLayers();

		// Retrieve physical devices
		vkres = m_pInstance->EnumeratePhysicalDevices(m_PhysicalDevices);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFatal(LogVulkanRHI(), "Failed to retrieve vulkan physical devices!");
			Destroy();
			return false;
		}

		// Create temporary renderview to query present support
		VulkanSwapChain* pTmpRenderView = HvNew VulkanSwapChain();
		pTmpRenderView->Init(this, g_WindowManager.GetMainWindow(), Renderer::VSyncMode::Off, nullptr);
		// And destroy the temporary renderview
		pTmpRenderView->Destroy();
		HvDelete pTmpRenderView;

		// Log physical device info
		g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Detail, "%u physical %s found", m_PhysicalDevices.Size(), m_PhysicalDevices.Size() > 1 ? "devices" : "device");
		for (u32 i = 0; i < m_PhysicalDevices.Size(); ++i)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Detail, "GPU %u:", i);
			m_PhysicalDevices[i]->LogInfo();
		}

		// TODO: figure out how to request extensions, layers and features
		DynArray<const AnsiChar*> requestedDeviceExtensions;
		DynArray<const AnsiChar*> requestedDeviceLayers;

		requestedDeviceExtensions.Push(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
		
		// Setup device features
		// TODO: figure out device features
		VkPhysicalDeviceFeatures features = {};

		// TODO: figure out if we need present support
		b8 needsPresentSupport = true;

		VulkanPhysicalDevice* pPhysicalDevice = GetMostSuitablePhysicalDevice(requestedDeviceExtensions, requestedDeviceLayers, features, needsPresentSupport);
		if (!pPhysicalDevice)
		{
			g_Logger.LogFatal(LogVulkanRHI(), "Failed to find suitable device!");
			Destroy();
			return false;
		}

		// Setup queue info
		DynArray<VkDeviceQueueCreateInfo> deviceQueues;
		u32 queueCount = 0;
		for (const Renderer::QueueInfo& info : desc.queueInfo)
		{
			queueCount += info.count;
		}
		DynArray<f32> queuePriorities;
		queuePriorities.Reserve(queueCount);
		u32 queueOffset = 0;
		for (const Renderer::QueueInfo& info : desc.queueInfo)
		{
			VkDeviceQueueCreateInfo queueInfo = {};
			queueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queueInfo.queueCount = info.count;
			queueInfo.queueFamilyIndex = pPhysicalDevice->GetQueueFamily(info.type);

			f32 priority = info.priority == Renderer::QueuePriority::High ? 1.f : 0.5f;
			for (u32 i = 0; i < info.count; ++i)
			{
				queuePriorities.Push(priority);
			}
			queueInfo.pQueuePriorities = queuePriorities.Data() + queueOffset;
			queueOffset = u32(queuePriorities.Size());
			deviceQueues.Push(queueInfo);
		}

		// Use all available device features for now
		features = pPhysicalDevice->GetFeatures();
		Helpers::DisableUnsupportedDeviceFeatures(pPhysicalDevice->GetFeatures(), features);

		// Create logical device
		m_pDevice = HvNew VulkanDevice();
		vkres = m_pDevice->Init(this, pPhysicalDevice, requestedDeviceExtensions, requestedDeviceLayers, features, deviceQueues);
		if (vkres != VK_SUCCESS)
		{
			Destroy();
			return false;
		}

		// Retrieve queues
		m_Queues = m_pDevice->CreateQueues(desc.queueInfo);

		// Create memory allocator
		m_pAllocator = HvNew VulkanAllocator();
		b8 res = m_pAllocator->Create(this);
		if (!res)
		{
			Destroy();
			return false;
		}

		// Create command list manager
		m_pCommandListManager = HvNew VulkanCommandListManager();
		res = m_pCommandListManager->Create(this);
		if (!res)
		{
			Destroy();
			return false;
		}

		// Create descriptor set manager
		m_pDescriptorSetManager = HvNew VulkanDescriptorSetManager();
		res = m_pDescriptorSetManager->Create(this);
		if (!res)
		{
			Destroy();
			return false;
		}

		return true;
	}

	b8 VulkanContext::Destroy()
	{
		if (m_pDescriptorSetManager)
		{
			m_pDescriptorSetManager->Destroy();
			HvDelete m_pDescriptorSetManager;
			m_pDescriptorSetManager = nullptr;
		}

		if (m_pCommandListManager)
		{
			m_pCommandListManager->Destroy();
			HvDelete m_pCommandListManager;
			m_pCommandListManager = nullptr;
		}

		if (m_pAllocator)
		{
			m_pAllocator->Destroy();
			HvDelete m_pAllocator;
			m_pAllocator = nullptr;
		}

		if (m_Queues.Size() > 0)
		{
			for (Renderer::Core::Queue* queue : m_Queues)
			{
				HvDelete queue;
			}
		}
		m_Queues.Clear(true);

		if (m_pDevice)
		{
			m_pDevice->Destroy();
			HvDelete m_pDevice;
			m_pDevice = nullptr;
		}

		if (m_PhysicalDevices.Size() > 0)
		{
			for (VulkanPhysicalDevice* pDev : m_PhysicalDevices)
			{
				HvDelete pDev;
			}
			m_PhysicalDevices.Clear(true);
		}

		if (m_pInstance)
		{
			m_pInstance->Destroy();
			HvDelete m_pInstance;
			m_pInstance = nullptr;
		}

		m_vkGetInstanceProcAddr = nullptr;
		if (m_VulkanHandle != DynLib::InvalidDynLibHandle)
		{
			DynLib::Unload(m_VulkanHandle);
			m_VulkanHandle = DynLib::InvalidDynLibHandle;
		}

		return true;
	}

	VkResult VulkanContext::UpdateSurfaceSupport(VkSurfaceKHR surface)
	{
		for (VulkanPhysicalDevice* pDevice : m_PhysicalDevices)
		{
			VkResult vkres = pDevice->UpdateSurfaceSupport(surface);
			if (vkres != VK_SUCCESS)
			{
				return vkres;
			}
		}
		return VK_SUCCESS;
	}

	VulkanPhysicalDevice* VulkanContext::GetMostSuitablePhysicalDevice(const DynArray<const AnsiChar*>& requestedExtensions,
		const DynArray<const AnsiChar*>& requestedLayers, const VkPhysicalDeviceFeatures& features, b8 needsPresentSupport)
	{
		// Select the 'best fitting' physical device
		// TODO improve physical device selection
		u32 deviceIndex = u32(-1);
		Renderer::GpuType devType = Renderer::GpuType::Unknown;
		for (u32 i = 0; i < m_PhysicalDevices.Size(); ++i)
		{
			VulkanPhysicalDevice* pPhysicalDevice = m_PhysicalDevices[i];

			// Check for extension and layer support
			b8 extensionAndLayerSupport = pPhysicalDevice->AreExtensionsSupported(requestedExtensions);
			if (!extensionAndLayerSupport)
				continue;

			extensionAndLayerSupport = pPhysicalDevice->AreLayersSupported(requestedLayers);
			if (!extensionAndLayerSupport)
				continue;

			// Check for best GPU type
			const Renderer::GpuInfo& info = pPhysicalDevice->GetGpuInfo();
			if (deviceIndex == u32(-1) || info.type != devType)
			{
				if (info.type == Renderer::GpuType::Discrete)
				{
					deviceIndex = i;
					devType = Renderer::GpuType::Discrete;
				}
				else if (devType != Renderer::GpuType::Discrete)
				{
					if (devType == Renderer::GpuType::Unknown)
					{
						deviceIndex = i;
						devType = info.type;
					}
					else if (info.type == Renderer::GpuType::Integrated)
					{
						deviceIndex = i;
						devType = Renderer::GpuType::Integrated;
					}
					// Don't care about CPU and software renderers for now
				}
			}
		}

		if (deviceIndex == u32(-1))
			return nullptr;

		return m_PhysicalDevices[deviceIndex];
	}
}
