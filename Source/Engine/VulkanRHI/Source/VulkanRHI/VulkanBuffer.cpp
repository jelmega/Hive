// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanBuffer.cpp: Vulkan buffer
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanBuffer.h"
#include "VulkanRHI/VulkanHelpers.h"
#include "VulkanRHI/VulkanContext.h"
#include "VulkanRHI/VulkanDevice.h"
#include "VulkanRHI/VulkanDynamicRHI.h"

namespace Hv::VulkanRHI {

	VulkanBuffer::VulkanBuffer()
		: Buffer()
		, m_Buffer(VK_NULL_HANDLE)
		, m_pAllocation(nullptr)
		, m_pStagingBuffer(nullptr)
		, m_View(VK_NULL_HANDLE)
	{
	}

	VulkanBuffer::~VulkanBuffer()
	{
	}

	b8 VulkanBuffer::Create(RHI::RHIContext* pContext, Renderer::BufferType type, u64 size,
		Renderer::BufferFlags flags, PixelFormat format)
	{
		m_pContext = pContext;
		m_Type = type;
		m_Size = size;
		m_Flags = flags;

		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();

		VkBufferCreateInfo bufferInfo = {};
		bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufferInfo.size = m_Size;
		bufferInfo.usage = Helpers::GetBufferUsage(m_Type, m_Flags);
		bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE; // TODO: Concurent?

		VkResult vkres = pDevice->vkCreateBuffer(bufferInfo, m_Buffer);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to create a vulkan buffer (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}

		VkMemoryRequirements memReqs = {};
		pDevice->vkGetBufferMemoryRequirements(m_Buffer, memReqs);
		m_MemorySize = memReqs.size;

		VkMemoryPropertyFlags memProps = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
		if (m_Flags & Renderer::BufferFlags::Dynamic || m_Type == Renderer::BufferType::Staging)
			memProps = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;

		VulkanAllocator* pAllocator = ((VulkanContext*)m_pContext)->GetAllocator();
		m_pAllocation = pAllocator->Allocate(memReqs, memProps);

		vkres = pDevice->vkBindBufferMemory(m_Buffer, m_pAllocation->GetMemory(), m_pAllocation->GetOffset());
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to bind memory to a vulkan buffer (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}

		if (m_Type == Renderer::BufferType::StorageTexel || m_Type == Renderer::BufferType::UniformTexel)
		{
			m_TexelBufferFormat = format;
			// TODO Buffer view as seperate class (maybe texture view too)???
			VkBufferViewCreateInfo viewInfo = {};
			viewInfo.sType = VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO;
			viewInfo.buffer = m_Buffer;
			viewInfo.offset = 0;
			viewInfo.range = m_Size;
			viewInfo.format = Helpers::GetFormat(format);

			vkres = pDevice->vkCreateBufferView(viewInfo, m_View);
			if (vkres != VK_SUCCESS)
			{
				g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to bind memory to a vulkan buffer view (VkResult: %s)!", Helpers::GetResultString(vkres));
				return vkres;
			}
		}

		if (!(m_Flags & (Renderer::BufferFlags::Dynamic | Renderer::BufferFlags::Static)) && m_Type != Renderer::BufferType::Staging)
		{
			m_pStagingBuffer = HvNew VulkanBuffer();
			b8 res = m_pStagingBuffer->Create(m_pContext, Renderer::BufferType::Staging, m_Size, Renderer::BufferFlags::None);
			if (!res)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to create staging buffer for buffer");
				return false;
			}
		}

		return true;
	}

	b8 VulkanBuffer::CreateVertexBuffer(RHI::RHIContext* pContext, u32 vertexCount, u16 vertexSize,
		Renderer::BufferFlags flags)
	{
		m_VertexCount = vertexCount;
		m_VertexSize = vertexSize;
		return Create(pContext, Renderer::BufferType::Vertex, m_VertexCount * m_VertexSize, flags);
	}

	b8 VulkanBuffer::CreateIndexBuffer(RHI::RHIContext* pContext, u32 indexCount, Renderer::IndexType indexType,
		Renderer::BufferFlags flags)
	{
		m_IndexCount = indexCount;
		m_IndexType = indexType;
		u8 indexSize = m_IndexType == Renderer::IndexType::UShort ? sizeof(u16) : sizeof(u32);
		return Create(pContext, Renderer::BufferType::Index, m_IndexCount * indexSize, flags);
	}

	b8 VulkanBuffer::Destroy()
	{
		if (m_pStagingBuffer)
		{
			m_pStagingBuffer->Destroy();
			HvDelete m_pStagingBuffer;
			m_pStagingBuffer = nullptr;
		}

		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
		if (m_View)
		{
			pDevice->vkDestroyBufferView(m_View);
			m_View = VK_NULL_HANDLE;
		}

		if (m_pAllocation)
		{
			VulkanAllocator* pAllocator = ((VulkanContext*)m_pContext)->GetAllocator();
			pAllocator->Free(m_pAllocation);
			m_pAllocation = nullptr;
		}

		if (m_Buffer)
		{
			pDevice->vkDestroyBuffer(m_Buffer);
			m_Buffer = VK_NULL_HANDLE;
		}

		return true;
	}

	b8 VulkanBuffer::Write(u64 offset, u64 size, void* pData, Renderer::BufferWriteFlags flags,
		Renderer::Core::CommandList* pCommandList)
	{
		if (m_Flags & Renderer::BufferFlags::NoWrite)
		{
			g_Logger.LogError(LogVulkanRHI(), "Can't write to a buffer with the 'NoWrite' flag set!");
			return false;
		}

		if (m_Flags & Renderer::BufferFlags::Dynamic || m_Type == Renderer::BufferType::Staging)
		{
			if (flags == Renderer::BufferWriteFlags::Discard)
			{
				void* pMappedData = m_pAllocation->Map(0, m_pAllocation->GetSize(), VulkanAllocationMapMode::Write);
				if (!pMappedData)
				{
					g_Logger.LogError(LogVulkanRHI(), "Failed to map the vulkan buffer memory!");
					return false;
				}
				Memory::Clear(pMappedData, m_pAllocation->GetSize());
				Memory::Copy((u8*)pMappedData + offset, pData, size);
				m_pAllocation->Unmap();
			}
			else
			{
				void* pMappedData = m_pAllocation->Map(offset, size, VulkanAllocationMapMode::Write);
				if (!pMappedData)
				{
					g_Logger.LogError(LogVulkanRHI(), "Failed to map the vulkan buffer memory!");
					return false;
				}
				Memory::Copy(pMappedData, pData, size);
				m_pAllocation->Unmap();
			}
		}
		else if (m_Flags & Renderer::BufferFlags::Static)
		{
			VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();

			// Create staging buffer
			VulkanBuffer* pStagingBuffer = HvNew VulkanBuffer();
			b8 res = pStagingBuffer->Create(m_pContext, Renderer::BufferType::Staging, size, Renderer::BufferFlags::None);
			if (!res)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to create temporary staging buffer for static buffer!");
				HvDelete pStagingBuffer;
				return false;
			}
			res = pStagingBuffer->Write(0, size, pData);
			if (!res)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to write to temporary staging buffer for static buffer!");
				pStagingBuffer->Destroy();
				HvDelete pStagingBuffer;
				return false;
			}

			// Record copy and execute
			res = Copy(pStagingBuffer, 0, offset, size);
			if (!res)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to copy temporary staging buffer to static buffer!");
			}

			pStagingBuffer->Destroy();
			HvDelete pStagingBuffer;
			return res;
		}
		else
		{
			b8 res = m_pStagingBuffer->Write(0, size, pData, flags);
			if (!res)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to write to staging buffer!");
				return false;
			}
			res = Copy(m_pStagingBuffer, 0, offset, size, pCommandList);
			if (!res)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to copy staging buffer to buffer!");
				return false;
			}
		}

		return true;
	}

	b8 VulkanBuffer::Read(u64 offset, u64 size, void* pData, Renderer::Core::CommandList* pCommandList)
	{
		if (m_Flags & Renderer::BufferFlags::NoRead)
		{
			g_Logger.LogError(LogVulkanRHI(), "Can't read from a buffer with the 'NoRead' flag set!");
			return false;
		}

		if (m_Flags & Renderer::BufferFlags::Dynamic || m_Type == Renderer::BufferType::Staging)
		{
			void* pMappedData = m_pAllocation->Map(offset, size, VulkanAllocationMapMode::Write);
			if (!pMappedData)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to map the vulkan buffer memory!");
				return false;
			}
			Memory::Copy(pData, pMappedData, size);
			m_pAllocation->Unmap();
		}
		else if (m_Flags & Renderer::BufferFlags::Static)
		{
			VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();

			// Create staging buffer
			VulkanBuffer* pStagingBuffer = HvNew VulkanBuffer();
			b8 res = pStagingBuffer->Create(m_pContext, Renderer::BufferType::Staging, size, Renderer::BufferFlags::None);
			if (!res)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to create temporary staging buffer for static buffer!");
				pStagingBuffer->Destroy();
				HvDelete pStagingBuffer;
				return false;
			}

			res = pStagingBuffer->Copy(this, offset, 0, size); //Copy(pStagingBuffer, 0, offset, size, nullptr);
			if (!res)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to copy temporary staging buffer to static buffer!");
				pStagingBuffer->Destroy();
				HvDelete pStagingBuffer;
				return false;
			}

			res = pStagingBuffer->Read(0, size, pData);
			if (!res)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to write to temporary staging buffer for static buffer!");
			}

			pStagingBuffer->Destroy();
			HvDelete pStagingBuffer;
			return res;
		}
		else
		{
			b8 res = Copy(m_pStagingBuffer, 0, offset, size, pCommandList);
			if (!res)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to copy staging buffer to buffer!");
				return false;
			}
			res = m_pStagingBuffer->Read(0, size, pData);
			if (!res)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to write to staging buffer!");
				return false;
			}
		}

		return true;
	}

	b8 VulkanBuffer::Copy(Buffer* pBuffer, u64 srcOffset, u64 dstOffset, u64 size,
		Renderer::Core::CommandList* pCommandList)
	{
		if (pCommandList)
		{
			pCommandList->CopyBuffer(pBuffer, srcOffset, this, dstOffset, size);
			return true;
		}

		Renderer::Core::CommandListManager* pCommandListManager = ((VulkanContext*)m_pContext)->GetCommandListManager();
		Renderer::Core::Queue* pCopyQueue = m_pContext->GetQueue(Renderer::QueueType::Transfer);
		Renderer::Core::CommandList* pCopyCommandList = pCommandListManager->CreateSingleTimeCommandList(pCopyQueue);

		// Record copy and execute and cleanup
		pCopyCommandList->CopyBuffer(pBuffer, srcOffset, this, dstOffset, size);
		b8 res = pCommandListManager->EndSingleTimeCommandList(pCopyCommandList);

		return res;
	}

	b8 VulkanBuffer::Copy(Buffer* pBuffer, const DynArray<Renderer::BufferCopyRegion>& regions,
		Renderer::Core::CommandList* pCommandList)
	{
		if (pCommandList)
		{
			pCommandList->CopyBuffer(pBuffer, this, regions);
			return true;
		}

		Renderer::Core::CommandListManager* pCommandListManager = ((VulkanContext*)m_pContext)->GetCommandListManager();
		Renderer::Core::Queue* pCopyQueue = m_pContext->GetQueue(Renderer::QueueType::Transfer);
		Renderer::Core::CommandList* pCopyCommandList = pCommandListManager->CreateCommandList(pCopyQueue);

		// Record copy and execute
		b8 res = pCopyCommandList->Begin();
		if (!res)
		{
			g_Logger.LogError(LogVulkanRHI(), "Failed to start temporary copy command buffer for static buffer!");
		}
		else
		{
			pCopyCommandList->CopyBuffer(pBuffer, this, regions);
			res = pCopyCommandList->End();
			if (!res)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to end temporary copy command buffer for static buffer!");
			}
			else
			{
				res = pCopyCommandList->Submit();
				if (!res)
				{
					g_Logger.LogError(LogVulkanRHI(), "Failed to submit temporary copy command buffer for static buffer!");
				}
				else
				{
					res = pCopyCommandList->Wait();
					if (!res)
					{
						g_Logger.LogError(LogVulkanRHI(), "Failed to wait for temporary copy command buffer for static buffer!");
					}
				}
			}
		}
		// Cleanup
		pCommandListManager->DestroyCommandList(pCopyCommandList);
		return res;
	}

	b8 VulkanBuffer::Copy(Renderer::Core::Texture* pTexture, const Renderer::TextureBufferCopyRegion& region,
		Renderer::Core::CommandList* pCommandList)
	{
		b8 isSingleTimeCommands = false;
		Renderer::Core::CommandListManager* pCommandListManager = ((VulkanContext*)m_pContext)->GetCommandListManager();

		if (pCommandList == nullptr)
		{
			Renderer::Core::Queue* pCopyQueue = m_pContext->GetQueue(Renderer::QueueType::Transfer);
			pCommandList = pCommandListManager->CreateCommandList(pCopyQueue);
			isSingleTimeCommands = true;
		}

		if (pTexture->GetLayout() != Renderer::TextureLayout::TransferSrc)
		{
			Renderer::TextureLayoutTransition transition = {};
			transition.layout = Renderer::TextureLayout::TransferDst;
			transition.baseMipLevel = 0;
			transition.mipLevelCount = pTexture->GetMipLevels();
			transition.baseArrayLayer = 0;
			transition.layerCount = pTexture->GetLayerCount();
			pCommandList->TransitionTextureLayout(Renderer::PipelineStage::Transfer, Renderer::PipelineStage::Transfer, pTexture, transition);
		}

		pCommandList->CopyTextureToBuffer(pTexture, this, region);

		if (isSingleTimeCommands)
		{
			pCommandListManager->EndSingleTimeCommandList(pCommandList);
		}

		return true;
	}

	b8 VulkanBuffer::Copy(Renderer::Core::Texture* pTexture, const DynArray<Renderer::TextureBufferCopyRegion>& regions,
		Renderer::Core::CommandList* pCommandList)
	{
		return false;
	}
}
