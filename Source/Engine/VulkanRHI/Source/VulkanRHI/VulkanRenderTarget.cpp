// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanRenderTarget.cpp: Vulkan render target
#pragma once
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanRenderTarget.h"
#include "VulkanRHI/VulkanTexture.h"
#include "VulkanRHI/VulkanContext.h"
#include "VulkanRHI/VulkanPhysicalDevice.h"
#include "VulkanRHI/VulkanDevice.h"
#include "VulkanRHI/VulkanBuffer.h"
#include "VulkanRHI/VulkanHelpers.h"

namespace Hv::VulkanRHI {

	VulkanRenderTarget::VulkanRenderTarget()
		: RenderTarget()
	{
	}

	VulkanRenderTarget::~VulkanRenderTarget()
	{
	}

	b8 VulkanRenderTarget::Create(RHI::RHIContext* pContext, const Renderer::Core::RenderTargetDesc& desc)
	{
		m_pContext = pContext;
		m_Type = desc.type;

		m_pTexture = HvNew VulkanTexture();

		Renderer::TextureFlags textureFlags = Renderer::TextureFlags::RenderTargetable;

		u32 width = desc.width;
		u32 height = desc.height;

		VulkanPhysicalDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice()->GetPhysicalDevice();
		if (width > pDevice->GetLimits().maxFramebufferWidth)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Warning, "Width to large (currenty: %u, max: %u). Width will be resized!", width, pDevice->GetLimits().maxFramebufferWidth);
			width = pDevice->GetLimits().maxFramebufferWidth;
		}
		if (height > pDevice->GetLimits().maxFramebufferHeight)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Warning, "Height to large (currenty: %u, max: %u). Height will be resized!", height, pDevice->GetLimits().maxFramebufferHeight);
			height = pDevice->GetLimits().maxFramebufferHeight;
		}

		Renderer::Core::TextureDesc texDesc = {};
		texDesc.width = width;
		texDesc.height = height;
		texDesc.format = desc.format;
		texDesc.type = Renderer::TextureType::Tex2D;
		texDesc.samples = desc.samples;
		texDesc.flags = textureFlags;
		texDesc.layout = Renderer::Helpers::GetTextureLayoutFromRTType(desc.type);

		Renderer::Core::Queue* pQueue = m_pContext->GetQueue(Renderer::QueueType::Graphics);
		Renderer::Core::CommandListManager* pCommandListManager = m_pContext->GetCommandListManager();
		Renderer::Core::CommandList* pCommandList = pCommandListManager->CreateSingleTimeCommandList(pQueue);

		b8 res = m_pTexture->Create(m_pContext, texDesc, pCommandList);
		if (!res)
		{
			g_Logger.LogError(LogVulkanRHI(), "Failed to create underlying texture for render target!");
			return false;
		}

		pCommandListManager->EndSingleTimeCommandList(pCommandList);

		return true;
	}

	b8 VulkanRenderTarget::Create(RHI::RHIContext* pContext, VkImage image, VkImageLayout layout, u32 width, u32 height, PixelFormat format, Renderer::SampleCount samples,
		Renderer::RenderTargetType type)
	{
		m_pContext = pContext;
		m_Type = type;

		m_pTexture = HvNew VulkanTexture();
		VulkanTexture* pTexture = (VulkanTexture*)m_pTexture;

		Renderer::TextureFlags textureFlags = Renderer::TextureFlags::RenderTargetable;

		b8 res = pTexture->Create(m_pContext, image, layout, width, height, format, samples, textureFlags);
		if (!res)
		{
			g_Logger.LogError(LogVulkanRHI(), "Failed to create underlying texture for render target!");
			return false;
		}

		return true;
	}

	b8 VulkanRenderTarget::Destroy()
	{
		b8 res = m_pTexture->Destroy();
		HvDelete m_pTexture;

		if (!res)
		{
			g_Logger.LogError(LogVulkanRHI(), "Failed to destroy underlying texture for render target!");
			return false;
		}

		return true;
	}
}
