// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanDevice.h: Vulkan logical device
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanDevice.h"
#include "VulkanRHI/VulkanContext.h"
#include "VulkanRHI/VulkanPhysicalDevice.h"
#include "VulkanRHI/VulkanHelpers.h"
#include "VulkanRHI/VulkanQueue.h"
#include "VulkanRHI/VulkanBuffer.h"

#define LOAD_VKD_FUNC(funcs, device, name) funcs.name = (PFN_vk##name)m_Vk.GetDeviceProcAddr(device, HV_STRINGIFY(vk##name))

namespace Hv::VulkanRHI {


	VulkanDevice::VulkanDevice()
		: m_pContext()
		, m_pPhysicalDevice(nullptr)
		, m_Vk()
		, m_VkExt()
		, m_VkQueueFuncs()
		, m_VkCommandBufferFuncs()
		, m_pAllocCallbacks(nullptr)
		, m_Device(VK_NULL_HANDLE)
	{
	}

	VulkanDevice::~VulkanDevice()
	{
	}

	VkResult VulkanDevice::Init(VulkanContext* pContext, VulkanPhysicalDevice* pPhysicalDevice, const DynArray<const AnsiChar*>& requestedExtensions, const DynArray<const AnsiChar*>& requestedLayers, const VkPhysicalDeviceFeatures& features, const DynArray<VkDeviceQueueCreateInfo>& deviceQueues)
	{
		m_pContext = pContext;
		m_pAllocCallbacks = m_pContext->GetAllocationCallbacks();
		m_pPhysicalDevice = pPhysicalDevice;
		m_EnabledExtensions = requestedExtensions;
		m_EnabledLayers = requestedLayers;

		// Load the required functions for device creation
		LoadFunctions();

		VkPhysicalDeviceFeatures devFeatures = features;


		VkDeviceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		createInfo.enabledExtensionCount = u32(m_EnabledExtensions.Size());
		createInfo.ppEnabledExtensionNames = m_EnabledExtensions.Data();
		createInfo.enabledLayerCount = u32(m_EnabledLayers.Size());
		createInfo.ppEnabledLayerNames = m_EnabledLayers.Data();
		createInfo.pEnabledFeatures = &devFeatures;
		createInfo.queueCreateInfoCount = u32(deviceQueues.Size());
		createInfo.pQueueCreateInfos = deviceQueues.Data();

		VkResult vkres = m_Vk.CreateDevice(m_pPhysicalDevice->GetPhysicalDevice(), &createInfo, m_pAllocCallbacks, &m_Device);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to create the vulkan device (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}

		LoadDeviceFunctions();

		return VK_SUCCESS;
	}

	void VulkanDevice::Destroy()
	{
		if (m_Device)
		{
			m_Vk.DestroyDevice(m_Device, m_pAllocCallbacks);
		}
	}

	b8 VulkanDevice::WaitIdle()
	{
		VkResult vkres = m_Vk.DeviceWaitIdle(m_Device);
		return vkres == VK_SUCCESS;
	}

	b8 VulkanDevice::IsExtensionAvailable(const String& extension)
	{
		return m_pPhysicalDevice->IsExtensionAvailable(extension);
	}

	b8 VulkanDevice::IsLayerAvailable(const String& layer)
	{
		return m_pPhysicalDevice->IsLayerAvailable(layer);
	}

	b8 VulkanDevice::IsExtensionEnabled(const String& extension)
	{
		for (const AnsiChar* name : m_EnabledExtensions)
		{
			if (name == extension)
				return true;
		}
		return false;
	}

	b8 VulkanDevice::IsLayerEnabled(const String& layer)
	{
		for (const AnsiChar* name : m_EnabledLayers)
		{
			if (name == layer)
				return true;
		}
		return false;
	}

	DynArray<Renderer::Core::Queue*>& VulkanDevice::CreateQueues(const DynArray<Renderer::QueueInfo>& deviceQueues)
	{;
		for (const Renderer::QueueInfo& info : deviceQueues)
		{
			for (u32 i = 0; i < info.count; ++i)
			{
				VulkanQueue* pQueue = HvNew VulkanQueue();
				b8 res = pQueue->Init(m_pContext, info.type, i, info.priority);
				if (!res)
				{
					g_Logger.LogError(LogVulkanRHI(), "Failed to initialize queue!");
				}
				m_Queues.Push(pQueue);
			}
		}
		return m_Queues;
	}

	VkQueue VulkanDevice::vkGetQueue(u32 family, u32 index)
	{
		VkQueue queue;
		m_Vk.GetDeviceQueue(m_Device, family, index, &queue);
		return queue;
	}

	u32 VulkanDevice::GetQueueFamily(Renderer::QueueType type)
	{
		return m_pPhysicalDevice->GetQueueFamily(type);
	}

	Renderer::Core::Queue* VulkanDevice::GetPresentQueue()
	{
		for (Renderer::Core::Queue* pQueue : m_Queues)
		{
			if (HV_IS_ENUM_FLAG_SET(pQueue->GetQueueType(), Renderer::QueueType::Graphics))
				return pQueue;
		}
		return nullptr;
	}

	////////////////////////////////////////////////////////////////////////////////
	VkResult VulkanDevice::vkCreateSwapchain(const VkSwapchainCreateInfoKHR& createInfo, VkSwapchainKHR& swapchain)
	{
		return m_VkExt.CreateSwapchainKHR(m_Device, &createInfo, m_pAllocCallbacks, &swapchain);
	}

	void VulkanDevice::vkDestroySwapchain(VkSwapchainKHR swapchain)
	{
		m_VkExt.DestroySwapchainKHR(m_Device, swapchain, m_pAllocCallbacks);
	}

	VkResult VulkanDevice::vkGetSwapchainImages(VkSwapchainKHR swapchain, u32& imageCount, VkImage* pImages)
	{
		return m_VkExt.GetSwapchainImagesKHR(m_Device, swapchain, &imageCount, pImages);
	}

	VkResult VulkanDevice::vkAquireNextImage(VkSwapchainKHR swapchain, u32& nextImageIndex, VkSemaphore semaphore,
		VkFence fence, u64 timeout)
	{
		return m_VkExt.AcquireNextImageKHR(m_Device, swapchain, timeout, semaphore, fence, &nextImageIndex);
	}

	VkResult VulkanDevice::vkCreateImage(const VkImageCreateInfo& createInfo, VkImage& image)
	{
		return m_Vk.CreateImage(m_Device, &createInfo, m_pAllocCallbacks, &image);
	}

	void VulkanDevice::vkDestroyImage(VkImage image)
	{
		m_Vk.DestroyImage(m_Device, image, m_pAllocCallbacks);
	}

	VkResult VulkanDevice::vkCreateImageView(const VkImageViewCreateInfo& createInfo, VkImageView& imageView)
	{
		return m_Vk.CreateImageView(m_Device, &createInfo, m_pAllocCallbacks, &imageView);
	}

	void VulkanDevice::vkDestroyImageView(VkImageView imageView)
	{
		m_Vk.DestroyImageView(m_Device, imageView, m_pAllocCallbacks);
	}

	void VulkanDevice::vkGetImageMemoryRequirements(VkImage image, VkMemoryRequirements& requirements)
	{
		m_Vk.GetImageMemoryRequirements(m_Device, image, &requirements);
	}

	VkResult VulkanDevice::vkBindImageMemory(VkImage image, VkDeviceMemory memory, VkDeviceSize offset)
	{
		return m_Vk.BindImageMemory(m_Device, image, memory, offset);
	}

	VkResult VulkanDevice::vkCreateSampler(const VkSamplerCreateInfo& createInfo, VkSampler& sampler)
	{
		return m_Vk.CreateSampler(m_Device, &createInfo, m_pAllocCallbacks, &sampler);
	}

	void VulkanDevice::vkDestroySampler(VkSampler sampler)
	{
		m_Vk.DestroySampler(m_Device, sampler, m_pAllocCallbacks);
	}

	VkResult VulkanDevice::vkCreateShaderModule(const VkShaderModuleCreateInfo& createInfo, VkShaderModule& shaderModule)
	{
		return m_Vk.CreateShaderModule(m_Device, &createInfo, m_pAllocCallbacks, &shaderModule);
	}

	void VulkanDevice::vkDestroyShaderModule(VkShaderModule shaderModule)
	{
		m_Vk.DestroyShaderModule(m_Device, shaderModule, m_pAllocCallbacks);
	}

	VkResult VulkanDevice::vkCreatePipelineLayout(const VkPipelineLayoutCreateInfo& createInfo, VkPipelineLayout& pipelineLayout)
	{
		return m_Vk.CreatePipelineLayout(m_Device, &createInfo, m_pAllocCallbacks, &pipelineLayout);
	}

	void VulkanDevice::vkDestroyPipelineLayout(VkPipelineLayout pipelineLayout)
	{
		m_Vk.DestroyPipelineLayout(m_Device, pipelineLayout, m_pAllocCallbacks);
	}

	VkResult VulkanDevice::vkCreatePipeline(const VkGraphicsPipelineCreateInfo& createInfo, VkPipeline& pipeline, VkPipelineCache cache)
	{
		return m_Vk.CreateGraphicsPipelines(m_Device, cache, 1, &createInfo, m_pAllocCallbacks, &pipeline);
	}

	VkResult VulkanDevice::vkCreatePipeline(const VkComputePipelineCreateInfo& createInfo, VkPipeline& pipeline, VkPipelineCache cache)
	{
		return m_Vk.CreateComputePipelines(m_Device, cache, 1, &createInfo, m_pAllocCallbacks, &pipeline);
	}

	void VulkanDevice::vkDestroyPipeline(VkPipeline pipeline)
	{
		m_Vk.DestroyPipeline(m_Device, pipeline, m_pAllocCallbacks);
	}

	VkResult VulkanDevice::vkCreateRenderPass(const VkRenderPassCreateInfo& createInfo, VkRenderPass& renderPass)
	{
		return m_Vk.CreateRenderPass(m_Device, &createInfo, m_pAllocCallbacks, &renderPass);
	}

	void VulkanDevice::vkDestroyRenderPass(VkRenderPass renderPass)
	{
		m_Vk.DestroyRenderPass(m_Device, renderPass, m_pAllocCallbacks);
	}

	VkResult VulkanDevice::vkCreateFramebuffer(const VkFramebufferCreateInfo& createInfo, VkFramebuffer& framebuffer)
	{
		return m_Vk.CreateFramebuffer(m_Device, &createInfo, m_pAllocCallbacks, &framebuffer);
	}

	void VulkanDevice::vkDestroyFramebuffer(VkFramebuffer framebuffer)
	{
		m_Vk.DestroyFramebuffer(m_Device, framebuffer, m_pAllocCallbacks);
	}

	VkResult VulkanDevice::vkCreateCommandPool(const VkCommandPoolCreateInfo& createInfo, VkCommandPool& commandPool)
	{
		return m_Vk.CreateCommandPool(m_Device, &createInfo, m_pAllocCallbacks, &commandPool);
	}

	void VulkanDevice::vkDestroyCommandPool(VkCommandPool commandPool)
	{
		m_Vk.DestroyCommandPool(m_Device, commandPool, m_pAllocCallbacks);
	}

	VkResult VulkanDevice::VkAllocateCommandBuffer(const VkCommandBufferAllocateInfo& allocInfo,
		VkCommandBuffer* pCommandBuffers)
	{
		HV_ASSERT(allocInfo.commandBufferCount == 1);
		return m_Vk.AllocateCommandBuffers(m_Device, &allocInfo, pCommandBuffers);
	}

	void VulkanDevice::vkFreeCommandBuffers(VkCommandPool commandPool, VkCommandBuffer commandBuffer)
	{
		m_Vk.FreeCommandBuffers(m_Device, commandPool, 1, &commandBuffer);
	}

	void VulkanDevice::vkFreeCommandBuffers(VkCommandPool commandPool, const DynArray<VkCommandBuffer> commandBuffers)
	{
		m_Vk.FreeCommandBuffers(m_Device, commandPool, u32(commandBuffers.Size()), commandBuffers.Data());
	}

	VkResult VulkanDevice::vkAllocateMemory(const VkMemoryAllocateInfo& allocInfo, VkDeviceMemory& memory)
	{
		return m_Vk.AllocateMemory(m_Device, &allocInfo, m_pAllocCallbacks, &memory);
	}

	void VulkanDevice::vkFreeMemory(VkDeviceMemory memory)
	{
		m_Vk.FreeMemory(m_Device, memory, m_pAllocCallbacks);
	}

	VkResult VulkanDevice::VkMapMemory(VkDeviceMemory memory, VkDeviceSize offset, VkDeviceSize size, void** ppData)
	{
		// Flags are 0, since until 1.1.73, flags are reserved
		return m_Vk.MapMemory(m_Device, memory, offset, size, 0, ppData);
	}

	void VulkanDevice::VkUnmapMemory(VkDeviceMemory memory)
	{
		m_Vk.UnmapMemory(m_Device, memory);
	}

	VkResult VulkanDevice::vkFlushMappedMemoryRanges(const VkMappedMemoryRange& range)
	{
		return m_Vk.FlushMappedMemoryRanges(m_Device, 1, &range);
	}

	VkResult VulkanDevice::vkFlushMappedMemoryRanges(const DynArray<VkMappedMemoryRange>& ranges)
	{
		return m_Vk.FlushMappedMemoryRanges(m_Device, u32(ranges.Size()), ranges.Data());
	}

	VkResult VulkanDevice::vkInvalidateMappedMemoryRanges(const VkMappedMemoryRange& range)
	{
		return m_Vk.InvalidateMappedMemoryRanges(m_Device, 1, &range);
	}

	VkResult VulkanDevice::vkInvalidateMappedMemoryRanges(const DynArray<VkMappedMemoryRange>& ranges)
	{
		return m_Vk.InvalidateMappedMemoryRanges(m_Device, u32(ranges.Size()), ranges.Data());
	}

	VkResult VulkanDevice::vkCreateBuffer(const VkBufferCreateInfo& createInfo, VkBuffer& buffer)
	{
		return m_Vk.CreateBuffer(m_Device, &createInfo, m_pAllocCallbacks, &buffer);
	}

	void VulkanDevice::vkDestroyBuffer(VkBuffer buffer)
	{
		m_Vk.DestroyBuffer(m_Device, buffer, m_pAllocCallbacks);
	}

	VkResult VulkanDevice::vkCreateBufferView(const VkBufferViewCreateInfo& createInfo, VkBufferView& bufferView)
	{
		return m_Vk.CreateBufferView(m_Device, &createInfo, m_pAllocCallbacks, &bufferView);
	}

	void VulkanDevice::vkDestroyBufferView(VkBufferView bufferView)
	{
		m_Vk.DestroyBufferView(m_Device, bufferView, m_pAllocCallbacks);
	}

	void VulkanDevice::vkGetBufferMemoryRequirements(VkBuffer buffer, VkMemoryRequirements& requirements)
	{
		m_Vk.GetBufferMemoryRequirements(m_Device, buffer, &requirements);
	}

	VkResult VulkanDevice::vkBindBufferMemory(VkBuffer buffer, VkDeviceMemory memory, VkDeviceSize offset)
	{
		return m_Vk.BindBufferMemory(m_Device, buffer, memory, offset);
	}

	VkResult VulkanDevice::vkCreateDescriptorSetLayout(const VkDescriptorSetLayoutCreateInfo& createInfo,
		VkDescriptorSetLayout& descriptorSetLayout)
	{
		return m_Vk.CreateDescriptorSetLayout(m_Device, &createInfo, m_pAllocCallbacks, &descriptorSetLayout);
	}

	void VulkanDevice::vkDestroyDescriptorSetLayout(VkDescriptorSetLayout descriptorSetLayout)
	{
		m_Vk.DestroyDescriptorSetLayout(m_Device, descriptorSetLayout, m_pAllocCallbacks);
	}

	VkResult VulkanDevice::vkCreateDescriptorPool(const VkDescriptorPoolCreateInfo& createInfo,
		VkDescriptorPool& descriptorPool)
	{
		return m_Vk.CreateDescriptorPool(m_Device, &createInfo, m_pAllocCallbacks, &descriptorPool);
	}

	void VulkanDevice::vkDestroyDescriptorPool(VkDescriptorPool descriptorPool)
	{
		m_Vk.DestroyDescriptorPool(m_Device, descriptorPool, m_pAllocCallbacks);
	}

	VkResult VulkanDevice::vkAllocateDescriptorSet(const VkDescriptorSetAllocateInfo& allocInfo,
		VkDescriptorSet& descriptorPool)
	{
		HV_ASSERT(allocInfo.descriptorSetCount == 1);
		return m_Vk.AllocateDescriptorSets(m_Device, &allocInfo, &descriptorPool);
	}

	void VulkanDevice::vkFreeDescriptorSet(VkDescriptorPool pool, VkDescriptorSet set)
	{
		m_Vk.FreeDescriptorSets(m_Device, pool, 1, &set);
	}

	void VulkanDevice::vkUpdateDescriptorSets(u32 numWrites, VkWriteDescriptorSet* writes, u32 numCopies,
		VkCopyDescriptorSet* copies)
	{
		m_Vk.UpdateDescriptorSets(m_Device, numWrites, writes, numCopies, copies);
	}

	VkResult VulkanDevice::vkCreateSemapore(const VkSemaphoreCreateInfo& createInfo, VkSemaphore& semaphore)
	{
		return m_Vk.CreateSemaphore(m_Device, &createInfo, m_pAllocCallbacks, &semaphore);
	}

	void VulkanDevice::vkDestroySemaphore(VkSemaphore semaphore)
	{
		m_Vk.DestroySemaphore(m_Device, semaphore, m_pAllocCallbacks);
	}

	VkResult VulkanDevice::vkCreateFence(const VkFenceCreateInfo& createInfo, VkFence& fence)
	{
		return m_Vk.CreateFence(m_Device, &createInfo, m_pAllocCallbacks, &fence);
	}

	void VulkanDevice::vkDestroyFence(VkFence fence)
	{
		m_Vk.DestroyFence(m_Device, fence, m_pAllocCallbacks);
	}

	VkResult VulkanDevice::vkResetFence(VkFence fence)
	{
		return m_Vk.ResetFences(m_Device, 1, &fence);
	}

	VkResult VulkanDevice::vkResetFences(DynArray<VkFence>& fences)
	{
		return m_Vk.ResetFences(m_Device, u32(fences.Size()), fences.Data());
	}

	VkResult VulkanDevice::vkWaitForFence(VkFence fence, u64 timeout)
	{
		return m_Vk.WaitForFences(m_Device, 1, &fence, true, timeout);
	}

	VkResult VulkanDevice::vkWaitForFences(DynArray<VkFence>& fences, b8 waitForAll, u64 timeout)
	{
		return m_Vk.WaitForFences(m_Device, u32(fences.Size()), fences.Data(), waitForAll, timeout);
	}

	VkResult VulkanDevice::vkGetFenceStatus(VkFence& fence)
	{
		return m_Vk.GetFenceStatus(m_Device, fence);
	}

	void VulkanDevice::LoadFunctions()
	{
		PFN_vkGetInstanceProcAddr getInstanceProcAddr = m_pContext->GetVkInstanceProcAddr();
		VkInstance instance = m_pContext->GetInstance()->GetInstance();
		m_Vk.GetDeviceProcAddr = (PFN_vkGetDeviceProcAddr)getInstanceProcAddr(instance, "vkGetDeviceProcAddr");
		m_Vk.CreateDevice = (PFN_vkCreateDevice)getInstanceProcAddr(instance, "vkCreateDevice");
	}

	void VulkanDevice::LoadDeviceFunctions()
	{
		// Device functions
		LOAD_VKD_FUNC(m_Vk, m_Device, DestroyDevice);
		LOAD_VKD_FUNC(m_Vk, m_Device, DeviceWaitIdle);

		// Queue functions
		LOAD_VKD_FUNC(m_Vk, m_Device, GetDeviceQueue);
		LOAD_VKD_FUNC(m_VkQueueFuncs, m_Device, QueueWaitIdle);
		LOAD_VKD_FUNC(m_VkQueueFuncs, m_Device, QueueSubmit);
		LOAD_VKD_FUNC(m_VkQueueFuncs, m_Device, QueuePresentKHR);

		// Image functions
		LOAD_VKD_FUNC(m_Vk, m_Device, CreateImage);
		LOAD_VKD_FUNC(m_Vk, m_Device, DestroyImage);
		LOAD_VKD_FUNC(m_Vk, m_Device, CreateImageView);
		LOAD_VKD_FUNC(m_Vk, m_Device, DestroyImageView);
		LOAD_VKD_FUNC(m_Vk, m_Device, GetImageMemoryRequirements);
		LOAD_VKD_FUNC(m_Vk, m_Device, BindImageMemory);

		// Sampler functions
		LOAD_VKD_FUNC(m_Vk, m_Device, CreateSampler);
		LOAD_VKD_FUNC(m_Vk, m_Device, DestroySampler);

		// Shader module functions
		LOAD_VKD_FUNC(m_Vk, m_Device, CreateShaderModule);
		LOAD_VKD_FUNC(m_Vk, m_Device, DestroyShaderModule);

		// Pipeline functions
		LOAD_VKD_FUNC(m_Vk, m_Device, CreatePipelineLayout);
		LOAD_VKD_FUNC(m_Vk, m_Device, DestroyPipelineLayout);
		LOAD_VKD_FUNC(m_Vk, m_Device, CreateGraphicsPipelines);
		LOAD_VKD_FUNC(m_Vk, m_Device, CreateComputePipelines);
		LOAD_VKD_FUNC(m_Vk, m_Device, DestroyPipeline);

		// Renderpass functions
		LOAD_VKD_FUNC(m_Vk, m_Device, CreateRenderPass);
		LOAD_VKD_FUNC(m_Vk, m_Device, DestroyRenderPass);

		// framebuffer
		LOAD_VKD_FUNC(m_Vk, m_Device, CreateFramebuffer);
		LOAD_VKD_FUNC(m_Vk, m_Device, DestroyFramebuffer);

		// Command pool
		LOAD_VKD_FUNC(m_Vk, m_Device, CreateCommandPool);
		LOAD_VKD_FUNC(m_Vk, m_Device, DestroyCommandPool);

		// Memory
		LOAD_VKD_FUNC(m_Vk, m_Device, AllocateMemory);
		LOAD_VKD_FUNC(m_Vk, m_Device, FreeMemory);
		LOAD_VKD_FUNC(m_Vk, m_Device, MapMemory);
		LOAD_VKD_FUNC(m_Vk, m_Device, UnmapMemory);
		LOAD_VKD_FUNC(m_Vk, m_Device, FlushMappedMemoryRanges);
		LOAD_VKD_FUNC(m_Vk, m_Device, InvalidateMappedMemoryRanges);

		// Buffers
		LOAD_VKD_FUNC(m_Vk, m_Device, CreateBuffer);
		LOAD_VKD_FUNC(m_Vk, m_Device, DestroyBuffer);
		LOAD_VKD_FUNC(m_Vk, m_Device, CreateBufferView);
		LOAD_VKD_FUNC(m_Vk, m_Device, DestroyBufferView);
		LOAD_VKD_FUNC(m_Vk, m_Device, GetBufferMemoryRequirements);
		LOAD_VKD_FUNC(m_Vk, m_Device, BindBufferMemory);

		// Descriptor set
		LOAD_VKD_FUNC(m_Vk, m_Device, CreateDescriptorSetLayout);
		LOAD_VKD_FUNC(m_Vk, m_Device, DestroyDescriptorSetLayout);
		LOAD_VKD_FUNC(m_Vk, m_Device, CreateDescriptorPool);
		LOAD_VKD_FUNC(m_Vk, m_Device, DestroyDescriptorPool);
		LOAD_VKD_FUNC(m_Vk, m_Device, AllocateDescriptorSets);
		LOAD_VKD_FUNC(m_Vk, m_Device, FreeDescriptorSets);
		LOAD_VKD_FUNC(m_Vk, m_Device, UpdateDescriptorSets);

		// Semaphore pool
		LOAD_VKD_FUNC(m_Vk, m_Device, CreateSemaphore);
		LOAD_VKD_FUNC(m_Vk, m_Device, DestroySemaphore);

		// fence pool
		LOAD_VKD_FUNC(m_Vk, m_Device, CreateFence);
		LOAD_VKD_FUNC(m_Vk, m_Device, DestroyFence);
		LOAD_VKD_FUNC(m_Vk, m_Device, WaitForFences);
		LOAD_VKD_FUNC(m_Vk, m_Device, GetFenceStatus);
		LOAD_VKD_FUNC(m_Vk, m_Device, ResetFences);

		// Renderview functions
		LOAD_VKD_FUNC(m_VkExt, m_Device, CreateSwapchainKHR);
		LOAD_VKD_FUNC(m_VkExt, m_Device, DestroySwapchainKHR);
		LOAD_VKD_FUNC(m_VkExt, m_Device, GetSwapchainImagesKHR);
		LOAD_VKD_FUNC(m_VkExt, m_Device, AcquireNextImageKHR);

		// Command buffer functions
		LOAD_VKD_FUNC(m_Vk, m_Device, AllocateCommandBuffers);
		LOAD_VKD_FUNC(m_Vk, m_Device, FreeCommandBuffers);

		LOAD_VKD_FUNC(m_VkCommandBufferFuncs, m_Device, BeginCommandBuffer);
		LOAD_VKD_FUNC(m_VkCommandBufferFuncs, m_Device, EndCommandBuffer);

		LOAD_VKD_FUNC(m_VkCommandBufferFuncs, m_Device, CmdBindPipeline);
		LOAD_VKD_FUNC(m_VkCommandBufferFuncs, m_Device, CmdBindDescriptorSets);
		LOAD_VKD_FUNC(m_VkCommandBufferFuncs, m_Device, CmdBindVertexBuffers);
		LOAD_VKD_FUNC(m_VkCommandBufferFuncs, m_Device, CmdBindIndexBuffer);

		LOAD_VKD_FUNC(m_VkCommandBufferFuncs, m_Device, CmdBeginRenderPass);
		LOAD_VKD_FUNC(m_VkCommandBufferFuncs, m_Device, CmdEndRenderPass);

		LOAD_VKD_FUNC(m_VkCommandBufferFuncs, m_Device, CmdSetViewport);
		LOAD_VKD_FUNC(m_VkCommandBufferFuncs, m_Device, CmdSetScissor);

		LOAD_VKD_FUNC(m_VkCommandBufferFuncs, m_Device, CmdDraw);
		LOAD_VKD_FUNC(m_VkCommandBufferFuncs, m_Device, CmdDrawIndexed);
		LOAD_VKD_FUNC(m_VkCommandBufferFuncs, m_Device, CmdDrawIndirect);

		LOAD_VKD_FUNC(m_VkCommandBufferFuncs, m_Device, CmdCopyBuffer);
		LOAD_VKD_FUNC(m_VkCommandBufferFuncs, m_Device, CmdCopyImage);
		LOAD_VKD_FUNC(m_VkCommandBufferFuncs, m_Device, CmdCopyBufferToImage);
		LOAD_VKD_FUNC(m_VkCommandBufferFuncs, m_Device, CmdCopyImageToBuffer);

		LOAD_VKD_FUNC(m_VkCommandBufferFuncs, m_Device, CmdPipelineBarrier);

	}
}

#undef LOAD_VKD_FUNC