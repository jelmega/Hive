// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanDescriptorSetManager.h: Vulkan descriptor set manager
#pragma once
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanDescriptorSetManager.h"
#include "VulkanRHI/VulkanDescriptorSet.h"
#include "VulkanRHI/VulkanDevice.h"
#include "VulkanRHI/VulkanContext.h"
#include "VulkanRHI/VulkanHelpers.h"
#include "VulkanRHI/VulkanDescriptorSetLayout.h"

#define MAX_DESC_SETS 1024 // Enough for now

namespace Hv::VulkanRHI {


	VulkanDescriptorSetManager::VulkanDescriptorSetManager()
		: m_DescriptorPool(VK_NULL_HANDLE)
	{
	}

	VulkanDescriptorSetManager::~VulkanDescriptorSetManager()
	{
	}

	b8 VulkanDescriptorSetManager::Create(RHI::RHIContext* pContext)
	{
		m_pContext = pContext;

		// for now, create 1 large descriptor pool
		// TODO: Improve this
		DynArray<VkDescriptorPoolSize> poolSizes;
		poolSizes.Reserve(VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT + 1);

		VkDescriptorPoolSize poolSize;
		poolSize.descriptorCount = MAX_DESC_SETS;
		poolSize.type = VK_DESCRIPTOR_TYPE_SAMPLER;
		poolSizes.Push(poolSize);
		poolSize.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		poolSizes.Push(poolSize);
		poolSize.type = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
		poolSizes.Push(poolSize);
		poolSize.type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
		poolSizes.Push(poolSize);
		poolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER;
		poolSizes.Push(poolSize);
		poolSize.type = VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER;
		poolSizes.Push(poolSize);
		poolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		poolSizes.Push(poolSize);
		poolSize.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
		poolSizes.Push(poolSize);
		poolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
		poolSizes.Push(poolSize);
		poolSize.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC;
		poolSizes.Push(poolSize);
		poolSize.type = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
		poolSizes.Push(poolSize);

		VkDescriptorPoolCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		createInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
		createInfo.maxSets = MAX_DESC_SETS;
		createInfo.poolSizeCount = u32(poolSizes.Size());
		createInfo.pPoolSizes = poolSizes.Data();

		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
		VkResult vkres = pDevice->vkCreateDescriptorPool(createInfo, m_DescriptorPool);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to create the vulkan descriptor pool (VkResult: %s)!", Helpers::GetResultString(vkres));
			return false;
		}

		return true;
	}

	b8 VulkanDescriptorSetManager::Destroy()
	{
		for (Renderer::Core::DescriptorSet* set : m_DescriptorSets)
		{
			set->Destroy();
			HvDelete set;
		}
		m_DescriptorSets.Clear();

		if (m_DescriptorPool)
		{
			VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
			pDevice->vkDestroyDescriptorPool(m_DescriptorPool);
		}

		return true;
	}

	Renderer::Core::DescriptorSetLayout* VulkanDescriptorSetManager::CreateDescriptorSetLayout(
		const DynArray<Renderer::Core::DescriptorSetBinding>& bindings)
	{
		// Try to reuse layouts, if it already exists
		for (Renderer::Core::DescriptorSetLayout* pLayout : m_Layouts)
		{
			if (pLayout->Matches(bindings))
				return pLayout;
		}

		// Else create a new layout
		VulkanDescriptorSetLayout* pLayout = HvNew VulkanDescriptorSetLayout();

		b8 res = pLayout->Create(m_pContext, this, bindings);
		if (!res)
		{
			g_Logger.LogError("Failed to create descriptor set!");
			HvDelete pLayout;
			return nullptr;
		}

		m_Layouts.Push(pLayout);
		return pLayout;
	}

	Renderer::Core::DescriptorSet* VulkanDescriptorSetManager::CreateDescriptorSet(Renderer::Core::DescriptorSetLayout* pLayout)
	{
		if (m_DescriptorSets.Size() == MAX_DESC_SETS)
		{
			g_Logger.LogError("Failed to create descriptor set, reach the max amount of allowed sets!");
			return nullptr;
		}

		VulkanDescriptorSet* pDescriptorSet = HvNew VulkanDescriptorSet();

		b8 res = pDescriptorSet->Create(m_pContext, this, pLayout);
		if (!res)
		{
			g_Logger.LogError("Failed to create descriptor set!");
			HvDelete pDescriptorSet;
			return nullptr;
		}

		m_DescriptorSets.Push(pDescriptorSet);
		return pDescriptorSet;
	}

	b8 VulkanDescriptorSetManager::DestroyDescriptorSetLayout(Renderer::Core::DescriptorSetLayout* pLayout)
	{
		if (pLayout->GetReferenceCount() > 0)
		{
			g_Logger.LogError(LogVulkanRHI(), "Can't destroy a descriptor set layout that is still being used!");
			return false;
		}

		Renderer::Core::DescriptorSetLayout** it = m_Layouts.Find(pLayout);
		if (it == m_Layouts.Back())
		{
			g_Logger.LogWarning("Failed to remove a descriptor set from manager!");
			return false;
		}

		b8 res = pLayout->Destroy();
		if (!res)
		{
			g_Logger.LogError("Failed to destroy a descriptor set layout!");
		}

		HvDelete pLayout;

		m_Layouts.Erase(it);
		return true;
	}

	b8 VulkanDescriptorSetManager::DestroyDescriptorSet(Renderer::Core::DescriptorSet* pDescriptorSet)
	{
		Renderer::Core::DescriptorSet** it = m_DescriptorSets.Find(pDescriptorSet);
		if (it == m_DescriptorSets.Back())
		{
			g_Logger.LogWarning("Failed to remove a descriptor set from manager");
			return false;
		}

		b8 res = pDescriptorSet->Destroy();
		if (!res)
		{
			g_Logger.LogError("Failed to destroy a descriptor set!");
		}

		HvDelete pDescriptorSet;

		m_DescriptorSets.Erase(it);
		return true;
	}
}

#undef MAX_DESC_SETS