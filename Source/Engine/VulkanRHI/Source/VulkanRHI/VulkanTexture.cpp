// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanTexure.h: Vulkan texture
#pragma once
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanTexture.h"
#include "VulkanRHI/VulkanHelpers.h"
#include "VulkanRHI/VulkanContext.h"
#include "VulkanRHI/VulkanDevice.h"
#include "VulkanRHI/VulkanDevice.h"

namespace Hv::VulkanRHI {


	VulkanTexture::VulkanTexture()
		: Texture()
		, m_Image(VK_NULL_HANDLE)
		, m_View(VK_NULL_HANDLE)
		, m_OwningQueueFamily(u32(-1))
		, m_pAllocation(nullptr)
		, m_pStagingBuffer(nullptr)
		, m_OwnsImage(false)
	{
	}

	VulkanTexture::~VulkanTexture()
	{
	}

	b8 VulkanTexture::Create(RHI::RHIContext* pContext, const Renderer::Core::TextureDesc& desc, Renderer::Core::CommandList* pCommandList)
	{
		m_pContext = pContext;
		m_Desc = desc;
		m_OwnsImage = true;

		if (m_Desc.flags &Renderer::TextureFlags::Dynamic)
		{
			m_Desc.layerCount = 1;
			m_Desc.mipLevels = 1;

			u8 formatSize = m_Desc.format.GetSize();
			if (formatSize == 0 || formatSize == 0xFF)
			{
				g_Logger.LogError(LogVulkanRHI(), "Can only use uncompressed data for dynamic textures!");
				return false;
			}
		}

		return CreateInternal(pCommandList);
	}

	b8 VulkanTexture::Create(RHI::RHIContext* pContext, VkImage image, VkImageLayout layout, u32 width, u32 height, PixelFormat format, Renderer::SampleCount samples,
		Renderer::TextureFlags flags)
	{
		m_pContext = pContext;
		m_Desc.width = width;
		m_Desc.height = height;
		m_Desc.format = format;
		m_Desc.samples = samples;
		m_Desc.flags = flags;
		m_Image = image;
		m_Desc.layout = Helpers::GetTextureLayout(layout);
		m_Desc.type = Renderer::TextureType::Tex2D;

		return CreateInternal(nullptr);
	}

	b8 VulkanTexture::Destroy()
	{
		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
		if (m_View)
		{
			pDevice->vkDestroyImageView(m_View);
		}

		if (m_pStagingBuffer)
		{
			m_pStagingBuffer->Destroy();
			HvDelete m_pStagingBuffer;
			m_pStagingBuffer = nullptr;
		}

		if (m_OwnsImage && m_Image)
		{
			if (m_pAllocation)
			{
				VulkanAllocator* pAllocator = ((VulkanContext*)m_pContext)->GetAllocator();
				pAllocator->Free(m_pAllocation);
			}

			pDevice->vkDestroyImage(m_Image);
		}

		return true;
	}

	b8 VulkanTexture::Write(const Renderer::TextureRegion& region, u64 size, void* pData,
		Renderer::Core::CommandList* pCommandList)
	{
		if (m_Desc.flags & Renderer::TextureFlags::Dynamic)
		{
			void* pMappedData = m_pAllocation->Map(0, size, VulkanAllocationMapMode::Write);
			if (!pMappedData)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to map the vulkan buffer memory!");
				return false;
			}

			u8 formatSize = m_Desc.format.GetSize();
			u64 lineSize = m_Desc.width * formatSize;
			u64 layerSize = lineSize * m_Desc.height;
			u64 copySize = region.extent.x * formatSize;
			u64 zOffset = region.offset.z * layerSize;
			u64 yOffset = region.offset.y * lineSize;
			u64 xOffset = region.offset.x * formatSize;
			u64 bytesWritten = 0;
			
			for (u32 z = 0; z < region.extent.y && bytesWritten + lineSize < size; ++z)
			{
				u64 offset = zOffset + z * layerSize + yOffset + xOffset;
				for (u32 y = 0; y < region.extent.y && bytesWritten + lineSize > size; ++y)
				{
					if (bytesWritten + lineSize > size)
						copySize = size - bytesWritten;

					Memory::Copy(pMappedData, (u8*)pData + offset, copySize);
					offset += lineSize;
					bytesWritten += lineSize;
				}
			}

			m_pAllocation->Unmap();

			return true;
		}
		else if (m_Desc.flags & Renderer::TextureFlags::Static)
		{
			VulkanBuffer* pStagingBuffer = HvNew VulkanBuffer();
			b8 res = pStagingBuffer->Create(m_pContext, Renderer::BufferType::Staging, size, Renderer::BufferFlags::None);
			if (!res)
			{
				HvDelete pStagingBuffer;
				return false;
			}
			res = pStagingBuffer->Write(0, size, pData);
			if (!res)
			{
				pStagingBuffer->Destroy();
				HvDelete pStagingBuffer;
				return false;
			}
			Renderer::TextureBufferCopyRegion copyRegion = {};
			copyRegion.texOffset = region.offset;
			copyRegion.texExtent = region.extent;
			copyRegion.mipLevel = region.mipLevel;
			copyRegion.baseArrayLayer = region.baseArrayLayer;
			copyRegion.layerCount = region.layerCount;
			res = Copy(pStagingBuffer, copyRegion);

			pStagingBuffer->Destroy();
			HvDelete pStagingBuffer;

			return res;
		}
		else
		{
			b8 res = m_pStagingBuffer->Write(0, size, pData);
			if (!res)
				return res;
			Renderer::TextureBufferCopyRegion copyRegion = {};
			copyRegion.texOffset = region.offset;
			copyRegion.texExtent = region.extent;
			copyRegion.mipLevel = region.mipLevel;
			copyRegion.baseArrayLayer = region.baseArrayLayer;
			copyRegion.layerCount = region.layerCount;
			return Copy(m_pStagingBuffer, copyRegion, pCommandList);
		}
	}

	b8 VulkanTexture::Read(const Renderer::TextureRegion& region, u64 size, void* pData,
		Renderer::Core::CommandList* pCommandList)
	{
		if (m_Desc.flags & Renderer::TextureFlags::Dynamic)
		{
			void* pMappedData = m_pAllocation->Map(0, size, VulkanAllocationMapMode::Write);
			if (!pMappedData)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to map the vulkan buffer memory!");
				return false;
			}

			u8 formatSize = m_Desc.format.GetSize();
			u64 lineSize = m_Desc.width * formatSize;
			u64 layerSize = lineSize * m_Desc.height;
			u64 copySize = region.extent.x * formatSize;
			u64 zOffset = region.offset.z * layerSize;
			u64 yOffset = region.offset.y * lineSize;
			u64 xOffset = region.offset.x * formatSize;
			u64 bytesWritten = 0;

			for (u32 z = 0; z < region.extent.y && bytesWritten + lineSize < size; ++z)
			{
				u64 offset = zOffset + z * layerSize + yOffset + xOffset;
				for (u32 y = 0; y < region.extent.y && bytesWritten + lineSize > size; ++y)
				{
					if (bytesWritten + lineSize > size)
						copySize = size - bytesWritten;

					Memory::Copy((u8*)pData, (u8*)pMappedData + offset, copySize);
					offset += lineSize;
					bytesWritten += lineSize;
				}
			}

			m_pAllocation->Unmap();
			return true;
		}
		else if (m_Desc.flags & Renderer::TextureFlags::Static)
		{
			VulkanBuffer* pStagingBuffer = HvNew VulkanBuffer();
			b8 res = pStagingBuffer->Create(m_pContext, Renderer::BufferType::Staging, size, Renderer::BufferFlags::None);
			if (!res)
			{
				HvDelete pStagingBuffer;
				return false;
			}
			Renderer::TextureBufferCopyRegion copyRegion = {};
			copyRegion.texOffset = region.offset;
			copyRegion.texExtent = region.extent;
			copyRegion.mipLevel = region.mipLevel;
			copyRegion.baseArrayLayer = region.baseArrayLayer;
			copyRegion.layerCount = region.layerCount;
			res = Copy(pStagingBuffer, copyRegion);
			if (!res)
			{
				pStagingBuffer->Destroy();
				HvDelete pStagingBuffer;
				return false;
			}

			res = pStagingBuffer->Read(0, size, pData);
			pStagingBuffer->Destroy();
			HvDelete pStagingBuffer;

			return res;
		}
		else
		{
			Renderer::TextureBufferCopyRegion copyRegion = {};
			copyRegion.texOffset = region.offset;
			copyRegion.texExtent = region.extent;
			copyRegion.mipLevel = region.mipLevel;
			copyRegion.baseArrayLayer = region.baseArrayLayer;
			copyRegion.layerCount = region.layerCount;
			b8 res = m_pStagingBuffer->Copy(this, copyRegion, pCommandList);
			if (!res)
				return res;
			
			return m_pStagingBuffer->Read(0, size, pData);
		}

	}

	b8 VulkanTexture::Copy(Texture* pTexture, const Renderer::TextureCopyRegion& region,
		Renderer::Core::CommandList* pCommandList)
	{
		b8 isSingleTimeCommands = false;
		Renderer::Core::CommandListManager* pCommandListManager = ((VulkanContext*)m_pContext)->GetCommandListManager();

		if (pCommandList == nullptr)
		{
			Renderer::Core::Queue* pCopyQueue = m_pContext->GetQueue(Renderer::QueueType::Transfer);
			pCommandList = pCommandListManager->CreateCommandList(pCopyQueue);
			isSingleTimeCommands = true;
		}

		if (m_Desc.layout != Renderer::TextureLayout::TransferDst)
		{
			Renderer::TextureLayoutTransition transition = {};
			transition.layout = Renderer::TextureLayout::TransferDst;
			transition.baseMipLevel = 0;
			transition.mipLevelCount = m_Desc.mipLevels;
			transition.baseArrayLayer = 0;
			transition.layerCount = m_Desc.layerCount;
			pCommandList->TransitionTextureLayout(Renderer::PipelineStage::Transfer, Renderer::PipelineStage::Transfer, this, transition);
		}
		if (pTexture->GetLayout() != Renderer::TextureLayout::TransferSrc)
		{
			Renderer::TextureLayoutTransition transition = {};
			transition.layout = Renderer::TextureLayout::TransferDst;
			transition.baseMipLevel = 0;
			transition.mipLevelCount = m_Desc.mipLevels;
			transition.baseArrayLayer = 0;
			transition.layerCount = m_Desc.layerCount;
			pCommandList->TransitionTextureLayout(Renderer::PipelineStage::Transfer, Renderer::PipelineStage::Transfer, this, transition);
		}

		pCommandList->CopyTexture(pTexture, this, region);

		if (isSingleTimeCommands)
		{
			pCommandListManager->EndSingleTimeCommandList(pCommandList);
		}

		return true;
	}

	b8 VulkanTexture::Copy(Renderer::Core::Buffer* pBuffer, const Renderer::TextureBufferCopyRegion& region,
		Renderer::Core::CommandList* pCommandList)
	{
		b8 isSingleTimeCommands = false;
		Renderer::Core::CommandListManager* pCommandListManager = ((VulkanContext*)m_pContext)->GetCommandListManager();

		if (pCommandList == nullptr)
		{
			Renderer::Core::Queue* pCopyQueue = m_pContext->GetQueue(Renderer::QueueType::Transfer);
			pCommandList = pCommandListManager->CreateSingleTimeCommandList(pCopyQueue);
			isSingleTimeCommands = true;
		}

		if (m_Desc.layout != Renderer::TextureLayout::TransferDst)
		{
			Renderer::TextureLayoutTransition transition = {};
			transition.layout = Renderer::TextureLayout::TransferDst;
			transition.baseMipLevel = 0;
			transition.mipLevelCount = m_Desc.mipLevels;
			transition.baseArrayLayer = 0;
			transition.layerCount = m_Desc.layerCount;
			pCommandList->TransitionTextureLayout(Renderer::PipelineStage::Transfer, Renderer::PipelineStage::Transfer, this, transition);
		}

		pCommandList->CopyBufferToTexture(pBuffer, this, region);

		// TODO: Should we transition back to the old layout?

		if (isSingleTimeCommands)
		{
			pCommandListManager->EndSingleTimeCommandList(pCommandList);
		}

		return true;
	}

	b8 VulkanTexture::CreateInternal(Renderer::Core::CommandList* pCommandList)
	{
		VkResult vkres;
		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();

		if (m_OwnsImage)
		{
			VkImageCreateInfo imageInfo = {};
			imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
			imageInfo.extent.width = m_Desc.width;
			imageInfo.extent.height = m_Desc.height;
			imageInfo.extent.depth = m_Desc.depth;
			imageInfo.arrayLayers = m_Desc.layerCount;
			imageInfo.mipLevels = m_Desc.mipLevels;
			imageInfo.imageType = Helpers::GetImageType(m_Desc.type);
			imageInfo.format = Helpers::GetFormat(m_Desc.format);
			imageInfo.tiling = (m_Desc.flags & Renderer::TextureFlags::Dynamic) ? VK_IMAGE_TILING_LINEAR : VK_IMAGE_TILING_OPTIMAL;
			imageInfo.samples = Helpers::GetSampleCount(m_Desc.samples);
			imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
			imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

			// usage
			imageInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
			if (!(m_Desc.flags & Renderer::TextureFlags::NoSampling))
				imageInfo.usage |=  VK_IMAGE_USAGE_SAMPLED_BIT;
			if (m_Desc.flags & Renderer::TextureFlags::Storage)
				imageInfo.usage |= VK_IMAGE_USAGE_STORAGE_BIT;
			if (m_Desc.flags & Renderer::TextureFlags::InputAttachment)
				imageInfo.usage |= VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT;

			// flags
			// Always mutable?
			imageInfo.flags |= VK_IMAGE_CREATE_MUTABLE_FORMAT_BIT;
			if (m_Desc.type == Renderer::TextureType::Cubemap || m_Desc.type == Renderer::TextureType::CubemapArray)
				imageInfo.flags |= VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
			if (m_Desc.type == Renderer::TextureType::Tex2DArray)
				imageInfo.flags |= VK_IMAGE_CREATE_2D_ARRAY_COMPATIBLE_BIT;
			// TODO: other flags

			vkres = pDevice->vkCreateImage(imageInfo, m_Image);
			if (vkres != VK_SUCCESS)
			{
				g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Error, "Failed to create vulkan image (VkResult: %s)!", Helpers::GetResultString(vkres));
				return false;
			}

			VkMemoryRequirements memReqs;
			pDevice->vkGetImageMemoryRequirements(m_Image, memReqs);
			m_MemorySize = memReqs.size;

			VulkanAllocator* pAllocator = ((VulkanContext*)m_pContext)->GetAllocator();
			VkMemoryPropertyFlags memProps = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
			if (m_Desc.flags & Renderer::TextureFlags::Dynamic)
				memProps = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;

			m_pAllocation = pAllocator->Allocate(memReqs, memProps);
			if (!m_pAllocation)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to allocate a vulkan allocation!");
				Destroy();
				return false;
			}

			vkres = pDevice->vkBindImageMemory(m_Image, m_pAllocation->GetMemory(), m_pAllocation->GetOffset());
			if (vkres != VK_SUCCESS)
			{
				g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Error, "Failed to bind vulkan image memory (VkResult: %s)!", Helpers::GetResultString(vkres));
				return false;
			}

			if (!(m_Desc.flags & Renderer::TextureFlags::Static || m_Desc.flags & Renderer::TextureFlags::Dynamic || m_Desc.flags & Renderer::TextureFlags::RenderTargetable))
			{
				m_pStagingBuffer = HvNew VulkanBuffer();
				b8 res = m_pStagingBuffer->Create(m_pContext, Renderer::BufferType::Staging, memReqs.size, Renderer::BufferFlags::None);
			}

			// Transition layout
			if (m_Desc.layout != Renderer::TextureLayout::Unknown)
			{
				Renderer::TextureLayoutTransition transition = {};
				transition.layout = m_Desc.layout;
				transition.baseArrayLayer = 0;
				transition.layerCount = m_Desc.layerCount;
				transition.baseMipLevel = 0;
				transition.mipLevelCount = m_Desc.mipLevels;
				m_Desc.layout = Renderer::TextureLayout::Unknown;

				if (pCommandList)
				{
					pCommandList->TransitionTextureLayout(Renderer::PipelineStage::Transfer, Renderer::PipelineStage::Transfer, this, transition);
				}
				else
				{
					Renderer::Core::CommandListManager* pManager = m_pContext->GetCommandListManager();
					Renderer::Core::Queue* pQueue = m_pContext->GetQueue(Renderer::QueueType::Transfer);
					pCommandList = pManager->CreateSingleTimeCommandList(pQueue);

					pCommandList->TransitionTextureLayout(Renderer::PipelineStage::Transfer, Renderer::PipelineStage::Transfer, this, transition);

					pManager->EndSingleTimeCommandList(pCommandList);
				}
			}
		}

		// Create image view
		VkImageViewCreateInfo viewInfo = {};
		viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		viewInfo.image = m_Image;
		viewInfo.format = Helpers::GetFormat(m_Desc.format);
		viewInfo.viewType = Helpers::GetImageViewType(m_Desc.type);;
		viewInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

		VkImageAspectFlags aspect = m_Desc.format.HasDepthComponent() ? VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;
		if (m_Desc.format.HasStencilComponent())
			aspect |= VK_IMAGE_ASPECT_STENCIL_BIT;
		viewInfo.subresourceRange.aspectMask = aspect;
		viewInfo.subresourceRange.baseArrayLayer = 0;
		viewInfo.subresourceRange.layerCount = m_Desc.layerCount;
		viewInfo.subresourceRange.baseMipLevel = 0;
		viewInfo.subresourceRange.levelCount = m_Desc.mipLevels;

		vkres = pDevice->vkCreateImageView(viewInfo, m_View);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Error, "Failed to create vulkan image view (VkResult: %s)!", Helpers::GetResultString(vkres));
			return false;
		}

		return true;
	}
}
