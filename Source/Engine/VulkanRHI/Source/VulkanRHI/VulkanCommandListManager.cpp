// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanCommandListManager.h: Vulkan command list manager
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanCommandListManager.h"
#include "VulkanRHI/VulkanContext.h"
#include "VulkanRHI/VulkanPhysicalDevice.h"
#include "VulkanRHI/VulkanDevice.h"
#include "VulkanRHI/VulkanQueue.h"
#include "VulkanRHI/VulkanHelpers.h"
#include "VulkanRHI/VulkanCommandList.h"
#include "VulkanRHI/VulkanBuffer.h"
#include "VulkanRHI/VulkanDynamicRHI.h"

namespace Hv::VulkanRHI {


	VulkanCommandListManager::VulkanCommandListManager()
	{
	}

	VulkanCommandListManager::~VulkanCommandListManager()
	{
	}

	b8 VulkanCommandListManager::Create(RHI::RHIContext* pContext)
	{
		m_pContext = pContext;

		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
		VulkanPhysicalDevice* pPhysDevice = pDevice->GetPhysicalDevice();

		VkCommandPoolCreateInfo poolInfo = {};
		poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
		const DynArray<Renderer::Core::Queue*>& queues = pDevice->GetQueues();
		for (Renderer::Core::Queue* pQueue : queues)
		{
			u32 queueFamily = ((VulkanQueue*)pQueue)->GetQueueFamily();
			poolInfo.queueFamilyIndex = queueFamily;

			if (m_CommandPools.Size() <= queueFamily)
				m_CommandPools.Resize(queueFamily + 1);

			VkResult vkres = pDevice->vkCreateCommandPool(poolInfo, m_CommandPools[queueFamily]);
			if (vkres != VK_SUCCESS)
			{
				g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to create a vulkan command pool (VkResult: %s)!", Helpers::GetResultString(vkres));
				return vkres;
			}
		}

		return true;
	}

	b8 VulkanCommandListManager::Destroy()
	{
		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();

		for (Renderer::Core::CommandList* pCommandList : m_CommandLists)
		{
			((VulkanCommandList*)pCommandList)->Destroy();
			HvDelete pCommandList;
		}

		for (VkCommandPool commandPool : m_CommandPools)
		{
			if (commandPool)
			{
				pDevice->vkDestroyCommandPool(commandPool);
			}
		}
		m_CommandPools.Clear();

		return true;
	}

	Renderer::Core::CommandList* VulkanCommandListManager::CreateCommandList(Renderer::Core::Queue* pQueue)
	{
		VulkanCommandList* pCommandList = HvNew VulkanCommandList();
		b8 res = pCommandList->Create(m_pContext, this, pQueue);
		if (!res)
		{
			g_Logger.LogError(LogVulkanRHI(), "Failed to create command list!");
			HvDelete pCommandList;
			return nullptr;
		}
		m_CommandLists.Push(pCommandList);
		return pCommandList;
	}

	b8 VulkanCommandListManager::DestroyCommandList(Renderer::Core::CommandList* pCommandList)
	{
		Renderer::Core::CommandList** it = m_CommandLists.Find(pCommandList);
		if (it == m_CommandLists.Back())
		{
			g_Logger.LogError(LogVulkanRHI(), "Failed to remove a command list, manager does not contain a command list!");
			return false;
		}

		b8 res = ((VulkanCommandList*)pCommandList)->Destroy();
		HvDelete pCommandList;

		m_CommandLists.Erase(it);

		return res;
	}

	Renderer::Core::CommandList* VulkanCommandListManager::CreateSingleTimeCommandList(Renderer::Core::Queue* pQueue)
	{
		VulkanCommandList* pCommandList = HvNew VulkanCommandList();
		b8 res = pCommandList->Create(m_pContext, this, pQueue);
		if (!res)
		{
			g_Logger.LogError(LogVulkanRHI(), "Failed to create single time command list!");
			HvDelete pCommandList;
			return nullptr;
		}

		res = pCommandList->Begin();
		if (!res)
		{
			g_Logger.LogError(LogVulkanRHI(), "Failed to begin single time command list!");
			pCommandList->Destroy();
			HvDelete pCommandList;
			return nullptr;
		}
		m_STCommandLists.Push(pCommandList);

		return pCommandList;
	}

	b8 VulkanCommandListManager::EndSingleTimeCommandList(Renderer::Core::CommandList* pCommandList)
	{
		b8 res = true;
		if (pCommandList->GetState() == Renderer::CommandListState::Recording)
		{
			res = pCommandList->End();
			if (!res)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to end single time command list!");
			}
		}
		if (res && pCommandList->GetState() == Renderer::CommandListState::Recorded)
		{
			res = pCommandList->Submit();
			if (!res)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to submit single time command list!");
			}
		}
		if (res && pCommandList->GetState() == Renderer::CommandListState::Submited)
		{
			res = pCommandList->Wait();
			if (!res)
			{
				g_Logger.LogError(LogVulkanRHI(), "Failed to wait for single time command list!");
			}
		}

		Renderer::Core::CommandList** it = m_STCommandLists.Find(pCommandList);
		if (it == m_STCommandLists.Back())
		{
			g_Logger.LogError(LogVulkanRHI(), "Failed to remove a single time command list, manager does not contain a command list!");
			return false;
		}

		((VulkanCommandList*)pCommandList)->Destroy();
		HvDelete pCommandList;

		m_STCommandLists.Erase(it);

		return res;
	}

	VkCommandPool VulkanCommandListManager::GetCommandPool(Renderer::Core::Queue* pQueue)
	{
		u32 index = ((VulkanQueue*)pQueue)->GetQueueFamily();
		return m_CommandPools[index];
	}
}
