// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanShader.h: Vulkan shader
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanShader.h"
#include "VulkanRHI/VulkanContext.h"
#include "VulkanRHI/VulkanDevice.h"
#include "VulkanRHI/VulkanHelpers.h"

namespace Hv::VulkanRHI {


	VulkanShader::VulkanShader()
		: Shader()
		, m_ShaderModule(VK_NULL_HANDLE)
		, m_StageInfo()
	{
	}

	VulkanShader::~VulkanShader()
	{
	}

	b8 VulkanShader::Create(RHI::RHIContext* pContext, const Renderer::Core::ShaderDesc& desc)
	{
		HV_ASSERT(desc.language == Renderer::ShaderLanguage::Spirv);
		m_pContext = pContext;
		m_Type = desc.type;
		m_FilePath = desc.filePath;
		m_EntryPoint = desc.entryPoint;
		m_Language = desc.language;

		b8 res = LoadShaderSource();
		if (!res)
			return false;

		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();

		VkShaderModuleCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		createInfo.codeSize = m_Source.Size();
		createInfo.pCode = (u32*)m_Source.Data();

		VkResult vkres = pDevice->vkCreateShaderModule(createInfo, m_ShaderModule);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Error, "Failed to create shader module (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}

		// Create shader stage info
		// TODO: how to do specialization info
		m_StageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		m_StageInfo.module = m_ShaderModule;
		m_StageInfo.stage = Helpers::GetShaderStage(m_Type);
		m_StageInfo.pName = m_EntryPoint.CStr();

		return true;
	}

	b8 VulkanShader::Destroy()
	{
		if (m_ShaderModule)
		{
			VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
			pDevice->vkDestroyShaderModule(m_ShaderModule);
			m_ShaderModule = VK_NULL_HANDLE;
		}
		return true;
	}
}
