// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanFramebuffer.h: Vulkan framebuffer
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanFramebuffer.h"
#include "VulkanRHI/VulkanTexture.h"
#include "VulkanRHI/VulkanRenderPass.h"
#include "VulkanRHI/VulkanContext.h"
#include "VulkanRHI/VulkanDevice.h"
#include "VulkanRHI/VulkanHelpers.h"

namespace Hv::VulkanRHI {


	VulkanFramebuffer::VulkanFramebuffer()
		: m_FrameBuffer(VK_NULL_HANDLE)
	{
	}

	VulkanFramebuffer::~VulkanFramebuffer()
	{
	}

	b8 VulkanFramebuffer::Create(RHI::RHIContext* pContext, const DynArray<Renderer::Core::RenderTarget*>& renderTargets, Renderer::Core::RenderPass* pRenderPass)
	{
		m_pContext = pContext;
		m_RenderTargets = renderTargets;
		m_pRenderPass = pRenderPass;

		DynArray<VkImageView> views;
		m_Width = 0xFFFF'FFFF;
		m_Height = 0xFFFF'FFFF;
		// TODO: support render targets with multiple layers
		u32 layers = 1;
		for (Renderer::Core::RenderTarget* pRT : m_RenderTargets)
		{
			views.Push(((VulkanTexture*)pRT->GetTexture())->GetImageView());
			m_Width = Math::Min(m_Width, pRT->GetWidth());
			m_Height = Math::Min(m_Height, pRT->GetHeight());
		}

		VkFramebufferCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		createInfo.attachmentCount = u8(views.Size());
		createInfo.pAttachments = views.Data();
		createInfo.renderPass = ((VulkanRenderPass*)m_pRenderPass)->GetRenderPass();
		createInfo.width = m_Width;
		createInfo.height = m_Height;
		createInfo.layers = layers;

		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
		VkResult vkres = pDevice->vkCreateFramebuffer(createInfo, m_FrameBuffer);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Error, "Failed to create a vulkan frame buffer (VkResult: %s)!", Helpers::GetResultString(vkres));
			return vkres;
		}

		return true;
	}

	b8 VulkanFramebuffer::Destroy()
	{
		if (m_FrameBuffer)
		{
			VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
			pDevice->vkDestroyFramebuffer(m_FrameBuffer);
			m_FrameBuffer = VK_NULL_HANDLE;
		}
		return true;
	}
}
