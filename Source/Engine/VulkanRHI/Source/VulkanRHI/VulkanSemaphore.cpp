// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanSemaphore.cpp: Vulkan semaphore
#pragma once
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanSemaphore.h"
#include "VulkanRHI/VulkanContext.h"
#include "VulkanRHI/VulkanDevice.h"
#include "VulkanRHI/VulkanHelpers.h"

namespace Hv::VulkanRHI {


	VulkanSemaphore::VulkanSemaphore()
		: m_Semaphore(VK_NULL_HANDLE)
	{
	}

	VulkanSemaphore::~VulkanSemaphore()
	{
	}

	b8 VulkanSemaphore::Create(RHI::RHIContext* pContext)
	{
		m_pContext = pContext;

		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();

		VkSemaphoreCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

		VkResult vkres = pDevice->vkCreateSemapore(createInfo, m_Semaphore);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to create the vulkan semaphore (VkResult: %s)!", Helpers::GetResultString(vkres));
			return false;
		}
		return true;
	}

	b8 VulkanSemaphore::Destroy()
	{
		if (m_Semaphore)
		{
			VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
			pDevice->vkDestroySemaphore(m_Semaphore);
			m_Semaphore = VK_NULL_HANDLE;
		}

		return true;
	}
}
