// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanDynamicRHI.cpp: Vulkan dynamic RHI
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanDynamicRHI.h"
#include "VulkanRHI/VulkanContext.h"
#include "VulkanRHI/VulkanDevice.h"
#include "VulkanRHI/VulkanPhysicalDevice.h"
#include "VulkanRHI/VulkanBuffer.h"
#include "VulkanRHI/VulkanSwapChain.h"
#include "VulkanRHI/VulkanShader.h"
#include "VulkanRHI/VulkanPipeline.h"
#include "VulkanRHI/VulkanRenderPass.h"
#include "VulkanRHI/VulkanFramebuffer.h"
#include "VulkanRHI/VulkanSampler.h"
#include "VulkanRHI/VulkanTexture.h"
#include "VulkanRHI/VulkanRenderTarget.h"

HV_DECLARE_LOG_CATEGORY(VulkanRHI, Hv::LogLevel::All);

namespace Hv::VulkanRHI {


	VulkanDynamicRHI::VulkanDynamicRHI()
		: IDynamicRHI()
	{
	}

	VulkanDynamicRHI::~VulkanDynamicRHI()
	{
	}

	b8 VulkanDynamicRHI::Init(const RHI::RHIDesc& desc)
	{
		m_Desc = desc;

		m_pContext = HvNew VulkanContext();
		b8 res = m_pContext->Init(desc);
		if (!res)
		{
			g_Logger.LogFatal("Failed to initialize vulkan context");
			Destroy();
			return false;
		}

		return true;
	}

	b8 VulkanDynamicRHI::Destroy()
	{
		if (m_pContext)
		{
			b8 res = m_pContext->Destroy();
			HvDelete m_pContext; // Delete if destroy failed
			if (!res)
			{
				g_Logger.LogFatal(LogVulkanRHI(), "Failed to desteoy context");
				return false;
			}
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////
	// Renderview																  //
	////////////////////////////////////////////////////////////////////////////////
	Renderer::Core::SwapChain* VulkanDynamicRHI::CreateRenderView(Window* pWindow, Renderer::VSyncMode vsync)
	{
		VulkanSwapChain* pView = HvNew VulkanSwapChain();

		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
		Renderer::Core::Queue* pQueue = pDevice->GetPresentQueue();
		b8 res = pView->Init(m_pContext, pWindow, vsync, pQueue);
		if (!res)
		{
			pView->Destroy();
			HvDelete pView;
			return nullptr;
		}
		return pView;
	}

	b8 VulkanDynamicRHI::DestroyRenderView(Renderer::Core::SwapChain* pRenderView)
	{
		b8 res = pRenderView->Destroy();
		HvDelete pRenderView;
		return res;
	}

	////////////////////////////////////////////////////////////////////////////////
	// Shaders																	  //
	////////////////////////////////////////////////////////////////////////////////
	Renderer::Core::Shader* VulkanDynamicRHI::CreateShader(const Renderer::Core::ShaderDesc& desc)
	{
		VulkanShader* pShader = HvNew VulkanShader();
		b8 res = pShader->Create(m_pContext, desc);
		if (!res)
		{
			pShader->Destroy();
			HvDelete pShader;
			return nullptr;
		}
		return pShader;
	}

	b8 VulkanDynamicRHI::DestroyShader(Renderer::Core::Shader* pShader)
	{
		b8 res = pShader->Destroy();
		HvDelete pShader;
		return res;
	}

	////////////////////////////////////////////////////////////////////////////////
	// Render pass																  //
	////////////////////////////////////////////////////////////////////////////////
	Renderer::Core::RenderPass* VulkanDynamicRHI::CreateRenderPass(
		const DynArray<Renderer::Core::RenderPassAttachment>& attachments,
		const DynArray<Renderer::Core::SubRenderPass>& subpasses)
	{
		VulkanRenderPass* pRenderPass = HvNew VulkanRenderPass();
		b8 res = pRenderPass->Create(m_pContext, attachments, subpasses);
		if (!res)
		{
			pRenderPass->Destroy();
			HvDelete pRenderPass;
			return nullptr;
		}
		return pRenderPass;
	}

	b8 VulkanDynamicRHI::DestroyRenderPass(Renderer::Core::RenderPass* pRenderPass)
	{
		b8 res = pRenderPass->Destroy();
		HvDelete pRenderPass;
		return res;
	}

	////////////////////////////////////////////////////////////////////////////////
	// Pipelines																  //
	////////////////////////////////////////////////////////////////////////////////
	Renderer::Core::Pipeline* VulkanDynamicRHI::CreatePipeline(const Renderer::Core::GraphicsPipelineDesc& desc)
	{
		VulkanPipeline* pPipeline = HvNew VulkanPipeline();
		b8 res = pPipeline->Create(m_pContext, desc);
		if (!res)
		{
			pPipeline->Destroy();
			HvDelete pPipeline;
			return nullptr;
		}
		return pPipeline;
	}

	Renderer::Core::Pipeline* VulkanDynamicRHI::CreatePipeline(const Renderer::Core::ComputePipelineDesc& desc)
	{
		VulkanPipeline* pPipeline = HvNew VulkanPipeline();
		b8 res = pPipeline->Create(m_pContext, desc);
		if (!res)
		{
			pPipeline->Destroy();
			HvDelete pPipeline;
			return nullptr;
		}
		return pPipeline;
	}

	b8 VulkanDynamicRHI::DestroyPipeline(Renderer::Core::Pipeline* pPipeline)
	{
		b8 res = pPipeline->Destroy();
		HvDelete pPipeline;
		return res;
	}

	Renderer::Core::Framebuffer* VulkanDynamicRHI::CreateFramebuffer(
		const DynArray<Renderer::Core::RenderTarget*>& renderTargets, Renderer::Core::RenderPass* pRenderPass)
	{
		VulkanFramebuffer* pFramebuffer = HvNew VulkanFramebuffer();
		b8 res = pFramebuffer->Create(m_pContext, renderTargets, pRenderPass);
		if (!res)
		{
			pFramebuffer->Destroy();
			HvDelete pFramebuffer;
			return nullptr;
		}
		return pFramebuffer;
	}

	b8 VulkanDynamicRHI::DestroyFramebuffer(Renderer::Core::Framebuffer* pFramebuffer)
	{
		b8 res = pFramebuffer->Destroy();
		HvDelete pFramebuffer;
		return res;
	}

	Renderer::Core::Sampler* VulkanDynamicRHI::CreateSampler(const Renderer::Core::SamplerDesc& desc)
	{
		VulkanSampler* pSampler = HvNew VulkanSampler();
		b8 res = pSampler->Create(m_pContext, desc);
		if (!res)
		{
			pSampler->Destroy();
			HvDelete pSampler;
			return nullptr;
		}
		return pSampler;
	}

	b8 VulkanDynamicRHI::DestroySampler(Renderer::Core::Sampler* pSampler)
	{
		b8 res = pSampler->Destroy();
		HvDelete pSampler;
		return res;
	}

	Renderer::Core::Texture* VulkanDynamicRHI::CreateTexture(const Renderer::Core::TextureDesc& desc, Renderer::Core::CommandList* pCommandList)
	{
		VulkanTexture* pTexture = HvNew VulkanTexture();
		b8 res = pTexture->Create(m_pContext, desc, pCommandList);
		if (!res)
		{
			pTexture->Destroy();
			HvDelete pTexture;
			return nullptr;
		}
		return pTexture;
	}

	b8 VulkanDynamicRHI::DestroyTexture(Renderer::Core::Texture* pTexture)
	{
		b8 res = pTexture->Destroy();
		HvDelete pTexture;
		return res;
	}

	Renderer::Core::RenderTarget* VulkanDynamicRHI::CreateRenderTarget(const Renderer::Core::RenderTargetDesc& desc)
	{
		VulkanRenderTarget* pRT = HvNew VulkanRenderTarget();
		b8 res = pRT->Create(m_pContext, desc);
		if (!res)
		{
			pRT->Destroy();
			HvDelete pRT;
			return nullptr;
		}
		return pRT;
	}

	b8 VulkanDynamicRHI::DestroyRenderTarget(Renderer::Core::RenderTarget* pRenderTarget)
	{
		b8 res = pRenderTarget->Destroy();
		HvDelete pRenderTarget;
		return res;
	}

	////////////////////////////////////////////////////////////////////////////////
	// Buffers																	  //
	////////////////////////////////////////////////////////////////////////////////
	Renderer::Core::Buffer* VulkanDynamicRHI::CreateBuffer(Renderer::BufferType type, u64 size, Renderer::BufferFlags flags)
	{
		VulkanBuffer* pBuffer = HvNew VulkanBuffer();
		b8 res = pBuffer->Create(m_pContext, type, size, flags);
		if (!res)
		{
			HvDelete pBuffer;
			g_Logger.LogError("Failed to create buffer");
			return nullptr;
		}
		return pBuffer;
	}

	Renderer::Core::Buffer* VulkanDynamicRHI::CreateVertexBuffer(u32 vertexCount, u16 vertexSize, Renderer::BufferFlags flags)
	{
		VulkanBuffer* pBuffer = HvNew VulkanBuffer();
		b8 res = pBuffer->CreateVertexBuffer(m_pContext, vertexCount, vertexSize, flags);
		if (!res)
		{
			HvDelete pBuffer;
			g_Logger.LogError("Failed to create buffer");
			return nullptr;
		}
		return pBuffer;
	}

	Renderer::Core::Buffer* VulkanDynamicRHI::CreateIndexBuffer(u32 indexCount, Renderer::IndexType indexType, Renderer::BufferFlags flags)
	{
		VulkanBuffer* pBuffer = HvNew VulkanBuffer();
		b8 res = pBuffer->CreateIndexBuffer(m_pContext, indexCount, indexType, flags);
		if (!res)
		{
			HvDelete pBuffer;
			g_Logger.LogError("Failed to create buffer");
			return nullptr;
		}
		return pBuffer;
	}

	b8 VulkanDynamicRHI::DestroyBuffer(Renderer::Core::Buffer* pBuffer)
	{
		if (!pBuffer)
		{
			g_Logger.LogError("Buffer can't be a nullptr!");
			return false;
		}
		b8 res = pBuffer->Destroy();
		if (!res)
		{
			g_Logger.LogError("Failed to destroy buffer!");
			return false;
		}
		HvDelete pBuffer;
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////
	// Other																	  //
	////////////////////////////////////////////////////////////////////////////////
	b8 VulkanDynamicRHI::WaitIdle()
	{
		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
		return pDevice->WaitIdle();
	}
}

Hv::RHI::IDynamicRHI* LoadDynamicRHI()
{
	return HvNew Hv::VulkanRHI::VulkanDynamicRHI();
}
