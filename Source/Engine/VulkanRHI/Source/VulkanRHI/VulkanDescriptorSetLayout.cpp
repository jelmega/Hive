// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanDescriptorSetLayout.cpp: Vulkan descriptor set layout
#include "VulkanRHIPCH.h"
#include "VulkanRHI/VulkanDescriptorSetLayout.h"
#include "VulkanRHI/VulkanHelpers.h"
#include "VulkanRHI/VulkanContext.h"
#include "VulkanRHI/VulkanDevice.h"

namespace Hv::VulkanRHI {

	VulkanDescriptorSetLayout::VulkanDescriptorSetLayout()
	{
	}

	VulkanDescriptorSetLayout::~VulkanDescriptorSetLayout()
	{
	}

	b8 VulkanDescriptorSetLayout::Create(RHI::RHIContext* pContext, Renderer::Core::DescriptorSetManager* pManager,
		const DynArray<Renderer::Core::DescriptorSetBinding>& bindings)
	{
		m_pContext = pContext;
		m_pManager = pManager;
		m_Bindings = bindings;

		VkDescriptorSetLayoutCreateInfo layoutInfo = {};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		// flags???

		DynArray<VkDescriptorSetLayoutBinding> vulkanBindings;
		vulkanBindings.Reserve(bindings.Size());

		for (u32 i = 0; i < bindings.Size(); ++i)
		{
			const Renderer::Core::DescriptorSetBinding& binding = bindings[i];
			VkDescriptorSetLayoutBinding vulkanBinding = {};
			vulkanBinding.binding = i;
			vulkanBinding.descriptorCount = binding.count;
			vulkanBinding.descriptorType = Helpers::GetDescriptorType(binding.type);
			vulkanBinding.stageFlags = Helpers::GetShaderStage(binding.shadertype);
			// TODO: pImmutableSamplers?

			vulkanBindings.Push(vulkanBinding);
		}
		layoutInfo.bindingCount = u32(vulkanBindings.Size());
		layoutInfo.pBindings = vulkanBindings.Data();

		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();
		VkResult vkres = pDevice->vkCreateDescriptorSetLayout(layoutInfo, m_Layout);
		if (vkres != VK_SUCCESS)
		{
			g_Logger.LogFormat(LogVulkanRHI(), LogLevel::Fatal, "Failed to create the vulkan descriptor set layout (VkResult: %s)!", Helpers::GetResultString(vkres));
			return false;
		}

		return true;
	}

	b8 VulkanDescriptorSetLayout::Destroy()
	{
		VulkanDevice* pDevice = ((VulkanContext*)m_pContext)->GetDevice();

		if (m_Layout)
		{
			pDevice->vkDestroyDescriptorSetLayout(m_Layout);
			m_Layout = VK_NULL_HANDLE;
		}

		return true;
	}
}
