// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanRHIPCH.h: Vulkan RHI Precompiled header
#pragma once
#include <RendererCore.h>
#define VK_NO_PROTOTYPES

#if HV_PLATFORM_WINDOWS
#	define VK_USE_PLATFORM_WIN32_KHR
#else
#	error	Vulkan surface not supported yet for your OS
#endif

#include <vulkan/vulkan.h>

HV_DECLARE_LOG_CATEGORY_EXTERN(VulkanRHI)