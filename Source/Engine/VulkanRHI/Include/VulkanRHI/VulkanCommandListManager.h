// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanCommandListManager.h: Vulkan command list manager
#pragma once
#include "VulkanRHIPCH.h"

namespace Hv::VulkanRHI {
	
	class VulkanCommandListManager final : public Renderer::Core::CommandListManager
	{
	public:
		VulkanCommandListManager();
		~VulkanCommandListManager();

		/**
		* Create a command list manager
		* @param[in] pContext	RHI context
		* @return				True if the command list manager was created successfully, false otherwise
		*/
		b8 Create(RHI::RHIContext* pContext) override final;
		/**
		* Destroy the command list manager
		* @return				True if the command list manager was created successfully, false otherwise
		*/
		b8 Destroy() override final;

		/**
		* Create a command list for a certain queue
		* @param[in] pQueue	Queue
		* @return				Pointer to a command buffer, nullptr if creation failed
		*/
		Renderer::Core::CommandList* CreateCommandList(Renderer::Core::Queue* pQueue) override final;
		/**
		* Destroy a command list
		* @param[in] pCommandList	Command list to destroy
		* @return					True if the command list was successfully destroyed, false otherwise
		*/
		b8 DestroyCommandList(Renderer::Core::CommandList* pCommandList) override final;

		/**
		 * Create and begin a single time command list for a certain queue
		 * @param[in] pQueue	Queue
		 * @return				Pointer to a single time command buffer, nullptr if creation failed
		 */
		Renderer::Core::CommandList* CreateSingleTimeCommandList(Renderer::Core::Queue* pQueue) override final;
		/**
		 * End and estroy a singe time command list
		 * @param[in] pCommandList	Single time command list
		 * @return					True if the single time command list was successfully destroyed, false otherwise
		 */
		b8 EndSingleTimeCommandList(Renderer::Core::CommandList* pCommandList) override final;

		/**
		 * Get the command pool associated with a queue
		 * @param[in] pQueue	Queue
		 * @return				Vulkan command pool
		 */
		VkCommandPool GetCommandPool(Renderer::Core::Queue* pQueue);

	private:
		DynArray<VkCommandPool> m_CommandPools;
	};

}
