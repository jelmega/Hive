// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanRenderPass.h: render pass
#pragma once
#include "VulkanRHIPCH.h"
#include "Renderer/Core/RenderPass.h"

namespace Hv::VulkanRHI {
	
	class HIVE_API VulkanRenderPass final : public Renderer::Core::RenderPass
	{
	private:

		struct SubpassDescription
		{
			DynArray<VkAttachmentReference> colorAttachments;
			DynArray<VkAttachmentReference> resolveAttachments;
			VkAttachmentReference depthAttachment;
			DynArray<VkAttachmentReference> inputAttachments;
			DynArray<u32> preserveAttachments;
			VkSubpassDescription subpassDesc;
		};

	public:
		VulkanRenderPass();
		~VulkanRenderPass();

		/**
		 * Create a render pass
		 * @param[in] pContext		RHI context
		 * @param[in] attachments	Attachments
		 * @param[in] subpasses		Sub passes
		 * @return					True if the renderpass was created successfully, false otherwise
		 */
		b8 Create(RHI::RHIContext* pContext, const DynArray<Renderer::Core::RenderPassAttachment>& attachments, const DynArray<Renderer::Core::SubRenderPass>& subpasses) override final;

		/**
		 * Destroy a renderpass
		 * @return	True if the renderpass was desstroyed successfully, false otherwise
		 */
		b8 Destroy() override final;

		/**
		 * Get the vulkan render pass
		 * @return	Vulkan render pass
		 */
		VkRenderPass GetRenderPass() { return m_RenderPass; }

	private:
		VkRenderPass m_RenderPass;	/**< Vulkan render pass */
	};

}