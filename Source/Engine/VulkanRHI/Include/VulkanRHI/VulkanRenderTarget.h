// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanRenderTarget.h: Vulkan render target
#pragma once
#include "VulkanRHIPCH.h"

namespace Hv::VulkanRHI {

	// TODO: complete class
	class HIVE_API VulkanRenderTarget final : public Renderer::Core::RenderTarget
	{
	public:

		VulkanRenderTarget();
		~VulkanRenderTarget();

		/**
		 * Create a render target
		 * @param[in] pContext	RHI context
		 * @param[in] desc		Render target description
		 * @return				True if the texture was created successfully, false otherwise
		 */
		b8 Create(RHI::RHIContext* pContext, const Renderer::Core::RenderTargetDesc& desc) override final;
		/**
		 * Create a render target from a vulkan image
		 * @param[in] pContext	RHI context
		 * @param[in] image		Vulkan image
		 * @param[in] layout	Vulkan image layout
		 * @param[in] width		Width
		 * @param[in] height	Height
		 * @param[in] format	Format
		 * @param[in] samples	Sample count
		 * @param[in] type		Render target type
		 * @return				True if the texture was created successfully, false otherwise
		 */
		b8 Create(RHI::RHIContext* pContext, VkImage image, VkImageLayout layout, u32 width, u32 height, PixelFormat format, Renderer::SampleCount samples, Renderer::RenderTargetType type);

		/**
		 * Destroy the texture
		 */
		b8 Destroy() override final;

	private:

	};

}
