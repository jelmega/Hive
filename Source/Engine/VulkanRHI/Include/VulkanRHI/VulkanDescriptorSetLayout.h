// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanDescriptorSetLayout.h: Vulkan descriptor set layout
#pragma once
#include "VulkanRHIPCH.h"
#include "Renderer/Core/DescriptorSetLayout.h"
#include "VulkanDescriptorSet.h"

namespace Hv::VulkanRHI {
	
	class HIVE_API VulkanDescriptorSetLayout : public Renderer::Core::DescriptorSetLayout
	{
	public:
		VulkanDescriptorSetLayout();
		~VulkanDescriptorSetLayout();

		/**
		 * Create the descriptor set
		 * @param[in] pContext	RHI context
		 * @param[in] pManager	Descriptor set manager
		 * @param[in] bindings	Descriptor set bindings
		 * @return				True if the descriptor set was created successfully, false otherwise
		 */
		virtual b8 Create(RHI::RHIContext* pContext, Renderer::Core::DescriptorSetManager* pManager, const DynArray<Renderer::Core::DescriptorSetBinding>& bindings);
		/**
		 * Destroy the descriptor set
		 * @return	True if the descriptor set was destroyed successfully, false otherwise
		 */
		virtual b8 Destroy();

		/**
		 * Get the vulkan descriptor set layout
		 * @return	Vulkan descriptor set layout
		 */
		VkDescriptorSetLayout GetLayout() { return m_Layout; }

	private:
		VkDescriptorSetLayout m_Layout;
	};

}
