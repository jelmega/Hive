#pragma once
#include "VulkanRHIPCH.h"

#define VKFUNC(name) PFN_vk##name name

namespace Hv::VulkanRHI {
	
	struct VulkanInstanceFuncs
	{
		// Instance independent
		VKFUNC(GetInstanceProcAddr);
		VKFUNC(CreateInstance);
		VKFUNC(EnumerateInstanceExtensionProperties);
		VKFUNC(EnumerateInstanceLayerProperties);
		VKFUNC(EnumerateInstanceVersion);

		// Instance
		VKFUNC(DestroyInstance);

		// Physical device
		VKFUNC(EnumeratePhysicalDevices);
	};

	struct VulkanInstanceExtFuncs
	{
		// VK_EXT_debug_report
		VKFUNC(CreateDebugReportCallbackEXT);
		VKFUNC(DestroyDebugReportCallbackEXT);

		// VK_KHR_surface
#if defined(VK_USE_PLATFORM_WIN32_KHR)
		VKFUNC(CreateWin32SurfaceKHR);
#else
#endif
		VKFUNC(DestroySurfaceKHR);
	};

	struct VulkanPhysicalDeviceFuncs
	{
		// Physical device
		VKFUNC(GetPhysicalDeviceFeatures);
		VKFUNC(GetPhysicalDeviceFormatProperties);
		VKFUNC(GetPhysicalDeviceImageFormatProperties);
		VKFUNC(GetPhysicalDeviceProperties);
		VKFUNC(GetPhysicalDeviceQueueFamilyProperties);
		VKFUNC(GetPhysicalDeviceMemoryProperties);
		VKFUNC(GetPhysicalDeviceSparseImageFormatProperties);

		// Surface support
		VKFUNC(GetPhysicalDeviceSurfaceSupportKHR);
		VKFUNC(GetPhysicalDeviceSurfacePresentModesKHR);
		VKFUNC(GetPhysicalDeviceSurfaceFormatsKHR);
		VKFUNC(GetPhysicalDeviceSurfaceCapabilitiesKHR);

		// Device extensions
		VKFUNC(EnumerateDeviceExtensionProperties);
		VKFUNC(EnumerateDeviceLayerProperties);
	};

	struct VulkanDeviceFunctions
	{
		// General
		VKFUNC(GetDeviceProcAddr);
		VKFUNC(CreateDevice);
		VKFUNC(DestroyDevice);
		VKFUNC(DeviceWaitIdle);

		// Queue
		VKFUNC(GetDeviceQueue);

		// Image
		VKFUNC(CreateImage);
		VKFUNC(DestroyImage);
		VKFUNC(CreateImageView);
		VKFUNC(DestroyImageView);
		VKFUNC(GetImageMemoryRequirements);
		VKFUNC(BindImageMemory);

		// Sampler
		VKFUNC(CreateSampler);
		VKFUNC(DestroySampler);

		// Shader module
		VKFUNC(CreateShaderModule);
		VKFUNC(DestroyShaderModule);

		// Pipeline
		VKFUNC(CreatePipelineLayout);
		VKFUNC(DestroyPipelineLayout);
		VKFUNC(CreateGraphicsPipelines);
		VKFUNC(CreateComputePipelines);
		VKFUNC(DestroyPipeline);

		// Renderpass
		VKFUNC(CreateRenderPass);
		VKFUNC(DestroyRenderPass);

		// Framebuffer
		VKFUNC(CreateFramebuffer);
		VKFUNC(DestroyFramebuffer);

		// Command pool
		VKFUNC(CreateCommandPool);
		VKFUNC(DestroyCommandPool);

		// Command buffer
		VKFUNC(AllocateCommandBuffers);
		VKFUNC(FreeCommandBuffers);

		// Memory
		VKFUNC(AllocateMemory);
		VKFUNC(FreeMemory);
		VKFUNC(MapMemory);
		VKFUNC(UnmapMemory);
		VKFUNC(FlushMappedMemoryRanges);
		VKFUNC(InvalidateMappedMemoryRanges);

		// Buffers
		VKFUNC(CreateBuffer);
		VKFUNC(DestroyBuffer);
		VKFUNC(CreateBufferView);
		VKFUNC(DestroyBufferView);
		VKFUNC(GetBufferMemoryRequirements);
		VKFUNC(BindBufferMemory);
		 
		// Descriptor set
		VKFUNC(CreateDescriptorSetLayout);
		VKFUNC(DestroyDescriptorSetLayout);
		VKFUNC(CreateDescriptorPool);
		VKFUNC(DestroyDescriptorPool);
		VKFUNC(AllocateDescriptorSets);
		VKFUNC(FreeDescriptorSets);
		VKFUNC(UpdateDescriptorSets);

		// Semaphore
		VKFUNC(CreateSemaphore);
		VKFUNC(DestroySemaphore);

		// Fence
		VKFUNC(CreateFence);
		VKFUNC(DestroyFence);
		VKFUNC(ResetFences);
		VKFUNC(WaitForFences);
		VKFUNC(GetFenceStatus);
	};

	struct VulkanDeviceExtFunctions
	{
		// VK_KHR_swapchain
		VKFUNC(CreateSwapchainKHR);
		VKFUNC(DestroySwapchainKHR);
		VKFUNC(GetSwapchainImagesKHR);
		VKFUNC(AcquireNextImageKHR);
	};

	struct VulkanQueueFunctions
	{
		VKFUNC(QueueWaitIdle);
		VKFUNC(QueueSubmit);
		VKFUNC(QueuePresentKHR);
	};

	struct VulkanCommandBufferFunctions
	{
		VKFUNC(BeginCommandBuffer);
		VKFUNC(EndCommandBuffer);

		VKFUNC(CmdBindPipeline);
		VKFUNC(CmdBindDescriptorSets);
		VKFUNC(CmdBindVertexBuffers);
		VKFUNC(CmdBindIndexBuffer);

		VKFUNC(CmdBeginRenderPass);
		VKFUNC(CmdEndRenderPass);

		VKFUNC(CmdSetViewport);
		VKFUNC(CmdSetScissor);

		VKFUNC(CmdDraw);
		VKFUNC(CmdDrawIndexed);
		VKFUNC(CmdDrawIndirect);

		VKFUNC(CmdCopyBuffer);
		VKFUNC(CmdCopyImage);
		VKFUNC(CmdCopyBufferToImage);
		VKFUNC(CmdCopyImageToBuffer);

		VKFUNC(CmdPipelineBarrier);


	};
}

#undef VK_FUNC
