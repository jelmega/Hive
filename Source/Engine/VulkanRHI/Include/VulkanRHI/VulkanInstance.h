// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanInstance.h: Vulkan Instance
#pragma once
#include "VulkanRHIPCH.h"
#include "VulkanFunctions.h"

#pragma warning(push)
#pragma warning(disable: 4251) // class '...' needs to have dll-interface to be used by clients of class '...'

namespace Hv::VulkanRHI {

	class VulkanContext;
	class VulkanPhysicalDevice;

	class HIVE_API VulkanInstance
	{
	public:

		VulkanInstance();
		~VulkanInstance();

		/**
		 * Initialize the instance
		 * @param[in] pContext				Vulkan context
		 * @param[in] requestedExtensions	Requested extensions to create the instance with
		 * @param[in] requestedLayers		Requested layers to create the instance with
		 * @return							True if the instance initialized successfully
		 */
		VkResult Init(VulkanContext* pContext, const DynArray<const AnsiChar*>& requestedExtensions, const DynArray<const AnsiChar*>& requestedLayers);
		/**
		 * Destroy the instance
		 */
		void Destroy();

		/**
		 * Check if an instance extension is available
		 * @param[in] extension		Name of the extension to check
		 * @return					True if the extension is available, false otherwise
		 */
		b8 IsExtensionAvailable(const String& extension);
		/**
		 * Check if an instance layer is available
		 * @param[in] layer		Name of the layer to check
		 * @return				True if the layer is available, false otherwise
		 */
		b8 IsLayerAvailable(const String& layer);
		/**
		 * Check if an instance extension is available
		 * @param[in] extension		Name of the extension to check
		 * @return					True if the extension is available, false otherwise
		 */
		b8 IsExtensionEnabled(const String& extension);
		/**
		 * Check if an instance layer is available
		 * @param[in] layer		Name of the layer to check
		 * @return				True if the layer is available, false otherwise
		 */
		b8 IsLayerEnabled(const String& layer);

		/**
		 * Log the available extensions to the console
		 */
		void LogAvailableExtensions();
		/**
		 * Log the available layers to the console
		 */
		void LogAvailableLayers();

		/**
		 * Enumerate the vulkan devices
		 * @param[out] physicalDevices	Physical devices
		 * @return						Vulkan result
		 */
		VkResult EnumeratePhysicalDevices(DynArray<VulkanPhysicalDevice*>& physicalDevices) const;

		/**
		 * Create a debug report callback
		 * @param[in] pUserData		User data
		 * @param[in] pfnCallback	Debug callback
		 * @param[in] level			Validation level
		 * @return					Vulkan result
		 */
		VkResult CreateDebugReportCallback(void* pUserData, PFN_vkDebugReportCallbackEXT pfnCallback, Renderer::RHIValidationLevel level);
		/**
		 * Destroy the debug report callback
		 */
		void DestroyDebugReportCallback();

		/**
		 * Get the vulkan allocation callbacks
		 * @return	Vulkan allocation callbacks
		 */
		VkAllocationCallbacks* GetAllocationCallbacks() { return m_pAllocCallbacks; }

		/**
		 * Get the vulkan instance
		 * @return	Vulkan instance
		 */
		VkInstance GetInstance() { return m_Instance; }

		////////////////////////////////////////////////////////////////////////////////
#ifdef VK_USE_PLATFORM_WIN32_KHR
		/**
		* Create a win32 vk surface
		* @param[in] createInfo	Create info
		* @param[out] surface		Vulkan surface
		* @return					Vulkan result
		*/
		VkResult vkCreateSurface(const VkWin32SurfaceCreateInfoKHR& createInfo, VkSurfaceKHR& surface);
#else
#endif
		/**
		* Destroy a vk surface
		* @param[in] surface	Surface
		*/
		void vkDestroySurface(VkSurfaceKHR surface);

	private:

		/**
		 * Load the 'free' vulkan functions
		 */
		void LoadFunctions();
		/**
		 * Load the instance dependent vulkan function
		 */
		void LoadInstanceFunctions();

		VulkanInstanceFuncs m_Vk;								/**< Vulkan instance functions */
		VulkanInstanceExtFuncs m_VkExt;							/**< Vulkan instance extension functions */
		VulkanPhysicalDeviceFuncs m_VkPhysicalDeviceFuncs;		/**< Vulkan physical device functions */
		VulkanContext* m_pContext;								/**< Vulkan context */

		VkAllocationCallbacks* m_pAllocCallbacks;				/**< Allocation callbacks */
		VkInstance m_Instance;									/**< Vk instance */
		DynArray<VkExtensionProperties> m_AvailableExtensions;	/**< Available vulkan extensions */
		DynArray<const AnsiChar*> m_EnabledExtensions;			/**< Enabled vulkan extensions */
		DynArray<VkLayerProperties> m_AvailableLayers;			/**< Available vulkan layers */
		DynArray<const AnsiChar*> m_EnabledLayers;				/**< Enabled vulkan layers */
		u32 m_VulkanApiVersion;									/**< Vulkan version */

		VkDebugReportCallbackEXT m_DebugReportCallback;			/**< Vulkan debug report callback */
	};

}

#pragma warning(pop)
