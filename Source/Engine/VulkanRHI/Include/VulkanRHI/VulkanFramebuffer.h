// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanFramebuffer.h: Vulkan framebuffer
#pragma once
#include "VulkanRHIPCH.h"

namespace Hv::VulkanRHI {
	
	class HIVE_API VulkanFramebuffer final : public Renderer::Core::Framebuffer
	{
	public:
		VulkanFramebuffer();
		~VulkanFramebuffer();

		/**
		* Create the frame buffer
		* @param[in] pContext			RHI context
		* @param[in] renderTargets		Render targets
		* @param[in] pRenderPass		Associated render pass
		* @return						True if the framebuffer was created successfully, false otherwise
		*/
		b8 Create(RHI::RHIContext* pContext, const DynArray<Renderer::Core::RenderTarget*>& renderTargets, Renderer::Core::RenderPass* pRenderPass) override final;

		/**
		* Destroy the frame buffer
		* @return	True if the framebuffer was destroyed successfully, false otherwise
		*/
		b8 Destroy() override final;

		/**<
		 * Get the vulkan framebuffer
		 * @return	Vulkan framebuffer
		 */
		VkFramebuffer GetFrameBuffer() { return m_FrameBuffer; }

	private:
		VkFramebuffer m_FrameBuffer;
	};

}