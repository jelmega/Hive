// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanDescriptorSetManager.h: Vulkan descriptor set manager
#pragma once
#include "VulkanRHIPCH.h"
#include "Renderer/Core/DescriptorSetManager.h"

namespace Hv::VulkanRHI {
	
	class HIVE_API VulkanDescriptorSetManager final : public Renderer::Core::DescriptorSetManager
	{
	public:
		VulkanDescriptorSetManager();
		~VulkanDescriptorSetManager();

		/**
		 * Create the descriptor set manager
		 * @param[in] pContext	RHI context
		 * @return				True if the descriptor set manager was created successfully, false otherwise
		 */
		b8 Create(RHI::RHIContext* pContext) override final;
		/**
		 * Create the descriptor set manager
		 * @return	True if the descriptor set manager was destroyed successfully, false otherwise
		 */
		b8 Destroy() override final;

		/**
		* Create a descriptor set
		* @param[in] bindings	Descriptor set bindings
		* @return				Pointer to a descriptor set, nullptr if the creation failed
		*/
		Renderer::Core::DescriptorSetLayout* CreateDescriptorSetLayout(const DynArray<Renderer::Core::DescriptorSetBinding>& bindings) override final;
		/**
		* Create a descriptor set
		* @param[in] pLayout	Descriptor set layout
		* @return				Pointer to a descriptor set, nullptr if the creation failed
		*/
		Renderer::Core::DescriptorSet* CreateDescriptorSet(Renderer::Core::DescriptorSetLayout* pLayout) override final;
		/**
		* Destroy a descriptor set
		* @param[in] pLayout	Descriptor set layout to destroy
		* @return				True if the descriptor set layout was destroyed successfully, false otherwise
		*/
		b8 DestroyDescriptorSetLayout(Renderer::Core::DescriptorSetLayout* pLayout) override final;
		/**
		* Destroy a descriptor set
		* @param[in] pDescriptorSet	Descriptor set to destroy
		* @return						True if the descriptor set was destroyed successfully, false otherwise
		*/
		b8 DestroyDescriptorSet(Renderer::Core::DescriptorSet* pDescriptorSet) override final;

		/**
		 * Get the vulkan descriptor pool
		 * @return	Vulkan descriptor pool
		 */
		VkDescriptorPool GetDescriptorPool() { return m_DescriptorPool; }

	private:
		VkDescriptorPool m_DescriptorPool;
	};

}
