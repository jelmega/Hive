// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanSampler.h: Vulkan sampler
#pragma once
#include "VulkanRHIPCH.h"
#include "Renderer/Core/Sampler.h"

namespace Hv::VulkanRHI {
	
	class HIVE_API VulkanSampler : public Renderer::Core::Sampler
	{
	public:
		VulkanSampler();
		~VulkanSampler();

		/**
		 * Create the sampler
		 * @param[in] pContext	RHI context
		 * @param[in] desc		Sampler description
		 * @return				True if the sampler was created successfully, false otherwise
		 */
		b8 Create(RHI::RHIContext* pContext, const Renderer::Core::SamplerDesc& desc) override final;
		/**
		 * Create the sampler
		 * @return	True if the sampler was destroyed successfully, false otherwise
		 */
		b8 Destroy() override final;

		/**
		* Get the vulkan sampler
		* @return	Vulkan sampler
		*/
		VkSampler GetSampler() { return m_Sampler; }

	private:
		VkSampler m_Sampler;
	};

}

