// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanSemaphore.h: Vulkan semaphore
#pragma once
#include "VulkanRHIPCH.h"

namespace Hv::VulkanRHI {
	
	class HIVE_API VulkanSemaphore final : public Renderer::Core::Semaphore
	{
	public:
		VulkanSemaphore();
		~VulkanSemaphore();

		/**
		 * Create the semaphore
		 * @return	True if the semaphore was created successfully, false otherwise
		 */
		b8 Create(RHI::RHIContext* pContext) override final;
		/**
		 * Destroy the semaphore
		 * @return	True if the semaphore was destroyed successfully, false otherwise
		 */
		b8 Destroy() override final;

		/**
		 * Get the vulkan semaphore
		 * @return	Vulkan semaphore
		 */
		VkSemaphore GetSemaphore() { return m_Semaphore; }

	private:
		VkSemaphore m_Semaphore;
	};

}
