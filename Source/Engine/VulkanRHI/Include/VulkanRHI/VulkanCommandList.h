// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanCommandList.h: Vulkan command list
#pragma once
#include "VulkanRHIPCH.h"
#include "VulkanFunctions.h"
#include "VulkanMemory.h"

namespace Hv::VulkanRHI {
	
	class VulkanCommandList final : public Renderer::Core::CommandList
	{
	public:
		VulkanCommandList();
		~VulkanCommandList();

		/**
		 * Begin the command buffer
		 * @return	True if the command list was begin successfully, false otherwise
		 */
		b8 Begin() override final;
		/**
		 * End the command buffer
		 * @return	True if the command list was ended successfully, false otherwise
		 */
		b8 End() override final;

		/**
		 * Bind a pipeline
		 * @param[in] pPipeline	Pipeline to bind
		 */
		void BindPipeline(Renderer::Core::Pipeline* pPipeline) override final;

		/**
		 * Begin a render pass
		 * @param[in] pRenderPass	Render pass to begin
		 * @param[in] pFramebuffer	Framebuffer
		 */
		void BeginRenderPass(Renderer::Core::RenderPass* pRenderPass, Renderer::Core::Framebuffer* pFramebuffer) override final;
		/**
		 * End the current render pass
		 */
		void EndRenderPass() override final;

		/**
		* Bind a vertex buffer
		* @param[in] inputSlot		Input slot of the buffer
		* @param[in] pBuffer		Vertex buffer to bind
		* @param[in] offset		Offset in buffer
		*/
		void BindVertexBuffer(u16 inputSlot, Renderer::Core::Buffer* pBuffer, u64 offset) override final;
		/**
		* Bind multiple vertex buffers
		* @param[in] inputSlot		Input slot of the first buffer
		* @param[in] buffers		Vertex buffer to bind
		* @param[in] offsets	s	Offset in buffer
		*/
		void BindVertexBuffer(u16 inputSlot, const DynArray<Renderer::Core::Buffer*>& buffers, const DynArray<u64>& offsets) override final;
		/**
		 * Bind a vertex buffer
		 * @param[in] pBuffer		Vertex buffer to bind
		 * @param[in] offset		Offset in buffer
		 */
		void BindIndexBuffer(Renderer::Core::Buffer* pBuffer, u64 offset) override final;
		/**
		 * Bind a vertex buffer
		 * @param[in] pBuffer		Vertex buffer to bind
		 * @param[in] offset		Offset in buffer
		 * @param[in] type			Index type
		 * @note					Use this function when using a combined buffer (vertices, indices, etc. in same buffer)
		 */
		void BindIndexBuffer(Renderer::Core::Buffer* pBuffer, u64 offset, Renderer::IndexType type) override final;
		/**
		 * Bind a descriptor set
		 * @param[in] firstSet	Index of first descriptor set
		 * @param[in] pSet		Descriptor set
		 * @note				Use this function when using a combined buffer (vertices, indices, etc. in same buffer)
		 */
		void BindDescriptorSets(u32 firstSet, Renderer::Core::DescriptorSet* pSet) override final;
		/**
		 * Bind a descriptor set
		 * @param[in] firstSet			Index of first descriptor set
		 * @param[in] pSet				Descriptor set
		 * @param[in] dynamicOffset		Dynamic offset (for dynamic descriptor set type)
		 * @note						Use this function when using a combined buffer (vertices, indices, etc. in same buffer)
		 */
		void BindDescriptorSets(u32 firstSet, Renderer::Core::DescriptorSet* pSet, u32 dynamicOffset) override final;
		/**
		 * Bind descriptor sets
		 * @param[in] firstSet			Index of first descriptor set
		 * @param[in] sets				Descriptor sets
		 * @note						Use this function when using a combined buffer (vertices, indices, etc. in same buffer)
		 */
		void BindDescriptorSets(u32 firstSet, const DynArray<Renderer::Core::DescriptorSet*>& sets) override final;
		/**
		 * Bind descriptor sets
		 * @param[in] firstSet			Index of first descriptor set
		 * @param[in] sets				Descriptor sets
		 * @param[in] dynamicOffsets	Dynamic offsets (for dynamic descriptor set types)
		 * @note						Use this function when using a combined buffer (vertices, indices, etc. in same buffer)
		 */
		void BindDescriptorSets(u32 firstSet, const DynArray<Renderer::Core::DescriptorSet*>& sets, const DynArray<u32>& dynamicOffsets) override final;

		/**
		 * Set the viewport
		 * @param[in] viewport	Viewport
		 */
		void SetViewport(Renderer::Viewport& viewport) override final;
		/**
		 * Set the scissor rect
		 * @param[in] scissor	Scissor rect
		 */
		void SetScissor(Renderer::ScissorRect& scissor) override final;

		/**
		 * Draw a number of vertices and instances
		 * @param[in] vertexCount		Amount of vertices to draw
		 * @param[in] instanceCount		Amount of instances to draw
		 * @param[in] firstVertex		Index of vertex to start drawing from
		 * @param[in] firstInstance		Index of instance to start drawing from
		 */
		void Draw(u32 vertexCount, u32 instanceCount = 1, u32 firstVertex = 0, u32 firstInstance = 0) override final;
		/**
		 * Draw a number of vertices and instances (indexed)
		 * @param[in] indexCount		Amount of indices to draw
		 * @param[in] instanceCount		Amount of instances to draw
		 * @param[in] firstIndex		Index of index to start drawing from
		 * @param[in] vertexOffset		Offset in vertex buffer (index n == vertex at offset + n)
		 * @param[in] firstInstance		Index of instance to start drawing from
		 */
		void DrawIndexed(u32 indexCount, u32 instanceCount = 1, u32 firstIndex = 0, u32 vertexOffset = 0, u32 firstInstance = 0) override final;

		/**
		* Copy data from one buffer to another
		* @param[in] pSrcBuffer	Buffer to copy from
		* @param[in] srcOffset		Offset in source buffer
		* @param[in] pDstBuffer	Buffer to copy to
		* @param[in] dstOffset		Offset in destination buffer
		* @param[in] size			Amount of bytes to copy
		* @note					While it's possible to use this function, it is preferable to use the buffer copy function, since this functions doesn't do additional checks on the buffers
		*							(make sure you know what you are doing!)
		*/
		void CopyBuffer(Renderer::Core::Buffer* pSrcBuffer, u64 srcOffset, Renderer::Core::Buffer* pDstBuffer, u64 dstOffset, u64 size) override final;
		/**
		 * Copy data from one buffer to another
		 * @param[in] pSrcBuffer	Buffer to copy from
		 * @param[in] pDstBuffer	Buffer to copy to
		 * @param[in] regions		Buffer copy regions
		 * @note					While it's possible to use this function, it is preferable to use the buffer copy function, since this functions doesn't do additional checks on the buffers
		 *							(make sure you know what you are doing!)
		 */
		void CopyBuffer(Renderer::Core::Buffer* pSrcBuffer, Renderer::Core::Buffer* pDstBuffer, const DynArray<Renderer::BufferCopyRegion>& regions) override final;
		/**
		 * Copy data from one buffer to another
		 *@param[in] pSrcTex		Texture to copy from
		 * @param[in] pDstTex		Texture to copy to
		 * @param[in] region		Texture copy region
		 * @note					While it's possible to use this function, it is preferable to use the texture copy function, since this functions doesn't do additional checks on the buffers
		 *							(make sure you know what you are doing!)
		 */
		void CopyTexture(Renderer::Core::Texture* pSrcTex, Renderer::Core::Texture* pDstTex, const Renderer::TextureCopyRegion& region) override final;
		/**
		 * Copy data from one buffer to another
		 * @param[in] pSrcTex		Texture to copy from
		 * @param[in] pDstTex		Texture to copy to
		 * @param[in] regions		Texture copy regions
		 * @note					While it's possible to use this function, it is preferable to use the texture copy function, since this functions doesn't do additional checks on the buffers
		 *							(make sure you know what you are doing!)
		 */
		void CopyTexture(Renderer::Core::Texture* pSrcTex, Renderer::Core::Texture* pDstTex, const DynArray<Renderer::TextureCopyRegion>& regions) override final;
		/**
		 * Copy data from one buffer to another
		 * @param[in] pBuffer		Buffer to copy from
		 * @param[in] pTexture		Texture to copy to
		 * @param[in] region		Copy region
		 * @note					While it's possible to use this function, it is preferable to use the texure copy function, since this functions doesn't do additional checks on the buffers
		 *							(make sure you know what you are doing!)
		 */
		void CopyBufferToTexture(Renderer::Core::Buffer* pBuffer, Renderer::Core::Texture* pTexture, const Renderer::TextureBufferCopyRegion& region) override final;
		/**
		 * Copy data from one buffer to another
		 * @param[in] pBuffer		Buffer to copy from
		 * @param[in] pTexture		Texture to copy to
		 * @param[in] regions		Copy regions
		 * @note					While it's possible to use this function, it is preferable to use the texure copy function, since this functions doesn't do additional checks on the buffers
		 *							(make sure you know what you are doing!)
		 */
		void CopyBufferToTexture(Renderer::Core::Buffer* pBuffer, Renderer::Core::Texture* pTexture, const DynArray<Renderer::TextureBufferCopyRegion>& regions) override final;
		/**
		 * Copy data from one buffer to another
		 * @param[in] pTexture		Texture to copy from
		 * @param[in] pBuffer		Buffer to copy to
		 * @param[in] region		Copy region
		 * @note					While it's possible to use this function, it is preferable to use the texure copy function, since this functions doesn't do additional checks on the buffers
		 *							(make sure you know what you are doing!)
		 */
		void CopyTextureToBuffer(Renderer::Core::Texture* pTexture, Renderer::Core::Buffer* pBuffer, const Renderer::TextureBufferCopyRegion& region) override final;
		/**
		 * Copy data from one buffer to another
		 * @param[in] pTexture		Texture to copy from
		 * @param[in] pBuffer		Buffer to copy to
		 * @param[in] regions		Copy regions
		 * @note					While it's possible to use this function, it is preferable to use the texure copy function, since this functions doesn't do additional checks on the buffers
		 *							(make sure you know what you are doing!)
		 */
		void CopyTextureToBuffer(Renderer::Core::Texture* pTexture, Renderer::Core::Buffer* pBuffer, const DynArray<Renderer::TextureBufferCopyRegion>& regions) override final;

		/**
		* Transition the layout of a texture
		* @param[in] pTexture		Texture to transition
		* @param[in] transition	Texture layout transition info
		*/
		void TransitionTextureLayout(Renderer::PipelineStage srcStage, Renderer::PipelineStage dstStage, Renderer::Core::Texture* pTexture, const Renderer::TextureLayoutTransition& transition) override final;

		/**
		 * Submit the command buffer to its queue
		 * @return	True of the command list was submitted successfully, false otherwise
		 */
		b8 Submit() override final;
		/**
		 * Submit the command buffer to its queue
		 * @param[in] waitSemaphores		Semaphores to wait on before execution
		 * @param[in] signalSemaphores	Semaphores to signal on completion
		 * @return	True of the command list was submitted successfully, false otherwise
		 */
		b8 Submit(const DynArray<Renderer::Core::Semaphore*>& waitSemaphores, const DynArray<Renderer::PipelineStage>& waitStages, const DynArray<Renderer::Core::Semaphore*>& signalSemaphores) override final;
		/**
		 * Wait for the command buffer to finish
		 * @param[in] timeout	Timeout
		 * @return	True of the command list was submitted waited on, false otherwise
		 */
		b8 Wait(u64 timeout = u64(-1)) override final;

	private:
		friend class VulkanCommandListManager;

		/**
		 * Create a command list
		 * @param[in] pContext	RHI context
		 * @param[in] pManager	Command list manager
		 * @param[in] pQueue		Queue
		 * @return	True if the command list was created successfully, false otherwise
		 */
		b8 Create(RHI::RHIContext* pContext, Renderer::Core::CommandListManager* pManager, Renderer::Core::Queue* pQueue) override final;
		/**
		 * Destroy a command list
		 * @return	True if the command list was destroyed successfully, false otherwise
		 */
		b8 Destroy() override final;

		/**
		 * Update the barriers
		 */
		void UpdateBarriers();

		VkCommandBuffer m_CommandBuffer;	/**< Vulkan command buffer */
		VulkanCommandBufferFunctions m_Vk;	/**< Vulkan command buffer functions */

		Renderer::PipelineStage m_SrcStage;
		Renderer::PipelineStage m_DstStage;
		DynArray<VkMemoryBarrier> m_GlobalBarriers;
		DynArray<VkBufferMemoryBarrier> m_BufferBarriers;
		DynArray<VkImageMemoryBarrier> m_ImageBarriers;
	};

}
