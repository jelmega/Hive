// Copyright 2018 Jelte Meganck. All Rights Reserved.
//
// VulkanContext.h: Vulkan RHI Context
#pragma once
#include "VulkanRHIPCH.h"
#include "VulkanMemory.h"

namespace Hv::VulkanRHI {
	class VulkanDevice;
	class VulkanPhysicalDevice;

	class VulkanInstance;
	
	class VulkanContext final : public RHI::RHIContext
	{
	public:
		struct VkFreeFuncs
		{
			PFN_vkCreateInstance vkCreateInstance;
			PFN_vkDestroyInstance vkDestroyInstance;
			PFN_vkEnumerateInstanceExtensionProperties vkEnumerateInstanceExtensionProperties;
			PFN_vkEnumerateInstanceLayerProperties vkEnumerateInstanceLayerProperties;
			PFN_vkEnumerateInstanceVersion vkEnumerateInstanceVersion;
			PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr;
		};
	public:

		VulkanContext();
		~VulkanContext();

		/**
		 * Initialize the RHI context
		 * @return	True if the context was initialized successfully initialized, false otherwise
		 */
		b8 Init(const RHI::RHIDesc& desc) override;
		/**
		 * Destroy the RHI context
		 * @return	True if the context was destroyed successfully initialized, false otherwise
		 */
		b8 Destroy() override;

		/**
		 * Update the physical device surface support
		 * @param[in] surface	Vulkan surface
		 */
		VkResult UpdateSurfaceSupport(VkSurfaceKHR surface);

		/**
		 * Find the most suitable physical device to create the logical device on
		 * @param[in] requestedExtensions	Requested extensions
		 * @param[in] requestedLayers		Requested layers
		 * @param[in] features				Requested features
		 * @param[in] needsPresentSupport	If the device needs present support
		 * @return							Most suitable physical device
		 */
		VulkanPhysicalDevice* GetMostSuitablePhysicalDevice(const DynArray<const AnsiChar*>& requestedExtensions, const DynArray<const AnsiChar*>& requestedLayers, const VkPhysicalDeviceFeatures& features, b8 needsPresentSupport);

		/**
		 * Get the vulkan allocation callbacks
		 * @return	Vulkan allocation callbacks
		 */
		VkAllocationCallbacks* GetAllocationCallbacks() { return &m_AllocationCallbacks; }
		/**
		 * Retrieve the vkInstanceProcAddr procedure
		 * @return	vkInstanceProcAddr procedure
		 */
		PFN_vkGetInstanceProcAddr GetVkInstanceProcAddr() const { return m_vkGetInstanceProcAddr; }

		/**
		 * Get the vulkan instance
		 * @return	Vulkan instance
		 */
		VulkanInstance* GetInstance() { return m_pInstance; }
		/**
		 * Get all vulkan physical devices
		 * @return	Vulkan physical devices
		 */
		DynArray<VulkanPhysicalDevice*>& GetPhysicalDevices() { return m_PhysicalDevices; }
		/**
		 * Get the vulkan logical device
		 * @return	Vulkan device
		 */
		VulkanDevice* GetDevice() { return m_pDevice; }
		/**
		 * Get the vulkan memory allocator
		 * @return	Vulkan memory allocator
		 */
		VulkanAllocator* GetAllocator() { return m_pAllocator; }

	private:
		DynLib::DynLibHandle m_VulkanHandle;				/**< Handle to vulkan dynamic library */
		PFN_vkGetInstanceProcAddr m_vkGetInstanceProcAddr;	/**< 'vkGetInstanceProcAddr' function */

		VkAllocationCallbacks m_AllocationCallbacks;		/**< Vulkan allocation callbacks */
		VulkanInstance* m_pInstance;						/**< Vulkan instance */
		DynArray<VulkanPhysicalDevice*> m_PhysicalDevices;	/**< Vulkan physical devices */
		VulkanDevice* m_pDevice;							/**< Vulkan device */
		VulkanAllocator* m_pAllocator;						/**< Vulkan memory allocator */
	};

}
